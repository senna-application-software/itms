<?php

namespace App\Imports\Employee;

use App\Models\Directorate;
use App\Models\Division;
use App\Models\Employee;
use App\Models\Group;
use App\Models\MasterJobFamily;
use App\Models\MasterPosition;
use App\Models\SubHolding;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

class EmployeeImport implements ToCollection, WithStartRow, SkipsEmptyRows
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $value) {
            $createdEmployee = Employee::create([
                'personnel_number'       => $value[0],
                'prev_persno'            => $value[1],
                'position'               => $value[2],
                'job_title'              => $value[3],
                'personnel_area_text'    => $value[4],
                'area_nomenklatur'       => $value[5],
                'gender_text'            => $value[6],
                'religious_denomination' => $value[7],
                'marst'                  => $value[8],
                'birthplace'             => $value[9],
                'date_of_birth'          => $this->transformDate($value[10]),
                'major_name'             => $value[11],
                'education_level'        => $value[12],
                'university_name'        => $value[13],
                'tanggal_mulai_bekerja'  => $this->transformDate($value[14]),
                'line_manager'           => $value[15],
                'no_ktp'                 => $value[16],
                'email'                  => $value[17],
                'no_hp'                  => $value[18],
                'status_aktif'           => $value[19],
                'status_karyawan'        => $value[20],
                'person_grade'           => $value[21],
                'bod_type'               => $value[22],
                'jabfung'                => $value[23],
                'home_address'           => $value[24],
                'no_npwp'                => $value[25],
            ]);

            $customUpdate = array();
            $lineManager  = Employee::where('prev_persno', $value[15])
                ->first();
            $customUpdate['line_manager_name']     = $lineManager ? $lineManager->prev_persno : null;
            $customUpdate['line_manager_position'] = $lineManager ? $lineManager->position_name : null;

            $position = MasterPosition::where('code_position', $value[2])
                ->first();

            if ($position) {
                $directorate = Directorate::where('code_directorate', $position->code_directorate)
                    ->first();
                $group = Group::where('code_group', $position->code_group)
                    ->first();
                $division = Division::where('code_division', $position->code_division)
                    ->first();
                $jobFamily = MasterJobFamily::where('code_job_family', $position->code_job_family)
                    ->first();
                $subHolding = SubHolding::where('code_subholding', $position->code_subholding)
                    ->first();

                $customUpdate['position_name']    = $position->name_position;
                $customUpdate['directorate']      = $directorate ? $directorate->name_directorate : null;
                $customUpdate['directorate_code'] = $directorate ? $directorate->code_directorate : null;
                $customUpdate['group']            = $group ? $group->name_group : null;
                $customUpdate['group_code']       = $group ? $group->code_group : null;
                $customUpdate['division']         = $division ? $division->name_division : null;
                $customUpdate['division_code']    = $division ? $division->code_division : null;
                $customUpdate['job_family']       = $jobFamily ? $jobFamily->name_job_family : null;
                $customUpdate['code_subholding']  = $subHolding ? $subHolding->code_subholding : null;
            }

            if ($createdEmployee) {
                Employee::where('id', $createdEmployee->id)
                    ->update([
                        'position_name'         => isset($customUpdate['position_name']) ? $customUpdate['position_name'] : null,
                        'directorate'           => isset($customUpdate['directorate']) ? $customUpdate['directorate'] : null,
                        'directorate_code'      => isset($customUpdate['directorate_code']) ? $customUpdate['directorate_code'] : null,
                        'division_code'         => isset($customUpdate['division_code']) ? $customUpdate['directorate_code'] : null,
                        'group_code'            => isset($customUpdate['group_code']) ? $customUpdate['directorate_code'] : null,
                        'group'                 => isset($customUpdate['group']) ? $customUpdate['group'] : null,
                        'division'              => isset($customUpdate['division']) ? $customUpdate['division'] : null,
                        'job_family'            => isset($customUpdate['job_family']) ? $customUpdate['job_family'] : null,
                        'code_subholding'       => isset($customUpdate['code_subholding']) ? $customUpdate['code_subholding'] : null,
                        'line_manager_name'     => isset($customUpdate['line_manager_name']) ? $customUpdate['line_manager_name'] : null,
                        'line_manager_position' => isset($customUpdate['line_manager_position']) ? $customUpdate['line_manager_position'] : null,
                    ]);
            }
        }
    }

    public function startRow(): int
    {
        return 2;
    }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
