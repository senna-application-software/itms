<?php

namespace App\Exceptions;

use Mail;
use Log;
use Throwable;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\ErrorHandler\ErrorRenderer\HtmlErrorRenderer;
use Symfony\Component\ErrorHandler\Exception\FlattenException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function report(Throwable $exception)
    {
        // emails.exception is the template of your email
        // it will have access to the $error that we are passing below

        if ($this->shouldReport($exception)) {
            $this->sendEmail($exception); // sends an email
        }

        return parent::report($exception);

    }

    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }

    public function sendEmail(Throwable $exception) {
        
        try {
            if (env('IS_MAIL_SEND_ERROR', false)) {
                $e          = FlattenException::create($exception);
                $handler    = new HtmlErrorRenderer(true); // boolean, true raises debug flag...
                $css        = $handler->getStylesheet();
                $content    = $handler->getBody($e);
                $header     = \Request::fullUrl();

                Mail::send('email-exception.index', compact('css','content','header'), function ($message) {
                    $message->to([env('MAIL_SEND_ERROR_ADDRESS')])
                    ->subject('Stop #ERROR500Bestie!');
                });
            }
        } catch (Throwable $exception) {
            Log::error($exception);
        }
    }

}
