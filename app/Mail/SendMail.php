<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    private $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $type = $this->data['type'];

        if($type == 'input_aspiration') {
            return $this->subject('EMAIL UNDANGAN PENGISIAN ASPIRASI')
                ->cc(["sena@kabayan.id","shasanah@gmail.com"])
                ->bcc("citra@citrairiani.com")
                ->markdown('email.input-aspirations', [
                    'prefix'            => $this->data['prefix'],
                    'nama'              => $this->data['nama'],
                    'username'          => $this->data['username'],
                    'password'          => $this->data['password'],
                    'periode_pengisian' => $this->data['periode_pengisian'],
                    'url'               => $this->data['url']
                ]);
        } else if ($type == 'update_profile') {
            return $this->subject('EMAIL UPDATE DATA PROFILE')
                ->cc(["sena@kabayan.id","shasanah@gmail.com"])
                ->bcc("citra@citrairiani.com")
                ->markdown('email.update-profile', [
                    'prefix'            => $this->data['prefix'],
                    'nama'              => $this->data['nama'],
                    'username'          => $this->data['username'],
                    'password'          => $this->data['password'],
                    'periode_update'    => $this->data['periode_update'],
                    'kandidat_nama_jabatan'     => $this->data['kandidat_nama_jabatan'],
                    'kandidat_nama_subholding'  => $this->data['kandidat_nama_subholding'],
                    'url'               => $this->data['url']
                ]);
        
        } else if ($type == 'talent_day') {
            return $this->subject('EMAIL UNDANGAN TALENT DAY ' . strtoupper($this->data['jabatan']))
                ->cc(["sena@kabayan.id","shasanah@gmail.com"])
                ->bcc("citra@citrairiani.com")
                ->markdown('email.talent-day', [
                    'nama'              => $this->data['nama'],
                    'prefix'            => $this->data['prefix'],
                    'hari_tanggal'      => $this->data['hari_tanggal'],
                    'waktu'             => $this->data['waktu'],
                    'lokasi'            => $this->data['lokasi'],
                    'agenda'            => $this->data['agenda'],
                    'jabatan'           => $this->data['jabatan'],
                    'sub_holding'       => $this->data['sub_holding'],
                ]);
        } else if ($type == 'talent_review') {
            return $this->subject('EMAIL INFORMASI TALENT REVIEW')
                ->cc(["sena@kabayan.id","shasanah@gmail.com"])
                ->bcc("citra@citrairiani.com")
                ->markdown('email.talent-review', [
                    'nama'              => $this->data['nama'],
                    'prefix'            => $this->data['prefix'],
            ]);
        }else if ($type == 'talent_selection') {
            return $this->subject('EMAIL HASIL TALENT SELECTION ' . strtoupper($this->data['posisi']))
                ->cc(["sena@kabayan.id","shasanah@gmail.com"])
                ->bcc("citra@citrairiani.com")
                ->markdown('email.talent-selection', [
                    'nama'              => $this->data['nama'],
                    'prefix'            => $this->data['prefix']
            ]);
        } else if ($type == 'pakta-integritas') {
            return $this->subject('EMAIL HASIL TALENT SELECTION ' . strtoupper($this->data['posisi']))
                ->cc(["sena@kabayan.id","shasanah@gmail.com"])
                ->bcc("citra@citrairiani.com")
                ->markdown('email.pakta-integritas', [
                    'nama'              => $this->data['nama'],
                    'prefix'            => $this->data['prefix'],
                    'posisi'            => $this->data['posisi'],
                    'area'              => $this->data['area'],
                    'name_subholding'       => $this->data['name_subholding']
                ])
                ->attach(public_path('pakta-integritas/' . $this->data['filename']))
                ->attach(public_path('dok-kepegawaian/' . $this->data['filename_sk']));
        }
    }
}