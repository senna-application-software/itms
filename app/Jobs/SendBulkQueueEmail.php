<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendBulkQueueEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    public $timeout = 7200; // 2 jam

    /**
     * Create a new job instance.
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     * @return void
     */
    public function handle()
    {
        $i = 0;
        foreach ($this->data as $value) {
            if (env('IS_MAIL_SEND_HAS_LIMIT', TRUE)) {
                if($i < 5) {
                    $detail["emails"]   = $value['emails'];
                    $detail["type"]     = $value['type'];
                    $detail["prefix"]   = $value['prefix'];
                    $detail["nama"]     = $value['nama'];
        
                    if($value['type'] == 'input_aspiration') {
                        $detail["username"] = $value['username'];
                        $detail["password"] = $value['password'];
                        $detail["periode_pengisian"] = $value['periode_pengisian'];
                    } else if ($value['type'] == 'talent_day') {
                        $detail["hari_tanggal"] = $value['hari_tanggal'];
                        $detail["waktu"] = $value['waktu'];
                        $detail["lokasi"] = $value['lokasi'];
                        $detail["agenda"] = $value['agenda'];
                        $detail["jabatan"] = $value['jabatan'];
                        $detail["sub_holding"] = $value['sub_holding'];
                    }else if ($value['type'] == 'update_profile') {
                        $detail["username"] = $value['username'];
                        $detail["password"] = $value['password'];
                        $detail["periode_update"] = $value['periode_update'];
                        $detail["kandidat_nama_jabatan"] = $value['kandidat_nama_jabatan'];
                        $detail["kandidat_nama_subholding"] = $value['kandidat_nama_subholding'];
                    } else if ($value['type'] == 'pakta-integritas') {
                        $detail["filename"] = $value['filename'];
                        $detail["name_subholding"] = $value['name_subholding'];
                        $detail["posisi"] = $value['posisi'];
                        $detail["area"] = $value['area'];
                        $detail['filename_sk'] = $value['filename_sk'];
                    } else if ($value['type'] == 'talent_selection') {
                        $detail["posisi"] = $value['posisi'];
                    }
                        $detail["url"]      = $value['url'];
                    sendmail($detail, $detail);
                    sleep(5);
                } else {
                    break;
                }   
                $i++;
            } else {
                $detail["emails"]   = $value['emails'];
                $detail["type"]     = $value['type'];
                $detail["prefix"]   = $value['prefix'];
                $detail["nama"]     = $value['nama'];
    
                if($value['type'] == 'input_aspiration') {
                    $detail["username"] = $value['username'];
                    $detail["password"] = $value['password'];
                    $detail["periode_pengisian"] = $value['periode_pengisian'];
                } else if ($value['type'] == 'talent_day') {
                    $detail["hari_tanggal"] = $value['hari_tanggal'];
                    $detail["waktu"] = $value['waktu'];
                    $detail["lokasi"] = $value['lokasi'];
                    $detail["agenda"] = $value['agenda'];
                    $detail["jabatan"] = $value['jabatan'];
                    $detail["sub_holding"] = $value['sub_holding'];
                }else if ($value['type'] == 'update_profile') {
                    $detail["username"] = $value['username'];
                    $detail["password"] = $value['password'];
                    $detail["periode_update"] = $value['periode_update'];
                    $detail["kandidat_nama_jabatan"] = $value['kandidat_nama_jabatan'];
                    $detail["kandidat_nama_subholding"] = $value['kandidat_nama_subholding'];
                } else if ($value['type'] == 'pakta-integritas') {
                    $detail["filename"] = $value['filename'];
                    $detail["name_subholding"] = $value['name_subholding'];
                    $detail["posisi"] = $value['posisi'];
                    $detail["area"] = $value['area'];
                    $detail['filename_sk'] = $value['filename_sk'];
                } else if ($value['type'] == 'talent_selection') {
                    $detail["posisi"] = $value['posisi'];
                }
                    $detail["url"]      = $value['url'];
                sendmail($detail, $detail);
                sleep(5);
            }   
        }
    }
    
    public function failed(Exception $exception)
    {
        // Send user notification of failure, etc...
        // echo "Error";
    }
}
