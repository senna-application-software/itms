<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class Performance extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    protected $table = 'master_performance';
    protected $primaryKey = 'id';  
    protected $guarded = [];
    
    public function __get($key)
    {
        if(in_array($key, Schema::getColumnListing($this->getTable())) && !is_null($this->getAttribute($key))){
            return ucwords($this->getAttribute($key));
        } else {
            return $this->getAttribute($key);
        }
    }
}
