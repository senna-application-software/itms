<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeePasangan extends Model
{
    use HasFactory;
    protected $table = "employees_pasangan";
    protected $fillable = [
        'nik',
        'nama',
        'tmp_lahir',
        'tgl_lahir',
        'tgl_menikah',
        'pekerjaan',
        'keterangan'
    ];
}
