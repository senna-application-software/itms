<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterPosition extends Model
{
    use HasFactory;
    protected $dates      = ['deleted_at'];
    protected $table      = 'master_position';
    protected $primaryKey = 'id';
    protected $guarded    = ['id'];

    public function masterPositionLevel()
    {
        return $this->belongsTo(PositionLevel::class, 'code_position_level', 'code_position_level');
    }
}
