<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeReferensi extends Model
{
    use HasFactory;
    protected $table = 'employees_references';
    protected $fillable = [
        'nik',
        'nama',
        'perusahaan',
        'jabatan',
        'no_hp'
    ];
}
