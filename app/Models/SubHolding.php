<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubHolding extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    protected $table = 'master_subholding';
    protected $primaryKey = 'id';  
    protected $guarded = [];

    public function positions() {
        return $this->hasMany(Position::class, "code_subholding", "code_subholding");
    }
}
