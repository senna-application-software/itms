<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/*
use App\EmployeeTalent'
*/

class MasterHukdis extends Model
{
    protected $dates = ['deleted_at'];
    protected $guard = 'employee';
    protected $table = 'master_hukdis';
    protected $primaryKey = 'id';  
    protected $guarded = [];
    
    protected $fillable = [
    'id', 'nik', 'is_active'   
    ];
    
}
