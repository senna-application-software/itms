<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KlusterBumn extends Model
{
    use HasFactory;
    protected $table = "master_klaster_bumn";
}
