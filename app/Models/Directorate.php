<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class Directorate extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    protected $table = 'master_directorate';
    protected $primaryKey = 'id';  
    protected $guarded = [];

    public function __get($key)
    {
        if($key == 'name_directorate' && !is_null($this->getAttribute($key))){
            return ucwords(strtolower($this->getAttribute($key)));
        } else {
            return $this->getAttribute($key);
        }
    }
}
