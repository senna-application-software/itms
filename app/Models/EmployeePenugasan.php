<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeePenugasan extends Model
{
    use HasFactory;
    protected $table = 'employee_penugasan';
    protected $fillable = [
        'nik',
        'penugasan',
        'tupoksi',
        'instansi_perusahaan',
        'start_date',
        'end_date'
    ];
}
