<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NilaiAssessment extends Model
{
    use HasFactory;
    protected $table = 'master_nilai_assessment';
    protected $fillable = [
        'id_pertanyaan',
        'nik',
        'sub_event',
        'nik_talentcom',
        'nilai',
        'keterangan',
        'is_draft'
    ];
}
