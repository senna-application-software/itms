<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Schema;

class Position extends Model
{
    //
    protected $table = 'master_position';
    protected $fillable = [
        'code_position', 
        'name_position', 
        'code_position_type',
        'code_position_level', 
        'code_division', 
        'code_group',
        'code_directorate',
        'code_job_family',
        'job_cluster',
        'org_level',
        'band_mid',
        'band_max',
        'band_notes',
        'grade_min',
        'grade_max',
        'grade_notes',
        'code_subholding',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function __get($key)
    {
        if($key == "name_position" && !is_null($this->getAttribute($key))){
            return capitalize_roman($this->getAttribute($key));
        } else {
            return $this->getAttribute($key);
        }
    }

    public function employees() {
        return $this->hasMany(Employee::class, "position", "code_position");
    }

    public function collated_employees() {
        return $this->hasMany(Employee::class, "position", DB::raw("master_position.code_position collate utf8mb4_unicode_ci"));
    }

    public function talent_aspirations() {
        return $this->hasMany(TalentAspiration::class, "code_position", "code_position");
    }
    
    public function division() {
        return $this->belongsTo(Division::class, "code_division", "code_division");
    }
    public function group() {
        return $this->belongsTo(Group::class, "code_group", "code_group");
    }
    public function directorate() {
        return $this->belongsTo(Directorate::class, "code_directorate", "code_directorate");
    }

    public function sub_holding() {
        return $this->belongsTo(SubHolding::class, "code_subholding", "code_subholding");
    }
    
    public function master_area() {
        return $this->belongsTo(MasterArea::class, "area", "code_area");
    }

    public function position_level() {
        return $this->belongsTo(PositionLevel::class, "code_position_level", "code_position_level");
    }

    public function pertanyaan() {
        return $this->belongsTo(Pertanyaan::class, 'code_division', 'code_division');
    }

    public function employee_to_position() {
        return $this->hasMany(EmployeesToPositions::class, "position_code", "code_position");
    }

    public function sub_event() {
        return $this->hasmany(SubEvent::class, 'position', "code_position");
    }

    public function get_sub_holding() {
        return $this->hasOne(SubHolding::class, "code_subholding", "code_subholding");
    }

    public function job_family() {
        return $this->belongsTo(JobFamily::class, 'code_job_family', 'code_job_family');
    }

    public function job_family_align() {        
        $code_job_family_align = [
            $this->code_job_family,
            $this->code_job_family_2,  
            $this->code_job_family_3
        ];
        $code_job_family_align = array_filter($code_job_family_align);

        if (empty($code_job_family_align)) {
            return "-";
        }

        $job_family = MasterJobFamily::whereIn('code_job_family', $code_job_family_align)
            ->get()->pluck("name_job_family")->map(function($value) {
                return ucwords(trim(strtolower($value)))."<br>";
            })->toArray();
        return implode(" ", $job_family);
    }

    public function masterPositionType() {
        return $this->belongsTo(PositionType::class, 'code_position_type', 'code_position_type');
    }
}
