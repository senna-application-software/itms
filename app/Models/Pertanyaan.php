<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    use HasFactory;
    protected $table = 'master_pertanyaan';

    public function nilai_assessment() {
        return $this->belongsTo(NilaiAssessment::class, 'id', 'id_pertanyaan');
    }
}
