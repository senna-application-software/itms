<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeKarya extends Model
{
    use HasFactory;
    protected $table = 'employees_karya';
    protected $fillable = [
        'nik',
        'judul',
        'tahun'
    ];
}
