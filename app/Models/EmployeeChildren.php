<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeChildren extends Model
{
    use HasFactory;
    protected $table = 'employees_children';
    protected $fillable = [
        'nik',
        'nama',
        'tmp_lahir',
        'tgl_lahir',
        'jns_kelamin',
        'pekerjaan',
        'keterangan'
    ];
}
