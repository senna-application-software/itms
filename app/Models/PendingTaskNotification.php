<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PendingTaskNotification extends Model
{
    use HasFactory;

    protected $table = 'pending_task_notification';

    public function employee()
    {
        return $this->hasOne(Employee::class,'id','employee_id');
    }

    public function position_event()
    {
        return $this->hasOne(Position::class,'code_position','position');
    }

    public function job_holder_position()
    {
        return $this->hasOne(Employee::class,'id','job_holder');
    }

    public function prev_positions()
    {
        return $this->hasOne(Position::class,'code_position','prev_position');
    }
}
