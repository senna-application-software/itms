<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventEmployeeEligibleModel extends Model
{
    use HasFactory;
    protected $table = 'event_employee_eligible';
    protected $fillable = [
        'nik',
        'id_event_window_time',
        'created_at',
        'updated_at'
    ];
}
