<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class XNAObserverModel extends Model
{
    use HasFactory;
    protected $table = 'xna_observer';
    protected $fillable = [
        'nik', 'content', 'slug', 'fill_by_observer'
    ];
}
