<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeesDocs extends Model
{
    use HasFactory;
    protected $table = 'employees_docs';
    protected $fillable = [
        'nik',
        'nm_peg_docs',
        'nm_pkt_docs',
        'tgl'
    ];
}
