<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeOrganization extends Model
{
    use HasFactory;
    protected $table = "employee_organization";
    protected $fillable = [
        'nik',
        'nama_keg_org',
        'jabatan',
        'uraian_singkat',
        'type',
        'start_date',
        'end_date'
    ];
}
