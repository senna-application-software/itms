<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Roles\EloquentRole as SentinelRole;

class Roles extends SentinelRole
{
    use HasFactory;
    protected $table = 'roles';
    protected $fillable
        = [
            'slug',
            'name',
            'permissions',
            'created_at',
            'updated_at',
        ];
}
