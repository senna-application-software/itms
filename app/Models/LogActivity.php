<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogActivity extends Model {
    use SoftDeletes;

    public $table = 'log_activities';

    protected $fillable = [
        'menu',
        'type',
        'user_id',
        'desc',
        'url',
        'data',
        'ip_address',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
