<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calibration extends Model
{
    //
    protected $table = 'employee_calibration';
    protected $fillable = ['user_id', 'panel', 'tahun', 'created_by', 'justification','is_box0'];


    public function employee()
    {
        return $this->hasOne('App\Models\Employee', 'prev_persno', 'user_id');
    }
}
