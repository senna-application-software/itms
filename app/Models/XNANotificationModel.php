<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class XNANotificationModel extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'xna_notification';
    protected $fillable = [
        'nik', 'content'
    ];
}
