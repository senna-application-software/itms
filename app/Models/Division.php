<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    protected $table = 'master_division';
    protected $primaryKey = 'id';  
    protected $guarded = [];
}
