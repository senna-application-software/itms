<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeesStatusUpdate extends Model
{
    use HasFactory;
    protected $table = 'employees_status_update';
    protected $fillable = [
        'event_window_time',
        'nik',
        'is_ket_perorangan',
        'is_interest',
        'is_keahlian',
        'is_riwayat_jabatan',
        'is_keanggotaan_org',
        'is_penghargaan',
        'is_riwayat_pendidikan',
        'is_karya_ilmiah',
        'is_peng_pemb_nara',
        'is_referensi',
        'is_ket_keluarga',
        'is_peng_keahlian',
        'is_peng_bumn',
        'is_asp'
    ];
}
