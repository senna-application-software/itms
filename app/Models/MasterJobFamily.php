<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;

class MasterJobFamily extends Model
{
    use SoftDeletes, HasFactory;
    protected $dates = ['deleted_at'];
    protected $table = 'master_job_family';
    protected $primaryKey = 'id';  
    protected $guarded = [];

    public function __get($key)
    {
        if($key == "name_job_family" && !is_null($this->getAttribute($key))){
            return ucwords(strtolower($this->getAttribute($key)));
        } else {
            return $this->getAttribute($key);
        }
    }
}
