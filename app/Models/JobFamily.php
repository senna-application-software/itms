<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobFamily extends Model
{
    use HasFactory;
    protected $table   = 'master_job_family';
    protected $guarded = ['id'];
}
