<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FungsiJabatanBumn extends Model
{
    use HasFactory;
    protected $table = 'master_fungsi_jabatan_bumn';
}
