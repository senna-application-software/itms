<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeKelas extends Model
{
    use HasFactory;
    protected $table = 'employees_kelas';
    protected $fillable = [
        'nik',
        'id_kelas',
        'status',
        'type'
    ];
}
