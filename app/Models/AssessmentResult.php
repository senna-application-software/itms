<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB; 
class AssessmentResult extends Model
{
    use HasFactory;
    protected $table = 'assessment_result';
    protected $guarded = [];

    public function employee() {
        return $this->belongsTo(Employee::class, 'nik', DB::raw('employees.prev_persno collate utf8mb4_general_ci'));
    }
}
