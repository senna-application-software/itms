<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'master_education';
    protected $fillable = [
        'nik',
        'level_education',
        'major_name',
        'university_name',
        'kota_negara',
        'penghargaan',
        'tahun_lulus'
    ];
}
