<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    use HasFactory;
    protected $table = 'box';
    
    public function box_cat()
    {
        return $this->belongsTo(BoxCategory::class, 'id_box_cat', 'id');
    }

}
