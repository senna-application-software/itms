<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubEventTalentComitee extends Model
{
    use HasFactory;
    protected $table = "sub_event_tcom";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function employees() {
        return $this->belongsTo(Employee::class, 'nik', 'prev_persno');
    }
}
