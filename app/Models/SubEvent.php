<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SubEvent extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'sub_event';
    protected $fillable = [
        'id_event',
        'name',
        'position', 
        'status',
        'vacant_type',
        'created_at', 
        'updated_at',
        'sourcing_type',
        'talent_id_ref_from_selection'
    ];
    protected $dates = ['deleted_at'];
    public function event() {
        return $this->belongsTo(EventWindowTimeModel::class, 'id_event');
    }

    public function talent() {
        return $this->hasMany(Talent::class, 'sub_event');
    }

    public function position() {
        return $this->belongsTo(Position::class, 'position', 'code_position');
    }

    public function get_position() {
        return $this->hasOne(Position::class, 'code_position', 'position');
    }
}
