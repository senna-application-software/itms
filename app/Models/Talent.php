<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Talent extends Model
{
    use SoftDeletes;
    
    protected $table = 'talent';
    protected $fillable = [
        'nik', 
        'cci',
        'cci_proposed',
        'performance',
        'performance_proposed',
        'panel',
        'proposed_box',
        'justification_box',
        'is_mapping',
        'is_profilling',
        'is_final',
        'is_change_box',
        'status',
        'sub_event',
    ];
    
    public function employee() {
        return $this->hasOne(Employee::class, 'prev_persno', 'nik');
    }
    
    public function collated_employee() {
        return $this->belongsTo(Employee::class, "nik", DB::raw("employees.prev_persno collate utf8mb4_general_ci"));
    }

    public function sub_event() {
        return $this->belongsTo(SubEvent::class, 'sub_event', 'id');
    }

    public function get_sub_event() {
        return $this->hasOne(SubEvent::class, 'id', 'sub_event');
    }

    public function box() {
        return $this->belongsTo(Box::class, 'panel', 'id');
    }
    public function employee_to_position()
    {
        return $this->hasOne(EmployeesToPositions::class, 'nik', 'nik');
    }
    public function event() {
        return $this->belongsTo(EventWindowTimeModel::class, 'id_event');
    }

    public function sub_events(){
        return $this->hasOne(SubEvent::class,'id','sub_event');
    }

    public function talent_committee() {
        return $this->hasMany(SubEventTalentComitee::class, 'id_subevent', 'sub_event');
    }
}
