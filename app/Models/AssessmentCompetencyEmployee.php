<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB; 
class AssessmentCompetencyEmployee extends Model
{
    use HasFactory;
    protected $table = 'assessment_result_by_competency';
    protected $guarded = [];

    public function employee() {
        return $this->belongsTo(Employee::class, 'nik', DB::raw('employees.prev_persno collate utf8mb4_general_ci'));
    }

    public function uncollated_employee() {
        return $this->belongsTo(Employee::class, 'nik', 'prev_persno');
    }
}
