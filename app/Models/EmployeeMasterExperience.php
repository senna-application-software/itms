<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeMasterExperience extends Model
{
    use HasFactory;
    protected $table = 'employees_master_exp';
    protected $fillable = [
        'nik',
        'name',
        'deskripsi',
        'type'
    ];
}
