<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengalamanKelasBumn extends Model
{
    use HasFactory;
    protected $table = 'master_kelas_bumn';
}
