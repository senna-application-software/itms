<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDiklat extends Model
{
    use HasFactory;
    protected $table = 'employee_diklat';
    protected $fillable = [
        'nik',
        'nama_kegiatan',
        'penyelenggara',
        'lama',
        'nomor_sertifikasi',
        'jenis'
    ];
}
