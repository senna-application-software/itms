<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalibrationHistory extends Model
{
    protected $table = 'employee_calibration_history';
    protected $fillable = ['user_id', 'panel', 'change_by_nik','updated_at','tahun','created_by', 'panel_before', 'sub_event'];


    public function get_employee()
    {
        return $this->hasOne('App\Models\Employee', 'prev_persno', 'user_id');
    }

    public function assignee()
    {
        return $this->hasOne('App\Models\Employee', 'prev_persno', 'change_by_nik');
    }
}
