<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDetailHistoryPosition extends Model
{
    use HasFactory;
    protected $table = 'employee_detail_history_position';
    protected $fillable = [
        'id_history_position',
        'uraian_singkat',
        'achievement'
    ];
}
