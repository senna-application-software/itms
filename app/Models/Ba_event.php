<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ba_event extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    protected $table = 'ba_event';
    protected $primaryKey = 'id';  
    protected $guarded = [];

    public function subevent() {
        return $this->hasOne(SubEvent::class, "id_event", "id_sub_event");
    }
}
