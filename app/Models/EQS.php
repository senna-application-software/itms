<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EQS extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    protected $table = 'master_eqs';
    protected $primaryKey = 'id';  
    protected $guarded = [];
    
    public function element_eqs(){
        return $this->hasMany(ElementEQS::class, 'id', 'id_eqs');
    }
}
