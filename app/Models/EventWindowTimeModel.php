<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventWindowTimeModel extends Model
{
    use HasFactory;
    protected $table = 'event_window_time';
    protected $fillable = [
        'year',
        'batch',
        'start_date',
        'end_date',
        'status',
        'created_by',
        'created_at',
        'updated_at'
    ];
}
