<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeKluster extends Model
{
    use HasFactory;
    protected $table = 'employees_klaster';
    protected $fillable = [
        'id_kluster',
        'nik',
        'status',
        'type'
    ];
}
