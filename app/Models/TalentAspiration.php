<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TalentAspiration extends Model
{
    use SoftDeletes;

    protected $table    = 'talent_aspiration';
    protected $fillable = [
        'id_event',
        'code_position',
        'nik',
        'aspiration_type',
        'is_add_talent_sourcing',
        'comitte_proposal',
    ];

    public function positions()
    {
        return $this->belongsTo(Position::class, "code_position", "code_position");
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, "nik", "prev_persno");
    }

}
