<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeJabatanBumn extends Model
{
    use HasFactory;
    protected $table = 'employees_jbtn_bumn';
    protected $fillable = [
        'id_jbtn_bumn',
        'nik',
        'status',
        'type'
    ];
}
