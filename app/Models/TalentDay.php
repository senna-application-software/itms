<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TalentDay extends Model
{
    protected $table = 'talent_day';
    protected $fillable = [
        'id_sub_event', 
        'tanggal', 
        'jam',
        'updated_at',
        'lokasi',
        'agenda', 
        'jabatan', 
        'nama_subholding'
    ];
}
