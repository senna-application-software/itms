<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PositionHistory extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'master_position_history';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'nik',
        'name',
        'start_date',
        'end_date',
        'position_name',
        'working_unit',
        'division_name',
        'directorate',
        'job_family',
        'band_name',
        'grade_name',
        'area_name',
        'code_subholding',
        'uraian_singkat',
        'achievement'
    ];

    public function __get($key)
    {
        if($key == "position_name" && !is_null($this->getAttribute($key))){
            return capitalize_roman($this->getAttribute($key));
        } else {
            return $this->getAttribute($key);
        }
    }
}
