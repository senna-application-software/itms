<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeHistoryPosition extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    protected $table = 'master_position_history';
    protected $primaryKey = 'id';  
    protected $guarded = [];
    
}