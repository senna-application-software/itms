<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EmployeesToPositions extends Model
{
    use SoftDeletes;
    
    protected $table = 'employees_to_positions';
    protected $fillable = [
        'nik', 
        'position_code', 
        'start_date', 
        'end_date',
    ];

    public function employee()
    {
        return $this->hasOne(Employee::class, 'prev_persno', 'nik');
    }

    public function positions()
    {
        return $this->belongsTo(Position::class, 'position_code', 'code_position');
    }
}
