<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventPositionEligibleModel extends Model
{
    use HasFactory;
    protected $table = 'event_position_eligible';
    protected $fillable = [
        'position_code',
        'id_event_window_time',
        'created_at',
        'updated_at'
    ];
}
