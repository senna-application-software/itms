<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeEQS extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    protected $table = 'employee_eqs';
    protected $primaryKey = 'id';  
    protected $guarded = [];
    
}