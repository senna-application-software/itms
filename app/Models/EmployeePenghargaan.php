<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeePenghargaan extends Model
{
    use HasFactory;
    protected $table = 'employee_penghargaan';
    protected $fillable = [
        'nik',
        'jenis_penghargaan',
        'tingkat',
        'diberikan_oleh',
        'tahun'
    ];
}
