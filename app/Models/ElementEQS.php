<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElementEQS extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    protected $table = 'element_eqs';
    protected $primaryKey = 'id';  
    protected $guarded = [];
    
}
