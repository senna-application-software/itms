<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Schema;

/*
use App\EmployeeTalent'
*/

class Employee extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $guard = 'employee';
    protected $table = 'employees';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    // protected $appends = ['year_of_position', 'year_of_service', 'year_of_grade', 'last_year_box','in_position'];

    // protected $fillable = [

        // "id","user_id","personnel_no","personnel_number","prev_persno","position","position_name","job_title","ps_group","lvl","directorate","directorate_code","chief","chief_code","group","group_code","division","division_code","name_of_organizational_unit","organization_abbr","p_subarea_text","personnel_area_text","name_of_employee_group","name_of_ee_subgroup","gender_text","religious_denomination","marst","birthplace","date_of_birth","pendidikan_terakhir","jurusan","tanggal_akhir_kerja","tanggal_mulai_bekerja","supervisor_area","nationality","abbreviation","payroll_area_text","work_schedule_rule","name","div_name","line_manager","line_manager_name","cost","cost_ctr","total_cost","budget","deleted_at","created_at","updated_at","directorate_id","chief_id","group_id","division_id","department_id","email","status","is_acting","phone","wsr","addr","city","state","country","phone2","postal_code","job_key_short","job_key_text","office_name","office_phone","office_addr","office_city","office_state","office_post_code","direct_line","ext"    

        // "tanggal_akhir_kerja",
        // "tanggal_mulai_bekerja",
        // "personnel_number",
        // "position_name",
        // "interest",
        // "gender_text",
        // "religious_denomination",
        // "birthplace",
        // "date_of_birth",
        // "major_name",
        // "home_address",
        // "no_npwp",
        // "sosmed_address",
        // "no_ktp",
        // "no_hp",
        // "email"
    // ];

    public function __get($key)
    {
        // if(in_array($key, Schema::getColumnListing($this->getTable())) && !is_null($this->getAttribute($key))){
        if($key == "personnel_number" && !is_null($this->getAttribute($key))){
            return ucwords(strtolower($this->getAttribute($key)));
        } else {
            return $this->getAttribute($key);
        }
    }
    
    public function getBdayFormatAttribute() {
        if ($this->attributes['date_of_birth']) {
            return tanggal_indo($this->attributes['date_of_birth']);
        } else {
            return "-";
        }
    }

    public function employee_calibration()
    {
        return $this->hasMany('App\Models\Calibration', 'user_id', 'prev_persno');
    }

    public function employee_calibration_history()
    {
        return $this->hasMany(CalibrationHistory::class, 'user_id', 'prev_persno');
    }

    public function positions()
    {
        return $this->belongsTo(Position::class, "position", "code_position");
    }

    public function performance()
    {
        return $this->hasMany(Performance::class, "nik", "prev_persno")->whereNotNull('perfomance_category_align')->groupBy('year', 'nik')->orderBy('year', 'ASC');
    }

    public function bawahan()
    {
        return $this->hasMany(Employee::class, "line_manager", "prev_persno");
    }

    public function talent_aspiration()
    {
        return $this->hasMany(TalentAspiration::class, "nik", "prev_persno");
    }

    public function hukdis()
    {
        return $this->hasMany(MasterHukdis::class, "nik", DB::raw("employees.prev_persno collate utf8mb4_unicode_ci"));
    }

    public function talent()
    {
        return $this->hasMany(Talent::class, "nik", "prev_persno");
    }

    public function collated_positions()
    {
        return $this->belongsTo(Position::class, DB::Raw("employees.position collate utf8mb4_unicode_ci"), "code_position");
    }

    public function collated_talent_aspiration()
    {
        return $this->hasMany(TalentAspiration::class,"nik",DB::raw("employees.prev_persno collate utf8mb4_general_ci"));
    }

    public function assessment_competencies()
    {
        return $this->hasMany(AssessmentCompetencyEmployee::class, "nik", "prev_persno");
    }

    public function collated_assessment_competencies()
    {
        return $this->hasMany(AssessmentCompetencyEmployee::class, "nik", DB::raw("employees.prev_persno collate utf8mb4_general_ci"));
    }

    public function subholding()
    {
        return $this->belongsTo(SubHolding::class, "code_subholding", "code_subholding");
    }

    public function get_subholding()
    {
        return $this->hasOne(SubHolding::class, "code_subholding", "code_subholding");
    }
    public function master_posisi()
    {
        return $this->hasOne(MasterPosition::class, "code_position", "position");
    }

    public function master_area()
    {
        return $this->belongsTo(MasterArea::class, "code_area", "area_nomenklatur");
    }
}
