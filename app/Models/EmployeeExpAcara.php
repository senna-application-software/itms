<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeExpAcara extends Model
{
    use HasFactory;
    protected $table = 'employees_exp_acara';
    protected $fillable = [
        'nik',
        'acara_tema',
        'penyelenggara',
        'periode',
        'lokasi_peserta'
    ];
}
