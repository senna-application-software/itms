<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\Models\Talent;
use App\Models\User;
use App\Http\Contorollers\TalentSourcingController;

class ProfileUpdateNotification extends Notification
{
    use Queueable;

    public function __construct(User $user, Talent $item)
    {
        $this->item = $item;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'title'     => 'Undangan Update Profile',
            'message'   => 'Salam InJourney Bestie, Dengan ini dimohon untuk melakukan pengisian update profile!',
            'action'    => route("my-profile", ["read"])
        ];
    }
}