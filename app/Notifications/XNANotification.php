<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\Models\XNANotificationModel;
use App\Models\User;
use App\XNA\XNATestController;

class XNANotification extends Notification
{
    use Queueable;

    public function __construct(User $user, XNANotificationModel $item)
    {
        $this->item = $item;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/dashboard/'.$this->item->id);

        return (new MailMessage)
            ->subject('Notification Via Email')
            ->markdown('email-notif.index', [
                'prefix'            => "Bapak",
                'nama'              => "Senna",
                'username'          => "senna@kabayan.id",
                'password'          => "itms2020",
                'periode_pengisian' => "02-02-2022 s.d 28-02-2022",
                'url'               => $url
            ]);
            // ->attach(asset('media/logos/'. env('COMPANY_LOGO')), [
            //     'as' => 'logo-injourney.png',
            //     'mime' => 'image/png',
            // ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => 'New Notification - Test.',
            'action' => url("/")
        ];
    }
}