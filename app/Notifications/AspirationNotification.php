<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\User;

class AspirationNotification extends Notification
{
    use Queueable;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'title'     => 'Undangan Pengisian Aspirasi',
            'message'   => 'Salam InJourney Bestie, Dengan ini dimohon untuk melakukan pengisian aspirasi!',
            'action'    => route("talent-planning", ["page" => "individual"])
        ];
    }
}