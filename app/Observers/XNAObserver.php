<?php

namespace App\Observers;

use App\Models\XNAObserverModel;

class XNAObserver
{
    public function created(XNAObserverModel $item)
    {
        $item->fill_by_observer = "filled by XNAObserver";
        $item->slug = \Str::slug($item->content);
        $item->save();
    }
}