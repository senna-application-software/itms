<?php

namespace App\Exports\Employee;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TemplateImportEmployee implements WithHeadings, WithStyles
{
    public function headings(): array
    {
        return [
            'Personnel Number',
            'Prev Persno',
            'Code Position',
            'Job Title',
            'Personnel Area Text',
            'Area Nomenklatur',
            'Gender Text',
            'Religious Denomination',
            'Marst',
            'Birthplace',
            'Date of Birth',
            'Major Name',
            'Education Level',
            'University Name',
            'Started Date Work',
            'Line Manager Prev Persno',
            'Residence Card Number',
            'Email',
            'Phone Number',
            'Status Active',
            'Employee Status',
            'Person Grade',
            'BOD Type',
            'Functional',
            'Home Address',
            'Tax ID Number',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1 => ['font' => ['bold' => true]],
        ];
    }
}
