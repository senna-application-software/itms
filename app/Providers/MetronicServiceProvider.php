<?php

namespace App\Providers;

use App\Classes\Theme\Init;
use App\Classes\Theme\InitExecutive;
use App\Classes\Theme\InitRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class MetronicServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // InitExecutive::run();
        Init::run();
    }
}
