<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;


use App\Observers\XNAObserver;
use App\Models\XNAObserverModel;

use App\Observers\XNAObserverNotif;
use App\Models\XNANotificationModel;
use App\Models\User;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        
        'App\Events\XNAEvent' => [
            'App\Listeners\XNAListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        XNAObserverModel::observe(XNAObserver::class);
    }

    // public function shouldDiscoverEvents()
    // {
    //     return true;
    // }
}
