<?php

namespace App\Http\Middleware;

use App\Models\LogActivity;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Sentinel;

class LogMiddlewareApps {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        LogActivity::create([
            'user_id'    => Sentinel::getUser()->id,
            'url'        => URL::current(),
            'data'       => json_encode($request->all()),
            'ip_address' => $request->ip(),
        ]);

        return $next($request);
    }
}
