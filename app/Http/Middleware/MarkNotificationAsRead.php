<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\User;

class MarkNotificationAsRead
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        if($request->has('read')) {
            $notification = $request->user()->notifications()->where('id', $request->read)->first();
            if($notification) {
                $notification->markAsRead();
            }
        }

        if($request->has('readAll')) {                
            $request->user()->notifications()->get()->markAsRead();
        }

        $is_admin = false;
        if (Sentinel::check()) {
            $is_admin =  Sentinel::check()->inRole('admin');
        }

        if($request->has('dan') && $is_admin) {
            // has(dan) a.k.a Delete All Notif
            \DB::table('notifications')->truncate();
        }
        return $next($request);
    }
}
