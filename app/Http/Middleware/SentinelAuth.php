<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SentinelAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $message = null)
    {
        if (Sentinel::check()) {
            if (Sentinel::check()->roles->count() > 1 && !Session::get('role')) {
                session()->put('role', '');
                return redirect('/role-switch');
            }
            return redirect(RouteServiceProvider::HOME);
        } elseif ($message != null) {
            return $next($request);
        }
        return $next($request);
    }
}
