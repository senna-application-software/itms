<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Session;
class CheckRole
{
    public function handle($request, Closure $next, $role)
    {
        $is_exist_at_master_employee = isset(Sentinel::getUser()->employee->personnel_number);
        $except_for_nik = ["admin","superadmin"];
        
        if (! $is_exist_at_master_employee ) {
            if (isset(Sentinel::check()->nik)) {
                $nik = Sentinel::check()->nik;
                if (! in_array($nik, $except_for_nik)) {
                    Sentinel::logout(null, true);
                    Session::flash('failed', __('Maaf, Data Anda belum dimasukkan kedalam master employee. Silahkan hubungi admin'));
                    return redirect()->route('auth.login');
                }

                return $next($request);
            }
            
            return redirect('/')->with(['errors' => 'Session Anda telah berakhir.<br/>Silahkan login ulang']);
        }

        $user = Sentinel::check();

        if ($user) {
            $roles = Sentinel::findById($user->id)->roles->first();
            if ($roles) {
                $role_array = explode("|", $role);
                if (in_array($roles->slug, $role_array) == false) {
                    return redirect()->back()->withError('errors');
                }
            } else if(!$roles) {
                Sentinel::logout(null, true);
                return redirect()->back()->with(['errors' => 'Role has not been set']);
            }

            return $next($request);
        }
        return redirect('/')->with(['errors' => 'Session Anda telah berakhir.<br/>Silahkan login ulang']);
    }
}