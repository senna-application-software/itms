<?php

namespace App\Http\Controllers\Aspirations;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EQS;
use App\Models\EventWindowTimeModel;
use App\Models\Position;
use App\Models\TalentAspiration;
use Sentinel;
use Illuminate\Http\Request;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Redirect;

class AspirationsController extends Controller
{
    private $error_403_custome_message = 'Tidak berhak mengakses halaman ini';
    private $error_404_custome_message = 'Halaman Tidak Ditemukan';
    private $show_engage = false;

    public function __construct() {
        $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first();
        if (is_null($event_id)) {
            $this->show_engage = true;
        }
    }

    public function index($page) {
        $level_akses = 'employee';

        if ($level_akses !== 'employee' ) {
            abort('403', $this->error_403_custome_message);
        }

        switch($page) {
            case 'individual':
                return $this->aspiration_individual();
            break;

            case 'job_holder':
                return $this->aspiration_job_holder();
            break;
            
            case 'supervisor':
                return $this->aspiration_supervisor();
            break;
            
            case 'unit':
                return $this->aspiration_unit();
            break;
            
            default:
                abort('404', $this->error_404_custome_message);
            break;
        }
    }

    public function aspiration_individual(){
        $page_title = 'Individual Aspiration';
        $page_description = 'Individual Aspiration page description goes here... (jika ada)';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Aspiration'],
            ['page' => '#', 'title' => $page_title ]
        ];
        $breadcrumbs = [
            'page_title' => $page_title,
            'page_description' => $page_description,
            'page_breadcrumbs' => $page_breadcrumbs
        ];
        $nik = Sentinel::getUser()->nik;
        $list_wizard = rule_aspiration($nik, 'wizard', 'individual');
        $check_data = empty(Sentinel::getUser()->employee->positions);
        if ($this->show_engage) {
            return view('aspirations.show_engage', compact('page_title', 'page_description', 'page_breadcrumbs'));
        }
        if(!$list_wizard) {
            $access = false;
            return view('aspirations.show_engage', compact('page_title', 'page_description', 'page_breadcrumbs', 'access'));
        }
        $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first()->id;
        $query_person_grade = Employee::with('positions')->where('prev_persno', $nik)->first();
        $person_grade = (is_null($query_person_grade->person_grade_align)) ? $query_person_grade->positions->grade_align_min : $query_person_grade->person_grade_align;
        $list_job_family_employee = $this->list_job_family_employee($nik);

        if (is_null($query_person_grade->person_grade_align)) {
            echo "<script>console.log('person grade align is null')</script>";
        }
        $bobot = EQS::get();
        
        // * Kondisi untuk internal employee, untuk memunculkan exlude position di grading_position function
        $isInternalEmployee = false;
        if (strtolower($query_person_grade->status_karyawan) != 'external') {
            $isInternalEmployee = true;
        }

        $positions = $this->grading_positions($person_grade, $query_person_grade->positions->code_job_family, $list_job_family_employee, $isInternalEmployee);
        $current_target_positions = TalentAspiration::where('id_event', $event_id)
            ->where('nik', $nik)
            ->with(['positions' => function($query) use($person_grade) {
                return $query->selectRaw("
                    *, 
                    IF (grade_align_min = $person_grade, 1, 0) as is_horizontal,
                    IF (grade_align_min > $person_grade, 1, 0) as is_vertical,
                    $person_grade as person_grade
                ")
                ->join("master_area", "master_area.code_area", "=", "master_position.area")
                ->join("master_subholding", "master_subholding.code_subholding", "=", "master_position.code_subholding");
            }])
            ->where('aspiration_type', 'individual')
            ->get()->map(function($value) use($bobot){
                $parameter = [
                    'employee' => [$value->nik],
                    'position' => $value->code_position
                ];
                $value->eqs_final = get_eqs_default_employees($parameter);
                return $value;
            });

        return view('aspirations.individual.index', compact(
            'page_title', 
            'page_description',
            'page_breadcrumbs', 
            'positions', 
            'current_target_positions',
            'list_wizard',
            'nik',
            'person_grade'
        ));
    }

    public function aspiration_job_holder(){
        $page_title = 'Job Holder Aspiration ';
        $page_description = 'Job holder Aspiration page description goes here... (jika ada)';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Aspiration'],
            ['page' => '#', 'title' => $page_title ]
        ];
        $breadcrumbs = [
            'page_title' => $page_title,
            'page_description' => $page_description,
            'page_breadcrumbs' => $page_breadcrumbs
        ];
        $nik = Sentinel::getUser()->nik;
        $list_wizard = rule_aspiration($nik, 'wizard', 'job_holder');
        $code_job_family[] = Sentinel::getUser()->employee->positions->code_job_family;
        $code_job_family[] = Sentinel::getUser()->employee->positions->code_job_family_2;
        $code_job_family[] = Sentinel::getUser()->employee->positions->code_job_family_3;
        $code_job_family = array_filter($code_job_family);
        $check_data = !empty($code_job_family);
        $validation = $this->check_validation($breadcrumbs, $this->show_engage, !is_bool($list_wizard), $check_data);
        if (!is_bool($validation)) {
            return $validation;
        }
        $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first()->id;
        // $year_end = date("Y");
        $year_end = 2020;
        $year_start = $year_end - 2;
        $grade_employee = Sentinel::getUser()->employee;
        $grade_employee = (is_null($grade_employee->person_grade_align)) ? $grade_employee->positions->grade_align_min : $grade_employee->person_grade_align;
        $target_position = Sentinel::getUser()->employee->position;

        $data = Employee::with(['performance' => function($query) use($year_end, $year_start) {
                $query->whereBetween('year', [$year_start, $year_end]);
            }])
            ->with(['talent_aspiration' => function($query) use($event_id, $target_position) {
                $query->where('id_event', $event_id)
                    ->where('code_position', $target_position)
                    ->where('aspiration_type', 'job_holder')
                    ->with('positions');
            }])
            ->with(['positions' => function($query) {
                $query->with('master_area', 'sub_holding');
            }])
            ->whereDoesntHave('hukdis', function($query) {
                $query->whereDate('start_date', "<=", date("Y-m-d"))
                    ->whereDate('end_date', ">=", date("Y-m-d"))
                    ->limit(1)
                    ->orderBy('id', 'DESC');
            })
            ->where('status_karyawan', "=", "Tetap")
            ->join('master_position', DB::raw('employees.position collate utf8mb4_general_ci'), '=', 'master_position.code_position')
            ->where('prev_persno', '!=', $nik)
            ->where(function($query) use($code_job_family) {
                $query->whereIn('master_position.code_job_family', $code_job_family)
                    ->orWhereIn('master_position.code_job_family_2', $code_job_family)
                    ->orWhereIn('master_position.code_job_family_3', $code_job_family);
            })
            ->whereRaw("person_grade_align >= $grade_employee AND $grade_employee <= ($grade_employee+2)")
            ->get();
        
        $check_data = count($data) > 0;
        $validation = $this->check_validation($breadcrumbs, $this->show_engage, !is_bool($list_wizard), $check_data);
        if (!is_bool($validation)) {
            return $validation;
        }
        $position = Sentinel::getUser()->employee->positions->name_position;

        $sub_event = null;
        return view('aspirations.job_holder.index', compact(
            'page_title', 
            'page_description',
            'page_breadcrumbs', 
            'data',
            'position',
            'list_wizard',
            'sub_event'
        ));
    }

    public function aspiration_supervisor(){
        $page_title = 'Supervisor Aspiration';
        $page_description = 'Supervisor Aspiration page description goes here... (jika ada)';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Aspiration'],
            ['page' => '#', 'title' => $page_title ]
        ];
        if ($this->show_engage) {
            return view('aspirations.show_engage', compact('page_title', 'page_description', 'page_breadcrumbs'));
        }
        $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first()->id;
        $nik = Sentinel::getUser()->nik;
    
        // $year_end = date("Y");
        $year_end = 2020;
        $year_start = $year_end - 2; 
        $list_wizard = rule_aspiration($nik, 'wizard', 'supervisor');
        if(!$list_wizard) {
            $access = false;
            return view('aspirations.show_engage', compact('page_title', 'page_description', 'page_breadcrumbs', 'access'));
        }
        $line_manager = Employee::where('prev_persno', $nik)->with('positions')->first();
        $data = Employee::where('line_manager', $nik)
            ->with(['positions' => function ($query) {
                $query->with('master_area', 'sub_holding');
            }])
            ->with(['performance' => function($query) use($year_end, $year_start) {
                $query->whereBetween('year', [$year_start, $year_end]);
            }])
            ->with(['talent_aspiration' => function($query) use($event_id) {
                $query->with(['positions' => function($subquery) {
                        return $subquery->join("master_area", "master_area.code_area", "=", "master_position.area")
                            ->join("master_subholding", "master_subholding.code_subholding", "=", "master_position.code_subholding");
                    }])->where('id_event', $event_id)->where('aspiration_type', 'supervisor');
            }])
            ->whereNotNull('position')
            ->whereDoesntHave('hukdis', function($query) {
                $query->whereDate('start_date', "<=", date("Y-m-d"))
                    ->whereDate('end_date', ">=", date("Y-m-d"))
                    ->limit(1)
                    ->orderBy('id', 'DESC');
            })
            ->where('status_karyawan', "=", "Tetap")
            ->get()->transform(function($value, $key) {
                // Init
                $person_grade = (is_null($value->person_grade_align)) ? $value->positions->grade_align_min : $value->person_grade_align;
                $talent_aspiration_employee = $value->talent_aspiration;

                if (!empty($talent_aspiration_employee)) {
                    $talent_aspiration_employee->transform(function($value, $key) use($person_grade) {
                        $value->positions->is_horizontal = ($value->positions->grade_align_min == $person_grade) ? 1 : 0;
                        $value->positions->is_vertical = ($value->positions->grade_align_min > $person_grade) ? 1 : 0;
                        return $value;
                    });
                }
                return $value;
            });

        $position = Sentinel::getUser()->employee->positions->name_position;
        $sub_event = null;
        
        return view('aspirations.supervisor.index', compact(
            'page_title', 
            'page_description',
            'page_breadcrumbs', 
            'data', 
            'nik', 
            'position',
            'line_manager',
            'list_wizard',
            'sub_event'
        ));
    }

    public function aspiration_unit(){
        $page_title = 'Unit';
        $page_description = 'Unit Aspiration page description goes here... (jika ada)';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Aspiration'],
            ['page' => '#', 'title' => $page_title ]
        ];
        if ($this->show_engage) {
            return view('aspirations.show_engage', compact('page_title', 'page_description', 'page_breadcrumbs'));
        }
        $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first()->id;
        // $year_end = date("Y");
        $year_end = 2020;
        $year_start = $year_end - 2; 
        $nik = Sentinel::getUser()->nik;
        $code_division = Sentinel::getUser()->employee->positions->code_division;

        $list_wizard = rule_aspiration($nik, 'wizard', 'unit');
        if(!$list_wizard) {
            $access = false;
            return view('aspirations.show_engage', compact('page_title', 'page_description', 'page_breadcrumbs', 'access'));
        }
        $data = Position::where('code_division', $code_division)
        ->with(['employees' => function($query) use($year_end, $year_start, $event_id) {
            $query->with(['performance' => function($subquery) use($year_end, $year_start) {
                $subquery->whereBetween('year', [$year_start, $year_end]);
            }])->with(['talent_aspiration' => function($query) use($event_id) {
                $query->with(['positions' => function($query) {
                    return $query->join("master_area", "master_area.code_area", "=", "master_position.area")
                        ->join("master_subholding", "master_subholding.code_subholding", "=", "master_position.code_subholding");
                    }])->where('id_event', $event_id)->where('aspiration_type', 'unit');
            }])->whereDoesntHave('hukdis', function($query) {
                $query->whereDate('start_date', "<=", date("Y-m-d"))
                    ->whereDate('end_date', ">=", date("Y-m-d"))
                    ->limit(1)
                    ->orderBy('id', 'DESC');
            });
        }])
        ->get()->transform(function($value, $key) {
            // Init
            $value->employees->transform(function($value, $key) {
                $person_grade = (is_null($value->person_grade_align)) ? $value->positions->grade_align_min : $value->person_grade_align;
                $talent_aspiration_employee = $value->talent_aspiration;
    
                if (!empty($talent_aspiration_employee)) {
                    $talent_aspiration_employee->transform(function($value, $key) use($person_grade) {
                        $value->positions->is_horizontal = ($value->positions->grade_align_min == $person_grade) ? 1 : 0;
                        $value->positions->is_vertical = ($value->positions->grade_align_min > $person_grade) ? 1 : 0;
                        return $value;
                    });
                }
                return $value;
            });
            return $value;
        })->filter(function($value, $key) {
            return ($value->employees->count() > 0);
        });

        return view('aspirations.unit.index', compact(
            'page_title', 
            'page_description',
            'page_breadcrumbs', 
            'data',
            'list_wizard'
        ));
    }

    public function generate_target_positions(Request $request) {
        $nik = $request->nik ?? false;
        if (!$nik) 
            return response()->json("Data Is Null", 200);
        $query_person_grade = Employee::with('positions')->where('prev_persno', $nik)->first();
        $person_grade = (is_null($query_person_grade->person_grade_align)) ? $query_person_grade->positions->grade_align_min : $query_person_grade->person_grade_align;
        $list_job_family_employee = $this->list_job_family_employee($nik);
        $grading_position = $this->grading_positions($person_grade, $query_person_grade->positions->code_job_family, $list_job_family_employee);
        if (is_null($query_person_grade->person_grade_align)) {
            $grading_position = (object) array_merge((array) $grading_position, ['note' => 'person grade align is null']);
        }
        return response()->json($grading_position, 200);
    }

    // Start Function
    
    public function insert_individual(Request $request){
        try {
            // Init
            $nik = Sentinel::getUser()->nik;
            $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first()->id;

            foreach ($request->target_positions as $target_position) {
                $data[] = [
                    'id_event' => $event_id,
                    'code_position' => $target_position,
                    'nik' => $nik,
                    'aspiration_type' => 'individual'
                ];
            }
        
            TalentAspiration::where('id_event', $event_id)
                ->where('nik', $nik)
                ->where('aspiration_type', 'individual')->delete();
            $query_response = TalentAspiration::insert($data);
            
            $code = 200;
            $response['saved'] = $query_response ?? true;
        } catch (\Illuminate\Database\QueryException $ex) {
            // $message = $ex->getMessage();
            $code  = 500;
            $response['saved'] = false;
        }

        return response()->json($response, $code);
    }

    public function insert_supervisor(Request $request) {
        try {
            $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first()->id;

            $data = call_user_func_array('array_merge', array_map(function($nik) use($request, $event_id) {
                return array_map(function($target) use($event_id, $nik) {
                    return [ 
                        'id_event' => $event_id,
                        'code_position' => $target,
                        'nik' => $nik,
                        'aspiration_type' => 'supervisor'
                    ];
                }, $request->target_positions_employee[$nik]);
            }, array_keys($request->target_positions_employee)));
            
            TalentAspiration::where('id_event', $event_id)
                ->whereIn('nik', $request->employees)
                ->where('aspiration_type', 'supervisor')->delete();
            if (!empty($data))
                $query_response = TalentAspiration::insert($data);
            $code = 200;
            $response['saved'] = $query_response;
        } catch (\Throwable $th) {
            // $message = $ex->getMessage();
            $code  = 500;
            $response['saved'] = false;
        }

        return response()->json($response, $code);    
    }

    public function insert_job_holder(Request $request) {
        try {
            $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first()->id;
            $user_position = Sentinel::getUser()->employee->position;

            $data = array_map(function($target_employee) use($event_id, $user_position) {
                return [
                    'id_event' => $event_id,
                    'code_position' => $user_position,
                    'nik' => $target_employee,
                    'aspiration_type' => 'job_holder'
                ];
            }, (array) $request->selected_employee);
            
            TalentAspiration::where('id_event', $event_id)
                ->whereIn('nik', $request->employees)->where('code_position', $user_position)
                ->where('aspiration_type', 'job_holder')->delete();

            $query_response = TalentAspiration::insert($data);
            $code = 200;
            $response['saved'] = $query_response;
        } catch (\Throwable $th) {
            // $message = $ex->getMessage();
            $code  = 500;
            $response['saved'] = false;
        }

        return response()->json($response, $code); 
    }

    public function insert_unit(Request $request) {
        try {
            $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first()->id;
            $data = call_user_func_array('array_merge', array_map(function($nik) use($request, $event_id) {
                return array_map(function($target) use($event_id, $nik) {
                    return [ 
                        'id_event' => $event_id,
                        'code_position' => $target,
                        'nik' => $nik,
                        'aspiration_type' => 'unit'
                    ];
                }, $request->target_positions_employee[$nik]);
            }, array_keys($request->target_positions_employee)));

            TalentAspiration::where('id_event', $event_id)
                ->whereIn('nik', $request->employees)
                ->where('aspiration_type', 'unit')->delete();
            if (!empty($data))
                $query_response = TalentAspiration::insert($data);
            $code = 200;
            $response['saved'] = $query_response;
        } catch (\Throwable $th) {
            // $message = $ex->getMessage();
            $code  = 500;
            $response['saved'] = false;
        }

        return response()->json($response, $code); 
    }

    public function gap_competency(Request $request) {
        $query_required_competency = DB::table('master_competency_required')
            ->select('*', DB::raw('LOWER(name_competency) as lower_name_competency'))->get();
        $required_competencies = $query_required_competency->pluck('lower_name_competency');
        $employee = Employee::with(['assessment_competencies' => function($query) use($required_competencies) {
                $query->whereIn(DB::raw('LOWER(soft_competency_align)'), $required_competencies)
                    ->where('competency_clasification', '=', 'Kompetensi BUMN')
                    ->orWhere('competency_clasification', '=', '1')
                    ->where('berlaku_dari', ">=", DB::raw(date("Y-m-d")))
                    ->whereRaw('NOW() <= berlaku_hingga');
                }])
            ->with('positions')
            ->where('prev_persno', '=', $request->nik)
            ->first();

        $org_level = DB::table('master_position')->where('code_position', $request->target_position)->first()->org_level;
        if ($org_level == "BOD") {
            $required = "required_for_bod";
        }else {
            $required = "required_for_bod_1";
        }

        // 
        $parameter = [
            'employee' => [$request->nik],
            'position' => $request->target_position
        ];

        $eqs_final = get_eqs_default_employees($parameter);

        $data = $query_required_competency->map(function($value) use($required, $employee, $eqs_final) {
            $find = $employee->assessment_competencies->filter(function($item) use($value) {
                return trim(strtolower($item->soft_competency_align)) == trim(strtolower($value->name_competency));
            })->first();
            $current = (!$find) ? 0 : (is_null($find->result) ? 0 : $find->result);
            $gap = $current - $value->{$required} ;
            $rename_competency = implode(" ", array_map(function($char) {
                return ucfirst($char);
            }, explode(" ", $value->name_competency)));
            return (object) [
                'name_competency' => $rename_competency,
                'current' => $current,
                "required" => (int) $value->{$required},
                "gap" => ($gap > 0) ? "+".$gap : $gap,
                "eqs" => $eqs_final
            ];
        });
        return response()->json($data, 200);
    }

    private function grading_positions($person_grade, $code_job_family, $rekomendasi_by_job_family, $isInternalEmployee = false) {
        $excludePosition = array(
            'PS-00241',
            'PS-00247',
            'PS-00249',
            'PS-00250'
        );
        $target_job_family = array_unique(array_merge([$code_job_family], $rekomendasi_by_job_family));
        $target_job_family = array_map(function($value) {
            return "'$value'";
        }, $target_job_family);
        $target_job_family = implode(", ", $target_job_family);
        $raw_eligible_position = DB::table('master_position')
        ->selectRaw("
            *, 
            IF (grade_align_min = $person_grade, 1, 0) as is_horizontal,
            IF (grade_align_min > $person_grade, 1, 0) as is_vertical,
            $person_grade as person_grade
        ")
        ->join("master_area", "master_area.code_area", "=", "master_position.area")
        ->join("master_subholding", "master_subholding.code_subholding", "=", "master_position.code_subholding")
        ->whereRaw("
            grade_align_min IS NOT NULL AND
            (CASE 
                WHEN CAST(grade_align_min as UNSIGNED) != 0 THEN (grade_align_min >= $person_grade AND grade_align_min <= ($person_grade+2))
            END) AND
            org_level IN ('BOD', 'BOD-1') AND
            master_position.code_position_type != 'TP-00002'
        ")
        ->when(!is_null($code_job_family), function($query) use($code_job_family, $target_job_family) {
            $query->whereRaw("
                master_position.code_job_family IN (
                    SELECT 
                        code_job_family_to
                    FROM 
                        master_risk_movement 
                    WHERE
                        code_job_family_from = '$code_job_family' AND
                        code_job_family_to IN ($target_job_family)
                    GROUP BY
                        master_risk_movement.code_job_family_to
                )
            ");
        })
        ->when($isInternalEmployee, function($query) use ($excludePosition) {
            $query->whereNotIn("master_position.code_position", $excludePosition);
        })
        ->orderBy("master_area.name_area", "ASC") 
        ->get()->toArray();
        
        $vertical_position = array_map(function($value) {
            return ($value->is_vertical) ? (object) $value : null;
        }, $raw_eligible_position);

        $horizontal_position = array_map(function($value) {
            return ($value->is_horizontal) ? (object) $value : null;
        }, $raw_eligible_position);

        return (object) array_merge(
            ['vertical' => array_values(array_filter($vertical_position))],
            ['horizontal' => array_values(array_filter($horizontal_position))],
        );
    }

    public function modal_eqs_dasar($nik) {
        // -1 karena tidak menggunakan event
        $data = data_career_card($nik, -1);
        return view('talent-profiling.element.career_card_plain', $data);
    }

    public function list_job_family_employee($nik) {
        $query_position_history = DB::table('master_position_history')
            ->where('nik', $nik)
            ->get();
            
        $allitems = new Collection();
        $code_job_family_align = [
            'code_job_family_align',
            'code_job_family_align_2',  
            'code_job_family_align_3'
        ];
        foreach ($code_job_family_align as $label) {
            $jf = $query_position_history->groupBy($label)->map(function($jf) {
                $list_start_menjabat = $jf->pluck('start_date');
                $list_akhir_menjabat = $jf->pluck('end_date');
                $tahun_awal = date("Y", strtotime($list_start_menjabat->first()));
                $condition_akhir_menjabat = (is_null($list_akhir_menjabat->last()) || $list_akhir_menjabat->last() == '0000-00-00') ? date("Y-m-d") : $list_akhir_menjabat->last();
                $tahun_akhir = date("Y", strtotime($condition_akhir_menjabat));
                return [
                    'higher_3year' => ($tahun_akhir - $tahun_awal >= 3) ? 1 : 0
                ];
            });
            $allitems = $allitems->mergeRecursive($jf);
        }

        $allitems = $allitems->forget("")->mapWithKeys(function($value, $key) {
            return [$key => ['higher_3year' => is_array($value['higher_3year']) ? array_sum($value['higher_3year']) : $value['higher_3year']]];
        })->where('higher_3year', '!=', '0')->toArray();
        return array_keys($allitems);
    }

    private function check_validation($breadcrumbs, $engage, $access=false, $data_lengkap=false) {
        $data_external = [
            'access' => $access,
            'data_lengkap' => $data_lengkap,
            'engage' => $engage
        ];
        if ($engage || !$access || !$data_lengkap) {
            return view('aspirations.show_engage', array_merge($breadcrumbs, $data_external));
        }
        return true;
    }
}
