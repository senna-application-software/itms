<?php

namespace App\Http\Controllers\PendingTask;

use App\Http\Controllers\Controller;
use App\Jobs\SendBulkQueueEmail;
use App\Models\Employee;
use App\Models\EventWindowTimeModel;
use App\Models\PendingTaskNotification;
use App\Models\SubEvent;
use App\Models\Talent;
use App\Models\TalentAspiration;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Svg\Tag\Rect;
use Yajra\DataTables\Facades\DataTables;

class PendingTaskController extends Controller
{

    private $event_active = 0;
    private $event_name = "";
    private $event_start = [];
    private $event_end = [];
    private $event_position_eligible = [];
    private $event_employee_eligible = [];

    public function __construct()
    {
        $raw = EventWindowTimeModel::where('status', 'open')->limit(1)->select('id', 'name', 'start_date', 'end_date')->first();
        if ($raw) {
            $this->event_active = $raw->id;
            $this->event_name = $raw->name;
            $this->event_start = date('j M', strtotime($raw->start_date));
            $this->event_end = date('j M', strtotime($raw->end_date));
        }
    }

    public function index()
    {
        $page_title = 'Pending Task';
        $page_description = '';
        $page_breadcrumbs = [];

        $pending_task_position = PendingTaskNotification::where('jenis_notifikasi', 'position')->where('status', 'pending')->count();
        $pending_task_employee = PendingTaskNotification::where('jenis_notifikasi', 'employee')->where('status', 'pending')->count();

        return view('pending-task.index', compact(
            'page_title',
            'page_description',
            'pending_task_position',
            'pending_task_employee',
            'page_breadcrumbs'
        ));
    }

    public function dataNotifPosition()
    {
        $data = PendingTaskNotification::where('jenis_notifikasi', 'position')
            ->orderBy('status', 'asc')
            ->orderBy('created_at', 'desc')
            ->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('position', function ($data) {
                return '[' . $data->position_event->sub_holding->name_subholding . '] ' . $data->position_event->name_position;
            })
            ->editColumn('job_holder', function ($data) {
                return $data->job_holder_position->personnel_number;
            })
            ->editColumn('tgl_kosong', function ($data) {
                return date_db_formatter($data->tgl_kosong);
            })
            ->editColumn('status', function ($data) {
                return $this->status_task($data->status);
            })
            ->addColumn('button', function ($data) {
                $btn = '-';
                if ($data->status  == 'pending') {
                    $btn = '<button class="btn btn-sm btn-primary" onclick="createEventPosition(this)" id="' . $data->id . '" data-position_code="' . $data->position . '">Create Event</button>';
                }
                return $btn;
            })
            ->rawColumns(['button', 'status'])
            ->make(true);
    }

    public function dataNotifEmployee()
    {
        $data = PendingTaskNotification::where('jenis_notifikasi', 'employee')
            ->orderBy('status', 'desc')
            ->orderBy('created_at', 'desc')
            ->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('employee_id', function ($data) {
                return $data->employee->personnel_number;
            })
            ->editColumn('prev_position', function ($data) {
                return $data->prev_positions->name_position;
            })
            ->addColumn('aspiration', function ($data) {
                $position_by_aspiration = TalentAspiration::where('nik', $data->employee->prev_persno)->where('id_event', $this->event_active)->get();
                $asp =  '';
                if(count($position_by_aspiration) == 0 ){
                    $asp = '-';
                } 
                foreach ($position_by_aspiration as $key => $item) {
                    $asp .= '[' . $item->positions->sub_holding->name_subholding . '] ' . $item->positions->name_position . '<br>';
                }
                return $asp;
            })
            ->addColumn('candidates', function ($data) {
                return '';
            })
            ->editColumn('status', function ($data) {
                return $this->status_task($data->status);
            })
            ->addColumn('button', function ($data) {
                $btn = '-';
                if ($data->status  == 'pending') {
                    $btn = '<button class="btn btn-sm btn-primary" onclick="createEventEmployee(this)" id="' . $data->id . '" data-position_code="' . $data->position . '">Create Event</button>';
                }
                return $btn;
            })
            ->rawColumns(['button', 'status','aspiration'])
            ->make(true);
    }

    public function badge()
    {
        $data = PendingTaskNotification::where('status', 'pending')->count();
        return response()->json($data);
    }

    public function addPosition(Request $request)
    {
        $pending_task = PendingTaskNotification::find($request->id);
        $data['pending_task'] = $pending_task;
        return view('pending-task.part_of.modal_add_eposition', $data);
    }

    public function addEmployee(Request $request)
    {
        $pending_task = PendingTaskNotification::find($request->id);
        $position_by_aspiration = TalentAspiration::where('nik', $pending_task->employee->prev_persno)->where('id_event', $this->event_active)->get();
        $aspiration_type = TalentAspiration::where('nik', $pending_task->employee->prev_persno)->where('id_event', $this->event_active)->groupBy('aspiration_type')->get();
        $sub_event_position = [];
        $sub_event = SubEvent::where('id_event', $this->event_active)->get();
        
        if(count($position_by_aspiration) < 2){
            $nik = $pending_task->employee->prev_persno;
            $query_person_grade = Employee::with('positions')->where('prev_persno', $nik)->first();
            $person_grade = (is_null($query_person_grade->person_grade_align)) ? $query_person_grade->positions->grade_align_min : $query_person_grade->person_grade_align;
            $list_job_family_employee = $this->list_job_family_employee($nik);
            $positions_by_job_family = $this->grading_positions($person_grade, $query_person_grade->positions->code_job_family, $list_job_family_employee);
            $data['position_by_job_family'] = $positions_by_job_family;
        }
       

        foreach ($sub_event as $item) {
            $sub_event_position[] = $item->position;
        }

        $data['aspiration_type'] = $aspiration_type;
        $data['positions'] = $position_by_aspiration;
        $data['pending_task'] = $pending_task;
        $data['sub_event_position'] = $sub_event_position;
        return view('pending-task.part_of.modal_add_eemployee', $data);
    }

    private function grading_positions($person_grade, $code_job_family, $rekomendasi_by_job_family) {
        $target_job_family = array_unique(array_merge([$code_job_family], $rekomendasi_by_job_family));
        $target_job_family = array_map(function($value) {
            return "'$value'";
        }, $target_job_family);
        $target_job_family = implode(", ", $target_job_family);
        $raw_eligible_position = DB::table('master_position')
        ->selectRaw("
            *, 
            IF (grade_align_min = $person_grade, 1, 0) as is_horizontal,
            IF (grade_align_min > $person_grade, 1, 0) as is_vertical,
            $person_grade as person_grade
        ")
        ->join("master_area", "master_area.code_area", "=", "master_position.area")
        ->join("master_subholding", "master_subholding.code_subholding", "=", "master_position.code_subholding")
        ->whereRaw("
            grade_align_min IS NOT NULL AND
            (CASE 
                WHEN CAST(grade_align_min as UNSIGNED) != 0 THEN (grade_align_min >= $person_grade AND grade_align_min <= ($person_grade+2))
            END) AND
            org_level IN ('BOD', 'BOD-1') AND
            master_position.code_position_type != 'TP-00002'
        ")
        ->when(!is_null($code_job_family), function($query) use($code_job_family, $target_job_family) {
            $query->whereRaw("
                master_position.code_job_family IN (
                    SELECT 
                        code_job_family_to
                    FROM 
                        master_risk_movement 
                    WHERE
                        code_job_family_from = '$code_job_family' AND
                        code_job_family_to IN ($target_job_family)
                    GROUP BY
                        master_risk_movement.code_job_family_to
                )
            ");
        })
        ->orderBy("master_area.name_area", "ASC") 
        ->get()->toArray();
        
        $vertical_position = array_map(function($value) {
            return ($value->is_vertical) ? (object) $value : null;
        }, $raw_eligible_position);

        $horizontal_position = array_map(function($value) {
            return ($value->is_horizontal) ? (object) $value : null;
        }, $raw_eligible_position);

        return (object) array_merge(
            ['vertical' => array_values(array_filter($vertical_position))],
            ['horizontal' => array_values(array_filter($horizontal_position))],
        );
    }

    public function list_job_family_employee($nik) {
        $query_position_history = DB::table('master_position_history')
            ->where('nik', $nik)
            ->get();
            
        $allitems = new Collection();
        $code_job_family_align = [
            'code_job_family_align',
            'code_job_family_align_2',  
            'code_job_family_align_3'
        ];
        foreach ($code_job_family_align as $label) {
            $jf = $query_position_history->groupBy($label)->map(function($jf) {
                $list_start_menjabat = $jf->pluck('start_date');
                $list_akhir_menjabat = $jf->pluck('end_date');
                $tahun_awal = date("Y", strtotime($list_start_menjabat->first()));
                $condition_akhir_menjabat = (is_null($list_akhir_menjabat->last()) || $list_akhir_menjabat->last() == '0000-00-00') ? date("Y-m-d") : $list_akhir_menjabat->last();
                $tahun_akhir = date("Y", strtotime($condition_akhir_menjabat));
                return [
                    'higher_3year' => ($tahun_akhir - $tahun_awal >= 3) ? 1 : 0
                ];
            });
            $allitems = $allitems->mergeRecursive($jf);
        }

        $allitems = $allitems->forget("")->mapWithKeys(function($value, $key) {
            return [$key => ['higher_3year' => is_array($value['higher_3year']) ? array_sum($value['higher_3year']) : $value['higher_3year']]];
        })->where('higher_3year', '!=', '0')->toArray();
        return array_keys($allitems);
    }

    public function storePosition(Request $request)
    {
        // dd($request->all());

        if ($request->position == null) {
            return redirect()->back()->with([
                "status" => "error",
                "title" => "Error",
                "message" => "Data Posisi tidak boleh kosong"
            ]);
        }

        $ewt = EventWindowTimeModel::whereIn('status', ['open'])->select('id', 'start_date', 'end_date')->first();
        $id_event = SubEvent::insertGetId([
            'id_event'      => $ewt->id,
            'position'      => $request->position,
            'name'          => $request->event_name,
            'status'        => 'sourcing',
            'vacant_type'   => 'vacant',
            'sourcing_type' => $request->sourcing_type
        ]);

        // Update Status Notif
        $pending_task = PendingTaskNotification::find($request->id_notif);
        $pending_task->status = 'on_process';
        $pending_task->id_sub_event = $id_event;
        $pending_task->save();

        // Insert Eqs Dasar
        $eligible_employee = TalentAspiration::where('code_position', $request->position)->groupBy('nik')
            ->where('id_event', $ewt->id)->get()->pluck('nik')->toArray();
        $load_eqs = [
            'employee' => $eligible_employee,
            'position' => $request->position
        ];
        $eqs_employees = kalkulasi_eqs($load_eqs, false);
        array_walk($eqs_employees, function (&$value, $key) use ($id_event) {
            $value["sub_event"] = $id_event;
            return $value;
        });
        DB::table('employee_eqs')->insert($eqs_employees);

        $position_qry = DB::select("select master_position.name_position, master_subholding.name_subholding from master_position
            left join master_subholding on master_subholding.code_subholding = master_position.code_subholding
            where master_position.code_position='" . $request->position . "'
        ");

        $kandidat_nama_jabatan = "";
        $kandidat_nama_subholding = "";

        if ($position_qry) {
            $kandidat_nama_jabatan = $position_qry[0]->name_position;
            $kandidat_nama_subholding = $position_qry[0]->name_subholding;
        }

        $periode_pengisian = date('d-m-Y', strtotime($ewt->start_date)) . ' s.d ' . date('d-m-Y', strtotime($ewt->end_date));
        $talent_data = TalentAspiration::join('employees', DB::raw('employees.prev_persno collate utf8mb4_unicode_ci'), '=', 'talent_aspiration.nik')
            ->where('code_position', $request->position)
            ->select(
                'nik',
                'email',
                'gender_text',
                'personnel_number',
                'prev_persno',
            )
            ->groupBy('nik')
            ->get();

        $data = [];
        foreach ($talent_data as $value) {
            $data[] = [
                'nik' => $value['nik'],
                'emails' => $value['email'],
                'type' => 'update_profile',
                'prefix' => genderPrefix($value['gender_text']),
                'sub_prefix' => genderSubprefix($value['gender_text']),
                'nama' => $value['personnel_number'],
                'username' => $value['prev_persno'],
                'password' => 'itms2022',
                'periode_update' => $periode_pengisian,
                'kandidat_nama_jabatan' => $kandidat_nama_jabatan,
                'kandidat_nama_subholding' => $kandidat_nama_subholding,
                'url' => url('my-profile')
            ];
        }

        if (count($data) > 0) {
            // send all mail in the queue.
            $job = (new SendBulkQueueEmail($data))->delay(now()->addSeconds(2));
            dispatch($job);

            $msg = "Bulk mail send successfully in the background...";
        } else {
            $msg = "Tidak ada perseta yang eligible. Tidak ada email yang akan dikirim untung mengundang peserta.";
        }

        return redirect()->back()->with([
            "status" => "success",
            "title" => "Berhasil",
            "message" => "Data sub event telah disimpan di database " . $msg
        ]);
    }
    public function storeEmployee(Request $request)
    {
        if ($request->position == null) {
            return redirect()->back()->with([
                "status" => "error",
                "title" => "Error",
                "message" => "Data Posisi tidak boleh kosong"
            ]);
        }

        $nik = PendingTaskNotification::find($request->id_notif)->employee->prev_persno;
        $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first()->id;

        $sub_event = SubEvent::where('id_event',$this->event_active)->where('position',$request->position);
       
        if($sub_event->count() > 0){
            $data_talent['nik'] = $nik;
            $data_talent['sub_event'] = $sub_event->first()->id;
            $data_talent['status'] = 'selected'; 
            Talent::insert($data_talent);
            return redirect()->back()->with([
                "status" => "success",
                "title" => "Berhasil",
                "message" => "Employee telah di masukan kedalam event"
            ]);
        }

        $data[] = [
            'id_event' => $event_id,
            'code_position' => $request->position,
            'nik' => $nik,
            'aspiration_type' => 'individual',
            'comitte_proposal' => '1',
        ];       
    
        TalentAspiration::where('id_event', $event_id)
            ->where('nik', $nik)
            ->where('aspiration_type', 'individual')->delete();
        $query_response = TalentAspiration::insert($data);

        $ewt = EventWindowTimeModel::whereIn('status', ['open'])->select('id', 'start_date', 'end_date')->first();
        $id_event = SubEvent::insertGetId([
            'id_event'      => $ewt->id,
            'position'      => $request->position,
            'name'          => $request->event_name,
            'status'        => 'sourcing',
            'vacant_type'   => 'vacant',
            'sourcing_type' => $request->sourcing_type,
        ]);

        try {
            $cek_pending_task = PendingTaskNotification::where('employee_id', $request->employee_id)->first();
            if ($cek_pending_task != null) {
                $cek_pending_task->status = 'on_process';
                $cek_pending_task->id_sub_event = $id_event;
                $cek_pending_task->save();
            }
        } catch (\Throwable $th) {
            //throw $th;
        }


        // Insert Eqs Dasar
        $eligible_employee = TalentAspiration::where('code_position', $request->position)->groupBy('nik')
            ->where('id_event', $ewt->id)->get()->pluck('nik')->toArray();
        $load_eqs = [
            'employee' => $eligible_employee,
            'position' => $request->position
        ];
        $eqs_employees = kalkulasi_eqs($load_eqs, false);
        array_walk($eqs_employees, function (&$value, $key) use ($id_event) {
            $value["sub_event"] = $id_event;
            return $value;
        });

         // Insert Eligible Employee
        $data_talent = [];
        foreach($eligible_employee as $key => $item){
            $data_talent[$key]['nik'] = $item;
            $data_talent[$key]['sub_event'] = $id_event;
            $data_talent[$key]['status'] = 'selected'; 
        }
        Talent::insert($data_talent);

        $position_qry = DB::select("select master_position.name_position, master_subholding.name_subholding from master_position
            left join master_subholding on master_subholding.code_subholding = master_position.code_subholding
            where master_position.code_position='" . $request->position . "'
        ");

        $kandidat_nama_jabatan = "";
        $kandidat_nama_subholding = "";

        if ($position_qry) {
            $kandidat_nama_jabatan = $position_qry[0]->name_position;
            $kandidat_nama_subholding = $position_qry[0]->name_subholding;
        }

        $periode_pengisian = date('d-m-Y', strtotime($ewt->start_date)) . ' s.d ' . date('d-m-Y', strtotime($ewt->end_date));
        $talent_data = TalentAspiration::join('employees', DB::raw('employees.prev_persno collate utf8mb4_unicode_ci'), '=', 'talent_aspiration.nik')
            ->where('code_position', $request->position)
            ->select(
                'nik',
                'email',
                DB::raw('(CASE WHEN gender_text="Male" THEN "Bapak" WHEN gender_text="Female" THEN "Ibu" ELSE "Saudara" END) prefix'),
                DB::raw('(CASE WHEN gender_text="Male" THEN "Bpk" WHEN gender_text="Female" THEN "Ibu" ELSE "Sdr" END) sub_prefix'),
                'personnel_number',
                'prev_persno',
            )
            ->groupBy('nik')
            ->get();

        $data = [];
        foreach ($talent_data as $value) {
            $data[] = [
                'nik' => $value['nik'],
                'emails' => $value['email'],
                'type' => 'update_profile',
                'prefix' => $value['prefix'],
                'sub_prefix' => $value['sub_prefix'],
                'nama' => $value['personnel_number'],
                'username' => $value['prev_persno'],
                'password' => 'itms2022',
                'periode_update' => $periode_pengisian,
                'kandidat_nama_jabatan' => $kandidat_nama_jabatan,
                'kandidat_nama_subholding' => $kandidat_nama_subholding,
                'url' => url('my-profile')
            ];
        }

        if (count($data) > 0) {
            // send all mail in the queue.
            $job = (new SendBulkQueueEmail($data))->delay(now()->addSeconds(2));
            dispatch($job);

            $msg = "Bulk mail send successfully in the background...";
        } else {
            $msg = "Tidak ada perseta yang eligible. Tidak ada email yang akan dikirim untung mengundang peserta.";
        }

        return redirect()->back()->with([
            "status" => "success",
            "title" => "Berhasil",
            "message" => "Data sub event telah disimpan di database "
        ]);
    }

    public function status_task($status)
    {
        if ($status == 'pending') {
            $label = '<span class="badge badge-danger">Pending</span>';
        } else if ($status == 'on_process') {
            $label = '<span class="badge badge-warning">On Process</span>';
        } else {
            $label = '<span class="badge badge-success">Done</span>';
        }

        return $label;
    }
}
