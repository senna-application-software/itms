<?php

namespace App\Http\Controllers\SearchEmployee;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MyProfile\MyProfileController;
use App\Models\Employee;
use App\Models\MasterJobFamily;
use App\Models\Position;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class SearchEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobFamily = MasterJobFamily::select('name_job_family')->get();

        $subholding          = "";
        $subHoldingCondition = ($subholding != "" && $subholding != null && $subholding != false);
        $rawPositions        = Position::when($subHoldingCondition, function ($query) use ($subholding) {
            $query->where('code_subholding', $subholding);
        })->with('master_area')->with('sub_holding')->get();
        $subholding = $rawPositions->pluck('sub_holding')->pluck('name_subholding', 'code_subholding');

        $grade = Employee::select('person_grade_align')
            ->orderBy('person_grade_align', 'ASC')
            ->whereNotNull('person_grade_align')
            ->get()
            ->unique('person_grade_align');

        $talentCluster   = array();
        $talentCluster[] = [
            'key' => 1,
            'val' => 'Unfit (Talent Cluster 1)',
        ];
        $talentCluster[] = [
            'key' => 2,
            'val' => 'Sleeping Tiger (Talent Cluster 2)',
        ];
        $talentCluster[] = [
            'key' => 3,
            'val' => 'Solid Contributor (Talent Cluster 3)',
        ];
        $talentCluster[] = [
            'key' => 4,
            'val' => 'Sleeping Tiger (Talent Cluster 4)',
        ];
        $talentCluster[] = [
            'key' => 5,
            'val' => 'Promotable (Talent Cluster 5)',
        ];
        $talentCluster[] = [
            'key' => 6,
            'val' => 'Solid Contributor (Talent Cluster 6)',
        ];
        $talentCluster[] = [
            'key' => 7,
            'val' => 'Promotable (Talent Cluster 7)',
        ];
        $talentCluster[] = [
            'key' => 8,
            'val' => 'Promotable (Talent Cluster 8)',
        ];
        $talentCluster[] = [
            'key' => 9,
            'val' => 'High Potential (Talent Cluster 9)',
        ];

        $page_title       = 'Search Employee';
        $page_description = 'Halaman Search Employee';
        $page_breadcrumbs = [
            ['page' => '/dashboard', 'title' => 'Home'],
            ['page' => '#', 'title' => 'Search Employee'],
        ];

        return view('search-employee.employee.index', compact('page_title', 'page_description', 'page_breadcrumbs', 'jobFamily', 'subholding', 'grade', 'talentCluster'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message  = "success";
        $code     = 200;
        $employee = false;

        $employee = Employee::find($id);

        if (!$employee) {
            $message = "failed";
            $code    = 400;
        }

        $data = array(
            'message' => $message,
            'code'    => $code,
            'data'    => $employee,
        );
        return response()->json($data, $code);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code     = 200;
        $message  = 'success';
        $employee = false;

        $employee = Employee::find($id);
        $employee->delete();

        if (!$employee) {
            $code     = 400;
            $message  = 'failed';
            $employee = false;
        }

        $data = array(
            'message' => $message,
            'code'    => $code,
            'data'    => $employee,
        );

        return response()->json($data, $code);
    }

    // * Function untuk get datatable server side master data employee
    public function dataTableEmployee(Request $req)
    {
        $data = query_employees($req);

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                if ($data->prev_persno) {
                    $edit = route('SearchDataEmployee.profile', $data->prev_persno);
                } else {
                    $edit = "#";
                }
                return '<div class="dropdown table-action">
                    <button class="btn btn-light-primary btn-icon btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ki ki-bold-more-ver"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a href="' . $edit . '" class="dropdown-item">
                            View
                        </a>
                    </div>
                </div>';
            })
            ->editColumn('status_karyawan', function ($data) {
                $statusKaryawan = 'Internal';
                $label          = "label-light-primary";

                if (strtolower($data->status_karyawan) == 'external') {
                    $statusKaryawan = 'External';
                    $label          = "label-light-success";
                }

                return '<span class="label font-weight-bold label-lg ' . $label . ' label-inline">' . $statusKaryawan . '</span>';
            })
            // Saya Comment ya kang @awal biar tidak banyak query biar load waktu nya optimal
            // ->editColumn('directorate', function ($data) {
            //     return isset($data->subholding) ? $data->subholding->name_subholding : "";
            // })
            // ->editColumn('area_nomenklatur', function ($data) {
            //     if ($data->area_nomenklatur != null) {
            //         return getArea($data->area_nomenklatur);
            //     } else {
            //         return "-";
            //     }
            // })
            // ->rawColumns(['action', 'status_karyawan', 'directorate', 'talentCluster'])
            ->rawColumns(['action', 'status_karyawan'])
            ->make(true);
    }
    public function profile($nik)
    {
        $data             = MyProfileController::index(true, $nik);
        $page_title       = 'Profile';
        $page_description = 'Halaman Profile';
        $page_breadcrumbs = [
            ['page' => '/dashboard', 'title' => 'Home'],
            ['page' => '#', 'title' => ' Profile Employee'],
        ];

        $data['page_title'] = $page_title;

        $data['page_description'] = $page_description;
        $data['page_breadcrumbs'] = $page_breadcrumbs;
        $data['back']             = !empty(request('back')) ? route('settings.' . request('back')) : url()->previous();

        return view('search-employee.profile', $data);
    }
}
