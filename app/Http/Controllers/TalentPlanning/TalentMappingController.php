<?php

namespace App\Http\Controllers\TalentPlanning;
use App\Models\Ba_event;
use App\Models\Callibration;
use App\Models\CalibrationHistory;
use App\Models\Employee;
use App\Models\SubEvent;
use App\Models\Talent;
use App\Models\TalentDay;
use App\Models\TalentAspiration;
use App\Models\NilaiAssessment;
use App\Models\EventWindowTimeModel;
use App\Jobs\SendBulkQueueEmail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use PDF;

use Sentinel;

class TalentMappingController extends Controller
{
    private $event_active = 0;
    private $event_name = "";
    private $event_start = [];
    private $event_end = [];
    private $event_position_eligible = [];
    private $event_employee_eligible = [];

    public function __construct() {
        $raw = EventWindowTimeModel::where('status', 'open')->limit(1)->select('id', 'name', 'start_date', 'end_date')->first();
        if($raw) {
            $this->event_active = $raw->id;
            $this->event_name = $raw->name;
            $this->event_start = date('j M', strtotime($raw->start_date));
            $this->event_end = date('j M', strtotime($raw->end_date));
        }
    }
    public function index(){
        $level_akses = Session::get('role');

        switch ($level_akses) {
            case 'talent_committee':
                $position = SubEvent::join('event_window_time', 'event_window_time.id', '=', 'sub_event.id_event')
                    ->join('talent_aspiration', 'talent_aspiration.id_event', '=', 'event_window_time.id')
                    ->where('event_window_time.status', 'open')
                    ->where('sub_event.status', '=', 'mapping')
                    ->where('talent_aspiration.nik', Sentinel::getUser()->nik)
                    ->select(
                        'sub_event.sourcing_type',
                        'master_subholding.name_subholding', 
                        'master_area.name_area',
                        DB::raw('(SELECT name_position FROM master_position WHERE master_position.code_position=sub_event.position) AS name_position'),
                        'sub_event.status',
                        'sub_event.id',
                        DB::raw('(SELECT count(distinct(nik)) FROM talent WHERE talent.sub_event=sub_event.id AND status="selected" AND is_profilling=1) AS participant'),
                        DB::raw('(SELECT count(distinct(nik)) FROM talent_aspiration WHERE talent_aspiration.id_event=sub_event.id_event AND talent_aspiration.code_position=sub_event.position) AS total')

                    )
                    ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
                    ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
                    ->join('master_area', 'master_area.code_area', 'master_position.area')
                    ->get();
                break;
            
            default:
                $position = SubEvent::join('event_window_time', 'event_window_time.id', '=', 'sub_event.id_event')
                    ->where('event_window_time.status', 'open')
                    ->where('sub_event.status', '=', 'mapping')
                    ->select(
                        'sub_event.sourcing_type',
                        'master_subholding.name_subholding', 
                        'master_area.name_area',
                        DB::raw('(SELECT name_position FROM master_position WHERE master_position.code_position=sub_event.position) AS name_position'),
                        'sub_event.status',
                        'sub_event.id',
                        DB::raw('(SELECT count(distinct(nik)) FROM talent WHERE talent.sub_event=sub_event.id AND status="selected" AND is_profilling="1") AS participant'),
                        DB::raw('(SELECT count(distinct(nik)) FROM talent_aspiration WHERE talent_aspiration.id_event=sub_event.id_event AND talent_aspiration.code_position=sub_event.position) AS total')
                    )
                    ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
                    ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
                    ->join('master_area', 'master_area.code_area', 'master_position.area')
                    ->get();
                break;
        }

        $page_title = 'Talent Mapping';
        $page_description = 'Membuat event baru';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Talent Mapping']
        ];

        $show_engage = $this->event_active != 0 ? false : true; 

        return view('talent-mapping.index', compact('page_title', 'page_description','page_breadcrumbs', 'position','show_engage'));
    }

    public function detail($id)
    {
        $sub_event  = SubEvent::select('sub_event.id', 'master_position.name_position', 'master_position.code_position',
            'master_subholding.name_subholding', 
            'sub_event.sourcing_type',
            'master_area.name_area'
        )
        ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
        ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
        ->join('master_area', 'master_area.code_area', 'master_position.area')
        ->where('sub_event.id', '=', $id)
        ->first();

        $page_title = 'Clustering';
        $page_description = 'Membuat event baru';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Talent Mapping'],
            ['page' => '#', 'title' =>'Clustering']
        ];

        $boxes = boxProperties($is_generate=true,$box=null); 
        $is_editable = 1;
        $is_talent_map = 1;
        return view('talent-mapping.detail', compact('page_title', 'page_description','sub_event','page_breadcrumbs','boxes', 'id', 'is_editable', 'is_talent_map'));
    }

    public function store(Request $request)
    {
        $id   = $request->id;

        $emp_selected = explode(',', $request->emp_selected);

        $box    = Talent::select('id')
        ->where('sub_event', '=', $id)
        ->whereIn('nik', $emp_selected)
        ->get();
        
        Talent::whereIn('id', $box)
        ->where('sub_event', $request->id)
        ->update([
            'is_mapping' => '1'
        ]);
        
        Talent::whereNotIn('id', $box)
        ->where('sub_event', $request->id)
        ->where('is_profilling', '1')
        ->where('status', 'selected')
        ->update([
            'status' => 'terminated',
            'is_mapping' => '1'
        ]);

        SubEvent::where('id', '=', $request->id)
        ->update([
            'status' => 'talent_day'
        ]);

        //Create Panel Schedule
        $schedule   = TalentDay::create([
            'id_sub_event'  => $request->id,
            'tanggal'       => $request->tanggal,
            'jam'           => $request->waktu,
            'lokasi'        => $request->lokasi,
            'agenda'        => $request->agenda
        ]);
        
        $this->send_email($id);

        return redirect()->route('talent-mapping.index')->with([
            "id_event" => $request->id,
            "status"    => "success", 
            "title"     => "Info",
            "message"   => "Data Talent Mapping has successfully selected, please check your inbox for details"
        ]);
    }

    public function send_email($id_sub_event) {
        $total_talent = Talent::where('sub_event', $id_sub_event)
            ->where('is_mapping', '1')
            ->where('status', 'selected')
            ->with('employee', function($q){
                $q->with("subholding");
            })
            ->get();
        $sub_event  = SubEvent::select('master_position.name_position','master_subholding.name_subholding',  'master_position.code_position', 'sub_event.id')
           
            ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
                    ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
                   
            ->where('sub_event.id', '=', $id_sub_event)
            ->first();
        
        if ($total_talent) {
            $td = DB::table('talent_day')->where('id_sub_event', $id_sub_event)->first();
            $convertdate = hari_ini(date('d-m-Y', strtotime($td->tanggal)));
            $dateemail = $convertdate['nama_hari'].', '.$convertdate['tgl'].'-'. $convertdate['nama_bulan'].'-'. $convertdate['thn'];
    
            if ($td) {
                $tanggal = $dateemail;
                $jam    = $td->jam;
                $lokasi = $td->lokasi;
                $agenda = $td->agenda;
            } else {
                $tanggal = "";
                $jam    = "";
                $lokasi = "";
                $agenda = "";
            }

            $data = [];
            foreach ($total_talent as $key => $v) {
                $data[] = [
                    'type'          => 'talent_day',
                    'hari_tanggal'  => $tanggal,
                    'waktu'         => $jam,
                    'lokasi'        => $lokasi,
                    'agenda'        => $agenda,
                    'jabatan'       => $sub_event->name_position,
                    'sub_holding'   => $sub_event->name_subholding,
                    'prefix'        => genderPrefix($v->employee->gender_text),
                    'sub_prefix'    => genderSubprefix($v->employee->gender_text),
                    'nama'          => $v->employee->personnel_number,
                    'emails'        => $v->employee->email,
                    'url'           => "",
                ];
            }
            if (count($data) > 0) { 
                $job = (new SendBulkQueueEmail($data))->delay(now()->addSeconds(2)); 
                dispatch($job);
            } 

            return redirect()->route('talent-mapping.index')->with([
                "status"    => "success", 
                "title"     => "Info",
                "message"   => "Data Talent Mapping has successfully selected, please check your inbox for details"
            ]);
        }

        return false;
    }

    public function comparison()
    {
        $page_title = 'Talent Mapping';
        $page_description = 'Membuat event baru';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Talent Planing'],
            ['page' => '#', 'title' =>'Talent Mapping']
        ];

        return view('talent-mapping.comparison', compact('page_title', 'page_description','page_breadcrumbs'));
    }

    //method untuk mengeluarkan data employee ke box cluster
    public function get_employee(Request $response)
    {
        $panel      = $response->panel;
        $all_panels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        $arr_view   = [];
        $arr_grade  = [];

        $panels = [1, 2, 3, 4, 5, 6, 7, 8, 9];

        $total_employee = Talent::select('employees.personnel_number', 'employees.prev_persno','talent.*')
            ->join('employees', DB::raw('employees.prev_persno collate utf8mb4_unicode_ci'), '=', 'talent.nik')
            ->join('sub_event', 'sub_event.id', '=', 'talent.sub_event')
            ->whereIn("talent.panel",$panels)
            ->where('sub_event.id', $response->subid)
            ->get();

        foreach ($all_panels as $key => $panel) {
            $data = Talent::select('employees.personnel_number', 'employees.prev_persno','talent.*')
            ->join('employees', DB::raw('employees.prev_persno collate utf8mb4_unicode_ci'), '=', 'talent.nik')
            ->where("talent.panel", $panel)
            ->where('talent.status', 'selected')
            ->where('talent.is_profilling', "1")
            ->where('talent.sub_event', $response->subid)
            ->get();
            
            $arr_view[]         = View::make('talent-mapping/element/panels', compact('data', 'panel'))->render();
            $arr_view_total[]   = View::make('talent-mapping/element/panel_percent', compact('data', 'panel','total_employee'))->render();
        }

        $data = json_encode(array(
            'view'  => $arr_view,
            'total' => $arr_view_total
        ));

        return $data;
    }

    //method update panel
    public function update_panel(Request $request)
    {
        $panel  = $request->panel;
        $id     = Crypt::decryptString($request->id);

        $data_talent  = Talent::where('id', '=', $id)->first();
        $nik = $data_talent->nik;
        $nik_admin = Sentinel::check()->nik;

        $cek_panel  = Talent::select('is_change_box', 'panel')->where('id', '=', $id)->first();

        // * untuk kondisi check panel sama check is_change_box saya hide ya,
        // * karena kebutuhannya untuk sekarang ingin update secara vertikal atau horizontal (Marking By : irp4ndi)
        // if(abs($cek_panel->panel - $panel) != 1){
        //     $data = json_encode(['message'  => 'down']);

        //     return $data;
        // }

        // if($cek_panel->is_change_box == '1'){
        //     $data = json_encode(['message'  => 'is_box']);
        //     return $data;
        // } else {
            $update   = Talent::where('id', '=', $id)
                ->update([
                    'panel'             => $panel,
                    'is_change_box'     => '1',
                    'justification_box' => $request->justification
                ]);

            $history    = CalibrationHistory::create([
                'user_id'       => $nik,
                'panel'         => $panel,
                'change_by_nik' => $nik_admin,
                'created_by'    => '3',
                'tahun'         => date('Y'),
                'panel_before'  => $cek_panel->panel,
                'sub_event'     => $data_talent->sub_event
            ]);
            $data_response = [
                'is_moved' => 1,
            ];
            return json_encode((object) array_merge(boxProperties(false, $panel), $data_response));
        // }
    }

    //method update employee percent
    public function update_percent(Request $request)
    {
        $request->persen = true;
        return $this->ajaxEmployee($request);

        if ($request->is_session == 'true') {
            if (isset($_SESSION['session-filter-grade'])) {
                $_SESSION['session-filter-grade'] = array_unique($request->grade);
            } else {
                $_SESSION['session-filter-grade'] = array_unique($request->grade);
            }

            if (isset($_SESSION['session-filter-chief'])) {
                $_SESSION['session-filter-chief'] = array_unique($request->pos);
            } else {
                $_SESSION['session-filter-chief'] = array_unique($request->pos);
            }
        }

        $panel      = $request->panel;
        $hrOnly     = $request->hrOnly;
        $pos        = $request->pos;
        $grade      = $request->grade;
        $qry        = $request->quer;
        $talent     = $request->talent;
        $all_panels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $arr_view   = [];

        $arr_grade = [];
        if (!isset($request->grade)) {
            if (isset($_SESSION['session-filter-grade'])) {
                $arr_grade = $_SESSION['session-filter-grade'];
            } else {
                $arr_grade = [8];
            }
        } else {
            if (is_array($grade) && !empty($grade)) {
                foreach ($grade as $val) {
                    $arr_grade[] = $val;
                }
            }
        }

        if ($talent == "false") {
            $panels = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        } else {
            $panels = [7, 8, 9];
        }

        foreach ($all_panels as $key => $panel) {
            if ($panel == 0) {
                $data = [];
                $total_employee = [];
            } else {
                $total_employee = Employee::with('employee_calibration')
                    ->whereHas('employee_calibration', function ($q) use ($panels) {
                        $q->whereIn('panel', $panels);
                    })
                    ->when(!empty($arr_grade), function ($request) use ($arr_grade) {
                        $request->whereIn('ps_group', $arr_grade);
                    });

                $data = Employee::with('employee_calibration')
                    ->whereHas('employee_calibration', function ($q) use ($panel) {
                        $q->where('panel', $panel);
                    })
                    ->when(!empty($arr_grade), function ($request) use ($arr_grade) {
                        $request->whereIn('ps_group', $arr_grade);
                    })
                    ->orderByRaw('ps_group DESC, name ASC');

                if ($hrOnly == "true") {
                    $data = $data->where('directorate', 'LIKE', '%Human Resource%');
                }

                if (!empty($pos)) {
                    $data = $data->where(function ($query) use ($pos) {
                        $query = $query->where('chief', $pos[0]);
                        foreach ($pos as $key => $value) {
                            $query = $query->orWhere('chief', $value);
                        }
                        return $query;
                    });
                    $total_employee = $total_employee->where(function ($query) use ($pos) {
                        $query = $query->where('chief', $pos[0]);
                        foreach ($pos as $key => $value) {
                            $query = $query->orWhere('chief', $value);
                        }
                        return $query;
                    });
                } else {
                    if (isset($_SESSION['session-filter-chief'])) {
                        $data = $data->whereIn('chief', $_SESSION['session-filter-chief']);
                        $total_employee = $total_employee->whereIn('chief', $_SESSION['session-filter-chief']);
                    }
                }
                $data = $data->get();
                $total_employee = $total_employee->get();
            }

            $fs = isset($request->isFullScreen) ? $request->isFullScreen : false;
            
            $arr_view[] = View::make('calibration/panel_percent', compact('data', 'fs', 'panel', 'total_employee'))->render();
            
        }
        
        $data = json_encode(array(
            'view' => $arr_view
        ));
        
        return $data;
    }

    public function berita_acara($id_sub_event)
    {
        $tc = data_ba_committee($id_sub_event);
        $nomor_ba = generate_no_ba("TC", $id_sub_event);

        $sub_event1 = DB::select("
            select talent.nik, 
            talent.panel,
            if(talent.is_change_box = '1', talent.proposed_box, talent.panel) as talent_cluster,
            employees.personnel_number,
            employees.prev_persno,
            employees.position as kode_posisi,
            employees.position_name,
            sub_event.talent_com_1,
            sub_event.talent_com_2,
            sub_event.talent_com_3,
            sub_event.position,
            sub_event.id as id_sub_event,
            employee_calibration_history.sub_event,
            employee_calibration_history.panel_before,
            employee_calibration_history.panel as history_panel,
            master_position.name_position,
            master_position.code_subholding,
            master_subholding.name_subholding,
            ba_event.no_ba
            from talent 
            left join ba_event on ba_event.id_sub_event = talent.sub_event
            left join employee_calibration_history on employee_calibration_history.user_id = talent.nik
            left join employees on employees.prev_persno collate utf8mb4_unicode_ci = talent.nik
            left join sub_event on talent.sub_event = sub_event.id
            left join master_position on master_position.code_position = sub_event.position
            left join master_subholding on master_subholding.code_subholding = master_position.code_subholding
            where talent.sub_event = '".$id_sub_event."' and ba_event.prefix = 'TC' and talent.status = 'selected' 
            group by talent.nik
        ");
        // query untuk mengambil data dari tabel employee_calibration_history yang datanya lebih dari 1 x pernindahan
        $sub_event = [];
        $count_nik = [];
        foreach ($sub_event1 as $key => $sb) {
            $get_data = DB::select("select * from employee_calibration_history where user_id = '".$sb->nik."' and sub_event = '".$id_sub_event."' group by user_id"); 
            if ($get_data) {
                foreach ($get_data as $keyy => $gd) {
                    $before_panel = DB::table('employee_calibration_history')->where('user_id', $sb->nik)->where('sub_event', $id_sub_event)->orderBy('id', 'ASC')->value('panel_before');
                    $history_panel = DB::table('employee_calibration_history')->where('user_id', $sb->nik)->where('sub_event', $id_sub_event)->orderBy('id', 'DESC')->value('panel');
                    $sub_event[] = [
                         'nik' => $gd->user_id ? $sb->nik : "-",
                         'panel' => $gd->panel,
                         'talent_cluster' => $gd->panel,
                         'personnel_number' => $sb->personnel_number,
                         'prev_persno' => $sb->prev_persno,
                         'kode_posisi' => $sb->kode_posisi,
                         'position_name' => $sb->position_name,
                         'talent_com_1' => $sb->talent_com_1,
                         'talent_com_2' => $sb->talent_com_2,
                         'talent_com_3' => $sb->talent_com_3,
                         'position' => $sb->position,
                         'id_sub_event' => $sb->sub_event,
                         'panel_before' => $before_panel ? $before_panel : null,
                         'history_panel' => $history_panel ? $history_panel : null,
                         'name_position' => $sb->name_position,
                         'code_subholding' => $sb->code_subholding,
                         'name_subholding' => $sb->name_subholding,
                         'no_ba' => $sb->no_ba
                     ];
                 }
            }else{
                $sub_event[] = [
                    'nik' => $sb->nik,
                    'panel' => $sb->panel,
                    'talent_cluster' => $sb->panel,
                    'personnel_number' => $sb->personnel_number,
                    'prev_persno' => $sb->prev_persno,
                    'kode_posisi' => $sb->kode_posisi,
                    'position_name' => $sb->position_name,
                    'talent_com_1' => $sb->talent_com_1,
                    'talent_com_2' => $sb->talent_com_2,
                    'talent_com_3' => $sb->talent_com_3,
                    'position' => $sb->position,
                    'id_sub_event' => $sb->sub_event,
                    'panel_before' => null,
                    'history_panel' => null,
                    'name_position' => $sb->name_position,
                    'code_subholding' => $sb->code_subholding,
                    'name_subholding' => $sb->name_subholding,
                    'no_ba' => $sb->no_ba
                ];
            }
        }
        $exist = Ba_event::where('prefix', 'TC')->where('id_sub_event', $id_sub_event)->where('tanggal_ba', date("Y-m-d"))->first();
        if ($exist != null) {
            Ba_event::where('id', $exist->id)
                ->update([
                    'data_ba' => json_encode($sub_event),
                    'data_commite' => json_encode($tc)
                ]);
            $pdf = PDF::loadView('talent-mapping.berita-acara', compact('sub_event','nomor_ba','tc') );    
            return $pdf->download('BERITA ACARA KESEPAKATAN PEMILIHAN PESERTA TALENT DAY.pdf');
        }else{
            return redirect()->route('talent-mapping.index');
        }
    }
}