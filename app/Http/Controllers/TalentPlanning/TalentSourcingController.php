<?php

namespace App\Http\Controllers\TalentPlanning;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\EventWindowTimeModel;
use App\Models\SubEvent;
use App\Models\Talent;
use App\Models\User;
use App\Models\TalentAspiration;
use App\Models\Employee;
use App\Models\Position;
use App\Models\JobFamily;
use App\Http\Controllers\MyProfile\MyProfileController;
use App\Models\EmployeeChildren;
use App\Models\EmployeeDetailHistoryPosition;
use App\Models\EmployeeDiklat;
use App\Models\EmployeeExpAcara;
use App\Models\EmployeeJabatanBumn;
use App\Models\EmployeeKarya;
use App\Models\EmployeeKeahlian;
use App\Models\EmployeeKelas;
use App\Models\EmployeeKluster;
use App\Models\EmployeeMasterExperience;
use App\Models\EmployeeOrganization;
use App\Models\EmployeePasangan;
use App\Models\EmployeePenghargaan;
use App\Models\EmployeePenugasan;
use App\Models\EmployeeReferensi;
use App\Models\EmployeesDocs;
use App\Models\EmployeesStatusUpdate;
use App\Models\EmployeesUraian;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use DB;
use PDF;
use App\Models\FungsiJabatanBumn;
use App\Models\Keahlian;
use App\Models\KlusterBumn;
use App\Models\Pendidikan;
use App\Models\PengalamanKelasBumn;
// use App\Models\Position;
use App\Models\PositionHistory;


class TalentSourcingController extends Controller
{
    private $event_active = 0;
    private $event_name = "";
    private $event_start = [];
    private $event_end = [];
    private $event_position_eligible = [];
    private $event_employee_eligible = [];

    public function __construct() {
        $raw = EventWindowTimeModel::where('status', 'open')->limit(1)->select('id', 'name', 'start_date', 'end_date')->first();
        if($raw) {
            $this->event_active = $raw->id;
            $this->event_name = $raw->name;
            $this->event_start = date('j M', strtotime($raw->start_date));
            $this->event_end = date('j M', strtotime($raw->end_date));
        }
    }

    public function index(){
        // * Untuk Query get data sub event ini saya sedikit modifikasi pada field total untuk keperluan sourcing type external (Modification By Rifki)
        // TODO karena satu hal dari user, querynya saya balikan lagi ke awal lagi (Modification By Rifki)
        $position = SubEvent::join('master_position', 'master_position.code_position', '=', 'sub_event.position')
        ->join('event_window_time', 'event_window_time.id', '=', 'sub_event.id_event')
        ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
        ->join('master_area', 'master_area.code_area', 'master_position.area')
        ->where([
            ['event_window_time.status', 'open'],
            ['sub_event.status', '=', 'sourcing']
        ])
        ->select(
            'master_position.name_position',
            'sub_event.status',
            'sub_event.id',
            'sub_event.sourcing_type',
            'master_subholding.name_subholding', 
            'master_area.name_area',
            DB::raw('(SELECT count(distinct(nik)) FROM talent WHERE talent.sub_event=sub_event.id AND talent.deleted_at IS NULL) AS participant'),
            DB::raw('(SELECT count(distinct(nik)) FROM talent WHERE talent.sub_event=sub_event.id AND talent.deleted_at IS NULL) AS total'),
            'sub_event.sourcing_type'
        )
        ->get();

        $page_title = 'Talent Sourcing';
        $page_description = 'Halaman Talent Sourcing (Career Cards)';
        $show_engage = $this->event_active != 0 ? false : true;
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Talent Planing'],
            ['page' => '#', 'title' =>'Talent Sourcing']
        ];

        return view('talent-sourcing.index', compact('page_title', 'page_description','page_breadcrumbs', 'position','show_engage'));
    }

    public function detail($id){
        $sub_event  = SubEvent::select(
            'master_position.name_position', 
            'master_position.code_position', 
            'sub_event.id', 'sub_event.id_event', 
            'sub_event.sourcing_type',
            'master_subholding.name_subholding', 
            'master_area.name_area'
        )
        ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
        ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
        ->join('master_area', 'master_area.code_area', 'master_position.area')
        ->where('sub_event.id', '=', $id)
        ->first();
        
        $year_end = date('Y') - 1;
        $year_start = $year_end - 3;

        $data = Talent::select('talent.*', 'talent_aspiration.is_add_talent_sourcing','talent_aspiration.comitte_proposal', 'talent_aspiration.id as talent_aspiration_id')
        ->join('talent_aspiration', 'talent.nik', '=', 'talent_aspiration.nik')
        ->with(['employee' => function($query) use($year_start, $year_end) {
            $query->with(['performance' => function($query) use($year_end, $year_start) {
                $query->whereBetween('year', [$year_start, $year_end]);
            }]);
            $query->with('master_posisi');
        }])
        ->where('talent.sub_event', '=', $id)
        ->where(function($query) {
            $query->whereNull('status')
                ->orWhere('status', '=', 'selected');
        })
        ->groupBy('talent.nik')
        ->get();
        
        $page_title = 'Talent Sourcing';
        $page_description = 'Halaman Talent Sourcing (Career Cards)';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Talent Planing'],
            ['page' => '#', 'title' =>'Talent Sourcing']
        ];

        return view('talent-sourcing.detail', compact('page_title', 'page_description','page_breadcrumbs', 'data', 'sub_event'));
    }

    public function store(Request $request)
    {
        $array_emp  = json_decode($request->dataStringify);
        // Kondisi Employee yang terminated
        $selected_employee = array_column($array_emp, 'nik');
        Talent::whereNotIn('nik', $selected_employee)
            ->where('sub_event', $request->sub_event)
            ->update([
                'status' => 'terminated'
            ]);
        

        foreach($array_emp as $item){
            $insert = Talent::updateOrCreate(
                [
                    'nik'       => $item->nik,
                    'sub_event' => $request->sub_event,
                ],
                [
                    'status'    => 'selected'
                ]
            );
            // * Update talent Aspiration jika usulan committee
            if($item->proposal) {
                TalentAspiration::where('id', $item->talentAspirationId)
                ->update([
                    'comitte_proposal' => 1
                ]);
            }else{
                TalentAspiration::where('id', $item->talentAspirationId)
                ->update([
                    'comitte_proposal' => 0
                ]);
            }

            $user = User::where("nik", $item->nik)->first();
            if ($user) {
                $user->notify(new \App\Notifications\ProfileUpdateNotification($user, $insert));
            }

        }

        $sub_event  = SubEvent::where('id', '=', $request->sub_event)
        ->update([
            'status' => 'profiling'
        ]);
        
        return redirect()->route('talent-sourcing.index')->with(["id_event" => $request->sub_event]);
    }
    public function GenerateBeritaAcara($id)
    {
        $sub_event = DB::select("
        select talent.nik, talent.status,
        if(talent.is_change_box = 1, talent.proposed_box, talent.panel) as talent_cluster,
        employees.personnel_number,
        employees.prev_persno,
        employees.position_name,
        -- employees.position_name,
        sub_event.talent_com_1,
        sub_event.talent_com_2,
        sub_event.talent_com_3,
        sub_event.position,
        master_position.name_position,
        master_position.code_subholding,
        master_subholding.name_subholding
        from talent 
        left join employees on employees.prev_persno collate utf8mb4_unicode_ci = talent.nik
        left join sub_event on talent.sub_event = sub_event.id
        left join master_position on master_position.code_position = sub_event.position
        left join master_subholding on master_subholding.code_subholding = master_position.code_subholding
        where talent.sub_event = '".$id."' AND talent.status = 'selected'
        ");

        $nomor_ba = "001/TSO/II/2022";

        // return view('talent-mapping.berita-acara', compact('sub_event','nomor_ba'));     

        $pdf = PDF::loadView('talent-sourcing.berita-acara', compact('sub_event','nomor_ba') );    
       return $pdf->download('Berita Acara Talent Sourcing.pdf');
    }

    // * Function untuk get data employee select2
    public function select2Employee(Request $req) {
        $val = $req->value;
        $code = 200;
        $message = "success";
        $employee = [];

        $year_end = date('Y') - 1;
        $year_start = $year_end - 3;

        $listDataTalentSourcingExist = Talent::select('talent.*', 'talent_aspiration.is_add_talent_sourcing')
        ->join('talent_aspiration', 'talent.nik', '=', 'talent_aspiration.nik')
        ->with(['employee' => function($query) use($year_start, $year_end) {
            $query->with(['performance' => function($query) use($year_end, $year_start) {
                $query->whereBetween('year', [$year_start, $year_end]);
            }]);
            $query->with('master_posisi');
        }])
        ->where('talent.sub_event', '=', $req->subEventId)
        ->where(function($query) {
            $query->whereNull('status')
                ->orWhere('status', '=', 'selected');
        })
        ->groupBy('talent.nik')
        ->pluck('talent.nik');
        
        if ($val == 1) { // * Kondisi untuk employee external
            $employee = Employee::select('employees.*', 'master_hukdis.nik')
            ->leftJoin('master_hukdis', DB::raw('employees.prev_persno collate utf8mb4_unicode_ci'), '=', 'master_hukdis.nik')
            ->where("status_karyawan", "External")
            ->whereNull('master_hukdis.nik')
            ->whereNotIn('employees.prev_persno', $listDataTalentSourcingExist)
            ->get();
        }

        if(isset($req->jobFamily) && $req->jobFamily) {
            $dataPosition = Position::where('code_position', $req->codePosition)
            ->first();

            if($dataPosition) {
                $jobFamilyArr = [
                    $dataPosition->code_job_family,
                    $dataPosition->code_job_family_2,
                    $dataPosition->code_job_family_3
                ];

                $dataPositionHistory = PositionHistory::whereIn('code_job_family_align', $jobFamilyArr)
                ->orWhereIn('code_job_family_align_2', $jobFamilyArr)
                ->orWhereIn('code_job_family_align_2', $jobFamilyArr)
                ->get();

                $arrNik = [];
                
                // * looping untuk mencari data employee per 3 tahun
                foreach($dataPositionHistory as $valueHistory) {
                    $startDate = $valueHistory->start_date;
                    $endDate = $valueHistory->end_date;
                    $nik = $valueHistory->nik;

                    $tahunAwal = date('Y', strtotime($startDate));
                    $condAkhirJabat = (is_null($endDate) || $endDate == '0000-00-00') ? date('Y-md-d') : $endDate;
                    $tahunAkhir = date('Y', strtotime($condAkhirJabat));

                    $lastJabat = ($tahunAwal - $tahunAkhir >= 3) ? 1 : 0;
                    if($lastJabat) {
                        array_push($arrNik, $nik);
                    }

                    array_unique($arrNik);
                }

                $employee = Employee::whereIn('prev_persno', $arrNik)
                ->whereNotIn('employees.prev_persno', $listDataTalentSourcingExist)
                ->get();
            }
        }
        
        $data = array(
            'message' => $message,
            'code' => $code,
            'data' => $employee
        );

        return response()->json($data, $code);
    }

    // * Function untuk post data employee dari talent sourcing
    public function addEmployeeSourcing(Request $req) {
        $status = "success";
        $title = "Success!";
        $message = "Success Add Employee";
        $create = false;

        if ($req->employee != null) {
            $create = TalentAspiration::create([
                'id_event' => $req->eventId,
                'code_position' => $req->codePosition,
                'nik' => $req->employee,
                'aspiration_type' => 'supervisor',
                'is_add_talent_sourcing' => 1,
                'comitte_proposal' => isset($req->proposal) || (isset($req->employeeType) && $req->employeeType == 1) ? 1 : 0
            ]);

            $create_talent = Talent::create([
                'sub_event' => $req->subEventId,
                'nik' => $req->employee,
                'status' => 'selected'
            ]);
        }

        if (!$create || !$create_talent) {
            $title = "Failed!";
            $message = "Failed Add Employee";
            $status = "error";
        }

        return redirect()->back()->with([
            "status" => $status, 
            "title" => $title,
            "message" => $message
        ]);
    }

    // * Function untuk delete talent aspiration
    public function deleteTalentAspiration($id) {
        $code     = 200;
        $message  = 'success';
        $talent = false;

        // TODO diganti jadi delete ke tabel talent, karena query dari detail datanya lebih condong ke tabel talen
        $talent = Talent::find($id);
        $talent->delete();

        if (!$talent) {
            $code     = 400;
            $message  = 'failed';
            $talent = false;
        }

        $data = array(
            'message' => $message,
            'code'    => $code,
            'data'    => $talent,
        );

        return response()->json($data, $code);
    }

    // * Function untuk show profile di halaman talent sourcing
    public function profile($nik, Request $req) {
        $data = MyProfileController::index(true, $nik);
        $page_title = 'Profile';
        $page_description = 'Halaman Profile';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>' Profile Employee']
        ];

        $data['page_title'] = $page_title;
        $data['page_description'] = $page_description;
        $data['page_breadcrumbs'] = $page_breadcrumbs;
        $data['back'] = isset($req->urlPrevious) ? $req->urlPrevious : '#';

        return view('talent-sourcing.profile', $data);
    }

    public function update_profile(Request $req)
    {
        $employee = json_decode($req->employee);
        $uid = $employee->id;
        if (request()->hasFile('profile_avatar')) {
            if ($employee->image != "default.svg" && file_exists(public_path('userprofile/') . $employee->image)) {
                unlink(public_path('userprofile/') . $employee->image);
            }
            $file = request()->file('profile_avatar');
            $filename = uniqid('img_') . '.' . $file->getClientOriginalExtension();
            $file->move('userprofile', $filename);
            User::where('id', $uid)->update(['image' => $filename]);
        }

        Employee::where('prev_persno', $employee->prev_persno)
            ->update([
                'personnel_number' => request('full_name'),
                'major_name' => request('academic'),
                'no_ktp' => request('nik'),
                'birthplace' => request('birth_place'),
                'date_of_birth' => request('birth_date'),
                'gender_text' => request('jenis_kelamin'),
                'religious_denomination' => request('religion'),
                'sosmed_address' => request('sosmed_addr'),
                'position_name' => request('jabatan'),
                'no_hp' => request('hp'),
                'email' => request('email'),
                'no_npwp' => request('npwp'),
                'home_address' => request('alamat_rumah')
            ]);

        $ewt = EventWindowTimeModel::where('status', 'open')->value('id');
        EmployeesStatusUpdate::updateOrCreate(
            [
                'event_window_time' => $ewt,
                'nik' => $employee->prev_persno
            ],
            [
                'is_ket_perorangan' => '1'
            ]
        );

        return redirect()->back()->with([
            "status" => "success",
            "title" => "Berhasil",
            "message" => "Data Berhasil Disimpan"
        ]);
    }

    public function update_interest(Request $req)
    {
        $employee = json_encode($req->employee);
        DB::beginTransaction();
        try {
            Employee::where('prev_persno', $employee->prev_persno)
                ->update([
                    'interest' => request('val'),
                ]);
            $ewt = EventWindowTimeModel::where('status', 'open')->value('id');
            EmployeesStatusUpdate::updateOrCreate(
                [
                    'event_window_time' => $ewt,
                    'nik' => $employee->prev_persno
                ],
                [
                    'is_interest' => '1'
                ]
            );
            DB::commit();
            return [
                "status" => "success",
                "title" => "Berhasil",
                "message" => "Data Berhasil Disimpan"
            ];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return [
                "status" => "failed",
                "title" => "Gagal",
                "message" => "Data Gagal Disimpan"
            ];
        }
    }

    public function update_checkbox(Request $req)
    {
        $employee = json_encode($req->employee);
        $nik = $employee->prev_persno;
        EmployeeKeahlian::where('nik', $nik)->delete();

        if (request('keahlian')) {
            foreach (request('keahlian') as $value) {
                $insert = new EmployeeKeahlian;
                $insert->id_keahlian = $value;
                $insert->nik = $nik;
                if (request('keahlian_lain') && $value == '23') {
                    $insert->keterangan = request('keahlian_lain');
                }
                $insert->checked = '1';
                $insert->save();
            }
        }

        $ewt = EventWindowTimeModel::where('status', 'open')->value('id');
        EmployeesStatusUpdate::updateOrCreate(
            [
                'event_window_time' => $ewt,
                'nik' => $employee->prev_persno
            ],
            [
                'is_keahlian' => '1'
            ]
        );

        return redirect()->back()->with([
            "status" => "success",
            "title" => "Berhasil",
            "message" => "Data Berhasil Disimpan"
        ]);
    }

    public function set_status(Request $req)
    {
        $employee = json_encode($req->employee);
        $sct = request('section');
        switch ($sct) {
            case 'rwyt-jbtn':
                $data = [
                    'is_riwayat_jabatan' => '1'
                ];
                break;
            case 'keangg-org':
                $data = [
                    'is_keanggotaan_org' => '1'
                ];
                break;
            case 'reward':
                $data = [
                    'is_penghargaan' => '1'
                ];
                break;
            case 'rwyt=pend':
                $data = [
                    'is_riwayat_pendidikan' => '1'
                ];
                break;
            case 'kry-tls':
                $data = [
                    'is_karya_ilmiah' => '1'
                ];
                break;
            case 'exp-pmb':
                $data = [
                    'is_peng_pemb_nara' => '1'
                ];
                break;
            case 'refs':
                $data = [
                    'is_referensi' => '1'
                ];
                break;
            case 'emp-fam':
                $data = [
                    'is_ket_keluarga' => '1'
                ];
                break;
            case 'exp-mstr':
                $data = [
                    'is_peng_keahlian' => '1'
                ];
                break;
            case 'peng-bumn':
                $data = [
                    'is_peng_bumn' => '1'
                ];
                break;
            case 'asp-bumn':
                $data = [
                    'is_asp' => '1'
                ];
                break;
            default:
                $data = [];
                break;
        }
        DB::beginTransaction();
        try {
            $ewt = EventWindowTimeModel::where('status', 'open')->value('id');
            EmployeesStatusUpdate::updateOrCreate(
                [
                    'event_window_time' => $ewt,
                    'nik' => $employee->prev_persno
                ],
                $data
            );
            DB::commit();
            return true;
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return false;
        }
    }
}
