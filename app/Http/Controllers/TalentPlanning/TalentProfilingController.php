<?php

namespace App\Http\Controllers\TalentPlanning;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\EventWindowTimeModel;
use App\Models\Employee;
use App\Models\EQS;
use App\Models\SubEvent;
use App\Models\Talent;
use App\Models\TalentAspiration;
use Illuminate\Support\Facades\DB;

class TalentProfilingController extends Controller
{
    private $event_active = 0;
    private $event_name = "";
    private $event_start = [];
    private $event_end = [];
    private $event_position_eligible = [];
    private $event_employee_eligible = [];

    public function __construct() {
        $raw = EventWindowTimeModel::where('status', 'open')->limit(1)->select('id', 'name', 'start_date', 'end_date')->first();
        if($raw) {
            $this->event_active = $raw->id;
            $this->event_name = $raw->name;
            $this->event_start = date('j M', strtotime($raw->start_date));
            $this->event_end = date('j M', strtotime($raw->end_date));
        }
    }

    public function index(){
        $position   = SubEvent::join('master_position', 'master_position.code_position', '=', 'sub_event.position')
        ->join('event_window_time', 'event_window_time.id', '=', 'sub_event.id_event')
        ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
        ->join('master_area', 'master_area.code_area', 'master_position.area')
        ->where([
            ['event_window_time.status', 'open'],
            ['sub_event.status', '=', 'profiling']
        ])
        ->select(
            'master_position.name_position',
            'sub_event.status',
            'sub_event.id',
            'sub_event.sourcing_type',
            'master_subholding.name_subholding', 
            'master_area.name_area',
            DB::raw('(SELECT count(distinct(nik)) FROM talent WHERE talent.sub_event=sub_event.id AND status="selected") AS participant'),
            DB::raw('(SELECT count(distinct(nik)) FROM talent_aspiration WHERE talent_aspiration.id_event=sub_event.id_event AND talent_aspiration.code_position=sub_event.position) AS total')
        )
        ->get();

        $page_title = 'Talent Profiling';
        $page_description = 'Halaman Talent Profiling (Career Cards)';
        $show_engage = $this->event_active != 0 ? false : true; 
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Talent Planing'],
            ['page' => '#', 'title' =>'Talent Profiling']
        ];

        return view('talent-profiling.index', compact('page_title', 'page_description','page_breadcrumbs', 'position','show_engage'));
    }

    public function detail($id){
        $sub_event  = SubEvent::select(
            'master_position.name_position', 
            'master_position.code_position', 
            'sub_event.id', 
            'sub_event.id_event', 
            'sub_event.sourcing_type', 
            'master_subholding.name_subholding', 
            'master_area.name_area'
        )
        ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
        ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
        ->join('master_area', 'master_area.code_area', 'master_position.area')
        ->where('sub_event.id', '=', $id)
        ->first();

        $year_end = date('Y') - 1;
        $year_start = $year_end - 3;

        $data   = Talent::select('talent.*', 'talent_aspiration.comitte_proposal')
        ->with(['employee' => function($query) use($year_start, $year_end) {
            $query->with(['performance' => function($query) use($year_end, $year_start) {
                $query->whereBetween('year', [$year_start, $year_end]);
            }]);
            $query->with('master_posisi');

        }])
        ->join('talent_aspiration', 'talent.nik', '=', 'talent_aspiration.nik')
        ->where([
            ['talent.sub_event', '=', $id],
            ['talent.status', '=', 'selected']
        ])
        ->groupBy('talent.nik')
        ->get();
        
        $page_title = 'Talent Profiling';
        $page_description = 'Halaman Talent Profiling (Career Cards)';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Talent Planing'],
            ['page' => '#', 'title' =>'Talent Profiling']
        ];
        $is_editable = 1;

        return view('talent-profiling.detail', compact('page_title', 'page_description','page_breadcrumbs', 'data', 'sub_event', 'is_editable'));
    }

    //method untuk keperluan data pop up career card
    public function view_career_card($nik)
    {
        $sub_event = request('sub_event');
        $data = data_career_card($nik, $sub_event);
        $eqs = nilai_eqs([$nik], "all", $sub_event);
        $talentAspirationId = request('talentAspirationId');

        if (!$sub_event) {
            echo "<script>console.log('Data Tidak Sesuai Mohon Diperiksa')</script>";
        }
        $data = array_merge(
            $data,
            $eqs,
            ['sub_event' => $sub_event],
            ['is_editable' => request('is_editable')],
            ['is_talent_map' => request('is_talent_map')]
        );

        if (request('is_talent_map')) {
            $data['data_range'] = current(boxProperties(false, $data['talent'][0]->panel));
        }

        if($talentAspirationId) {
            $dataTalentAspiration = TalentAspiration::find($talentAspirationId);
            $data['dataTalentAspiration'] = $dataTalentAspiration;
        }

        return view('talent-profiling.element.career_card_plain', $data);
    }

    public function get_talent_cluster(Request $request) {
        $performance = $request->performance;
        $cci = $request->cci;
        $box = talentMapping($cci, $performance);
        $kinerja = round($request->kinerja / 80 * 100, 2);
        $data = [
            'kinerja' => $kinerja,
            'talent_cluster' => $box,
            'job_fit' => $request->cci,
        ];
        DB::table('employee_eqs')
        ->where('nik', $request->nik)
        ->where('sub_event', $request->sub_event)
        ->update($data);

        $data_talent = [
            'performance' => $performance,
            'cci' => $cci,
            'panel' => $box,
        ];
        $talent_employee = Talent::where('nik', $request->nik)->where('sub_event', $request->sub_event);
        if ($talent_employee->first()) {
            $talent_employee->update($data_talent);
        }

        $nik = $request->nik;
        $sub_event = $request->sub_event;
        
        $data = [
            'box' => $box,
            'predikat' => predikat_talent_cluster($box),
            'job_fit' => round($request->cci * 20 / 100),
            'nik' => $nik,
        ];
        $eqs = nilai_eqs([$nik], "all", $sub_event);
        $response = array_merge($data, $eqs);
        return response()->json($response, 200);
    }

    public function submit_assessment(Request $request)
    {
        $insert = Talent::where('id', '=', $request->id)
        ->update([
            'performance'   => $request->performance,
            'cci'           => $request->assessment,
        ]);

        return json_encode(array(
            "status"=>200
        ));
    }

    public function store(Request $request)
    {
        $talent   = $request->talent_id;

        foreach($talent as $item){
            //cari panel
            $get_talent = Talent::where('id', '=', $item);
            $sub_event = $get_talent->first()->sub_event;
            $nik = $get_talent->first()->nik;
            $data_employee = DB::table('employee_eqs')->where('nik', $nik)->where('sub_event', $sub_event)->first();
            if ($data_employee != null) {
                $kinerja = $data_employee->kinerja;
                $track_record = $data_employee->track_record;
            }else{
                $kinerja = 0;
                $track_record = 0;
            }
            $get_talent->update([
                'cci' => $data_employee->job_fit ?? 0,
                'performance' => ($kinerja * 80 / 100) + ($track_record * 20 / 100)
            ]);

            $panel   = talentMapping($get_talent->first()->cci, $get_talent->first()->performance);

            Talent::where('id', '=', $item)
            ->update([
                'panel'       => $panel,
                'is_profilling' => '1'
            ]);
        }

        Talent::whereNotIn('id', $talent)
        ->where('sub_event', $request->sub_event)
        ->update([
            'status' => 'terminated',
            'is_profilling' => '1'
        ]);

        SubEvent::where('id', '=', $request->sub_event)
        ->update([
            'status' => 'mapping'
        ]);
        
        return redirect()->route('talent-profiling.index');
    }
}
