<?php

namespace App\Http\Controllers\TalentDay;
use App\Models\Ba_event;
use App\Http\Controllers\Controller;
use App\Models\EventWindowTimeModel;
use App\Models\NilaiAssessment;
use App\Models\Pertanyaan;
use App\Models\SubEvent;
use App\Models\Talent;
use App\Jobs\SendBulkQueueEmail;
use App\Models\SubEventTalentComitee;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Session;
use PDF;

class TalentDayController extends Controller
{
    private $event_active = 0;
    private $event_name = "";
    private $event_start = [];
    private $event_end = [];
    private $event_position_eligible = [];
    private $event_employee_eligible = [];

    public function __construct() {
        $raw = EventWindowTimeModel::where('status', 'open')->limit(1)->select('id', 'name', 'start_date', 'end_date')->first();
        if($raw) {
            $this->event_active = $raw->id;
            $this->event_name = $raw->name;
            $this->event_start = date('j M', strtotime($raw->start_date));
            $this->event_end = date('j M', strtotime($raw->end_date));
        }
    }

    public function index()
    {
        $active_event = EventWindowTimeModel::where('status', 'open')->value('id');
        $sub_event = SubEvent::where('id_event', $active_event)
            ->where('sub_event.status', 'talent_day')
            ->select(
                'sub_event.id',
                'sub_event.status',
                'sub_event.sourcing_type',
                'master_subholding.name_subholding', 
                'master_area.name_area',
                DB::raw('(SELECT name_position FROM master_position WHERE master_position.code_position=sub_event.position) AS name_position'),
                DB::raw('(SELECT count(distinct(nik)) FROM talent WHERE talent.sub_event=sub_event.id AND talent.status="selected" AND talent.is_mapping="1") AS participant'),
                DB::raw('(SELECT count(distinct(nik)) FROM talent_aspiration WHERE talent_aspiration.id_event=sub_event.id_event AND talent_aspiration.code_position=sub_event.position) AS total')

            )
            ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
            ->join('event_window_time', 'event_window_time.id', '=', 'sub_event.id_event')
            ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
            ->join('master_area', 'master_area.code_area', 'master_position.area')
            ->get();

        $page_title = 'Talent Day';
        $page_description = 'Halaman Talent Day';
        $page_breadcrumbs = [
            ['page' => '/dashboard', 'title' => 'Home'],
            ['page' => '#', 'title' => 'On Going Event'],
            ['page' => '/talent-day', 'title' => 'Talent Day']
        ];

        $show_engage = $this->event_active != 0 ? false : true; 

        return view('talent-day.index', compact('page_title', 'page_description', 'page_breadcrumbs', 'sub_event','show_engage'));
    }

    public function detail($id)
    {
        $sub_event  = SubEvent::select(
            'master_position.name_position', 
            'master_position.code_position', 
            'sub_event.id',
            'sub_event.id_event', 
            'sub_event.sourcing_type', 
            'master_subholding.name_subholding', 
            'master_area.name_area'
        )
            ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
            ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
            ->join('master_area', 'master_area.code_area', 'master_position.area')
            ->where('sub_event.id', '=', $id)
            ->first();

        // $year_end = 2020;
        // $year_start = $year_end - 2;
        
        $year_end = date('Y') - 1;
        $year_start = $year_end - 3;

        $data   = Talent::select('talent.*', 'talent_aspiration.comitte_proposal')
        ->with(['employee' => function ($query) use ($year_start, $year_end) {
            $query->with(['performance' => function ($query) use ($year_end, $year_start) {
                $query->whereBetween('year', [$year_start, $year_end]);
            }]);
            $query->with('master_posisi');
        }])
        ->join('talent_aspiration', 'talent.nik', '=', 'talent_aspiration.nik')
        ->where([
            ['talent.sub_event', '=', $id],
            ['talent.status', '=', 'selected'],
            ['talent.is_mapping', '=', '1'],
            ['talent.status', '=', 'selected']
        ])
        ->groupBy('talent.nik')
        ->get();

        $page_title = 'Talent Day';
        $page_description = 'Halaman Talent Day';
        $page_breadcrumbs = [
            ['page' => '/dashboard', 'title' => 'Home'],
            ['page' => '/sub-event', 'title' => 'On Going Event'],
            ['page' => '/talent-day', 'title' => 'Talent Day'],
            ['page' => '#', 'title' => 'Detail']
        ];

        return view('talent-day.detail.index', compact('page_title', 'page_description', 'page_breadcrumbs', 'data', 'sub_event'));
    }

    public function detail_assessment_index() {
        if(Session::get('role') == "talent_committee") {
            return $this->detail_assessment(request('id'), Sentinel::getUser()->nik);
        } else if (Session::get('role') == "admin") {
            return $this->detail_assessment(request('id'), request('tcom'));
        }
    }

    public function detail_assessment($id, $tcom = null)
    {
        if($id && $tcom) {
            $common = Talent::with(['sub_events' => function ($query) {
                $query->with('get_position');
            }])
            ->where('talent.id', $id)
            ->first();
    
            $position_name = $common->sub_events->get_position->name_position;
    
            $year_end = date('Y') - 1;
            $year_start = $year_end - 3;
    
            $data   = Talent::with(['employee' => function ($query) use ($year_start, $year_end) {
                $query->with(['performance' => function ($query) use ($year_end, $year_start) {
                    $query->whereBetween('year', [$year_start, $year_end]);
                }]);
            }])->where([
                ['talent.id', '=', $id],
                ['talent.status', '=', 'selected'],
                ['talent.is_mapping', '=', '1'],
                ['talent.status', '=', 'selected']
            ])->first();
    
            $assessment = Pertanyaan::get();
    
            $sub_event = $common->sub_event;
    
            $total_nilai = NilaiAssessment::where('nik', $data->employee->prev_persno)
                ->where('sub_event', $sub_event)
                ->where('nik_talentcom', $tcom)
                ->sum('nilai');
    
            $ket_nilai = "";
    
            if($total_nilai) {
                $total_nilai = number_format($total_nilai / count($assessment), 2);
                $ket_nilai = assessment_label($total_nilai);
            }
    
            DB::update("update talent set talent_day_result='".$ket_nilai."' where sub_event='".$sub_event."' and nik='".$data->employee->prev_persno."'");
    
            $page_title = 'Talent Day';
            $page_description = 'Halaman Talent Day';
            $page_breadcrumbs = [
                ['page' => '/dashboard', 'title' => 'Home'],
                ['page' => '/sub-event', 'title' => 'On Going Event'],
                ['page' => '/talent-day', 'title' => 'Talent Day'],
                ['page' => '#', 'title' => 'Detail Assessment']
            ];
    
            return view('talent-day.detail.assessment', compact('page_title', 'page_description', 'page_breadcrumbs', 'data', 'position_name', 'assessment', 'sub_event', 'total_nilai','ket_nilai'));
        } else if ($id) {
            $year_end = date('Y') - 1;
            $year_start = $year_end - 3;

            $assessor = Talent::with(['talent_committee' => function($subq){
                $subq->with('employees');
            }])
            ->with(['sub_events' => function($subq) {
                $subq->with('get_position');
            }])
            ->with(['employee' => function ($query) use ($year_start, $year_end) {
                $query->with(['performance' => function ($query) use ($year_end, $year_start) {
                    $query->whereBetween('year', [$year_start, $year_end]);
                }]);
            }])
            ->where('talent.id', $id)
            ->get()
            ->map(function($value){
                $talent_com = [];
                foreach ($value->talent_committee as $arrval) {
                    $talent_com[] = [
                        'nik' => $arrval->nik,
                        'nama' => $arrval->employees->personnel_number,
                        'avg' => assessment_subevent($arrval->id_subevent, $value->nik, $arrval->nik, 'avg'),
                        'status' => assessment_subevent($arrval->id_subevent, $value->nik, $arrval->nik, 'status'),
                        'completed' => assessment_subevent($arrval->id_subevent, $value->nik, $arrval->nik)
                    ];
                }
                return [
                    'id' => $value->id,
                    'position' => $value->sub_events->get_position->name_position,
                    'employee' => $value->employee,
                    'sub_event' => $value->sub_event,
                    'talent_committee' => $talent_com
                ];
            })
            ->toArray();

            $data = (object) $assessor[0];

            $sub_event = $data->sub_event;

            $page_title = 'Talent Day';
            $page_description = 'Halaman Talent Day';
            $page_breadcrumbs = [
                ['page' => '/dashboard', 'title' => 'Home'],
                ['page' => '/sub-event', 'title' => 'On Going Event'],
                ['page' => '/talent-day', 'title' => 'Talent Day'],
                ['page' => '#', 'title' => 'Daftar Assessor']
            ];
            return view('talent-day.detail.assessment-highlight', compact('page_title', 'page_description', 'page_breadcrumbs', 'assessor', 'sub_event', 'data'));
        } else {
            return abort(404);
        }

    }

    public function detail_assessment_update()
    {
        $pk = request('pk');
        $value = request('value');
        $sct = request('sct');

        if ($sct == "nil") {
            NilaiAssessment::updateOrCreate(
                ['id_pertanyaan' => $pk, 'nik' => request('nik'), 'sub_event' => request('sub'), 'nik_talentcom' => Sentinel::getUser()->nik],
                ['nilai' => $value]
            );
        } else if ($sct == "ket") {
            NilaiAssessment::updateOrCreate(
                ['id_pertanyaan' => $pk, 'nik' => request('nik'), 'sub_event' => request('sub'), 'nik_talentcom' => Sentinel::getUser()->nik],
                ['keterangan' => $value]
            );
        }

        return $value;
    }

    public function store()
    {
        $talent   = request('talent_id');
        $id_sub_event = request('sub_event');

        $total_talent = Talent::where('sub_event', $id_sub_event)->count();
        $total_assessment = NilaiAssessment::where('sub_event', $id_sub_event)->groupBy('nik')->groupBy('sub_event')->count();

        // if ($total_talent != $total_assessment) { 
        //     return redirect()->route('talent-day.index')->with([
        //         "status" => "error", 
        //         "title" => "Peserta belum semua dinilai",
        //         "message" => "Peserta harus dinilai semuanya"
        //     ]);
        // } 

        foreach ($talent as $item) {
            Talent::where('id', '=', $item)
                ->update([
                    'is_day' => '1'
                ]);
        }

        Talent::whereNotIn('id', $talent)
            ->where('sub_event', $id_sub_event)
            ->where('is_mapping', '1')
            ->where('status', 'selected')
            ->update([
                'status' => 'terminated',
                'is_day' => '1'
            ]);

        SubEvent::where('id', '=', $id_sub_event)
            ->update([
                'status' => 'review'
            ]);
        
        return redirect()->route('talent-day.index')->with([
            "id_event" => $id_sub_event
        ]);
            
    }

    public function save() {
        DB::beginTransaction();
        if(request('sct') == 'drft') {
            try {
                NilaiAssessment::where('nik', request('nk'))
                    ->where('sub_event', request('sub'))
                    ->where('nik_talentcom', Sentinel::getUser()->nik)
                    ->update([
                        'is_draft' => '1'
                    ]);
                DB::commit();
                return true;
            } catch (\Throwable $th) {
                //throw $th;
                DB::rollBack();
                return false;
            }
        } else if (request('sct') == 'save') {
            try {
                NilaiAssessment::where('nik', request('nk'))
                    ->where('sub_event', request('sub'))
                    ->where('nik_talentcom', Sentinel::getUser()->nik)
                    ->update([
                        'is_draft' => '0'
                    ]);
                DB::commit();
                return true;
            } catch (\Throwable $th) {
                //throw $th;
                DB::rollBack();
                return false;
            }
        }
    }

    public function berita_acara($id_sub_event)
    {
        $tc = data_ba_committee($id_sub_event);
        $nomor_ba = generate_no_ba("TD", $id_sub_event);

        $sub_event = DB::select("
            select talent.nik, 
            talent.talent_day_result as result,
            talent.panel,
            if(talent.is_change_box = '1', talent.proposed_box, talent.panel) as talent_cluster,
            employees.personnel_number,
            employees.prev_persno,
            employees.position_name,
            employees.position as kode_posisi,
            sub_event.talent_com_1,
            sub_event.talent_com_2,
            sub_event.talent_com_3,
            sub_event.position,
            sub_event.id as id_sub_event,
            master_position.name_position,
            master_position.code_subholding,
            master_subholding.name_subholding,
            ba_event.no_ba,
            talent.id as id_talent
            from talent 
            left join ba_event on ba_event.id_sub_event = talent.sub_event
            left join employees on employees.prev_persno collate utf8mb4_unicode_ci = talent.nik
            left join sub_event on talent.sub_event = sub_event.id
            left join master_position on master_position.code_position = sub_event.position
            left join master_subholding on master_subholding.code_subholding = master_position.code_subholding
            where talent.sub_event = '".$id_sub_event."' and ba_event.prefix = 'TD' and talent.status = 'selected'
        ");

        // return view('talent-day.berita-acara', compact('sub_event','nomor_ba','tc'));

        $exist = Ba_event::where('prefix', 'TD')->where('id_sub_event', $id_sub_event)->where('tanggal_ba', date("Y-m-d"))->first();
        if ($exist != null) {
            Ba_event::where('id', $exist->id)
                ->update([
                    'data_ba' => json_encode($sub_event),
                    'data_commite' => json_encode($tc)
                ]);
                $pdf = PDF::loadView('talent-day.berita-acara', compact('sub_event','nomor_ba','tc') );    
                return $pdf->download('BERITA ACARA KESEPAKATAN HASIL PENILAIAN TALENT DAY.pdf');
        }else{
            return redirect()->route('talent-day.index');
        }
    }
}
