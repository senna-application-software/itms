<?php

namespace App\Http\Controllers\MasterData;

use App\Exports\Employee\TemplateImportEmployee;
use App\Http\Controllers\Controller;
use App\Imports\Employee\EmployeeImport;
use App\Models\Directorate;
use App\Models\Division;
use App\Models\Employee;
use App\Models\Group;
use App\Models\MasterJobFamily;
use App\Models\MasterPosition;
use App\Models\Position;
use App\Models\RoleUser;
use App\Models\SubHolding;
use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Sentinel;
use URL;
use Yajra\Datatables\Datatables;

class MasterDataEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobFamily = MasterJobFamily::all();

        $subholding          = "";
        $subHoldingCondition = ($subholding != "" && $subholding != null && $subholding != false);
        $rawPositions        = Position::when($subHoldingCondition, function ($query) use ($subholding) {
            $query->where('code_subholding', $subholding);
        })->with('master_area')->with('sub_holding')->get();
        $subholding = $rawPositions->pluck('sub_holding')->pluck('name_subholding', 'code_subholding');

        $grade = Employee::select('person_grade_align')
            ->orderBy('person_grade_align', 'ASC')
            ->whereNotNull('person_grade_align')
            ->get()
            ->unique('person_grade_align');

        $talentCluster   = array();
        $talentCluster[] = [
            'key' => 1,
            'val' => 'Unfit (Talent Cluster 1)',
        ];
        $talentCluster[] = [
            'key' => 2,
            'val' => 'Sleeping Tiger (Talent Cluster 2)',
        ];
        $talentCluster[] = [
            'key' => 3,
            'val' => 'Solid Contributor (Talent Cluster 3)',
        ];
        $talentCluster[] = [
            'key' => 4,
            'val' => 'Sleeping Tiger (Talent Cluster 4)',
        ];
        $talentCluster[] = [
            'key' => 5,
            'val' => 'Promotable (Talent Cluster 5)',
        ];
        $talentCluster[] = [
            'key' => 6,
            'val' => 'Solid Contributor (Talent Cluster 6)',
        ];
        $talentCluster[] = [
            'key' => 7,
            'val' => 'Promotable (Talent Cluster 7)',
        ];
        $talentCluster[] = [
            'key' => 8,
            'val' => 'Promotable (Talent Cluster 8)',
        ];
        $talentCluster[] = [
            'key' => 9,
            'val' => 'High Potential (Talent Cluster 9)',
        ];

        $page_title       = 'Master Employee';
        $page_description = 'Halaman Master Data Employee';
        $page_breadcrumbs = [
            ['page' => 'dashboard', 'title' => 'Home'],
            ['page' => '#', 'title' => 'Master Data'],
            ['page' => '#', 'title' => 'Master Employee'],
        ];

        return view('master-data.employee.index', compact('page_title', 'page_description', 'page_breadcrumbs', 'jobFamily', 'subholding', 'grade', 'talentCluster'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'prev_persno'      => 'required|unique:employees',
            'personnel_number' => 'required',
            'position'         => 'required',
        ]);

        $dateBirthDate    = date('Y-m-d', strtotime($request->date_of_birth));
        $dateMulaiBekerja = date('Y-m-d', strtotime($request->tanggal_mulai_bekerja));

        $request['date_of_birth']         = $dateBirthDate;
        $request['tanggal_mulai_bekerja'] = $dateMulaiBekerja;

        $lineManager = Employee::where('prev_persno', $request->line_manager)
            ->first();
        $request['line_manager_name']     = $lineManager ? $lineManager->prev_persno : null;
        $request['line_manager_position'] = $lineManager ? $lineManager->position_name : null;

        $position = MasterPosition::where('code_position', $request->position)
            ->first();

        if ($position) {
            $directorate = Directorate::where('code_directorate', $position->code_directorate)
                ->first();
            $group = Group::where('code_group', $position->code_group)
                ->first();
            $division = Division::where('code_division', $position->code_division)
                ->first();
            $jobFamily = MasterJobFamily::where('code_job_family', $position->code_job_family)
                ->first();
            $subHolding = SubHolding::where('code_subholding', $position->code_subholding)
                ->first();

            $request['position_name']    = $position->name_position;
            $request['directorate']      = $directorate ? $directorate->name_directorate : null;
            $request['directorate_code'] = $directorate ? $directorate->code_directorate : null;
            $request['group']            = $group ? $group->name_group : null;
            $request['group_code']       = $group ? $group->code_group : null;
            $request['division']         = $division ? $division->name_division : null;
            $request['division_code']    = $division ? $division->code_division : null;
            $request['job_family']       = $jobFamily ? $jobFamily->name_job_family : null;
            $request['code_subholding']  = $subHolding ? $subHolding->code_subholding : null;
        }

        $createdEmployee = Employee::create($request->only(['personnel_number', 'prev_persno', 'position', 'job_title', 'personnel_area_text', 'area_nomenklatur', 'gender_text', 'religious_denomination', 'marst', 'birthplace', 'date_of_birth', 'major_name', 'education_level', 'university_name', 'tanggal_mulai_bekerja', 'line_manager', 'no_ktp', 'email', 'no_hp', 'status_aktif', 'status_karyawan', 'person_grade', 'bod_type', 'jabfung', 'home_address', 'no_npwp', 'line_manager_name', 'line_manager_position']));

        if ($createdEmployee) {
            Employee::where('id', $createdEmployee->id)
                ->update([
                    'position_name'    => isset($request['position_name']) ? $request['position_name'] : null,
                    'directorate'      => isset($request['directorate']) ? $request['directorate'] : null,
                    'directorate_code' => isset($request['directorate_code']) ? $request['directorate_code'] : null,
                    'division_code'    => isset($request['division_code']) ? $request['directorate_code'] : null,
                    'group_code'       => isset($request['group_code']) ? $request['directorate_code'] : null,
                    'group'            => isset($request['group']) ? $request['group'] : null,
                    'division'         => isset($request['division']) ? $request['division'] : null,
                    'job_family'       => isset($request['job_family']) ? $request['job_family'] : null,
                    'code_subholding'  => isset($request['code_subholding']) ? $request['code_subholding'] : null,
                ]);

            $credentials = array(
                'nik'      => $request->prev_persno,
                'password' => 'itms2022',
            );

            $userRegister = Sentinel::registerAndActivate($credentials);
            $roleEmployee = Sentinel::findRoleBySlug('employee');

            if ($roleEmployee) {
                RoleUser::create([
                    'user_id' => $userRegister->id,
                    'role_id' => $roleEmployee->id,
                ]);
            }
        }

        return redirect()->back()->with([
            "status"  => 'success',
            "title"   => 'Success!',
            "message" => "Employee Created",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message  = "success";
        $code     = 200;
        $employee = false;

        $employee = Employee::find($id);

        if (!$employee) {
            $message = "failed";
            $code    = 400;
        }

        $data = array(
            'message' => $message,
            'code'    => $code,
            'data'    => $employee,
        );
        return response()->json($data, $code);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code     = 200;
        $message  = 'success';
        $employee = false;

        $employee = Employee::find($id);
        if ($employee && $employee->prev_persno) {
            $user = User::where('nik', $employee->prev_persno)
                ->first();

            if ($user) {
                $user->delete();
            }
        }
        $employee->delete();

        if (!$employee) {
            $code     = 400;
            $message  = 'failed';
            $employee = false;
        }

        $data = array(
            'message' => $message,
            'code'    => $code,
            'data'    => $employee,
        );

        return response()->json($data, $code);
    }

    // * Function untuk get datatable server side master data employee
    public function dataTableEmployee(Request $req)
    {
        $data = query_employees($req);

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                if ($data->prev_persno) {
                    $edit = route('talent-sourcing.profile', ['nik' => $data->prev_persno, 'urlPrevious' => URL::to('/masterData/masterDataEmployee')]);
                } else {
                    $edit = "#";
                }

                return '<div class="dropdown table-action">
                    <button class="btn btn-light-primary btn-icon btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ki ki-bold-more-ver"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a href="javascript:void(0)" class="dropdown-item viewDataEmployee" data-id="' . $data->id . '">
                            View
                        </a>
                        <a href="' . $edit . '" class="dropdown-item">
                            Edit
                        </a>
                        <a href="javascript:void(0)" class="dropdown-item deleteEmployee" data-id="' . $data->id . '">
                            Delete
                        </a>
                    </div>
                </div>';
            })
            ->editColumn('status_karyawan', function ($data) {
                $statusKaryawan = 'Internal';
                $label          = "label-light-primary";

                if ($data->status_karyawan == 'External') {
                    $statusKaryawan = 'External';
                    $label          = "label-light-success";
                }

                return '<span class="label font-weight-bold label-lg ' . $label . ' label-inline">' . $statusKaryawan . '</span>';
            })
            ->rawColumns(['action', 'status_karyawan'])
            ->make(true);
    }

    // * Function untuk handle supply data form employee
    public function supplyDataFormEmployee(Request $req)
    {
        $type    = $req->type;
        $status  = 200;
        $message = 'success';
        $resp    = [];

        if ($type == 'position') {
            $resp = MasterPosition::select('code_position as key', 'name_position as value')
                ->get();
        } else if (
            $type == 'job_title' ||
            $type == 'gender_text' ||
            $type == 'religious_denomination' ||
            $type == 'marst' ||
            $type == 'education_level' ||
            $type == 'status_aktif' ||
            $type == 'status_karyawan' ||
            $type == 'person_grade' ||
            $type == 'jabfung'
        ) {
            $resp = $this->select2SupplyStatic($type);
        } else if ($type == 'directorate') {
            $resp = Directorate::select('code_directorate as key', 'name_directorate as value')
                ->get();
        } else if ($type == 'group') {
            $resp = Group::select('code_group as key', 'name_group as value')
                ->get();
        } else if ($type == 'division') {
            $resp = Division::select('code_division as key', 'name_division as value')
                ->get();
        } else if ($type == 'line_manager') {
            $resp = Employee::select('prev_persno as key', 'personnel_number as value')
                ->get();
        } else if ($type == 'subholding') {
            $resp = Subholding::select('code_subholding as key', 'name_subholding as value')
                ->get();
        }

        $data = array(
            'status'  => $status,
            'message' => $message,
            'data'    => array(
                'resp' => $resp,
            ),
        );

        return response()->json($data, $status);
    }

    // * Function untuk handle static select2 di form add employee
    private function select2SupplyStatic($type)
    {
        $resp = [];
        if ($type == 'job_title') {
            $resp[] = [
                'key'   => 'Accelerator',
                'value' => 'Accelerator',
            ];
            $resp[] = [
                'key'   => 'Advisor',
                'value' => 'Advisor',
            ];
            $resp[] = [
                'key'   => 'Assistant Vice President',
                'value' => 'Assistant Vice President',
            ];
            $resp[] = [
                'key'   => 'Chief',
                'value' => 'Chief',
            ];
            $resp[] = [
                'key'   => 'Co General Manager',
                'value' => 'Co General Manager',
            ];
            $resp[] = [
                'key'   => 'Controller',
                'value' => 'Controller',
            ];
            $resp[] = [
                'key'   => 'Coordinator',
                'value' => 'Coordinator',
            ];
            $resp[] = [
                'key'   => 'Deputy Exceutive General Manager',
                'value' => 'Deputy Exceutive General Manager',
            ];
            $resp[] = [
                'key'   => 'Director',
                'value' => 'Director',
            ];
            $resp[] = [
                'key'   => 'Direktur',
                'value' => 'Direktur',
            ];
            $resp[] = [
                'key'   => 'Executive General Manager',
                'value' => 'Executive General Manager',
            ];
            $resp[] = [
                'key'   => 'Expert',
                'value' => 'Expert',
            ];
            $resp[] = [
                'key'   => 'General Manager',
                'value' => 'General Manager',
            ];
            $resp[] = [
                'key'   => 'Head',
                'value' => 'Head',
            ];
            $resp[] = [
                'key'   => 'Manager',
                'value' => 'Manager',
            ];
            $resp[] = [
                'key'   => 'Non Job',
                'value' => 'Non Job',
            ];
            $resp[] = [
                'key'   => 'Officer',
                'value' => 'Officer',
            ];
            $resp[] = [
                'key'   => 'Senior Advisor',
                'value' => 'Senior Advisor',
            ];
            $resp[] = [
                'key'   => 'Senior Auditor',
                'value' => 'Senior Auditor',
            ];
            $resp[] = [
                'key'   => 'Senior Expert',
                'value' => 'Senior Expert',
            ];
            $resp[] = [
                'key'   => 'Senior General Manager',
                'value' => 'Senior General Manager',
            ];
            $resp[] = [
                'key'   => 'Senior Manager',
                'value' => 'Senior Manager',
            ];
            $resp[] = [
                'key'   => 'Senior Vice President',
                'value' => 'Senior Vice President',
            ];
            $resp[] = [
                'key'   => 'Specialist',
                'value' => 'Specialist',
            ];
            $resp[] = [
                'key'   => 'Team Leader',
                'value' => 'Team Leader',
            ];
            $resp[] = [
                'key'   => 'Vice President',
                'value' => 'Vice President',
            ];
        } else if ($type == 'gender_text') {
            $resp[] = [
                'key'   => 'Male',
                'value' => 'Male',
            ];
            $resp[] = [
                'key'   => 'Female',
                'value' => 'Female',
            ];
        } else if ($type == 'religious_denomination') {
            $resp[] = [
                'key'   => 'Islam',
                'value' => 'Islam',
            ];
            $resp[] = [
                'key'   => 'Budha',
                'value' => 'Budha',
            ];
            $resp[] = [
                'key'   => 'Hindu',
                'value' => 'Hindu',
            ];
            $resp[] = [
                'key'   => 'Katholik',
                'value' => 'Katholik',
            ];
            $resp[] = [
                'key'   => 'Protestan',
                'value' => 'Protestan',
            ];
        } else if ($type == 'marst') {
            $resp[] = [
                'key'   => 'Maried',
                'value' => 'Maried',
            ];
            $resp[] = [
                'key'   => 'Single',
                'value' => 'Single',
            ];
        } else if ($type == 'education_level') {
            $resp[] = [
                'key'   => 'S1',
                'value' => 'S1',
            ];
            $resp[] = [
                'key'   => 'S2',
                'value' => 'S2',
            ];
            $resp[] = [
                'key'   => 'S3',
                'value' => 'S3',
            ];
            $resp[] = [
                'key'   => 'D1',
                'value' => 'D1',
            ];
            $resp[] = [
                'key'   => 'D2',
                'value' => 'D2',
            ];
            $resp[] = [
                'key'   => 'D3',
                'value' => 'D3',
            ];
            $resp[] = [
                'key'   => 'D4',
                'value' => 'D4',
            ];
            $resp[] = [
                'key'   => 'TNI',
                'value' => 'TNI',
            ];
            $resp[] = [
                'key'   => 'SMA',
                'value' => 'SMA',
            ];
        } else if ($type == 'status_aktif') {
            $resp[] = [
                'key'   => 'BOD',
                'value' => 'BOD',
            ];
            $resp[] = [
                'key'   => 'DTG',
                'value' => 'DTG',
            ];
            $resp[] = [
                'key'   => 'Honorer',
                'value' => 'Honorer',
            ];
            $resp[] = [
                'key'   => 'Karyawan Kontrak',
                'value' => 'Karyawan Kontrak',
            ];
            $resp[] = [
                'key'   => 'Karyawan Perbantuan',
                'value' => 'Karyawan Perbantuan',
            ];
            $resp[] = [
                'key'   => 'Karyawan Perusahaan',
                'value' => 'Karyawan Perusahaan',
            ];
            $resp[] = [
                'key'   => 'Karyawan Tetap',
                'value' => 'Karyawan Tetap',
            ];
            $resp[] = [
                'key'   => 'KP',
                'value' => 'KP',
            ];
            $resp[] = [
                'key'   => 'Pegawai Perusahaan',
                'value' => 'Pegawai Perusahaan',
            ];
            $resp[] = [
                'key'   => 'Pegawai Perusahaan Diperbantukan',
                'value' => 'Pegawai Perusahaan Diperbantukan',
            ];
            $resp[] = [
                'key'   => 'Pegawai Perusahaan Ditugaskan',
                'value' => 'Pegawai Perusahaan Ditugaskan',
            ];
            $resp[] = [
                'key'   => 'PKWT',
                'value' => 'PKWT',
            ];
            $resp[] = [
                'key'   => 'PKWT KHUSUS',
                'value' => 'PKWT KHUSUS',
            ];
            $resp[] = [
                'key'   => 'PNS Diperbantukan',
                'value' => 'PNS Diperbantukan',
            ];
            $resp[] = [
                'key'   => 'TNI Ditugaskan',
                'value' => 'TNI Ditugaskan',
            ];
        } else if ($type == 'status_karyawan') {
            $resp[] = [
                'key'   => 'External',
                'value' => 'External',
            ];
            $resp[] = [
                'key'   => 'Honorer',
                'value' => 'Honorer',
            ];
            $resp[] = [
                'key'   => 'Kontrak',
                'value' => 'Kontrak',
            ];
            $resp[] = [
                'key'   => 'Perbantuan',
                'value' => 'Perbantuan',
            ];
            $resp[] = [
                'key'   => 'Tetap',
                'value' => 'Tetap',
            ];
        } else if ($type == 'person_grade') {
            $resp[] = [
                'key'   => '1',
                'value' => '1',
            ];
            $resp[] = [
                'key'   => '14',
                'value' => '14',
            ];
            $resp[] = [
                'key'   => '15',
                'value' => '15',
            ];
            $resp[] = [
                'key'   => '16',
                'value' => '16',
            ];
            $resp[] = [
                'key'   => '17',
                'value' => '17',
            ];
            $resp[] = [
                'key'   => '18',
                'value' => '18',
            ];
            $resp[] = [
                'key'   => '19',
                'value' => '19',
            ];
            $resp[] = [
                'key'   => '1A',
                'value' => '1A',
            ];
            $resp[] = [
                'key'   => '1B',
                'value' => '1B',
            ];
            $resp[] = [
                'key'   => '2',
                'value' => '2',
            ];
            $resp[] = [
                'key'   => '20',
                'value' => '20',
            ];
            $resp[] = [
                'key'   => '21',
                'value' => '21',
            ];
            $resp[] = [
                'key'   => '2A',
                'value' => '2A',
            ];
            $resp[] = [
                'key'   => '2B',
                'value' => '2B',
            ];
            $resp[] = [
                'key'   => '3',
                'value' => '3',
            ];
            $resp[] = [
                'key'   => '4',
                'value' => '4',
            ];
            $resp[] = [
                'key'   => '45',
                'value' => '45',
            ];
            $resp[] = [
                'key'   => '48',
                'value' => '48',
            ];
            $resp[] = [
                'key'   => '5',
                'value' => '5',
            ];
            $resp[] = [
                'key'   => '6',
                'value' => '6',
            ];
            $resp[] = [
                'key'   => 'BOD',
                'value' => 'BOD',
            ];
            $resp[] = [
                'key'   => 'BOD INP',
                'value' => 'BOD INP',
            ];
            $resp[] = [
                'key'   => 'BOD INU',
                'value' => 'BOD INU',
            ];
            $resp[] = [
                'key'   => 'D1',
                'value' => 'D1',
            ];
            $resp[] = [
                'key'   => 'D2',
                'value' => 'D2',
            ];
            $resp[] = [
                'key'   => 'D3',
                'value' => 'D3',
            ];
            $resp[] = [
                'key'   => 'E1',
                'value' => 'E1',
            ];
            $resp[] = [
                'key'   => 'E2',
                'value' => 'E2',
            ];
            $resp[] = [
                'key'   => 'E3',
                'value' => 'E3',
            ];
            $resp[] = [
                'key'   => 'E4',
                'value' => 'E4',
            ];
            $resp[] = [
                'key'   => 'E5',
                'value' => 'E5',
            ];
            $resp[] = [
                'key'   => 'F1',
                'value' => 'F1',
            ];
        } else if ($type == 'jabfung') {
            $resp[] = [
                'key'   => 'Direksi',
                'value' => 'Direksi',
            ];
            $resp[] = [
                'key'   => 'Fungsional',
                'value' => 'Fungsional',
            ];
            $resp[] = [
                'key'   => 'NonJob',
                'value' => 'NonJob',
            ];
            $resp[] = [
                'key'   => 'Struktural',
                'value' => 'Struktural',
            ];
        }

        return $resp;
    }

    // * Function untuk handle import data employee
    public function importEmployee(Request $req)
    {
        $req->validate([
            'file' => 'required|mimes:xlsx',
        ]);

        Excel::import(new EmployeeImport, $req->file);

        return redirect()->back()->with([
            "status"  => 'success',
            "title"   => 'Success!',
            "message" => "File has been import",
        ]);
    }

    // * Function untuk download template employee import
    public function templateImportEmployee()
    {
        return Excel::download(new TemplateImportEmployee, 'Template Import Employe.xlsx');
    }
}
