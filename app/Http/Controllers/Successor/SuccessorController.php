<?php

namespace App\Http\Controllers\Successor;

use App\Http\Controllers\Controller;
use App\Jobs\SendBulkQueueEmail;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Models\Talent;
use App\Models\Position;
use App\Models\Employee;
use App\Models\User;
use App\Models\TalentAspiration;
use PDF;

class SuccessorController extends Controller
{
    public function index()
    {
        $page_title = 'Successor List';
        $page_description = '';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Successor List']
        ];
        $posisi_terpilih = Position::with("get_sub_holding")->get();
        $grade = Employee::groupBy("person_grade_align")->orderBy("person_grade_align","ASC")->get();
        
        return view('Successor.index', compact(
            'page_title', 
            'page_description',
            'page_breadcrumbs',
            'posisi_terpilih',
            'grade',
            
        ));
    }

    public function detail(request $req)
    {
        $page_title = 'Successor List';
        $page_description = '';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '/successor-list', 'title' =>'Successor List'],
            ['page' => '#', 'title' =>'Detail']
        ];
        $talent = Talent::with("employee.subholding","employee.positions.directorate","employee.positions.group","employee.positions.division","get_sub_event.position","get_sub_event.position.get_sub_holding")->where("id",$req->id)->first();
        $employee = $talent->employee;
        $endorsment = getEndorsement($talent->nik, $talent->get_sub_event->position, $talent->get_sub_event->id_event);

        $eqs = nilai_eqs([$talent->nik], "all", $talent->sub_event);
        $talent_clushter = predikat_talent_cluster($talent->panel);
        $back = url()->previous();
        return view('Successor.detail', compact(
            'page_description',
            'page_title', 
            'page_breadcrumbs',
            'employee',
            'talent',
            'endorsment',
            'eqs',
            'talent_clushter',
            'back'
        ));
    }

    public function cetak(request $req)
    {
        $page_title = 'Successor List';
        $page_description = '';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '/successor-list', 'title' =>'Successor List'],
            ['page' => '#', 'title' =>'Detail']
        ];

        $talent = Talent::with("employee.subholding","employee.positions.directorate","employee.positions.group","employee.positions.division","get_sub_event.position","get_sub_event.position.get_sub_holding")->where("id",$req->id)->first();
        $employee = $talent->employee;
        $endorsment = getEndorsement($talent->nik, $talent->get_sub_event->position, $talent->get_sub_event->id_event);

        $eqs = nilai_eqs([$talent->nik], "all", $talent->sub_event);
        $talent_clushter = predikat_talent_cluster($talent->panel);
        $img = User::where('nik', $talent->nik)->value('image');
        // return view('Successor.cetak_detail', compact(
        //     'page_description',
        //     'page_title', 
        //     'page_breadcrumbs',
        //     'employee',
        //     'talent',
        //     'endorsment',
        //     'eqs',
        //     'talent_clushter',
        //     'img'
        // ));

        $pdf = PDF::loadView('Successor.cetak_detail', compact(
            'employee',
            'talent',
            'endorsment',
            'eqs',
            'talent_clushter',
            'img'
        ));    
        return $pdf->download('Detail Successor '.$employee->personnel_number.'.pdf');
    }

    public function datatable(request $req)
    { 
        $get = Talent::with("employee","employee.positions","employee.get_subholding","get_sub_event","get_sub_event.position","get_sub_event.position.get_sub_holding")->where("status","final");

        if(!empty($req->posisi_terpilih)){
            $get->whereHas('get_sub_event', function ($query) use($req) {
                $query->where('position',$req->posisi_terpilih);
            });
        }
        if(!empty($req->grade)){
            $get->whereHas('collated_employee', function ($query) use($req) {
                $query->where('person_grade_align',$req->grade);
            });
        }
        $data = $get->get();
        $new_data = array();
        foreach($data as $val){
            
            if(!empty($req->sumber_aspirasi)){
                foreach (getEndorsement($val->nik, $val->get_sub_event->position, $val->get_sub_event->id_event) as $index =>  $item)                             
                {
                    if($item->aspiration_type == $req->sumber_aspirasi){
                        array_push($new_data,$val);
                    }
                }
            }else{
                array_push($new_data,$val);
            }
        }
        return Datatables::of($new_data)
            ->addIndexColumn()
            ->AddColumn('button', function($data) {
                    $html = 
                        '<div class="dropdown table-action">
                            <a href="'.url("/successor-list/cetak?id=").$data->id.'" class="btn btn-primary btn-sm" >
                                Cetak Data
                            </a>
                            <a href="'.url("/successor-list/detail?id=").$data->id.'" class="btn btn-primary btn-sm mt-2">
                                Lihat Detail
                            </a>
                        </div>';
                return $html;
            })->EditColumn('status', function($data){
                switch("completed") {
                    default:            $label = "label-light-primary"; break;
                    case "sourcing":    $label = "label-light-primary"; break;
                    case "profiling":   $label = "label-light-warning"; break;
                    case "mapping":     $label = "label-light-danger"; break;
                    case "talent_day":  $label = "label-light-success"; break;
                    case "review":      $label = "label-primary"; break;
                    case "selection":   $label = "label-warning"; break;
                    case "completed":   $label = "label-dark"; break;
                }

                return '<span class="label font-weight-bold label-lg '.$label.' label-inline">'.$data->status.'</span>';

            })->EditColumn('nama_pegawai', function($data){
                return $data->employee->personnel_number;
            })->EditColumn('posisi_sebelum', function($data){
                return "[".$data->employee->get_subholding->name_subholding."] ".$data->employee->positions->name_position;
            })->EditColumn('posisi_terpilih', function($data){
                return "[".$data->get_sub_event->get_position->get_sub_holding->name_subholding."] ".$data->get_sub_event->get_position->name_position;
            })->EditColumn('grade', function($data){
                return empty($data->employee->person_grade_align) ? "-" : $data->employee->person_grade_align;
            })->EditColumn('tanggal_terpilih', function($data){
                return tanggal_indo(substr($data->updated_at,0,10));
            })->EditColumn('sumber_aspirasi', function($data){
                $as = "";
                foreach (getEndorsement($data->nik, $data->get_sub_event->position, $data->get_sub_event->id_event) as $index =>  $item)                             
                {
                    if ( $item->aspiration_type == "individual")
                    {
                        if($index == 0){
                            $as .= "INDIVIDUAL (IA)";
                        }else{
                            $as .= ",INDIVIDUAL (IA)";
                        }
                    }elseif ($item->aspiration_type == "supervisor")
                    {
                        if($index == 0){
                            $as .= "SUPERVISOR (SA)";
                        }else{
                            $as .= " ,SUPERVISOR (SA)";
                        }
                    }
                    elseif ($item->aspiration_type == "unit")
                    {
                        if($index == 0){
                            $as .= "UNIT (UA)";
                        }else{
                            $as .= " ,UNIT (UA)";
                        }
                    }
                    elseif ($item->aspiration_type == "job_holder")
                    {
                        if($index == 0){
                            $as .= "JOB HOLDER (JA)";
                        }else{
                            $as .= " ,JOB HOLDER (JA)";
                        }
                    }
                    elseif ($item->aspiration_type == "-")
                    {
                        if($index == 0){
                            $as .= "-";
                        }else{
                            $as .= " ,-";
                        }
                    }
                }
                return $as;
            })->AddColumn('vacant_type', function($data) {
                switch($data->get_sub_event->vacant_type) {
                    default:           
                        $label      = "label-light-primary"; 
                        $label_text = "Undefined"; 
                        break;
                    case "vacant_soon": 
                        $label = "label-light-primary"; 
                        $label_text = "Vacant Soon"; 
                        break;
                    case "vacant":      
                        $label      = "label-light-warning"; 
                        $label_text = "Vacant"; 
                        break;
                    case "incumbent":   
                        $label = "label-light-danger"; 
                        $label_text = "Incumbent"; 
                        break;
                    case "ctc":         
                        $label = "label-light-success"; 
                        $label_text = "CTC"; 
                        break;
                    case "semua_posisi":
                        $label = "label-primary"; 
                        $label_text = "General"; 
                        break;
                    case "higher_3year":
                        $label = "label-light-warning"; 
                        $label_text = "Higher than 3 year"; 
                        break;
                    case "external":    
                        $label = "label-dark"; 
                        $label_text = "External Pool"; 
                        break;
                }
                
                return '<span class="label font-weight-bold label-lg '.$label.' label-inline">'.$label_text.'</span>';
            })
            ->rawColumns(['button', 'status', 'vacant_type'])
            ->make(true);
    }
}
