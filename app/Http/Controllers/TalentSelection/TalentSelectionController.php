<?php

namespace App\Http\Controllers\TalentSelection;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PendingTask\PendingTaskController;
use Illuminate\Http\Request;

use App\Models\Employee;
use App\Models\SubEvent;
use App\Models\Talent;
use App\Models\TalentAspiration;
use App\Models\EventWindowTimeModel;
use App\Jobs\SendBulkQueueEmail;
use App\Models\EmployeesToPositions;
use App\Models\PendingTaskNotification;
use App\Models\Position;
use App\Models\Ba_event;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use DB;
use Illuminate\Database\Eloquent\Collection;
use PDF;

class TalentSelectionController extends Controller
{
    private $event_active = 0;
    private $event_name = "";
    private $event_start = [];
    private $event_end = [];
    private $event_position_eligible = [];
    private $event_employee_eligible = [];

    public function __construct()
    {
        $raw = EventWindowTimeModel::where('status', 'open')->limit(1)->select('id', 'name', 'start_date', 'end_date')->first();
        if ($raw) {
            $this->event_active = $raw->id;
            $this->event_name = $raw->name;
            $this->event_start = date('j M', strtotime($raw->start_date));
            $this->event_end = date('j M', strtotime($raw->end_date));
        }
    }

    public function index()
    {
        $position   = SubEvent::select(
            'master_position.name_position',
            'sub_event.status',
            'sub_event.id',
            'sub_event.sourcing_type',
            'master_subholding.name_subholding',
            'master_area.name_area',
            DB::raw('(SELECT name_position FROM master_position WHERE master_position.code_position=sub_event.position) AS name_position'),
            DB::raw('(SELECT count(distinct(nik)) FROM talent WHERE talent.sub_event=sub_event.id AND talent.status="selected" AND talent.is_review="1") AS participant'),
            DB::raw('(SELECT count(distinct(nik)) FROM talent_aspiration WHERE talent_aspiration.id_event=sub_event.id_event AND talent_aspiration.code_position=sub_event.position) AS total')

        )
            ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
            ->join('event_window_time', 'event_window_time.id', '=', 'sub_event.id_event')
            ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
            ->join('master_area', 'master_area.code_area', 'master_position.area')
            ->where([
                ['event_window_time.status', 'open'],
                ['sub_event.status', '=', 'selection']
            ])
            ->get();

        $page_title = 'Talent Selection';
        $page_description = 'Halaman Talent Selection (Career Cards)';
        $page_breadcrumbs = [
            ['page' => 'dashboard', 'title' => 'Home'],
            ['page' => 'talent-selection', 'title' => 'Talent Selection']
        ];

        $show_engage = $this->event_active != 0 ? false : true;

        return view('talent-selection.index', compact('page_title', 'page_description', 'page_breadcrumbs', 'position', 'show_engage'));
    }

    public function detail($id)
    {
        $sub_event  = SubEvent::select(
            'master_position.name_position',
            'master_position.code_position',
            'sub_event.id',
            'sub_event.id_event',
            'sub_event.sourcing_type',
            'master_subholding.name_subholding',
            'master_area.name_area'
        )
            ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
            ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
            ->join('master_area', 'master_area.code_area', 'master_position.area')
            ->where('sub_event.id', '=', $id)
            ->first();

        $year_end = date('Y') - 1;
        $year_start = $year_end - 3;

        $data   = Talent::select('talent.*', 'master_position_level.name_position_level', 'talent_aspiration.comitte_proposal')
            ->with(['employee' => function ($query) use ($year_start, $year_end) {
                $query->with(['performance' => function ($query) use ($year_end, $year_start) {
                    $query->whereBetween('year', [$year_start, $year_end]);
                }]);
                $query->with('master_posisi');
            }])->where([
                ['talent.sub_event', '=', $id],
                ['talent.status', '=', 'selected']
            ])
            ->leftJoin('employees', DB::raw('employees.prev_persno collate utf8mb4_general_ci'), '=', 'talent.nik')
            ->leftJoin('master_position', 'master_position.code_position', '=', DB::raw('employees.position collate utf8mb4_general_ci'))
            ->leftJoin('master_position_level', 'master_position_level.code_position_level', '=', 'master_position.code_position_level')
            ->join('talent_aspiration', 'talent.nik', '=', 'talent_aspiration.nik')
            ->groupBy('talent.nik')
            ->get();

        $page_title = 'Talent Selection';
        $page_description = 'Halaman Talent Selection (Career Cards)';
        $page_breadcrumbs = [
            ['page' => 'dashboard', 'title' => 'Home'],
            ['page' => 'talent-selection', 'title' => 'Talent Selection'],
            ['page' => '#', 'title' => 'Detail']
        ];

        return view('talent-selection.detail', compact('page_title', 'page_description', 'page_breadcrumbs', 'data', 'sub_event'));
    }

    //method untuk keperluan data pop up career card
    public function view_career_card($nik)
    {
        $data = data_career_card($nik);
        return view('talent-profiling.element.career_card_plain', $data);
    }

    public function submit_assessment(Request $request)
    {
        $insert = Talent::where('id', '=', $request->id)
            ->update([
                'performance'   => $request->performance,
                'cci'           => $request->assessment,
            ]);

        return json_encode(array(
            "status" => 200
        ));
    }

    public function store(Request $request)
    {

        if ($request->tag != null) {
            $talent   = $request->talent_id;
            $id_sub_event = $request->sub_event;

            foreach ($talent as $item) {
                //cari panel
                $get_talent = Talent::where('id', '=', $item)->first();

                $panel   = talentMapping($get_talent->cci, $get_talent->performance);


                // NOTIFIKASI
                $cek_prev_position = EmployeesToPositions::where('position_code', $get_talent->sub_events->position)->first();
                $prev_position = $get_talent->employee->position;
                if ($cek_prev_position != null) {
                    $prev_position = $cek_prev_position->position_code;
                }
                $next_position = $get_talent->sub_events->position;

                $cek_event_position = SubEvent::where('position', $prev_position)->where('id_event', $this->event_active)->first();
                if ($cek_event_position == null) {
                    // Posisi baru yang diisi oleh seleceted
                    $position_name = Position::where('code_position', $prev_position)->first()->name_position;
                    $new_position = Position::where('code_position', $next_position)->first()->name_position;

                    $position_sebelumnya = EmployeesToPositions::where('position_code', $next_position)->orderBy('id', 'desc')->first();

                    $new = new PendingTaskNotification();
                    $new->jenis_notifikasi = 'position';
                    $new->judul = $position_name . ' is Empty';
                    $new->deskripsi = $get_talent->employee->personnel_number . ' yang sebelumnya menjabat <strong>' . $position_name  . '</strong> telah terpilih untuk mengisi posisi ' . $new_position;
                    $new->status = 'pending';
                    $new->tgl_kosong = date('Y-m-d H:i:s');
                    $new->position = $prev_position;
                    $new->job_holder = $get_talent->employee->id;
                    $new->save();

                    try {
                        $user = Sentinel::check();
                        $nik = $user->nik;
                        $user->notify(new \App\Notifications\PendingTaskNotification($user, $new));
                    } catch (\Throwable $th) {
                        //throw $th;
                    }
                }

                // Cek Posisi apakah sudah ada yang menempati atau masih kosng
                $cek_position = EmployeesToPositions::where('position_code', $next_position)->orderBy('id', 'desc')->first();
                // dd($cek_position);
                if ($cek_position != null) {
                    if ($cek_position->nik != null) {
                        $employee_tergantikan = $cek_position->employee;

                        $notif_for_employee = new PendingTaskNotification();
                        $notif_for_employee->jenis_notifikasi = 'employee';
                        $notif_for_employee->judul = $employee_tergantikan->personnel_number . ' Needs Position';
                        $notif_for_employee->deskripsi = $employee_tergantikan->personnel_number . ' yang sebelumnya menjabat <strong>' . $cek_position->positions->name_position  . '</strong>, posisi nya telah diisi oleh ' . $get_talent->employee->personnel_number;
                        $notif_for_employee->status = 'pending';
                        $notif_for_employee->employee_id = $employee_tergantikan->id;
                        $notif_for_employee->prev_position = $employee_tergantikan->position;
                        $notif_for_employee->save();
                        // dd($notif_for_employee);

                        try {
                            $user = Sentinel::check();
                            $nik = $user->nik;
                            $user->notify(new \App\Notifications\PendingTaskNotification($user, $notif_for_employee));
                        } catch (\Throwable $th) {
                            //throw $th;
                        }
                    }
                }

                try {
                    $cek_status_pending_task = PendingTaskNotification::where('id_sub_event', $id_sub_event)->first();
                    if ($cek_status_pending_task != null) {
                        $cek_status_pending_task->status = 'done';
                        $cek_status_pending_task->save();
                    }
                } catch (\Throwable $th) {
                    //throw $th;
                }

                // END NOTIFIKASI

                // die();

                $update = Talent::where('id', '=', $item)
                    ->update([
                        // 'panel'       => $panel,
                        'is_final' => '1',
                        'status' => 'final'
                    ]);
            }

            // die();

            Talent::whereNotIn('id', $talent)
                ->where('sub_event', $id_sub_event)
                ->where('is_review', '1')
                ->where('status', 'selected')
                ->update([
                    'status' => 'terminated',
                    'is_final' => '1'
                ]);

            $sub_event  = SubEvent::where('id', '=', $request->sub_event)
                ->update([
                    'status' => 'completed'
                ]);
            // return redirect()->route('talent-selection.index')->with(["id_event" => $id_sub_event]);
            $year_end = date('Y') - 1;
            $year_start = $year_end - 3;

            $dataxx   = Talent::with(['employee' => function ($query) use ($year_start, $year_end) {
                $query->with(['performance' => function ($query) use ($year_end, $year_start) {
                    $query->whereBetween('year', [$year_start, $year_end]);
                }]);
            }])
                ->join('sub_event', 'talent.sub_event', 'sub_event.id')
                ->join('master_position', 'sub_event.position', 'master_position.code_position')
                ->where([
                    ['talent.sub_event', '=', $id_sub_event],
                    ['talent.status', '=', 'selected']
                ])->whereIn('talent.id', $talent)->get();


            $data = [];
            foreach ($dataxx as $key => $v) {
                $data = [
                    'type'          => 'talent_selection',
                    'prefix'        => genderPrefix($v->employee->gender_text),
                    'sub_prefix'    => genderSubprefix($v->employee->gender_text),
                    'nama' => $v->employee->personnel_number,
                    'position' => $v->name_position,
                    'emails' => $v->employee->email,
                ];
                // sendmail($data);
            }
            if (count($data) > 0) {
                // send çall mail in the queue.
                $job = (new SendBulkQueueEmail($data))->delay(now()->addSeconds(2));
                dispatch($job);

                $msg = "!";
            } else {
                $msg = "Tidak ada perseta yang eligible. Tidak ada email yang akan dikirim untuk mengundang peserta.";
            }

            return redirect()->route('talent-selection.index')->with([
                "id_event" => $id_sub_event,
                "status" => "success",
                "title" => "Berhasil",
                "message" => "Peserta Telah terpilih" . $msg
            ]);
        } else {

            $sub_event_id = $request->sub_event;
            $sub_event  = SubEvent::select('master_position.name_position', 'master_position.code_position', 'master_position.code_job_family', 'sub_event.id', 'sub_event.id_event')
                ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
                ->where('sub_event.id', '=', $sub_event_id)
                ->first();
            $talent   = $request->talent_id;
            foreach ($talent as $item) {
                //cari panel
                $get_talent = Talent::where('id', '=', $item)->first();
                $panel   = talentMapping($get_talent->cci, $get_talent->performance);
            }
            $year_end = date('Y') - 1;
            $year_start = $year_end - 3;

            $data   = Talent::with(['employee' => function ($query) use ($year_start, $year_end) {
                $query->with(['performance' => function ($query) use ($year_end, $year_start) {
                    $query->whereBetween('year', [$year_start, $year_end]);
                }]);
                $query->with('master_posisi');
            }])->with('event')->with('employee_to_position')->where([
                ['talent.sub_event', '=', $sub_event_id],
                ['talent.status', '=', 'selected']
            ])
                ->with(["box" => function ($query) {
                    $query->with("box_cat");
                }])->whereIn('id', $talent)
                ->get()->transform(function ($value, $key) use ($sub_event_id) {
                    $value->eqs = (int) substr(nilai_eqs($value->employee->prev_persno, false, $sub_event_id), 0, 3);
                    return $value;
                })->sortByDesc('eqs');

            $page_title = 'Talent Comparison';
            $page_description = 'Halaman Talent Comparison (Career Cards)';
            $page_breadcrumbs = [
                ['page' => 'dashboard', 'title' => 'Home'],
                ['page' => 'talent-selection', 'title' => 'Talent Selection'],
                ['page' => 'talent-selection', 'title' => 'Detail Selection'],
                ['page' => '#', 'title' => 'Compare']
            ];

            return view('talent-selection.comparation', compact('page_title', 'page_description', 'page_breadcrumbs', 'data', 'sub_event'));
        }
    }

    public function GenerateBeritaAcara($id_sub_event)
    {
        $tc = data_ba_committee($id_sub_event);
        $nomor_ba = generate_no_ba("TS", $id_sub_event);

        $sub_event = DB::select("
            select talent.nik, 
            talent.talent_day_result as result,
            talent.panel,
            if(talent.is_change_box = '1', talent.proposed_box, talent.panel) as talent_cluster,
            employees.personnel_number,
            employees.prev_persno,
            employees.position_name,
            employees.position as kode_posisi,
            sub_event.talent_com_1,
            sub_event.talent_com_2,
            sub_event.talent_com_3,
            sub_event.position,
            sub_event.id as id_sub_event,
            master_position.name_position,
            master_position.code_subholding,
            master_subholding.name_subholding,
            ba_event.no_ba,
            talent.id as id_talent
            from talent 
            left join ba_event on ba_event.id_sub_event = talent.sub_event
            left join employees on employees.prev_persno collate utf8mb4_unicode_ci = talent.nik
            left join sub_event on talent.sub_event = sub_event.id
            left join master_position on master_position.code_position = sub_event.position
            left join master_subholding on master_subholding.code_subholding = master_position.code_subholding
            where talent.sub_event = '" . $id_sub_event . "' and ba_event.prefix = 'TS' and talent.status = 'final'
        ");
        //  return view('talent-selection.berita-acara', compact('sub_event','nomor_ba','tc'));        
        
        $exist = Ba_event::where('prefix', 'TS')->where('id_sub_event', $id_sub_event)->where('tanggal_ba', date("Y-m-d"))->first();
        if ($exist != null) {
            Ba_event::where('id', $exist->id)
                ->update([
                    'data_ba' => json_encode($sub_event),
                    'data_commite' => json_encode($tc)
                ]);
                $pdf = PDF::loadView('talent-selection.berita-acara', compact('sub_event', 'nomor_ba', 'tc'));
                return $pdf->download('BERITA ACARA KESEPAKATAN PENUNJUKKAN SELECTED CANDIDATE.pdf');
        }else{
            return redirect()->route('talent-selection.index');
        }
        
    }

    public function addFlagging(Request $request)
    {
        $talent = Talent::find($request->id);
        $aspiration_raw = TalentAspiration::join('master_position', 'master_position.code_position', '=', 'talent_aspiration.code_position')
            ->join('master_subholding', 'master_position.code_subholding', 'master_subholding.code_subholding')
            ->leftJoin('master_division', 'master_division.code_division', 'master_position.code_division')
            ->where('nik', $talent->employee->prev_persno)
            ->where("id_event", $this->event_active)
            ->groupBy('talent_aspiration.code_position')
            ->select('talent_aspiration.code_position', 'name_position', 'name_division', 'grade_min', 'grade_align_min', 'org_level', 'grade_align_notes', 'name_subholding')
            ->get();

        $aspiration = [];
        $person_grade = $talent->employee->person_grade_align;
        foreach ($aspiration_raw as $value) {
            $aspiration[] = [
                'code_position' => $value->code_position,
                'name_position' => $value->name_position,
                'name_division' => $value->name_division,
                'name_subholding' => $value->name_subholding,
                'aspiration_type' => TalentAspiration::where('code_position', $value->code_position)->where('nik', $talent->employee->prev_persno)->select('aspiration_type')->groupBy('aspiration_type')->get()->implode('aspiration_type', ', '),
                'grade_align' => $value->grade_align_min ? $value->grade_align_min : "",
                'aspiration_level' => ($value->grade_align_min > $person_grade ? 'Vertical' : ($value->grade_align_min == $person_grade ? 'Horizontal' : '')),
                'total' => TalentAspiration::where('code_position', $value->code_position)->select(DB::raw('count(distinct(nik)) AS total'))->value('total')
            ];
        }

        if (count($aspiration) <= 2) {
            $nik = $talent->employee->prev_persno;
            $query_person_grade = Employee::with('positions')->where('prev_persno', $nik)->first();
            $person_grade = (is_null($query_person_grade->person_grade_align)) ? $query_person_grade->positions->grade_align_min : $query_person_grade->person_grade_align;
            $list_job_family_employee = $this->list_job_family_employee($nik);
            $positions_by_job_family = $this->grading_positions($person_grade, $query_person_grade->positions->code_job_family, $list_job_family_employee);
            $data['position_by_job_family'] = $positions_by_job_family;
        }

        $data['talent_aspiration'] = $aspiration;
        $data['talent'] = $talent;
        return view('talent-selection.modal_add_event', $data);
    }

    public function addEventPosition($nik, Request $request)
    {
        // dd($nik);
        $talent = Talent::find($request->talent_id);
        $position = Position::where('code_position', $request->code_position)->first();
        $sub_event = request('sub_event');
        $data = data_career_card($nik, $sub_event);
        $eqs = nilai_eqs([$nik], "all", $sub_event);
        if (!$sub_event) {
            echo "<script>console.log('Data Tidak Sesuai Mohon Diperiksa')</script>";
        }
        $data = array_merge(
            $data,
            $eqs,
            ['sub_event' => $sub_event],
            ['is_editable' => request('is_editable')],
        );

        $data['position'] = $position;
        $data['talents'] = $talent;

        if (request('is_talent_map')) {
            $data['data_range'] = current(boxProperties(false, $data['talent'][0]->panel));
        }

        return view('talent-selection.modal_event_position', $data);
    }

    public function storeEventPosition(Request $request)
    {

        if ($request->position == null) {
            return redirect()->back()->with([
                "status" => "error",
                "title" => "Error",
                "message" => "Data Posisi tidak boleh kosong"
            ]);
        }

        $talent = Talent::find($request->talent_id);
        $talent->is_disposisi = 1;
        $talent->save();

        $nik = Talent::find($request->talent_id)->employee->prev_persno;
        $event_id = EventWindowTimeModel::where('status', 'open')->where('status_input', 1)->first()->id;

        $sub_event = SubEvent::where('id_event', $this->event_active)->where('position', $request->position);
        if ($sub_event->count() > 0) {
            $data_talent['nik'] = $nik;
            $data_talent['sub_event'] = $sub_event->first()->id;
            $data_talent['status'] = 'selected';
            Talent::insert($data_talent);
            return redirect()->back()->with([
                "status" => "success",
                "title" => "Berhasil",
                "message" => "Employee telah di masukan kedalam event"
            ]);
        }

        $data[] = [
            'id_event' => $event_id,
            'code_position' => $request->position,
            'nik' => $nik,
            'aspiration_type' => 'individual',
            'comitte_proposal' => '1',
        ];


        TalentAspiration::where('id_event', $event_id)
            ->where('nik', $nik)
            ->where('aspiration_type', 'individual')->delete();
        $query_response = TalentAspiration::insert($data);

        $ewt = EventWindowTimeModel::whereIn('status', ['open'])->select('id', 'start_date', 'end_date')->first();
        $id_event = SubEvent::insertGetId([
            'id_event'      => $ewt->id,
            'position'      => $request->position,
            'name'          => $request->event_name,
            'status'        => 'sourcing',
            'vacant_type'   => 'vacant',
            'sourcing_type' => 0,
            'talent_id_ref_from_selection' => $request->talent_id
        ]);

        // UPdate status pending task
        try {
            $cek_pending_task = PendingTaskNotification::where('position', $request->position)->first();
            if ($cek_pending_task != null) {
                $cek_pending_task->status = 'on_process';
                $cek_pending_task->id_sub_event = $id_event;
                $cek_pending_task->save();
            }
        } catch (\Throwable $th) {
            //throw $th;
        }


        // Insert Eqs Dasar
        $eligible_employee = TalentAspiration::where('code_position', $request->position)->groupBy('nik')
            ->where('id_event', $ewt->id)->get();
        $load_eqs = [
            'employee' => $eligible_employee->pluck('nik')->toArray(),
            'position' => $request->position
        ];
        $eqs_employees = kalkulasi_eqs($load_eqs, false);
        array_walk($eqs_employees, function (&$value, $key) use ($id_event) {
            $value["sub_event"] = $id_event;
            return $value;
        });
        DB::table('employee_eqs')->insert($eqs_employees);

        // Insert Eligible Employee
        $data_talent = [];
        foreach ($eligible_employee as $key => $item) {
            $data_talent[$key]['nik'] = $item->nik;
            $data_talent[$key]['sub_event'] = $id_event;
            $data_talent[$key]['status'] = 'selected';
        }
        Talent::insert($data_talent);

        $position_qry = DB::select("select master_position.name_position, master_subholding.name_subholding from master_position
            left join master_subholding on master_subholding.code_subholding = master_position.code_subholding
            where master_position.code_position='" . $request->position . "'
        ");

        $kandidat_nama_jabatan = "";
        $kandidat_nama_subholding = "";

        if ($position_qry) {
            $kandidat_nama_jabatan = $position_qry[0]->name_position;
            $kandidat_nama_subholding = $position_qry[0]->name_subholding;
        }

        $periode_pengisian = date('d-m-Y', strtotime($ewt->start_date)) . ' s.d ' . date('d-m-Y', strtotime($ewt->end_date));
        $talent_data = TalentAspiration::join('employees', DB::raw('employees.prev_persno collate utf8mb4_unicode_ci'), '=', 'talent_aspiration.nik')
            ->where('code_position', $request->position)
            ->select(
                'nik',
                'email',
                DB::raw('(CASE WHEN gender_text="Male" THEN "Bapak" WHEN gender_text="Female" THEN "Ibu" ELSE "Saudara" END) prefix'),
                DB::raw('(CASE WHEN gender_text="Male" THEN "Bpk" WHEN gender_text="Female" THEN "Ibu" ELSE "Sdr" END) sub_prefix'),
                'personnel_number',
                'prev_persno',
            )
            ->groupBy('nik')
            ->get();

        $data = [];
        foreach ($talent_data as $value) {
            $data[] = [
                'nik' => $value['nik'],
                'emails' => $value['email'],
                'type' => 'update_profile',
                'prefix' => $value['prefix'],
                'sub_prefix' => $value['sub_prefix'],
                'nama' => $value['personnel_number'],
                'username' => $value['prev_persno'],
                'password' => 'itms2022',
                'periode_update' => $periode_pengisian,
                'kandidat_nama_jabatan' => $kandidat_nama_jabatan,
                'kandidat_nama_subholding' => $kandidat_nama_subholding,
                'url' => url('my-profile')
            ];
        }

        if (count($data) > 0) {
            // send all mail in the queue.
            $job = (new SendBulkQueueEmail($data))->delay(now()->addSeconds(2));
            dispatch($job);

            $msg = "Bulk mail send successfully in the background...";
        } else {
            $msg = "Tidak ada perseta yang eligible. Tidak ada email yang akan dikirim untung mengundang peserta.";
        }

        return redirect()->back()->with([
            "status" => "success",
            "title" => "Berhasil",
            "message" => "Event telah di buat dan data telah disimpan di database "
        ]);
    }


    private function grading_positions($person_grade, $code_job_family, $rekomendasi_by_job_family)
    {
        $target_job_family = array_unique(array_merge([$code_job_family], $rekomendasi_by_job_family));
        $target_job_family = array_map(function ($value) {
            return "'$value'";
        }, $target_job_family);
        $target_job_family = implode(", ", $target_job_family);
        $raw_eligible_position = DB::table('master_position')
            ->selectRaw("
            *, 
            IF (grade_align_min = $person_grade, 1, 0) as is_horizontal,
            IF (grade_align_min > $person_grade, 1, 0) as is_vertical,
            $person_grade as person_grade
        ")
            ->join("master_area", "master_area.code_area", "=", "master_position.area")
            ->join("master_subholding", "master_subholding.code_subholding", "=", "master_position.code_subholding")
            ->whereRaw("
            grade_align_min IS NOT NULL AND
            (CASE 
                WHEN CAST(grade_align_min as UNSIGNED) != 0 THEN (grade_align_min >= $person_grade AND grade_align_min <= ($person_grade+2))
            END) AND
            org_level IN ('BOD', 'BOD-1') AND
            master_position.code_position_type != 'TP-00002'
        ")
            ->when(!is_null($code_job_family), function ($query) use ($code_job_family, $target_job_family) {
                $query->whereRaw("
                master_position.code_job_family IN (
                    SELECT 
                        code_job_family_to
                    FROM 
                        master_risk_movement 
                    WHERE
                        code_job_family_from = '$code_job_family' AND
                        code_job_family_to IN ($target_job_family)
                    GROUP BY
                        master_risk_movement.code_job_family_to
                )
            ");
            })
            ->orderBy("master_area.name_area", "ASC")
            ->get()->toArray();

        $vertical_position = array_map(function ($value) {
            return ($value->is_vertical) ? (object) $value : null;
        }, $raw_eligible_position);

        $horizontal_position = array_map(function ($value) {
            return ($value->is_horizontal) ? (object) $value : null;
        }, $raw_eligible_position);

        return (object) array_merge(
            ['vertical' => array_values(array_filter($vertical_position))],
            ['horizontal' => array_values(array_filter($horizontal_position))],
        );
    }

    public function list_job_family_employee($nik)
    {
        $query_position_history = DB::table('master_position_history')
            ->where('nik', $nik)
            ->get();

        $allitems = new Collection();
        $code_job_family_align = [
            'code_job_family_align',
            'code_job_family_align_2',
            'code_job_family_align_3'
        ];
        foreach ($code_job_family_align as $label) {
            $jf = $query_position_history->groupBy($label)->map(function ($jf) {
                $list_start_menjabat = $jf->pluck('start_date');
                $list_akhir_menjabat = $jf->pluck('end_date');
                $tahun_awal = date("Y", strtotime($list_start_menjabat->first()));
                $condition_akhir_menjabat = (is_null($list_akhir_menjabat->last()) || $list_akhir_menjabat->last() == '0000-00-00') ? date("Y-m-d") : $list_akhir_menjabat->last();
                $tahun_akhir = date("Y", strtotime($condition_akhir_menjabat));
                return [
                    'higher_3year' => ($tahun_akhir - $tahun_awal >= 3) ? 1 : 0
                ];
            });
            $allitems = $allitems->mergeRecursive($jf);
        }

        $allitems = $allitems->forget("")->mapWithKeys(function ($value, $key) {
            return [$key => ['higher_3year' => is_array($value['higher_3year']) ? array_sum($value['higher_3year']) : $value['higher_3year']]];
        })->where('higher_3year', '!=', '0')->toArray();
        return array_keys($allitems);
    }
}
