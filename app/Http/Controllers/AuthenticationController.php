<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Sentinel;
use Session;
class AuthenticationController extends Controller
{
    private $page_title;
    private $page_description;
    private $page_brand;
    private $page_directory;
    private $page_style;

    public function __construct(){
        $this->page_title         = "Login Page";
        $this->page_description   = "Description for Login Page";
        $this->page_directory     = "auth.login";
        $this->page_style         = "style2";
        
        // $this->dummy_user();
        // $this->dummy_user_role();
    }

    public function dummy_user_role() {
        $user = Sentinel::findById(1);
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);

        $user = Sentinel::findById(2);
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);

        $user = Sentinel::findById(3);
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);

        $user = Sentinel::findById(4);
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);

        $user = Sentinel::findById(5);
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);

        $user = Sentinel::findById(6);
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);

        $user = Sentinel::findById(7);
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);

        $user = Sentinel::findById(8);
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);

        $user = Sentinel::findById(9);
        $role = Sentinel::findRoleBySlug('talent_committee');
        $role->users()->attach($user);
    }

    public function dummy_user() {
        $credentials = [
            'nik'    => 'superadmin',
            'password' => 'superadmin-itms2022',
        ];
        
        $user = Sentinel::registerAndActivate($credentials);

        $credentials = [
            'nik'    => 'admin',
            'password' => 'itms2022',
        ];
        
        $user = Sentinel::registerAndActivate($credentials);

        $credentials = [
            'nik'    => 'admin-ap1',
            'password' => 'itms2022',
        ];
        
        $user = Sentinel::registerAndActivate($credentials);

        $credentials = [
            'nik'    => 'admin-ap2',
            'password' => 'itms2022',
        ];
        
        $user = Sentinel::registerAndActivate($credentials);

        $credentials = [
            'nik'    => 'admin-twc',
            'password' => 'itms2022',
        ];
        
        $user = Sentinel::registerAndActivate($credentials);

        $credentials = [
            'nik'    => 'admin-hin',
            'password' => 'itms2022',
        ];
        
        $user = Sentinel::registerAndActivate($credentials);

        $credentials = [
            'nik'    => 'admin-itdc',
            'password' => 'itms2022',
        ];
        
        $user = Sentinel::registerAndActivate($credentials);

        $credentials = [
            'nik'    => 'admin-snh',
            'password' => 'itms2022',
        ];
        
        $user = Sentinel::registerAndActivate($credentials);

        $credentials = [
            'nik'    => 'executive',
            'password' => 'itms2022',
        ];
        
        $user = Sentinel::registerAndActivate($credentials);


        // $credentials = [
        //     'nik'    => 'employee',
        //     'password' => 'itms2022',
        // ];

        // $user = Sentinel::registerAndActivate($credentials);

        // $credentials = [
        //     'nik'    => 'interviewer',
        //     'password' => 'itms2022',
        // ];

        // $user = Sentinel::registerAndActivate($credentials);

        // $credentials = [
        //     'nik'    => '67943845',
        //     'password' => 'itms2022',
        // ];
        
        // $user = Sentinel::registerAndActivate($credentials);
    }

    protected function rules()
    {
        return [
            'token' => 'required',
            'nik' => 'required|nik',
            'password' => 'required',
        ];
    }

    public function index() {

        $page_title       = $this->page_title;
        $page_description = $this->page_description;
        $page_directory   = $this->page_directory;
        $page_style       = $this->page_style;

        return view('auth.index', compact('page_title','page_description','page_directory','page_style'));
    }

    public function login_validate(Request $request) {
        
        if (filter_var($request->nik, FILTER_VALIDATE_EMAIL)) {
            $login_using = 'Email';

            $credentials = array(
                'email'    => $request->nik,
                'password' => $request->password,
            );
        } else {
            $login_using = 'NIK';

            $credentials = array(
                'nik'    => $request->nik,
                'password' => $request->password ?? "",
            );
        }
        $remember = $request->remember == 'On' ? true : false;

        try {
            if ($user = Sentinel::authenticate($credentials, $remember)) {
                Session::flash('success', __('Sukses'));

                if (Sentinel::inRole('talent_committee') && Sentinel::inRole('employee')) {
                    session()->put('role', '');
                    return redirect('/role-switch');
                }

                session()->put('role', Sentinel::getRoles()[0]->slug);

                return redirect('/dashboard');
                
            } else {
                Session::flash('failed', __($login_using.' atau Password tidak sesuai'));
                
                return redirect()->route('auth.login');
            }
        } catch (ThrottlingException $ex) {
            $message = $ex->getMessage();
            Session::flash('timeout', __($message));
            
            return redirect()->route('auth.login');
            
        } catch (NotActivatedException $ex) {
            Session::flash('failed', __('auth.login_unsuccessful_not_active'));
            
            return redirect()->route('auth.login');
        }
    }
    
    public function logout() {
        Session::flush();

        Sentinel::logout(null, true);
        
        Session::flash('success', __('Berhasil Logout'));
        
        return redirect()->route('auth.login');
    }
}
