<?php

namespace App\Http\Controllers;

use App\Models\AssessmentCompetencyEmployee;
use App\Models\Directorate;
use App\Models\Division;
use App\Models\SubEvent;
use App\Models\Employee;
use App\Models\EmployeesToPositions;
use Sentinel;
use App\Models\EventWindowTimeModel;
use App\Models\Group;
use App\Models\PendingTaskNotification;
use App\Models\Position;
use App\Models\PositionType;
use App\Models\SubHolding;
use App\Models\Talent;
use App\Models\TalentAspiration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Svg\Tag\Rect;
use Yajra\Datatables\Datatables;

class DashboardController extends Controller
{
    private $event_active = 0;
    private $event_name = "";
    private $event_start = [];
    private $event_end = [];
    private $event_position_eligible = [];
    private $event_employee_eligible = [];

    public function __construct() {
        $raw = EventWindowTimeModel::where('status', 'open')->limit(1)->select('id', 'name', 'start_date', 'end_date')->first();
        if($raw) {
            $this->event_active = $raw->id;
            $this->event_name = $raw->name;
            $this->event_start = date('j M', strtotime($raw->start_date));
            $this->event_end = date('j M', strtotime($raw->end_date));
        }
    }

    public function index() {
        $level_akses = Session::get('role');
        
        switch($level_akses) {
            case 'admin':
                return $this->dashboard_admin($level_akses);
            break;
            
            case 'talent_committee':
                return $this->dashboard_executive($level_akses);
            break;
            
            case 'employee':
                return $this->dashboard_employee($level_akses);
            break;
            
            case 'executive':
                return $this->dashboard_executive($level_akses);
            break;

            default:
                abort('403');
            break;
        }
    }

    public function dashboard_admin($level_akses){
        $page_title = 'Dashboard';
        $page_description = 'Dashboard';
        
        $event_name = $this->event_name;
        $event_start = $this->event_start;
        $event_end = $this->event_end;
        $show_engage = $this->event_active != NULL ? false : true;
        $is_completed_step_1 = $this->event_active == NULL ? false : true;
        $is_completed_step_2 = count($this->event_position_eligible) == 0 ? false : true;
        $is_completed_step_3 = count($this->event_employee_eligible) == 0 ? false : true;

        $subholding = SubHolding::get();
        // Pemenuhan SDM 
        $pemenuhan_sdm = $this->data_pemenuhan_sdm();
        $option_pemenuhan_sdm = $this->data_pemenuhan_sdm('option');
        // competency member
        $top_strong_competency = $this->assessment_employee();
        $top_weak_competency = $this->assessment_employee("ASC");
        $type_position = PositionType::get();
        $boxes = collect(boxProperties($is_generate=true,$box=null));

        $idx = EventWindowTimeModel::get()->pluck('id')->toArray();

        $pending_task = PendingTaskNotification::where('status','pending')->orderBy('created_at','desc')->take(2)->get();
        
        $history_event_position = SubEvent::select(
            'sub_event.*',
            'master_position.name_position',
            'master_position.code_subholding',
            'master_division.name_division'
        )
        ->leftJoin('master_position', 'master_position.code_position', '=', 'sub_event.position')
        ->leftJoin('master_division', 'master_position.code_division', '=', 'master_division.code_division')
        ->whereIn('sub_event.id_event', $idx)
        ->withCount('talent')
        ->get();

        return view('dashboard.index', compact('level_akses',
            'page_title',
            'page_description', 
            'event_name',
            'event_start',
            'event_end',
            'show_engage', 
            'type_position',
            'is_completed_step_1',
            'is_completed_step_2',
            'is_completed_step_3',
            'subholding',
            'boxes',
            'history_event_position',
            'pemenuhan_sdm',
            'option_pemenuhan_sdm',
            'top_strong_competency',
            'top_weak_competency',
            'pending_task'
        ));
    }

    public function dashboard_employee($level_akses){
        $page_title = 'Dashboard';
        $page_description = 'Dashboard';
        
        $show_engage = $this->event_active != 0 ? false : true;
        $is_completed_step_1 = $this->event_active == 0 ? false : true;
        $is_completed_step_2 = count($this->event_position_eligible) == 0 ? false : true;
        $is_completed_step_3 = count($this->event_employee_eligible) == 0 ? false : true;

        $pending_task = [];
        try {
            $pending_task = PendingTaskNotification::where('status','pending')->where('employee_id',Sentinel::getUser()->employee->id)->get();
        } catch (\Throwable $th) {
            //throw $th;
        }
        

        $nik = Sentinel::getUser()->nik;
        $talent = TalentAspiration::join('event_window_time', 'talent_aspiration.id_event', 'event_window_time.id')
        ->leftJoin('sub_event', function($join){
            $join->on('sub_event.id_event', 'talent_aspiration.id_event');
            $join->on('sub_event.position', 'talent_aspiration.code_position');
        })
        ->leftJoin('talent', 'talent_aspiration.nik', 'talent.nik')
        ->leftJoin('master_position', 'talent_aspiration.code_position', 'master_position.code_position')
        ->leftJoin('master_subholding', 'master_position.code_subholding', 'master_subholding.code_subholding')
        ->where('talent_aspiration.nik', $nik)
        ->whereNotIn('talent_aspiration.id_event', [$this->event_active])
        ->select(
            'sub_event.name AS name_event',
            'master_position.name_position',
            'master_subholding.name_subholding',
            'sub_event.vacant_type',
            'event_window_time.start_date',
            'event_window_time.end_date',
            DB::raw('(CASE WHEN is_final="1" THEN (CASE WHEN talent.status="final" THEN "Selected" ELSE "Talent Selection" END) '.
                'WHEN is_review="1" THEN (CASE WHEN talent.status="selected" THEN "Talent Review [Selected]" ELSE "Talent Review" END) '.
                'WHEN is_day="1" THEN (CASE WHEN talent.status="selected" THEN "Talent Day [Selected]" ELSE "Talent Day" END) '.
                'WHEN is_mapping="1" THEN (CASE WHEN talent.status="selected" THEN "Talent Mapping [Selected]" ELSE "Talent Mapping" END) '.
                'WHEN is_profilling="1" THEN (CASE WHEN talent.status="selected" THEN "Talent Profiling [Selected]" ELSE "Talent Profiling" END) '.
                'WHEN talent.nik IS NOT NULL THEN "Talent Sourcing [Selected]" '.
                'WHEN sub_event.id IS NULL THEN "-" '.
                'ELSE "Talent Sourcing" END) AS proses'),
        )
        ->distinct()
        ->get();

        $aspiration_raw= TalentAspiration::join('master_position', 'master_position.code_position', '=','talent_aspiration.code_position')
            ->join('master_subholding', 'master_position.code_subholding', 'master_subholding.code_subholding')
            ->leftJoin('master_division', 'master_division.code_division', 'master_position.code_division')
            ->leftJoin('master_area', 'master_area.code_area', 'master_position.area')
            ->where('nik', $nik)
            ->where("id_event", $this->event_active)
            ->groupBy('talent_aspiration.code_position')
            ->select('talent_aspiration.code_position' ,'name_position', 'name_area', 'grade_min', 'grade_align_min','org_level','grade_align_notes', 'name_subholding')
            ->get();

        $aspiration = [];
        $paramter_arr = [];
        $person_grade = Sentinel::getUser()->employee->person_grade_align;
        foreach ($aspiration_raw as $value) {
            $arr_nik = TalentAspiration::where('code_position', $value->code_position)->pluck('nik')->toArray();
            
            foreach($arr_nik as $k => $v) {
                $paramter_arr = [
                    'employee' => [$v],
                    'position' => $value->code_position
                ];
                $aspiration_arr[$v] = get_eqs_default_employees($paramter_arr,'eqs_only');
            }

            asort($aspiration_arr);
            $ranking    = 1;
            $my_ranking = 0;

            foreach($aspiration_arr as $key => $rank ) {
                if ($nik == $key) {
                    $my_ranking = $ranking;
                }
                $ranking++;
                
            }

            $paramter = [
                'employee' => [$nik],
                'position' => $value->code_position
            ];
            $aspiration[] = [
                'name_position' => $value->name_position,
                'name_area' => $value->name_area,
                'name_subholding' => $value->name_subholding,
                'aspiration_type' => TalentAspiration::where('code_position', $value->code_position)->where('nik', $nik)->select('aspiration_type')->groupBy('aspiration_type')->get()->implode('aspiration_type', ', '),
                'grade_align' => $value->grade_align_min ? $value->grade_align_min : "",
                'aspiration_level' => ($value->grade_align_min > $person_grade ? 'Vertical' : ($value->grade_align_min == $person_grade ? 'Horizontal' : '')),
                'total' => TalentAspiration::where('code_position', $value->code_position)->select(DB::raw('count(distinct(nik)) AS total'))->value('total'),
                'nilai_eqs' => get_eqs_default_employees($paramter),
                'my_ranking'    => $my_ranking,
                'nik'           => $nik,
                'code_position'  => $value->code_position
            ];
        }

        $boxes = collect(boxProperties($is_generate=true,$box=null));

        $paramter = [
            'employee' => [$nik],
            'position' => Sentinel::getUser()->employee->position
        ];

        $cluster = get_eqs_default_employees($paramter, 'box');
        $eqs_employee = get_eqs_default_employees($paramter);
        return view('dashboard.index', compact('level_akses','page_title', 'page_description', 
            'show_engage', 
            'is_completed_step_1',
            'is_completed_step_2',
            'is_completed_step_3',
            'aspiration',
            'boxes',
            'cluster',
            'talent',
            'pending_task',
            'eqs_employee'
        ));
    }

    public function dashboard_lm($level_akses){
        $page_title = 'Dashboard';
        $page_description = 'Dashboard';
        
        $show_engage = $this->event_active != 0 ? false : true;
        $is_completed_step_1 = $this->event_active == 0 ? false : true;
        $is_completed_step_2 = count($this->event_position_eligible) == 0 ? false : true;
        $is_completed_step_3 = count($this->event_employee_eligible) == 0 ? false : true;

        return view('dashboard.index', compact('level_akses','page_title', 'page_description', 
            'show_engage', 
            'is_completed_step_1',
            'is_completed_step_2',
            'is_completed_step_3'
        ));
    }

    public function dashboard_talent_committee($level_akses){
        $page_title = 'Dashboard';
        $page_description = 'Dashboard';
        
        $show_engage = $this->event_active != 0 ? false : true;
        $is_completed_step_1 = $this->event_active == 0 ? false : true;
        $is_completed_step_2 = count($this->event_position_eligible) == 0 ? false : true;
        $is_completed_step_3 = count($this->event_employee_eligible) == 0 ? false : true;
        $list_subholding = DB::table('master_subholding')->get();

        return view('dashboard.index', compact('level_akses','page_title', 'page_description', 
            'show_engage', 
            'is_completed_step_1',
            'is_completed_step_2',
            'is_completed_step_3',
            'list_subholding'
        ));
    }

    public function dashboard_executive($level_akses){
        // Default Init
        $page_title = 'Dashboard';
        $page_description = 'Dashboard';
        $show_engage = $this->event_active != 0 ? false : true;
        $event_id = $this->event_active;
        // Monitoring Human Capital KPI

        $subholding = SubHolding::get();

        $req = (object) [
            'filterTalentCluster' => ['5', '7', '8', '9']
        ];

        $raw_data = query_employees($req);

        // return $raw_data;

        $data_kpi = array_map(function ($value) use ($raw_data) {
            $all_employees = get_employee_subholding($value['code_subholding']);
            $total_employee = $all_employees->count();
            $code_subholding = $value['code_subholding'];
            $total_nominated = $raw_data->filter(function ($value) use ($code_subholding) {
                return $value->code_subholding == $code_subholding;
            })->count();
            $nominated_female = $raw_data->filter(function($value) use ($code_subholding) {
                return (strtolower($value->gender_text) == "perempuan" | strtolower($value->gender_text) == "female") && $value->code_subholding == $code_subholding;
            })->count();
            $nominated_milenial = $raw_data->filter(function ($value) use ($code_subholding) {
                return date('Y', strtotime($value->date_of_birth)) > '1995' && $value->code_subholding == $code_subholding;
            })->count();
            if($code_subholding == "inj") {
                $persen_perempuan = 29;
                $persen_milenial = 31;
                $persen_itms = 80;
            } else if ($code_subholding == "ap1") {
                $persen_perempuan = 10;
                $persen_milenial = 19;
                $persen_itms = 80;
            } else if ($code_subholding == "ap2") {
                $persen_perempuan = 15;
                $persen_milenial = 20;
                $persen_itms = 80;
            } else if ($code_subholding == "hin") {
                $persen_perempuan = 33;
                $persen_milenial = 33;
                $persen_itms = 80;
            } else if ($code_subholding == "twc") {
                $persen_perempuan = 40;
                $persen_milenial = 40;
                $persen_itms = 80;
            } else if ($code_subholding == "snh") {
                $persen_perempuan = 25;
                $persen_milenial = 25;
                $persen_itms = 80;
            } else if ($code_subholding == "itdc") {
                $persen_perempuan = 50;
                $persen_milenial = 50;
                $persen_itms = 80;
            }
            return [
                'code_subholding' => $code_subholding,
                'name_subholding' => $value['name_subholding'],
                'logo' => $value['logo'],
                'total_nominated' => $total_nominated,
                'nominated_female' => $nominated_female,
                'nominated_milenial' => $nominated_milenial,
                'total_employees' => $total_employee,
                'employees_penugasan' => $all_employees->whereNotIn('status_aktif', ['aktif'])->count(),
                'persen_perempuan' => $persen_perempuan,
                'persen_milenial' => $persen_milenial,
                'persen_itms' => $persen_itms
            ];
        }, $subholding->toArray());

        // return $data_kpi;
        
        // Pemenuhan SDM 
        $pemenuhan_sdm = $this->data_pemenuhan_sdm();
        $option_pemenuhan_sdm = $this->data_pemenuhan_sdm('option');
        // competency member
        $top_strong_competency = $this->assessment_employee();
        $top_weak_competency = $this->assessment_employee("ASC");
        // Boxed
        $boxes = collect(boxProperties($is_generate=true,$box=null));
        $list_subholding = DB::table('master_subholding')->get();

        return view('dashboard.index', compact('level_akses',
            'show_engage',
            'page_title',
            'page_description', 
            'subholding',
            'list_subholding',
            'boxes',
            'top_strong_competency',
            'top_weak_competency',
            'pemenuhan_sdm',
            'option_pemenuhan_sdm',
            'data_kpi'
        ));
    }
    
    public function get_vacant_table(Request $request) {
        $data = EmployeesToPositions::join(DB::raw('(SELECT max(id) as id FROM employees_to_positions GROUP BY position_code) LatestPosition'), function($join){
            $join->on('employees_to_positions.id', '=', 'LatestPosition.id');
        })
            ->join('master_position', 'employees_to_positions.position_code', '=', 'master_position.code_position')
            ->join('master_subholding', 'master_subholding.code_subholding','=','master_position.code_subholding')
            ->join('master_position_type', 'master_position_type.code_position_type','=','master_position.code_position_type')
            ->where(DB::raw("TIMESTAMPDIFF(MONTH,start_date,end_date)"), '<=' , '12')
            ->orwhere('nik', null)
            ->select(
                'name_position_type',
                'position_code', 
                'name_position',
                'name_subholding',
                DB::raw("(CASE WHEN (TIMESTAMPDIFF(MONTH,start_date,end_date) <= 12 AND TIMESTAMPDIFF(MONTH,start_date,end_date) > 0) THEN 'Vacant Soon' WHEN TIMESTAMPDIFF(MONTH,start_date,end_date) < 1 | nik IS NULL THEN 'Vacant Position' END) AS status")
            )
            ->when($request->position_type != "", function($query) use($request) {
                $query->whereHas('positions', function($subquery) use($request) {
                    $subquery->where('code_position_type', $request->position_type);
                });
            })
            // ->havingRaw('status IS NOT NULL')
            ->limit(5)
            ->get();
        $result = "";
        foreach ($data as $value) {
            $result .= "<tr id='". $value->position_code ."'>";
            $result .= "<td>". $value->name_position ."</td>";
            $result .= "<td>". $value->name_subholding ."</td>";
            $result .= "<td>". $value->status ."</td>";
            $result .= "<td>". $value->name_position_type ."</td>";
            $result .= "</tr>";
        }
        return $result;
    }

    public function top_desired_position_chart(Request $request) {
        $subholding = $request->subholding;
        $position = TalentAspiration::selectRaw("*, count(id) as jumlah")
            ->with('positions')
            ->when(!empty($subholding), function($query) use($subholding){
                $query->whereHas('positions', function($subquery) use($subholding) {
                    return $subquery->where('code_subholding', $subholding);
                });
            })
            ->groupBy('code_position')
            ->orderBy('jumlah', 'desc')
            ->limit(10)
            ->get()->map(function($value) {
                return [
                    'name' => $value->positions->name_position,
                    'y' => $value->jumlah
                ];
            });
        return response()->json($position, 200);
    }

    public function top_desired_position_datatable(Request $request) {
        $subholding = $request->subholding;
        $data = TalentAspiration::selectRaw("*, count(id) as jumlah")
            ->with(['positions' => function($query) {
                $query->with('division')
                    ->with('directorate')
                    ->with('group')
                    ->with('sub_holding');
            }])
            ->when(!empty($subholding), function($query) use($subholding){
                $query->whereHas('positions', function($subquery) use($subholding) {
                    return $subquery->where('code_subholding', $subholding);
                });
            })
            ->groupBy('code_position')
            ->orderBy('jumlah', 'desc')
            ->limit(10)
            ->get()->map(function($value) {
                return [
                    'position' => $value->positions->name_position ?? "-",
                    'directorate' => $value->positions->sub_holding->name_subholding ?? "-",
                    'job_family_align' => $value->positions->job_family_align() ?? "-",
                    'job_grade_align' => $value->positions->grade_align_min ?? "-",
                    'count' => $value->jumlah
                ];
            });
        return Datatables::of($data)
        ->addIndexColumn()
        ->editColumn('job_family_align', function($data) {
            return $data['job_family_align'];
        })
        ->rawColumns(['job_family_align'])
        ->make(true);
    }

    public function organization_diagram(Request $request) {
        $where = [
            'subholding' => $request->subholding,
            'code_directorate' => $request->code_directorate
        ];
        return response()->json(generate_diagram_organization($where), 200);
    }


    public function employee_demograph(Request $request) {
        $subholding = $request->subholding;
        // Subholding Condition
        $condition_subholding = "";
        if (!is_null($subholding) && !empty($subholding)) {
            $condition_subholding = "AND code_subholding='$subholding'";
        }
        $patokan_umur_x = "CEIL(DATEDIFF(NOW(), '1980-1-1') / 365.25)";
        $patokan_umur_z = "CEIL(DATEDIFF(NOW(), '1996-1-1') / 365.25)";
        // Male
        // dd($patokan_umur_x, $patokan_umur_z);
        $where_male = "(LOWER(gender_text) = 'male' OR LOWER(gender_text) = 'laki-laki')";
        // Gen X
        $query[] = "(SELECT count(id) FROM employees WHERE $where_male AND ((DATEDIFF(NOW(), date_of_birth) / 365.25) > $patokan_umur_x) $condition_subholding) as male_gen_x";
        // Gen Y
        $query[] = "(SELECT count(id) FROM employees WHERE $where_male AND ((DATEDIFF(NOW(), date_of_birth) / 365.25) BETWEEN $patokan_umur_z AND $patokan_umur_x) $condition_subholding) as male_gen_y";
        // Gen Z
        $query[] = "(SELECT count(id) FROM employees WHERE $where_male AND ((DATEDIFF(NOW(), date_of_birth) / 365.25) < $patokan_umur_z) $condition_subholding) as male_gen_z";
    
        // Female
        $where_female = "(LOWER(gender_text) = 'female' OR LOWER(gender_text) = 'perempuan')";
        // Gen X
        $query[] = "(SELECT count(id) FROM employees WHERE $where_female AND ((DATEDIFF(NOW(), date_of_birth) / 365.25) > $patokan_umur_x) $condition_subholding) as female_gen_x";
        // Gen Y
        $query[] = "(SELECT count(id) FROM employees WHERE $where_female AND ((DATEDIFF(NOW(), date_of_birth) / 365.25) BETWEEN $patokan_umur_z AND $patokan_umur_x) $condition_subholding) as female_gen_y";
        // Gen Z
        $query[] = "(SELECT count(id) FROM employees WHERE $where_female AND ((DATEDIFF(NOW(), date_of_birth) / 365.25) < $patokan_umur_z) $condition_subholding) as female_gen_z";
    
        // Ekstra
        $query[] = "0 as total_male, 0 as total_female";

        $exclude_pendidikan = ['SLTP', 'SD'];
        $card_pendidik = DB::table('master_education_level')->whereNotIn('code', $exclude_pendidikan)->get()->pluck('code')->toArray();
        $external_pendidikan = [
            'TNI',
            'Lainnya'
        ];
        $card_pendidik = array_merge($card_pendidik, $external_pendidikan);
        $card_pendidik = collect($card_pendidik)->mapWithKeys(function($value) {
            return [$value => 0];
        });
        $urutan_pendidikan = [
            "SLTA",
            "D.I",
            "D.II",
            "D.III",
            "D.IV",
            "S.1",
            "S.2",
            "S.3",
            "Lainnya",
        ];

        $kondisi_education = "
        (CASE
            WHEN education_level = 'SIII' OR education_level = 'S.III' OR education_level = 'S3' THEN 'S.3'
            WHEN education_level = 'SII' OR education_level = 'S.II' OR education_level = 'S2' THEN 'S.2'
            WHEN education_level = 'SI' OR education_level = 'S.I' OR education_level = 'S1' THEN 'S.1'
            WHEN education_level = 'DIV' OR education_level = 'D4' THEN 'D.IV'
            WHEN education_level = 'DIII' OR education_level = 'D3' THEN 'D.III'
            WHEN education_level = 'DII' OR education_level = 'D2' THEN 'D.II'
            WHEN education_level = 'DI' OR education_level = 'D1' THEN 'D.I'
            WHEN education_level = 'SMA' THEN 'SLTA'
            WHEN education_level IN ('SLTA', 'D.I', 'D.II', 'D.III', 'D.IV', 'S.1', 'S.2', 'S.3') THEN education_level
            ELSE 'Lainnya'
        END)
        ";

        $card_pendidikan = DB::table('employees')
            ->selectRaw("$kondisi_education as education_level, code_subholding")
            ->orderByRaw("FIELD(education_level, 'SLTA', 'D1', 'D2', 'D3', 'D4', 'D1', 'D2', 'D3', 'TNI', 'Lainnya')")
            ->whereNotNull('gender_text')
            ->whereNull('employees.deleted_at')
            ->when(!empty($subholding), function($v) use ($subholding) {
                return $v->where("code_subholding", $subholding);
            })
            ->get();

        if (empty($subholding)) {
            $data_card = $card_pendidikan->groupBy("education_level")->map(function($value) use($card_pendidik) {
                return $value->count();
            })->sortBy( function($key) use ($urutan_pendidikan) {
                return array_search($key, $urutan_pendidikan);
            })->toArray();
            $card_pendidikan = ['all_data' => $data_card];
        } else {
            $card_pendidikan = $card_pendidikan->groupBy(['code_subholding'])->map(function($value) use($card_pendidik) {
                return $card_pendidik->mapWithKeys(function($x, $key) use($value) {
                    return [
                        $key => 
                        $value->where('education_level', $key)->count() 
                    ];
                });
            })->sortBy( function($key) use ($urutan_pendidikan) {
                return array_search($key, $urutan_pendidikan);
            })->toArray();
        }

        $background_color = [
            'SLTA' => '#55adff',
            'D.I' => '#62d0e9',
            'D.II' => '#57dbab',
            'D.III' => '#cad172',
            'D.IV' => '#f6c953',
            'S.1' => '#ffaf66',
            'S.2' => '#fd9c72',
            'S.3' => '#fc6e6e'
        ];

        $card = "";
        $subholding = ($subholding == "") ? 'all_data' : $subholding;

        foreach ($card_pendidikan[$subholding] as $key => $item) {
            if (in_array($key, $external_pendidikan) && $item < 1) {
                continue;
            }
            $color = $background_color[$key] ?? $background_color[array_rand($background_color)];
            $card .= "
            <div class='col-xl-3 col-md-12 col-12'>
                <div class='card card-custom card-stretch gutter-b' style='background-color:$color'>
                    <div class='card-body d-flex p-4'>
                        <div class='d-flex flex-column'>
                            <h4 class='text-white'>".str_replace('.', '', $key)."</h4>
                            <h2 class='text-white'>$item</h2>
                        </div>
                    </div>
                </div>
            </div>
            ";
        }
        
        $query = implode(",", $query);
        $employees = DB::table('employees')
            ->selectRaw($query)
            ->whereNull('employees.deleted_at')
            ->limit(1)->get()->map(function($value) use($card) {
                $total_male = ($value->male_gen_x + $value->male_gen_y + $value->male_gen_z);
                $total_female = ($value->female_gen_x + $value->female_gen_y + $value->female_gen_z);
                $total_items = $total_male + $total_female;
                if ($total_items <= 0) {
                    $value->total = 0;
                    $value->persentase_total_male = 0;
                    $value->persentase_total_female = 0;
                    $value->card = $card;
                    return $value;
                }

                return (object) [
                    "male_gen_x" => (int) $value->male_gen_x,
                    "male_gen_y" => (int) $value->male_gen_y,
                    "male_gen_z" => (int) $value->male_gen_z,
                    "persentase_total_male" => round($total_male / $total_items * 100,2),
                    "total_male" => (int) $total_male,
                    "female_gen_x" => (int) $value->female_gen_x,
                    "female_gen_y" => (int) $value->female_gen_y,
                    "female_gen_z" => (int) $value->female_gen_z,
                    "persentase_total_female" => round($total_female / $total_items * 100,2),
                    "total_female" => (int) $total_female,
                    "total" => (int) $total_items,
                    "card" => $card,
                ];
            });
    
        return response()->json($employees->first(), 200);
    }

    public function employee_education(Request $request) {
        $subholding = $request->subholding;

        $data = DB::table('master_education')
            ->selectRaw('employees.personnel_number, master_education.nik, REPLACE(master_education.level_education, ".", "") as education, employees.code_subholding')
            ->join('employees', 'master_education.nik', '=', DB::raw("employees.prev_persno COLLATE utf8mb4_general_ci"))
            ->when(!is_null($subholding) && !empty($subholding), function($query) use($subholding) {
                $query->where('code_subholding', $subholding);
            })
            ->whereNotIn('master_education.level_education', ['N/A', 'SD', 'SLTP'])
            ->groupBy('master_education.nik')
            ->orderBy('master_education.id', 'DESC')
            ->get()->groupBy('education')->transform(function($value, $key) {
                return $value->count();
            });
        return response()->json($data, 200);
    }

    public function positions_stats(Request $request) {
        $subholding = $request->subholding;

        $data = EmployeesToPositions::join(DB::raw('(SELECT max(id) as id FROM employees_to_positions GROUP BY position_code) LatestPosition'), function($join){
            $join->on('employees_to_positions.id', '=', 'LatestPosition.id');
            })
            ->join('master_position', 'employees_to_positions.position_code', '=', 'master_position.code_position')
            ->join('master_subholding', 'master_subholding.code_subholding','=','master_position.code_subholding')
            ->join('master_position_type', 'master_position_type.code_position_type','=','master_position.code_position_type')
            ->select('name_position_type', 'position_code', 'name_position', 'name_subholding',DB::raw("
                (CASE 
                    WHEN TIMESTAMPDIFF(MONTH,start_date,end_date) < 1 THEN 'vacant'
                END) AS status"))
            ->when(!is_null($subholding) && !empty($subholding), function($query) use($subholding) {
                $query->where('master_position.code_subholding', $subholding);
            })
            ->get()->groupBy('status')->map(function($value) {
                return $value->count();
            })->toArray();
        
        if (!empty($data) && !is_null($data)) {
            $data["terisi"] = $data[""];
            unset($data[""]);
            $data["total"] = array_sum($data);
        }
        return response()->json($data, 200);
    }

    public function female_talent(Request $request) {
        $subholding = $request->subholding;

        $data = Talent::selectRaw('*, IF(status = "terminated", 1, 0) as is_terminated')
            ->whereHas('employee', function($query) use($subholding) {
                $query->where('gender_text', 'Perempuan')
                    ->orWhere('gender_text', 'Female')
                    ->when(!is_null($subholding) && !empty($subholding), function($query) use($subholding) {
                        $query->where('code_subholding', $subholding);
                    });
            })
            ->get();
        $data = [
            'terminated' => $data->where('is_terminated', '1')->count(),
            'selected' => $data->where('is_terminated', '0')->count(),
        ];
        return response()->json($data, 200);
    }

    public function milenial_talent(Request $request) {
        $subholding = $request->subholding;

        $data = Talent::selectRaw('*, IF(status = "terminated", 1, 0) as is_terminated')
            ->whereHas('employee', function($query) use($subholding) {
                $query->where(DB::raw('YEAR(date_of_birth)'), '>=', '1996')
                    ->when(!is_null($subholding) && !empty($subholding), function($query) use($subholding) {
                        $query->where('code_subholding', $subholding);
                    });
            })
            ->get();
        $data = [
            'terminated' => $data->where('is_terminated', '1')->count(),
            'selected' => $data->where('is_terminated', '0')->count(),
        ];
        return response()->json($data, 200);
    }

    public function directorat_subholding(Request $request) {
        $data = Position::where('code_subholding', $request->subholding ?? 'ap1')
            ->with('directorate')
            ->groupBy('code_directorate')->get();
        return response()->json($data, 200);
    }

    public function get_chart_talent_mapping_box() {
        $employees_nik = DB::table('employees')
            ->whereNull('employees.deleted_at')
            ->select('prev_persno')
            ->leftjoin('master_position', DB::Raw('employees.position collate utf8mb4_general_ci'), 'master_position.code_position')
            ->where('status_karyawan', '!=', 'external')
            ->when(request('sub'), function($query) {
                $query->where('master_position.code_subholding', request('sub'));
            })->get()->pluck('prev_persno');
        
        $raw_eqs = kalkulasi_eqs(['employee' => $employees_nik], false);
        $boxes = array_map(function($value) {
            $performance_index = ($value['kinerja'] * 80 / 100) + ($value['track_record'] * 20 / 100);
            return [
                'nik' => $value['nik'],
                'kinerja' => $value['kinerja'],
                'track_record' => $value['track_record'],
                'performance_index' => $performance_index,
                'job_fit' => $value['job_fit'],
                'box' => talentMapping($value['job_fit'], $performance_index)
            ];
        }, $raw_eqs);

        $boxes = collect($boxes);
        $panels = collect([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
        $total_employee = $employees_nik->count();
        $data = $panels->map(function($value) use($boxes, $total_employee) {
            return [
                'total' => $boxes->where('box', $value)->count(),
                'percentage' => ($boxes->where('box', $value)->count() == 0) ? 0 : round($boxes->where('box', $value)->count() / $total_employee * 100)
            ];
        });
        $hardcode = [
            'ap1' => [
                [
                    'total' => 9,
                    'percentage' => 5,
                ],
                [
                    'total' => 9,
                    'percentage' => 5,
                ],
                [
                    'total' => 17,
                    'percentage' => 9,
                ],
                [
                    'total' => 22,
                    'percentage' => 12,
                ],
                [
                    'total' => 34,
                    'percentage' => 19,
                ],
                [
                    'total' => 34,
                    'percentage' => 19,
                ],
                [
                    'total' => 17,
                    'percentage' => 9,
                ],
                [
                    'total' => 17,
                    'percentage' => 9,
                ],
                [
                    'total' => 12,
                    'percentage' => 6,
                ],
                [
                    'total' => 10,
                    'percentage' => 5,
                ],
            ],
            'ap2' => [
                [
                    'total' => 12,
                    'percentage' => 5,
                ],
                [
                    'total' => 12,
                    'percentage' => 5,
                ],
                [
                    'total' => 24,
                    'percentage' => 10,
                ],
                [
                    'total' => 31,
                    'percentage' => 13,
                ],
                [
                    'total' => 47,
                    'percentage' => 19,
                ],
                [
                    'total' => 47,
                    'percentage' => 19,
                ],
                [
                    'total' => 24,
                    'percentage' => 10,
                ],
                [
                    'total' => 24,
                    'percentage' => 10,
                ],
                [
                    'total' => 17,
                    'percentage' => 7,
                ],
                [
                    'total' => 11,
                    'percentage' => 4,
                ],
            ],
            'avt' => [
                [
                    'total' => 1,
                    'percentage' => 3,
                ],
                [
                    'total' => 1,
                    'percentage' => 3,
                ],
                [
                    'total' => 3,
                    'percentage' => 11,
                ],
                [
                    'total' => 4,
                    'percentage' => 14,
                ],
                [
                    'total' => 5,
                    'percentage' => 18,
                ],
                [
                    'total' => 5,
                    'percentage' => 18,
                ],
                [
                    'total' => 3,
                    'percentage' => 11,
                ],
                [
                    'total' => 3,
                    'percentage' => 11,
                ],
                [
                    'total' => 2,
                    'percentage' => 7,
                ],
                [
                    'total' => 1,
                    'percentage' => 3,
                ],
            ],
            'hin' => [
                [
                    'total' => 0,
                    'percentage' => 0,
                ],
                [
                    'total' => 0,
                    'percentage' => 0,
                ],
                [
                    'total' => 1,
                    'percentage' => 12,
                ],
                [
                    'total' => 1,
                    'percentage' => 12,
                ],
                [
                    'total' => 1,
                    'percentage' => 12,
                ],
                [
                    'total' => 1,
                    'percentage' => 12,
                ],
                [
                    'total' => 1,
                    'percentage' => 12,
                ],
                [
                    'total' => 1,
                    'percentage' => 12,
                ],
                [
                    'total' => 1,
                    'percentage' => 12,
                ],
                [
                    'total' => 1,
                    'percentage' => 12,
                ],
            ],
            'itdc' => [
                [
                    'total' => 2,
                    'percentage' => 5,
                ],
                [
                    'total' => 2,
                    'percentage' => 5,
                ],
                [
                    'total' => 4,
                    'percentage' => 10,
                ],
                [
                    'total' => 5,
                    'percentage' => 13,
                ],
                [
                    'total' => 7,
                    'percentage' => 18,
                ],
                [
                    'total' => 7,
                    'percentage' => 18,
                ],
                [
                    'total' => 4,
                    'percentage' => 10,
                ],
                [
                    'total' => 4,
                    'percentage' => 10,
                ],
                [
                    'total' => 3,
                    'percentage' => 8,
                ],
                [
                    'total' => 1,
                    'percentage' => 2,
                ],
            ],
            'snh' => [
                [
                    'total' => 1,
                    'percentage' => 9,
                ],
                [
                    'total' => 1,
                    'percentage' => 9,
                ],
                [
                    'total' => 1,
                    'percentage' => 9,
                ],
                [
                    'total' => 1,
                    'percentage' => 9,
                ],
                [
                    'total' => 2,
                    'percentage' => 18,
                ],
                [
                    'total' => 2,
                    'percentage' => 18,
                ],
                [
                    'total' => 1,
                    'percentage' => 9,
                ],
                [
                    'total' => 1,
                    'percentage' => 9,
                ],
                [
                    'total' => 1,
                    'percentage' => 9,
                ],
                [
                    'total' => 1,
                    'percentage' => 9,
                ],
            ],
            'twc' => [
                [
                    'total' => 1,
                    'percentage' => 3,
                ],
                [
                    'total' => 1,
                    'percentage' => 3,
                ],
                [
                    'total' => 3,
                    'percentage' => 10,
                ],
                [
                    'total' => 4,
                    'percentage' => 14,
                ],
                [
                    'total' => 6,
                    'percentage' => 21,
                ],
                [
                    'total' => 6,
                    'percentage' => 21,
                ],
                [
                    'total' => 3,
                    'percentage' => 10,
                ],
                [
                    'total' => 3,
                    'percentage' => 10,
                ],
                [
                    'total' => 1,
                    'percentage' => 3,
                ],
                [
                    'total' => 1,
                    'percentage' => 3,
                ],
            ],
            'all_data' => [
                [
                    'total' => 26,
                    'percentage' => 5,
                ],
                [
                    'total' => 26,
                    'percentage' => 5,
                ],
                [
                    'total' => 53,
                    'percentage' => 10,
                ],
                [
                    'total' => 68,
                    'percentage' => 13,
                ],
                [
                    'total' => 102,
                    'percentage' => 19,
                ],
                [
                    'total' => 102,
                    'percentage' => 19,
                ],
                [
                    'total' => 53,
                    'percentage' => 10,
                ],
                [
                    'total' => 53,
                    'percentage' => 10,
                ],
                [
                    'total' => 37,
                    'percentage' => 7,
                ],
                [
                    'total' => 26,
                    'percentage' => 5,
                ],
            ],
        ];
        return $data;
        // $subholding = (request('sub') == "") ? 'all_data' : request('sub');
        // return $hardcode[$subholding];
    }

    public function get_chart_talent_mapping_bar() {
        $position_type_code = PositionType::select('code_position_type')->orderBy('code_position_type', 'desc')->get();
        $data = [];
        $id = EventWindowTimeModel::where('status', 'open')->value('id');
        foreach ($position_type_code as $position_key => $position_value) {
            $data[$position_key] = Talent::join('sub_event', 'sub_event.id', 'talent.sub_event')
                ->join('master_position', 'sub_event.position', 'master_position.code_position')
                ->where('sub_event.id_event', $id)
                ->where('master_position.code_subholding', request('sub'))
                ->where('master_position.code_position_type', $position_value)
                ->count();
        }
        return $data;
    }

    public function employee_external() {
        $page_title = 'Employee External';
        $page_description = 'Insert Employee External';
        $page_breadcrumbs = [
            ['page' => '/dashboard', 'title' => 'Employee External']
        ];

        $directorate = Directorate::select('code_directorate', 'name_directorate')->get();
        $division = Division::select('code_division', 'name_division')->get();
        $group = Group::select('code_group', 'name_group')->get();
        $position = Position::select('code_position', 'name_position')->get();
        $subholding = SubHolding::select('code_subholding', 'name_subholding')->get();

        return view('dashboard.employee_external', compact(
                'page_title',
                'page_description',
                'page_breadcrumbs',
                'directorate',
                'division',
                'group',
                'position',
                'subholding'
            )
        );
    }

    public function insert_external() {
        $jabatan = Position::with('position_level', 'directorate', 'group', 'division', 'master_area', 'job_family')
            ->where('code_position', request('codePosition'))
            ->first();


        if($jabatan) {
            $insert = new Employee;
            $insert->personnel_number = request('full_name');
            $insert->position = request('codePosition');
            $insert->no_ktp = request('nik');
            $insert->prev_persno = request('nik');
            $insert->no_npwp = request('npwp');
            $insert->status_karyawan = 'External';
            $insert->code_subholding = $jabatan->code_subholding;
            $insert->directorate_code = $jabatan->code_directorate;
            $insert->division_code = $jabatan->code_division;
            $insert->group_code = $jabatan->code_group;
            $insert->position_name = $jabatan->name_position;
            $insert->personnel_area_text = $jabatan->area;
            $insert->job_family = $jabatan->job_family_asal;
            $insert->bod_type = $jabatan->org_level;
            if($jabatan->division) {
                $insert->division = $jabatan->division->name_division;
            }
            if($jabatan->group) {
                $insert->group = $jabatan->group->name_group;
            }
            if($jabatan->directorate) {
                $insert->directorate = $jabatan->directorate->name_directorate;
            }
            if($jabatan->position_level) {
                $insert->job_title = $jabatan->position_level->name_position_level;
            }
            if($jabatan->master_area) {
                $insert->area_nomenklatur = $jabatan->master_area->name_area;
            }
            if($jabatan->job_family) {
                $insert->job_family_align = $jabatan->job_family->name_job_family;
            }
            $insert->save();
    
            $credentials = [
                'nik'    => request('nik'),
                'password' => 'itms2022',
            ];
    
            Sentinel::registerAndActivate($credentials);
        }

        return redirect()->back()->with([
            "status" => "success", 
            "title" => "Berhasil",
            "message" => "Data Employee External telah disimpan di database ",
            "confirm" => "Apakah data ingin dilengkapi ?",
            "nik" => request('nik')
        ]);
    }

    private function data_pemenuhan_sdm($return='data') {
        $data = Position::with('employees')
            ->where('code_position_type', '!=', 'TP-00002')
            ->get()
            ->groupBy('code_subholding')->mapWithKeys(function($value, $key) {
                $count_positions = $value->count();
                $terisi = $value->filter(function($data) {
                    return !empty($data->employees->toArray());
                })->count();
                $persentase = $terisi / $count_positions * 100;
                return [strtoupper($key) => round($persentase)];
            });
        if ($return == 'data'){
            return $data;    
        }else if ($return == 'option'){
            return (object) [
                'radius' => 30,
                'inner_radius' => 21,
                'color' => [
                    'AP1' => '#db715e',
                    'AP2' => '#a438fc',
                    'HIN' => '#53c3c7',
                    'SNH' => '#fab54c',
                    'TWC' => '#a0bd3e',
                    'ITDC' => '#acb5b5',
                    'INJ' => '#3699ff'
                ]
            ];
        }
    }

    private function assessment_employee_old($order="DESC") {
        $subholding = SubHolding::get()->groupBy('name_subholding')->toArray();
        $required_competencies = DB::table('master_competency_required')->get()->pluck('name_competency');
        $total_employees = Employee::count();
        $assessment_employee = AssessmentCompetencyEmployee::where('soft_competency_align', '!=', "")
            ->whereIn('soft_competency_align', $required_competencies)
            ->selectRaw('assessment_result_by_competency.*, master_subholding.name_subholding')
            ->join('employees', 'assessment_result_by_competency.nik', DB::raw('employees.prev_persno collate utf8mb4_general_ci'))
            ->join('master_position', 'master_position.code_position', DB::raw('employees.position collate utf8mb4_general_ci'))
            ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
            ->get()->groupBy('soft_competency_align')->map(function($value, $key) use($total_employees) {
                $result = $value->groupBy('name_subholding')->map(function($value) use($total_employees) {
                    return round($value->pluck('result')->sum() / ($total_employees * 4) * 100);
                });
                return (object) [
                    'result' => $result,
                    'key' => $key
                ];
            })->sortBy([
                function($current, $next) use($order) {
                    if ($order == "DESC")
                        return $current->result->sum() < $next->result->sum();
                    else
                        return ($current->result->sum() > 0 && $next->result->sum() > 0 && $current->result->sum() > $next->result->sum());
                }   
            ])->groupBy('key')->map(function($value) {
                return $value->first()->result;
            })->slice(0, 3)->toArray();

        array_walk($subholding, function(&$value, $key) use($assessment_employee) {
            $data_per_subholding = array_map(function($item) use($key, $assessment_employee) {
                return [$item => (!isset($assessment_employee[$item][$key])) ? 0 : $assessment_employee[$item][$key]];
            }, array_keys($assessment_employee));
            $value = call_user_func_array('array_merge', $data_per_subholding);
        });
        return $subholding;
    }

    private function assessment_employee($order="DESC") {
        $subholding = SubHolding::get()->groupBy('name_subholding')->toArray();
        $required_competencies = DB::table('master_competency_required')->get()->pluck('name_competency');
        $total_employees = Employee::with('positions.sub_holding')->get();
        $assessment_employee = AssessmentCompetencyEmployee::where('soft_competency_align', '!=', "")
            ->whereIn('soft_competency_align', $required_competencies)
            ->selectRaw('assessment_result_by_competency.*, master_subholding.name_subholding')
            ->join('employees', 'assessment_result_by_competency.nik', DB::raw('employees.prev_persno collate utf8mb4_general_ci'))
            ->join('master_position', 'master_position.code_position', DB::raw('employees.position collate utf8mb4_general_ci'))
            ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
            ->get()->groupBy('soft_competency_align')->map(function($value, $key) use($total_employees, $order) {
                $result = $value->groupBy('name_subholding')->map(function($value, $subholding) use($total_employees, $order, $key) {
                    $employee_perholding = $total_employees->pluck('positions')->pluck('sub_holding')->pluck('name_subholding')->filter(function($perholding) use ($subholding) {
                        return $perholding == $subholding;
                    })->count();
                    return [
                        'result' => $order == "DESC" ? round($value->pluck('result')->sum() / ($total_employees->count() * 4) * 100, 2) : round((($employee_perholding * 4) - $value->pluck('result')->sum()) / ($total_employees->count() * 4) * 100, 2),
                        'result_perholding' => round($value->pluck('result')->sum() / ($employee_perholding * 4) * 100, 2),
                        'raw' => $value->pluck('result')->sum(),
                        'subholding' => $subholding
                    ];
                });
                return (object) [
                    'result' => $result->pluck('result', 'subholding'),
                    'result_perholding' => $result->pluck('result_perholding', 'subholding'),
                    'raw_data' => $result->pluck('raw', 'subholding'),
                    'key' => $key
                ];
            })->sortBy([
                function($current, $next) {
                    return $current->result->sum() < $next->result->sum();
                }   
            ])->groupBy('key')->map(function($value) {
                return [
                    'result' => $value->first()->result,
                    'result_perholding' => $value->first()->result_perholding,
                    'raw_data' => $value->first()->raw_data,
                ];
            })->slice(0, 3)->toArray();
        array_walk($subholding, function(&$value, $key) use($assessment_employee) {
            $data_per_subholding = array_map(function($item) use($key, $assessment_employee) {
                return [
                    $item => [
                        'result' => (!isset($assessment_employee[$item]['result'][$key])) ? 0 : $assessment_employee[$item]['result'][$key],
                        'result_perholding' => (!isset($assessment_employee[$item]['result_perholding'][$key])) ? 0 : $assessment_employee[$item]['result_perholding'][$key],
                        'raw_data' => (!isset($assessment_employee[$item]['raw_data'][$key])) ? 0 : $assessment_employee[$item]['raw_data'][$key]
                    ]
                ];
            }, array_keys($assessment_employee));
            $value = call_user_func_array('array_merge', $data_per_subholding);
        });

        return $subholding;
    }

    public function filter_positions() {
        $selected = request('selected');
        $load = request('load');

        $positions = Position::when($load == "directorat", function ($query) use ($selected) {
            $query->when(trim($selected) != '' && $selected != '-', function ($q) use ($selected) {
                $q->where('code_subholding', $selected);
            })
            ->groupBy('code_directorate')
            ->with('directorate');
        })
        ->when($load == "group", function ($query) use ($selected) {
            $query->when(trim($selected) != '' && $selected != '-', function ($q) use ($selected) {
                $q->where('code_subholding', $selected);
            })
            ->groupBy('code_group')
            ->with('group');
        })
        ->when($load == "divisi", function ($query) use ($selected) {
            $query->when(trim($selected) != '' && $selected != '-', function ($q) use ($selected) {
                $q->where('code_subholding', $selected);
            })
            ->groupBy('code_division')
            ->with('division');
        })
        ->when($load == "position", function ($query) use ($selected) {
            $query->when(trim($selected[0]) != '' && $selected[0] != '-', function ($q) use ($selected) {
                $q->where('code_subholding', $selected[0]);
            })
            ->when(trim($selected[1]) != '' && $selected[1] != '-', function ($q) use ($selected) {
                $q->where('code_directorate', $selected[1]);
            })
            ->when(trim($selected[2]) != '' && $selected[2] != '-', function ($q) use ($selected) {
                $q->where('code_group', $selected[2]);
            })
            ->when(trim($selected[3]) != '' && $selected[3] != '-', function ($q) use ($selected) {
                $q->where('code_division', $selected[3]);
            })
            ->select(
                "*",
                DB::raw('(SELECT name_area FROM master_area WHERE master_area.code_area=area) AS area_position')
            );
        })
        ->get()
        ->map(function ($value) use ($load) {
            if ($load == "subholding" && !empty($value->sub_holding)) {
                return [
                    'code' => $value->sub_holding->code_subholding,
                    'name' => $value->sub_holding->name_subholding,
                    'load' => $load
                ];
            }
            if ($load == "directorat" && !empty($value->directorate)) {
                return [
                    'code' => $value->directorate->code_directorate,
                    'name' => $value->directorate->name_directorate,
                    'load' => $load
                ];
            }
            if ($load == "group" && !empty($value->group)) {
                return [
                    'code' => $value->group->code_group,
                    'name' => $value->group->name_group,
                    'load' => $load
                ];
            }
            if ($load == "divisi" && !empty($value->division)) {
                return [
                    'code' => $value->division->code_division,
                    'name' => $value->division->name_division,
                    'load' => $load
                ];
            }

            if ($load == "position" && !empty($value)) {
                return [
                    'code' => $value->code_position,
                    'name' => '['. $value->area_position .'] '.$value->name_position,
                    'load' => $load
                ];
            }
        })
        ->toArray();

        return response()->json(array_values(array_filter($positions)), 200);
    }

    public function role_switch() {
        if (Sentinel::check()->roles->count() < 2) {
            abort("403");
        }

        session()->put('role', '');
        // return session()->all();
        // return Sentinel::check()->roles->count();
        // return Session::get('role');
        return view('select-role.index');
    }

    public function role_switch_process() {
        if (Sentinel::check()->roles->count() < 2) {
            abort("403");
        }

        session()->put('role', request('role'));

        return redirect('/dashboard');
    }

    public function data_eqs_detail_posisi(Request $request) {
        $parameter = [
            'employee' => [$request->nik],
            'position' => $request->code_position
        ];

        $all = ['all'];
        $data = kalkulasi_eqs($parameter, false, $all);

        return view('dashboard.element.detail-informasi', [
            'data' => $data
        ]);

    }

}
