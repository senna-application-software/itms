<?php

namespace App\Http\Controllers\ToolSettings;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\MasterHukdis;
use App\Models\MasterJobFamily;
use App\Models\MasterArea;
use App\Models\Position;
use App\Models\SubHolding;
use App\Models\TalentAspiration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ToolSettingsController extends Controller
{
    public function talent_poll() {
        // Kebutuhan Perholding
        $subholding = "";
        $subholding_condition = ($subholding != "" && $subholding != null && $subholding != false);
        $raw_positions = Position::when($subholding_condition, function($query) use($subholding) {
            $query->where('code_subholding', $subholding);
        })->with('master_area')->with('sub_holding')->get();
        
        $positions = $raw_positions;
        $jobFamily = MasterJobFamily::select('name_job_family')->get();
        $subholding = $raw_positions->pluck('sub_holding')->pluck('name_subholding', 'code_subholding');
        $grade = Employee::select('person_grade_align')
            ->orderBy('person_grade_align', 'ASC')
            ->whereNotNull('person_grade_align')
            ->get()
            ->unique('person_grade_align');
        $talentCluster   = array();
        $talentCluster[] = [
            'key' => 1,
            'val' => 'Unfit (Talent Cluster 1)',
        ];
        $talentCluster[] = [
            'key' => 2,
            'val' => 'Sleeping Tiger (Talent Cluster 2)',
        ];
        $talentCluster[] = [
            'key' => 3,
            'val' => 'Solid Contributor (Talent Cluster 3)',
        ];
        $talentCluster[] = [
            'key' => 4,
            'val' => 'Sleeping Tiger (Talent Cluster 4)',
        ];
        $talentCluster[] = [
            'key' => 5,
            'val' => 'Promotable (Talent Cluster 5)',
        ];
        $talentCluster[] = [
            'key' => 6,
            'val' => 'Solid Contributor (Talent Cluster 6)',
        ];
        $talentCluster[] = [
            'key' => 7,
            'val' => 'Promotable (Talent Cluster 7)',
        ];
        $talentCluster[] = [
            'key' => 8,
            'val' => 'Promotable (Talent Cluster 8)',
        ];
        $talentCluster[] = [
            'key' => 9,
            'val' => 'High Potential (Talent Cluster 9)',
        ];

        $page_title       = 'Talent Pool';
        $page_description = 'Halaman Talent Pool';
        $page_breadcrumbs = [
            ['page' => 'dashboard', 'title' => 'Home'],
            ['page' => '#', 'title' => 'Talent Pool'],
        ];

        return view('tools-settings.talent_poll', compact('page_title', 'page_description', 'page_breadcrumbs', 'jobFamily', 'subholding', 'grade', 'talentCluster', 'positions'));
    }

    public function table_talent_poll(Request $req) {
        $data = query_employees($req);

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                if ($data->prev_persno) {
                    $edit = route('SearchDataEmployee.profile', $data->prev_persno)."?back=talent_pool";
                    $view_aspirasi = route('settings.list_aspirasi_employee',['nik' => $data->prev_persno]);
                } else {
                    $edit = "#";
                    $view_aspirasi = '#';
                }
                return '<div class="dropdown table-action">
                    <button class="btn btn-light-primary btn-icon btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ki ki-bold-more-ver"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a href="' . $edit  . '" class="dropdown-item">
                            View Profile
                        </a>
                        <a href="' . $view_aspirasi  . '" class="dropdown-item modal-view-aspirasi">
                            View Aspirasi
                        </a>
                    </div>
                </div>';
            })
            ->addColumn('info', function ($data) {
                $isCalibrate = (!empty($data->employee_calibration_history->toArray())) ? "Calibrated" : "Non-Calibrated";
                $label          = "label-light-primary";
                if ($isCalibrate == "Non-Calibrated") {
                    $label          = "label-light-danger";
                }

                return '<span class="label font-weight-bold label-lg ' . $label . ' label-inline label-pill">' . $isCalibrate . '</span>';
            })
            ->editColumn('status_karyawan', function ($data) {
                $statusKaryawan = 'Internal';
                $label          = "label-light-primary";

                if ($data->status_karyawan == "External") {
                    $statusKaryawan = 'External';
                    $label          = "label-light-success";
                }

                return '<span class="label font-weight-bold label-lg ' . $label . ' label-inline">' . $statusKaryawan . '</span>';
            })
            ->rawColumns(['action', 'info', 'status_karyawan'])
            ->make(true);
    }

    public function filterOption(Request $req) {
        $type = $req->type;
        $filter = $req->filter;

        $data = [];
        $message = 'failed';
        $status = 404;

        if($type == 'area') {
            $status = 200;
            $message = 'success';

            $data = MasterArea::select('code_area as key', 'name_area as value')
            ->when(!is_null($filter) && !empty($filter), function($query) use ($filter) {
                $query->where('code_subholding', $filter);
            })
            ->get();
        } else if($type == 'position') {
            $status = 200;
            $message = 'success';

            $data = Position::select('code_position as key', 'name_position as value')
            ->when(!is_null($filter) || !empty($filter), function($query) use ($filter) {
                $query->where('area', $filter);
            })
            ->get();
        } else if($type == 'subholding-position') {
            $status = 200;
            $message = 'success';
            
            $data = Position::select('code_position as key', 'name_position as value')
            ->when(!is_null($filter) || !empty($filter), function($query) use ($filter) {
                $query->where('code_subholding', $filter);
            })
            ->get();
        }

        $response = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );

        return response()->json($response, $status);
    }

    public function listAspirasiEmployee(Request $request) {

        $page_title       = 'Talent Pool';
        $page_description = 'Halaman Talent Pool';
        $page_breadcrumbs = [
            ['page' => 'dashboard', 'title' => 'Home'],
            ['page' => 'settings/talent_poll', 'title' => 'Talent Pool'],
            ['page' => '#', 'title' => 'Aspirasi per Employee'],
        ];

        $data =Employee::where('prev_persno', $request->nik)->first();
        if(!$data) {
            abort('404','Mohon periksa NIK. Data untuk Karyawan tersebut tidak ditemukan di database');
        }
        return view('tools-settings.list-aspirasi-employee', compact('page_title', 'page_description', 'page_breadcrumbs', 'data'));
    }

    public function dataListAspirasiEmployee(Request $request) {
        $data = TalentAspiration::join('master_position', 'master_position.code_position', '=','talent_aspiration.code_position')
            ->join('employees', 'talent_aspiration.nik', '=', DB::raw('employees.prev_persno collate utf8mb4_unicode_ci'))
            ->join('master_subholding', 'master_position.code_subholding', 'master_subholding.code_subholding')
            ->leftJoin('master_division', 'master_division.code_division', 'master_position.code_division')
            ->leftJoin('master_area', 'master_area.code_area', 'master_position.area')
            ->where('nik', $request->nik)
            ->select('talent_aspiration.aspiration_type','employees.personnel_number','name_position', 'name_area','master_subholding.name_subholding')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->editColumn('aspiration_type', function ($data) {
                $tipe = '-';
                $label = "label-light-primary";

                if ($data->aspiration_type == "individual") {
                    $tipe   = 'Individual (IA)';
                    $label  = "label-light-success";
                }

                if ($data->aspiration_type == "supervisor") {
                    $tipe   = 'Supervisor (SA)';
                    $label  = "label-light-success";
                }

                if ($data->aspiration_type == "job_holder") {
                    $tipe   = 'Job Holder (JA)';
                    $label  = "label-light-success";
                }

                if ($data->aspiration_type == "unit") {
                    $tipe   = 'Unit (UA)';
                    $label  = "label-light-success";
                }

                return '<span class="label font-weight-bold label-lg ' . $label . ' label-inline">' . $tipe . '</span>';
            })
            ->rawColumns(['action', 'info', 'aspiration_type'])
            ->make(true);
    }
}
