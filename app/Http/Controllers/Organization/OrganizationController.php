<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use DB;

use App\Models\Employee;
use App\Models\Position;
use App\Models\EmployeesToPositions;

class OrganizationController extends Controller
{
    public function index()
    {
        $page_title = 'Struktur Organisasi';
        $page_description = 'Deskripsi halaman';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Event']
        ];

        $list_subholding = DB::table('master_subholding')->get();

        return view('organization.index', compact('page_title', 'page_description','page_breadcrumbs','list_subholding'));
    }

    public function subholding(Request $request)
    {
        $page_title = 'Struktur Organisasi';
        $page_description = 'Deskripsi halaman';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Event']
        ];

        $code_subholding = $request->get('code_subholding');

        return view('organization.subholding', compact('page_title', 'page_description','page_breadcrumbs','code_subholding'));
    }

    public function show(Request $request)
    {
        $where  = [
            'subholding'    => $request->code_subholding
        ];

        $data   = generate_diagram_organization($where);
        
        $page_title = 'Struktur Organisasi';
        $page_description = 'Deskripsi halaman';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Event']
        ];

        return view('organization.show', compact('page_title', 'page_description','page_breadcrumbs', 'data'));
    }

    public function datatable(Request $request)
    {
        $data = DB::table('master_position')
            ->select(['master_position.code_division',
                'master_position.code_group',
                'master_position.code_position',
                'master_position.code_directorate',
                'master_directorate.name_directorate as direktorat',
                'master_group.name_group as group',
                'master_division.name_division as divisi',
                'master_subholding.name_subholding as subholding'])
            ->leftJoin('master_subholding','master_subholding.code_subholding','=','master_position.code_subholding')
            ->leftJoin('master_directorate','master_directorate.code_directorate','=','master_position.code_directorate')
            ->leftJoin('master_group','master_group.code_group','=','master_position.code_group')
            ->leftJoin('master_division','master_division.code_division','=','master_position.code_division')
            ->where('master_position.code_subholding',$request->code_subholding)
            ->whereNotNull('master_position.code_division')
            ->groupBy('master_position.code_division')
            ->get();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->AddColumn('button', function($data) {
            $html = 
                '<div class="dropdown table-action">
                    <button class="btn btn-light-primary btn-icon btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ki ki-bold-more-ver"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a href="'. route('struktur-organisasi.show') .'" class="dropdown-item">
                            <i class="fa fa-eye mr-2"></i>
                            Lihat
                        </a>
                    </div>
                </div>';

            return $html;
            }
        )   
        ->rawColumns(['button'])
        ->make(true);
    }

    public function refresh_emp_to_post()
    {
        $employee_all = Employee::all();

        foreach($employee_all as $item_emp){
            $position_code    = $item_emp->position;

            $find_position  = Position::where([
                ["code_position", "=", $position_code]
            ])->first();

            if(!empty($find_position)){
                $update_or_create = EmployeesToPositions::updateOrCreate([
                    "nik"   => $item_emp->prev_persno
                ],[
                    "position_code" => $item_emp->position,
                    "nik"           => $item_emp->prev_persno,
                    "start_date"    => date('Y-m-d'),
                    "end_date"      => date('Y-m-d'),
                ]);
            }
        }
    }
}
