<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\EventWindowTimeModel;
use App\Models\Employee;
use App\Models\MasterHukdis;
use App\Models\EmployeesToPositions;
use App\Jobs\SendBulkQueueEmail;
use DB;
use DateTime;
use App\Models\User;

class EventController extends Controller
{
    public function index()
    {    
        $page_title = 'Event';
        $page_description = 'Membuat event baru';
        $page_breadcrumbs = [ 
            ['page' => 'dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Event Aspiration']
        ];

        return view('event.index', compact('page_title', 'page_description','page_breadcrumbs'));
    }

    public function create(Request $request)
    {
        $retrieve       = EventWindowTimeModel::where('status', 'open')->pluck('id')->count();
        $retrieve_batch = EventWindowTimeModel::where('year', $request->year)->pluck('id')->count();

        if ($retrieve != 0) {
            return redirect()->back()->with([
                "status" => "error", 
                "title" => "Terdapat Event Aktif",
                "message" => "Tidak dapat menambahkan event baru jika ada event yang berstatus aktif"
            ]);
        }

        $event = EventWindowTimeModel::create([
            'year'          => $request->year,
            'batch'         => $request->year . '-'. ($retrieve_batch + 1),
            'start_date'    => date_db_formatter($request->start_date),
            'end_date'      => date_db_formatter($request->end_date),
            'status'        => 'open'
        ]);

        if ($event) {
            $periode_pengisian = EventWindowTimeModel::where('status', 'open')->first();
            $Eligible = EmployeesToPositions::with("employee")->whereNotNull('nik')->whereNotNull('start_date')->whereNotNull('end_date')->get();
            foreach ($Eligible as $v) {
                $fdate      = $v->start_date;
                $tdate      = $v->end_date;
                $datetime1  = new DateTime($fdate);
                $datetime2  = new DateTime($tdate);
                $interval   = $datetime1->diff($datetime2);
                if(isset($v->employee->prev_persno)) {
                    $list_employe[] = [ 
                        'lama_kerja'    => $interval->format('%y'), 
                        'nik'           => $v->employee->prev_persno, 
                        'email'         => $v->employee->email,
                        'prefix'        => genderPrefix($v->employee->gender_text),
                        'sub_prefix'    => genderSubprefix($v->employee->gender_text),
                        'nama'          => $v->employee->personnel_number,
                        'username'      => $v->employee->prev_persno,
                        'password'      => 'itms2022',
                        'periode_pengisian' => date("d-m-Y", strtotime($periode_pengisian->start_date)) . ' s.d ' . date("d-m-Y", strtotime($periode_pengisian->end_date))
                    ];   
                }        
            }

            $data = [];
            foreach ($list_employe as $le) {
                if ($le['lama_kerja'] >= 2) {
                    $em = MasterHukdis::where('nik', $le['nik'])->first();
                    if (!$em) {
                        $data[]=[
                            'nik'           => $le['nik'], 
                            'emails'        => $le['email'], 
                            'type'          => 'input_aspiration',
                            'prefix'        => $le['prefix'],
                            'sub_prefix'    => $le['sub_prefix'],
                            'nama'          => $le['nama'],
                            'username'      => $le['username'],
                            'password'      => $le['password'],
                            'periode_pengisian' => $le['periode_pengisian'],
                            'url'           => env('APP_URL','/')
                        ];

                        $user = User::where("nik", $le["nik"])->first();
                        // dd($user);
                        if($user) {
                            $user->notify(new \App\Notifications\AspirationNotification($user));
                        }
                    }
                }
            }

            if (count($data) > 0) { 
                // send all mail in the queue.
                $job = (new SendBulkQueueEmail($data))->delay(now()->addSeconds(2)); 
                dispatch($job);

                $msg = "Bulk mail send successfully in the background...";
            } else {
                $msg = "Tidak ada perseta yang eligible. Tidak ada email yang akan dikirim untung mengundang peserta.";
            }
        }

        return redirect()->back()->with([
            "status" => "success", 
            "title" => "Berhasil",
            "message" => "Data event telah disimpan di database " . $msg
        ]);
    }

    public function edit(Request $request)
    {
        $retrieve       = EventWindowTimeModel::where('status', 'open')->pluck('id');
        if (isset($retrieve[0]) && $retrieve[0] != $request->id_event) {
            return redirect()->back()->with([
                "status" => "error", 
                "title" => "Terdapat Event Aktif",
                "message" => "Tidak dapat mengubah status event menjadi aktif jika ada event lainnya yang masih aktif"
            ]);
        }

        EventWindowTimeModel::where('id', $request->id_event)
            ->update([
                'start_date'    => date_db_formatter($request->start_date),
                'end_date'      => date_db_formatter($request->end_date),
                'status'        => $request->status,
                'status_input'  => $request->status == 'closed' ? 0 : $request->status_input,
            ]);

        return redirect()->back()->with([
            "status" => "success", 
            "title" => "Berhasil",
            "message" => "Data event telah diubah di database"
        ]);
    }

    public function get_attribute($id)
    {
        $datas   = EventWindowTimeModel::find($id);

        $data["id"]           = $datas->id;
        $data["start_date"]   = date('d/m/Y', strtotime($datas->start_date));
        $data["end_date"]     = date('d/m/Y', strtotime($datas->end_date));
        $data["status"]       = $datas->status;
        $data["status_input"] = $datas->status_input;
        $data["year"]         = $datas->year;

        return response()->json($data);
    }

    public function datatable()
    {
        $data = EventWindowTimeModel::all();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->AddColumn('button', function($data) {
            $html = 
            '<div class="dropdown table-action">
                <button class="btn btn-light-primary btn-icon btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ki ki-bold-more-ver"></i>
                </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
            
            if($data->status == "open") {
                $html .= '<div class="dropdown-item" onclick="edit_data('. $data->id .')">
                            <i class="fa fa-edit mr-2"></i>
                            Ubah Data
                        </div>';
            } else {
                $html .= '<div class="dropdown-item" onclick="edit_data('. $data->id .')">
                            <i class="fa fa-edit mr-2"></i>
                            Ubah Data
                        </div>';
                $html .= '<a href="' . route('sub-event.history', $data->id) . '" class="dropdown-item">
                        <i class="fa fa-eye mr-2"></i>
                        History Event
                    </a>';
            }

            $html .='</div>
                </div>';

            return $html;
        })->EditColumn('status', function($data){
            if ($data->status == 'open' && $data->status_input == 1)
                $label = 'label-light-success'; 
            elseif (($data->status == 'open' && $data->status_input == 0))
                $label = 'label-light-danger'; 
            else 
                $label = 'label-light-dark';
            $status = ($data->status == 'open' && $data->status_input == 0) ? 'Submit Aspiration Closed' : $data->status;
            $html = '<span class="label font-weight-bold label-lg  '.$label.' label-inline">'. $status    .'</span>';

            return $html;
        })
        ->rawColumns(['button', 'status'])
        ->make(true);
    }
}
