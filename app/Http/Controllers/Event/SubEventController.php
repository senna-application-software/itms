<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Jobs\SendBulkQueueEmail;
use App\Models\Directorate;
use App\Models\Employee;
use App\Models\EmployeesDocs;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\SubEvent;
use App\Models\TalentAspiration;
use App\Models\EmployeesToPositions;
use App\Models\EventWindowTimeModel;
use App\Models\PendingTaskNotification;
use App\Models\Position;
use App\Models\SubEventTalentComitee;
use App\Models\SubHolding;
use App\Models\Talent;
use Barryvdh\DomPDF\Facade\Pdf;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;

class SubEventController extends Controller
{
    public function index()
    {
        $sub_holding = SubHolding::get();
        $page_title = 'Detail Event';
        $page_description = 'Membuat sub event baru';
        $page_breadcrumbs = [
            ['page' => '/dashboard', 'title' => 'Home'],
            ['page' => '#', 'title' => 'Event Positions']
        ];
        return view('sub-event.index', compact(
            'page_title',
            'page_description',
            'page_breadcrumbs',
            'sub_holding'
        ));
    }

    public function history($id)
    {
        $sub_holding = SubHolding::get();
        $page_title = 'History Event';
        $page_description = 'Melihat history event';
        $page_breadcrumbs = [
            ['page' => '/dashboard', 'title' => 'Home'],
            ['page' => '#', 'title' => 'Event Positions']
        ];

        $data   = SubEvent::select(
            'sub_event.*',
            'master_position.name_position',
        )
            ->leftJoin('master_position', 'master_position.code_position', '=', 'sub_event.position')
            ->where('sub_event.id_event', '=', $id)
            ->get();

        return view('sub-event.history', compact(
            'data',
            'page_title',
            'page_description',
            'page_breadcrumbs',
            'sub_holding'
        ));
    }

    public function filter_positions(Request $request)
    {
        $tipe = $request->tipe;
        $selected = $request->selected;
        $load = $request->load;

        $positions = EmployeesToPositions::with(['positions' => function ($query) use ($load, $selected) {
            $query->when($load == "subholding", function ($query) use ($selected) {
                // $query->where('code_subholding', $selected)
                $query->groupBy('code_subholding')
                    ->with('sub_holding');
            })
            ->when($load == "directorat", function ($query) use ($selected) {
                $query->when(trim($selected) != '' && $selected != '-', function ($q) use ($selected) {
                    $q->where('code_subholding', $selected);
                })
                ->groupBy('code_directorate')
                ->with('directorate');
            })
            ->when($load == "group", function ($query) use ($selected) {
                $query->when(trim($selected) != '' && $selected != '-', function ($q) use ($selected) {
                    $q->where('code_subholding', $selected);
                })
                ->groupBy('code_group')
                ->with('group');
            })
            ->when($load == "divisi", function ($query) use ($selected) {
                $query->when(trim($selected) != '' && $selected != '-', function ($q) use ($selected) {
                    $q->where('code_subholding', $selected);
                })
                ->groupBy('code_division')
                ->with('division');
            })
            ->when($load == "position", function ($query) use ($selected) {
                $query->when(trim($selected[0]) != '' && $selected[0] != '-', function ($q) use ($selected) {
                    $q->where('code_subholding', $selected[0]);
                })
                ->when(trim($selected[1]) != '' && $selected[1] != '-', function ($q) use ($selected) {
                    $q->where('code_directorate', $selected[1]);
                })
                ->when(trim($selected[2]) != '' && $selected[2] != '-', function ($q) use ($selected) {
                    $q->where('code_group', $selected[2]);
                })
                ->when(trim($selected[3]) != '' && $selected[3] != '-', function ($q) use ($selected) {
                    $q->where('code_division', $selected[3]);
                })
                ->select(
                    '*',
                    DB::raw("(SELECT name_area FROM master_area WHERE master_area.code_area=area) AS area_positions")
                );
            });
        }])
        ->when($tipe == "vacant", function ($query) {
            $query->whereNull('nik');
        })
        ->when($tipe == "vacant_soon", function ($query) {
            $query->whereRaw('DATEDIFF(DATE(end_date), NOW()) < 365.25');
        })
        ->when($tipe == "higher_3year", function ($query) {
            $query->whereRaw('DATEDIFF(DATE(end_date), DATE( start_date)) > (365.25*3)');
        })
        ->get()
        ->map(function ($value) use ($load) {
            if ($load == "subholding" && !empty($value->positions->sub_holding)) {
                return [
                    'code' => $value->positions->sub_holding->code_subholding,
                    'name' => $value->positions->sub_holding->name_subholding,
                    'load' => $load
                ];
            }
            if ($load == "directorat" && !empty($value->positions->directorate)) {
                return [
                    'code' => $value->positions->directorate->code_directorate,
                    'name' => $value->positions->directorate->name_directorate,
                    'load' => $load
                ];
            }
            if ($load == "group" && !empty($value->positions->group)) {
                return [
                    'code' => $value->positions->group->code_group,
                    'name' => $value->positions->group->name_group,
                    'load' => $load
                ];
            }
            if ($load == "divisi" && !empty($value->positions->division)) {
                return [
                    'code' => $value->positions->division->code_division,
                    'name' => $value->positions->division->name_division,
                    'load' => $load
                ];
            }

            if ($load == "position" && !empty($value->positions)) {
                return [
                    'code' => $value->positions->code_position,
                    'name' => '['. $value->positions->sub_holding->name_subholding .']['. $value->positions->area_positions .'] '.$value->positions->name_position,
                    'load' => $load
                ];
            }
        })->toArray();
        return response()->json(array_values(array_filter($positions)), 200);
    }

    public function create(Request $request)
    {
        if ($request->position == null) {
            return redirect()->back()->with([
                "status" => "error",
                "title" => "Error",
                "message" => "Data Posisi tidak boleh kosong"
            ]);
        }

        if ($request->talent_com == null) {
            return redirect()->back()->with([
                "status" => "error",
                "title" => "Error",
                "message" => "Talent Committee tidak boleh kosong"
            ]);
        }

        $ewt = EventWindowTimeModel::whereIn('status', ['open'])->select('id', 'start_date', 'end_date')->first();
        $id_event = SubEvent::insertGetId([
            'id_event'      => $ewt->id,
            'position'      => $request->position,
            'name'          => $request->name == null ? "Event " . $request->vacant_type : $request->name,
            'status'        => 'sourcing',
            'vacant_type'   => $request->vacant_type,
            'sourcing_type' => $request->sourcing_type
        ]);

        foreach ($request->talent_com as $key => $value) {
            $insrt = new SubEventTalentComitee;
            $insrt->id_subevent = $id_event;
            $insrt->nik = $value;
            $insrt->urutan = ($key+1);
            $insrt->save();

            $credentials = [
                'nik'    => $value
            ];
            
            try {
                $user = Sentinel::findByCredentials($credentials);
                $role = Sentinel::findRoleBySlug('talent_committee');
                $role->users()->attach($user);
            } catch (\Throwable $th) {
                //throw $th;
            }
        }

        // UPdate status pending task
        try {
            $cek_pending_task = PendingTaskNotification::where('position', $request->position)->first();
            if ($cek_pending_task != null) {
                $cek_pending_task->status = 'on_process';
                $cek_pending_task->id_sub_event = $id_event;
                $cek_pending_task->save();
            }
        } catch (\Throwable $th) {
            //throw $th;
        }


        // Insert Eqs Dasar
        $eligible_employee = TalentAspiration::where('code_position', $request->position)->groupBy('nik')
            ->where('id_event', $ewt->id)->get();
        $load_eqs = [
            'employee' => $eligible_employee->pluck('nik')->toArray(),
            'position' => $request->position
        ];
        $eqs_employees = kalkulasi_eqs($load_eqs, false);
        array_walk($eqs_employees, function (&$value, $key) use ($id_event) {
            $value["sub_event"] = $id_event;
            return $value;
        });
        DB::table('employee_eqs')->insert($eqs_employees);

        // Insert Eligible Employee
        $data_talent = $eligible_employee->map(function ($value) use ($id_event) {
            return [
                'nik' => $value->nik,
                'sub_event' => $id_event,
                'status' => 'selected'
            ];
        })->toArray();
        Talent::insert($data_talent);

        $position_qry = DB::select("select master_position.name_position, master_subholding.name_subholding from master_position
            left join master_subholding on master_subholding.code_subholding = master_position.code_subholding
            where master_position.code_position='" . $request->position . "'
        ");

        $kandidat_nama_jabatan = "";
        $kandidat_nama_subholding = "";

        if ($position_qry) {
            $kandidat_nama_jabatan = $position_qry[0]->name_position;
            $kandidat_nama_subholding = $position_qry[0]->name_subholding;
        }

        $periode_pengisian = date('d-m-Y', strtotime($ewt->start_date)) . ' s.d ' . date('d-m-Y', strtotime($ewt->end_date));
        $talent_data = TalentAspiration::join('employees', DB::raw('employees.prev_persno collate utf8mb4_unicode_ci'), '=', 'talent_aspiration.nik')
            ->where('code_position', $request->position)
            ->select(
                'nik',
                'email',
                'gender_text',
                'personnel_number',
                'prev_persno',
            )
            ->groupBy('nik')
            ->get();

        $data = [];
        foreach ($talent_data as $value) {
            $data[] = [
                'nik' => $value['nik'],
                'emails' => $value['email'],
                'type' => 'update_profile',
                'prefix' => genderPrefix($value['gender_text']),
                'sub_prefix' => genderSubprefix($value['gender_text']),
                'nama' => $value['personnel_number'],
                'username' => $value['prev_persno'],
                'password' => 'itms2022',
                'periode_update' => $periode_pengisian,
                'kandidat_nama_jabatan' => $kandidat_nama_jabatan,
                'kandidat_nama_subholding' => $kandidat_nama_subholding,
                'url' => url('my-profile')
            ];
        }

        if (count($data) > 0) {
            // send all mail in the queue.
            $job = (new SendBulkQueueEmail($data))->delay(now()->addSeconds(2));
            dispatch($job);

            $msg = "Bulk mail send successfully in the background...";
        } else {
            $msg = "Tidak ada perseta yang eligible. Tidak ada email yang akan dikirim untung mengundang peserta.";
        }

        return redirect()->back()->with([
            "status" => "success",
            "title" => "Berhasil",
            "message" => "Data sub event telah disimpan di database " . $msg
        ]);
    }

    public function datatable()
    {
        $open_event = EventWindowTimeModel::where('status', 'open')->first();

        if (!empty($open_event)) {
            $id = $open_event->id;
        } else {
            $id = 0;
        }

        $data = SubEvent::select(
            'sub_event.*',
            'master_position.name_position',
            'master_position.code_subholding',
            'master_division.name_division',
            'master_subholding.name_subholding',
            'master_area.name_area'
        )
            ->leftJoin('master_position', 'master_position.code_position', '=', 'sub_event.position')
            ->leftJoin('master_division', 'master_position.code_division', '=', 'master_division.code_division')
            ->leftJoin('master_subholding', 'master_position.code_subholding', 'master_subholding.code_subholding')
            ->leftJoin('master_area', 'master_position.area', 'master_area.code_area')
            ->where('sub_event.id_event', $id)
            ->withCount('talent')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->AddColumn('button', function ($data) {
                if ($data->status == 'sourcing' && $data->talent_count == 0) {
                    $html =
                        '<div class="dropdown table-action">
                            <button class="btn btn-light-primary btn-icon btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ki ki-bold-more-ver"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a href="' . route('sub-event.detail_subevent', $data->id) . '" class="dropdown-item">
                                    Detail Sub Event 
                                </a>
                                <a href="' . route('sub-event.delete_subevent', $data->id) . '" class="dropdown-item">
                                    Delete
                                </a>
                            </div>
                        </div>';
                } else {
                    $html =
                        '<div class="dropdown table-action">
                            <button class="btn btn-light-primary btn-icon btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ki ki-bold-more-ver"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a href="' . route('sub-event.detail_subevent', $data->id) . '" class="dropdown-item">
                                    Detail Sub Event
                                </a>
                            </div>
                        </div>';
                }
                return $html;
            })->EditColumn('status', function ($data) {
                switch ($data->status) {
                    default:
                        $label = "label-light-primary";
                        break;
                    case "sourcing":
                        $label = "label-light-primary";
                        break;
                    case "profiling":
                        $label = "label-light-warning";
                        break;
                    case "mapping":
                        $label = "label-light-danger";
                        break;
                    case "talent_day":
                        $label = "label-light-success";
                        break;
                    case "review":
                        $label = "label-primary";
                        break;
                    case "selection":
                        $label = "label-warning";
                        break;
                    case "completed":
                        $label = "label-dark";
                        break;
                }

                return '<span class="label font-weight-bold label-lg ' . $label . ' label-inline">' . $data->status . '</span>';
            })->AddColumn('vacant_type', function ($data) {

                switch ($data->vacant_type) {
                    default:
                        $label      = "label-light-primary";
                        $label_text = "Undefined";
                        break;
                    case "vacant_soon":
                        $label = "label-light-primary";
                        $label_text = "Vacant Soon";
                        break;
                    case "vacant":
                        $label      = "label-light-warning";
                        $label_text = "Vacant";
                        break;
                    case "incumbent":
                        $label = "label-light-danger";
                        $label_text = "Incumbent";
                        break;
                    case "ctc":
                        $label = "label-light-success";
                        $label_text = "CTC";
                        break;
                    case "semua_posisi":
                        $label = "label-primary";
                        $label_text = "General";
                        break;
                    case "higher_3year":
                        $label = "label-light-warning";
                        $label_text = "Higher than 3 year";
                        break;
                    case "external":
                        $label = "label-dark";
                        $label_text = "External Pool";
                        break;
                }

                return '<span class="label font-weight-bold label-lg ' . $label . ' label-inline">' . $label_text . '</span>';
            })
            ->editColumn('sourcingType', function ($data) {
                $sourcingType = 'Internal';
                $label = "label-light-primary";

                if ($data->sourcing_type == 1) {
                    $sourcingType = 'External';
                    $label = "label-light-success";
                }

                return '<span class="label font-weight-bold label-lg ' . $label . ' label-inline">' . $sourcingType . '</span>';
            })
            ->editColumn('name_position', function ($data) {
                return $data->name_position . '<br><span class="text-muted font-size-sm">' . $data->name_subholding . '<br>' . $data->name_area . '</span>';
            })
            ->rawColumns(['button', 'status', 'vacant_type', 'sourcingType', 'name_position'])
            ->make(true);
    }

    public function get_attribute_position($id_post)
    {
        $id = EventWindowTimeModel::whereIn('status', ['open'])->value('id');

        $aspiration = TalentAspiration::where([
            ['id_event', $id],
            ['code_position', $id_post]
        ])
            ->groupBy('nik')
            ->get();

        if (!empty($aspiration)) {
            $emp    = $aspiration->count();
        } else {
            $emp    = 0;
        }

        $data = [
            'eligible'  => $emp . ' Employee'
        ];

        return response()->json($data);;
    }

    public function detail_subevent()
    {
        $page_title = 'Detail Event';
        $page_description = 'Membuat sub event baru';
        $page_breadcrumbs = [
            ['page' => '/dashboard', 'title' => 'Home'],
            ['page' => '#', 'title' => 'Event'],
            ['page' => '#', 'title' => 'Event Position'],
            ['page' => '#', 'title' => 'Detail Event Position']
        ];

        $posisi = SubEvent::where('id', request('id'))->select('name', 'position', 'status', 'id_event')->first();
        $event_name = $posisi->name;
        $nm_posisi = Position::where('code_position', $posisi->position)
            ->select(
                'name_position',
                DB::raw('(SELECT name_area FROM master_area WHERE master_area.code_area=master_position.area) AS area_position'),
                DB::raw('(SELECT name_subholding FROM master_subholding WHERE master_subholding.code_subholding=master_position.code_subholding) AS name_subholding')
            )
            ->first();

        $data_talent = Talent::where('sub_event', request('id'))->pluck('nik');
        $data_aspiration = TalentAspiration::join('employees', 'nik', DB::raw('prev_persno COLLATE utf8mb4_unicode_ci'))
            ->whereNotIn('nik', $data_talent)
            ->where('id_event', $posisi->id_event)
            ->where('code_position', $posisi->position)
            ->get()
            ->map(function($value){
                return [
                    'id' => NULL,
                    'nama' => ucwords($value->personnel_number),
                    'proses' => "Talent Sourcing",
                    'is_email' => NULL
                ];
            })
            ->toArray();

        $data_talent = Talent::with('employee')
            ->where('sub_event', request('id'))
            ->select(
                "*",
                DB::raw('(CASE WHEN is_final="1" THEN (CASE WHEN talent.status="final" THEN "Selected" ELSE "Talent Selection" END) ' .
                    'WHEN is_review="1" THEN (CASE WHEN talent.status="selected" THEN "Talent Review [Selected]" ELSE "Talent Review" END) ' .
                    'WHEN is_day="1" THEN (CASE WHEN talent.status="selected" THEN "Talent Day [Selected]" ELSE "Talent Day" END) ' .
                    'WHEN is_mapping="1" THEN (CASE WHEN talent.status="selected" THEN "Talent Mapping [Selected]" ELSE "Talent Mapping" END) ' .
                    'WHEN is_profilling="1" THEN (CASE WHEN talent.status="selected" THEN "Talent Profiling [Selected]" ELSE "Talent Profiling" END) END) AS proses'),
            )
            ->get()
            ->map(function($value){
                return [
                    'id' => $value->id,
                    'nama' => ucwords($value->employee->personnel_number),
                    'proses' => $value->proses,
                    'is_email' => $value->is_email
                ];
            })
            ->toArray();;

        $data = array_merge($data_aspiration, $data_talent);

        $col = array_column($data, "nama");
        array_multisort($col, SORT_ASC, $data);

        $data_tcom = SubEventTalentComitee::with(['employees' => function($que) {
            $que->with(['positions' => function ($subq) {
                $subq->with(['get_sub_holding'])
                    ->with('master_area');
            }]);
        }])
        ->where('id_subevent', request('id'))
        ->get();

        $show_engage = count($data) == 0 ? true : false;
        $status = '';
        switch ($posisi->status) {
            case 'talent_day':
                $status = 'Talent Day';
                break;

            case 'completed':
                $status = 'Completed';

            default:
                $status = 'Talent ' . ucwords($posisi->status);
                break;
        }

        return view('sub-event.detail', compact(
            'page_title',
            'page_description',
            'page_breadcrumbs',
            'event_name',
            'nm_posisi',
            'data',
            'data_tcom',
            'status',
            'show_engage'
        ));
    }


    public function hapus_event($id)
    {
        $subEvent = SubEvent::find($id);
        if ($subEvent && $subEvent->talent_id_ref_from_selection) {
            Talent::where('id', $subEvent->talent_id_ref_from_selection)
                ->update([
                    'is_disposisi' => null
                ]);
        }
        SubEvent::where('id', $id)->delete();
        SubEventTalentComitee::where('id_subevent', $id)->delete();
        try {
            $pending_task = PendingTaskNotification::where('id_sub_event', $id)->first();
            if ($pending_task != null) {
                $pending_task->status = 'pending';
                $pending_task->id_sub_event = null;
                $pending_task->save();
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
        return redirect()->back()->with([
            "status" => "success",
            "title" => "Berhasil",
            "message" => "Data sub event telah dihapus"
        ]);
    }

    public function send_mail_pakta()
    {
        $data_raw = Talent::with(['sub_events' => function($query) {
            $query->with(['get_position' => function($child_query) {
                $child_query->with('sub_holding', 'master_area');
            }]);
        }])
        ->with(['employee' => function($query){
            $query->with(['positions' => function($child_query) {
                $child_query->with('sub_holding');
            }]);
        }])
        ->where('talent.id', request('id'))
        ->first();

        if ($data_raw) {
            $filename = "PAKTA INTEGRITAS - " . $data_raw->nik . " - " . $data_raw->employee->personnel_number . " - " . date('dmYhms') . ".pdf";
            $filename_kepeg = 'DOKUMEN SK - ' . $data_raw->nik . ' - ' . $data_raw->employee->personnel_number . " - " . date('dmYhms') . '.pdf';
            $name_subholding = "-";
            if($data_raw->sub_events->get_position->sub_holding) {
                if($data_raw->sub_events->get_position->sub_holding->name_subholding) {
                    $name_subholding = $data_raw->sub_events->get_position->sub_holding->name_subholding;
                }
            }
            $data[] = [
                'type' => 'pakta-integritas',
                'nama' => $data_raw->employee->personnel_number,
                'prefix' => genderPrefix($data_raw->employee->gender_text),
                'posisi' => $data_raw->sub_events->get_position->name_position,
                'area' => $data_raw->sub_events->get_position->master_area->name_area,
                'emails' => $data_raw->employee->email,
                'filename' => $filename,
                'filename_sk' => $filename_kepeg,
                'logo' => $data_raw->sub_events->get_position->sub_holding->logo,
                'name_subholding' =>  $name_subholding,
                'url' => ''
            ];

            if ($data_raw->code_subholding == 'inj') {
                $pdf = PDF::loadView('sub-event.pakta-integritas-pdf', compact('data_raw'));
            } else {
                $pdf = PDF::loadView('sub-event.pakta-integritas-sub-pdf', compact('data_raw'));
            }
            $pdf->save(public_path('pakta-integritas/' . $filename));

            Talent::where('id', request('id'))
                ->update(['is_email' => '1']);

            $pdf = PDF::loadView('sub-event.surat-kepegawaian-pdf', compact('data_raw'));
            $pdf->save(public_path('dok-kepegawaian/' . $filename_kepeg));

            $insrt = new EmployeesDocs;
            $insrt->nik = $data_raw->nik;
            $insrt->nm_peg_docs = $filename_kepeg;
            $insrt->nm_pkt_docs = $filename;
            $insrt->tgl = date('Y-m-d');
            $insrt->save();

            $job = (new SendBulkQueueEmail($data))->delay(now()->addSeconds(2));
            dispatch($job);
            return true;
        }
        return false;
    }

    public function get_talent_com() {
        $position_code = request('position');
        $subholding = Position::where('code_position', $position_code)->value('code_subholding');
        $emp_data = Employee::join('master_position', DB::raw('employees.position collate utf8mb4_unicode_ci'), 'master_position.code_position')
            ->join('master_subholding', 'master_position.code_subholding', 'master_subholding.code_subholding')
            ->whereIn('master_position.code_subholding', [$subholding, 'inj'])
            ->whereIn('master_position.org_level',['BOD'])
            ->select('prev_persno', 'personnel_number', 'master_position.name_position', 'master_subholding.name_subholding')
            ->orderBy(DB::raw('FIELD(master_position.code_subholding, "inj")'), 'DESC')
            ->orderBy('grade_align_max', 'DESC')
            ->get();
        return (object)[
            'data' => $emp_data,
            'default' => talent_committee_default($position_code)
        ];
    }
}
