<?php

namespace App\Http\Controllers\XNA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\Employee;
use App\Models\PendingTaskNotification;
use App\Models\Talent;
use App\Models\User;

use App\Models\XNAObserverModel;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class XNATestController extends Controller
{
    use Notifiable;

    public function observer()
    {
        // observer adalah event yang mentrigger aksi ketika
        // terjadi perubahan data di database
        // misal ketika data dicreate atau diupdate
        // perlu didaftarkan pula EventServiceProvidernya (boot)
        // XNAObserverModel::observe(XNAObserver::class);

        $nik = Sentinel::check()->nik;

        XNAObserverModel::create([
            'nik'     => $nik,
            'content' => "Data Event Observer"
        ]);

        return redirect("/");
    }

    public function event_and_listener()
    {
        // Event selalu berpasangan dengan Listener
        // daftarkan ini pada EventServiceProvider protect ($listen)
        // 'App\Events\XNAEvent' => [
        //     'App\Listeners\XNAListener',
        // ],
        // php artisan event:list
        // php artisan event:generate

        $data_user = [
            'nik'     => '9942',
            'content' => "Create Data Event Listener"
        ];

        event(new \App\Events\XNAEvent($data_user));

        return redirect("/");
    }

    public function pusher()
    {
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'useTLS' => true
        );

        $pusher = new \App\Pusher\Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $data['message'] = 'test Real time notification.... (xna)';
        $pusher->trigger('my-pusher-channel', 'my-event', $data);

        return redirect("/");
    }

    public function notification_via_email_and_bell()
    {

        // php artisan make:notification XNANotification --markdown=email-notif.index
        // jika datanya akan disimpan di database, generate tabel:
        // php artisan notifications:table
        // php artisan make:middleware MarkNotificationAsRead
        // daftarkan di kernel group \App\Http\Middleware\MarkNotificationAsRead::class,
        // set di tabel user
        // routeNotificationForMail()

        // https://www.sitepoint.com/add-real-time-notifications-laravel-pusher/

        $user = Sentinel::check();
        $nik = $user->nik;

        $data = \App\Models\XNANotificationModel::create([
            'nik'     => $nik,
            'content' => "Create Data Notification for bell"
        ]);

        $user->notify(new \App\Notifications\XNANotification($user, $data));
        dd("end");
    }

    public function notification_willy()
    {
        $new = new PendingTaskNotification();
        $new->jenis_notifikasi = 'position';
        $new->judul = 'position a' . ' is Empty';
        $new->deskripsi = 'tes desk ';
        $new->status = 'pending';
        $new->tgl_kosong = date('Y-m-d H:i:s');
        $new->position = 122;
        $new->job_holder = 11;
        $new->save();

        $user = Sentinel::check();
        $nik = $user->nik;
        $user->notify(new \App\Notifications\PendingTaskNotification($user, $new));
    }

    public function notification()
    {
        return Sentinel::check()->unreadNotifications()->limit(5)->get()->toArray();
    }

    public function register()
    {
        $user = User::pluck("nik")->toArray();
        $emp =  Employee::pluck("prev_persno")->toArray();

        $notexists = array_diff($emp, $user);
        foreach ($notexists as $val) {
            $credentials = [
                'nik'    => $val,
                'password' => 'itms2022',
            ];

            $user = Sentinel::registerAndActivate($credentials);
        }

        foreach ($notexists as $val) {
            $credentials = [
                'nik'    => $val
            ];

            $user = Sentinel::findByCredentials($credentials);
            $role = Sentinel::findRoleBySlug('employee');
            $role->users()->attach($user);
        }

        dd("eol");
    }

    public function scandir_image()
    {
        $parameter = [
            'employee' => ['0682162-F'],
            'position' => 'PS-00556'
        ];

        $all = ['all'];
        dd(kalkulasi_eqs($parameter, false));

        return is_completed_assessment(2);
        $data_raw = Talent::with(['sub_events' => function ($query) {
            $query->with(['get_position' => function ($child_query) {
                $child_query->with('sub_holding', 'master_area');
            }]);
        }])
            ->with(['employee' => function ($query) {
                $query->with(['positions' => function ($child_query) {
                    $child_query->with('sub_holding');
                }]);
            }])
            ->where('talent.id', request('id'))
            ->first();

        if ($data_raw) {
            $filename = "PAKTA INTEGRITAS - " . $data_raw->nik . " - " . $data_raw->employee->personnel_number . " - " . date('dmYhms') . ".pdf";
            $filename_kepeg = 'DOKUMEN SK - ' . $data_raw->nik . ' - ' . $data_raw->employee->personnel_number . " - " . date('dmYhms') . '.pdf';
            $name_subholding = "-";
            if ($data_raw->sub_events->get_position->sub_holding) {
                if ($data_raw->sub_events->get_position->sub_holding->name_subholding) {
                    $name_subholding = $data_raw->sub_events->get_position->sub_holding->name_subholding;
                }
            }
            $data = [
                'type' => 'pakta-integritas',
                'nama' => $data_raw->employee->personnel_number,
                'prefix' => genderPrefix($data_raw->employee->gender_text),
                'posisi' => $data_raw->sub_events->get_position->name_position,
                'area' => $data_raw->sub_events->get_position->master_area->name_area,
                'emails' => $data_raw->employee->email,
                'filename' => $filename,
                'filename_sk' => $filename_kepeg,
                'logo' => $data_raw->sub_events->get_position->sub_holding->logo,
                'name_subholding' =>  $name_subholding,
                'url' => ''
            ];

            if ($data_raw->code_subholding == 'inj') {
                $pdf = PDF::loadView('sub-event.pakta-integritas-pdf', compact('data_raw'));
            } else {
                $pdf = PDF::loadView('sub-event.pakta-integritas-sub-pdf', compact('data_raw'));
            }
            $pdf->save(public_path('pakta-integritas/' . $filename));

            $pdf = PDF::loadView('sub-event.surat-kepegawaian-pdf', compact('data_raw'));
            $pdf->save(public_path('dok-kepegawaian/' . $filename_kepeg));
            return Mail::to("dodyirawan@kabayan.id")->send(new \App\Mail\SendMail($data));
        }

        dd(tanggal_terbilang(date('d'), date('m'), date('Y')));
        $string = "1289118-I";
        echo '<img src="' . get_employee_pict($string) . '">';
        dd("xx");

        $subholding     = "twc";
        $folder_asal    = "public/unclean-" . $subholding;
        $folder_tujuan  = "public/" . $subholding;

        $file   = Storage::files($folder_asal);
        $arr    = [];

        switch ($subholding) {
            default:
                $regex_nama = '/[\w+]/m';
                $regex_ext  = '/\.([JPG|jpg|png|PNG|JPEG|jpeg]+)/m';
                break;

            case "ap1":
                $regex_nama =
                    $regex_ext  = '/([0-9]+\-\w{1})(.*)\.([JPG|jpg|png|PNG|JPEG|jpeg]+)/m';
                $shift_name = 1;
                $shift_ext  = 3;

                break;

            case "ap2":
            case "itdc":
            case "snh":
            case "twc":
                $regex_nama =
                    $regex_ext  = '/([0-9]+)[\_\w\s]+\.([JPG|jpg|png|PNG|JPEG|jpeg]+)/m';
                $shift_name = 1;
                $shift_ext  = 2;
                break;

            case "hin":
                $regex_nama =
                    $regex_ext  = '/([0-9]+)(\s{1}\-\s{1}+)(.*)\.([JPG|jpg|png|PNG|JPEG|jpeg]+)/m';
                $shift_name = 1;
                $shift_ext  = 4;
                break;
        }

        foreach ($file as $f) {
            $string         = str_replace($folder_asal . "/", "", $f);

            preg_match_all($regex_nama, $string, $nama_files, PREG_SET_ORDER, 0);
            // dd($nama_files);
            if (empty($nama_files)) {
                continue;
            }

            preg_match_all($regex_ext, $string, $extension, PREG_SET_ORDER, 0);
            $ext = array_shift($extension)[$shift_ext];

            if (!in_array($ext, ["JPG", "jpg", "png", "PNG", "JPEG", "jpeg"])) {
                continue;
            }

            $nama_file      = array_shift($nama_files)[$shift_name];
            $destination    = public_path(DIRECTORY_SEPARATOR . $folder_tujuan . DIRECTORY_SEPARATOR . $nama_file . "." . $ext);
            $new_name       = $folder_tujuan . DIRECTORY_SEPARATOR . $nama_file . "." . $ext;
            $exists         = Storage::disk('local')->exists($new_name);

            if (in_array($nama_file, $arr)) {
                continue;
            }

            if (!$exists) {
                Storage::move($f, $new_name);
            }
        }

        dd(Storage::files($folder_tujuan));
    }

    public function rule_aspiration()
    {
        $employee = DB::table('employees')
            ->whereNull('employees.deleted_at')
            ->join('master_position', 'master_position.code_position', '=', DB::raw("employees.position collate utf8mb4_unicode_ci"))
            ->get()
            ->map(function ($value) {
                return [
                    'nik' => $value->prev_persno,
                    'nama' => $value->personnel_number,
                    'status_karyawam' => $value->status_karyawan,
                    'org_level' => $value->org_level,
                    'aspirasi' => rule_aspiration($value->prev_persno, 'menu_test')
                ];
            });
        return response()->json($employee, 200);
    }

    public function gender_employee()
    {
        $employee = DB::table('employees')
            ->whereNull('employees.deleted_at')
            ->get()
            ->map(function ($value) {
                return [
                    'nik' => $value->prev_persno,
                    'nama' => $value->personnel_number,
                    'gender' => genderPrefix($value->gender_text)
                ];
            })
            ->toArray();
        return response()->json($employee, 200);
    }

    public function cluster_employee()
    {
        // Get nik employee
        $prevPersno = Employee::get()->pluck('prev_persno')
            ->toArray();
        // Filter nik null
        $filterPrevPersno = array_filter($prevPersno);
        $dataEqs = kalkulasi_eqs(['employee' => $filterPrevPersno], false);
        // Menentukan employee dengan box
        $boxes = array_map(function ($value) {
            $performance_index = ($value['kinerja'] * 80 / 100) + ($value['track_record'] * 20 / 100);
            return [
                'nik'               => $value['nik'],
                'kinerja'           => $value['kinerja'],
                'track_record'      => $value['track_record'],
                'performance_index' => $performance_index,
                'job_fit'           => $value['job_fit'],
                'box'               => talentMapping($value['job_fit'], $performance_index),
                'predikat'          => predikat_talent_cluster(talentMapping($value['job_fit'], $performance_index)),
            ];
        }, $dataEqs);
        dd($boxes);
        return response()->json($boxes, 200);
    }
}
