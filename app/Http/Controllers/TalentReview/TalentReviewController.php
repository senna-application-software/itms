<?php

namespace App\Http\Controllers\TalentReview;

use App\Models\Ba_event;
use App\Http\Controllers\Controller;
use App\Http\Controllers\TalentPlanning\TalentSourcingController;
use Illuminate\Http\Request;

use App\Models\Employee;
use App\Models\SubEvent;
use App\Models\Talent;
use App\Models\TalentAspiration;
use App\Models\EventWindowTimeModel;
use App\Jobs\SendBulkQueueEmail;
use PDF;
use DB;

class TalentReviewController extends Controller
{
    private $event_active = 0;
    private $event_name = "";
    private $event_start = [];
    private $event_end = [];
    private $event_position_eligible = [];
    private $event_employee_eligible = [];

    public function __construct() {
        $raw = EventWindowTimeModel::where('status', 'open')->limit(1)->select('id', 'name', 'start_date', 'end_date')->first();
        if($raw) {
            $this->event_active = $raw->id;
            $this->event_name = $raw->name;
            $this->event_start = date('j M', strtotime($raw->start_date));
            $this->event_end = date('j M', strtotime($raw->end_date));
        }
    }

    public function index(){
        $position   = SubEvent::select('master_position.name_position', 'sub_event.status', 'sub_event.id',
            'master_subholding.name_subholding','master_area.name_area','sub_event.sourcing_type',
            DB::raw('(SELECT name_position FROM master_position WHERE master_position.code_position=sub_event.position) AS name_position'),
            DB::raw('(SELECT count(distinct(nik)) FROM talent WHERE talent.sub_event=sub_event.id AND talent.status="selected" AND talent.is_day="1") AS participant'),
            DB::raw('(SELECT count(distinct(nik)) FROM talent_aspiration WHERE talent_aspiration.id_event=sub_event.id_event AND talent_aspiration.code_position=sub_event.position) AS total')

        )
        ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
        ->join('event_window_time', 'event_window_time.id', '=', 'sub_event.id_event')
        ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
        ->join('master_area', 'master_area.code_area', 'master_position.area')
        ->where([
            ['event_window_time.status', 'open'],
            ['sub_event.status', '=', 'review']
        ])
        ->get();

        $page_title = 'Talent Review';
        $page_description = 'Halaman Talent Review (Career Cards)';
        $show_engage = $this->event_active != 0 ? false : true;
        $page_breadcrumbs = [ 
            ['page' => 'dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Talent Review']
        ];

        $show_engage = $this->event_active != 0 ? false : true; 

        return view('talent-review.index', compact('page_title', 'page_description','page_breadcrumbs', 'position','show_engage'));
    }

    public function detail($id){
        $sub_event  = SubEvent::select(
            'master_position.name_position', 
            'master_position.code_position', 
            'sub_event.id',
            'sub_event.id_event', 
            'sub_event.sourcing_type', 
            'master_subholding.name_subholding', 
            'master_area.name_area'
        )
        ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
        ->join('master_subholding', 'master_subholding.code_subholding', 'master_position.code_subholding')
        ->join('master_area', 'master_area.code_area', 'master_position.area')
        ->where('sub_event.id', '=', $id)
        ->first();

        $year_end = date('Y') - 1;
        $year_start = $year_end - 3;

        $data   = Talent::select('talent.*', 'talent_aspiration.comitte_proposal')
        ->with(['employee' => function($query) use($year_start, $year_end) {
            $query->with(['performance' => function($query) use($year_end, $year_start) {
                $query->whereBetween('year', [$year_start, $year_end]);
            }]);
            $query->with('master_posisi');
        }])
        ->join('talent_aspiration', 'talent.nik', '=', 'talent_aspiration.nik')
        ->where([
            ['talent.sub_event', '=', $id],
            ['talent.status', '=', 'selected'],
            ['talent.is_day', '=', '1'],
            ['talent.status', '=', 'selected']
        ])
        ->groupBy('talent.nik')
        ->get();

        $page_title = 'Talent Review';
        $page_description = 'Halaman Talent Review (Career Cards)';
        $page_breadcrumbs = [ 
            ['page' => '/dashboard', 'title' =>'Home'],
            ['page' => '#', 'title' =>'Talent Review'],
            ['page' => '#', 'title' =>'Detail']
        ];

        return view('talent-review.detail', compact('page_title', 'page_description','page_breadcrumbs', 'data', 'sub_event'));
    }

    //method untuk keperluan data pop up career card
    public function view_career_card($nik)
    {
        $data = data_career_card($nik);
        return view('talent-profiling.element.career_card_plain', $data);
    }

    public function store(Request $request)
    {
        if ($request->tag != null) {
            $talent   = $request->talent_id;
            $id_sub_event = $request->sub_event;
            foreach($talent as $item){
                //cari panel
                $get_talent = Talent::where('id', '=', $item)->first();
                $update = Talent::where('id', '=', $item)
                ->update([
                    'is_review' => '1'
                ]);

            }

            Talent::whereNotIn('id', $talent)
            ->where('sub_event', $id_sub_event)
            ->where('is_day', '1')
            ->where('status', 'selected')
            ->update([
                'status' => 'terminated',
                'is_review' => '1'
            ]);
            
            $sub_event  = SubEvent::where('id', '=', $request->sub_event)
            ->update([
                'status' => 'selection'
            ]);
                
                /*
                $year_end = date('Y') - 1;
                $year_start = $year_end - 3;
        
                $dataxx   = Talent::with(['employee' => function($query) use($year_start, $year_end) {
                    $query->with(['performance' => function($query) use($year_end, $year_start) {
                        $query->whereBetween('year', [$year_start, $year_end]);
                    }]);
                }])->where([
                    ['talent.sub_event', '=', $id_sub_event],
                    ['talent.status', '=', 'selected']
                ])->whereIn('id',$talent)->get();
                $data = [];
                foreach ($dataxx as $key => $v) {
                    $data [] = [
                        'type'          => 'talent_review',
                        'prefix'        => genderPrefix($v->employee->gender_text),
                        'sub_prefix'    => genderSubprefix($v->employee->gender_text),
                        'nama' => $v->employee->personnel_number,
                        'emails' => $v->employee->email,
                        'url' => ''
                    ];
                }
                if (count($data) > 0) { 
                    // send çall mail in the queue.
                    $job = (new SendBulkQueueEmail($data))->delay(now()->addSeconds(2)); 
                    dispatch($job);
        
                    $msg = "please check your inbox for details";
                } else {
                    $msg = "Tidak ada perseta yang eligible. Tidak ada email yang akan dikirim untung mengundang peserta.";
                }
                */
            
                return redirect()->route('talent-review.index')->with([
                    "id_event" => $id_sub_event,
                    "status" => "success", 
                    "title" => "Berhasil",
                    "message" => "Data Talent Review has successfully selected"
                ]);
        }else{
            $sub_event_id = $request->sub_event;
            $sub_event  = SubEvent::select('master_position.name_position', 'master_position.code_position', 'master_position.code_job_family', 'sub_event.id', 'sub_event.id_event', 'sub_event.id_event')
            ->join('master_position', 'master_position.code_position', '=', 'sub_event.position')
            ->where('sub_event.id', '=', $sub_event_id)
            ->first();
            $talent   = $request->talent_id;
            foreach($talent as $item){
                //cari panel
                $get_talent = Talent::where('id', '=', $item)->first();
                $panel   = talentMapping($get_talent->cci, $get_talent->performance);            
            }
            $year_end = date('Y') - 1;
            $year_start = $year_end - 3;

            $data   = Talent::with(['employee' => function($query) use($year_start, $year_end) {
                $query->with(['performance' => function($query) use($year_end, $year_start) {
                    $query->whereBetween('year', [$year_start, $year_end]);
                }]);
                $query->with('master_posisi');
                $query->with(['positions' => function($q) {
                    $q->with('masterPositionType');
                }]);
            }])->with('event')->with('employee_to_position')->where([
                ['talent.sub_event', '=', $sub_event_id],
                ['talent.status', '=', 'selected']
            ])
            ->with(["box" => function($query) {
                $query->with("box_cat");
            }])->whereIn('id',$talent)
            ->get()->transform(function($value, $key) use($sub_event_id) {
                $value->eqs = (int) substr(nilai_eqs($value->employee->prev_persno, false, $sub_event_id), 0, 3);
                return $value;
            })->sortByDesc('eqs');
    
            $page_title = 'Talent Comparison';
            $page_description = 'Halaman Talent Comparison (Career Cards)';
            $page_breadcrumbs = [ 
                ['page' => '/dashboard', 'title' =>'Home'],
                ['page' => '#', 'title' =>'Talent Review'],
                ['page' => '#', 'title' =>'Compare']
            ];
    
            return view('talent-review/comparation', compact('page_title', 'page_description','page_breadcrumbs','data', 'sub_event'));
        }

    }
    
    public function GenerateBeritaAcara($id_sub_event)
    {
        $tc = data_ba_committee($id_sub_event);
        $nomor_ba = generate_no_ba("TR", $id_sub_event);  
        
        $sub_event = DB::select("
            select talent.nik, 
            talent.talent_day_result as result,
            if(talent.is_change_box = '1', talent.proposed_box, talent.panel) as talent_cluster,
            employees.personnel_number,
            employees.prev_persno,
            employees.position_name,
            employees.position as kode_posisi,
            sub_event.talent_com_1,
            sub_event.talent_com_2,
            sub_event.talent_com_3,
            sub_event.position,
            sub_event.id as id_sub_event,
            master_position.name_position,
            master_position.code_subholding,
            master_subholding.name_subholding,
            ba_event.no_ba,
            talent.id as id_talent
            from talent 
            left join ba_event on ba_event.id_sub_event = talent.sub_event
            left join employees on employees.prev_persno collate utf8mb4_unicode_ci = talent.nik
            left join sub_event on talent.sub_event = sub_event.id
            left join master_position on master_position.code_position = sub_event.position
            left join master_subholding on master_subholding.code_subholding = master_position.code_subholding
            where talent.sub_event = '".$id_sub_event."' and ba_event.prefix = 'TR' and talent.status = 'selected'
        ");
        // dd($sub_event);
        // return view('talent-review.berita-acara', compact('sub_event','nomor_ba','tc'));        

        
        $exist = Ba_event::where('prefix', 'TR')->where('id_sub_event', $id_sub_event)->where('tanggal_ba', date("Y-m-d"))->first();
        if ($exist != null) {
            Ba_event::where('id', $exist->id)
                ->update([
                    'data_ba' => json_encode($sub_event),
                    'data_commite' => json_encode($tc)
                ]);
                $pdf = PDF::loadView('talent-review.berita-acara', compact('sub_event','nomor_ba','tc') );    
                return $pdf->download('BERITA ACARA KESEPAKATAN HASIL PENILAIAN TALENT REVIEW.pdf');
        }else{
            return redirect()->route('talent-review.index');
        }
    }
}
