<?php

namespace App\Http\Controllers\MyProfile;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeChildren;
use App\Models\EmployeeDetailHistoryPosition;
use App\Models\EmployeeDiklat;
use App\Models\EmployeeExpAcara;
use App\Models\EmployeeJabatanBumn;
use App\Models\EmployeeKarya;
use App\Models\EmployeeKeahlian;
use App\Models\EmployeeKelas;
use App\Models\EmployeeKluster;
use App\Models\EmployeeMasterExperience;
use App\Models\EmployeeOrganization;
use App\Models\EmployeePasangan;
use App\Models\EmployeePenghargaan;
use App\Models\EmployeePenugasan;
use App\Models\EmployeeReferensi;
use App\Models\EmployeesDocs;
use App\Models\EmployeesStatusUpdate;
use App\Models\EmployeesUraian;
use App\Models\EventWindowTimeModel;
use App\Models\FungsiJabatanBumn;
use App\Models\Keahlian;
use App\Models\KlusterBumn;
use App\Models\Pendidikan;
use App\Models\PengalamanKelasBumn;
use App\Models\Position;
use App\Models\PositionHistory;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;

class MyProfileController extends Controller
{
    public function index($onlydata = false, $nik = 0)
    {
        if (!Sentinel::inRole("employee") && $onlydata == false) {
            abort("403", "Maaf, Anda Tidak diizinkan akses ke halaman ini");
        }

        if (!$onlydata) {
            $nik = Sentinel::getUser()->nik;
            $keahlian = Keahlian::select('id', 'nama_keahlian')->get();
        } else {
            $keahlian = Keahlian::leftJoin('employee_keahlian', 'master_keahlian.id', 'employee_keahlian.id_keahlian')
                ->where('employee_keahlian.nik', $nik)
                ->select('nama_keahlian')
                ->get();
        }

        $history_jabatan = PositionHistory::where('nik', $nik)
            ->select('id', 'start_date', 'end_date', 'position_name', 'uraian_singkat', 'achievement', 'area_name')
            ->orderBy('id', 'DESC')
            ->get();

        $penugasan = EmployeePenugasan::where('nik', $nik)
            ->select('id', 'penugasan', 'tupoksi', 'instansi_perusahaan', 'start_date', 'end_date')
            ->get();

        $organisasi_formal = EmployeeOrganization::select('id', 'nama_keg_org', 'jabatan', 'start_date', 'end_date', 'uraian_singkat')
            ->where('nik', $nik)
            ->where('type', 'formal')
            ->get();

        $organisasi_nonformal = EmployeeOrganization::select('id', 'nama_keg_org', 'jabatan', 'start_date', 'end_date', 'uraian_singkat')
            ->where('nik', $nik)
            ->where('type', 'nonformal')
            ->get();

        $penghargaan = EmployeePenghargaan::where('nik', $nik)
            ->select('id', 'jenis_penghargaan', 'tingkat', 'diberikan_oleh', 'tahun')
            ->get();

        $pendidikan = Pendidikan::leftJoin('master_education_level', 'master_education.level_education', 'master_education_level.code')
            ->where('nik', $nik)
            ->select('master_education.id','master_education.nik', 'level_education', 'major_name', 'university_name', 'tahun_lulus', 'kota_negara', 'penghargaan')
            ->orderBy('master_education_level.id', 'DESC')
            ->get();

        $diklat_jabatan = EmployeeDiklat::where('nik', $nik)
            ->where('jenis', 'jabatan')
            ->select('id', 'nama_kegiatan', 'penyelenggara', 'lama', 'nomor_sertifikasi')
            ->get();

        $diklat_fungsional = EmployeeDiklat::where('nik', $nik)
            ->where('jenis', 'fungsional')
            ->select('id', 'nama_kegiatan', 'penyelenggara', 'lama', 'nomor_sertifikasi')
            ->get();

        $karya = EmployeeKarya::where('nik', $nik)
            ->select('id', 'judul', 'tahun')
            ->get();

        $exp_acara = EmployeeExpAcara::where('nik', $nik)
            ->select('id', 'nik', 'acara_tema', 'penyelenggara', 'periode', 'lokasi_peserta')
            ->get();

        $referensi = EmployeeReferensi::where('nik', $nik)
            ->select('id', 'nama', 'perusahaan', 'jabatan', 'no_hp')
            ->get();

        $pasangan = EmployeePasangan::where('nik', $nik)
            ->select('id', 'nama', 'tmp_lahir', 'tgl_lahir', 'tgl_menikah', 'pekerjaan', 'keterangan')
            ->get();

        $children = EmployeeChildren::where('nik', $nik)
            ->select('id', 'nama', 'tmp_lahir', 'tgl_lahir', 'jns_kelamin', 'pekerjaan', 'keterangan')
            ->get();

        $pengalaman = EmployeeMasterExperience::where('nik', $nik)
            ->where('type', 'pengalaman')
            ->select('id', 'name', 'deskripsi')
            ->get();

        $emp_keahlian = EmployeeMasterExperience::where('nik', $nik)
            ->where('type', 'keahlian')
            ->select('id', 'name', 'deskripsi')
            ->get();

        $uraian_pengalaman = EmployeesUraian::where('nik', $nik)
            ->where('jenis', 'pengalaman')
            ->value('uraian');

        $uraian_aspirasi = EmployeesUraian::where('nik', $nik)
            ->where('jenis', 'aspirasi')
            ->value('uraian');

        if($onlydata){
            $master_kelas_bumn = PengalamanKelasBumn::join('employees_kelas', 'master_kelas_bumn.id', 'employees_kelas.id_kelas')
                ->where('employees_kelas.nik', $nik)
                ->where('employees_kelas.status', '1')
                ->where('employees_kelas.type', 'pengalaman')
                ->select('master_kelas_bumn.name')
                ->get();
            $master_klaster_bumn = KlusterBumn::join('employees_klaster', 'master_klaster_bumn.id', 'employees_klaster.id_kluster')
                ->where('employees_klaster.nik', $nik)
                ->where('employees_klaster.status', '1')
                ->where('employees_klaster.type', 'pengalaman')
                ->select('master_klaster_bumn.name')
                ->get();
            $master_fungsi_jabatan_bumn = FungsiJabatanBumn::join('employees_jbtn_bumn', 'master_fungsi_jabatan_bumn.id', 'employees_jbtn_bumn.id_jbtn_bumn')
                ->where('employees_jbtn_bumn.nik', $nik)
                ->where('employees_jbtn_bumn.status', '1')
                ->where('employees_jbtn_bumn.type', 'pengalaman')
                ->select('master_fungsi_jabatan_bumn.name')
                ->get();
            $master_asp_kelas_bumn = PengalamanKelasBumn::join('employees_kelas', 'master_kelas_bumn.id', 'employees_kelas.id_kelas')
                ->where('employees_kelas.nik', $nik)
                ->where('employees_kelas.status', '1')
                ->where('employees_kelas.type', 'pengalaman')
                ->select('master_kelas_bumn.name')
                ->get();
            $master_asp_klaster_bumn = KlusterBumn::join('employees_klaster', 'master_klaster_bumn.id', 'employees_klaster.id_kluster')
                ->where('employees_klaster.nik', $nik)
                ->where('employees_klaster.status', '1')
                ->where('employees_klaster.type', 'pengalaman')
                ->select('master_klaster_bumn.name')
                ->get();
            $master_asp_fungsi_jabatan_bumn = FungsiJabatanBumn::join('employees_jbtn_bumn', 'master_fungsi_jabatan_bumn.id', 'employees_jbtn_bumn.id_jbtn_bumn')
                ->where('employees_jbtn_bumn.nik', $nik)
                ->where('employees_jbtn_bumn.status', '1')
                ->where('employees_jbtn_bumn.type', 'pengalaman')
                ->select('master_fungsi_jabatan_bumn.name')
                ->get();
        } else {
            $master_kelas_bumn = PengalamanKelasBumn::get();
            $master_klaster_bumn = KlusterBumn::get();
            $master_fungsi_jabatan_bumn = FungsiJabatanBumn::get();
            $master_asp_kelas_bumn = '';
            $master_asp_klaster_bumn = '';
            $master_asp_fungsi_jabatan_bumn = '';
        }

        $page_title = 'My Profile';
        $page_description = 'Update Profile';
        $page_breadcrumbs = [
            ['page' => '/dashboard', 'title' => 'Personal Information']
        ];

        $employee = Employee::where('prev_persno', $nik)->first();

        $position = Position::where('code_position', $employee->position)
            ->first();

        $docs = EmployeesDocs::where('nik', $nik)
            ->select('id', 'nm_peg_docs', 'nm_pkt_docs', 'tgl')
            ->get();

        if ($onlydata) {
            return compact(
                'keahlian',
                'history_jabatan',
                'penugasan',
                'organisasi_formal',
                'organisasi_nonformal',
                'penghargaan',
                'pendidikan',
                'diklat_jabatan',
                'diklat_fungsional',
                'karya',
                'exp_acara',
                'referensi',
                'pasangan',
                'children',
                'pengalaman',
                'emp_keahlian',
                'master_kelas_bumn',
                'master_klaster_bumn',
                'master_fungsi_jabatan_bumn',
                'uraian_pengalaman',
                'uraian_aspirasi',
                'employee',
                'master_asp_kelas_bumn',
                'master_asp_klaster_bumn',
                'master_asp_fungsi_jabatan_bumn',
                'docs',
                'position'
            );
        }

        $docs = EmployeesDocs::where('nik', Sentinel::getUser()->nik)
            ->select('id', 'nm_peg_docs', 'nm_pkt_docs', 'tgl')
            ->get();

        return view('my-profile.index', compact(
            'page_title',
            'page_description',
            'page_breadcrumbs',
            'keahlian',
            'history_jabatan',
            'penugasan',
            'organisasi_formal',
            'organisasi_nonformal',
            'penghargaan',
            'pendidikan',
            'diklat_jabatan',
            'diklat_fungsional',
            'karya',
            'exp_acara',
            'referensi',
            'pasangan',
            'children',
            'pengalaman',
            'emp_keahlian',
            'master_kelas_bumn',
            'master_klaster_bumn',
            'master_fungsi_jabatan_bumn',
            'uraian_pengalaman',
            'uraian_aspirasi',
            'docs'
        ));
    }

    public function clone_data($nik = 0)
    {
        return Self::index(true, $nik);
    }

    public function update_profile()
    {
        $uid = Sentinel::getUser()->id;
        if (request()->hasFile('profile_avatar')) {
            if (Sentinel::getUser()->image != "default.svg" && file_exists(public_path('userprofile/') . Sentinel::getUser()->image)) {
                unlink(public_path('userprofile/') . Sentinel::getUser()->image);
            }
            $file = request()->file('profile_avatar');
            $filename = uniqid('img_') . '.' . $file->getClientOriginalExtension();
            $file->move('userprofile', $filename);
            User::where('id', $uid)->update(['image' => $filename]);
        }

        Employee::where('prev_persno', Sentinel::getUser()->nik)
            ->update([
                'personnel_number' => request('full_name'),
                'no_ktp' => request('nik'),
                'birthplace' => request('birth_place'),
                'date_of_birth' => request('birth_date'),
                'gender_text' => request('jenis_kelamin'),
                'religious_denomination' => request('religion'),
                'sosmed_address' => request('sosmed_addr'),
                'position_name' => request('jabatan'),
                'no_hp' => request('hp'),
                'email' => request('email'),
                'no_npwp' => request('npwp'),
                'home_address' => request('alamat_rumah')
            ]);

        $ewt = EventWindowTimeModel::where('status', 'open')->value('id');
        EmployeesStatusUpdate::updateOrCreate(
            [
                'event_window_time' => $ewt,
                'nik' => Sentinel::getUser()->nik
            ],
            [
                'is_ket_perorangan' => '1'
            ]
        );

        return redirect()->back()->with([
            "status" => "success",
            "title" => "Berhasil",
            "message" => "Data Berhasil Disimpan"
        ]);
    }

    public function update_interest()
    {
        DB::beginTransaction();
        try {
            Employee::where('prev_persno', Sentinel::getUser()->nik)
                ->update([
                    'interest' => request('val'),
                ]);
            $ewt = EventWindowTimeModel::where('status', 'open')->value('id');
            EmployeesStatusUpdate::updateOrCreate(
                [
                    'event_window_time' => $ewt,
                    'nik' => Sentinel::getUser()->nik
                ],
                [
                    'is_interest' => '1'
                ]
            );
            DB::commit();
            return [
                "status" => "success",
                "title" => "Berhasil",
                "message" => "Data Berhasil Disimpan"
            ];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return [
                "status" => "failed",
                "title" => "Gagal",
                "message" => "Data Gagal Disimpan"
            ];
        }
    }

    public function get_keahlian()
    {
        return EmployeeKeahlian::where('nik', Sentinel::getUser()->nik)
            ->where('checked', '1')
            ->select('id_keahlian', 'keterangan')
            ->get();
    }

    public function update_checkbox()
    {
        $nik = Sentinel::getUser()->nik;
        EmployeeKeahlian::where('nik', $nik)->delete();

        if (request('keahlian')) {
            foreach (request('keahlian') as $value) {
                $insert = new EmployeeKeahlian;
                $insert->id_keahlian = $value;
                $insert->nik = $nik;
                if (request('keahlian_lain') && $value == '23') {
                    $insert->keterangan = request('keahlian_lain');
                }
                $insert->checked = '1';
                $insert->save();
            }
        }

        $ewt = EventWindowTimeModel::where('status', 'open')->value('id');
        EmployeesStatusUpdate::updateOrCreate(
            [
                'event_window_time' => $ewt,
                'nik' => Sentinel::getUser()->nik
            ],
            [
                'is_keahlian' => '1'
            ]
        );

        return redirect()->back()->with([
            "status" => "success",
            "title" => "Berhasil",
            "message" => "Data Berhasil Disimpan"
        ]);
    }

    public function init_riwayat_two()
    {
        $insert = new EmployeePenugasan;
        $insert->nik = Sentinel::getUser()->nik;
        $insert->save();
        return $insert->id;
    }

    public function update_riwayat_two()
    {

        $value = request('value');
        $data = [];
        if (request('sct') == "penugasan") {
            $data = [
                'penugasan' => $value
            ];
        } else if (request('sct') == "tupoksi") {
            $data = [
                'tupoksi' => $value
            ];
        } else if (request('sct') == "start-date") {
            $data = [
                'start_date' => $value
            ];
        } else if (request('sct') == "end-date") {
            $data = [
                'end_date' => $value
            ];
        } else if (request('sct') == "instansi") {
            $data = [
                'instansi_perusahaan' => $value
            ];
        }

        EmployeePenugasan::where('nik', Sentinel::getUser()->nik)
            ->where('id', request('pk'))
            ->update($data);

        return $value;
    }

    public function init_organization()
    {
        $insert = new EmployeeOrganization;
        $insert->nik = Sentinel::getUser()->nik;
        if (request('sct') == "formal") {
            $insert->type = "formal";
        } else if (request('sct') == "nonformal") {
            $insert->type = "nonformal";
        }
        $insert->save();
        return $insert->id;
    }

    public function update_organization()
    {
        $data = [];
        $value = request('value');
        $section = request('sct');
        if ($section == "nm-keg") {
            $data = [
                'nama_keg_org' => $value
            ];
        } else if ($section == "jabatan") {
            $data = [
                'jabatan' => $value
            ];
        } else if ($section == "start-date") {
            $data = [
                'start_date' => $value
            ];
        } else if ($section == "uraian") {
            $data = [
                'uraian_singkat' => $value
            ];
        } else if ($section == "end-date") {
            $data = [
                'end_date' => $value
            ];
        }

        EmployeeOrganization::where('id', request('pk'))
            ->update($data);
        return $value;
    }

    public function init_reward()
    {
        $insert = new EmployeePenghargaan;
        $insert->nik = Sentinel::getUser()->nik;
        $insert->save();
        return $insert->id;
    }

    public function update_reward()
    {
        $data = [];
        $value = request('value');
        $section = request('sct');
        if ($section == "jns-rwd") {
            $data = [
                'jenis_penghargaan' => $value
            ];
        } else if ($section == "tingkat-rwd") {
            $data = [
                'tingkat' => $value
            ];
        } else if ($section == "diberikan-rwd") {
            $data = [
                'diberikan_oleh' => $value
            ];
        } else if ($section == "tahun") {
            $data = [
                'tahun' => $value
            ];
        }
        EmployeePenghargaan::where('id', request('pk'))
            ->update($data);
        return $value;
    }

    public function update_pendidikan()
    {
        $data = [];
        $value = request('value');
        $section = request('sct');
        if ($section == 'kt-ngr') {
            $data = [
                'kota_negara' => $value
            ];
        } else if ($section == 'rwd-formal') {
            $data = [
                'penghargaan' => $value
            ];
        } else if ($section == 'lvl') {
            $data = [
                'level_education' => $value
            ];
        } else if ($section == "tahun") {
            $data = [
                'tahun_lulus' => $value
            ];
        } else if ($section == "unimnm") {
            $data = [
                'university_name' => $value
            ];
        } else if ($section == "mjr") {
            $data = [
                'major_name' => $value
            ];
        }

        Pendidikan::updateOrCreate(
            [
                'id' => request('pk'),
                'nik' => Sentinel::getUser()->nik
            ],
            $data
        );

        return $value;
    }

    public function init_diklat()
    {
        $sct = request('sct');
        $insert = new EmployeeDiklat;
        $insert->nik = Sentinel::getUser()->nik;
        if ($sct == 'jbtn') {
            $insert->jenis = "jabatan";
        } else if ($sct == 'fnctional') {
            $insert->jenis = "fungsional";
        }
        $insert->save();
        return $insert->id;
    }

    public function update_diklat()
    {
        $form = request('frm');
        $value = request('value');
        $data = [];
        if ($form == "nmkeg") {
            $data = [
                'nama_kegiatan' => $value
            ];
        } else if ($form == "pny") {
            $data = [
                'penyelenggara' => $value
            ];
        } else if ($form == "dur") {
            $data = [
                'lama' => $value
            ];
        } else if ($form == "stfkt") {
            $data = [
                'nomor_sertifikasi' => $value
            ];
        }

        EmployeeDiklat::where('id', request('pk'))
            ->where('nik', Sentinel::getUser()->nik)
            ->update($data);

        return $value;
    }

    public function init_karya()
    {
        $insert = new EmployeeKarya;
        $insert->nik = Sentinel::getUser()->nik;
        $insert->save();
        return $insert->id;
    }

    public function update_karya()
    {
        $value = request('value');
        $section = request('sct');
        $data = [];
        if ($section == "jdl") {
            $data = [
                'judul' => $value
            ];
        } else if ($section == "thn") {
            $data = [
                'tahun' => $value
            ];
        }

        EmployeeKarya::where('id', request('pk'))
            ->where('nik', Sentinel::getUser()->nik)
            ->update($data);

        return $value;
    }

    public function init_exp_acara()
    {
        $insert = new EmployeeExpAcara;
        $insert->nik = Sentinel::getUser()->nik;
        $insert->save();
        return $insert->id;
    }

    public function update_exp_acara()
    {
        $value = request('value');
        $section = request('sct');
        $data = [];

        if ($section == "acr") {
            $data = [
                'acara_tema' => $value
            ];
        } else if ($section == "pny") {
            $data = [
                'penyelenggara' => $value
            ];
        } else if ($section == "period") {
            $data = [
                'periode' => $value
            ];
        } else if ($section == "lok") {
            $data = [
                'lokasi_peserta' => $value
            ];
        }

        EmployeeExpAcara::where('id', request('pk'))
            ->where('nik', Sentinel::getUser()->nik)
            ->update($data);

        return $value;
    }

    public function init_reference()
    {
        $insert = new EmployeeReferensi;
        $insert->nik = Sentinel::getUser()->nik;
        $insert->save();
        return $insert->id;
    }

    public function update_reference()
    {
        $value = request('value');
        $section = request('sct');
        $data = [];

        if ($section == "nm") {
            $data = [
                'nama' => $value
            ];
        } else if ($section == "comp") {
            $data = [
                'perusahaan' => $value
            ];
        } else if ($section == "jbtn") {
            $data = [
                'jabatan' => $value
            ];
        } else if ($section == "nohp") {
            $data = [
                'no_hp' => $value
            ];
        }

        EmployeeReferensi::where('id', request('pk'))
            ->where('nik', Sentinel::getUser()->nik)
            ->update($data);

        return $value;
    }

    public function init_pasangan()
    {
        $insert = new EmployeePasangan;
        $insert->nik = Sentinel::getUser()->nik;
        $insert->save();
        return $insert->id;
    }

    public function update_pasangan()
    {
        $value = request('value');
        $section = request('sct');
        $data = [];

        if ($section == "nm") {
            $data = [
                'nama' => $value
            ];
        } else if ($section == "tmp") {
            $data = [
                'tmp_lahir' => $value
            ];
        } else if ($section == "tgl-lhr") {
            $data = [
                'tgl_lahir' => $value
            ];
        } else if ($section == "tgl-nkh") {
            $data = [
                'tgl_menikah' => $value
            ];
        } else if ($section == "pkrjaan") {
            $data = [
                'pekerjaan' => $value
            ];
        } else if ($section == "ket") {
            $data = [
                'keterangan' => $value
            ];
        }

        EmployeePasangan::where('id', request('pk'))
            ->where('nik', Sentinel::getUser()->nik)
            ->update($data);

        return $value;
    }

    public function init_children()
    {
        $insert = new EmployeeChildren;
        $insert->nik = Sentinel::getUser()->nik;
        $insert->save();
        return $insert->id;
    }

    public function update_children()
    {
        $value = request('value');
        $section = request('sct');
        $data = [];

        if ($section == "nm") {
            $data = [
                'nama' => $value
            ];
        } else if ($section == "tmp") {
            $data = [
                'tmp_lahir' => $value
            ];
        } else if ($section == "tgl-lhr") {
            $data = [
                'tgl_lahir' => $value
            ];
        } else if ($section == "jns-kel") {
            $data = [
                'jns_kelamin' => $value
            ];
        } else if ($section == "pkrjaan") {
            $data = [
                'pekerjaan' => $value
            ];
        } else if ($section == "ket") {
            $data = [
                'keterangan' => $value
            ];
        }

        EmployeeChildren::where('id', request('pk'))
            ->where('nik', Sentinel::getUser()->nik)
            ->update($data);

        return $value;
    }

    public function init_pengalaman()
    {
        $insert = new EmployeeMasterExperience;
        $insert->nik = Sentinel::getUser()->nik;
        if (request('sct') == 'exp') {
            $insert->type = 'pengalaman';
        } else if (request('sct') == 'mstr') {
            $insert->type = 'keahlian';
        }
        $insert->save();
        return $insert->id;
    }

    public function update_pengalaman()
    {
        $value = request('value');
        $section = request('sct');
        $data = [];

        if ($section == "nm") {
            $data = [
                'name' => $value
            ];
        } else if ($section == "desc") {
            $data = [
                'deskripsi' => $value
            ];
        }

        EmployeeMasterExperience::where('id', request('pk'))
            ->where('nik', Sentinel::getUser()->nik)
            ->update($data);

        return $value;
    }

    public function update_pengalaman_bumn()
    {
        $type = '';
        if (request('typ') == "pnglaman") {
            $type = "pengalaman";
        } else if (request('typ') == "asp") {
            $type = "aspirasi";
        }

        $status = 0;
        if (request('status') == "true") {
            $status = 1;
        }

        if (request('sct') == "kls") {
            EmployeeKelas::updateOrCreate(
                [
                    'nik' => Sentinel::getUser()->nik,
                    'id_kelas' => request('val'),
                    'type' => $type
                ],
                [
                    'status' => $status
                ]
            );
        } else if (request('sct') == "klstr") {
            EmployeeKluster::updateOrCreate(
                [
                    'nik' => Sentinel::getUser()->nik,
                    'id_kluster' => request('val'),
                    'type' => $type
                ],
                [
                    'status' => $status
                ]
            );
        } else if (request('sct') == "jbtn-bumn") {
            EmployeeJabatanBumn::updateOrCreate(
                [
                    'nik' => Sentinel::getUser()->nik,
                    'id_jbtn_bumn' => request('val'),
                    'type' => $type
                ],
                [
                    'status' => $status
                ]
            );
        } else if (request('sct') == "uraian") {
            EmployeesUraian::updateOrCreate(
                [
                    'nik' => Sentinel::getUser()->nik,
                    'jenis' => $type
                ],
                [
                    'uraian' => request('value')
                ]
            );
        }

        return request('val') == '' ? request('value') : request('val');
    }

    public function deletethis()
    {
        if (request('sct') == 'rwyt-pngsn') {
            EmployeePenugasan::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'frml-org') {
            EmployeeOrganization::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'frml-rwrd') {
            EmployeePenghargaan::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'emply-dklt') {
            EmployeeDiklat::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'karya') {
            EmployeeKarya::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'exp-pmbicara') {
            EmployeeExpAcara::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'exp-master') {
            EmployeeMasterExperience::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'pend-frml') {
            Pendidikan::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'wife-husb') {
            EmployeePasangan::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'emp-child') {
            EmployeeChildren::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'refs-prof') {
            EmployeeReferensi::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        } else if (request('sct') == 'rwyt-jbtn') {
            PositionHistory::where('id', request('id'))
                ->where('nik', Sentinel::getUser()->nik)
                ->delete();
        }
    }

    public function init_pendidikan()
    {
        $insert = new Pendidikan;
        $insert->nik = Sentinel::getUser()->nik;
        $insert->save();
        return $insert->id;
    }

    public function get_employee_kelas()
    {
        return EmployeeKelas::where('nik', Sentinel::getUser()->nik)
            ->select('id_kelas', 'status', 'type')
            ->get();
    }

    public function get_employee_cluster()
    {
        return EmployeeKluster::where('nik', Sentinel::getUser()->nik)
            ->select('id_kluster', 'status', 'type')
            ->get();
    }

    public function get_employee_fnc_jbtn()
    {
        return EmployeeJabatanBumn::where('nik', Sentinel::getUser()->nik)
            ->select('id_jbtn_bumn', 'status', 'type')
            ->get();
    }

    public function get_jabatan()
    {
        return Position::join('master_area', 'master_position.area', 'master_area.code_area')
            ->select(
                'master_position.code_position AS value',
                DB::raw("CONCAT(name_position,' [',name_area,']') AS text")
            )
            ->where('master_position.code_subholding', Sentinel::getUser()->employee->code_subholding)
            ->get();
    }

    public function update_riwayat_jabatan()
    {
        $section = request('sct');
        $value = request('value');

        switch ($section) {
            case 'uraian':
                $data = [
                    'uraian_singkat' => $value
                ];
                break;
            case 'achievement':
                $data = [
                    'achievement' => $value
                ];
                break;
            case 'jbtn':
                $data = [
                    'position_name' => $value
                ];
                break;
            case 'start-date':
                $data = [
                    'start_date' => $value
                ];
                break;
            case 'end-date':
                $data = [
                    'end_date' => $value
                ];
                break;
            default:
                $data = [];
                break;
        }

        PositionHistory::where('nik', Sentinel::getUser()->nik)
            ->where('id', request('pk'))
            ->update($data);

        return $value;
    }

    public function init_riwayat_one()
    {
        $insrt = new PositionHistory;
        $insrt->nik = Sentinel::getUser()->nik;
        $insrt->save();
        return $insrt->id;
    }

    public function set_status()
    {
        $sct = request('section');
        switch ($sct) {
            case 'rwyt-jbtn':
                $data = [
                    'is_riwayat_jabatan' => '1'
                ];
                break;
            case 'keangg-org':
                $data = [
                    'is_keanggotaan_org' => '1'
                ];
                break;
            case 'reward':
                $data = [
                    'is_penghargaan' => '1'
                ];
                break;
            case 'rwyt=pend':
                $data = [
                    'is_riwayat_pendidikan' => '1'
                ];
                break;
            case 'kry-tls':
                $data = [
                    'is_karya_ilmiah' => '1'
                ];
                break;
            case 'exp-pmb':
                $data = [
                    'is_peng_pemb_nara' => '1'
                ];
                break;
            case 'refs':
                $data = [
                    'is_referensi' => '1'
                ];
                break;
            case 'emp-fam':
                $data = [
                    'is_ket_keluarga' => '1'
                ];
                break;
            case 'exp-mstr':
                $data = [
                    'is_peng_keahlian' => '1'
                ];
                break;
            case 'peng-bumn':
                $data = [
                    'is_peng_bumn' => '1'
                ];
                break;
            case 'asp-bumn':
                $data = [
                    'is_asp' => '1'
                ];
                break;
            default:
                $data = [];
                break;
        }
        DB::beginTransaction();
        try {
            $ewt = EventWindowTimeModel::where('status', 'open')->value('id');
            EmployeesStatusUpdate::updateOrCreate(
                [
                    'event_window_time' => $ewt,
                    'nik' => Sentinel::getUser()->nik
                ],
                $data
            );
            DB::commit();
            return true;
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return false;
        }
    }
}
