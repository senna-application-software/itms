<?php

namespace App\Http\Controllers\HistoriBeritaAcara;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MyProfile\MyProfileController;
use App\Models\Employee;
use App\Models\MasterJobFamily;
use App\Models\Position;
use App\Models\SubEvent;
use App\Models\Ba_event;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use PDF;


class HistoriBeritaAcaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_event = SubEvent::all();

        $prosesBA   = array();
        $prosesBA[] = [
            'key' => 'TC',
            'val' => 'Talent Mapping',
        ];
        $prosesBA[] = [
            'key' => 'TD',
            'val' => 'Talent Day',
        ];
        $prosesBA[] = [
            'key' => 'TR',
            'val' => 'Talent Review',
        ];
        $prosesBA[] = [
            'key' => 'TS',
            'val' => 'Talent Selection',
        ];


       
        $page_title       = 'Histori Berita Acara';
        $page_description = 'Halaman Histori Berita Acara';
        $page_breadcrumbs = [
            ['page' => '/dashboard', 'title' => 'Home'],
            ['page' => '#', 'title' => 'Histori Berita Acara'],
        ];

        return view('history-berita-acara.list-berita-acara.index', compact('page_title', 'page_description', 'page_breadcrumbs', 'sub_event','prosesBA'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message  = "success";
        $code     = 200;
        $employee = false;

        $employee = Employee::find($id);

        if (!$employee) {
            $message = "failed";
            $code    = 400;
        }

        $data = array(
            'message' => $message,
            'code'    => $code,
            'data'    => $employee,
        );
        return response()->json($data, $code);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code     = 200;
        $message  = 'success';
        $employee = false;

        $employee = Employee::find($id);
        $employee->delete();

        if (!$employee) {
            $code     = 400;
            $message  = 'failed';
            $employee = false;
        }

        $data = array(
            'message' => $message,
            'code'    => $code,
            'data'    => $employee,
        );

        return response()->json($data, $code);
    }

    // * Function untuk get datatable server side master data employee
    public function dataTableBeritaAcara(Request $req)
    {
        $data = Ba_event::with('subevent')
        // Query where area Berita acara
        ->when(isset($req->filternamaevent) && !is_null($req->filternamaevent) && !empty($req->filternamaevent), function($query) use ($req) {
            $query->whereHas('subevent', function($subQuery) use ($req) {
                $subQuery->where('id',  '=' , $req->filternamaevent);
            });
        })

        ->when(isset($req->filterprosesba) && !is_null($req->filterprosesba) && !empty($req->filterprosesba), function($query) use ($req) {
            $query->where('prefix', 'like', '%' . $req->filterprosesba . '%');
        })
        ->when(isset($req->filtertanggalberitaacara) && !is_null($req->filtertanggalberitaacara) && !empty($req->filtertanggalberitaacara), function($query) use ($req) {
            $date = date('Y-m-d', strtotime($req->filtertanggalberitaacara));
            $query->where('tanggal_ba', '=', $date);
        })
        ->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                if ($data->id) {
                    $edit = route('BeritaAcara.downloadberitaacara', $data->id);
                } else {
                    $edit = "#";
                }
                return ' <a href="'.$edit.'" class="btn btn-light-primary font-weight-bold mr-2">
                            <i class="flaticon-download icon-sm"></i> Berita Acara
                            </a>';
            })
            ->editColumn('prefix', function ($data) {
                if ($data->prefix == 'TC') {
                    $label = "Talent Mapping";
                }elseif ($data->prefix == 'TD') {
                    $label = 'Talent Day';
                }elseif ($data->prefix == 'TR') {
                    $label = 'Talent Review';
                }elseif ($data->prefix == 'TS') {
                    $label = 'Talent Selection';
                }else{
                    $label = '-';
                }
                return $label;
            })
            ->editColumn('id_sub_event', function ($data) {
                if (isset($data->subevent->name) && $data->subevent->name != null) {
                    $label = $data->subevent->name;
                }else{
                    $label = '-';
                }
                return $label;
            })
            ->editColumn('tanggal_ba', function ($data) {
                $label = date('d-m-Y', strtotime($data->tanggal_ba));
                return $label;
            })
          
            ->rawColumns(['action'])
            ->make(true);
    }
    public function downloadberitaacara($id)
    {
        $data       = Ba_event::where('id', $id)->first();
        $sub_event  = isset($data->ba) ? json_decode($data->data_ba) : [];
        $tc         = isset($$data->data_commite) ? json_decode($data->data_commite) : [];
        $nomor_ba   = isset($data->no_ba) ? $data->no_ba : "-";
        $datax      = isset($data->prefix) ? $data->prefix : "-";
        
        if ($datax == 'TC') {
            $label = "BERITA ACARA KESEPAKATAN PEMILIHAN PESERTA TALENT DAY";
        }elseif ($datax == 'TD') {
            $label = "BERITA ACARA KESEPAKATAN HASIL PENILAIAN TALENT DAY";
        }elseif ($datax == 'TR') {
            $label = "BERITA ACARA KESEPAKATAN HASIL PENILAIAN TALENT REVIEW";
        }elseif ($datax == 'TS') {
            $label = "BERITA ACARA KESEPAKATAN PENUNJUKKAN SELECTED CANDIDATE";
        }
        // return view('history-berita-acara.berita-acara', compact('sub_event','nomor_ba','tc','datax'));

        $pdf = PDF::loadView('history-berita-acara.berita-acara', compact('sub_event','nomor_ba','tc', 'datax') );    
        return $pdf->download("$label.pdf");
    }
}
