<?php

namespace App\Http\Controllers\Calibration;

use App\Models\Callibration;
use App\Models\CalibrationHistory;
use App\Models\Employee;
use App\Models\SubEvent;
use App\Models\TalentAspiration;

use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Crypt;

use Yajra\Datatables\Datatables;
use Yajra\DataTables\Html\Builder;

class CalibrationController extends Controller
{
    public function history(Request $request) {
        $thn                = isset($request->year) ? $request->year : date('Y');
        $param_chief        = [];
        $param_position     = [];
        $grade              = $request->filter_grade;
        $lastyear           = date("Y");
        $sub_event          = $request->sub_event;

        // * Catatan : saya tambahkan kondisi talent.sub_event = $sub_event ya, soalnya kalo ga ditambahin itu nanti jadi double.
        // * Selain itu saya membutuhkan kolom talent.justification_box untuk nampilin data di frontnya, jadi kalo bingung datanya kenapa ga tampil, kemungkinan gegara nge where nya itu double
        // * ke talent.sub_event dan employee_calibration_history.sub_event.
        $data_calibration = CalibrationHistory::select(
            'employee_calibration_history.*',
            'employees.personnel_number',
            'employees.position_name',
            'employees.person_grade',
            'talent.justification_box'
        )
        ->leftJoin('employees', 'employees.prev_persno' ,'=' , 'employee_calibration_history.user_id')
        ->leftJoin('talent', 'talent.nik' ,'=' , 'employee_calibration_history.user_id')
        ->where([
            ['employee_calibration_history.created_by', '=', '3'],
            ['employee_calibration_history.sub_event', '=', $sub_event],
            ['talent.sub_event', '=', $sub_event]
        ])
        ->orderBy('employee_calibration_history.updated_at', 'desc')
        ->get();

        return Datatables::of($data_calibration)
        ->EditColumn('panel_before', function($data){
            return predikat_talent_cluster($data->panel_before);
        })
        ->EditColumn('panel', function($data){
            return predikat_talent_cluster($data->panel);
        })
        ->EditColumn('updated_at', function($data){
            return date('d-m-Y', strtotime($data->updated_at));
        })

        ->make(true);
    }
}
