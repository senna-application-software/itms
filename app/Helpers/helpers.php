<?php

use App\Http\Controllers\MyProfile\MyProfileController;
use App\Models\Employee;
use App\Models\EQS;
use App\Models\MasterHukdis;
use App\Models\User;
use \Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\ElementEQS;
use App\Models\EmployeesToPositions;
use App\Models\Performance;
use App\Models\Talent;
use App\Models\TalentAspiration;
use App\Models\SubHolding;
use App\Models\AssessmentResult;
use App\Models\Position;
use App\Models\MasterPosition;
use Illuminate\Database\Eloquent\Collection;
use App\Models\EmployeeEQS;
use App\Models\EventWindowTimeModel;
use App\Models\SubEvent;
use App\Models\MasterArea;
use App\Models\NilaiAssessment;
use App\Models\Pertanyaan;
use App\Models\SubEventTalentComitee;
use Illuminate\Support\Facades\Session;

date_default_timezone_set("Asia/Jakarta");


if (!function_exists('get_employee_pict')) {
    function get_employee_pict($nik,$is_pdf=false)
    {

        // keperluan testing
        return $img  = asset('userprofile/default.svg');
        
        $haystack = ['568940161','20001581','21104026','0682162-F'];
        if (in_array($nik, $haystack)) {
            return $img  = asset('userprofile/default.svg');
        }
        

        $mode_asli = true;

        if(!$mode_asli) {
            $filename = User::where('nik', $nik)->value('image');
            if ($filename) {
                $img  = asset('userprofile/' . $filename);
            } else {
                $img  = asset('userprofile/default.svg');
            }

            return $img;

        } else {      
            $folder_asal = DB::select("select code_subholding from employees where prev_persno='". $nik . "'");
            if ($folder_asal) {
                $folder_asal = array_shift($folder_asal);
                $arr = Storage::files("public/user-profile/".$folder_asal->code_subholding);
                foreach ($arr as $src) {

                    if (stristr($src, $nik)) {
                        $mime_type = pathinfo(Storage::path($src), PATHINFO_EXTENSION);
                        $dir_path = pathinfo($src, PATHINFO_DIRNAME).DIRECTORY_SEPARATOR."thumb";
                        $dir_name = str_replace("public","storage",pathinfo($src, PATHINFO_DIRNAME)).DIRECTORY_SEPARATOR."thumb";
                        
                        $file_path = $dir_path.DIRECTORY_SEPARATOR.pathinfo($src, PATHINFO_FILENAME).".".$mime_type;
                        $file_loc = $dir_name.DIRECTORY_SEPARATOR.pathinfo($src, PATHINFO_FILENAME).".".$mime_type;

                        if (Storage::disk('local')->exists($file_path)) {
                            return asset($file_loc);
                        }

                        $thumb = Storage::disk('local')->exists($src);
                        if ($thumb) {
                            return asset(print_thumb($src));
                        }
                    }
                }
            }
            return asset('userprofile/default.svg');
        }
    }
}

if (!function_exists('print_thumb')) {
    function print_thumb($src, $desired_width = 150) {
        if( !Storage::disk('local')->exists($src) )
        return false;

        $mime_type = pathinfo(Storage::path($src), PATHINFO_EXTENSION);

        $dir_path = pathinfo($src, PATHINFO_DIRNAME).DIRECTORY_SEPARATOR."thumb";
        $dir_name = str_replace("public","storage",pathinfo($src, PATHINFO_DIRNAME)).DIRECTORY_SEPARATOR."thumb";

        $file_path = $dir_path.DIRECTORY_SEPARATOR.pathinfo($src, PATHINFO_FILENAME).".".$mime_type;
        $file_loc = $dir_name.DIRECTORY_SEPARATOR.pathinfo($src, PATHINFO_FILENAME).".".$mime_type;

        if (Storage::disk('local')->exists($file_path)) {
            return $file_loc;
        }

        if (!Storage::exists($dir_path)) {
            Storage::makeDirectory($dir_path);
        }

        switch(strtolower($mime_type)) {
            case 'jpeg':
            case 'jpg':
                $source_image = imagecreatefromjpeg(Storage::path($src));
                break;
            case 'png':
                $source_image = imagecreatefrompng(Storage::path($src));
                break;
            case 'gif':
                $source_image = imagecreatefromgif(Storage::path($src));
                break;
        }

        $width = imagesx($source_image);
        $height = imagesy($source_image);

        $desired_height = floor($height * ($desired_width / $width));

        $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
        // header('Content-Type: image/jpeg');

        imagejpeg($virtual_image, Storage::path($file_path), 100);
        imagedestroy($virtual_image);
        return $file_loc;
    }
}

if (!function_exists('box_color')) {
    function box_color($panel)
    {
        $bg = "#fff";

        switch($panel){
            case 1: $bg = '#ff3d3d'; break;
            case 2: $bg = '#3699FF'; break;
            case 3: $bg = '#ff3d3d'; break;
            case 4: $bg = '#3699FF'; break;
            case 5: $bg = '#3699FF'; break;
            case 6: $bg = '#9c9c9c'; break;
            case 7: $bg = '#02ad2a'; break;
            case 8: $bg = '#02ad2a'; break;
            case 9: $bg = '#006317'; break;
            default: $bg = '#fff';break;
        }
        
        return $bg;
    }
}

if (!function_exists('getRouteSlugs')) {
    function getRouteSlugs(){
        $slugs  = [];
        $routes = Route::getRoutes();

        foreach ($routes as $route)
        {
            $slugs[] = $route->uri();
        }

        return array_unique($slugs);
    }
}

if (!function_exists('cekRoute')) {
    function cekRoute($routeForCheck){
        return in_array($routeForCheck,getRouteSlugs());
    }
}


if (!function_exists('talentMapping')) {
    function talentMapping($cci,$perf){
        $box = 0;
        
        if ($cci <= 59) {
            $cci_idx = 1;
        } else if ($cci >= 60 and $cci <= 80) {
            $cci_idx = 2;
        } else {
            $cci_idx = 3;
        }

        if ($perf <= 71) {
            $perf_idx = 7;
        } else if ($perf >= 72 and $perf <= 99) {
            $perf_idx = 8;
        } else {
            $perf_idx = 9;
        }

        switch ($cci_idx * $perf_idx) {
            case 7:
                $box = 1;
            break;

            case 8:
                $box = 3;
            break;

            case 9:
                $box = 6;
            break;

            case 14:
                $box = 2;
            break;

            case 16:
                $box = 5;
            break;

            case 18:
                $box = 8;
            break;

            case 21:
                $box = 4;
            break;

            case 24:
                $box = 7;
            break;

            case 27:
                $box = 9;
            break;
        }

        return $box;
    }
}

if (!function_exists('boxProperties')) {
    function boxProperties($is_generate = false, $no_box = 0) {
        $q = "SELECT 
        box.box,
        box.color as 'color',
        box.urut,
        box_matrix.id_sumbu_batas_x,
        box_matrix.id_sumbu_batas_y,
        box_matrix.nilai_acuan,
        box_cat.cat_name,
        box_cat.color as 'cat_color',
        (SELECT batas_bawah FROM sumbu_batas WHERE id=box_matrix.id_sumbu_batas_x) as min_performance, 
        (SELECT batas_atas FROM sumbu_batas WHERE id=box_matrix.id_sumbu_batas_x) as max_performance,
        (SELECT batas_bawah FROM sumbu_batas WHERE id=box_matrix.id_sumbu_batas_y) as min_cci,
        (SELECT batas_atas FROM sumbu_batas WHERE id=box_matrix.id_sumbu_batas_y) as max_cci 
        
        FROM box
        left JOIN box_matrix on box_matrix.id = box.id_box_matrix
        left JOIN box_cat on box_cat.id = box.id_box_cat ";
        
        if ($is_generate) {
            $q .= " order by box.urut asc";
        }

        if (!$is_generate) {
            $q .= " where box.box='".$no_box."'";
        }

        return DB::select($q);

    }
    
}
if (!function_exists('sendmail')) {
    function sendmail($data){   
        \Mail::to($data["emails"])->send(new \App\Mail\SendMail($data));
    }
} 

if (!function_exists('rule_aspiration')) {
    function rule_aspiration($nik, $return="data", $tambahan=false) {
        // Init
        $login_as = cekBOD($nik);
        $is_direktur = "('LV-00030', 'LV-00029', 'LV-00010', 'LV-00026')";
        $employee = Employee::with(['positions' => function($query) use($is_direktur) {
                $query->selectRaw("
                    *,
                    (SELECT is_member FROM master_area WHERE master_position.area = master_area.code_area) as is_member,
                    IF(code_position_level IN $is_direktur, 1, 0) as is_direktur
                "); 
            }])
            ->where("prev_persno", $nik)
            ->withCount(['bawahan' => function($query) {
                return $query;
            }])->first();

        $submenu = [];
        $wizard = [];
        
        // Rule Individual
        if (isset($employee->positions) && isset($employee->tanggal_mulai_bekerja) && $employee->tanggal_mulai_bekerja != null && $employee->status_karyawan == "Tetap") {
            // Rule Minimal 2 tahun
            $rule_tahun_masuk = false;
            $tanggal_mulai = new DateTime($employee->tanggal_mulai_bekerja, new DateTimeZone('Asia/Jakarta'));
            $now = new DateTime("NOW", new DateTimeZone('Asia/Jakarta'));
            $diff = $now->diff($tanggal_mulai);
            if ($diff->format("%R%y") <= -2) {
                $rule_tahun_masuk = true;
            }
            // Rule Hukdis
            $hukdis = MasterHukdis::where('nik', $nik)->latest()->first();
            $rule_hukdis = true;
            if (!empty($hukdis)) {
                $start_hukdis = date("Y-m-d", strtotime($hukdis->start_date));
                $end_hukdis = date("Y-m-d", strtotime($hukdis->end_date));
                $now = date("Y-m-d");
                if ($now >= $start_hukdis && $now <= $end_hukdis) {
                    $rule_hukdis = false;
                }
            }
            if (($rule_tahun_masuk && $rule_hukdis && $employee->status_karyawan != "Honorer") || $employee->positions->code_subholding == "inj") {
                array_push($submenu, [
                    'title' => 'Individual Aspiration',
                    'page' => 'aspirations/individual'
                ]);
                $wizard['items'][] = "individual";
            }
        }
        // Rule Supervisor
        if (isset($employee->positions) && isset($employee->bawahan_count) && $employee->bawahan_count > 0) {
            array_push($submenu, [
                'title' => 'Supervisor Aspiration',
                'page' => 'aspirations/supervisor'
            ]);
            $wizard['items'][] = "supervisor";
        }   

        // Rule Job Holder
        $position_history = DB::table('master_position_history')
            ->selectRaw('DATEDIFF(DATE(start_date), NOW()) as lama_menjabat')
            ->where('nik', $nik)
            ->orderBy('urut', 'DESC')
            ->whereRaw('
                start_date IS NOT NULL AND
                start_date != "0000-00-00"
            ')
            ->first();

        $kondisi_masa_jabatan = ($position_history && (isset($employee->positions->org_level) && $employee->positions->org_level == "BOD-1") && $position_history->lama_menjabat <= -180);
        $kondisi_direktur_member = isset($employee->positions) ? ($employee->positions->is_member ?? false && $employee->positions->is_direktur ?? false) : false;

        // * Menambahkan kondisi tipe posisi, jika fungsional maka menu job holder tidak muncul
        $conditionTypePosition = ($employee->positions->masterPositionType && strtolower($employee->positions->masterPositionType->name_position_type) == 'struktural' ? true : false);

        if (isset($employee->positions) && ($kondisi_masa_jabatan || $kondisi_direktur_member) && $conditionTypePosition) {
            array_push($submenu, [
                'title' => 'Job Holder Aspiration',
                'page' => 'aspirations/job_holder'
            ]);
            $wizard['items'][] = "job_holder";
        }

        // Rule Unit
        if (isset($employee->positions) && $login_as == "Kepala Unit") {
            array_push($submenu, [
                'title' => 'Unit Aspiration',
                'page' => 'aspirations/unit'
            ]);
            $wizard['items'][] = "unit";
        }

        if ($return == "menu") {
            return menu_aspiration($submenu);
        }elseif($return == "menu_test") {
            return $submenu;
        }elseif ($return == "wizard") {
            if ($tambahan) {
                if (!in_array($tambahan, $wizard['items'])) {
                    return false;
                }else {
                    $current_key = current(array_keys($wizard['items'], $tambahan));
                    if (isset($wizard['items'][$current_key - 1]))
                        $wizard['prev'] = $wizard['items'][$current_key - 1];
                    if (isset($wizard['items'][$current_key + 1]))
                        $wizard['next'] = $wizard['items'][$current_key + 1];
                }
            }
            return $wizard;
        }
        return $employee;
    }
}

if (!function_exists('cekBOD')) {
    function cekBOD($nik){   
        $q = "SELECT LOWER(bod_type) AS bod_name FROM employees WHERE line_manager ='". $nik ."' GROUP BY bod_type";
        $query = collect(DB::select($q))->pluck("bod_name")->toArray();
        
        if ($query) {
            $diff = array_diff(array("bod-1","bod-2"), $query);
            
            if (count($diff) == 0) {
                $login_as = "Kepala Unit";
            } else if (count($diff) == 1 && in_array('bod-1',$diff)) {
                $login_as = "Kepala Group";
            } else if (count($diff) == 1 && in_array('bod-2',$diff)) {
                $login_as = "Kepala Divisi";
            } else {
                $login_as = "";
            }
        } else {
            $login_as = "Employee";
        }
        return $login_as;
    }
}

if (!function_exists('menu_aspiration')) {
    function menu_aspiration($menu_aspiration) {
        return [
            'items' => [
                // Dashboard
                [
                    'title' => 'Dashboard',
                    'root' => false,
                    'icon' => 'media/svg/icons/Design/Layers.svg',
                    'page' => 'dashboard',
                    'new-tab' => false,
                ],
        
                [
                    'section' => 'Aspirations',
                ],
        
                [
                    'title' => 'Aspirations',
                    'icon' => 'media/svg/icons/Communication/Chat4.svg',
                    'bullet' => 'line',
                    'root' => false,
                    'page' => '#',
                    'new-tab' => false,
                    'submenu' => $menu_aspiration,
                ],
        
                // Custom
                [
                    'section' => 'My Profile',
                ],
                [
                    'title' => 'Personal Information',
                    'icon' => 'media/svg/icons/General/User.svg',
                    'bullet' => 'line',
                    'root' => false,
                    'page' => 'my-profile',
                ],

                [
                    'title' => 'Learning & Development',
                    'root' => false,
                    'icon' => 'media/svg/icons/Home/Book-open.svg',
                    'page' => '#',
                    'new-tab' => false,
                ],
            ]
        
        ];
    }
}
if (!function_exists('generate_diagram_organization')) {
    function generate_diagram_organization($where_codition=[]) {
        $data = Position::selectRaw('
                master_position.id, 
                employees.personnel_number as job_holder,
                employees.prev_persno,
                master_position.name_position as position,
                master_position.org_level as org_level,
                master_position_type.name_position_type,
                (
                    SELECT
                        subposition.id
                    FROM
                        master_position subposition 
                    WHERE
                        (CASE
                            WHEN master_position.org_level = "BOD" && master_position.name_position != "Direktur Utama" THEN subposition.name_position = "Direktur Utama"
                            WHEN master_position.org_level = "BOD-1" THEN subposition.code_directorate = master_position.code_directorate AND subposition.org_level = "BOD"
                            WHEN master_position.org_level = "BOD-2" THEN subposition.code_directorate = master_position.code_directorate AND subposition.org_level = "BOD-1"
                        END)
                    ORDER BY subposition.id ASC LIMIT 1
                ) AS pid,
                (SELECT count(id) FROM talent WHERE talent.nik = employees.prev_persno collate utf8mb4_unicode_ci AND talent.status="final" group by talent.nik) as number_successor'
            )
            ->leftjoin('employees', DB::raw('employees.position collate utf8mb4_unicode_ci'), '=', 'master_position.code_position')
            ->join('master_position_type', 'master_position_type.code_position_type', '=', 'master_position.code_position_type')
            ->where('master_position.code_position_type', '!=', 'TP-00002')
            ->when(isset($where_codition['subholding']), function($query) use($where_codition) {
                $query->where('master_position.code_subholding', $where_codition['subholding']);
            })
            ->when(isset($where_codition['code_directorate']), function($query) use($where_codition) {
                $query->where('master_position.code_directorate', $where_codition['code_directorate']);
            })
            ->whereIn('org_level', ['BOD', 'BOD-1'])
            ->orderBy(DB::raw('FIELD(org_level, "BOD", "BOD-1")'))
            ->get()->map(function($value) {
                $each_data = [
                    'id' => $value->id,
                    'Job Holder' => $value->job_holder ?? "-",
                    'Position' => $value->position,
                    'org_level' => $value->org_level,
                    'Number of Successor' => empty($value->number_successor) ? 0 : $value->number_successor,
                    'img' => get_employee_pict($value->prev_persno)
                ];
                if ($value->pid != null) {
                    $each_data['pid'] = $value->pid;
                }
                return $each_data;
            })->filter(function($value) {
                return (trim(strtolower($value['Position'])) == 'direktur utama' && $value['org_level'] == 'BOD') || isset($value['pid']);
            })->toArray();
        
        return array_values($data);
    }
}
if (!function_exists('ulangtahun')) {
    function ulangtahun($ulangtahun)
    {
        $birthDate = new DateTime($ulangtahun);
        $today = new DateTime("today");
        if ($birthDate > $today) { 
            // exit("0 Years 0 months ");
            return $ulangtahun;
        }
        $y = $today->diff($birthDate)->y;
        $m = $today->diff($birthDate)->m;
        $d = $today->diff($birthDate)->d;
        return $y." Years ".$m." months ";
    }
}
if (!function_exists('masakerja')) {
    function masakerja($masakerja)
    {

        $birthDate = new DateTime($masakerja);
        $today = new DateTime("today");
        if ($birthDate > $today) { 
            // exit("0 Years 0 months ");
            return $masakerja;
        }
        $y = $today->diff($birthDate)->y;
        $m = $today->diff($birthDate)->m;
        $d = $today->diff($birthDate)->d;
        return $y." Years ".$m." months ";
    }
}

if (!function_exists('data_career_card')) {
    function data_career_card($nik, $sub_event) {
        $year_end = date('Y') - 1;
        $year_start = $year_end - 3;
        $data["employee"]       = DB::select("SELECT * FROM employees
            LEFT JOIN master_position on master_position.code_position = employees.position collate utf8mb4_unicode_ci
            LEFT JOIN master_division on master_division.code_division = employees.division_code collate utf8mb4_unicode_ci
            LEFT JOIN master_group on master_group.code_group = employees.group_code collate utf8mb4_unicode_ci
            LEFT JOIN master_directorate on master_directorate.code_directorate = employees.directorate_code collate utf8mb4_unicode_ci
            WHERE prev_persno = '".$nik."'
        ");
        $data["talent"]         = DB::select("SELECT * FROM talent WHERE nik = '".$nik."' AND sub_event = '".$sub_event."'");
        $data["hukdis"]         = DB::select("SELECT * FROM master_hukdis WHERE nik = '".$nik."'");
        $data["performance"]    = DB::select("SELECT * FROM master_performance WHERE nik = '".$nik."' and year between '".$year_start."' and '". $year_end."'");
        $data["training"]       = DB::select("SELECT * FROM master_training WHERE nik = '".$nik."' ORDER BY start_date DESC");
        $data["certificate"]    = DB::select("SELECT * FROM master_certificate WHERE nik = '".$nik."' ORDER BY start_date DESC");
        $data["hukdis"]    = DB::select("SELECT * FROM master_hukdis WHERE nik = '".$nik."' ORDER BY start_date DESC");
        $data["is_plain"]    = $sub_event == -1 ? true : null;

        $tmp = new MyProfileController;
        $data['sub_data'] = $tmp->clone_data($nik);

        return $data;
    }
}

if (!function_exists('hari_ini')) {
    function hari_ini() {

        $hari = date("D");
    
        switch($hari){
            case 'Sun':
                $hari_ini = "Minggu";
            break;
    
            case 'Mon':			
                $hari_ini = "Senin";
            break;
    
            case 'Tue':
                $hari_ini = "Selasa";
            break;
    
            case 'Wed':
                $hari_ini = "Rabu";
            break;
    
            case 'Thu':
                $hari_ini = "Kamis";
            break;
    
            case 'Fri':
                $hari_ini = "Jumat";
            break;
    
            case 'Sat':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";		
            break;
        }
    
        $data = [
            "nama_hari" => $hari_ini,
            "tgl" => date("d"),
            "bln" => date("m"),
            "nama_bulan" => nama_bulan(date("m")),
            "thn" => date("Y"),
            "tanggal" => date("d-m-Y")
        ];
        return $data;
    
    }
    
}


if (!function_exists('nama_bulan')) {
    function nama_bulan($bln) {

        $hari = date("D");
    
        switch($bln){
            case '1':
                $nama_bulan = "Januari";
            break;
    
            case '2':			
                $nama_bulan = "Februari";
            break;
    
            case '3':
                $nama_bulan = "Maret";
            break;
    
            case '4':
                $nama_bulan = "April";
            break;
    
            case '5':
                $nama_bulan = "Mei";
            break;
    
            case '6':
                $nama_bulan = "Juni";
            break;
    
            case '7':
                $nama_bulan = "Juli";
            break;

            case '8':
                $nama_bulan = "Agustus";
            break;
    
            case '9':
                $nama_bulan = "September";
            break;
    
            case '10':
                $nama_bulan = "Oktober";
            break;
    
            case '11':
                $nama_bulan = "Nopember";
            break;
    
            case '12':
                $nama_bulan = "Desember";
            break;
            
            default:
                $nama_bulan = "Tidak di ketahui";		
            break;
        }
        return $nama_bulan;
    }
    
}

if (!function_exists('kalkulasi_eqs')) {
    function kalkulasi_eqs($parameter=false, $kalkulasi=true, $return = ['all']) {
        if (!$parameter)
            return false;
        // Init
        if (isset($parameter['sub_event'])) {
            $listed_employee = Talent::where('sub_event', $parameter['sub_event'])
                ->get()->map(function($value) {
                    return $value->nik;
                });
        }
        if (isset($parameter['employee'])) {
            $listed_employee = Employee::whereIn('prev_persno', $parameter['employee'])
                ->get()->map(function($value) {
                    return $value->prev_persno;
                });
        }

        if (empty($listed_employee)) {
            return false;
        }

        $allitems = new Collection();
        if (in_array('all', $return) || in_array('kinerja', $return)){
            // Kinerja
            // $year_end = date("Y");
            $year_end = 2020;
            $year_start = $year_end - 2;
            $bobot_kinerja = EQS::where('element', 'Kinerja')->first();
            $element_kinerja = ElementEQS::where('id_eqs', $bobot_kinerja->id)
                ->get()
                ->map(function($value) use($bobot_kinerja, $kalkulasi, $year_start, $year_end) {
                    $nilai_element = $value->nilai;
                    $bobot_kinerja = $bobot_kinerja->bobot;
                    $count_performance = "(SELECT COUNT(id) FROM master_performance deep_sub_performance WHERE deep_sub_performance.nik = sub_performance.nik AND deep_sub_performance.perfomance_nilai IS NOT NULL)";
                    if ($kalkulasi) 
                        return "(SELECT (SUM(sub_performance.perfomance_nilai) / $count_performance) * $bobot_kinerja DIV 100 FROM master_performance sub_performance WHERE sub_performance.year BETWEEN $year_start AND $year_end AND sub_performance.nik=master_performance.nik)";
                    return "(SELECT (SUM(sub_performance.perfomance_nilai) / $count_performance) FROM master_performance sub_performance WHERE sub_performance.year BETWEEN $year_start AND $year_end AND sub_performance.nik=master_performance.nik)";
                })->first();
            $performance = Performance::selectRaw("
                    nik, 
                    $element_kinerja as nilai_akhir_kinerja
                ")
                ->whereIn('nik', $listed_employee)
                ->groupBy('nik')
                ->get();
            $allitems = $allitems->mergeRecursive($performance);
        }

        if (in_array('all', $return) || in_array('track_record', $return)) {
            // Track Record 
            $bobot_track_record = EQS::where('element', 'Track Record')->first();
            $external_condition_element_track_record = ElementEQS::when($kalkulasi, function($query) use($bobot_track_record) {
                    $query->selectRaw("(nilai*$bobot_track_record->bobot/100) as nilai");
                })
                ->when(!$kalkulasi, function($query) use($bobot_track_record) {
                    $query->selectRaw("nilai");
                })
                ->where('id_eqs', $bobot_track_record->id)->where('nilai_likert', 5)->first()->nilai;
            $element_track_record = ElementEQS::where('id_eqs', $bobot_track_record->id)
                ->get()
                ->map(function($value) use($bobot_track_record, $kalkulasi) {
                    $nilai_element = $value->nilai;
                    $bobot_track_record = $bobot_track_record->bobot;
                    if ($value->nilai_likert <= 2){
                        return "WHEN DATEDIFF(DATE(end_date), NOW()) > 0 AND hukdis_cat IN ('sedang', 'berat') THEN 0";
                    }else if ($value->nilai_likert == 3) {
                        if ($kalkulasi) 
                            return "WHEN DATEDIFF(DATE(end_date), NOW()) > 0 AND hukdis_cat IN ('ringan') THEN ($nilai_element * $bobot_track_record DIV 100)";
                            
                        return "WHEN DATEDIFF(DATE(end_date), NOW()) > 0 AND hukdis_cat IN ('ringan') THEN $nilai_element";
                    }else if ($value->nilai_likert == 4){
                        if ($kalkulasi) 
                            return "WHEN DATEDIFF(DATE(end_date), DATE(DATE_SUB(NOW(),INTERVAL 10 YEAR))) > 0 THEN ($nilai_element * $bobot_track_record DIV 100)";

                        return "WHEN DATEDIFF(DATE(end_date), DATE(DATE_SUB(NOW(),INTERVAL 10 YEAR))) > 0 THEN $nilai_element";
                    }else    
                        return "";
                })->unique()->implode(" ");
            $hukdis = MasterHukdis::selectRaw("
                    nik,
                    (CASE $element_track_record END) as nilai_akhir_track_record
                ")
                ->whereIn('nik', $listed_employee)
                ->groupBy('nik')
                ->get();
            $allitems = $allitems->mergeRecursive($hukdis);
        }

        if ((in_array('all', $return) || in_array('aspirasi', $return)) && isset($parameter['position'])) {
            // Aspiration 
            $bobot_aspiration = EQS::where('element', 'Aspirasi')->first();
            $element_aspiration = ElementEQS::where('id_eqs', $bobot_aspiration->id)
                ->get()
                ->map(function($value) use($bobot_aspiration) {
                    $bobot_aspiration = $bobot_aspiration->bobot;
                    if ($value->keterangan == "Individual Aspiration") {
                        $rediclare = 'individual';
                    }else if ($value->keterangan == "Job Holder Aspiration") {
                        $rediclare = 'job_holder';
                    }else if ($value->keterangan == "Unit Aspiration") {
                        $rediclare = 'unit';
                    }else if ($value->keterangan == "Supervisor Aspiration") {
                        $rediclare = 'supervisor';
                    }
                    return [
                        'type' => $rediclare,
                        'value' => $value->nilai
                    ];
                })->toArray();

            $id_event = EventWindowTimeModel::where('status', 'open')->limit(1)->first()->id ?? -1;
            $aspiration = TalentAspiration::selectRaw('nik, aspiration_type')
                ->whereIn('nik', $listed_employee)
                // TODO: @fachri ini bug  ke dashboard executive - done
                ->where('code_position', $parameter['position'])
                ->where('id_event', $id_event)
                ->groupBy('nik', 'aspiration_type')
                ->get()->map(function($value) use($element_aspiration) {
                    $key = array_search($value->aspiration_type, array_column($element_aspiration, "type"));
                    $element_aspiration = (!is_numeric($key)) ? 0 : $element_aspiration[$key]['value'];
                    $value->nilai_element_aspiration = $element_aspiration;
                    return $value;
                })->groupBy('nik')->map(function($fix_value) use($bobot_aspiration, $kalkulasi) {
                    $bobot_aspiration = $bobot_aspiration->bobot;
                    $fix_value = $fix_value->toArray();
                    return [
                        'nik' => current($fix_value)['nik'],
                        'nilai_akhir_aspiration' => ($kalkulasi) ? array_sum(array_column($fix_value, 'nilai_element_aspiration')) * $bobot_aspiration / 100 : array_sum(array_column($fix_value, 'nilai_element_aspiration'))
                    ];
                });
            $allitems = $allitems->mergeRecursive($aspiration);
        }

        if (in_array('all', $return) || in_array('training_sertifikasi', $return)) {
            $bobot_training = EQS::where('element', 'Training dan Sertifikasi')->first();
            $element_training = ElementEQS::where('id_eqs', $bobot_training->id)
                ->get()
                ->map(function($value) {
                    if ($value->nilai_likert > 0){
                        return (object) [
                            'jenis' => ($value->nilai_likert == 1) ? "training" : "sertifikasi",
                            'nilai' => $value->nilai
                        ];
                    }
                })->filter();

            $training = DB::table('master_training')
                ->whereIn('nik', $listed_employee)
                ->orderBy('start_date', "DESC")
                ->get()->groupBy('nik')->map(function($value) {
                    return $value->count();
                })->toArray();
            
            $sertifikasi = DB::table('master_certificate')
                ->whereIn('nik', $listed_employee)
                ->where(DB::raw("TRIM(LOWER(certificate_type))"), '!=', trim(strtolower('T : MANDATORY')))
                ->orderBy('start_date', "DESC")
                ->get()->groupBy('nik')->map(function($value) {
                    return $value->count();
                })->toArray();
            
            $training_sertifikasi_employees = $listed_employee->toArray();
            array_walk($training_sertifikasi_employees, function(&$value) use($element_training, $bobot_training, $training, $sertifikasi, $kalkulasi) {
                $training_value = isset($training[$value]) ? 
                    ($element_training->where('jenis', 'training')->first() ? $element_training->where('jenis', 'training')->first()->nilai : 0) 
                    : 0;
                // $training_value = isset($training[$value]) ? $element_training->where('jenis', 'training')->first()->nilai : 0;
                // $sertifikasi_value = isset($sertifikasi[$value]) ? $element_training->where('jenis', 'sertifikasi')->first()->nilai: 0;
                $sertifikasi_value = isset($sertifikasi[$value]) ? 
                    ($element_training->where('jenis', 'sertifikasi')->first() ? $element_training->where('jenis', 'sertifikasi')->first()->nilai: 0)
                    : 0;
                $nilai_akhir = ($training_value + $sertifikasi_value) * ($bobot_training->bobot / 2) / 100;
                if (!$kalkulasi)
                    $nilai_akhir = ($training_value + $sertifikasi_value);
                    
                $value = [
                    'nik' => $value,
                    'nilai_akhir_training_sertifikasi' => $nilai_akhir
                ];
            });
            $allitems = $allitems->mergeRecursive($training_sertifikasi_employees);
        }

        if ((in_array('all', $return) || in_array('job_fit', $return))) {
            // Job Fit
            $bobot_job_fit = EQS::where('element', 'Kesesuaian Kompetensi')->first();
            $gap_competency_employees = gap_competency_employees($listed_employee, $parameter['position'] ?? false);
            $allitems = $allitems->mergeRecursive($gap_competency_employees);
        }

        if ((in_array('all', $return) || in_array('experience', $return)) && isset($parameter['position'])) {
            // Job Fit
            $code_job_family = DB::table('master_position')->where('code_position', $parameter['position'])->first()->code_job_family;
            $bobot_experience = EQS::where('element', 'Experience')->first();
            $experience = employee_job_family($listed_employee, $code_job_family, true);
            $allitems = $allitems->mergeRecursive($experience);
        }

        // Get Data / Store Data
        $raw_data = $allitems->toArray();
        $data = [];
        $keys = array_unique(array_column($raw_data, 'nik'));
        $data = array_map(function($nik) use($raw_data, $external_condition_element_track_record, $kalkulasi) {
            $nilai_akhir_kinerja = array_column(array_filter($raw_data, function($element_value) use($nik) {
                return $element_value['nik'] == $nik;
            }), 'nilai_akhir_kinerja');
            $nilai_akhir_track_record = array_column(array_filter($raw_data, function($element_value) use($nik) {
                return $element_value['nik'] == $nik;
            }), 'nilai_akhir_track_record');
            $nilai_akhir_aspiration = array_column(array_filter($raw_data, function($element_value) use($nik) {
                return $element_value['nik'] == $nik;
            }), 'nilai_akhir_aspiration');
            $nilai_training_sertifikasi = array_column(array_filter($raw_data, function($element_value) use($nik) {
                return $element_value['nik'] == $nik;
            }), 'nilai_akhir_training_sertifikasi');
            $nilai_job_fit = array_column(array_filter($raw_data, function($element_value) use($nik) {
                return $element_value['nik'] == $nik;
            }), 'job_fit');
            $nilai_experience = array_column(array_filter($raw_data, function($element_value) use($nik) {
                return $element_value['nik'] == $nik;
            }), 'experience');

            if ($kalkulasi) {
                return array_merge_recursive(
                    ['nik' => $nik], 
                    ['nilai_akhir_kinerja' => (!current($nilai_akhir_kinerja)) ? 0 : current($nilai_akhir_kinerja) ], 
                    ['nilai_akhir_track_record' => (!current($nilai_akhir_track_record)) ? (int) $external_condition_element_track_record : current($nilai_akhir_track_record) ], 
                    ['nilai_akhir_job_fit' => (!current($nilai_job_fit)) ? 0 : current($nilai_job_fit)],
                    ['nilai_akhir_experience' => (!current($nilai_experience)) ? 0 : current($nilai_experience)],
                    ['nilai_akhir_talent_cluster' => 0],
                    ['nilai_akhir_aspiration' => (!current($nilai_akhir_aspiration)) ? 0 : current($nilai_akhir_aspiration) ],
                    ['nilai_training_sertifikasi' => (!current($nilai_training_sertifikasi)) ? 0 : current($nilai_training_sertifikasi) ],
                    ['nilai_akhir_penghargaan' => 0],
                    ['nilai_sosial' => 0],
                );
            }
            return array_merge_recursive(
                ['nik' => $nik], 
                ['kinerja' => (!current($nilai_akhir_kinerja)) ? 0 : current($nilai_akhir_kinerja) ], 
                ['track_record' => (!current($nilai_akhir_track_record)) ? (int) $external_condition_element_track_record : current($nilai_akhir_track_record) ], 
                ['job_fit' => (!current($nilai_job_fit)) ? 0 : current($nilai_job_fit)],
                ['experience' => (!current($nilai_experience)) ? 0 : current($nilai_experience)],
                ['talent_cluster' => 0],
                ['training_sertifikasi' => (!current($nilai_training_sertifikasi)) ? 0 : current($nilai_training_sertifikasi) ],
                ['aspiration' => (!current($nilai_akhir_aspiration)) ? 0 : current($nilai_akhir_aspiration) ],
                ['penghargaan' => 0],
                ['sosial' => 0],
            );
        }, $keys);
        return $data;
    }

}

if (!function_exists('gap_competency_employees')) {
    function gap_competency_employees($nik=[], $target_position) {
        $query_required_competency = DB::table('master_competency_required')
        ->select('*', DB::raw('LOWER(name_competency) as lower_name_competency'))->get();
        $required_competencies = $query_required_competency->pluck('lower_name_competency');
        $employees = Employee::with(['assessment_competencies' => function($query) use($required_competencies) {
                $query->whereIn(DB::raw('LOWER(soft_competency_align)'), $required_competencies);
            }])
            ->with('positions')
            ->whereIn('prev_persno', $nik)
            ->get();
        
        $required = false;
        if ($target_position) {
            $org_level = DB::table('master_position')->where('code_position', $target_position)->first()->org_level;
            if ($org_level == "BOD") {
                $required = "required_for_bod";
            }else {
                $required = "required_for_bod_1";
            }
        }

        $raw_data = $query_required_competency->map(function($value) use($required, $employees) {
            return $employees->map(function($employee) use($value, $required) {
                if (!$required) {
                    if (isset($employee->positions->org_level) && $employee->positions->org_level == "BOD") {
                        $required = "required_for_bod";
                    }else {
                        $required = "required_for_bod_1";
                    }
                }
                $find = $employee->assessment_competencies->filter(function($item) use($value) {
                    return trim(strtolower($item->soft_competency_align)) == trim(strtolower($value->name_competency));
                })->first();
                $current = (!$find) ? 0 : (is_null($find->result) ? 0 : $find->result);
                $rename_competency = implode(" ", array_map(function($char) {
                    return ucfirst($char);
                }, explode(" ", $value->name_competency)));
                return (object) [
                    'nik' => $employee->prev_persno,
                    'name_competency' => $rename_competency,
                    'current' => $current,
                    "required" => (int) $value->{$required},
                ];
            });
        })->toArray();

        $data = collect(call_user_func_array('array_merge', $raw_data))
            ->groupBy('nik')->mapWithKeys(function($value, $key) {
                return [
                    $key => [
                        'nik' => $key,
                        // 'job_fit' => ($value->sum('current') < 1) ? 0 : ($value->sum('current') / $value->sum('required') * 100)
                        'job_fit' => ($value->sum('current') < 1) ? 0 : round($value->sum('current') / $value->sum('required') * 100, 2)
                    ]
                ];
            })->sortBy([
                function($a, $b) {
                    return $a['job_fit'] < $b['job_fit'];
                }
            ]);
        return $data;
    }
}

if (!function_exists('get_sub_holding')) {
    function get_sub_holding($code) { 
        $sub_holding = SubHolding::where('code_subholding', $code)->first()->name_subholding;
        return $sub_holding;
    }
    
}

if (!function_exists('nilai_eqs')) {
    function nilai_eqs($employee_nik, $key_eqs=false, $sub_event=false) {
        if ($sub_event) {
            $bobot = EQS::get();
            $eqs = DB::table('employee_eqs')
                ->selectRaw('kinerja, track_record, job_fit, experience, training_sertifikasi, aspiration')
                ->where('nik', $employee_nik)
                ->when(($sub_event != false), function($query) use($sub_event) {
                    $query->where('sub_event', $sub_event);
                })->orderBy('sub_event', "DESC")
                ->limit(1)
                ->get()->map(function($value) use($bobot) {
                    $value->mentah_kinerja = $value->kinerja;
                    $value->mentah_track_record = $value->track_record;
                    $value->mentah_job_fit = $value->job_fit;
                    $value->kinerja = round($value->kinerja * $bobot->where('element', 'Kinerja')->first()->bobot / 100, 2); 
                    $value->track_record = round($value->track_record * $bobot->where('element', 'Track Record')->first()->bobot / 100, 2); 
                    $value->job_fit = round($value->job_fit * $bobot->where('element', 'Kesesuaian Kompetensi')->first()->bobot / 100, 2); 
                    $value->experience = round($value->experience * $bobot->where('element', 'Experience')->first()->bobot / 100, 2); 
                    $value->training_sertifikasi = round(($value->training_sertifikasi / 200 * 100) * $bobot->where('element', 'Training dan Sertifikasi')->first()->bobot / 100, 2); 
                    $value->aspiration = round($value->aspiration * $bobot->where('element', 'Aspirasi')->first()->bobot / 100, 2); 
                    return $value;
                })->first();
            $eqs_final = array_sum([
                isset($eqs->kinerja) ? round($eqs->kinerja, 2) : 0,
                isset($eqs->track_record) ? round($eqs->track_record, 2) : 0,
                isset($eqs->job_fit) ? round($eqs->job_fit, 2) : 0,
                isset($eqs->experience) ? round($eqs->experience, 2) : 0,
                isset($eqs->training_sertifikasi) ? round($eqs->training_sertifikasi, 2) : 0,
                isset($eqs->aspiration) ? round($eqs->aspiration, 2) : 0,
            ]);
            if ($key_eqs == "all") {
                return [
                    'eqs' => $eqs,
                    'eqs_final' => $eqs_final . " of 107"
                ];
            }
            return $eqs_final . " of 107";
        }
        $parameter = [
            'employee' => [$employee_nik]
        ];

        $eqs = kalkulasi_eqs($parameter);

        if(empty($eqs)) return 0;

        $acc = array_shift($eqs);  
        $hsl = 0;     
        
        if($key_eqs == false) {
            foreach ($acc as $key => $val) {            
                if($key == "nik") {
                    continue;
                }
                $hsl += $val;
            }
    
            return $hsl . " of 107";
        }

        if(empty($acc)) return 0;

        foreach ($acc as $key => $val) {            
            if($key == $key_eqs) {
                return $val;
            }
        }
        
        return 0;
    }
}

if (!function_exists('date_db_formatter')) {
    function date_db_formatter($date) {

        if (strpos($date,"/") !== false) {
            $try = explode("/",$date);
        } else {
            $try = explode("-",$date);
        }   

        if(count($try) !== 3) {
            return $date;
        }

        $start_tahun        = $try[2];
        $start_bulan_or_tgl = $try[1];
        $start_tgl_or_bulan = $try[0];

        if (date_create($start_tahun."-".$start_bulan_or_tgl."-".$start_tgl_or_bulan) !== false) {
            return date_format(date_create($start_tahun."-".$start_bulan_or_tgl."-".$start_tgl_or_bulan),"Y-m-d");
        } else if(date_create($start_tahun."-".$start_tgl_or_bulan."-".$start_bulan_or_tgl) !== false)  {
            return date_format(date_create($start_tahun."-".$start_tgl_or_bulan."-".$start_bulan_or_tgl),"Y-m-d");
        } else {
            return $date;
        }
    }    
}

if (!function_exists('predikat_talent_cluster')) {
    function predikat_talent_cluster($box) {
        switch ($box) {
            default: $predikat = $box; break;
            case 1:  $predikat = "Unfit"; break;
            case 2: 
            case 4:  $predikat = "Sleeping Tiger"; break;
            case 3: 
            case 6:  $predikat = "Solid Contributor"; break;
            case 5: 
            case 7: 
            case 8:  $predikat = "Promotable"; break;
            case 9:  $predikat = "High Potential"; break;
        }

        return $predikat;
    }    
}


if (!function_exists('romawi')) {
    function romawi($num){ 
        $n = intval($num); 
        $res = ''; 

        //array of roman numbers
        $romanNumber_Array = array( 
            'M'  => 1000, 
            'CM' => 900, 
            'D'  => 500, 
            'CD' => 400, 
            'C'  => 100, 
            'XC' => 90, 
            'L'  => 50, 
            'XL' => 40, 
            'X'  => 10, 
            'IX' => 9, 
            'V'  => 5, 
            'IV' => 4, 
            'I'  => 1); 

        foreach ($romanNumber_Array as $roman => $number){ 
            //divide to get  matches
            $matches = intval($n / $number); 

            //assign the roman char * $matches
            $res .= str_repeat($roman, $matches); 

            //substract from the number
            $n = $n % $number; 
        } 

        // return the result
        return $res; 
    } 
} 

if (!function_exists('generate_no_ba')) {
    function generate_no_ba($prefix, $id_sub_event) {

        $prefix = strtoupper($prefix);
        // format "001/TD/II/2022";
        $no_ba = "Incorect Prefix:: cannot generate number!";

        $query = DB::select("select * from ba_no where prefix='".$prefix."'");
        if ($query) {
            $previous = $query[0]->last_no - 1;
            $urut  = str_repeat("0",6 - strlen($query[0]->last_no)) . $query[0]->last_no;
            $no_ba = $urut . "/" . strtoupper($prefix) . "/" . romawi(date('m')) . "/" . date("Y");
        }
        
        $exist = DB::select("select * from ba_event where prefix ='".$prefix."' and id_sub_event='".$id_sub_event."'");

        if(!$exist) {
            DB::update("update ba_no set last_no = last_no + 1 where prefix='".$prefix."'");
            DB::insert("insert into ba_event (no_ba,id_sub_event,prefix, tanggal_ba, created_at) values('".$no_ba."',".$id_sub_event.",'".$prefix."','".date("Y-m-d")."','".\Carbon\Carbon::now()."')");
        }
        return str_repeat("0",6 - strlen($previous)) . $previous . "/" . strtoupper($prefix) . "/" . romawi(date('m')) . "/" . date("Y");
    }    
}

if (!function_exists('data_ba_committee')) {
    function data_ba_committee($id_sub_event){
        return SubEventTalentComitee::with(['employees' => function($subq){
            $subq->with(['positions' => function($subqtwo) {
                $subqtwo->with('get_sub_holding');
            }]);
        }])
        ->where('id_subevent', $id_sub_event)
        ->get()
        ->map(function($value){
            return [
                'name' => $value->employees->personnel_number,
                'position' => $value->employees->positions->name_position,
                'organisasi' => $value->employees->positions->get_sub_holding->name_subholding
            ];
        })
        ->toArray();
    }
}

if (!function_exists('talent_committee_default')) {
    /**
     * Fungsi ini digunakan untuk mengambil default talent commiittee
     * Yang digunakan pada form talent committee tambah sub event
     */
    function talent_committee_default($code_position) {
        if (!$code_position){ 
            return $tc = [];
        } else {
            $data = Position::where('code_position', $code_position)
                ->select('org_level', 'code_subholding', 'code_directorate')
                ->first();

            $bod = $data->org_level;
            $subholding = $data->code_subholding;
            $directorate = $data->code_directorate;

            $basic = [
                [
                    "nik" => get_employee_nik_by_jabatan("President Director", "inj") ?? ""
                ],
                [
                    "nik" => get_employee_nik_by_jabatan("Vice President Director", "inj") ?? ""
                ],
                [
                    "nik" => get_employee_nik_by_jabatan("Director%", "inj") ?? ""
                ]
            ];

            // Semua proses yang berkaitan dengan BOD & BOC Member
            // Semua proses yang berkaitan dengan BOD-1 INJ
            if ($bod == "BOD" || $bod == "BOC" || ($bod == "BOD-1" && $subholding == "inj")) {
                return $basic;
            }
            
            if ($subholding != "inj" && ($bod == "BOD" || $bod == "BOC" || $bod == "BOD-1")) {
                // $direktur_utama = DB::table('master_position')
                //     ->selectRaw('personnel_number')
                //     ->join('master_area', 'master_position.area', '=', 'master_area.code_area')
                //     ->join('employees', DB::raw('employees.position collate utf8mb4_general_ci'), '=', 'master_position.code_position')
                //     ->where('master_position.code_subholding', $subholding)
                //     ->orderBy('grade_align_max', 'DESC')
                //     ->limit(1)->first(); 
                $direktur_utama = DB::table('master_position')
                    ->join('master_area', 'master_position.area', '=', 'master_area.code_area')
                    ->join('employees', DB::raw('employees.position collate utf8mb4_general_ci'), '=', 'master_position.code_position')
                    ->where('master_position.code_subholding', $subholding)
                    ->where('master_position.org_level', 'BOD')
                    ->orderBy('grade_align_max', 'DESC')
                    ->value('prev_persno');
               
                if ($subholding == "ap1") {
                    $direktur_sdm = 'Direktur Sumber Daya Manusia dan Umum';
                } else if($subholding == "ap2"){
                    $direktur_sdm = 'Director of Human Capital';
                } else if($subholding == "hin"){
                    $direktur_sdm = 'DIREKTUR SDM';
                } else if($subholding == "twc"){
                    $direktur_sdm = 'Direktur Keuangan, Investasi, Manajemen Risiko & SDM';
                } else if($subholding == "snh"){
                    $direktur_sdm = 'Direktur Keuangan, Manajemen Risiko dan Administrasi';
                } else if($subholding == "itdc"){
                    $direktur_sdm = 'Technical & Human Capital Director';
                }

                $tc = [
                    [
                        "nik" => $direktur_utama ?? ""
                    ],
                    [
                        "nik" => get_employee_nik_by_jabatan($direktur_sdm, $subholding) ?? ""
                    ]
                ];

                return array_merge($basic, $tc);
            } 

            if ($subholding == "inj" && $bod == "BOD-2") {
                $tc = [
                    [
                        "nik" => get_employee_nik_by_jabatan("Director of Human Capital & Digital", $subholding) ?? ""
                    ],
                    [
                        "nik" => get_employee_nik_by_jabatan("Director%", $subholding, $directorate) ?? ""
                    ],
                    [
                        "nik" => get_employee_nik_by_jabatan("Vice President of Human Capital Business Partner", $subholding) ?? ""
                    ]
                ];

                return $tc;
            } 
            
            if ($bod == "BOD-2" || $bod == "BOD-3" || $bod == "officer") {
                $tc = [
                    [
                        "nik" => get_employee_nik_by_jabatan("Vice President of Human Capital Business Partner", "inj") ?? ""
                    ],
                    [
                        "nik" => get_employee_nik_by_jabatan("%Vice President%", $subholding, $directorate) ?? ""
                    ],
                    [
                        "nik" => get_employee_nik_by_jabatan("Assistant Vice President of Human Capital Business Partner", "inj") ?? ""
                    ]
                ];

                return $tc;
            }

            return $tc = [];

        }
    }    
}

if (!function_exists('get_employee_by_jabatan')) {
    /**
     * Fungsi ini digunakan untuk mendapatkan nik pegawai berdasarkan posisi atau jabatan
     * @var val adalah posisi / jabatan
     * @var sub adalah subholding
     * @var dir adalah directorate
     * @var only_bod is self explanatory
     */
    function get_employee_nik_by_jabatan($val, $sub = null, $dir = null, $only_bod = true) {
        return Employee::join('master_position', DB::raw('employees.position collate utf8mb4_unicode_ci'), 'master_position.code_position')
            ->where('master_position.name_position', 'LIKE', $val)
            ->when($sub, function($que) use ($sub) {
                $que->where('master_position.code_subholding', $sub);
            })
            ->when($dir, function($que) use ($dir) {
                $que->where('master_position.code_directorate', $dir);
            })
            ->when($only_bod, function($que) {
                $que->where('master_position.org_level', 'BOD');
            })
            ->orderBy('master_position.grade_align_max', 'DESC')
            ->value('prev_persno');
    }
}

if (!function_exists('getSubHolding')) {
    function getSubHolding($code) {
        $result = SubHolding::where('code_subholding', $code)->first()->name_subholding;
        return $result;
    }
}
if (!function_exists('getResultAssessment')) {
    function getResultAssessment($nik) {

        $result = AssessmentResult::where('nik', $nik)->first();
        if ($result) {
            return $result->hasil_ases;
        }
        return '-';
    }
}
if (!function_exists('getEndorsement')) {
    function getEndorsement($nik, $code, $id_event) {
        $result = TalentAspiration::with('employee')->where('nik', $nik)->where('code_position', $code)->where('id_event', $id_event)->get();
        if ($result) {
            return $result;
        }
        return '-';
    }
}

if (!function_exists('tanggal_indo')) {
    function tanggal_indo($tanggal)
    {
        $bulan = array (1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }
}

if (!function_exists('Performanceindex')) {
    function Performanceindex($nik, $id_sub_event) {
        $result = EmployeeEQS::where('nik', $nik)->where('sub_event', $id_sub_event)->first();
        if ($result) {
            $kinerja = $result->kinerja;
            $v = ($result->kinerja * 80 / 100) + ($result->track_record * 20 / 100);
            $d = ElementEQS::where('batas_bawah','<' , $v)->where('id_eqs', '1')->first();
    
            if ($d) {
                return $d->keterangan;
            }
            return 'Poor';
        }
        return "Poor";
    }
}

if (!function_exists('masakerja_job_family')) {
    function masakerja_job_family($awal, $akhir)
    {
        $date_awal = new DateTime($awal);
        $date_akhir = new DateTime($akhir);
        if ($date_awal > $date_akhir) { 
            // exit("0 Years 0 months ");
            return $awal;
        }
        $y = $date_akhir->diff($date_awal)->y;
        $m = $date_akhir->diff($date_awal)->m;
        $d = $date_akhir->diff($date_awal)->d;
        return $y." Years ".$m." months";
    }
}

if (!function_exists('employee_job_family')) {
    function employee_job_family($listed_employee, $code_job_family, $is_kalkulasi_eqs=false) {
        $history_employee = DB::table('master_position_history')
                ->where(function($query) use($code_job_family) {
                    $query->where('master_position_history.code_job_family_align', $code_job_family)
                        ->orWhere('master_position_history.code_job_family_align_2', $code_job_family)
                        ->orWhere('master_position_history.code_job_family_align_3', $code_job_family);
                })
                ->whereIn('nik', $listed_employee)
                ->groupBy('id');
        if ($is_kalkulasi_eqs) {
            $have_experience = $history_employee->get()->groupBy('nik')->map(function($value) {
                $total = [
                    'y' => 0,
                    'm' => 0,
                    'd' => 0
                ];
                $value->map(function($data_pertahun, $key) use(&$total) {
                    $tanggal_mulai = new DateTime($data_pertahun->start_date, new DateTimeZone('Asia/Jakarta'));
                    $enddate = (is_null($data_pertahun->end_date) || $data_pertahun->end_date == '0000-00-00') ? date("Y-m-d") : $data_pertahun->end_date;
                    $now = new DateTime($enddate, new DateTimeZone('Asia/Jakarta'));
                    $total['y'] += $now->diff($tanggal_mulai)->format("%R%y");
                    $total['m'] += $now->diff($tanggal_mulai)->format("%R%m");
                    $total['d'] += $now->diff($tanggal_mulai)->format("%R%d");
                });
                $m = $total['m'] + ($total['d'] / 30);
                $y = $total['y'] + ($m / 12);
                $total = ($y < 0) ? $y * -1 : $y;
                if ($total >= 0 && $total <= 5) {
                    return [
                        'nik' => $value->first()->nik,
                        'experience' => 75,
                    ];
                }else if ($total > 5 && $total <= 10) {
                    return [
                        'nik' => $value->first()->nik,
                        'experience' => 100,
                    ];
                }else if ($total > 10 && $total <= 15) {
                    return [
                        'nik' => $value->first()->nik,
                        'experience' => 110,
                    ];
                }else if ($total > 15) {
                    return [
                        'nik' => $value->first()->nik,
                        'experience' => 120,
                    ];
                }else {
                    return [
                        'nik' => $value->first()->nik,
                        'experience' => 75,
                    ];
                }
                return $value;
            })->sortBy([
                function($a, $b) {
                    return $a['experience'] < $b['experience'];
                }
            ]);
            $experience = collect($listed_employee);
            return $experience->transform(function($value, $key) use($have_experience) {
                return [
                    'nik' => $value,
                    'experience' => $have_experience->where('nik', $value)->first() ? $have_experience->where('nik', $value)->first()['experience'] : 75
                ];
            });
        }
        return $history_employee->get()->groupBy('nik')->map(function($value) {
            $list_start_menjabat = $value->pluck('start_date');
            $list_akhir_menjabat = $value->pluck('end_date');
            $tahun_awal = date("Y-m-d", strtotime($list_start_menjabat->first()));
            $condition_akhir_menjabat = (is_null($list_akhir_menjabat->last()) || $list_akhir_menjabat->last() == '0000-00-00') ? date("Y-m-d") : $list_akhir_menjabat->last();
            $tahun_akhir = date("Y-m-d", strtotime($condition_akhir_menjabat));
            return masakerja_job_family($tahun_awal, $tahun_akhir);
        })->first();
    }
}

if (!function_exists('count_curr_nom_percent')) {
    /**
     * Fungsi ini digunakan untuk menghitung persentase nominasi talent saat ini dalam bentuk persen
     * @var nominated_type_total digunakan untuk total nominasi jenis, contoh: nominated_female
     * @var total_nominated adalah keseluruhan nominasi
     */
    function count_curr_nom_percent($nominated_type_total, $total_nominated) {
        if($nominated_type_total && $total_nominated) {
            return round(($nominated_type_total / $total_nominated) * 100);
        } else {
            return 0;
        }
    }
}

if (!function_exists('count_lads_total')) {
    /**
     * Fungsi ini digunakan untuk menghitung jumlah nominasi dalam bentuk angka
     * @var total_nominated adalah keseluruhan nominasi
     * @var persentase adalah persentase nominasi
     */
    function count_lads_total($total_nominated, $persentase) {
        return round(($total_nominated * $persentase) / 100);
    }
}

if (!function_exists('count_bar_percent')) {
    /**
     * Fungsi ini digunakan untuk menghitung persentase yang digunakan dalam bar chart
     * @var curr_nom_percent adalah persentase nominasi yang digenerate oleh fungsi count_curr_nom_percent
     * @var persentase adalah persentase nominasi
     */
    function count_bar_percent($curr_nom_percent, $persentase) {
        if ($curr_nom_percent && $persentase) {
            return round(($curr_nom_percent / $persentase) * 100);
        } else {
            return 0;
        }
    }
}


if (!function_exists('menu_ongoing_event')) {
    /**
     * Fungsi ini digunakan untuk menampilkan list on going event
     */
    function menu_ongoing_event() {
        // Init
        $event_id = EventWindowTimeModel::where('status', 'open')->select('id', 'name', 'start_date', 'end_date')->first()->id ?? -1;
        if(Session::get('role') == "talent_committee") {
            $query = DB::table('sub_event')
                ->join('sub_event_tcom', 'sub_event.id', 'sub_event_tcom.id_subevent')
                ->whereNull('deleted_at')
                ->where('id_event', $event_id)
                ->where('sub_event_tcom.nik', Sentinel::getUser()->nik)
                ->get();

            $list_item = [
                [
                    'title' => 'Talent Day',
                    'page' => 'talent-day',
                    'status' => 'talent_day',
                ],
                [
                    'title' => 'Talent Review',
                    'page' => 'talent-review',
                    'status' => 'talent_review',
                ]
            ];
        } else {
            $query = DB::table('sub_event')->whereNull('deleted_at')->where('id_event', $event_id)->get();
    
            $list_item = [
                [
                    'title' => 'Talent Sourcing',
                    'page' => 'talent-sourcing',
                    'status' => 'sourcing',
                ],
                [
                    'title' => 'Talent Profiling',
                    'page' => 'talent-profiling',
                    'status' => 'profiling',
                ],
                [
                    'title' => 'Talent Mapping',
                    'page' => 'talent-mapping',
                    'status' => 'mapping',
                ],
                [
                    'title' => 'Talent Day',
                    'page' => 'talent-day',
                    'status' => 'talent_day',
                ],
                [
                    'title' => 'Talent Review',
                    'page' => 'talent-review',
                    'status' => 'review',
                ],
                [
                    'title' => 'Talent Selection',
                    'page' => 'talent-selection',
                    'status' => 'selection',
                ]
            ];
        }
        $data = [];
        foreach ($list_item as $item) {
            $title = $item['title'];
            $status = $item['status'];
            $page = $item['page'];
            
            $jumlah_event =  isset($query->groupBy('status')[$status]) ? $query->groupBy('status')[$status]->count() : 0;
            $data[] = [
                'title' => $title,
                'page' => $page,
                'label' => [ 
                    'type'  => 'label-rounded label label-primary mt-3',
                    'value' => $jumlah_event,
                    'display' => ($jumlah_event > 0)
                ],
            ];
            
        }
        return $data;
    }
}

if (!function_exists('menu_admin')) {
    /**
     * Fungsi ini digunakan untuk menampilkan menu admin
     */
    function menu_admin() {
        return [
            'items' => [
                // Dashboard
                [
                    'title' => 'Dashboard',
                    'root' => true,
                    'icon' => [
                        'icon' => 'media/svg/icons/Design/Layers.svg',
                        'svg-class' => 'svg-icon svg-icon-primary'
                    ],                    
                    'page' => 'dashboard',
                    'new-tab' => false,
                ],
        
                [
                    'title' => 'Pending Task',
                    'root' => false,
                    'icon' => [
                        'icon' => 'media/svg/icons/Navigation/Waiting.svg'
                    ],
                    'page' => 'pending-task',
                    'new-tab' => false,
                    'label' => [
                        'type'  => 'label-rounded label label-primary jumlah_pending_task',
                        'value' => 0,
                        'display' => true
                    ],
                ],
        
                [
                    'section' => 'Main Menu',
                ],
        
                [
                    'title' => 'Event',
                    'icon' => [
                        'icon' => 'media/svg/icons/Devices/LTE2.svg',
                        'svg-class' => 'svg-icon svg-icon-danger'
                    ],
                    'bullet' => 'line',
                    'root' => false,
                    'submenu' => [
                        [
                            'title' => 'Event Aspiration',
                            'page' => 'event'
                        ],
                        [
                            'title' => 'Event Positions',
                            'page' => 'sub-event'
                        ]
                    ]
                ],
                [
                    'title' => 'On Going Event',
                    'icon' => [
                        'icon' => 'media/svg/icons/General/Star.svg',
                        'svg-class' => 'svg-icon svg-icon-warning'
                    ],
                    'bullet' => 'line',
                    'root' => true,
                    'submenu' => menu_ongoing_event()
                ],
                [
                    'title' => 'Struktur Organisasi',
                    'icon' => [
                        'icon' => 'media/svg/icons/Code/Git4.svg',
                        'svg-class' => 'svg-icon svg-icon-dark-25'
                    ],
                    'bullet' => 'line',
                    'root' => false,
                    'page' => 'struktur-organisasi',
                    'new-tab' => false,
                    'label' => [
                        'type'  => 'label-rounded label label-primary',
                        'value' => 1,
                        'display' => false
                    ],
                ],
                [
                    'title' => 'Learning & Development',
                    'icon' => [
                        'icon' => 'media/svg/icons/Home/Book-open.svg',
                        'svg-class' => 'svg-icon svg-icon-success'
                    ],
                    'bullet' => 'line',
                    'root' => false,
                    'page' => '#',
                    'new-tab' => false,
                    'label' => [
                        'type'  => 'label-rounded label label-primary',
                        'value' => 1,
                        'display' => false
                    ],
                ],
                [
                    'title' => 'Successor List',
                    'icon' => [
                        'icon' => 'media/svg/icons/Communication/Group.svg',
                        'svg-class' => 'svg-icon svg-icon-primary'
                    ],
                    
                    'bullet' => 'line',
                    'root' => false,
                    'page' => 'successor-list',
                    'new-tab' => false,
                    'label' => [
                        'type'  => 'label-rounded label label-primary',
                        'value' => 1,
                        'display' => false
                    ],
                ],
                [
                    'title' => 'Search Employee',
                    'root' => false,
                    'icon' => 'media/svg/icons/General/Search.svg',
                    'page' => 'SearchEmployee/SearchDataEmployee',
                    'new-tab' => false,
                ],
                [
                    'title' => 'Talent Pool',
                    'root' => false,
                    'icon' => [
                        'icon' => 'media/svg/icons/Communication/Shield-user.svg',
                        'svg-class' => 'svg-icon svg-icon-success'
                    ],
                    'page' => 'settings/talent_poll',
                    'new-tab' => false,
                ],
                // Custom
                [
                    'section' => 'Master Data',
                ],
                [
                    'title' => 'Master Data',
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'bullet' => 'line',
                    'root' => true,
                    'submenu' => [
                        [
                            'title' => 'Master Employee',
                            'page' => 'masterData/masterDataEmployee'
                        ],
                        [
                            'title' => 'Master Posisi',
                            'page' => '#'
                        ],
                        [
                            'title' => 'Master Grade',
                            'page' => '#'
                        ],
                        [
                            'title' => 'Master Unit',
                            'page' => '#'
                        ],
                        [
                            'title' => 'Master Subholding',
                            'page' => '#'
                        ],
                        [
                            'title' => 'Master Competency',
                            'page' => '#'
                        ],
                        [
                            'title' => 'Master Learning',
                            'page' => '#'
                        ],
                        [
                            'title' => 'Master Training',
                            'page' => '#'
                        ],
                        // TODO: @Awal; saya hide dulu
                        [
                            'title' => 'Histori Berita Acara',
                            'page' => 'Histori/BeritaAcara'
                        ]
                    ]
                ],
                [
                    'title' => 'Tools & Setting',
                    'icon' => [
                        'icon' => 'media/svg/icons/Design/Component.svg',
                        'svg-class' => 'svg-icon svg-icon-danger'
                    ],
                    'bullet' => 'line',
                    'root' => true,
                    'submenu' => [
                        [
                            'title' => 'Vacant Position',
                            'page' => '#'
                        ],
                        [
                            'title' => 'Employee External',
                            'page' => 'employee_external'
                        ]
                    ]
                ],
        
                // Custom
                [
                    'section' => 'My Profile',
                ],
                [
                    'title' => 'Personal Information',
                    'icon' => 'media/svg/icons/General/User.svg',
                    'bullet' => 'line',
                    'root' => false
                ],
            ]
        
        ];
    }
}

if (!function_exists('menu_talent_committee')) {
    /**
     * Fungsi ini digunakan untuk mengambil menu role talent committee
     */
    function menu_talent_committee() {
        return [
            'items' => [
                [
                    'title' => 'Dashboard',
                    'root' => true,
                    'icon' => [
                        'icon' => 'media/svg/icons/Design/Layers.svg',
                        'svg-class' => 'svg-icon svg-icon-primary'
                    ],                    
                    'page' => 'dashboard',
                    'new-tab' => false,
                ],
                [
                    'title' => 'Search Employee',
                    'root' => false,
                    'icon' => 'media/svg/icons/General/Search.svg',
                    'page' => 'SearchEmployee/SearchDataEmployee',
                    'new-tab' => false,
                ],
                [
                    'title' => 'Talent Pool',
                    'root' => false,
                    'icon' => [
                        'icon' => 'media/svg/icons/Communication/Shield-user.svg',
                        'svg-class' => 'svg-icon svg-icon-success'
                    ],
                    'page' => 'settings/talent_poll',
                    'new-tab' => false,
                ],
                [
                    'section' => 'Main Menu',
                ],
                [
                    'title' => 'On Going Event',
                    'icon' => [
                        'icon' => 'media/svg/icons/General/Star.svg',
                        'svg-class' => 'svg-icon svg-icon-warning'
                    ],
                    'bullet' => 'line',
                    'root' => true,
                    'submenu' => menu_ongoing_event()
                ]
            ]
        ];
    }
}

if (!function_exists('getJobAlignment')) {
    function getJobAlignment($nik) {

        $result = Employee::where('prev_persno', $nik)->with('positions')->first();

        if ($result) {
            return $result->positions->grade_align_max ?? "-";
        }
        return '-';
    }
}

if (!function_exists('genderPrefix')) {
    /**
     * Fungsi ini digunakan pada saat generate enail
     * Untuk menentukan panggilan berdasarkan jenis kelamin
     * @var jk adalah jenis kelamin
     */
    function genderPrefix($jk) {
        switch (strtolower($jk)) {
            case 'male':
            case 'laki-laki':
            case 'laki - laki':
            case 'pria':
                $gender = "Bapak";
                break;

            case 'female':
            case 'perempuan':
                $gender = 'Ibu';
                break;
            
            default:
                $gender = "Saudara/i";
                break;
        }
        return $gender;
    }
}

if (!function_exists('genderSubprefix')) {
    /**
     * Fungsi ini digunakan pada saat generate enail
     * Untuk menentukan panggilan berdasarkan jenis kelamin
     * @var jk adalah jenis kelamin
     */
    function genderSubprefix($jk) {
        switch (strtolower($jk)) {
            case 'male':
            case 'laki-laki':
            case 'laki - laki':
            case 'pria':
                $gender_sub = "Bpk";
                break;

            case 'female':
            case 'perempuan':
                $gender_sub = "Ibu";
                break;
            
            default:
                $gender_sub = "Saudara/i";
                break;
        }
        return $gender_sub;
    }
}
if (!function_exists('getAreaLocationWork')) {
    function getAreaLocationWork($code) {
        $area = DB::table('master_position')
        ->select('master_area.name_area')
        ->join('master_area', 'master_position.area', '=', 'master_area.code_area')
        ->join('employees', DB::raw('employees.position collate utf8mb4_general_ci'), '=', 'master_position.code_position')
        ->where('master_position.code_position', $code)
        ->first();
        if ($area->name_area != null) {
            return $area->name_area;
        }else{
            return '-';
        }
    }
}

if (!function_exists('masakerja_employee_job_family')) {
    function masakerja_employee_job_family($listed_employee, $code_job_family) {
        // * Querynya ditambahin end_date != 0000-00-00, karena permintaan dari user kalo data end_date nya ga ada maka dianggap kosong aja pengalamannya (modification by irp4ndi)
        $history_employee = DB::table('master_position_history')
                ->where([
                    ['master_position_history.code_job_family_align', '=', $code_job_family],
                    ['nik', '=', $listed_employee],
                    ['end_date', '!=', '0000-00-00']
                ])
                ->whereNotNull('end_date')
                ->get();

            if (count($history_employee) > 0) {
                $tahun = [];
                $bulan = [];
                foreach ($history_employee as $key => $v) {
                    $start = new DateTime($v->start_date);
                    if ($v->end_date == '0000-00-00') { // * kondisi ini seharusnya sudah tidak diperlukan, soalnya querynya sudah saya tambahkan end_date != 0000-00-00
                        $end = new DateTime('today');
                    }else{
                        $end = new DateTime($v->end_date);
                    }

                    if ($start > $end) { 
                        return 0;
                    }
                    $y = $end->diff($start)->y;
                    $m = $end->diff($start)->m;
                    $d = $end->diff($start)->d;

                    $tahun[] =$y;
                    $bulan[] =$m;
                }

                if (array_sum($bulan) > 12) {
                    $mm = round(array_sum($bulan)/12, 1);
                }else{
                    $mm = 0..".".array_sum($bulan);
                }
                

                if (array_sum($tahun) > 0) {
                    $yy = array_sum($tahun);
                }else{
                    $yy = 0;
                }
                $hitung = $yy + $mm;
                return (isset($xx[0]) ? $xx[0] : 0) ." Years " . (isset($xx[1]) ? $xx[1] : 0) ." months";

            }else{
                return '-';
            }
    }
}
if (!function_exists('masakerja_employee')) {
    function masakerja_employee($nik, $position) {
        // * Querynya ditambahin end_date != 0000-00-00, karena permintaan dari user kalo data end_date nya ga ada maka dianggap kosong aja pengalamannya (modification by irp4ndi)
        $history_employee = DB::table('master_position_history')
        ->whereRaw('LOWER(master_position_history.position_name) like ? ', strtolower($position))
        ->where([
            ['nik', '=', $nik],
            ['end_date', '!=', '0000-00-00']
        ])
        ->whereNotNull('end_date')
        ->get();

        if (count($history_employee) > 0) {
            $tahun = [];
            $bulan = [];
            foreach ($history_employee as $key => $v) {
                $start = new DateTime($v->start_date);
                if ($v->end_date == '0000-00-00') { // * kondisi ini seharusnya sudah tidak diperlukan, soalnya querynya sudah saya tambahkan end_date != 0000-00-00
                    $end = new DateTime('today');
                }else{
                    $end = new DateTime($v->end_date);
                }
    
                if ($start > $end) { 
                    return 0;
                }
                $y = $end->diff($start)->y;
                $m = $end->diff($start)->m;
                $d = $end->diff($start)->d;

                $tahun[] =$y;
                $bulan[] =$m;
            }
            if (array_sum($bulan) > 12) {
                $mm = round(array_sum($bulan)/12, 1);
            }else{
                $mm = 0..".".array_sum($bulan);
            }
            
    
            if (array_sum($tahun) > 0) {
                $yy = array_sum($tahun);
            }else{
                $yy = 0;
            }
    
    
            $hitung = $yy + $mm; 
            $xx = explode(".",$hitung);
            return (isset($xx[0]) ? $xx[0] : 0) ." Years " . (isset($xx[1]) ? $xx[1] : 0) ." months";
        }else{
            return '-';
        }


    }
}
if (!function_exists('employee_prefs')) {
    /**
     * Fungsi ini digunakan untuk mengambil info employee / user
     */
    function employee_prefs($nik, $section) {
        if($section == "nomenklatur") {
            $value = EmployeesToPositions::join('master_position', 'employees_to_positions.position_code', 'master_position.code_position')
                ->join('master_area', 'master_position.area', 'master_area.code_area')
                ->where('employees_to_positions.nik', $nik)
                ->orderBy('end_date', 'DESC')
                ->value('name_area');
            return $value ?? "-";
        } else if ($section == 'position') {
            $value = EmployeesToPositions::join('master_position', 'employees_to_positions.position_code', 'master_position.code_position')
                ->where('employees_to_positions.nik', $nik)
                ->orderBy('end_date', 'DESC')
                ->value('name_position');
            return $value ?? "-";
        } else if ($section == 'job_family') {
            $value = EmployeesToPositions::join('master_position', 'employees_to_positions.position_code', 'master_position.code_position')
                ->join('master_job_family', 'master_position.code_job_family', 'master_job_family.code_job_family')
                ->where('employees_to_positions.nik', $nik)
                ->orderBy('end_date', 'DESC')
                ->value('name_job_family');
            return $value ?? "-";
        }
    }
}
if (!function_exists('getPosisi')) {
    function getPosisi($code) {

            $result = DB::table('master_position')
            ->select('name_position')
            ->where('master_position.code_position', $code)
            ->first();
            if ($result->name_position != null) {
                $xx = ucwords(strtolower($result->name_position));
            }else{
                $xx = '-';
            }
            return $xx;
    }
}

if (!function_exists('query_employees')) {
    /**
     * fungsi yang digunakan untuk menambilkan data employee
     * @var req adalah request untuk datatable
     * @var is_talent_poll digunakan jika request dari talent_poll
     */
    function query_employees($req, $is_talent_pool=false) {
        // Mengambil list nik yang tidak dalam masa hukuman disipilin
        $nik = MasterHukdis::where(DB::raw('NOW()'), '<=', 'end_date')->get()->pluck('nik');

        // Mengecek Event Active Kebutuan Talent Poll
        $event_id = EventWindowTimeModel::where('status', '=', 'open')->first()->id ?? false;

        $filterSubholding = (isset($req->filterSubholding) && !is_null($req->filterSubholding) || !empty($req->filterDirectorate)) || isset($req->filterDirectorate) && !is_null($req->filterDirectorate) && !empty($req->filterDirectorate);
        // Query Data Employee
        $data = Employee::whereNotIn('prev_persno', $nik)
            ->with('subholding')
            ->with(['positions' => function($query) {
                $query->with('master_area')
                    ->with('sub_holding');
            }])
            // Status Kalibrasi Employee Kebutuan Talent Poll
            ->with(['employee_calibration_history' => function($query) use($event_id) {
                $sub_event = [-1];
                if ($event_id) {
                    $sub_event = SubEvent::where('id_event', $event_id)->pluck('id');
                }
                $query->whereIn('sub_event', $sub_event);
            }])
            ->orderBy('employees.id', 'desc')
            ->when($is_talent_pool, function($query) {
                $query->whereNotNull('position');
            })
            // Query where filter job family employee
            ->when(isset($req->filterJobFamily) && !is_null($req->filterJobFamily) && !empty($req->filterJobFamily), function($query) use($req) {
                $query->where(DB::raw('TRIM(LOWER(job_family_align))'), 'like', '%' . trim(strtolower($req->filterJobFamily)) . '%');
            })
            // Query where subholding / organization employee
            ->when($filterSubholding, function($query) use($req) {
                $query->whereHas('collated_positions', function($subquery) use($req) {
                    $subquery->where('code_subholding', '=', $req->filterSubholding);
                });
            })
            // Query where filter grade align employee
            ->when(isset($req->filterGrade) && !is_null($req->filterGrade) && !empty($req->filterGrade), function($query) use($req) {
                $query->where('person_grade_align', 'like', '%' . $req->filterGrade . '%');
            })
            // Query where filter employee sudah di kalibrasi / belum dikalibrasi
            ->when(isset($req->filterCalibrate) && !is_null($req->filterCalibrate) && !empty($req->filterCalibrate) && $req->filterCalibrate, function($query) use($event_id) {
                $query->whereHas('employee_calibration_history', function($query) use($event_id) {
                    $sub_event = [-1];
                    if ($event_id) {
                        $sub_event = SubEvent::where('id_event', $event_id)->pluck('id');
                    }
                    $query->whereIn('sub_event', $sub_event);    
                });
            })
            // Query where target posisi untuk mencari eligible employee
            ->when(isset($req->filtertargetPositions) && !is_null($req->filtertargetPositions) && !empty($req->filtertargetPositions), function($query) use($req) {
                $target_position = Position::where('code_position', $req->filtertargetPositions)->first();
                $code_job_family = [];
                if ($target_position) {
                    $code_job_family[] = $target_position->code_job_family;
                    $code_job_family[] = $target_position->code_job_family_2;
                    $code_job_family[] = $target_position->code_job_family_3;
                    $code_job_family = array_filter($code_job_family); 
                }
                $low_risk_job_family = DB::table('master_risk_movement')
                    ->whereIn('code_job_family_from', $code_job_family)
                    ->whereNotIn(DB::raw('TRIM(LOWER(risk_cat))'), ['medium - high', 'high'])
                    ->get()->pluck('code_job_family_to');
                $query->whereHas('collated_positions', function($subquery) use($low_risk_job_family) {
                    $subquery->whereIn('code_job_family', $low_risk_job_family)
                        ->orWhereIn('code_job_family_2', $low_risk_job_family)
                        ->orWhereIn('code_job_family_3', $low_risk_job_family);
                });
            })
            // Query where area nomenklatur
            ->when(isset($req->filterAreaNomenklatur) && !is_null($req->filterAreaNomenklatur) && !empty($req->filterAreaNomenklatur), function($query) use ($req) {
                $query->whereHas('collated_positions', function($subQuery) use ($req) {
                    $subQuery->where('area', $req->filterAreaNomenklatur);
                });
            })
            ->get();
        // TalentCluster
        
        // Get nik employee
        $prevPersno = $data->pluck('prev_persno')
            ->toArray();
        // Filter nik null
        $filterPrevPersno = array_filter($prevPersno);
        // Get default eqs employee
        $dataEqs = kalkulasi_eqs(['employee' => $filterPrevPersno], false);
        // Menentukan employee dengan box
        $boxes = array_map(function ($value) {
            $performance_index = ($value['kinerja'] * 80 / 100) + ($value['track_record'] * 20 / 100);
            return [
                'nik'               => $value['nik'],
                'performance_index' => $performance_index,
                'job_fit'           => $value['job_fit'],
                'box'               => talentMapping($value['job_fit'], $performance_index),
            ];
        }, $dataEqs);
        $boxes = collect($boxes);
        // Menggabungkan Talent Cluster dengan query Employee
        $data->transform(function($value) use($boxes) {            
            $box = $boxes->where('nik', $value->prev_persno)->first()['box'] ?? 1;
            $value->box = $box;
            $value->name_organization = $value->positions->sub_holding->name_subholding ?? "-";
            $value->name_position = $value->positions->name_position ?? "-";
            $value->name_area = $value->positions->master_area->name_area ?? "-";
            $value->predikat_cluster = predikat_talent_cluster($box)." (Talent Cluster $box)";
            return $value;
        });
        // Filter Talent Cluster
        if ($req->filterTalentCluster) {
            return $data->filter(function($value) use($req) {
                if (is_array($req->filterTalentCluster)){
                    return in_array($value->box, $req->filterTalentCluster);
                }
                return $value->box == $req->filterTalentCluster;
            });
        }

        return $data;
    }
}

if (!function_exists('get_box_default_employees')) {
    function get_eqs_default_employees($parameter, $return='eqs') {
        $bobot = EQS::get();
        $eqs = collect(kalkulasi_eqs($parameter, false))->map(function($value) use($bobot) {
            $value['mentah_kinerja'] = $value['kinerja'];
            $value['mentah_track_record'] = $value['track_record'];
            $value['mentah_job_fit'] = $value['job_fit'];
            $value['kinerja'] = round($value['kinerja'] * $bobot->where('element', 'Kinerja')->first()->bobot / 100, 2); 
            $value['track_record'] = round($value['track_record'] * $bobot->where('element', 'Track Record')->first()->bobot / 100, 2); 
            $value['job_fit'] = round($value['job_fit'] * $bobot->where('element', 'Kesesuaian Kompetensi')->first()->bobot / 100, 2); 
            $value['experience'] = round($value['experience'] * $bobot->where('element', 'Experience')->first()->bobot / 100, 2); 
            $value['training_sertifikasi'] = round(($value['training_sertifikasi'] / 200 * 100) * $bobot->where('element', 'Training dan Sertifikasi')->first()->bobot / 100, 2); 
            $value['aspiration'] = round($value['aspiration'] * $bobot->where('element', 'Aspirasi')->first()->bobot / 100, 2); 
            return $value;
        })->first();

        $eqs_final = array_sum([
            isset($eqs['kinerja']) ? round($eqs['kinerja'], 2) : 0,
            isset($eqs['track_record']) ? round($eqs['track_record'], 2) : 0,
            isset($eqs['job_fit']) ? round($eqs['job_fit'], 2) : 0,
            isset($eqs['experience']) ? round($eqs['experience'], 2) : 0,
            isset($eqs['training_sertifikasi']) ? round($eqs['training_sertifikasi'], 2) : 0,
            isset($eqs['aspiration']) ? round($eqs['aspiration'], 2) : 0,
        ]);

        if ($return == "box") {
            $performance_index = ($eqs['mentah_kinerja'] * 80 / 100) + ($eqs['mentah_track_record'] * 20 / 100);
            return talentMapping($eqs['mentah_job_fit'], $performance_index);
        }

        if ($return == "eqs_only") {
            return $eqs_final;
        }

        return $eqs_final . " of 107";
    }
}

if (!function_exists('penyebut')) {
    function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
}

if (!function_exists('terbilang')) {
    function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(penyebut($nilai));
		} else {
			$hasil = trim(penyebut($nilai));
		}     		
		return ucwords($hasil);
	}
}

if (!function_exists('tanggal_terbilang')) {
    function tanggal_terbilang($tgl,$bln,$thn) {
        return terbilang($tgl) . " bulan " . nama_bulan($bln) . " tahun " . terbilang($thn);
    }
}
if(!function_exists('bilangantext')){
    function bilangantext($bilangan)
    {
        if ($bilangan != null) {
            $angka = array('0','0','0','0','0','0','0','0','0','0',
            '0','0','0','0','0','0');
            $kata = array('','satu','dua','tiga','empat','lima',
                    'enam','tujuh','delapan','sembilan');
            $tingkat = array('','ribu','juta','milyar','triliun');

            $panjang_bilangan = strlen($bilangan);

            /* pengujian panjang bilangan */
            if ($panjang_bilangan > 15) {
            $kalimat = "Diluar Batas";
            return $kalimat;
            }

            /* mengambil angka-angka yang ada dalam bilangan,
            dimasukkan ke dalam array */
            for ($i = 1; $i <= $panjang_bilangan; $i++) {
            $angka[$i] = substr($bilangan,-($i),1);
            }

            $i = 1;
            $j = 0;
            $kalimat = "";


            /* mulai proses iterasi terhadap array angka */
            while ($i <= $panjang_bilangan) {

            $subkalimat = "";
            $kata1 = "";
            $kata2 = "";
            $kata3 = "";

            /* untuk ratusan */
            if ($angka[$i+2] != "0") {
            if ($angka[$i+2] == "1") {
            $kata1 = "seratus";
            } else {
            $kata1 = $kata[$angka[$i+2]] . " ratus";
            }
            }

            /* untuk puluhan atau belasan */
            if ($angka[$i+1] != "0") {
            if ($angka[$i+1] == "1") {
            if ($angka[$i] == "0") {
                $kata2 = "sepuluh";
            } elseif ($angka[$i] == "1") {
                $kata2 = "sebelas";
            } else {
                $kata2 = $kata[$angka[$i]] . " belas";
            }
            } else {
            $kata2 = $kata[$angka[$i+1]] . " puluh";
            }
            }

            /* untuk satuan */
            if ($angka[$i] != "0") {
            if ($angka[$i+1] != "1") {
            $kata3 = $kata[$angka[$i]];
            }
            }

            /* pengujian angka apakah tidak nol semua,
            lalu ditambahkan tingkat */
            if (($angka[$i] != "0") OR ($angka[$i+1] != "0") OR
            ($angka[$i+2] != "0")) {
            $subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
            }

            /* gabungkan variabe sub kalimat (untuk satu blok 3 angka)
            ke variabel kalimat */
            $kalimat = $subkalimat . $kalimat;
            $i = $i + 3;
            $j = $j + 1;

            }

            /* mengganti satu ribu jadi seribu jika diperlukan */
            if (($angka[5] == "0") AND ($angka[6] == "0")) {
            $kalimat = str_replace("satu ribu","seribu",$kalimat);
            }

            return trim($kalimat);
        }else{
            return '-';
        }          
    }
}

if (!function_exists('tanggal_dmy')) {
    /**
     * fungsi ini digunakan untuk mengubah tanggal dari database
     * menjadi format d M Y, contoh 19 Mar 2022
     * @var val adalah nilai dari database
     * @var sep adalah separator yang akan digunakan
     * @var zero_date_now jika true maka akan mengembalikan "Sekarang" apabila val = 0000-00-00
     */
    function tanggal_dmy($val, $sep, $zero_date_now) {
        if ($zero_date_now && $val == "0000-00-00") {
            return "Sekarang";
        } else if ($val) {
            return date("d".$sep."M".$sep."Y", strtotime($val));
        } else {
            return "-";
        }
    }
}
if (!function_exists('getArea')) {
    function getArea($code) {
        $area = MasterArea::where('code_area', $code)->first();
        if ($area != null) {
            return $area->name_area;
        }else{
            return '-';
        }
    }
}

if (!function_exists('get_employee_subholding')) {
    /**
     * Fungsi ini digunakan untuk mendapatkan data employee yang ada pada subholding
     * @var sub adalah code subholding
     */
    function get_employee_subholding($sub) {
        return Employee::join('master_position', DB::raw('employees.position collate utf8mb4_unicode_ci'), 'master_position.code_position')
            ->where('master_position.code_subholding', $sub)
            ->get();
    }
}

if (!function_exists('capitalize_roman')) {
    /**
     * Fungsi ini digunakan untuk membuat kapital setiap kalimat
     * Kecuali pada romawi kapital semua
     * @var words adalah kata / kalimat yang akan diubah
     */
    function capitalize_roman($words) {
        $pattern = '/\b(?![LXIVCDM]+\b)([A-Z]+)\b/';
        return preg_replace_callback($pattern, function($matches) {
            return ucfirst(strtolower($matches[0]));
        }, $words);
    }
}

if (!function_exists('get_total_question')) {
    /**
     * Fungsi ini digunakan untuk mendapatkan total pertanyaan
     */
    function get_total_question() {
        return Pertanyaan::count();
    }
}

if (!function_exists('assessment_subevent')) {
    /**
     * Fungsi ini digunakan untuk mendapatkan rata" niladi dari assessment
     * Atau total assessment yang telah terisi berdasarkan talent committee
     * @var id_subevent adalah id dari sub event
     * @var nik adalah nik dari talent / peserta
     * @var nik_tcom adalah nik dari talent committee
     * @var type adalah nilai yang dibutuhkan
     */
    function assessment_subevent($id_subevent, $nik, $nik_tcom, $type = null) {
        $total_mark = NilaiAssessment::where('sub_event', $id_subevent)
            ->where('nik', $nik)
            ->where('nik_talentcom', $nik_tcom);
        if ($type == "avg") {
            $total_mark = $total_mark->sum('nilai');
            if(get_total_question() && $total_mark) {
                return number_format($total_mark / get_total_question(), 2);
            } else {
                return 0;
            }
        } else if ($type == "status") {
            return $total_mark->value('is_draft') ? "Saved" : "Draft";
        } else if ($type == "raw_count") {
            return $total_mark->count();
        } else {
            return $total_mark->count() . " of " . get_total_question() . " completed";
        }
    }
}

if (!function_exists('assessment_label')) {
    /**
     * Fungsi ini digunakan untuk menentukan keterangan nilai assessment
     * @var total_nilai adalah total nilai dari assessment dibagi pertanyaan
     * @var with_style jika true maka akan mengembalikan dalam bentuk badge html
     */
    function assessment_label($total_nilai, $with_style = false) {
        $type = "";
        if(round($total_nilai) >= 5) {
            $type = "badge-success";
            $ket_nilai = "Sangat Baik";
        } else if (round($total_nilai) >= 4) {
            $type = "badge-primary";
            $ket_nilai = "Baik";
        } else if (round($total_nilai) >= 3) {
            $type = "badge-warning";
            $ket_nilai = "Cukup";
        } else if (round($total_nilai) >= 2) {
            $type = "badge-warning";
            $ket_nilai = "Kurang";
        } else {
            $type = "badge-danger";
            $ket_nilai = "Sangat Kurang";
        }

        if ($with_style) {
            $ket_nilai = "<span class='badge ". $type ." text-white w-100'>". $ket_nilai ."</span>";
        }

        return $ket_nilai;
    }
}

if (!function_exists('is_completed_assessment')) {
    /**
     * Fungsi ini digunakan untuk mendapatkan status assessment
     * Yang akan digunakan pada proses talent day
     * @var id merupakan id talent
     */
    function is_completed_assessment($id) {
        $talent = Talent::with('talent_committee')
            ->where('id', $id)
            ->first();

        $tmp = [];
        foreach ($talent->talent_committee as $value) {
            $tmp[] = assessment_subevent($talent->sub_event, $talent->nik, $value->nik, "raw_count") == get_total_question();
        }

        return (in_array(false, $tmp) == false);
    }
}

if (!function_exists('final_mark_assessment')) {
    /**
     * Fungsi ini digunakan untuk mendapatkan status assessment
     * Yang akan digunakan pada proses talent day
     * @var id merupakan id talent
     */
    function final_mark_assessment($id) {
        $talent = Talent::with('talent_committee')
            ->where('id', $id)
            ->first();

        $tmp = 0;
        $total_assessor = 0;
        foreach ($talent->talent_committee as $value) {
            $tmp += assessment_subevent($talent->sub_event, $talent->nik, $value->nik, "avg");
            $total_assessor++;
        }

        $total = 0;
        if ($tmp && $total_assessor) {
            $total = number_format($tmp / $total_assessor, 2);
        }

        return $total;
    }
}

if(!function_exists('star_rating')) {
    function start_rating($box){
        $box = 5;
        $rating = '<img src="'. assets('media/svg/icons/General/Star.svg') .'"/>';
        
        #1 HIPO
        #2 PROMOTABLE

    }
}