<?php

namespace App\Listeners;

use App\Events\XNAEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class XNAListener 
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\XNAEvent  $event
     * @return void
     */
    public function handle(XNAEvent $event)
    {
         // return false untuk menghentikan event; 
        // return false; 

        dd($event);

        $user = User::find($event->user_id)->toArray();

        Mail::send('emails.event_order_placed', ['user' => $user], function($message) use ($user) {
            $message->to($user['email']);
            $message->subject('Thanks for your order');
            $message->from('no-reply@shouts.dev', 'Shouts.dev');
        });
    }
}
