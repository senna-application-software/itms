var codePosition = $('#codePosition').val(),
    eventId = $('#eventId').val();

// * Init Document Jquery
$(function () {
    initModalAddEmployee();
    $('#employeeType').val("").trigger('change');
    $('.select2-global').select2({
        placeholder: "-Select-",
        allowClear: true
    });
    employeeList();
    employeeInternalList();
});

// * Function init modal add employee
function initModalAddEmployee() {
    $('#employee').html("");
    $('#proposal').prop('disabled', false);
    $('#proposal').prop('checked', false);
}

// * Method untuk get employee berdasarkan typenya
// TODO method ini sepertinya sudah tidak terpakai, mungkin dikesampingkan terlebih dahulu
$('#employeeType').on("change", function (e) {
    let value = $(this).val();

    if (value != null) {
        if (value == 1) {
            $('#proposal').prop('disabled', true);
            $('#proposal').prop('checked', true);
        } else {
            $('#proposal').prop('disabled', false);
            $('#proposal').prop('checked', false);
        }

        $.ajax({
            method: 'post',
            url: routeSelect2Employee,
            data: {
                _token: csrfToken,
                value: value,
                eventId: eventId,
                codePosition: codePosition
            },
            success: function (resp) {
                if (resp.data.length == 0) { // * cek jika data kosong dan type employee nya itu internal
                    Swal.fire('Sorry!', 'No Position', 'warning');
                }

                let html = `<option></option>`;
                $.each(resp.data, function (i, val) {
                    html += `<option value="${val.prev_persno}">${val.personnel_number}</option>`;
                });
                $('#employee').html("");
                $('#employee').html(html);
            },
            error: function (err) {
                console.log("Error : ", err);
            }
        });
    } else {
        initModalAddEmployee();
    }
});

// * Method untuk delete talent
$('#deleteTalentAspiration').on('click', function (e) {
    let dataTarget = e.target;

    Swal.fire({
        title: 'Are you sure ?',
        showCancelButton: true,
    }).then((result) => {
        routeDeleteTalentAspiration = routeDeleteTalentAspiration.replace(":id", dataTarget.getAttribute('data-id'));
        if (result.isConfirmed) {
            $.ajax({
                url: routeDeleteTalentAspiration,
                method: 'delete',
                data: {
                    _token: csrfToken
                },
                success: function (resp) {
                    location.reload();
                },
                error: function (err) {
                    console.log("Error !", err);
                }
            });
        }

        routeDeleteTalentAspiration = routeConstDeleteTalentAspiration;
    })
});

// * function untuk mengisi option employee
// TODO pengganti untuk employeeType
function employeeList() {
    $.ajax({
        method: 'post',
        url: routeSelect2Employee,
        data: {
            _token: csrfToken,
            value: 1,
            subEventId: $('#subEventId').val()
        },
        success: function (resp) {
            let html = `<option></option>`;
            $.each(resp.data, function (i, val) {
                html += `<option value="${val.prev_persno}">${val.personnel_number}</option>`;
            });
            $('#employee').html("");
            $('#employee').html(html);
        },
        error: function (err) {
            console.log("Error : ", err);
        }
    });
}

// * Function untuk get employee internal list untuk modal add employee internal
function employeeInternalList() {
    $.ajax({
        method: 'post',
        url: routeSelect2Employee,
        data: {
            _token: csrfToken,
            value: 0,
            jobFamily: true,
            codePosition: codePosition,
            subEventId: $('#subEventId').val()
        },
        success: function (resp) {
            let html = `<option></option>`;
            $.each(resp.data, function (i, val) {
                html += `<option value="${val.prev_persno}">${val.personnel_number}</option>`;
            });
            $('#employeeInternal').html("");
            $('#employeeInternal').html(html);
        },
        error: function (err) {
            console.log("Error : ", err);
        }
    });
}
