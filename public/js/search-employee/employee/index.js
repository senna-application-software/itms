// * Init Document Ready Jquery
$(function () {
    fillDataTable();
    $('#jobFamily').select2({
        placeholder: "-Select Job Family-",
        allowClear: true
    });

    $('#subholding').select2({
        placeholder: "-Select Organization-",
        allowClear: true
    });


    $('#grade').select2({
        placeholder: "-Select Person Grade-",
        allowClear: true
    });

    $('#talentCluster').select2({
        placeholder: "-Select Talent Cluster-",
        allowClear: true
    });
});

// * Show modal view data employee
$('body').on('click', '.viewDataEmployee', function (e) {
    e.preventDefault();
    let dataTarget = e.target;

    routeViewEmployee = routeViewEmployee.replace(':id', dataTarget.getAttribute('data-id'));
    $.ajax({
        url: routeViewEmployee,
        dataType: 'json',
        success: function (resp) {
            let employee = resp.data,
                tableEmployee;

            tableEmployee = viewEmployee(employee);

            $('#dataEmployee').html(tableEmployee);
            $('#modalViewEmployee').modal('show');
        },
        error: function (err) {
            console.log("Error : ", err);
        }
    });

    routeViewEmployee = routeConstViewEmployee; // * Untuk reset route view
});

// * Delete data employee
$('body').on('click', '.deleteEmployee', function (e) {
    e.preventDefault();
    let dataTarget = e.target;

    Swal.fire({
        title: 'Are you sure ?',
        showCancelButton: true,
    }).then((result) => {
        routeDeleteEmployee = routeDeleteEmployee.replace(":id", dataTarget.getAttribute('data-id'));
        if (result.isConfirmed) {
            $.ajax({
                url: routeDeleteEmployee,
                dataType: 'json',
                method: 'delete',
                data: {
                    _token: csrfToken
                },
                success: function (resp) {
                    Swal.fire('Deleted!', '', 'success');
                    $('#tableEmployee').DataTable().ajax.reload();
                },
                error: function (err) {
                    console.log("Error : ", err);
                }
            });
        }
        routeDeleteEmployee = routeConstDeleteEmployee;
    });
});

// * Function untuk fill datatabel employee
var fillDataTable = (filterJobFamily = '', filterGrade = '', filterSubholding = '', filterTalentCluster = '') => {
    // * Handle server side table processing
    $('#tableEmployee').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: routeDataTable,
            data: {
                filterJobFamily: filterJobFamily,
                filterGrade: filterGrade,
                filterSubholding: filterSubholding,
                filterTalentCluster: filterTalentCluster
            }
        },
        columns: [
            { data: "DT_RowIndex", name: 'DT_RowIndex' },
            { data: "prev_persno", name: 'prev_persno' },
            { data: "personnel_number", name: 'personnel_number' },
            { data: "name_organization", name: 'sub_holding' },
            { data: "name_position", name: 'position_name' },
            { data: "name_area", name: 'area_nomenklatur' },
            { data: "person_grade_align", name: 'person_grade_align' },
            { data: "status_karyawan", name: 'status_karyawan' },
            { data: "predikat_cluster", name: 'predikat_cluster' },
            { data: "action", name: 'action' },
        ]
    });
}

$('.filter').on('change', function () {
    var filterJobFamily = $('#jobFamily').val(),
        filterSubholding = $('#subholding').val(),
        filterGrade = $('#grade').val(),
        filterTalentCluster = $('#talentCluster').val();

    $('#tableEmployee').DataTable().destroy();
    $('#tableEmployee tbody').empty();
    fillDataTable(filterJobFamily, filterGrade, filterSubholding, filterTalentCluster);
});

function padTo2Digits(num) {
    return num.toString().padStart(2, '0');
}

// * Function untuk handle view data employee
function viewEmployee(data) {
    let tableEmployee,
        tanggalLahir;

    tanggalLahir = new Date(data.date_of_birth);
    let tglLahirConvert = [
        padTo2Digits(tanggalLahir.getDate()),
        padTo2Digits(tanggalLahir.getMonth() + 1),
        tanggalLahir.getFullYear(),
    ].join('-');

    tableEmployee = `<table class="table">
                <tr>
                    <td>Employee Name</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.personnel_number)}</td>
                </tr>

                <tr>
                    <td>Employee ID</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.prev_persno)}</td>
                </tr>

                <tr>
                    <td>Position Name</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.position_name)}</td>
                </tr>

                <tr>
                    <td>Job Title</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.job_title)}</td>
                </tr>

                <tr>
                    <td>Directorate</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.directorate)}</td>
                </tr>

                <tr>
                    <td>Directorate Code</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.directorate_code)}</td>
                </tr>

                <tr>
                    <td>Group</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.group)}</td>
                </tr>

                <tr>
                    <td>Group Code</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.group_code)}</td>
                </tr>

                <tr>
                    <td>Division</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.division)}</td>
                </tr>

                <tr>
                    <td>Division Code</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.division_code)}</td>
                </tr>

                <tr>
                    <td>Area Nomenklatur</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.area_nomenklatur)}</td>
                </tr>

                <tr>
                    <td>Gender</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.gender_text)}</td>
                </tr>

                <tr>
                    <td>Marst</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.marst)}</td>
                </tr>

                <tr>
                    <td>Birthplace</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.birthplace)}, ${tglLahirConvert}</td>
                </tr>

                <tr>
                    <td>Education Level</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.education_level)}</td>
                </tr>

                <tr>
                    <td>University Name</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.university_name)}</td>
                </tr>

                <tr>
                    <td>ID Card Number</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.no_ktp)}</td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.email)}</td>
                </tr>
                
                <tr>
                    <td>Phone Number</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.no_hp)}</td>
                </tr>

                <tr>
                    <td>Employee Type</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.status_karyawan)}</td>
                </tr>
            </table>`;

    return tableEmployee;
}

// * Function untuk filter employee data view
function employeeDataFilter(data) {
    let emptyString = "",
        emptyNull = null;

    if (data == emptyString || data == emptyNull) {
        data = "-";
    }

    return data;
}