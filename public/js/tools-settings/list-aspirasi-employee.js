$('#tableListAspirasiEmployee').DataTable({
    processing: true,
    serverSide: true,
    scrollX: true,
    ajax: {
        url: routeDataTableListAspirasiEmployee,
        data: {
            nik: nik
        }
    },
    columns: [
        { data: "DT_RowIndex", name: 'DT_RowIndex' },
        { data: "aspiration_type", name: 'tipe_aspirasi' },
        { data: "name_position", name: 'position_name' },
        { data: "name_subholding", name: 'name_subholding' },
        { data: "name_area", name: 'area_nomenklatur' }
    ]
});