// * Init Document Ready Jquery
$(function () {
    fillDataTable();
    $('#targetPosition').select2({
        placeholder: "-Select Position-",
        allowClear: true
    });

    $('#jobFamily').select2({
        placeholder: "-Select Job Family-",
        allowClear: true
    });

    $('#subholding').select2({
        placeholder: "-Select Organization-",
        allowClear: true
    });

    $('#grade').select2({
        placeholder: "-Select Person Grade-",
        allowClear: true
    });

    $('#talentCluster').select2({
        placeholder: "-Select Talent Cluster-",
        allowClear: true
    });

    $('#isCalibrate').select2({
        placeholder: "-Select Type Calibration-",
        allowClear: true
    });

    $('#areaNomenklatur').select2({
        placeholder: "-Select Nomenclature Area-",
        allowClear: true
    });

    optionFilter('position', $('#targetPosition'));
    optionFilter('area', $('#areaNomenklatur'));
});

// * Function untuk fill datatabel employee
var fillDataTable = (filterJobFamily = '', filterGrade = '', filterSubholding = '', filterTalentCluster = '', filterCalibrate = '', filtertargetPositions = '', filterAreaNomenklatur = '') => {
    // * Handle server side table processing
    $('#tableEmployee').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: routeDataTable,
            data: {
                filterJobFamily: filterJobFamily,
                filterGrade: filterGrade,
                filterSubholding: filterSubholding,
                filterCalibrate: filterCalibrate,
                filtertargetPositions: filtertargetPositions,
                filterTalentCluster: filterTalentCluster,
                filterAreaNomenklatur: filterAreaNomenklatur
            }
        },
        columns: [
            { data: "DT_RowIndex", name: 'DT_RowIndex' },
            { data: "prev_persno", name: 'prev_persno' },
            { data: "personnel_number", name: 'personnel_number' },
            { data: "name_organization", name: 'sub_holding' },
            { data: "name_position", name: 'position_name' },
            { data: "name_area", name: 'area_nomenklatur' },
            { data: "person_grade_align", name: 'person_grade_align' },
            { data: "status_karyawan", name: 'status_karyawan' },
            { data: "predikat_cluster", name: 'predikat_cluster' },
            { data: "info", name: 'info' },
            { data: "action", name: 'action' },
        ]
    });
}

$('.filter').on('change', function () {
    var filterJobFamily = $('#jobFamily').val(),
        filtertargetPositions = $('#targetPosition').val(),
        filterSubholding = $('#subholding').val(),
        filterGrade = $('#grade').val(),
        filterTalentCluster = $('#talentCluster').val(),
        filterCalibrate = $('#isCalibrate').val(),
        filterAreaNomenklatur = $('#areaNomenklatur').val();

    $('#tableEmployee').DataTable().destroy();
    $('#tableEmployee tbody').empty();
    fillDataTable(filterJobFamily, filterGrade, filterSubholding, filterTalentCluster, filterCalibrate, filtertargetPositions, filterAreaNomenklatur);
});


// * Function untuk filter employee data view
function employeeDataFilter(data) {
    let emptyString = "",
        emptyNull = null;

    if (data == emptyString || data == emptyNull) {
        data = "-";
    }

    return data;
}

// * Function untuk option ajax
function optionFilter(type, selector, filter = null) {
    $.ajax({
        dataType: 'json',
        method: 'post',
        data: {
            type: type,
            filter: filter,
            _token: csrfToken
        },
        url: routeFilterOption,
        success: function (resp) {
            let html = `<option></option>`;
            $.each(resp.data, function (i, val) {
                html += `<option value="${val.key}">${val.value}</option>`;
            });
            selector.html("");
            selector.html(html);
        },
        error: function (err) {

        }
    });
}

// * Event Listener on change subholding
$('#subholding').on('change', function () {
    $('#areaNomenklatur').html("");
    optionFilter('area', $('#areaNomenklatur'), $(this).val());
    optionFilter('subholding-position', $('#targetPosition'), $(this).val());
});

// * Event Listener on change position
$('#areaNomenklatur').on('change', function () {
    $('#targetPosition').html("");
    optionFilter('position', $('#targetPosition'), $(this).val());
});