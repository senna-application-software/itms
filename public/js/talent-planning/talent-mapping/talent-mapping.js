
var calibrasi = function() {
    // Private functions
    var _kalibrasi = function() {
        var kanban = new jKanban({
            element: '#callibration_panel',
            gutter: '30px',
            widthBoard: '333px',
            click: function(el) {
                alert(el.innerHTML);
            },
            boards: [
                {
                    'id': 'box4',
                    'title': 'Box 4',
                    'class': 'light-primary',
                    'item': [{
                        'title': '<span class="font-weight-bold">Nama Karyawan A</span>',
                        'class': 'light-dark',
                    },
                    {
                        'title': '<span class="font-weight-bold">Nama Karyawan B</span>',
                        'class': 'light-dark',
                    }]
                },
                {
                    'id': 'box7',
                    'title': 'Box 7',
                    'class': 'light-success',
                    'item': [{
                        'title': '<span class="font-weight-bold">Nama Karyawan F</span>',
                        'class': 'light-dark',
                    },
                    {
                        'title': '<span class="font-weight-bold">Nama Karyawan G</span>',
                        'class': 'light-dark',
                    },
                    {
                        'title': '<span class="font-weight-bold">Nama Karyawan C</span>',
                        'class': 'light-dark',
                    },
                    {
                        'title': '<span class="font-weight-bold">Nama Karyawan I</span>',
                        'class': 'light-dark',
                    }]
                },
                {
                    'id': 'box9',
                    'title': 'Box 9',
                    'class': 'success',
                    'item': []
                },
                {
                    'id': 'box2',
                    'title': 'Box 2',
                    'class': 'light-warning',
                    'item': []
                },
                {
                    'id': 'box5',
                    'title': 'Box 5',
                    'class': 'primary',
                    'item': []
                },
                {
                    'id': 'box8',
                    'title': 'Box 8',
                    'class': 'light-success',
                    'item': []
                },
                {
                    'id': 'box1',
                    'title': 'Box 1',
                    'class': 'danger',
                    'item': [
                    ]
                },
                {
                    'id': 'box3',
                    'title': 'Box 3',
                    'class': 'light-warning',
                    'item': []
                },
                {
                    'id': 'box6',
                    'title': 'Box 6',
                    'class': 'secondary',
                    'item': []
                }
            ]
        });

        var toDoButton = document.getElementById('addToDo');
            toDoButton.addEventListener('click', function() {
            kanban.addElement(
                '_todo', {
                    'title': `
                        <div class="d-flex align-items-center">
                            <div class="symbol symbol-light-primary mr-3">
                                <img alt="Pic" src="assets/media/users/300_14.jpg" />
                            </div>
                            <div class="d-flex flex-column align-items-start">
                                <span class="text-dark-50 font-weight-bold mb-1">Requirement Study</span>
                                <span class="label label-inline label-light-success font-weight-bold">Scheduled</span>
                            </div>
                        </div>
                    `
                }
            );
        });

        var addBoardDefault = document.getElementById('addDefault');
        addBoardDefault.addEventListener('click', function() {
            kanban.addBoards(
                [{
                    'id': '_default',
                    'title': 'New Board',
                    'class': 'primary-light',
                    'item': [{
                            'title': `
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-success mr-3">
                                        <img alt="Pic" src="assets/media/users/300_13.jpg" />
                                    </div>
                                    <div class="d-flex flex-column align-items-start">
                                        <span class="text-dark-50 font-weight-bold mb-1">Payment Modules</span>
                                        <span class="label label-inline label-light-primary font-weight-bold">In development</span>
                                    </div>
                                </div>
                        `},{
                            'title': `
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-success mr-3">
                                        <img alt="Pic" src="assets/media/users/300_12.jpg" />
                                    </div>
                                    <div class="d-flex flex-column align-items-start">
                                    <span class="text-dark-50 font-weight-bold mb-1">New Project</span>
                                    <span class="label label-inline label-light-danger font-weight-bold">Pending</span>
                                </div>
                            </div>
                        `}
                    ]
                }]
            )
        });

        var removeBoard = document.getElementById('removeBoard');
        removeBoard.addEventListener('click', function() {
            kanban.removeBoard('_done');
        });
    }

    var _kalibrashi = function() {
        var kanban = new jKanban({
            element: '#callibration_panel_0',
            gutter: '0px',
            widthBoard: '250px',
            click: function(el) {
                alert(el.innerHTML);
            },
            boards: [
                {
                    'id': 'box0',
                    'title': 'Box 0',
                    'class': 'light-dark',
                    'item': [{
                        'title': '<span class="font-weight-bold">Nama Karyawan A</span>',
                        'class': 'light-dark',
                    },
                    {
                        'title': '<span class="font-weight-bold">Nama Karyawan B</span>',
                        'class': 'light-dark',
                    }
                    ]
                }
            ]
        });
        var toDoButton = document.getElementById('addToDo');
            toDoButton.addEventListener('click', function() {
            kanban.addElement(
                '_todo', {
                    'title': `
                        <div class="d-flex align-items-center">
                            <div class="symbol symbol-light-primary mr-3">
                                <img alt="Pic" src="assets/media/users/300_14.jpg" />
                            </div>
                            <div class="d-flex flex-column align-items-start">
                                <span class="text-dark-50 font-weight-bold mb-1">Requirement Study</span>
                                <span class="label label-inline label-light-success font-weight-bold">Scheduled</span>
                            </div>
                        </div>
                    `
                }
            );
        });

        var addBoardDefault = document.getElementById('addDefault');
        addBoardDefault.addEventListener('click', function() {
            kanban.addBoards(
                [{
                    'id': '_default',
                    'title': 'New Board',
                    'class': 'primary-light',
                    'item': [{
                            'title': `
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-success mr-3">
                                        <img alt="Pic" src="assets/media/users/300_13.jpg" />
                                    </div>
                                    <div class="d-flex flex-column align-items-start">
                                        <span class="text-dark-50 font-weight-bold mb-1">Payment Modules</span>
                                        <span class="label label-inline label-light-primary font-weight-bold">In development</span>
                                    </div>
                                </div>
                        `},{
                            'title': `
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-success mr-3">
                                        <img alt="Pic" src="assets/media/users/300_12.jpg" />
                                    </div>
                                    <div class="d-flex flex-column align-items-start">
                                    <span class="text-dark-50 font-weight-bold mb-1">New Project</span>
                                    <span class="label label-inline label-light-danger font-weight-bold">Pending</span>
                                </div>
                            </div>
                        `}
                    ]
                }]
            )
        });

        var removeBoard = document.getElementById('removeBoard');
        removeBoard.addEventListener('click', function() {
            kanban.removeBoard('_done');
        });
    }

    // Public functions
    return {
        init: function() {
            _kalibrasi();
            _kalibrashi();
        }
    };
}();

jQuery(document).ready(function() {
    calibrasi.init();
});