$(function () {
    $('#directorate').val("");
    $('#division').val("");
    $('#group').val("");
    $('#position').val("");
});

// * event on change ini untuk mengisi type hidden nama - nama directorate, division dan lainnya.
$('#codeDirectorate').on('change', function (e) {
    let option = $('#codeDirectorate option:selected').text();
    $('#directorate').val(option);
});

$('#codeDivision').on('change', function (e) {
    let option = $('#codeDivision option:selected').text();
    $('#division').val(option);
});

$('#codeGroup').on('change', function (e) {
    let option = $('#codeGroup option:selected').text();
    $('#group').val(option);
});

$('#codePosition').on('change', function (e) {
    let option = $('#codePosition option:selected').text();
    $('#position').val(option);
});
// * End event on change ini untuk mengisi type hidden nama - nama directorate, division dan lainnya.