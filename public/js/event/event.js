var arrows = {
    leftArrow: '<i class="la la-angle-left"></i>',
    rightArrow: '<i class="la la-angle-right"></i>'
}
// enable clear button for modal demo
$('#kt_datepicker_3_modal,#kt_datepicker_4_modal,#kt_datepicker_3_edit,#kt_datepicker_4_edit').datepicker({
    rtl: KTUtil.isRTL(),
    todayBtn: "linked",
    clearBtn: true,
    todayHighlight: true,
    dateFormat: 'dd/mm/yyyy',
    templates: arrows,
    startDate: new Date()
});