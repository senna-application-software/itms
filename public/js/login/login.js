$("#icon-view-pwd").on("click", function(){
    let thisIcon = $(this).hasClass("fa-eye");
    if (thisIcon){
        $(this).removeClass("fa-eye");
        $(this).addClass("fa-eye-slash");
        $('#password').prop('type', 'text');
    } else {
        $(this).removeClass("fa-eye-slash");
        $(this).addClass("fa-eye");
        $('#password').prop('type', 'password');
    }
});