// * Init Document Ready Jquery
$(function () {
    fillDataTable();
    $('#jobFamily').select2({
        placeholder: "-Select Job Family-",
        allowClear: true
    });

    $('#subholding').select2({
        placeholder: "-Select Organization-",
        allowClear: true
    });

    $('#grade').select2({
        placeholder: "-Select Person Grade-",
        allowClear: true
    });

    $('#talentCluster').select2({
        placeholder: "-Select Talent Cluster-",
        allowClear: true
    });
});

// * Show modal view data employee
$('body').on('click', '.viewDataEmployee', function (e) {
    e.preventDefault();
    let dataTarget = e.target;

    routeViewEmployee = routeViewEmployee.replace(':id', dataTarget.getAttribute('data-id'));
    $.ajax({
        url: routeViewEmployee,
        dataType: 'json',
        success: function (resp) {
            let employee = resp.data,
                tableEmployee;

            tableEmployee = viewEmployee(employee);

            $('#dataEmployee').html(tableEmployee);
            $('#modalViewEmployee').modal('show');
        },
        error: function (err) {
            console.log("Error : ", err);
        }
    });

    routeViewEmployee = routeConstViewEmployee; // * Untuk reset route view
});

// * Delete data employee
$('body').on('click', '.deleteEmployee', function (e) {
    e.preventDefault();
    let dataTarget = e.target;

    Swal.fire({
        title: 'Are you sure ?',
        showCancelButton: true,
    }).then((result) => {
        routeDeleteEmployee = routeDeleteEmployee.replace(":id", dataTarget.getAttribute('data-id'));
        if (result.isConfirmed) {
            $.ajax({
                url: routeDeleteEmployee,
                dataType: 'json',
                method: 'delete',
                data: {
                    _token: csrfToken
                },
                success: function (resp) {
                    Swal.fire('Deleted!', '', 'success');
                    $('#tableEmployee').DataTable().ajax.reload();
                },
                error: function (err) {
                    console.log("Error : ", err);
                }
            });
        }
        routeDeleteEmployee = routeConstDeleteEmployee;
    });
});

// * Function untuk fill datatabel employee
var fillDataTable = (filterJobFamily = '', filterGrade = '', filterSubholding = '', filterTalentCluster = '') => {
    // * Handle server side table processing
    $('#tableEmployee').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: routeDataTable,
            data: {
                filterJobFamily: filterJobFamily,
                filterGrade: filterGrade,
                filterSubholding: filterSubholding,
                filterTalentCluster: filterTalentCluster
            }
        },
        columns: [
            { data: "DT_RowIndex", name: 'DT_RowIndex' },
            { data: "prev_persno", name: 'prev_persno' },
            { data: "personnel_number", name: 'personnel_number' },
            { data: "name_organization", name: 'sub_holding' },
            { data: "name_position", name: 'position_name' },
            { data: "name_area", name: 'area_nomenklatur' },
            { data: "person_grade_align", name: 'person_grade_align' },
            { data: "status_karyawan", name: 'status_karyawan' },
            { data: "predikat_cluster", name: 'predikat_cluster' },
            { data: "action", name: 'action' },
        ]
    });
}

$('.filter').on('change', function () {
    var filterJobFamily = $('#jobFamily').val(),
        filterSubholding = $('#subholding').val(),
        filterGrade = $('#grade').val(),
        filterTalentCluster = $('#talentCluster').val();

    $('#tableEmployee').DataTable().destroy();
    $('#tableEmployee tbody').empty();
    fillDataTable(filterJobFamily, filterGrade, filterSubholding, filterTalentCluster);
});

function padTo2Digits(num) {
    return num.toString().padStart(2, '0');
}

// * Function untuk handle view data employee
function viewEmployee(data) {
    let tableEmployee,
        tanggalLahir,
        tglLahirConvert,
        emptyString = "",
        emptyNull = null,
        emptyUndefined = undefined;

    if (data.date_of_birth != emptyNull) {
        tanggalLahir = new Date(data.date_of_birth);
        tglLahirConvert = [
            padTo2Digits(tanggalLahir.getDate()),
            padTo2Digits(tanggalLahir.getMonth() + 1),
            tanggalLahir.getFullYear(),
        ].join('-');
    } else {
        tglLahirConvert = '-';
    }

    tableEmployee = `<table class="table">
                <tr>
                    <td>Employee Name</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.personnel_number)}</td>
                </tr>

                <tr>
                    <td>Employee ID</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.prev_persno)}</td>
                </tr>

                <tr>
                    <td>Position Name</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.position_name)}</td>
                </tr>

                <tr>
                    <td>Job Title</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.job_title)}</td>
                </tr>

                <tr>
                    <td>Directorate</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.directorate)}</td>
                </tr>

                <tr>
                    <td>Directorate Code</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.directorate_code)}</td>
                </tr>

                <tr>
                    <td>Group</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.group)}</td>
                </tr>

                <tr>
                    <td>Group Code</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.group_code)}</td>
                </tr>

                <tr>
                    <td>Division</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.division)}</td>
                </tr>

                <tr>
                    <td>Division Code</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.division_code)}</td>
                </tr>

                <tr>
                    <td>Area Nomenklatur</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.area_nomenklatur)}</td>
                </tr>

                <tr>
                    <td>Gender</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.gender_text)}</td>
                </tr>

                <tr>
                    <td>Marst</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.marst)}</td>
                </tr>

                <tr>
                    <td>Birthplace</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.birthplace)}, ${tglLahirConvert}</td>
                </tr>

                <tr>
                    <td>Education Level</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.education_level)}</td>
                </tr>

                <tr>
                    <td>University Name</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.university_name)}</td>
                </tr>

                <tr>
                    <td>ID Card Number</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.no_ktp)}</td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.email)}</td>
                </tr>
                
                <tr>
                    <td>Phone Number</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.no_hp)}</td>
                </tr>

                <tr>
                    <td>Employee Type</td>
                    <td>:</td>
                    <td>${employeeDataFilter(data.status_karyawan)}</td>
                </tr>
            </table>`;

    return tableEmployee;
}

// * Function untuk filter employee data view
function employeeDataFilter(data) {
    let emptyString = "",
        emptyNull = null;

    if (data == emptyString || data == emptyNull) {
        data = "-";
    }

    return data;
}

// * Event Listener click modal add employee button
$('#btnModalAddEmployee').on('click', function (e) {
    let formEmployeeHtml,
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        };

    formEmployeeHtml = formAddEmployee();

    $('#modalAddEmployee .modal-body').html("");
    $('#modalAddEmployee .modal-body').append(formEmployeeHtml);

    supplyDataFormEmployee('position', $('#position'));
    supplyDataFormEmployee('job_title', $('#job_title'));
    supplyDataFormEmployee('directorate', $('#directorate_code'));
    supplyDataFormEmployee('group', $('#group_code'));
    supplyDataFormEmployee('division', $('#division_code'));
    supplyDataFormEmployee('gender_text', $('#gender_text'));
    supplyDataFormEmployee('religious_denomination', $('#religious_denomination'));
    supplyDataFormEmployee('marst', $('#marst'));
    supplyDataFormEmployee('education_level', $('#education_level'));
    supplyDataFormEmployee('line_manager', $('#line_manager'));
    supplyDataFormEmployee('status_aktif', $('#status_aktif'));
    supplyDataFormEmployee('status_karyawan', $('#status_karyawan'));
    supplyDataFormEmployee('person_grade', $('#person_grade'));
    supplyDataFormEmployee('jabfung', $('#jabfung'));
    supplyDataFormEmployee('subholding', $('#code_subholding'));

    $('.selectFormEmployee').select2({
        placeholder: "-Select-",
        allowClear: true
    });

    $('.dateFormEmployee').datepicker({
        rtl: KTUtil.isRTL(),
        clearBtn: true,
        templates: arrows
    });
});

// * Function untuk view form Employee add
function formAddEmployee() {
    let formEmployee;

    formEmployee = `
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Personnel Number</label>
                    <input type="text" name="personnel_number" class="form-control" id="personnel_number" required>
                </div>
        
                <div class="form-group">
                    <label>Prev Persno</label>
                    <input type="text" name="prev_persno" class="form-control" id="prev_persno" required>
                </div>
        
                <div class="form-group">
                    <label>Position</label>
                    <select name="position" class="form-control selectFormEmployee" id="position" required>
                        <option></option>
                    </select>
                </div>
        
                <div class="form-group">
                    <label>Job Title</label>
                    <select name="job_title" class="form-control selectFormEmployee" id="job_title">
                        <option></option>
                    </select>
                </div>
        
                <div class="form-group">
                    <label>Personnel Area Text</label>
                    <input type="text" name="personnel_area_text" class="form-control" id="personnel_area_text">
                </div>
                
                <div class="form-group">
                    <label>Area Nomenklatur</label>
                    <input type="text" name="area_nomenklatur" class="form-control" id="area_nomenklatur">
                </div>

                <div class="form-group">
                    <label>Gender Text</label>
                    <select name="gender_text" class="form-control selectFormEmployee" id="gender_text">
                        <option></option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Religious Denomination</label>
                    <select name="religious_denomination" class="form-control selectFormEmployee" id="religious_denomination">
                        <option></option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Marst</label>
                    <select name="marst" class="form-control selectFormEmployee" id="marst">
                        <option></option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Birth Place</label>
                    <input type="text" name="birthplace" class="form-control" id="birthplace">
                </div>

                <div class="form-group">
                    <label>Date of Birth</label>
                    <div class="input-group date">
                        <input type="text" name="date_of_birth" class="form-control dateFormEmployee" id="date_of_birth" data-date-format="dd-mm-yyyy" readonly>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-calendar"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Major Name</label>
                    <input type="text" name="major_name" class="form-control" id="major_name">
                </div>

                <div class="form-group">
                    <label>Education Level</label>
                    <select name="education_level" class="form-control selectFormEmployee" id="education_level">
                        <option></option>
                    </select>
                </div>
            </div>
                
            <div class="col-md-6">
                <div class="form-group">
                    <label>University Name</label>
                    <input type="text" name="university_name" class="form-control" id="university_name">
                </div>
                
                <div class="form-group">
                    <label>Date Started Work</label>
                    <div class="input-group date">
                        <input type="text" name="tanggal_mulai_bekerja" class="form-control dateFormEmployee" id="tanggal_mulai_bekerja" data-date-format="dd-mm-yyyy" readonly>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-calendar"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Line Manager</label>
                    <select name="line_manager" class="form-control selectFormEmployee" id="line_manager">
                        <option></option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Residence Card Number</label>
                    <input type="number" name="no_ktp" class="form-control" id="no_ktp">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" id="email">
                </div>

                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="number" name="no_hp" class="form-control" id="no_hp">
                </div>

                <div class="form-group">
                    <label>Status Active</label>
                    <select name="status_aktif" class="form-control selectFormEmployee" id="status_aktif">
                        <option></option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Employee Status</label>
                    <select name="status_karyawan" class="form-control selectFormEmployee" id="status_karyawan">
                        <option></option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Person Grade</label>
                    <select name="person_grade" class="form-control selectFormEmployee" id="person_grade">
                        <option></option>
                    </select>
                </div>

                <div class="form-group">
                    <label>BOD Type</label>
                    <input type="text" name="bod_type" class="form-control" id="bod_type">
                </div>

                <div class="form-group">
                    <label>Functional</label>
                    <select name="jabfung" class="form-control selectFormEmployee" id="jabfung">
                        <option></option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Home Address</label>
                    <textarea type="text" name="home_address" class="form-control" id="home_address"></textarea>
                </div>

                <div class="form-group">
                    <label>Tax ID Number</label>
                    <input type="number" name="no_npwp" class="form-control" id="no_npwp">
                </div>
            </div>
        </div>
    `;

    return formEmployee;
}

// * Function untuk supply data select2 employee form add
function supplyDataFormEmployee(type, selector) {
    $.ajax({
        method: "post",
        dataType: "json",
        data: {
            _token: csrfToken,
            type: type
        },
        url: routeSupplyDataFormEmployee,
        success: function (resp) {
            let html = `<option></option>`;
            $.each(resp.data.resp, function (i, val) {
                html += `<option value="${val.key}">${val.value}</option>`;
            });
            selector.html("");
            selector.html(html);
        },
        error: function (err) {
            console.log("Error : ", err);
        }
    })
}
