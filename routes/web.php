<?php

use Illuminate\Support\Facades\Route;

Route::get('/offline', function () {
    return view('vendor/laravelpwa/offline');
});

Route::fallback(function () {

    if (!\Sentinel::check()) {
        abort('401', 'Anda harus login terlebih dahulu');
    }

    $fallbackPlaceholder = Route::current()->fallbackPlaceholder;
    if (cekRoute($fallbackPlaceholder)) {
        abort('403', 'Metode tidak didukung');
    }

    abort('404', 'Halaman tidak ditemukan');
});

Route::middleware(['sentinel.auth'])->group(function () {
    Route::get('/', 'AuthenticationController@index')->name('auth.login');
    Route::post('/validate', 'AuthenticationController@login_validate')->name('auth.validate');
});

Route::get('/logout', 'AuthenticationController@logout')->name('auth.logout');

// Route::get('/logout', function () {
//     Sentinel::logout(null, true);
//     return redirect('/');
// });

Route::middleware(['guest', 'role:admin|employee|talent_committee|executive'])->group(function () {
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/role-switch', 'DashboardController@role_switch')->name('role_switch');
    Route::post('/role-switch-process', 'DashboardController@role_switch_process')->name('role_switch_process');
    Route::post('/vacant-table', 'DashboardController@get_vacant_table');
    Route::post('/chart-talent-mapping-boxed', 'DashboardController@get_chart_talent_mapping_box');
    Route::post('/chart-talent-mapping-bar', 'DashboardController@get_chart_talent_mapping_bar');
    Route::get('/data-eqs-detail-posisi', 'DashboardController@data_eqs_detail_posisi')->name('dashboard.data_eqs_detail_posisi');

    Route::group(['namespace' => 'Event'], function () {
        Route::group(['prefix' => '/event'], function () {
            Route::get('/', 'EventController@index');
            Route::get('/datatable', 'EventController@datatable')->name('event.datatable');
            Route::post('/create', 'EventController@create')->name('event.create');
            Route::post('/edit', 'EventController@edit')->name('event.edit');
            Route::get('/get-attribute/{id}', 'EventController@get_attribute')->name('event.get_attribute');
        });

        Route::group(['prefix' => '/sub-event'], function () {
            Route::get('/', 'SubEventController@index')->name('sub-event.index');
            Route::get('/history/{id}', 'SubEventController@history')->name('sub-event.history');
            Route::get('/datatable', 'SubEventController@datatable')->name('sub-event.datatable');
            Route::get('/get-attribute-position/{id_post}', 'SubEventController@get_attribute_position')->name('sub-event.get_attribute_position');
            Route::post('/create', 'SubEventController@create')->name('sub-event.create');
            Route::post('/filter-positions', 'SubEventController@filter_positions')->name('sub-event.filter_positions');
            Route::get('/detail-subevent/{id}', 'SubEventController@detail_subevent')->name('sub-event.detail_subevent');
            Route::get('/delete-subevent/{id}', 'SubEventController@hapus_event')->name('sub-event.delete_subevent');
            Route::post('/send-mail-integ', 'SubEventController@send_mail_pakta')->name('sub-event.send_mail_pakta');
            Route::post('/get-talent-com', 'SubEventController@get_talent_com')->name('sub-event.get_talent_com');
        });
    });

    //routes organisasi
    Route::group(['namespace' => 'Organization'], function () {
        Route::group(['prefix' => '/struktur-organisasi'], function () {
            Route::get('/', 'OrganizationController@index');
            Route::get('/detail', 'OrganizationController@show')->name('struktur-organisasi.show');
            Route::get('/datatable', 'OrganizationController@datatable')->name('struktur-organisasi.datatable');
            Route::get('/subholding', 'OrganizationController@subholding')->name('struktur-organisasi.subholding');
        });
    });

    Route::group(['namespace' => 'Calibration'], function () {
        Route::group(['prefix' => '/calibration'], function () {
            Route::get('/', 'CalibrationController@index')->name('calibration.index');
            Route::get('/detail/{id}', 'CalibrationController@detail')->name('calibration.detail');
            Route::get('/comparison', 'CalibrationController@comparison')->name('calibration.comparison');
            Route::get('/view-career-card/{nik}/{isPlain?}', 'CalibrationController@view_career_card');
            Route::post('/get-employee', 'CalibrationController@get_employee');
        });
    });

    Route::group(['namespace' => 'TalentPlanning'], function () {
        Route::group(['prefix' => '/talent-profiling'], function () {
            Route::get('/', 'TalentProfilingController@index')->name('talent-profiling.index');
            Route::get('/detail/{id}', 'TalentProfilingController@detail')->name('talent-profiling.detail');
            Route::get('/view-career-card/{id}', 'TalentProfilingController@view_career_card');
            Route::post('/get-talent-cluster', 'TalentProfilingController@get_talent_cluster');
            Route::post('/submit-assessment', 'TalentProfilingController@submit_assessment')->name('talent-profiling.submit_assessment');
            Route::post('/done-profiling', 'TalentProfilingController@store')->name('talent-profiling.done_profiling');
        });

        Route::group(['prefix' => '/talent-sourcing'], function () {
            Route::get('/', 'TalentSourcingController@index')->name('talent-sourcing.index');
            Route::get('/detail/{id}', 'TalentSourcingController@detail')->name('talent-sourcing.detail');
            Route::post('/done-sourcing', 'TalentSourcingController@store')->name('talent-sourcing.done_sourcing');
            Route::get('/berita-acara/{id}', 'TalentSourcingController@GenerateBeritaAcara')->name('talent-sourcing.pdf');
            Route::post('/select2Employee', 'TalentSourcingController@select2Employee')->name('talent-sourcing.select2Employee');
            Route::post('/addEmployeeSourcing', 'TalentSourcingController@addEmployeeSourcing')->name('talent-sourcing.addEmployeeSourcing');
            Route::delete('/deleteTalentAspiration/{id}', 'TalentSourcingController@deleteTalentAspiration')->name('talent-sourcing.deleteTalentAspiration');
            Route::get('/profile/{nik}', 'TalentSourcingController@profile')->name('talent-sourcing.profile');
            Route::post('/update_profile', 'TalentSourcingController@update_profile')->name('talent-sourcing.update_profile');
            Route::post('/update_interest', 'TalentSourcingController@update_interest')->name('talent-sourcing.update_interest');
            Route::post('/set_status', 'TalentSourcingController@set_status')->name('talent-sourcing.set_status');
        });

        Route::group(['prefix' => '/talent-mapping'], function () {
            Route::get('/', 'TalentMappingController@index')->name('talent-mapping.index');
            Route::get('/detail/{id}', 'TalentMappingController@detail')->name('talent-mapping.detail');
            Route::get('/comparison', 'TalentMappingController@comparison')->name('talent-mapping.comparison');
            Route::get('/view-career-card/{nik}/{isPlain?}', 'TalentMappingController@view_career_card');
            Route::post('/get-employee', 'TalentMappingController@get_employee');
            Route::post('/update-panel', 'TalentMappingController@update_panel');
            Route::post('/update-percent', 'TalentMappingController@update_percent');
            Route::post('/done-mapping', 'TalentMappingController@store')->name('talent-mapping.done_mapping');
            Route::get('/berita-acara/{id_sub_event}', 'TalentMappingController@berita_acara')->name('talent-mapping.berita-acara');
        });
    });

    Route::group(['namespace' => 'Calibration'], function () {
        Route::group(['prefix' => 'calibration'], function () {
            Route::get('/', 'CalibrationController@history')->name('talent-calibration.history');
        });
    });

    Route::group(['namespace' => 'TalentDay'], function () {
        Route::group(['prefix' => '/talent-day'], function () {
            Route::get('/', 'TalentDayController@index')->name('talent-day.index');
            Route::get('/detail/{id}', 'TalentDayController@detail')->name('talent-day.detail');
            Route::get('/detail-assessment', 'TalentDayController@detail_assessment_index')->name('talent-day.detail-assessment');
            Route::post('/detail-assessment/update', 'TalentDayController@detail_assessment_update')->name('talent-day.detail-assessment.update');
            Route::post('/detail-assessment/save', 'TalentDayController@save')->name('talent-day.detail_assessment.save');
            Route::post('/done-assessment', 'TalentDayController@store')->name('talent-day.done-assessment');
            Route::get('/berita-acara/{id_sub_event}', 'TalentDayController@berita_acara')->name('talent-day.berita-acara');
        });
    });

    // Notification Route
    Route::group(['namespace' => 'PendingTask'], function () {
        Route::group(['prefix' => '/pending-task'], function () {
            Route::get('/', 'PendingTaskController@index')->name('pending_task.index');
            Route::get('/position', 'PendingTaskController@dataNotifPosition')->name('pending_task.data.position');
            Route::get('/employee', 'PendingTaskController@dataNotifEmployee')->name('pending_task.data.employee');
            Route::get('/bagde', 'PendingTaskController@badge')->name('pending_task.badge');
            Route::get('/add/position', 'PendingTaskController@addPosition')->name('pending_task.add.position');
            Route::get('/add/employee', 'PendingTaskController@addEmployee')->name('pending_task.add.employee');
            Route::post('/store/position', 'PendingTaskController@storePosition')->name('pending_task.store.position');
            Route::post('/store/employee', 'PendingTaskController@storeEmployee')->name('pending_task.store.employee');
        });
    });

    // Employee routes
    Route::group(['namespace' => 'Aspirations'], function () {
        Route::group(['prefix' => '/aspirations'], function () {
            Route::get('/{page}', 'AspirationsController@index')->name('talent-planning');
            Route::post('/generate-target-positions', 'AspirationsController@generate_target_positions')->name('generate_target_positions');
            Route::post('/gap-competency', 'AspirationsController@gap_competency')->name('gap_competency');
            Route::get('/modal-eqs-dasar/{nik}', 'AspirationsController@modal_eqs_dasar')->name('modal-eqs-dasar');
            Route::post('/individual', 'AspirationsController@insert_individual')->name('insert-planning-individual');
            Route::post('/supervisor', 'AspirationsController@insert_supervisor')->name('insert-planning-supervisor');
            Route::post('/job_holder', 'AspirationsController@insert_job_holder')->name('insert-planning-job_holder');
            Route::post('/unit', 'AspirationsController@insert_unit')->name('insert-planning-unit');
        });
    });

    // talent review
    Route::group(['namespace' => 'TalentReview'], function () {
        Route::group(['prefix' => '/talent-review'], function () {
            Route::get('/', 'TalentReviewController@index')->name('talent-review.index');
            Route::get('/detail/{id}', 'TalentReviewController@detail')->name('talent-review.detail');
            Route::get('/view-career-card/{id}', 'TalentReviewController@view_career_card');
            Route::post('/submit-assessment', 'TalentReviewController@submit_assessment')->name('talent-review.submit_assessment');
            Route::post('/done-review', 'TalentReviewController@store')->name('talent-review.done_review');
            Route::get('/berita-acara/{id}', 'TalentReviewController@GenerateBeritaAcara')->name('talent-review.pdf');
        });
    });

    // talent selection
    Route::group(['namespace' => 'TalentSelection'], function () {
        Route::group(['prefix' => '/talent-selection'], function () {
            Route::get('/', 'TalentSelectionController@index')->name('talent-selection.index');
            Route::get('/detail/{id}', 'TalentSelectionController@detail')->name('talent-selection.detail');
            Route::get('/view-career-card/{id}', 'TalentSelectionController@view_career_card');
            Route::post('/submit-assessment', 'TalentSelectionController@submit_assessment')->name('talent-selection.submit_assessment');
            Route::post('/done-selection', 'TalentSelectionController@store')->name('talent-selection.done_selection');
            Route::post('/done-comparation', 'TalentSelectionController@done_comparation')->name('talent-selection.done_comparation');
            Route::get('/berita-acara/{id}', 'TalentSelectionController@GenerateBeritaAcara')->name('talent-selection.pdf');
            Route::get('/flagging', 'TalentSelectionController@addFlagging')->name('talent_selection.add.flagging');
            Route::get('/add-event-position/{nik}', 'TalentSelectionController@addEventPosition')->name('talent_selection.add.event');
            Route::post('/store-event-position', 'TalentSelectionController@storeEventPosition')->name('talent_selection.store.event');
        });
    });

    Route::group(['namespace' => 'MyProfile'], function () {
        Route::group(['prefix' => '/my-profile'], function () {
            Route::get('/', 'MyProfileController@index')->name("my-profile");
            Route::post('/update-profile', 'MyProfileController@update_profile')->name('my-profile.update_profile');
            Route::post('/get-keahlian', 'MyProfileController@get_keahlian')->name('my-profile.get_keahlian');
            Route::post('/update-checkbox', 'MyProfileController@update_checkbox')->name('my-profile.update_checkbox');
            Route::post('/init-riwayat-two', 'MyProfileController@init_riwayat_two')->name('my-profile.init_riwayat_two');
            Route::post('/update-riwayat-two', 'MyProfileController@update_riwayat_two')->name('my-profile.update_riwayat_two');
            Route::post('/init-organization', 'MyProfileController@init_organization')->name('my-profile.init_organization');
            Route::post('/update-organization', 'MyProfileController@update_organization')->name('my-profile.update_organization');
            Route::post('/init-reward', 'MyProfileController@init_reward')->name('my-profile.init_reward');
            Route::post('/update-reward', 'MyProfileController@update_reward')->name('my-profile.update_reward');
            Route::post('/update-pendidikan', 'MyProfileController@update_pendidikan')->name('my-profile.update_pendidikan');
            Route::post('/init-diklat', 'MyProfileController@init_diklat')->name('my-profile.init_diklat');
            Route::post('/update_diklat', 'MyProfileController@update_diklat')->name('my-profile.update_diklat');
            Route::post('/init-karya', 'MyProfileController@init_karya')->name('my-profile.init_karya');
            Route::post('/update-karya', 'MyProfileController@update_karya')->name('my-profile.update_karya');
            Route::post('/init-exp-acara', 'MyProfileController@init_exp_acara')->name('my-profile.init_exp_acara');
            Route::post('/update-exp-acara', 'MyProfileController@update_exp_acara')->name('my-profile.update_exp_acara');
            Route::post('/init-refs', 'MyProfileController@init_reference')->name('my-profile.init_reference');
            Route::post('/update_refs', 'MyProfileController@update_reference')->name('my-profile.update_reference');
            Route::post('/init-pasangan', 'MyProfileController@init_pasangan')->name('my-profile.init_pasangan');
            Route::post('/update-pasangan', 'MyProfileController@update_pasangan')->name('my-profile.update_pasangan');
            Route::post('/init-children', 'MyProfileController@init_children')->name('my-profile.init_children');
            Route::post('/update-children', 'MyProfileController@update_children')->name('my-profile.update_children');
            Route::post('/init-pengalaman', 'MyProfileController@init_pengalaman')->name('my-profile.init_pengalaman');
            Route::post('/update-pengalaman', 'MyProfileController@update_pengalaman')->name('my-profile.update_pengalaman');
            Route::post('/update_interest', 'MyProfileController@update_interest')->name('my-profile.update_interest');
            Route::post('/update-pengalaman-bumn', 'MyProfileController@update_pengalaman_bumn')->name('my-profile.update_pengalaman_bumn');
            Route::post('/delete', 'MyProfileController@deletethis')->name('my-profile.delete');
            Route::post('/init-pendidikan', 'MyProfileController@init_pendidikan')->name('my-profile.init_pendidikan');
            Route::post('/get-employees-kelas', 'MyProfileController@get_employee_kelas')->name('my-profile.get_employee_kelas');
            Route::post('/get-employee-cluster', 'MyProfileController@get_employee_cluster')->name('my-profile.get_employee_cluster');
            Route::post('/get-employee-fnc-jbtn', 'MyProfileController@get_employee_fnc_jbtn')->name('my-profile.get_employee_fnc_jbtn');
            Route::post('/get-jabatan', 'MyProfileController@get_jabatan')->name('my-profile.get_jabatan');
            Route::post('/update-riwayat-jabatan', 'MyProfileController@update_riwayat_jabatan')->name('my-profile.update_riwayat_jabatan');
            Route::post('/init-riwayat-one', 'MyProfileController@init_riwayat_one')->name('my-profile.init_riwayat_one');
            Route::post('/set-status', 'MyProfileController@set_status')->name('my-profile.set_status');
        });
    });

    Route::group(['prefix' => '/masterData'], function () {
        Route::group(['namespace' => 'MasterData'], function () {
            // * Route Master data employee
            Route::resource('/masterDataEmployee', 'MasterDataEmployeeController');
            Route::get('/dataTableEmployee', 'MasterDataEmployeeController@dataTableEmployee')->name('masterDataEmployee.dataTableEmployee');
            Route::post('/supplyDataFormEmployee', 'MasterDataEmployeeController@supplyDataFormEmployee')->name('masterDataEmployee.supplyDataFormEmployee');
            Route::post('/importEmployee', 'MasterDataEmployeeController@importEmployee')->name('masterDataEmployee.importEmployee');
            Route::get('/templateImportEmployee', 'MasterDataEmployeeController@templateImportEmployee')->name('masterDataEmployee.templateImportEmployee');
        });
    });

    Route::group(['prefix' => '/SearchEmployee'], function () {
        Route::group(['namespace' => 'SearchEmployee'], function () {
            // * Route Master data employee
            Route::resource('/SearchDataEmployee', 'SearchEmployeeController');
            Route::get('/dataTableEmployee', 'SearchEmployeeController@dataTableEmployee')->name('SearchDataEmployee.dataTableEmployee');
            Route::get('/profile/{nik}', 'SearchEmployeeController@profile')->name('SearchDataEmployee.profile');

        });
    });
    Route::group(['prefix' => '/Histori'], function () {
        Route::group(['namespace' => 'HistoriBeritaAcara'], function () {
            // * Route Master data employee
            Route::resource('/BeritaAcara', 'HistoriBeritaAcaraController');
            Route::get('/dataTableHistoriBeritaAcara', 'HistoriBeritaAcaraController@dataTableBeritaAcara')->name('BeritaAcara.dataTableBeritaAcara');
            Route::get('/downloadberitaacara/{id}', 'HistoriBeritaAcaraController@downloadberitaacara')->name('BeritaAcara.downloadberitaacara');

        });
    });

    Route::group(['namespace' => 'ToolSettings'], function() {
        Route::group(['prefix' => '/settings'], function() {
            Route::get('/talent_poll', 'ToolSettingsController@talent_poll')->name('settings.talent_pool');
            Route::get('/table_talent_poll', 'ToolSettingsController@table_talent_poll')->name('settings.table_talent_poll');
            Route::post('/filterOption', 'ToolSettingsController@filterOption')->name('settings.filterOption');
            Route::get('/list-aspirasi-employee/{nik}', 'ToolSettingsController@listAspirasiEmployee')->name('settings.list_aspirasi_employee');
            Route::get('/data-list-aspirasi-employee', 'ToolSettingsController@dataListAspirasiEmployee')->name('settings.data_list_aspirasi_employee');
        });
    });

    // kebutuhan Dashboard Executive
    Route::get('/top_desired_position/chart', 'DashboardController@top_desired_position_chart')->name('top_desired_position.chart');
    Route::get('/top_desired_position/datatable', 'DashboardController@top_desired_position_datatable')->name('top_desired_position.datatable');
    Route::get('/organization_diagram', 'DashboardController@organization_diagram')->name('organization_diagram');
    Route::post('/employee_demograph', 'DashboardController@employee_demograph')->name('employee_demograph');
    Route::post('/employee_education', 'DashboardController@employee_education')->name('employee_education');
    Route::post('/positions_stats', 'DashboardController@positions_stats')->name('positions_stats');
    Route::post('/female_talent', 'DashboardController@female_talent')->name('female_talent');
    Route::post('/milenial_talent', 'DashboardController@milenial_talent')->name('milenial_talent');
    Route::post('/directorat_subholding', 'DashboardController@directorat_subholding')->name('directorat_subholding');
    Route::get('/employee_external', 'DashboardController@employee_external')->name('employee_external');
    Route::post('/insert_external', 'DashboardController@insert_external')->name('insert_external');
    Route::post('/filter_positions_raw', 'DashboardController@filter_positions')->name('filter_positions');

    // Successor
    Route::group(['namespace' => 'Successor'], function () {
        Route::group(['prefix' => '/successor-list'], function () {
            Route::get('/', 'SuccessorController@index');
            Route::get('/detail', 'SuccessorController@detail');
            Route::get('/cetak', 'SuccessorController@cetak');
            Route::get('/datatable', 'SuccessorController@datatable')->name('successor.datatable');
        });
    });
});

// Demo routes
Route::get('/demo', 'PagesController@index');
Route::get('/datatables', 'PagesController@datatables');
Route::get('/ktdatatables', 'PagesController@ktDatatables');
Route::get('/select2', 'PagesController@select2');
Route::get('/jquerymask', 'PagesController@jQueryMask');
Route::get('/icons/custom-icons', 'PagesController@customIcons');
Route::get('/icons/flaticon', 'PagesController@flaticon');
Route::get('/icons/fontawesome', 'PagesController@fontawesome');
Route::get('/icons/lineawesome', 'PagesController@lineawesome');
Route::get('/icons/socicons', 'PagesController@socicons');
Route::get('/icons/svg', 'PagesController@svg');

// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');

Route::get('my-channel', function () {
    $options = array(
        'cluster' => 'ap1',
        'useTLS'  => true,
    );
    $pusher = new Pusher\Pusher(
        'bd6afc5c94fd97aeab37',
        '21a4f2fc7e2b2514fd60',
        '1345851',
        $options
    );

    $data['message'] = 'test Real time notification.... (xna)';
    $pusher->trigger('my-pusher-channel', 'my-event', $data);
    return redirect("/");
});

Route::group(['prefix' => '/test'], function () {
    Route::group(['namespace' => 'XNA'], function () {
        Route::get('/test-observer', 'XNATestController@observer')->name('test.test-observer');
        Route::get('/test-pusher', 'XNATestController@pusher')->name('test.test-pusher');
        Route::get('/test-event-listener', 'XNATestController@event_and_listener')->name('test.test-event-listener');
        Route::get('/test-event-notif', 'XNATestController@notification_via_email_and_bell')->name('test.test-event-notif');
        Route::get('/test-event-willy', 'XNATestController@notification_willy')->name('test.test-event-willy');
        Route::get('/test-get-notif', 'XNATestController@notification')->name('test.test-get-notif');
        Route::get('/register', 'XNATestController@register')->name('test.register');
        Route::get('/scandir', 'XNATestController@scandir_image')->name('test.image');
        Route::get('/rule_aspiration', 'XNATestController@rule_aspiration')->name('test.rule_aspiration');
        Route::get('/gender_employee', 'XNATestController@gender_employee')->name('test.gender_employee');
        Route::get('/cluster_employee', 'XNATestController@cluster_employee')->name('test.cluster_employee');
    });
});
