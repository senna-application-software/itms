{{-- Extends layout --}}
@extends('layout.default')

{{-- Content Section --}}
@section('page_toolbar')
    {{-- kalo di subheader mau ditambahkan button action taruh di sini --}}
@endsection

@section('modal')
@endsection

@section('content')
    <div class="row" data-sticky-container>
        <div class="col-lg-4 col-xl-3">
            <div class="card card-custom " data-margin-top="120px" data-sticky-for="1023">
                <div class="card-body p-0">
                    <ul class="nav nav-bold nav-hover my-5" role="tablist">
                        <li class="nav-item w-100">
                            <a class="nav-link active" data-toggle="tab" href="#personal_info">
                                KETERANGAN PERORANGAN
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#all_panel">
                                INTEREST
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#keahlian_tab">
                                KEAHLIAN
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#riwayat_jabatan_tab">
                                RIWAYAT JABATAN
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#keanggotaan_tab">
                                KEANGGOTAAN ORGANISASI PROFESI/KOMUNITAS YANG DIIKUTI
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#penghargaan_tab">
                                PENGHARGAAN
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#pendidikan_tab">
                                RIWAYAT PENDIDIKAN DAN PELATIHAN
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#karya_tab">
                                KARYA TULIS ILMIAH
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#pengalaman_nara_tab">
                                PENGALAMAN SEBAGAI PEMBICARA/NARASUMBER/JURI
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#referensi_tab">
                                REFERENSI
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#keterangan_keluarga_tab">
                                KETERANGAN KELUARGA
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#pengalaman_keahlian_tab">
                                PENGALAMAN DAN KEAHLIAN
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#pengalaman_karir_tab">
                                PENGALAMAN KARIR KELAS, KLASTER, dan JABATAN di BUMN
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#aspirasi_bumn_tab">
                                ASPIRASI PADA KELAS, KLASTER, dan JABATAN di BUMN
                            </a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" data-toggle="tab" href="#dokumen_kepegawaian_tab">
                                DOKUMEN KEPEGAWAIAN
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-xl-9">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="personal_info" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-body d-flex flex-column">
                            <form action="{{ route('my-profile.update_profile') }}" id="updPrfFrm" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="row pt-6">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Nama Lengkap </label>
                                            <input type="text" name="full_name" class="form-control"
                                                value="{{ Sentinel::getUser()->employee->personnel_number }}">
                                            <span class="form-text text-muted">* Sesuai KTP, Menggunakan Huruf
                                                Kapital</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Gelar Akademik</label>
                                            <input type="text" class="form-control"
                                                value="{{ $pendidikan[0]->level_education ?? '-' }} {{ $pendidikan[0]->major_name ?? '-' }}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">NIK</label>
                                            <input type="text" name="nik" class="form-control"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                value="{{ Sentinel::getUser()->employee->no_ktp }}" required>
                                            <span class="form-text text-muted">* Diisi Dengan Nomor KTP, 16 Digit
                                                Angka</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">NPWP</label>
                                            <input type="text" name="npwp" class="form-control"
                                                value="{{ Sentinel::getUser()->employee->no_npwp }}" minlength="15"
                                                maxlength="15"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                required>
                                            <span class="form-text text-muted">* Diisi Dengan 15 Digit Angka Tanpa
                                                Tanda
                                                Baca</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <div class="radio-inline">
                                                <label class="radio">
                                                    <input type="radio" name="jenis_kelamin"
                                                        @if (Sentinel::getUser()->employee->gender_text == 'Male') checked="checked" @endif />
                                                    <span></span>
                                                    Laki - Laki
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" name="jenis_kelamin"
                                                        @if (Sentinel::getUser()->employee->gender_text == 'Female') checked="checked" @endif />
                                                    <span></span>
                                                    Perempuan
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Tempat Lahir</label>
                                                    <input type="text" name="birth_place" class="form-control"
                                                        value="{{ Sentinel::getUser()->employee->birthplace }}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Tanggal Lahir</label>
                                                    <input type="date" name="birth_date" class="form-control"
                                                        value="{{ Sentinel::getUser()->employee->date_of_birth }}"
                                                        required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Agama</label>
                                            <select name="religion" class="form-control" required>
                                                <option value="" disabled @if (strtolower(Sentinel::getUser()->employee->religious_denomination) == '') selected @endif>
                                                </option>
                                                <option value="islam" @if (strtolower(Sentinel::getUser()->employee->religious_denomination) == 'islam') selected @endif>
                                                    Islam
                                                </option>
                                                <option value="kristen" @if (strtolower(Sentinel::getUser()->employee->religious_denomination) == 'kristen') selected @endif>
                                                    Kristen
                                                </option>
                                                <option value="katolik" @if (strtolower(Sentinel::getUser()->employee->religious_denomination) == 'katolik') selected @endif>
                                                    Katolik
                                                </option>
                                                <option value="hindu" @if (strtolower(Sentinel::getUser()->employee->religious_denomination) == 'hindu') selected @endif>
                                                    Hindu
                                                </option>
                                                <option value="budha" @if (strtolower(Sentinel::getUser()->employee->religious_denomination) == 'budha') selected @endif>
                                                    Buddha
                                                </option>
                                                <option value="konghucu" @if (strtolower(Sentinel::getUser()->employee->religious_denomination) == 'konghucu') selected @endif>
                                                    Konghucu
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Nomor Handphone</label>
                                            <input type="text" name="hp" class="form-control"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                value="{{ Sentinel::getUser()->employee->no_hp }}" required>
                                            <span class="form-text text-muted">* Harus diisi</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" name="email" class="form-control"
                                                value="{{ Sentinel::getUser()->employee->email }}" required>
                                            <span class="form-text text-muted">* Harus diisi</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Foto Setengah Badan 4x6 (Max. 512Kb)</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="inputGroupFile01"
                                                        aria-describedby="inputGroupFileAddon01" name="profile_avatar"
                                                        accept="image/*">
                                                    <label class="custom-file-label" for="inputGroupFile01"></label>
                                                </div>
                                            </div>
                                            <span class="form-text text-muted">* .png, .jpg, .jpeg</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Alamat Rumah</label>
                                            <textarea name="alamat_rumah" class="form-control" id="" rows="10"
                                                required>{{ Sentinel::getUser()->employee->home_address }}</textarea>
                                            <span class="form-text text-muted">* Harus diisi</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Alamat Social Media</label>
                                            <textarea name="sosmed_addr" rows="10"
                                                class="form-control">{{ Sentinel::getUser()->employee->sosmed_address }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="all_panel" role="tabpanel">
                    <div class="card card-custom">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">INTEREST
                                    <span class="text-muted font-size-sm">
                                        <br>
                                        Sesuai dengan pengalaman dan/atau pendidikan
                                    </span>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="interest" rows="10">{{ Sentinel::getUser()->employee->interest }}</textarea>
                                </div>
                                <div class="col-md-12 mt-5 text-right">
                                    <button type="submit" class="btn btn-primary" onclick="saveInterest()">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="keahlian_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Keahlian
                                    <span class="text-muted font-size-sm">
                                        <br>
                                        (Sesuai dengan pengalaman dan/atau pendidikan)
                                    </span>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body d-flex flex-column">
                            <form action="{{ route('my-profile.update_checkbox') }}" class="form"
                                method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row text-center">
                                                @foreach ($keahlian as $data)
                                                    <div class="col-md-3 py-3">
                                                        @if (str_contains($data->nama_keahlian, 'Lainnya'))
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <label class="checkbox">
                                                                        <input type="checkbox" name="keahlian[]"
                                                                            value="{{ $data->id }}"
                                                                            onclick="frmLain($(this))" />
                                                                        <span class="mr-5"></span>
                                                                        {{ $data->nama_keahlian }}
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-12 mt-2">
                                                                    <div class="form-group">
                                                                        <input type="text" name="keahlian_lain"
                                                                            class="form-control" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="keahlian[]"
                                                                    value="{{ $data->id }}" />
                                                                <span class="mr-5"></span>
                                                                {{ $data->nama_keahlian }}
                                                            </label>
                                                        @endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="riwayat_jabatan_tab" role="tabpanel">
                    <div class="col-xl-12 col-md-12 col-sm-12 mb-6">
                        <div class="card card-custom card-stretch">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">Riwayat Jabatan</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <h6>A. Jabatan/Pekerjaan yang Pernah/Sedang Diemban</h6>
                                <button class="btn btn-primary" id="addHistoryOne" type="button">Add Data</button>
                                <table class="table" id="history_one_tbl">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;">No</th>
                                            <th style="width: 15%;">Jabatan</th>
                                            <th style="width: 25%;">Uraian Singkat Tugas Dan Kewenangan</th>
                                            <th style="width: 10%;">Dari</th>
                                            <th style="width: 10%;">Hingga</th>
                                            <th style="width: 25%;">Achievement</th>
                                            <th style="width: 10%;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($history_jabatan as $data)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    <a href="javascript:void(0)" class="hist-jbtn-editable"
                                                        data-pk="{{ $data->id }}" style="word-break: break-all">
                                                        {{ $data->position_name }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="uraian-editable"
                                                        data-pk="{{ $data->id }}" style="word-break: break-all">
                                                        {{ $data->uraian_singkat }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="st-date-hst-jbtn-editable"
                                                        data-pk="{{ $data->id }}">
                                                        {{ $data->start_date }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="en-date-hst-jbtn-editable"
                                                        data-pk="{{ $data->id }}">
                                                        {{ $data->end_date }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="achievement-editable"
                                                        data-pk="{{ $data->id }}" style="word-break: break-all">
                                                        {{ $data->achievement }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger"
                                                        onclick="deletethis('rwyt-jbtn', '{{ $data->id }}');$(this).parent().parent().remove();">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <hr class="w-100">
                                <h6 class="mt-2">B. Penugasan yang berkaitan dengan Jabatan Direksi/Dewan
                                    Komisaris/Dewan Pengawas <span class="text-danger">(bila ada)</span>
                                </h6>
                                <button class="btn btn-primary" id="addHistoryTwo" type="button">Add Data</button>
                                <table class="table" id="history_two_tbl">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;">No</th>
                                            <th style="width: 15%;">Penugasan</th>
                                            <th style="width: 25%;">Tupoksi</th>
                                            <th style="width: 10%;">Dari</th>
                                            <th style="width: 10%;">Hingga</th>
                                            <th style="width: 25%;">Instansi / Perusahaan</th>
                                            <th style="width: 10%;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($penugasan as $data)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    <a href="javascript:void(0)" class="penugasan-editable"
                                                        data-pk="{{ $data->id }}" style="word-break: break-all">
                                                        {{ $data->penugasan }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="tupoksi-editable"
                                                        data-pk="{{ $data->id }}" style="word-break: break-all">
                                                        {{ $data->tupoksi }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="start-date-penugasan-editable"
                                                        data-pk="{{ $data->id }}">
                                                        {{ $data->start_date }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="end-date-penugasan-editable"
                                                        data-pk="{{ $data->id }}">
                                                        {{ $data->end_date }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="instansi-two-editable"
                                                        data-pk="{{ $data->id }}" style="word-break: break-all">
                                                        {{ $data->instansi_perusahaan }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger"
                                                        onclick="$('#addHistoryTwo').removeAttr('disabled');deletethis('rwyt-pngsn', '{{ $data->id }}');$(this).parent().parent().remove();">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <hr class="w-100">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button type="button" class="btn btn-primary text-right"
                                            onclick="set_status('rwyt-jbtn')">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="keanggotaan_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header pt-5">
                            <div class="card-title">
                                <h3 class="card-label">KEANGGOTAAN ORGANISASI PROFESI / KOMUNITAS YANG DIIKUTI
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="mt-2">A. Kegiatan / Organisasi yang Pernah/Sedang Diikuti
                                <br><span class="text-danger">
                                    (yang terkait dengan pekerjaan / profesional)
                                </span>
                            </h6>
                            <button class="btn btn-primary" id="addOrgFormal" type="button">Add Data</button>
                            <table class="table" id="org_formal_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 25%;">Nama Kegiatan / Organisasi</th>
                                        <th style="width: 15%;">Jabatan</th>
                                        <th style="width: 10%;">Dari</th>
                                        <th style="width: 10%;">Hingga</th>
                                        <th style="width: 25%;">Uraian Singkat Kegiatan / Organisasi</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($organisasi_formal as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="name-keg-org-formal-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->nama_keg_org }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="jabatan-keg-org-formal-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->jabatan }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="start-date-keg-org-formal-editable"
                                                    data-pk="{{ $data->id }}">
                                                    {{ $data->start_date }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="end-date-keg-org-formal-editable"
                                                    data-pk="{{ $data->id }}">
                                                    {{ $data->end_date }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="uraian-keg-org-formal-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->uraian_singkat }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="$('#addOrgFormal').removeAttr('disabled');deletethis('frml-org', '{{ $data->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <hr class="w-100">
                            <h6 class="mt-2">
                                B. Kegiatan/Organisasi yang Pernah/Sedang Diikuti
                                <span class="text-danger">(non formal)</span>
                            </h6>
                            <button class="btn btn-primary" id="addOrgNonFormal" type="button">Add Data</button>
                            <table class="table" id="org_nonformal_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 25%;">Nama Kegiatan / Organisasi</th>
                                        <th style="width: 15%;">Jabatan</th>
                                        <th style="width: 10%;">Dari</th>
                                        <th style="width: 10%;">Hingga</th>
                                        <th style="width: 25%;">Uraian Singkat Kegiatan / Organisasi</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($organisasi_nonformal as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="name-keg-org-nonformal-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->nama_keg_org }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="jabatan-keg-org-nonformal-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->jabatan }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="start-date-keg-org-nonformal-editable"
                                                    data-pk="{{ $data->id }}">
                                                    {{ $data->start_date }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="end-date-keg-org-nonformal-editable"
                                                    data-pk="{{ $data->id }}">
                                                    {{ $data->end_date }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="uraian-keg-org-nonformal-editable"
                                                    data-pk="{{ $data->id }}">
                                                    {{ $data->uraian_singkat }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="$('#addOrgNonFormal').removeAttr('disabled');deletethis('frml-org', '{{ $data->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary text-right"
                                        onclick="set_status('keangg-org')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="penghargaan_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header pt-5">
                            <div class="card-title">
                                <h3 class="card-label">PENGHARGAAN
                                    <span class="text-muted font-size-sm"><br>Internal & Eksternal</span>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <button class="btn btn-primary" id="addPenghargaan" type="button">Add Data</button>
                            <table class="table" id="penghargaan_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 30%;">Jenis Penghargaan</th>
                                        <th style="width: 15%;">Tingkat</th>
                                        <th style="width: 30%;">Diberikan Oleh</th>
                                        <th style="width: 10%;">Tahun</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($penghargaan as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="jns-rwd-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->jenis_penghargaan }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="tingkat-rwd-editable"
                                                    data-pk="{{ $data->id }}" data-value="{{ $data->tingkat }}">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="diberikan-rwd-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->diberikan_oleh }}
                                                </a>
                                            <td>
                                                <a href="javascript:void(0)" class="tahun-rwd-editable"
                                                    data-pk="{{ $data->id }}" data-value="{{ $data->tahun }}">
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="$('#addPenghargaan').removeAttr('disabled');deletethis('frml-rwrd', '{{ $data->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary text-right"
                                        onclick="set_status('reward')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pendidikan_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header row pt-5">
                            <div class="col-md-12">
                                <div class="card-title">
                                    <h3 class="card-label">RIWAYAT PENDIDIKAN DAN PELATIHAN
                                        <span class="text-muted font-size-sm">
                                            <br>Diisi mulai dari rentang terakhir
                                        </span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5>
                                1. Pendidikan Formal
                            </h5>
                            <button class="btn btn-primary" type="button" id="addPndFrml">Add Data</button>
                            <table class="table" id="pend_frml_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 5%">Jenjang</th>
                                        <th style="width: 15%">Penjurusan</th>
                                        <th style="width: 25%">Perguruan Tinggi</th>
                                        <th style="width: 10%">Tahun Lulus</th>
                                        <th style="width: 15%">Kota / Negara</th>
                                        <th style="width: 25%">Penghargaan yang Didapat</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pendidikan as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="lvl-formal-editable"
                                                    data-pk="{{ $data->id }}"
                                                    data-value="{{ $data->level_education }}">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="mjr-formal-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->major_name }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="uninm-formal-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->university_name }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="yr-formal-editable"
                                                    data-pk="{{ $data->id }}">
                                                    {{ $data->tahun_lulus }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="kota-negara-formal-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->kota_negara }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="rwd-formal-editable"
                                                    data-pk="{{ $data->id }}" style="word-break: break-all">
                                                    {{ $data->penghargaan }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="deletethis('pend-frml', '{{ $data->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <hr class="w-100">
                            <h5 class="mt-2">
                                2. Pendidikan dan Latihan / Pengembangan Kompetensi yang Pernah Diikuti (minimal 16 Jam)
                            </h5>
                            <h6>
                                A. Diklat Jabatan
                            </h6>
                            <button class="btn btn-primary" type="button" id="addDkltJbtn">Add Data</button>
                            <table class="table" id="dklt_jbtn_table">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 30%;">Nama Pendidikan dan Latihan / Pengembangan Kompetensi</th>
                                        <th style="width: 15%;">Penyelenggara / Kota</th>
                                        <th style="width: 15%;">Lama Diklat / Pengembangan Kompetensi</th>
                                        <th style="width: 25%;">Nomor Sertifikasi</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($diklat_jabatan as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="nm-keg-dklt-jbt"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->nama_kegiatan }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="pny-dklt-jbt"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->penyelenggara }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="dur-dklt-jbt"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->lama }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="no-srtfks-dklt-jbt"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->nomor_sertifikasi }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="deletethis('emply-dklt', '{{ $row->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <hr class="w-100">
                            <h6>
                                B. Diklat Fungsional
                            </h6>
                            <button class="btn btn-primary" type="button" id="addDkltFnctional">Add Data</button>
                            <table class="table" id="dklt_fnct_table">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 30%;">Nama Pendidikan dan Latihan / Pengembangan Kompetensi</th>
                                        <th style="width: 15%;">Penyelenggara / Kota</th>
                                        <th style="width: 15%;">Lama Diklat / Pengembangan Kompetensi</th>
                                        <th style="width: 25%;">Nomor Sertifikasi</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($diklat_fungsional as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="nm-keg-dklt-fnctional"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->nama_kegiatan }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="pny-dklt-fnctional"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->penyelenggara }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="dur-dklt-fnctional"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->lama }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="no-srtfks-dklt-fnctional"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->nomor_sertifikasi }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="deletethis('emply-dklt', '{{ $row->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary text-right"
                                        onclick="set_status('rwyt=pend')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="karya_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header row pt-5">
                            <div class="col-md-12">
                                <div class="card-title">
                                    <h3 class="card-label"> KARYA TULIS ILMIAH
                                        <span class="text-danger">(dalam 5 tahun terakhir)</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <button class="btn btn-primary" id="addKaryaTulis" type="button">Add Data</button>
                            <table class="table" id="kry_tls_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 75%;">Judul dan Media Publikasi</th>
                                        <th style="width: 10%;">Tahun</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($karya as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="jdl-krya-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->judul }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="thn-krya-editable"
                                                    data-pk="{{ $row->id }}">
                                                    {{ $row->tahun }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="deletethis('karya', '{{ $row->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary text-right"
                                        onclick="set_status('kry-tls')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pengalaman_nara_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header row pt-5">
                            <div class="col-md-12">
                                <div class="card-title">
                                    <h3 class="card-label">PENGALAMAN SEBAGAI PEMBICARA / NARASUMBER / JURI
                                        <br><span class="text-muted font-size-sm">
                                            dalam 5 tahun terakhir
                                        </span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <button class="btn btn-primary" id="addExpAcara" type="button">Add Data</button>
                            <table class="table" id="exp_acara_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 25%;">Acara / Tema</th>
                                        <th style="width: 25%;">Penyelenggara</th>
                                        <th style="width: 10%;">Periode</th>
                                        <th style="width: 25%;">Lokasi dan Peserta</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($exp_acara as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0);" class="acr-tma-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->acara_tema }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="pny-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->penyelenggara }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="period-tma-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->periode }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="lok-psrt-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->lokasi_peserta }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="deletethis('exp-pmbicara', '{{ $row->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary text-right"
                                        onclick="set_status('exp-pmb')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="referensi_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header row pt-5">
                            <div class="col-md-12">
                                <div class="card-title">
                                    <h3 class="card-label">REFERENSI
                                        <br><span class="text-muted font-size-sm">
                                            (yang dapat memberikan keterangan atas keabsahan)
                                        </span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <button class="btn btn-primary" id="addReference" type="button">Add Data</button>
                            <table class="table" id="reference_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 25%">Nama</th>
                                        <th style="width: 25%">Perusahaan</th>
                                        <th style="width: 20%;">Jabatan</th>
                                        <th style="width: 15%;">Nomor Handphone</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($referensi as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0);" class="nm-ref-editable"
                                                    data-pk="{{ $row->id }}"
                                                    style="word-break: break-all">{{ $row->nama }}</a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="comp-ref-editable"
                                                    data-pk="{{ $row->id }}"
                                                    style="word-break: break-all">{{ $row->perusahaan }}</a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="jbtn-ref-editable"
                                                    data-pk="{{ $row->id }}"
                                                    style="word-break: break-all">{{ $row->jabatan }}</a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="no-ref-editable"
                                                    data-pk="{{ $row->id }}"
                                                    style="word-break: break-all">{{ $row->no_hp }}</a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="deletethis('refs-prof', '{{ $row->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary text-right"
                                        onclick="set_status('refs')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="keterangan_keluarga_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header row pt-5">
                            <div class="col-md-12">
                                <div class="card-title">
                                    <h3 class="card-label">KETERANGAN KELUARGA
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="mt-2">
                                1. Istri / Suami
                            </h6>
                            <button class="btn btn-primary" id="addPasangan" type="button">Add Data</button>
                            <table class="table" id="pasangan_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 20%;">Nama</th>
                                        <th style="width: 10%;">Tempat Lahir</th>
                                        <th style="width: 10%;">Tanggal Lahir</th>
                                        <th style="width: 10%;">Tanggal Menikah</th>
                                        <th style="width: 15%;">Pekerjaan</th>
                                        <th style="width: 20%">Keterangan</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pasangan as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="nm-pasangan-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->nama }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="tmp-pasangan-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->tmp_lahir }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="tgl-lhr-pasangan-editable"
                                                    data-pk="{{ $row->id }}">
                                                    {{ $row->tgl_lahir }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="tgl-nkh-pasangan-editable"
                                                    data-pk="{{ $row->id }}">
                                                    {{ $row->tgl_menikah }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="pkrjaan-pasangan-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->pekerjaan }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="ket-pasangan-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->keterangan }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="deletethis('wife-husb', '{{ $row->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <hr class="w-100">
                            <h6 class="mt-2">
                                2. Anak
                            </h6>
                            <button class="btn btn-primary" id="addChild" type="button">Add Data</button>
                            <table class="table" id="children_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 20%;">Nama</th>
                                        <th style="width: 10%;">Tempat Lahir</th>
                                        <th style="width: 10%;">Tanggal Lahir</th>
                                        <th style="width: 10%;">Jenis Kelamin</th>
                                        <th style="width: 15%;">Pekerjaan</th>
                                        <th style="width: 20%;">Keterangan</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($children as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="nm-children-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->nama }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="tmp-children-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->tmp_lahir }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="tgl-lhr-children-editable"
                                                    data-pk="{{ $row->id }}">
                                                    {{ $row->tgl_lahir }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="jns-kel-children-editable"
                                                    data-pk="{{ $row->id }}"
                                                    data-value="{{ $row->jns_kelamin }}">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="pkrjaan-children-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->pekerjaan }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="ket-children-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->keterangan }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="deletethis('emp-child', '{{ $row->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary text-right"
                                        onclick="set_status('emp-fam')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pengalaman_keahlian_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header row pt-5">
                            <div class="col-md-12">
                                <div class="card-title">
                                    <h3 class="card-label">PENGALAMAN DAN KEAHLIAN
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="mt-2">
                                1. Pengalaman
                            </h6>
                            <button class="btn btn-primary" id="addPnglmn" type="button">Add Data</button>
                            <table class="table" id="pengalaman_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 35%;">Pengalaman</th>
                                        <th style="width: 50%">Deskripsi Pengalaman</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pengalaman as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="pnglmn-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->name }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="desc-pnglmn-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->deskripsi }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="deletethis('exp-master', '{{ $row->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <hr class="w-100">
                            <h6 class="mt-2">
                                2. Keahlian
                            </h6>
                            <button class="btn btn-primary" id="addMaster" type="button">Add Data</button>
                            <table class="table" id="master_tbl">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 35%;">Keahlian</th>
                                        <th style="width: 50%;">Deskripsi Keahlian</th>
                                        <th style="width: 10%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($emp_keahlian as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="mstr-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->name }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" class="desc-mstr-editable"
                                                    data-pk="{{ $row->id }}" style="word-break: break-all">
                                                    {{ $row->deskripsi }}
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger"
                                                    onclick="deletethis('exp-master', '{{ $row->id }}');$(this).parent().parent().remove();">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary text-right"
                                        onclick="set_status('exp-mstr')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pengalaman_karir_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header row pt-5">
                            <div class="col-md-12">
                                <div class="card-title">
                                    <h3 class="card-label">PENGALAMAN KARIR KELAS, KLASTER, dan JABATAN di BUMN
                                        <span class="text-muted font-size-sm">
                                            <br>
                                            Sesuai dengan pengalaman kerja baik ketika menduduki posisi atau ketika
                                            bekerja sama
                                            dengan pihak-pihak BUMN
                                        </span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body d-flex flex-column">
                            <h6 class="text-center">
                                Berikan tanda ( √ ) pada KELAS BUMN yang PERNAH menjadi Perusahaan tempat Anda bekerja
                            </h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row text-center">
                                            @foreach ($master_kelas_bumn as $row)
                                                <div class="col-md-3 py-3">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="pengalaman_kelas_bumn[]"
                                                            value="{{ $row->id }}" />
                                                        <span class="mr-5"></span>
                                                        {{ $row->name }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="w-100">
                            <h6 class="text-center mt-2">
                                Berikan centang ( √ ) pada KLASTER BUMN dimana Anda memiliki Exposure
                            </h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row text-center">
                                            @foreach ($master_klaster_bumn as $row)
                                                <div class="col-md-3 py-3">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="pengalaman_klaster_bumn[]"
                                                            value="{{ $row->id }}" />
                                                        <span class="mr-5"></span>
                                                        {{ $row->name }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="w-100">
                            <h6 class="text-center mt-2">
                                Berikan centang ( √ ) pada FUNGSI JABATAN BUMN yang pernah Anda Jabat
                            </h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row text-center">
                                            @foreach ($master_fungsi_jabatan_bumn as $row)
                                                <div class="col-md-3 py-3">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="pengalaman_fungsi_bumn[]"
                                                            value="{{ $row->id }}" />
                                                        <span class="mr-5"></span>
                                                        {{ $row->name }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="w-100">
                            <h6 class="text-center mt-2">Uraikan secara singkat mengenai pengalaman Anda pada KELAS,
                                KLASTER, dan EXPOSURE yang Anda pilih di atas.
                            </h6>
                            <a href="javascript:void(0)" class="pengalaman_uraian_singkat"
                                data-value="{{ $uraian_pengalaman }}"></a>
                            <div class="row mt-5">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary text-right"
                                        onclick="set_status('peng-bumn')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="aspirasi_bumn_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header row pt-5">
                            <div class="col-md-12">
                                <div class="card-title">
                                    <h3 class="card-label">ASPIRASI PADA KELAS, KLASTER, dan JABATAN di BUMN
                                        <span class="text-muted font-size-sm">
                                            <br>
                                            Sesuai dengan pengalaman dan/atau pendidikan
                                        </span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body d-flex flex-column">
                            <h6 class="text-center">
                                Berikan tanda ( √ ) pada KELAS BUMN yang menjadi Aspirasi Anda
                            </h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row text-center">
                                            @foreach ($master_kelas_bumn as $row)
                                                <div class="col-md-3 py-3">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="aspirasi_kelas_bumn[]"
                                                            value="{{ $row->id }}" />
                                                        <span class="mr-5"></span>
                                                        {{ $row->name }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="w-100">
                            <h6 class="text-center mt-2">
                                Berikan centang ( √ ) pada KLASTER BUMN yang menjadi Aspirasi Anda
                            </h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row text-center">
                                            @foreach ($master_klaster_bumn as $row)
                                                <div class="col-md-3 py-3">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="aspirasi_klaster_bumn[]"
                                                            value="{{ $row->id }}" />
                                                        <span class="mr-5"></span>
                                                        {{ $row->name }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="w-100">
                            <h6 class="text-center mt-2">
                                Berikan centang ( √ ) pada FUNGSI JABATAN BUMN yang menjadi Aspirasi Anda
                            </h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row text-center">
                                            @foreach ($master_fungsi_jabatan_bumn as $row)
                                                <div class="col-md-3 py-3">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="aspirasi_jabatan_bumn[]"
                                                            value="{{ $row->id }}" />
                                                        <span class="mr-5"></span>
                                                        {{ $row->name }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="w-100">
                            <h6 class="text-center mt-2">Uraikan / Berikan argumentasi / Berikan alasan Anda atas
                                Aspirasi
                                yang telah Anda pilih.
                                Berdasar atas keahlian / pengalaman apa yang menjadi dasar Anda memilih Aspirasi atas
                                Kelas,
                                Klaster dan Fungsi Jabatan tersebut?
                            </h6>
                            <a href="javascript:void(0)" class="aspirasi_uraian_singkat"
                                data-value="{{ $uraian_aspirasi }}"></a>
                            <div class="row mt-5">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary text-right"
                                        onclick="set_status('asp-bumn')">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="dokumen_kepegawaian_tab" role="tabpanel">
                    <div class="card card-custom card-stretch">
                        <div class="card-header row pt-5">
                            <div class="col-md-12">
                                <div class="card-title">
                                    <h3 class="card-label">DOKUMEN KEPEGAWAIAN
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body d-flex flex-column">
                            <table class="table">
                                <tr>
                                    <th>No</th>
                                    <th>Dokumen SK</th>
                                    <th>Dokumen Pakta Integritas</th>
                                    <th>Tanggal Rilis</th>
                                </tr>
                                @forelse($docs as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            <a href="{{ asset('dok-kepegawaian/' . $row->nm_peg_docs) }}"
                                                target="_blank">{{ $row->nm_peg_docs }}</a>
                                        </td>
                                        <td>
                                            <a href="{{ asset('pakta-integritas/' . $row->nm_pkt_docs) }}"
                                                target="_blank">{{ $row->nm_pkt_docs }}</a>
                                        </td>
                                        <td>{{ date('d/m/Y', strtotime($row->tgl)) }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="4">Tidak Ada Data</td>
                                    </tr>
                                @endforelse
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap-editable/css/bootstrap-editable.css"
        integrity="sha512-e0rbO6UJET0zDdXOHjwc6D44UpeKumn7cU7XR/fa4S0/Jso0bZqcCqlIF6mtvcimMbf846mkv8aSWFnTwABr/g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .control-group.form-group,
        .control-group.form-group div,
        .control-group.form-group div .editable-input {
            width: 100%;
        }

    </style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    @if (session('status') == 'success')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
            });
        </script>
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js">
    </script>
    <script src="{{ asset('js/pages/features/miscellaneous/sticky-panels.js') }}"></script>
    <script>
        $.fn.editable.defaults.mode = 'inline';
        $.fn.editable.defaults.onblur = "submit";
        $.fn.editable.defaults.send = "always";
        $(document).ready(function() {
            new KTImageInput('kt_image_3');
            initCheckbox();
            $.ajaxSetup({
                headers: {
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content"),
                },
            });
            initEditable();
        });

        function initEditable() {
            history_jabatan_1();
            history_jabatan_2();
            organization_formal();
            organization_informal();
            reward();
            pendidikan_formal();
            dklt_jbtn();
            dklt_fnctional();
            krytls();
            exp_acara();
            pasangan();
            children();
            pengalaman();
            master();
            refs();
            init_kelas();
            init_klaster();
            init_func_jabatan();

            $('.pengalaman_uraian_singkat').editable({
                type: 'textarea',
                url: "{{ route('my-profile.update_pengalaman_bumn') }}",
                showbuttons: false,
                rows: 10,
                cols: 30,
                inputclass: 'w-100',
                params: function(params) {
                    params.typ = "pnglaman";
                    params.sct = "uraian";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('form-control')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('form-control')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('form-control')
                    }
                }
            });

            $('.aspirasi_uraian_singkat').editable({
                type: 'textarea',
                url: "{{ route('my-profile.update_pengalaman_bumn') }}",
                showbuttons: false,
                rows: 10,
                cols: 30,
                inputclass: 'w-100',
                params: function(params) {
                    params.typ = "asp";
                    params.sct = "uraian";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('form-control')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('form-control')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('form-control')
                    }
                }
            });
        }

        // Riwayat Jabatan
        // Jabatan/Pekerjaan yang Pernah/Sedang Diemban
        function history_jabatan_1() {
            $('.uraian-editable').editable({
                type: 'textarea',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_riwayat_jabatan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "uraian";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.achievement-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_riwayat_jabatan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "achievement";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }

                }
            });

            $('.hist-jbtn-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_riwayat_jabatan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "jbtn";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.st-date-hst-jbtn-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_riwayat_jabatan') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "start-date";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty');
                        $(this).css("background-color", "#F64E60");
                        $(this).addClass('btn btn-danger');
                        $(this).addClass('editable-empty');
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });

            $('.en-date-hst-jbtn-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_riwayat_jabatan') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "end-date";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else if (value == "0000-00-00") {
                        $(this).text("Sekarang");
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty');
                        $(this).css("background-color", "#F64E60");
                        $(this).addClass('btn btn-danger');
                        $(this).addClass('editable-empty');
                    } else if (value == "0000-00-00") {
                        $(this).text("Sekarang");
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });
        }

        // Riwayat Jabatan
        // Penugasan yang berkaitan dengan Jabatan Direksi/Dewan Komisaris/Dewan Pengawas (bila ada)
        function history_jabatan_2() {
            $('.penugasan-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_riwayat_two') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "penugasan";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.tupoksi-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_riwayat_two') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "tupoksi";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.start-date-penugasan-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_riwayat_two') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "start-date";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty');
                        $(this).css("background-color", "#F64E60");
                        $(this).addClass('btn btn-danger');
                        $(this).addClass('editable-empty');
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });

            $('.end-date-penugasan-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_riwayat_two') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "end-date";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty');
                        $(this).css("background-color", "#F64E60");
                        $(this).addClass('btn btn-danger');
                        $(this).addClass('editable-empty');
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });

            $('.instansi-two-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_riwayat_two') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "instansi";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            if ($("a.penugasan-editable").length == 7) {
                $("#addHistoryTwo").attr("disabled", true);
            }
        }

        function organization_formal() {
            $('.name-keg-org-formal-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_organization') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "nm-keg";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.jabatan-keg-org-formal-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_organization') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "jabatan";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.start-date-keg-org-formal-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_organization') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "start-date";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty');
                        $(this).css("background-color", "#F64E60");
                        $(this).addClass('btn btn-danger');
                        $(this).addClass('editable-empty');
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });

            $('.end-date-keg-org-formal-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_organization') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "end-date";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty');
                        $(this).css("background-color", "#F64E60");
                        $(this).addClass('btn btn-danger');
                        $(this).addClass('editable-empty');
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });

            $('.uraian-keg-org-formal-editable').editable({
                type: 'textarea',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_organization') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "uraian";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            if ($("a.name-keg-org-formal-editable").length == 4) {
                $("#addOrgFormal").attr("disabled", true);
            }
        }

        function organization_informal() {
            $('.name-keg-org-nonformal-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_organization') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "nm-keg";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.jabatan-keg-org-nonformal-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_organization') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "jabatan";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.start-date-keg-org-nonformal-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_organization') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "start-date";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty');
                        $(this).css("background-color", "#F64E60");
                        $(this).addClass('btn btn-danger');
                        $(this).addClass('editable-empty');
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });

            $('.end-date-keg-org-nonformal-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_organization') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "end-date";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty');
                        $(this).css("background-color", "#F64E60");
                        $(this).addClass('btn btn-danger');
                        $(this).addClass('editable-empty');
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });

            $('.uraian-keg-org-nonformal-editable').editable({
                type: 'textarea',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_organization') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "uraian";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            if ($("a.name-keg-org-nonformal-editable").length == 4) {
                $("#addOrgNonFormal").attr("disabled", true);
            }
        }

        function reward() {
            $('.jns-rwd-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_reward') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "jns-rwd";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.tingkat-rwd-editable').editable({
                type: 'select',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_reward') }}",
                showbuttons: false,
                source: [{
                        value: "Organisasi Kerja",
                        text: "Organisasi Kerja"
                    },
                    {
                        value: "Nasional",
                        text: "Nasional"
                    },
                    {
                        value: "Internasional",
                        text: "Internasional"
                    }
                ],
                params: function(params) {
                    params.sct = "tingkat-rwd";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.diberikan-rwd-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_reward') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "diberikan-rwd";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.tahun-rwd-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_reward') }}",
                showbuttons: false,
                tpl: `<input type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" maxlength="4" />`,
                params: function(params) {
                    params.sct = "tahun";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            if ($("a.jns-rwd-editable").length == 5) {
                $("#addPenghargaan").attr("disabled", true);
            }
        }

        function pendidikan_formal() {
            $('.lvl-formal-editable').editable({
                type: 'select',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pendidikan') }}",
                showbuttons: false,
                source: [{
                        value: "SD",
                        text: "SD"
                    },
                    {
                        value: "SLTP",
                        text: "SLTP"
                    },
                    {
                        value: "SLTA",
                        text: "SLTA"
                    },
                    {
                        value: "D.I",
                        text: "D.I"
                    },
                    {
                        value: "D.II",
                        text: "D.II"
                    },
                    {
                        value: "D.III",
                        text: "D.III"
                    },
                    {
                        value: "D.IV",
                        text: "D.IV"
                    },
                    {
                        value: "S.1",
                        text: "S.1"
                    },
                    {
                        value: "S.2",
                        text: "S.2"
                    },
                    {
                        value: "S.3",
                        text: "S.3"
                    },
                ],
                params: function(params) {
                    params.sct = "lvl";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.mjr-formal-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pendidikan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "mjr";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.uninm-formal-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pendidikan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "unimnm";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.yr-formal-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pendidikan') }}",
                showbuttons: false,
                tpl: `<input type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" maxlength="4" />`,
                params: function(params) {
                    params.sct = "tahun";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.kota-negara-formal-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pendidikan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "kt-ngr";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.rwd-formal-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pendidikan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "rwd-formal";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });
        }

        function initCheckbox() {
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.get_keahlian') }}",
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    setFrm(false);
                    if (response.length) {
                        $.each(response, function(indexInArray, valueOfElement) {
                            $('input[name="keahlian[]"][value=' +
                                valueOfElement.id_keahlian + ']').attr("checked", true);
                            if (valueOfElement.keterangan) {
                                setFrm(true);
                                $('input[name=keahlian_lain]').val(valueOfElement.keterangan);
                            }
                        });
                    }
                }
            });
        }

        function frmLain(context) {
            if (context.is(':checked')) {
                setFrm(true);
            } else {
                setFrm(false);
            }
        }

        function setFrm(is_enable) {
            if (is_enable) {
                $("input[name=keahlian_lain]").removeAttr('disabled').attr('required', true);
            } else {
                $("input[name=keahlian_lain]").val('').attr('disabled', true).removeAttr('required');
            }
        }

        function limit(element, max) {
            var max_chars = max;
            if (element.value.length > max_chars) {
                element.value = element.value.substr(0, max_chars);
            }
        }

        $("#addOrgFormal").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_organization') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    sct: "formal"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.name-keg-org-formal-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="name-keg-org-formal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="jabatan-keg-org-formal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="start-date-keg-org-formal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="end-date-keg-org-formal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="uraian-keg-org-formal-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="$(`#addOrgFormal`).removeAttr(`disabled`);deletethis(`frml-org`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#org_formal_tbl tbody").append(hatml);
                        organization_formal();
                    }
                }
            });
        });

        $("#addOrgNonFormal").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_organization') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    sct: "nonformal"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.name-keg-org-nonformal-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="name-keg-org-nonformal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="jabatan-keg-org-nonformal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="start-date-keg-org-nonformal-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="end-date-keg-org-nonformal-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="uraian-keg-org-nonformal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="$(`#addOrgNonFormal`).removeAttr(`disabled`);deletethis(`frml-org`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#org_nonformal_tbl tbody").append(hatml);
                        organization_informal();
                    }
                }
            });
        });

        $("#addHistoryOne").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_riwayat_one') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.hist-jbtn-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="hist-jbtn-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="uraian-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="st-date-hst-jbtn-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="en-date-hst-jbtn-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="achievement-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`rwyt-jbtn`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#history_one_tbl tbody").append(hatml);
                        history_jabatan_1();
                    }
                }
            });
        });

        $("#addHistoryTwo").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_riwayat_two') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.penugasan-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="penugasan-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="tupoksi-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="start-date-penugasan-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="end-date-penugasan-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="instansi-two-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`rwyt-pngsn`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#history_two_tbl tbody").append(hatml);
                        history_jabatan_2();
                    }
                }
            });
        });

        $("#addPenghargaan").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_reward') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.jns-rwd-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="jns-rwd-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="tingkat-rwd-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="diberikan-rwd-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="tahun-rwd-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`frml-rwrd`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#penghargaan_tbl tbody").append(hatml);
                        reward();
                    }
                }
            });
        });

        function dklt_jbtn() {
            $('.nm-keg-dklt-jbt').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_diklat') }}",
                showbuttons: false,
                params: function(params) {
                    params.frm = "nmkeg";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.pny-dklt-jbt').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_diklat') }}",
                showbuttons: false,
                params: function(params) {
                    params.frm = "pny";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.dur-dklt-jbt').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_diklat') }}",
                showbuttons: false,
                params: function(params) {
                    params.frm = "dur";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.no-srtfks-dklt-jbt').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_diklat') }}",
                showbuttons: false,
                params: function(params) {
                    params.frm = "stfkt";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });
        }

        $("#addDkltJbtn").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_diklat') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    sct: "jbtn"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.nm-keg-dklt-jbt").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="nm-keg-dklt-jbt" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="pny-dklt-jbt" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="dur-dklt-jbt" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="no-srtfks-dklt-jbt" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`emply-dklt`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#dklt_jbtn_table tbody").append(hatml);
                        dklt_jbtn();
                    }
                }
            });
        });

        function dklt_fnctional() {
            $('.nm-keg-dklt-fnctional').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_diklat') }}",
                showbuttons: false,
                params: function(params) {
                    params.frm = "nmkeg";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.pny-dklt-fnctional').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_diklat') }}",
                showbuttons: false,
                params: function(params) {
                    params.frm = "pny";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.dur-dklt-fnctional').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_diklat') }}",
                showbuttons: false,
                params: function(params) {
                    params.frm = "dur";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.no-srtfks-dklt-fnctional').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_diklat') }}",
                showbuttons: false,
                params: function(params) {
                    params.frm = "stfkt";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });
        }

        $("#addDkltFnctional").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_diklat') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    sct: "fnctional"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.nm-keg-dklt-fnctional").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="nm-keg-dklt-fnctional" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="pny-dklt-fnctional" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="dur-dklt-fnctional" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="no-srtfks-dklt-fnctional" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`emply-dklt`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#dklt_fnct_table tbody").append(hatml);
                        dklt_fnctional();
                    }
                }
            });
        });

        function krytls() {
            $('.jdl-krya-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_karya') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "jdl";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.thn-krya-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_karya') }}",
                showbuttons: false,
                tpl: `<input type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" maxlength="4" />`,
                params: function(params) {
                    params.sct = "thn";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });
        }

        $("#addKaryaTulis").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_karya') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.jdl-krya-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="jdl-krya-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="thn-krya-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`karya`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#kry_tls_tbl tbody").append(hatml);
                        krytls();
                    }
                }
            });
        });

        function exp_acara() {
            $('.acr-tma-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_exp_acara') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "acr";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.pny-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_exp_acara') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "pny";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.period-tma-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_exp_acara') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "period";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.lok-psrt-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_exp_acara') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "lok";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });
        }

        $("#addExpAcara").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_exp_acara') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.acr-tma-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="acr-tma-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="pny-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="period-tma-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="lok-psrt-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`exp-pmbicara`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#exp_acara_tbl tbody").append(hatml);
                        exp_acara();
                    }
                }
            });
        });

        function refs() {
            $('.nm-ref-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_reference') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "nm";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.comp-ref-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_reference') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "comp";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.jbtn-ref-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_reference') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "jbtn";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.no-ref-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_reference') }}",
                showbuttons: false,
                tpl: `<input type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" />`,
                params: function(params) {
                    params.sct = "nohp";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });
        }

        $("#addReference").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_reference') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.nm-ref-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="nm-ref-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="comp-ref-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="jbtn-ref-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="no-ref-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`refs-prof`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#reference_tbl tbody").append(hatml);
                        refs();
                    }
                }
            });
        });

        function pasangan() {
            $('.nm-pasangan-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pasangan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "nm";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.tmp-pasangan-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pasangan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "tmp";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.tgl-lhr-pasangan-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pasangan') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "tgl-lhr";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });

            $('.tgl-nkh-pasangan-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pasangan') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "tgl-nkh";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });

            $('.pkrjaan-pasangan-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pasangan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "pkrjaan";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.ket-pasangan-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pasangan') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "ket";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });
        }

        $("#addPasangan").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_pasangan') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.nm-pasangan-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="nm-pasangan-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="tmp-pasangan-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="tgl-lhr-pasangan-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="tgl-nkh-pasangan-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="pkrjaan-pasangan-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="ket-pasangan-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`wife-husb`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#pasangan_tbl tbody").append(hatml);
                        pasangan();
                    }
                }
            });
        });

        function children() {
            $('.nm-children-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_children') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "nm";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.tmp-children-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_children') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "tmp";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.tgl-lhr-children-editable').editable({
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_children') }}",
                showbuttons: false,
                tpl: `<input type="date" />`,
                params: function(params) {
                    params.sct = "tgl-lhr";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        let date = new Date(value);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        let date = new Date(response);
                        $(this).text(getFullDate(date.getDate()) + "/" + getFullMonth(date.getMonth()) + "/" +
                            date
                            .getFullYear());
                        $(this).css("background-color", "rgba(0, 0,0,0)");
                        $(this).removeClass('btn btn-danger');
                    }
                }
            });

            $('.jns-kel-children-editable').editable({
                type: 'select',
                source: [{
                        value: 'Laki - Laki',
                        text: "Laki - Laki"
                    },
                    {
                        value: "Perempuan",
                        text: "Perempuan"
                    }
                ],
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_children') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "jns-kel";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.pkrjaan-children-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_children') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "pkrjaan";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.ket-children-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_children') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "ket";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });
        }

        $("#addChild").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_children') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.nm-children-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="nm-children-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="tmp-children-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="tgl-lhr-children-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="jns-kel-children-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="pkrjaan-children-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="ket-children-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`emp-child`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#children_tbl tbody").append(hatml);
                        children();
                    }
                }
            });
        });

        function pengalaman() {
            $('.pnglmn-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pengalaman') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "nm";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.desc-pnglmn-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pengalaman') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "desc";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });
        }

        $("#addPnglmn").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_pengalaman') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    sct: "exp"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.pnglmn-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="pnglmn-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="desc-pnglmn-editable" data-pk="' +
                            response + '"style="word-break: break-all"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`exp-master`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#pengalaman_tbl tbody").append(hatml);
                        pengalaman();
                    }
                }
            });
        });

        function master() {
            $('.mstr-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pengalaman') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "nm";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });

            $('.desc-mstr-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('my-profile.update_pengalaman') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "desc";
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                }
            });
        }

        $("#addMaster").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_pengalaman') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    sct: "mstr"
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.mstr-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="mstr-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="desc-mstr-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`exp-master`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#master_tbl tbody").append(hatml);
                        master();
                    }
                }
            });
        });

        function saveInterest() {
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.update_interest') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    val: $("textarea[name=interest]").val()
                },
                serverSide: true,
                success: function(response) {
                    swalfire(response.title, response.text, response.status);
                }
            });
        }

        $('input[name="pengalaman_kelas_bumn[]"]').change(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.update_pengalaman_bumn') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    status: $(this).is(':checked'),
                    val: $(this).val(),
                    sct: 'kls',
                    typ: 'pnglaman'
                },
                success: function(response) {
                    // console.log("sukses");
                }
            });
        });

        $('input[name="pengalaman_klaster_bumn[]"]').change(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.update_pengalaman_bumn') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    status: $(this).is(':checked'),
                    val: $(this).val(),
                    sct: 'klstr',
                    typ: 'pnglaman'
                },
                success: function(response) {
                    // console.log("sukses");
                }
            });
        });

        $('input[name="pengalaman_fungsi_bumn[]"]').change(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.update_pengalaman_bumn') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    status: $(this).is(':checked'),
                    val: $(this).val(),
                    sct: 'jbtn-bumn',
                    typ: 'pnglaman'
                },
                success: function(response) {
                    // console.log("sukses");
                }
            });
        });

        $('input[name="aspirasi_kelas_bumn[]"]').change(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.update_pengalaman_bumn') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    status: $(this).is(':checked'),
                    val: $(this).val(),
                    sct: 'kls',
                    typ: 'asp'
                },
                success: function(response) {
                    // console.log("sukses");
                }
            });
        });

        $('input[name="aspirasi_klaster_bumn[]"]').change(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.update_pengalaman_bumn') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    status: $(this).is(':checked'),
                    val: $(this).val(),
                    sct: 'klstr',
                    typ: 'asp'
                },
                success: function(response) {
                    // console.log("sukses");
                }
            });
        });

        $('input[name="aspirasi_jabatan_bumn[]"]').change(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.update_pengalaman_bumn') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    status: $(this).is(':checked'),
                    val: $(this).val(),
                    sct: 'jbtn-bumn',
                    typ: 'asp'
                },
                success: function(response) {
                    // console.log("sukses");
                }
            });
        });

        function deletethis(type, id) {
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.delete') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    sct: type,
                    id: id
                },
                success: function(response) {
                    if (response) {
                        // console.log("k");
                    }
                }
            });
        }

        $("#addPndFrml").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.init_pendidikan') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function(response) {
                    if (response) {
                        let hatml = '<tr>' +
                            '<td>' + ($("a.lvl-formal-editable").length + 1) + '</td>' +
                            '<td><a href="javascript:void(0)" class="lvl-formal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="mjr-formal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="uninm-formal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="yr-formal-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><a href="javascript:void(0)" class="kota-negara-formal-editable" data-pk="' +
                            response + '" style="word-break: break-all"></a></td>' +
                            '<td><a href="javascript:void(0)" class="rwd-formal-editable" data-pk="' +
                            response + '"></a></td>' +
                            '<td><button class="btn btn-danger" onclick="deletethis(`pend-frml`, `' +
                        response +
                        '`);$(this).parent().parent().remove();"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>';
                        $("#pend_frml_tbl tbody").append(hatml);
                        pendidikan_formal();
                    }
                }
            });
        });

        $("#updPrfFrm").submit(function(e) {
            if ($("input[name=npwp]").val().length != 15) {
                swalfire("Failed", 'NPWP must 15 number !!', 'failed');
                return false;
            }
            return true;
        });

        function swalfire(title, text, icon) {
            Swal.fire({
                title: title,
                text: text,
                icon: icon,
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        }

        function getFullDate(date) {
            if (date < 9) {
                date = "0" + date;
            }
            return date;
        }

        function getFullMonth(month) {
            if (month < 9) {
                month = "0" + (month + 1);
            } else {
                month = month + 1;
            }
            return month;
        }

        function init_kelas() {
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.get_employee_kelas') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    $.each(response, function(indexInArray, valueOfElement) {
                        if (valueOfElement.type == "pengalaman") {
                            $("input[name='pengalaman_kelas_bumn[]'][value='" + valueOfElement
                                .id_kelas + "']").prop('checked', valueOfElement.status);
                        } else if (valueOfElement.type == "aspirasi") {
                            $("input[name='aspirasi_kelas_bumn[]'][value='" + valueOfElement
                                .id_kelas + "']").prop('checked', valueOfElement.status);
                        }
                    });
                }
            });
        }

        function init_klaster() {
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.get_employee_cluster') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    $.each(response, function(indexInArray, valueOfElement) {
                        if (valueOfElement.type == "pengalaman") {
                            $("input[name='pengalaman_klaster_bumn[]'][value='" + valueOfElement
                                .id_kluster + "']").prop('checked', valueOfElement.status);
                        } else if (valueOfElement.type == "aspirasi") {
                            $("input[name='aspirasi_klaster_bumn[]'][value='" + valueOfElement
                                .id_kluster + "']").prop('checked', valueOfElement.status);
                        }
                    });
                }
            });
        }

        function init_func_jabatan() {
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.get_employee_fnc_jbtn') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    $.each(response, function(indexInArray, valueOfElement) {
                        if (valueOfElement.type == "pengalaman") {
                            $("input[name='pengalaman_fungsi_bumn[]'][value='" + valueOfElement
                                .id_jbtn_bumn + "']").prop('checked', valueOfElement.status);
                        } else if (valueOfElement.type == "aspirasi") {
                            $("input[name='aspirasi_jabatan_bumn[]'][value='" + valueOfElement
                                .id_jbtn_bumn + "']").prop('checked', valueOfElement.status);
                        }
                    });
                }
            });
        }

        function set_status(params) {
            $.ajax({
                type: "POST",
                url: "{{ route('my-profile.set_status') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    section: params
                },
                success: function(response) {
                    if (response) {
                        swalfire('Success', 'Save Data Success', 'success');
                    } else {
                        swalfire('Failed', 'Save Data Failed', 'failed')
                    }
                }
            });
        }
    </script>
@endsection
