@extends('layout.default')

@section('styles')
@endsection

@section('page_toolbar')
@endsection


@section('content')
<div class="card card-custom">
    <div class="card-body">
             {{-- Filter --}}
             <div class="form-row mb-5">
                <div class="col-lg-3">
                    <input type="text" readonly class="form-control filter" id="tanggalberitaacara" placeholder="Tgl Berita Acara DD-MM-YYYY">
                </div>
                <div class="col-lg-3">
                    <select class="form-control filter" id="namaevent" autocomplete="off">
                        <option></option>
                        @foreach($sub_event as $value)
                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-lg-3">
                    <select class="form-control filter" id="prosesba">
                        <option></option>
                        @foreach($prosesBA as $value)
                        <option value="{{ $value['key'] }}">{{ $value['val'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            {{-- End Filter --}}
    
        <table class="table table-bordered table-hover text-center" id="tableBeritaAcara">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Event</th>
                    <th>Proses Berita Acara</th>
                    <th>Number Berita Acara</th>
                    <th>Tanggal Berita Acara</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@section('scripts')
@if (session('status'))
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif

<script type="text/javascript">
    const routeDataTable = "{{ route('BeritaAcara.dataTableBeritaAcara') }}",
        routeConstViewEmployee = "{{ route('BeritaAcara.show', ':id') }}",
        routeConstDeleteEmployee = "{{ route('BeritaAcara.destroy', ':id') }}",
        csrfToken = "{{ csrf_token() }}";

    var routeViewEmployee = "{{ route('BeritaAcara.show', ':id') }}",
        routeDeleteEmployee = "{{ route('BeritaAcara.destroy', ':id') }}";
</script>
<script type="text/javascript" src="{{ asset('js/berita-acara-histori/index.js?nocache='.time()) }}"></script>
@endsection