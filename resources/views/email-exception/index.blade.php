<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <style>{!! $css ?? '' !!}</style>
    </head>
    <body class="theme-dark">
        <h1>#stopERR500Bestie!</h1>
        <h3>URL Error: {!! $header !!}</h3>
        {!! $content ?? '' !!}
    </body>
</html>