{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="row">
    @foreach($position as $item)
    <div class="col-md-4 mb-5">
        <div class="card shadow-sm" style="min-height: 25vh">
            <div class="card-body d-flex flex-column">
                <a href="{{ route('calibration.detail', $item->id) }}">
                    <h5 class="card-title text-primary mb-2">{{ $item->name_position }}</h5>
                </a>
                <div>
                    <span class="label label-light-warning label-lg label-inline">{{ $item->status }}</span>
                </div>
                <div class="row mt-5 mt-auto">
                    <div class="col-md-5 align-self-center">
                        Total Candidats
                    </div>
                    <div class="col-md-auto text-center">
                        <h1 class="text-primary">18 of 37</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link href="{{asset('plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<style>
    @media only screen and (max-width: 600px) {
        .d-flex {
            flex-direction: column;
            flex-flow: wrap;
            justify-content: space-between;
        }
        .d-flex > .btn {
            margin: 4px;
        }
    }
    
    .container-customeHeight {
        height: 200px;
    }

    .label-custome {
        padding: 6px 8px;
        width: 40px;
        height: 40px;
        justify-content: center;
        margin: 0;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        border-radius: 50%;
        background-color: #EBEDF3;
        font-weight: 900!important;
        color: black!important;
        font-size: 3rem!important;
    }
    .content {
        overflow-x: auto;
    }
    .overlay {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
    }
    .blockOverlay {
        background-color: transparent!important
    }
    
    .placeHold{
        border: 1px dashed blue;
        background-color: #c7c7c7;
        height:40px;
        width:228px;
        list-style-type:none;
    }

    .symbol-label-custome {
        width:35px!important;
        height:25px!important;
    }
    
    .facet {
        width:228px;
        height:40px;
        margin-right: 2px;
        display:flex;
        overflow-x:hidden;
        flex:auto;
    }  

    .blockOverlay {
        background-color: transparent!important
    }

    .master-facet {
        list-style-type:none;
        min-width:100%;
        flex-wrap: wrap;
        flex-direction: row;
        overflow-y: auto;
        /* height: 180px; */
        place-content: flex-start;
        /* flex-flow: column; */
    }

    .master-facet-bigger {
        list-style-type:none;
        min-width:100%;
        flex-wrap: wrap;
        flex-direction: row;
        overflow-x: auto;
        overflow-y: auto;
        height: 214px;
        /* flex-flow: column; */
    }

    .xxxx {        
        list-style-type:none;
        overflow: auto;
        flex-flow: wrap;
        flex-direction: row;
        flex-wrap: wrap;
        flex-basis: auto;
        display: flex;
        align-items: flex-start;
        height: 228px;        
    }
</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
<script src="{{asset('js/pages/crud/forms/widgets/select2.js')}}"></script>
<script src="{{asset('plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
<script>
    var dataFilter  = "";
    var hrOnly      = false;
    var pos         = [];
    var grade       = [];
    var talent      = false;
    var is_session  = false;

    //fungsi load employee
    function loadEmployee(panel) {
        $('.0,.1,.2,.3,.4,.5,.6,.7,.8,.9').html("")

        KTApp.block('.0,.1,.2,.3,.4,.5,.6,.7,.8,.9', {
            overlayColor    : '#000000',
            state           : 'primary',
            message         : 'Processing...'
        });

        let url = "{{ url('calibration/get-employee') }}"
        var xhr = $.ajax({
            url     : url,
            type    : 'POST',
            data    : {
                "_token"    : "{{ csrf_token() }}",
                // "panel"     : panel,
                "hrOnly"    : hrOnly,
                "pos"       : pos,
                "grade"     : grade,
                "talent"    : talent,
                "is_session" : is_session
            },
            timeout: 100000,
            success : function (response) {

                let res = JSON.parse(response);
                $.each(res.view, function( key, value ) {
                    $('.'+key).html(value)
                    $('.'+key).find(".check-compare").each(function() {
                        $('.'+key).find(".check-compare").removeClass("fa-check");
                        $('.'+key).find(".check-compare").removeClass("text-success");
                        $('.'+key).find(".check-compare").addClass("fa-square");
                        $('.'+key).find(".check-compare").css("display","inline");
                    });
                });
                $.each(res.total, function( key, value ) {
                    $('body #label-panel-'+key).html(value)
                });
                KTApp.unblock('.0,.1,.2,.3,.4,.5,.6,.7,.8,.9')
            },
            error: function(jqXHR, textStatus, errorThrown){
                KTApp.unblock('.'+panel)
                
                let appendText = "<span>Unable to load. Please reload this box</span><br>"
                let appendButton = "<button class='btn btn-sm btn-inline btn-dark' onclick='loadEmployee("+panel+");'>Reload</button>"
                let $containerDivFail = $("<div class='btn-reload-fail-box-"+panel+"'>"+appendText+appendButton+"</div>")
                $('.'+panel).html($containerDivFail)
            }
        })
    }
    
    //fungsi click score card
    $(document).on("click",".open-career-card", function (e) {
        e.preventDefault();
        var nik = $(this).data('nik')
        
        var url = "{{ url('calibration/view-career-card') }}" + '/' + nik + '/true'
        $.ajax({
            url     : url,
            type    : 'GET',
            success : function (response) {
                $('body #xl-modal').modal('show');
                $('.data-modal').html(response)
            }
        })
    })

    var compareData = [];
    var compareDataEnc = [];

    //fungsi cek checkbox
    $(document).on("click",".compare-mark", function (e) {
        
        $(this).find(".check-compare").removeClass("fa-square");
        $(this).find(".check-compare").addClass("fa-check");
        $(this).find(".check-compare").addClass("text-success");

        let usernik = $(this).data('nik');
        let usernikEnc = $(this).data('nik-enc');

        if (compareData.includes(usernik)) {
            compareData = removeNikFromArray(compareData, usernik)
            compareDataEnc = removeNikFromArray(compareDataEnc, usernikEnc)
            // $(this).find(".check-compare").hide();
            $(this).find(".check-compare").removeClass("fa-check");        
            $(this).find(".check-compare").removeClass("text-success");
            $(this).find(".check-compare").addClass("fa-square");

        } else {
            compareData.push(usernik);
            compareDataEnc.push(usernikEnc);
            // $(this).find(".check-compare").show();
            $(this).find(".check-compare").removeClass("fa-square");
            $(this).find(".check-compare").addClass("fa-check");
            $(this).find(".check-compare").addClass("text-success");
        }

        let jmlData = compareData.length

        if ( jmlData > 3 ) {
            let delNik = compareData[0]
            // $(".compare-"+delNik).find(".check-compare").hide();
            $(".compare-"+delNik).find(".check-compare").removeClass("fa-check");        
            $(".compare-"+delNik).find(".check-compare").removeClass("text-success");
            $(".compare-"+delNik).find(".check-compare").addClass("fa-square");
            compareData.shift();
            compareDataEnc.shift();
        }

        if ( jmlData < 2 ) {
            $(".btn-compare").prop('href','javascript:void(0)')            
            $(".btn-compare").text("Compare")    
        } else {
            $(".btn-compare").text("OK")

            let url = "{{ url('calibration/compare') . '/' }}"  + compareDataEnc.join("-")
            $(".btn-compare").prop('href',url)
        }
    })

    loadEmployee(0);
</script>
@endsection
