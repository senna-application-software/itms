{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 custome-overflow">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <tr class="bg-light-white">
                                    <td colspan="2" class="align-middle">
                                        <h4>Comparison Parameters</h4>
                                    </td>
                                    <td colspan="2">
                                        <div class="row">
                                            <div class="col-sm-12 col-lg-6 col-md-12">
                                                <div class="flex-shrink-0 mr-7">

                                                    <div class="symbol symbol-50 symbol-lg-120 symbol-light-danger">
                                                        <img src="http://kabayanconsulting.co.id:3002/talent-review/public/asset-mt/media/svg/avatars/007-boy-2.svg"
                                                            alt="image" class=""
                                                            style="width:100%; max-height:250px; object-fit: cover; object-position: 100% 0%;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-lg-6 col-md-12">
                                                <small>
                                                    Fuli humaeroh
                                                    <br>69964160
                                                    <br>SVP-Head of Regional &amp; SME
                                                    <br>Indonesian
                                                </small>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <div class="row">
                                            <div class="col-sm-12 col-lg-6 col-md-12">
                                                <div class="flex-shrink-0 mr-7">

                                                    <div class="symbol symbol-50 symbol-lg-120 symbol-light-danger">
                                                        <img src="http://kabayanconsulting.co.id:3002/talent-review/public/asset-mt/media/svg/avatars/007-boy-2.svg"
                                                            alt="image" class=""
                                                            style="width:100%; max-height:250px; object-fit: cover; object-position: 100% 0%;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-lg-6 col-md-12">
                                                <small>
                                                    Gede krishna jaya
                                                    <br>69962511
                                                    <br>SVP-Head of Channel Management
                                                    <br>Indonesian
                                                </small>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <div class="row">
                                            <div class="col-sm-12 col-lg-6 col-md-12">
                                                <div class="flex-shrink-0 mr-7">

                                                    <div class="symbol symbol-50 symbol-lg-120 symbol-light-danger">
                                                        <img src="http://kabayanconsulting.co.id:3002/talent-review/public/asset-mt/media/svg/avatars/007-boy-2.svg"
                                                            alt="image" class=""
                                                            style="width:100%; max-height:250px; object-fit: cover; object-position: 100% 0%;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-lg-6 col-md-12">
                                                <small>
                                                    Chandra pradyot singh
                                                    <br>74198047
                                                    <br>SVP-Head of Sales Planning &amp; Automation
                                                    <br>Indian
                                                </small>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="rating">
                                    <td rowspan="4" class=" align-middle font-size-sm text-warning">
                                        Performance Rating
                                    </td>
                                </tr>
                                <tr class="rating">
                                    <td class="font-size-sm align-middle text-warning">2019</td>
                                    <td colspan="2" class="text-center">
                                        3
                                    </td>
                                    <td colspan="2" class="text-center">
                                        4
                                    </td>
                                    <td colspan="2" class="text-center">
                                        4
                                    </td>
                                </tr>
                                <tr class="rating">
                                    <td class="font-size-sm align-middle text-warning">2020</td>
                                    <td colspan="2" class="text-center">
                                        4
                                    </td>
                                    <td colspan="2" class="text-center">
                                        4
                                    </td>
                                    <td colspan="2" class="text-center">
                                        4
                                    </td>
                                </tr>
                                <tr class="rating">
                                    <td class="font-size-sm align-middle text-warning">2021</td>
                                    <td colspan="2" class="text-center">
                                        -
                                    </td>
                                    <td colspan="2" class="text-center">
                                        -
                                    </td>
                                    <td colspan="2" class="text-center">
                                        -
                                    </td>
                                </tr>

                                <tr class="mapping">
                                    <td rowspan="3" class=" align-middle font-size-sm text-danger">
                                        Talent Mapping
                                    </td>
                                </tr>
                                <tr class="mapping">
                                    <td class="font-size-sm align-middle text-danger">2021</td>
                                    <td colspan="2" class="justify-content-center">
                                        <table class="table-sm" style="margin:auto; border:none !important">
                                            <tbody>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px; height: 20px;text-align:center; border:1px solid #FFF">
                                                            4
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #02ad2a; width: 20px; height: 20px; text-align:center; border:1px solid #FFF">
                                                            7
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style=" background-color: #000;  width: 20px; height: 20px; text-align:center; border:1px solid #FFF">
                                                            9
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            2
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            5
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            8
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            1
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            3
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            6
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2" class="justify-content-center">
                                        <table class="table-sm" style="margin:auto; border:none !important">
                                            <tbody>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            4
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #02ad2a; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            7
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            9
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            2
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            5
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            8
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            1
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            3
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            6
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2" class="justify-content-center">
                                        <table class="table-sm" style="margin:auto; border:none !important">
                                            <tbody>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            4
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #02ad2a; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            7
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            9
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            2
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            5
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            8
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            1
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            3
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            6
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="mapping">
                                    <td class="font-size-sm align-middle text-danger">2022</td>
                                    <td colspan="2" class="justify-content-center">
                                        <table class="table-sm" style="margin:auto; border:none !important">
                                            <tbody>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            4
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #02ad2a; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            7
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            9
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            2
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            5
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            8
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            1
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            3
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            6
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2" class="justify-content-center">
                                        <table class="table-sm" style="margin:auto; border:none !important">
                                            <tbody>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            4
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #02ad2a; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            7
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            9
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            2
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            5
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            8
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            1
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            3
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            6
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2" class="justify-content-center">
                                        <table class="table-sm" style="margin:auto; border:none !important">
                                            <tbody>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            4
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #02ad2a; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            7
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            9
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            2
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            5
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            8
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:none !important">
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            1
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            3
                                                        </div>
                                                    </td>
                                                    <td style="border:none !important; padding:3px !important">
                                                        <div class="text-white font-weight-bolder label label-square" style="background-color: #000; width: 20px;height: 20px;text-align:center;border:1px solid #FFF">
                                                            6
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="competency" style="display:none">
                                    <td colspan="9" class="align-middle font-size-sm text-success">
                                        Competency
                                    </td>
                                </tr>
                                <tr class="competency" style="display:none">
                                    <td rowspan="2" class=" align-middle font-size-sm text-success">
                                        Leadership
                                    </td>
                                    <td class=" align-middle font-size-sm text-success">Strength</td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Learning &amp; Growth Mindset</b> &nbsp;&nbsp;
                                        <br>
                                        I am capable to learn everything in various condition even in bad condition, and I may pick
                                        things that is belief to grew. And I keep on that, doing everything disciplinary based on
                                        road mad of plan. For several cases, I have to go thru some structural way, but in most
                                        important case after learning in these years we can do better by having special task force
                                        (even to tribe) to shorten the time.<br>
                                        <br>
                                        We can do, when we believe so. There should be something can be done, doing something is
                                        still better than waiting.
                                        <br><br>
                                    </td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Change Agility</b> &nbsp;&nbsp;
                                        <br>
                                        able to identify core strength/weakness of the team member, tailor the work relation
                                        accordingly
                                        <br><br>
                                    </td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Ownership, Drive &amp; Engagement</b> &nbsp;&nbsp;
                                        <br>
                                        Objective, Goal Setting, Communication, Trust, Care, Courage, Alignment (building consensus
                                        with focus and clarity), Engagement (Right people involved), Ownership (knowing what, when
                                        and by who)
                                        <br><br>
                                        <b>Change Agility</b> &nbsp;&nbsp;
                                        <br>
                                        Purpose (Why Change), Process (new ways of doing things), People (equip with change),
                                        Performance (right measures for success)
                                        <br><br>
                                        <b>Learning &amp; Growth Mindset</b> &nbsp;&nbsp;
                                        <br>
                                        Desire to Learn, embrace challenges, find inspiration in others success, develop new skills,
                                        <br><br>
                                    </td>
                                </tr>
                                <tr class="competency" style="display:none">
                                    <td class=" align-middle font-size-sm text-success">Development</td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Effective Decision Making</b> &nbsp;&nbsp;
                                        <br>
                                        World is changed. The way of working is also changed. I should run as fas as the changes
                                        happens. The mechanism of making decision that I always do before should be changed. Last
                                        time I may call everybody to one room and discuss there in detail until I am positive of one
                                        decision. Now I should occupy myself with tools that can help me to make effective decision.
                                        Using the data that I completely have, self experience so for, online ‘call a friend’ as my
                                        online coordination with my subordinate and other colleagues, one system to simulate before
                                        I decide something.<br>
                                        <br>
                                        The devil is not only in detail, but when we decide something without complete understanding
                                        and input from relevant parties.
                                        <br><br>
                                    </td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Learning &amp; Growth Mindset</b> &nbsp;&nbsp;
                                        <br>
                                        in some situation day to day job take out focus from creating a new vision and direction for
                                        the group
                                        <br><br>
                                    </td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Customer Focus</b> &nbsp;&nbsp;
                                        <br>
                                        need to have an option of others here to submit the key....
                                        <br><br>
                                    </td>
                                </tr>
                                <tr class="competency" style="display:none">
                                    <td rowspan="2" class=" align-middle font-size-sm text-success">
                                        Technical
                                    </td>
                                    <td class=" align-middle font-size-sm text-success">Strength</td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Agile Project Management</b> &nbsp;&nbsp;
                                        <br>
                                        I understand is not easy to execute one project. Seem there are so many obstacle around. But
                                        by doing agile project management, we can apply method that is more agile and flexible that
                                        fit to member. But we have to put strong point on it, timely manner and number. Whatever we
                                        say and do, should reflect to numbers, dashboard can help. Alert system can also support to
                                        remind us in every turn and limit.<br>
                                        <br>
                                        Agile and flexible doesn’t mean that we are not discipline, even we are more.
                                        <br><br>
                                    </td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Sales Planning &amp; Analysis</b> &nbsp;&nbsp;
                                        <br>
                                        Always use data as basis for decision making and able to connect the dots
                                        <br><br>
                                    </td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Sales Planning &amp; Analysis</b> &nbsp;&nbsp;
                                        <br>
                                        Sales Strategy, Forecasting, Sales Quality and Revenue, Product GTM, Margin &amp; commission
                                        structure, Trade Insights
                                        <br><br>
                                        <b>Application Development and Management </b> &nbsp;&nbsp;
                                        <br>
                                        Vision, Research, Design, Execute, Analyze, Vendor Management, Budget control
                                        <br><br>
                                        <b>Business Insights</b> &nbsp;&nbsp;
                                        <br>
                                        Understanding of situation with combination of data and analysis, lead in decision making
                                        <br><br>
                                    </td>
                                </tr>
                                <tr class="competency" style="display:none">
                                    <td class=" align-middle font-size-sm text-success">Development</td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Agile Project Management</b> &nbsp;&nbsp;
                                        <br>
                                        I understand is not easy to execute one project. Seem there are so many obstacle around. But
                                        by doing agile project management, we can apply method that is more agile and flexible that
                                        fit to member. But we have to put strong point on it, timely manner and number. Whatever we
                                        say and do, should reflect to numbers, dashboard can help. Alert system can also support to
                                        remind us in every turn and limit.<br>
                                        <br>
                                        Agile and flexible doesn’t mean that we are not discipline, even we are more.
                                        <br><br>
                                    </td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Sales Planning &amp; Analysis</b> &nbsp;&nbsp;
                                        <br>
                                        Always use data as basis for decision making and able to connect the dots
                                        <br><br>
                                    </td>
                                    <td colspan="2" class="text-center font-size-xs" style="width:25%;">
                                        <b>Sales Planning &amp; Analysis</b> &nbsp;&nbsp;
                                        <br>
                                        Sales Strategy, Forecasting, Sales Quality and Revenue, Product GTM, Margin &amp; commission
                                        structure, Trade Insights
                                        <br><br>
                                        <b>Application Development and Management </b> &nbsp;&nbsp;
                                        <br>
                                        Vision, Research, Design, Execute, Analyze, Vendor Management, Budget control
                                        <br><br>
                                        <b>Business Insights</b> &nbsp;&nbsp;
                                        <br>
                                        Understanding of situation with combination of data and analysis, lead in decision making
                                        <br><br>
                                    </td>
                                </tr>
                                <tr class="aspiration" style="display:none">
                                    <td colspan="2" rowspan="2" class="align-middle font-size-sm text-primary">
                                        Career Aspiration
                                    </td>
                                    <td colspan="2" class="justify-content-center font-size-xs text-center">
                                        In the next 5 years , Fuli wants to be at the top position with bigger areas of
                                        responsibility.<br>
                                        Aspired to be a leader who make things happen and helps company to become competitive yet
                                        sustainable.
                                    </td>
                                    <td colspan="2" class="justify-content-center font-size-xs text-center">
                                        To be assigned to SVP Digital
                                    </td>
                                    <td colspan="2" class="justify-content-center font-size-xs text-center">
                                        Move to next level vertically as CSDO OR horizontally in Marketing as Head of Core Product
                                    </td>
                                </tr>
                                <tr class="aspiration" style="display:none">
                                    <td colspan="2">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td class="justify-content-center font-size-xs text-center">Willing To Be
                                                        Rotated Abroad</td>
                                                    <td class="justify-content-center font-size-xs text-center">Open For Cross
                                                        Directorate Rotation </td>
                                                    <td class="justify-content-center font-size-xs text-center">Open For Rotation to
                                                        Subsidiary </td>
                                                    <td class="justify-content-center font-size-xs text-center">Others</td>
                                                </tr>
                                                <tr>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        0
                                                    </td>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        0
                                                    </td>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        0
                                                    </td>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        I Am Open To Rotated Within CBO Or Other Directorate Which Ia Good For
                                                        Indosat Objective.
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td class="justify-content-center font-size-xs text-center">Willing To Be
                                                        Rotated Abroad</td>
                                                    <td class="justify-content-center font-size-xs text-center">Open For Cross
                                                        Directorate Rotation </td>
                                                    <td class="justify-content-center font-size-xs text-center">Open For Rotation to
                                                        Subsidiary </td>
                                                    <td class="justify-content-center font-size-xs text-center">Others</td>
                                                </tr>
                                                <tr>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        0
                                                    </td>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        0
                                                    </td>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        0
                                                    </td>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        To Be Assigned To SVP Digital
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td class="justify-content-center font-size-xs text-center">Willing To Be
                                                        Rotated Abroad</td>
                                                    <td class="justify-content-center font-size-xs text-center">Open For Cross
                                                        Directorate Rotation </td>
                                                    <td class="justify-content-center font-size-xs text-center">Open For Rotation to
                                                        Subsidiary </td>
                                                    <td class="justify-content-center font-size-xs text-center">Others</td>
                                                </tr>
                                                <tr>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        Willing To Relocate In Other OPCO If The Role Of CSDO Or In Marketing Core
                                                        Product Or In Digital Care / Services
                                                    </td>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        Want To Continue In B2C / Commercial Function
                                                    </td>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        Want To Continue In B2C / Commercial Function
                                                    </td>
                                                    <td class="justify-content-center font-size-xs text-center">
                                                        No
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="planning" style="display:none">
                                    <td colspan="2"></td>
                                    <td class="font-size-sm text-center align-middle text-info">Career Planning</td>
                                    <td class="font-size-sm text-center align-middle text-info">Employee Development</td>
                                    <td class="font-size-sm text-center align-middle text-info">Career Planning</td>
                                    <td class="font-size-sm text-center align-middle text-info">Employee Development</td>
                                    <td class="font-size-sm text-center align-middle text-info">Career Planning</td>
                                    <td class="font-size-sm text-center align-middle text-info">Employee Development</td>
                                </tr>
                                <tr class="planning" style="display:none">
                                    <td rowspan="6" class=" align-middle font-size-sm text-info">
                                        Plan &amp; Development
                                    </td>
                                </tr>
                                <tr class="planning" style="display:none">
                                    <td class="font-size-sm align-middle text-info">2022</td>
                                    <td class="text-center font-size-xs">
                                        Leads Sales Function for Public Sector or FSC segment
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Coaching for Business Development
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Head of Region
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Strategic orientation
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Head of core product
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Analytical skill, business&nbsp;planning&nbsp;
                                    </td>
                                </tr>
                                <tr class="planning" style="display:none">
                                    <td class="font-size-sm align-middle text-info">2023</td>
                                    <td class="text-center font-size-xs">
                                        Leads Sales Function for Public Sector or FSC segment
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Coaching for Executive
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Head of marketing
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Strategic orientation
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Head of Sales
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Leadership ability
                                    </td>
                                </tr>
                                <tr class="planning" style="display:none">
                                    <td class="font-size-sm align-middle text-info">2024</td>
                                    <td class="text-center font-size-xs">
                                        Director in Subsidiary
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Building Organizational Capability &amp; Talent
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Take rest from job
                                    </td>
                                    <td class="text-center font-size-xs">

                                    </td>
                                    <td class="text-center font-size-xs">
                                        Head of Sales
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Leadership ability
                                    </td>
                                </tr>
                                <tr class="planning" style="display:none">
                                    <td class="font-size-sm align-middle text-info">2025</td>
                                    <td class="text-center font-size-xs">
                                        Director in Subsidiary
                                    </td>
                                    <td class="text-center font-size-xs">
                                        CEO Academy
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Take rest from job
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Leadership ability
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Head of sales
                                    </td>
                                    <td class="text-center font-size-xs">
                                        Leadership ability
                                    </td>
                                </tr>
                                <tr class="planning" style="display:none">
                                    <td class="font-size-sm align-middle text-info">2026</td>
                                    <td class="text-center font-size-xs">
                                        -
                                    </td>
                                    <td class="text-center font-size-xs">
                                        -
                                    </td>
                                    <td class="text-center font-size-xs">
                                        -
                                    </td>
                                    <td class="text-center font-size-xs">
                                        -
                                    </td>
                                    <td class="text-center font-size-xs">
                                        -
                                    </td>
                                    <td class="text-center font-size-xs">
                                        -
                                    </td>
                                </tr>
                                <tr class="target-position" style="display:none">
                                    <td colspan="2" class=" align-middle font-size-sm text-muted">
                                        Targeted Position
                                    </td>
                                    <td colspan="2" class="justify-content-center font-size-xs text-center">
                                        CEO in Subsidiary or CBO
                                    </td>
                                    <td colspan="2" class="justify-content-center font-size-xs text-center">
                                        Head of Region
                                    </td>
                                    <td colspan="2" class="justify-content-center font-size-xs text-center">
                                        Chief in other Opco,&nbsp;Company&nbsp;?
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link href="{{asset('plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<style>
    @media only screen and (max-width: 600px) {
        .d-flex {
            flex-direction: column;
            flex-flow: wrap;
            justify-content: space-between;
        }
        .d-flex > .btn {
            margin: 4px;
        }
    }
    
    .container-customeHeight {
        height: 200px;
    }

    .label-custome {
        padding: 6px 8px;
        width: 40px;
        height: 40px;
        justify-content: center;
        margin: 0;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        border-radius: 50%;
        background-color: #EBEDF3;
        font-weight: 900!important;
        color: black!important;
        font-size: 3rem!important;
    }
    .content {
        overflow-x: auto;
    }
    .overlay {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
    }
    .blockOverlay {
        background-color: transparent!important
    }
    
    .placeHold{
        border: 1px dashed blue;
        background-color: #c7c7c7;
        height:40px;
        width:228px;
        list-style-type:none;
    }

    .symbol-label-custome {
        width:35px!important;
        height:25px!important;
    }
    
    .facet {
        width:228px;
        height:40px;
        margin-right: 2px;
        display:flex;
        overflow-x:hidden;
        flex:auto;
    }  

    .blockOverlay {
        background-color: transparent!important
    }

    .master-facet {
        list-style-type:none;
        min-width:100%;
        flex-wrap: wrap;
        flex-direction: row;
        overflow-y: auto;
        /* height: 180px; */
        place-content: flex-start;
        /* flex-flow: column; */
    }

    .master-facet-bigger {
        list-style-type:none;
        min-width:100%;
        flex-wrap: wrap;
        flex-direction: row;
        overflow-x: auto;
        overflow-y: auto;
        height: 214px;
        /* flex-flow: column; */
    }

    .xxxx {        
        list-style-type:none;
        overflow: auto;
        flex-flow: wrap;
        flex-direction: row;
        flex-wrap: wrap;
        flex-basis: auto;
        display: flex;
        align-items: flex-start;
        height: 228px;        
    }
</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
<script src="{{asset('js/pages/crud/forms/widgets/select2.js')}}"></script>
<script src="{{asset('plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
@endsection
