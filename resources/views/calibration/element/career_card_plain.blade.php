<div class="card card-custom gutter-b" style="zoom:0.85">
    <div class="card-body">
        <div class="d-flex">
            <table class="tg" style="width:100%;">
                <thead>
                    <tr>
                        <th class="tg-0pky" colspan="8">
                            <h3>{{ strtoupper(strtolower($employee->personnel_number)) }}</h3>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tg-0pky" rowspan="8" valign="top" style="width:12em">
                            <div class="symbol symbol-50 symbol-lg-120 symbol-light-danger">
                                <img src="{{ get_employee_pict($employee->prev_persno) }}" alt="image" class="" style="width:100%; max-height:250px; object-fit: cover; object-position: 100% 0%;" />
                            </div>
                        </td>
                        <td class="font-weight-bolder" valign="top" style="width:20em">CURRENT POSITION</td>
                        <td class="font-weight-bolder" valign="top" style="width:15em">NATIONALITY</td>
                        <td class="font-weight-bolder" valign="top" style="min-width:fit-content;" rowspan=7>
                            <div class="d-flex justify-content-between pb-4">
                                <div class="" style="background:black; color:white;max-height:150px;width:45%">
                                    <div class="pt-2 text-center font-weight-bolder">PERFORMANCE RATINGS</div>
                                    <div>
                                        <table style="width:100%;display:inline-table">
                                            <tr>
                                                @for ($i = date('Y') - 3; $i < date('Y'); $i++)
                                                    <td class="text-center font-weight-boldest text-center">{{ $i }}</td>
                                                @endfor
                                            </tr>
                                            <tr>
                                                @for ($i = date('Y') - 3; $i < date('Y'); $i++)
                                                    <td rowspan=2 class="pt-8 text-center font-weight-boldest">
                                                        <h1> 0 </h1>
                                                    </td>
                                                @endfor
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="width:2%">&nbsp;</div>
                                <div class="pb-4" style="background:black; color:white;max-height:150px;width:53%">
                                    <div class="pt-2 text-center font-weight-bolder">TALENT MAPPING BOX</div>
                                    <div class="d-flex justify-content-around">
                                        <!-- <table class="lpanel" style="zoom:0.8;"></table>
                                        <table class="cpanel" style="zoom:0.8"></table> -->
                                        <table class="align-items-center" style="width:100%;display:contents;zoom:1.2">
                                            <tr>
                                                @for ($i = date('Y') - 1; $i <= date('Y'); $i++)
                                                    <td class="text-center {{ ($i != date('Y')) ? 'pr-8' : '' }} font-weight-boldest">{{ $i }}</td>
                                                @endfor
                                            </tr>
                                            <tr>
                                                @for ($i = date("Y") - 1; $i <= date('Y'); $i++)
                                                    <td class="text-center {{ ($i != date('Y')) ? 'pr-8' : '' }} ">
                                                        
                                                    </td>
                                                @endfor
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="tg-0pky" valign="top">{{ $employee->position_name }}</td>
                        <td class="tg-0pky" valign="top">{{ ucwords(strtolower($employee->nationality)) }}</td>
                    </tr>

                    <tr>
                        <td class="pt-6 font-weight-bolder" valign="bottom">YOG</td>
                        <td class="pt-6 font-weight-bolder" valign="bottom">YOS</td>
                    </tr>

                    <tr>
                        <td class="tg-0pky" valign="top">
                            -
                        </td>
                        <td class="tg-0pky" valign="top">
                            -
                        </td>
                    </tr>

                    <tr>
                        <td class="pt-6 font-weight-bolder" colspan="2" valign="bottom">PREVIOUS EXPERIENCE</td>
                    </tr>

                    <tr>
                        <td class="tg-0pky" colspan="2">
                            <ol class="pl-4">
                                <li class=" font-size-sm">AVP-Head of Facilities SO - Central Java</li>
                                <li class=" font-size-sm">Facilities SO - Cent. Java & DIY Manager</li>
                            </ol>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card card-custom gutter-b">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label">EMPLOYEE COMPETENCY PROFILE</h3>
        </div>
        <div class="card-toolbar">
            <a href="javascript:void(0)" class="btn btn-icon btn-sm btn-light-primary mr-1" id="kt_card_skill_profile">
            <i class="ki ki-arrow-down icon-nm"></i>
            </a>
        </div>
    </div>
    <div class="card-body kt_card_skill_profile_body">
        <div class="d-flex">
            <table class="table table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>COMPETENCY</th>
                        <th width="45%"class="text-danger">AREA OF STRENGTH</th>
                        <th colspan="2" class="text-primary">AREA OF DEVELOPMENT</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="text-center text-success">BEHAVIORAL</th>
                        <td class="bg-light-danger aosl-panel">
                            <b>Lorem ipsum dolor sit amet</b> &nbsp;&nbsp;
                            <br>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quasi quam, quisquam obcaecati tenetur nihil. Sit eius facilis rem consequatur! Corporis iste magni vero similique recusandae. Doloremque non dicta nesciunt!
                            <br><br>
                        </td>
                        <td  colspan="2" class="bg-light-primary aodl-panel">
                            <b>Lorem ipsum dolor sit amet</b> &nbsp;&nbsp;
                            <br>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quasi quam, quisquam obcaecati tenetur nihil. Sit eius facilis rem consequatur! Corporis iste magni vero similique recusandae. Doloremque non dicta nesciunt!
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center text-info">TECHNICAL</th>
                        <td class="bg-light-info aost-panel">
                            <b>Lorem ipsum dolor sit amet</b> &nbsp;&nbsp;
                            <br>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quasi quam, quisquam obcaecati tenetur nihil. Sit eius facilis rem consequatur! Corporis iste magni vero similique recusandae. Doloremque non dicta nesciunt!
                            <br><br>
                        </td>
                        <td  colspan="2" class="bg-light-success aodt-panel">
                            <b>Lorem ipsum dolor sit amet</b> &nbsp;&nbsp;
                            <br>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quasi quam, quisquam obcaecati tenetur nihil. Sit eius facilis rem consequatur! Corporis iste magni vero similique recusandae. Doloremque non dicta nesciunt!
                            <br><br>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card card-custom gutter-b">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label">DEVELOPMENT PLANNING</h3>
        </div>
        <div class="card-toolbar">
            <a href="javascript:void(0)" id="kt_card_employee_career" class="btn btn-icon btn-sm btn-light-primary mr-1">
            <i class="ki ki-arrow-down icon-nm"></i>
            </a>
        </div>
    </div>
    <div class="card-body kt_card_employee_career_body">
        <div class="journel-panel" style="overflow-x:auto">
            <div class="row">
                <table class="table table-bordered table-sm">
                    <tr>
                        <td style="width: 200px" class="font-weight-bolder text-center">CAREER ASPIRATION</td>
                        <td colspan="6" class="text-left">
                            <section class="aspiration-text">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptates obcaecati totam nobis cupiditate voluptatibus ut mollitia, architecto perferendis et quidem doloremque dicta harum ex similique tenetur optio repellendus pariatur consequatur?
                            </section>
                        </td>
                    </tr>
                </table>
                <table class="table table-bordered table-sm">
                    <tr>
                        <td style="width: 200px" class="text-center align-middle font-weight-bolder">Employee Development</td>
                        @for ($i = date('Y') ; $i < date('Y') + 5; $i++)
                            <td class="align-middle text-center">
                                {{$i}}
                            </td>
                        @endfor
                        <td class="text-center align-middle">Targeted Position</td>
                    </tr>
                    <tr>
                        <td class="text-center align-middle">Career Planning</td>
                        @for ($i = date('Y') ; $i < date('Y') + 5; $i++)
                        <td class="align-middle text-center">
                            -
                        </td>
                        @endfor
                        <td class="text-center align-middle" rowspan="2">
                            -
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center align-middle">Development Program</td>
                        @for ($i = date('Y') ; $i < date('Y') + 5; $i++)
                            <td class="align-middle text-center">
                                -
                            </td>
                        @endfor
                    </tr>
                </table>
            </div>
            <div class="row">
                <table class="table table-bordered table-sm" style="width:100%">
                    <thead class="text-center">
                        <tr>
                            <th style="width:25%" class="font-weight-bolder">Willing To Be Rotated Abroad</th>
                            <th style="width:25%" class="font-weight-bolder">Open For Cross Directorate Rotation </th>
                            <th style="width:25%" class="font-weight-bolder">Open For Rotation to Subsidiary </th>
                            <th style="width:25%" class="font-weight-bolder">Others</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>