@if(!empty($data))
    @foreach($data as $key => $value)
        <li class="facet mb-1" data-nik="{{ Crypt::encryptString($value->prev_persno) }}" style="list-style-type:none">
            <div class="badge badge-light-secondary font-weight-bold" style="text-align:left !important;" 
                data-toggle="tooltip" data-theme="dark" title="{{ strtoupper($value->personnel_number) }}">
                <div class="d-flex align-items-center ttip">
                    @if(!$fs)
                        <div class="symbol symbol-50 symbol-light mr-4">
                            <span class="symbol-label symbol-label-custome compare-mark compare-{{ $value->prev_persno }}" data-nik="{{ $value->prev_persno }}" data-nik-enc="{{ Crypt::encryptString($value->prev_persno) }}" style="cursor:pointer;">
                                <i class="fa fa-check text-success float-right overlay check-compare" style="display:none;"></i>
                                <img src="{{ get_employee_pict($value->prev_persno) }}" class="h-75 align-self-end" alt="">
                                
                            </span>
                        </div>
                        <h6>
                            <a href="javascript:void(0);" class="open-career-card {{ $panel != 0 ? 'text-white' : 'text-dark' }}" data-nik="{{ Crypt::encryptString($value->prev_persno) }}">
                                @if(!empty($value->is_box0) > 0)
                                    @if($value->is_box0 == '1')
                                        <b class="text-danger">{{ $value->is_box0 == '1' ? 0 : '' }}</b>
                                    @else
                                    @endif
                                @else
                                @endif
                                <b>{{ strtoupper($value->personnel_number) }}</b>
                            </a>
                        </h6>
                    @else
                        <span>
                            <a href="javascript:void(0);" class="open-career-card {{ $panel != 0 ? 'text-white' : 'text-dark' }}" data-nik="{{ Crypt::encryptString($value->prev_persno) }}">
                                <b>{{ strtoupper($value->personnel_number) }}</b>
                            </a>
                        </span>
                    @endif
                </div>
            </div>
        </li>
    @endforeach
@endif
