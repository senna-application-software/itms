{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div id="code-subholding" data-code="{{ $code_subholding }}"></div>
<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">List Organisasi</h3>
        </div>
    </div>

    <div class="card-body">
        <table class="table table-bordered table-hover table-checkable" id="table-organization" style="margin-top: 13px !important">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Subholding</th>
                    <th>Direktorat</th>
                    <th>Group</th>
                    <th>Divisi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
{{-- Styles Section --}}

{{-- Styles Section --}}
@section('styles')
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    $('#table-organization').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! url("/struktur-organisasi/datatable") !!}',
            data: function(d) {
                    d.code_subholding = $('#code-subholding').data("code")
                }
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'subholding', name: 'subholding'},
            { data: 'direktorat', name: 'direktorat'},
            { data: 'group', name: 'group'},
            { data: 'divisi', name: 'divisi'},
            { data: 'button', name: 'button'},
        ]
    });
</script>
@endsection