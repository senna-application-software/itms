{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Detail Organisasi</h3>
        </div>
    </div>

    <div class="card-body">
        <div id="inJourneyDiagram" class="diagram-entry"></div>
    </div>
</div>
@endsection
{{-- Styles Section --}}

@section('styles')
<style type="text/css" id="diagramStylesExport">
    #inJourneyDiagram {
        width: 100%;
        height: 100%;
    }

    .node rect {
        text: blue;
        stroke: transparent;
        fill: #57BFC4;
    }

    table td {
        padding: 10px;
    }

    .node text {
        fill: #fff
    }

    .path {
        stroke: black !important;
        stroke-width: 3px;
    }

    #bg-ppdf-btns {
        min-width: 270px;
        font-family: ooredoo, Segoe UI, Roboto, Helvetica, Arial, sans-serif;
    }

    foreignObject {
        font-size: 18px;
        color: white;
    }

    .node.directorate rect {
        fill: #584f4f;
    }

    .node.chief rect {
        fill: #528dd5;
    }
    
    .node.group rect {
        fill: #92d050;
    }
    
    .node.division rect {
        fill: #ffc000;
    }

    .node.department rect {
        fill: #ff98ff;
    }

    .node.expert rect {
        fill: #6363f0;
    }

    .node.staff rect {
        fill: #8800ff;
    }
</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('js/organization/orgchart.js') }}"></script>
<script>
    var chart;
    var struktur = {!! $data !!};

    chart = new OrgChart(document.getElementById("inJourneyDiagram"), {
        lazyLoading: true,
        template:   "olivia",
        enableDragDrop:true,
        siblingSeparation: 50,
        scaleInitial:.75,
        toolbar:!0,
            collapse: {
            level: 2,
            allChildren: true
        },
        menu: {
            pdfPreview: { 
                text: "PDF Preview", 
                icon: OrgChart.icon.pdf(24,24, '#7A7A7A'),
                onClick: preview
            },
            pdf: { text: "Export PDF" },
            png: { text: "Export PNG" },
            svg: { text: "Export SVG" },
            csv: { text: "Export CSV" }
        },
        nodeMenu: {
            pdf: { text: "Upload Job Desc" },
            png: { text: "Export PNG" },
            svg: { text: "Export SVG" }
        },
        nodeBinding: {
            field_0: "Job Holder",
            field_1: "Position",
            field_2: "Number of Successor",

            img_0: "img"
        },
        nodes: struktur
    });

    function preview(){
        OrgChart.pdfPrevUI.show(chart, {
            format: 'A4'
        });
    }
</script>
@endsection