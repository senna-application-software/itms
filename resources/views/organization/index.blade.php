{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    @if(count($list_subholding) > 0)
        @foreach($list_subholding as $key => $val)
            @if($val->code_subholding == "inj")
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-sm-12 container-member offset-xl-4">
                        <a href="{{ route('struktur-organisasi.subholding', ['code_subholding' => $val->code_subholding]) }}">
                            <div class="card card-custom bgi-no-repeat gutter-b" style="height: 180px;">
                                <div class="card-body d-flex align-items-center">
                                    <div class="row">
                                        <div class="col">
                                            <h4 class="font-weight-bolder">{{ $val->name_subholding }}</h4>
                                        </div>
                                        <div class="col">
                                            <img class="logo-member" style="max-width:150px" src="{{ asset('images/logo-member/'. $val->logo )}}" alt="image">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            @endif
        @endforeach

        <div class="row">
            @foreach($list_subholding as $key => $val)
                @if($val->code_subholding != "inj")
                    <div class="col-xl-4 col-md-6 col-sm-12 container-member">
                        <a href="{{ route('struktur-organisasi.subholding', ['code_subholding' => $val->code_subholding]) }}">
                            <div class="card card-custom bgi-no-repeat gutter-b" style="height: 180px;">
                                <div class="card-body d-flex align-items-center">
                                    <div class="row">
                                        <div class="col ">
                                            <h6 class="font-weight-bolder">{{ $val->name_subholding }}</h6>
                                        </div>
                                        <div class="col overflow-hidden">
                                            <img class="logo-member" style="max-width:150px" src="{{ asset('images/logo-member/'. $val->logo )}}" alt="image">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endif
            @endforeach
        </div>
    @else
        <div class="col-sm-12">
            <div class="alert alert-custom alert-danger" role="alert">
                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                <div class="alert-text">Tidak ada Data!</div>
            </div>
        </div>
    @endif
@endsection
{{-- Styles Section --}}

{{-- Styles Section --}}
@section('styles')
    <style>
        img.logo-member {
            filter: grayscale(100%);
        }

        .container-member:hover div img.logo-member{
            filter: grayscale(0%);
        }
    </style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
@endsection