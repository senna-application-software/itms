@push('scripts')

<script>
    //fungsi click score card
    $(document).on("click",".open-career-card", function (e) {
        e.preventDefault();
        var id = $(this).data('id')
        var url = "{{ url('talent-profiling/view-career-card') }}" + '/' + $(this).data("nik")

        $.ajax({
            url     : url,
            type    : 'GET',
            data    : {
                sub_event: "{{ $sub_event->id ?? $sub_event }}",
                is_editable: "{{ $is_editable ?? 0}}",
                is_talent_map: "{{ $is_talent_map ?? 0}}",
                talentAspirationId: e.target.getAttribute('data-id')
            },
            success : function (response) {
                $('body #xl-modal').modal('show');
                $('.data-modal').html(response)
            }
        })
    });

    $(document).on("click",".open-career-card-plain", function (e) {
        e.preventDefault();
        var id = $(this).data('id')
        //modal-eqs-dasar
        var url = "{{ url('aspirations/modal-eqs-dasar') }}" + '/' + $(this).data("nik")
        $.ajax({
            url     : url,
            type    : 'GET',
            success : function (response) {
                $('body #xl-modal').modal('show');
                $('.data-modal').html(response)
            }
        })
    })

    $(document).on("click", "body #kt_card_skill_profile", function() {
        $(".kt_card_skill_profile_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_employee_career", function() {
        $(".kt_card_employee_career_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_personal_information", function() {
        $(".kt_card_personal_information_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_hukdis", function() {
        $(".kt_card_hukdis_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_org_memb", function() {
        $(".kt_card_org_memb_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_rwd", function() {
        $(".kt_card_rwd_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_pnd_pel", function() {
        $(".kt_card_pnd_pel_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_kry_tls", function() {
        $(".kt_card_kry_tls_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_exp_spk_judg", function() {
        $(".kt_card_exp_spk_judg_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_refs", function() {
        $(".kt_card_refs_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_fams", function() {
        $(".kt_card_fams_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_exp_and_master", function() {
        $(".kt_card_exp_and_master_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_exp_klstr_jbtn", function() {
        $(".kt_card_exp_klstr_jbtn_body").slideToggle()
    });

    $(document).on("click", "body #kt_card_asp_klstr_jbtn", function() {
        $(".kt_card_asp_klstr_jbtn_body").slideToggle()
    });


    function careerCard(url, data, is_down) {
        $.ajax({
            url     : url,
            type    : 'GET',
            data    : {
                sub_event: "{{ $sub_event->id ?? $sub_event }}",
                is_editable: "{{ $is_editable ?? 0}}"
            },
            success : function (response) {
                console.log(is_down);
                let min_performance = (data[0].min_performance - 20 > 0) ? data[0].min_performance - 20 : 0;
                let max_performance = (data[0].max_performance - 20 > 0) ? data[0].max_performance - 20 : 0;
                $('body #xl-modal').modal('show');
                $('.data-modal').html(response)
                $("#kinerjaPerformance").attr('min', min_performance);
                $("#kinerjaPerformance").attr('max', max_performance);
                $("#cci").attr('min', data[0].min_cci);
                $("#cci").attr('max', data[0].max_cci);
                if (is_down) {
                    $("#kinerjaPerformance").val(max_performance);
                    $("#cci").val(data[0].max_cci);
                }else {
                    $("#kinerjaPerformance").val(min_performance);
                    $("#cci").val(data[0].min_cci);
                }
                let kinerja_value = $("#kinerjaPerformance").val();
                let track_record_value = $("#trackRecordPerformance").val();
                let performance_index = parseInt(kinerja_value) + parseInt(track_record_value)
                $("#performance").val(performance_index);
                updateTalentCluster()
            }
        })
    }
    var rangeable = 0;
</script>
@if (isset($is_talent_map))
<script>
    $("#xl-modal").on('shown.bs.modal', function () {
        let min_pref = parseInt($("#kinerjaPerformance").attr('min')) + 20;
        let max_pref = parseInt($("#kinerjaPerformance").attr('max')) + 20;
        let label_pref = `Performance: ${min_pref} - ${max_pref}`;
        $("#performance").attr("min", min_pref)
        $("#performance").attr("max", max_pref)
        $(".range-performance-index").html(label_pref);
        let min_cci = $("#cci").attr('min');
        let max_cci = $("#cci").attr('max');
        let label_cci = `Assessment: ${min_cci} - ${max_cci}`;
        $(".range-assesment").html(label_cci);
        $(".range-info").show();
    })
    rangeable = 1;
</script>
@endif
@if($is_editable ?? false)
<script>
    $(document).on("keydown", "#kinerjaPerformance", function () {
        if (!$(this).val() || (parseInt($(this).val()) <= parseInt($(this).attr('max')) && parseInt($(this).val()) >= 0))
        $(this).data("old", $(this).val());
        let total = parseInt(($(this).val() == "") ? 0 : $(this).val()) + parseInt($("#trackRecordPerformance").val());
        $("#performance").val(total);
    });
    $(document).on("keyup", "#kinerjaPerformance", function () {
        // Check correct, else revert back to old value.
        if (!$(this).val() || (parseInt($(this).val()) <= parseInt($(this).attr('max')) && parseInt($(this).val()) >= 0))
        ;
        else
        $(this).val($(this).data("old"));
        let total = parseInt(($(this).val() == "") ? 0 : $(this).val()) + parseInt($("#trackRecordPerformance").val());
        $("#performance").val(total);
    });
    $(document).on("keydown", "#cci", function () {
        if (!$(this).val() || (parseInt($(this).val()) <= parseInt($(this).attr('max')) && parseInt($(this).val()) >= 0))
        $(this).data("old", $(this).val());
    });
    $(document).on("keyup", "#cci", function () {
        // Check correct, else revert back to old value.
        if (!$(this).val() || (parseInt($(this).val()) <= parseInt($(this).attr('max')) && parseInt($(this).val()) >= 0))
        ;
        else
        $(this).val($(this).data("old"));
    });

    $(document).on("change", "#kinerjaPerformance", function () {
        if (parseInt($(this).val()) >= parseInt($(this).attr("min")) && parseInt($(this).val()) <= parseInt($(this).attr("max"))){
            updateTalentCluster();
            $(".notif-range-performance-index").hide()
        }else {
            $(".notif-range-performance-index").show()
        } 
    });
    $(document).on("change", "#cci", function () {
        if (parseInt($(this).val()) >= parseInt($(this).attr("min")) && parseInt($(this).val()) <= parseInt($(this).attr("max"))){
            updateTalentCluster();
            $(".notif-range-cci").hide()
        }else {
            $(".notif-range-cci").show()
        } 
    });

    function updateTalentCluster() {
        let performance = $("#performance").val();
        let cci = $("#cci").val();
        let kinerjaPerformance = $("#kinerjaPerformance").val();
        let sub_event = $("#subEventModal").val();
        let nik = $("#nikSelectedEmployee").val();
        $.ajax({
            method: "POST",
            url: '{{ url("talent-profiling/get-talent-cluster") }}',
            data: {
                _token: "{{ csrf_token() }}",
                performance: performance,
                cci: cci,
                kinerja: kinerjaPerformance,
                sub_event: sub_event,
                nik: nik
            },
            success: function(res) {
                $("#talent").val(res.predikat)
                $("[name=talent]").val(res.box)
                $("[name=kompetensi]").val(res.eqs.job_fit)
                $("[name=kinerja]").val(res.eqs.kinerja)
                $("#EQSModal").html(`EQS: ${res.eqs_final}`)
                $(`[data-nik-eqs='${res.nik}']`).html(`EQS: ${res.eqs_final}`)
            },
            error: function(error) {
                console.log('error')
            }
        })

    }

    </script>
@endif
@endpush