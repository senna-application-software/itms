@push('scripts')

<script>
    //fungsi click score card
    $(document).on("click",".open-career-card-by-position", function (e) {
        e.preventDefault();
        $('body #xl-modal').modal('show');
        $('.modal-title').html($(this).data("name_position"));

        var url = "{{ route('dashboard.data_eqs_detail_posisi') }}";

        $.ajax({
            url     : url,
            type    : 'GET',
            data    : {
                nik : $(this).data("nik"),
                code_position : $(this).data("code_position")
            },
            success : function (response) {
                $('.data-modal').html(response)
            }
        })
    });
</script>
@endpush