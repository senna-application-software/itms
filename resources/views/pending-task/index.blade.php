{{-- Extends layout --}}
@extends('layout.default')

{{-- Content Section --}}
@section('page_toolbar')
    {{-- kalo di subheader mau ditambahkan button action taruh di sini --}}
@endsection

@section('modal')
    <div class="modal fade" id="modalPosition" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">

    </div>

    <div class="modal fade" id="modalEmployee" data-backdrop="static" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">

    </div>
@endsection

@section('content')
    <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample3">
        <div class="card">
            <div class="card-header" id="headingThree3">
                <div class="card-title justify-content-between" data-toggle="collapse" data-target="#collapseThree3">
                    Position Event for Empty Position
                    <span class="label label-rounded-circle label label-danger mr-5">{{ $pending_task_position }}</span>
                </div>
            </div>
            <div id="collapseThree3" class="collapse show" data-parent="#accordionExample3">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="table-notif">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Position Name</th>
                                    <th>Previous Job Holder</th>
                                    <th>Date Position Vacant</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center"></th> --}}
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="accordion accordion-solid accordion-toggle-plus mt-3" id="accordionExample31">
        <div class="card">
            <div class="card-header" id="headingThree31">
                <div class="card-title justify-content-between" data-toggle="collapse" data-target="#collapseThree31">
                    Position Event for Employee
                    <span class="label label-rounded-circle label label-danger mr-5">{{ $pending_task_employee }}</span>
                </div>
                {{-- <div class="card-toolbar"></div> --}}
            </div>
            <div id="collapseThree31" class="collapse show" data-parent="#accordionExample31">
                <div class="card-body">

                    {{-- <div class="overflow-auto"> --}}
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="table-notif-employee">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Pegawai</th>
                                    <th>Posisi Sebelum</th>
                                    <th>Position by Aspiration</th>
                                    {{-- <th>Candidates</th> --}}
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    {{-- <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center"><button class="btn btn-sm btn-primary" data-toggle="modal"
                                            data-target="#modalPerson">Pilih
                                            Posisi</button></th> --}}
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/event/event.js') }}" type="text/javascript"></script>

    <script>
        $('#table-notif').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            pageLenghth: 5,
            ajax: "{{ route('pending_task.data.position') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'position',
                    name: 'position'
                },
                {
                    data: 'job_holder',
                    name: 'job_holder'
                },
                {
                    data: 'tgl_kosong',
                    name: 'tgl_kosong'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'button',
                    name: 'button'
                },
            ]
        });
        $('#table-notif-employee').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('pending_task.data.employee') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'employee_id',
                    name: 'employee_id'
                },
                {
                    data: 'prev_position',
                    name: 'prev_position'
                },
                {
                    data: 'aspiration',
                    name: 'aspiration'
                },

                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'button',
                    name: 'button'
                },
            ]
        });

        $('#member').select2();
        $('#grade').select2();

        function createEventPosition(obj) {
            let code_position = $(obj).attr('data-position_code');
            let id = $(obj).attr('id');
            $.ajax({
                url: '{{ route('pending_task.add.position') }}',
                method: 'get',
                data: {
                    id: id,
                },
                success: function(res) {
                    $('#modalPosition').html(res);
                    $('#modalPosition').modal('show');
                    getEligible(code_position);
                }
            })


        }

        function getEligible(code_position) {
            $.ajax({
                method: 'GET',
                url: "{{ url('/sub-event/get-attribute-position') }}" + "/" + code_position,
                success: function(response) {
                    $('#employee_eligible').val(response.eligible);
                },
                error: function() {
                    Swal.fire('Sorry', 'Something Went Wrong !', 'error');
                }
            });
        }

        function createEventEmployee(obj) {
            let id = $(obj).attr('id');
            $.ajax({
                url: '{{ route('pending_task.add.employee') }}',
                method: 'get',
                data: {
                    id: id,
                },
                success: function(res) {
                    $('#modalEmployee').html(res);
                    $('#modalEmployee').modal('show');
                    $('#position').select2();
                    $('#listPosition').select2();
                }
            })
        }
    </script>


    @if (session('status') == 'success')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
            });
        </script>
    @endif

    @if (session('status') == 'error')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
            });
        </script>
    @endif
@endsection
