<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Position Event for Employee</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>
        <form action="{{ route('pending_task.store.employee') }}" method="POST">
            @csrf
            <div class="modal-body">

                <div class="form-group">
                    <label>Employee Name</label>
                    <input type="hidden" name="employee_id" value="{{ $pending_task->employee_id }}">
                    <input type="hidden" name="id_notif" value="{{ $pending_task->id }}">
                    <input type="hidden" name="event_name" value="{{ $pending_task->judul }}">
                    <input type="text" class="form-control form-control-solid"
                        value="{{ $pending_task->employee->personnel_number }}" readonly>
                </div>
                <div class="form-group">
                    <label>Position By Aspiration</label>
                    <select name="position" id="position" class="form-control" {{ (isset($position_by_job_family) ? '' : 'required') }} style="width: 100%">
                        @php
                            $jml_aspiration = 0;
                        @endphp
                        @if (count($positions) != 0)
                            @foreach ($aspiration_type as $at)
                                <optgroup label="{{ $at->aspiration_type }}">
                                    @foreach ($positions as $item)
                                        @if ($at->aspiration_type == $item->aspiration_type)
                                            @if (!in_array($item->positions->code_position, $sub_event_position))
                                            @php
                                                $jml_aspiration ++;
                                            @endphp
                                                <option value="{{ $item->positions->code_position }}">
                                                    [{{ $item->positions->sub_holding->name_subholding }}]
                                                    {{ $item->positions->name_position }}</option>
                                            @else
                                                <option disabled value="{{ $item->positions->code_position }}">
                                                    [{{ $item->positions->sub_holding->name_subholding }}]
                                                    {{ $item->positions->name_position }}</option>
                                            @endif
                                        @endif
                                    @endforeach
                                </optgroup>
                            @endforeach
                        @else
                            <option value="">- Tidak Ada Aspirasi -</option>
                        @endif

                    </select>
                </div>
                @if (isset($position_by_job_family) && $jml_aspiration == 0)
                    <div class="form-group">
                        <label>Position By Job Family</label><br>
                        
                        <select name="position" id="listPosition" required class="form-control form-lg mr-1" style="width: 100%" autocomplete="off">
                            <option value="-" selected disabled>- Available Position -</option>
                            @if (!empty($position_by_job_family->vertical))
                                <optgroup label="Vertical">
                                    @foreach ($position_by_job_family->vertical as $position)
                                        <option value="{{ $position->code_position }}"
                                            data-vertical={{ $position->is_vertical ?? 0 }}
                                            data-horizontal={{ $position->is_horizontal ?? 0 }}>
                                            [{{ $position->name_subholding }}] [{{ $position->name_area }}]
                                            {{ $position->name_position }} ({{ $position->grade_align_notes }})
                                        </option>
                                    @endforeach
                                </optgroup>
                            @endif

                            @if (!empty($position_by_job_family->horizontal))
                                <optgroup label="Horizontal">
                                    @foreach ($position_by_job_family->horizontal as $position)
                                        <option value="{{ $position->code_position }}"
                                            data-vertical={{ $position->is_vertical ?? 0 }}
                                            data-horizontal={{ $position->is_horizontal ?? 0 }}>
                                            [{{ $position->name_subholding }}] [{{ $position->name_area }}]
                                            {{ $position->name_position }} ({{ $position->grade_align_notes }})
                                        </option>
                                    @endforeach
                                </optgroup>
                            @endif
                        </select>
                       
                    </div>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Create</button>
            </div>
        </form>
    </div>
</div>
