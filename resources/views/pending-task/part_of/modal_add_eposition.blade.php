<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Position Event for Empty Position</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>
        <form action="{{ route('pending_task.store.position') }}" method="POST">
            @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label>Event Name</label>
                    <input type="hidden" name="id_notif" value="{{ $pending_task->id }}">
                    <input type="hidden" name="position" value="{{ $pending_task->position }}">
                    <input type="text" name="event_name" class="form-control form-control-solid" value="{{ $pending_task->judul }}"
                        readonly>
                </div>
                <div class="form-group">
                    <label>Sourcing Type</label>
                    <select class="form-control select-search" name="sourcing_type">
                        <option value="0">Internal</option>
                        <option value="1">External</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Total Eligible Candidate</label>
                    <input type="text" class="form-control form-control-solid" name="employee_eligible"
                        id="employee_eligible" autocomplete="off" readonly>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Create</button>
            </div>
        </form>
    </div>
</div>
