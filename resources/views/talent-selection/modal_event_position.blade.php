<div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Position Event for
                {{ $employee ? $employee[0]->personnel_number : '-' }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>

        <div class="modal-body">
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <input type="hidden" id="nikSelectedEmployee"
                                    value="{{ $employee ? $employee[0]->prev_persno : 0 }}">
                                <input type="hidden" id="subEventModal" value="{{ $sub_event }}">
                                <input type="hidden" name="talent_id" value="{{ $talent ? $talent[0]->id : 0 }}"
                                    id="talent_id">
                                <div class="col-md-4 justify-content-center text-center">
                                    <div class="symbol symbol-50 symbol-lg-120 ml-4 symbol-light-danger">
                                        <div class="symbol-label"
                                            style="background-image: url( {{ get_employee_pict($employee ? $employee[0]->prev_persno : 0) }} )">
                                        </div>
                                    </div>
                                    <h6 class="text-center">
                                        {{ strtoupper($employee ? $employee[0]->personnel_number : '-') }}
                                    </h6>
                                    <h3 class="text-center" id="EQSModal">EQS {{ $eqs_final }}</h3>
                                </div>

                                <div class="col-md-8">
                                    <form action="{{ route('talent_selection.store.event') }}" method="POST">
                                        <div class="row">

                                            @csrf
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Event Name</label>
                                                    <input type="text" name="event_name"
                                                        class="form-control bg-secondary" readonly
                                                        value="Create Position Event for {{ $employee ? $employee[0]->personnel_number : '-' }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>Position</label>
                                                    <input type="hidden" name="position"
                                                        value="{{ $position->code_position }}">
                                                        <input type="hidden" name="talent_id" value="{{ $talents->id }}">
                                                    <input type="text" class="form-control bg-secondary" readonly
                                                        value="[{{ $position->sub_holding->name_subholding }}] {{ $position->name_position }}">
                                                </div>
                                                <button type="submit" class="btn btn-light-primary font-weight-bold" style="float: right">Create Event</button> 
                                                <button type="button" class="btn btn-light-danger font-weight-bold" style="float: right;margin-right:5px"
                                                    data-dismiss="modal">Cancel</button> 
                                                
                                            </div>

                                        </div>
                                    </form>
                                    <hr>
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Kinerja (K)</label>
                                                <input type="text" name="kinerja" class="form-control bg-secondary"
                                                    value="{{ $eqs ? $eqs->kinerja : 0 }}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Track Record (TR)</label>
                                                <input type="text" name="track_record" class="form-control bg-secondary"
                                                    value="{{ $eqs ? $eqs->track_record : 0 }}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Kesesuaian Kompetensi (Job Fit)</label>
                                                <input type="text" name="kompetensi" class="form-control bg-secondary"
                                                    value="{{ $eqs ? $eqs->job_fit : 0 }}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Experience</label>
                                                <input type="text" name="experience" class="form-control bg-secondary"
                                                    value="{{ $eqs ? $eqs->experience : 0 }}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Training dan Sertifikasi</label>
                                                <input type="text" name="training_sertificate"
                                                    class="form-control bg-secondary"
                                                    value="{{ $eqs ? $eqs->training_sertifikasi : 0 }}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Aspirasi</label>
                                                <input type="text" name="aspirasi" class="form-control bg-secondary"
                                                    value="{{ $eqs ? $eqs->aspiration : 0 }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label for="">Performance Index</label>
                                        <div class="row">
                                            <div class="col-md-12 d-flex justify-content-between">
                                                <div class="input-group mb-2 mr-1">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="Score Kinerja * 80%">K</span>
                                                    </div>
                                                    @if ($is_editable ?? false)
                                                        <input type="number" name="kinerja_performance"
                                                            class="form-control mr-2" id="kinerjaPerformance"
                                                            value="{{ $eqs ? floor(($eqs->mentah_kinerja * 80) / 100) : 0 }}"
                                                            min="{{ isset($data_range) && $data_range->min_performance - 20 > 0 ? $data_range->min_performance - 20 : 0 }}"
                                                            max="{{ isset($data_range) ? $data_range->max_performance - 20 : (100 * 80) / 100 }}">
                                                    @else
                                                        <input type="text" name="kinerja_performance"
                                                            class="form-control mr-2 bg-secondary" id=""
                                                            value="{{ $eqs ? floor(($eqs->mentah_kinerja * 80) / 100) : 0 }}"
                                                            min="0" max="{{ (100 * 80) / 100 }}" readonly>
                                                    @endif
                                                </div>
                                                <i class="fa fa-plus mt-3" style="color: black;"></i>
                                                <div class="input-group mb-2 ml-2">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="Score Track Record * 20%">TR</span>
                                                    </div>
                                                    <input type="text" name="track_record_performance"
                                                        class="form-control mr-2 bg-secondary"
                                                        id="trackRecordPerformance"
                                                        value="{{ $eqs ? ($eqs->mentah_track_record * 20) / 100 : 0 }}"
                                                        min="0" max="{{ (100 * 20) / 100 }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="">Performance Index</span>
                                                    </div>
                                                    <input type="text" name="performance"
                                                        class="form-control mr-2 bg-secondary" id="performance"
                                                        value="{{ $eqs ? floor(($eqs->mentah_kinerja * 80) / 100) + floor(($eqs->mentah_track_record * 20) / 100) : 0 }}"
                                                        min="0" max="110" readonly>
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <span class="text-info range-info range-performance-index"
                                                        style="display:none;"></span>
                                                    <span class="text-danger notif-range-performance-index"
                                                        style="display:none;">Nilai yang anda masukan diluar angka range
                                                        diatas</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Assessment (Job Fit)</label>
                                        @if ($is_editable ?? false)
                                            <input type="number" name="assessment" class="form-control" id="cci"
                                                value="{{ $eqs ? $eqs->mentah_job_fit : 0 }}"
                                                min="{{ isset($data_range) ? $data_range->min_cci : 0 }}"
                                                max="{{ isset($data_range) ? $data_range->max_cci : 100 }}">
                                        @else
                                            <input type="text" name="assessment" class="form-control bg-secondary" id=""
                                                value="{{ $eqs ? $eqs->mentah_job_fit : 0 }}" min="0" max="100"
                                                readonly>
                                        @endif
                                        <div class="d-flex flex-column">
                                            <span class="text-info range-info range-assesment"
                                                style="display:none;"></span>
                                            <span class="text-danger notif-range-cci" style="display:none;">Nilai yang
                                                anda masukan diluar angka range diatas</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Talent Cluster</label>
                                        <input type="text" id="talent" class="form-control bg-secondary"
                                            value="{{ !empty($talent) && !is_null($talent[0]->panel)? predikat_talent_cluster($talent[0]->panel): predikat_talent_cluster(talentMapping((floor($eqs->mentah_kinerja) * 80) / 100 + ($eqs->mentah_track_record * 20) / 100,$eqs ? $eqs->mentah_job_fit : 0)) }}"
                                            readonly>
                                        <span class="text-info range-info range-talent-cluster"
                                            style="display:none;"></span>
                                        <input type="hidden" name="talent"
                                            value="{{ $talent ? $talent[0]->panel : '' }}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 text-center">
                            <h4>Performance</h4>
                            <div class="row justify-content-center">
                                <div class="d-flex align-items-center">
                                    @forelse ($performance as $val)
                                        <div class="symbol symbol-60 mr-3 text-center">
                                            <h4>{{ $val->year }}</h4>
                                            <span>{{ $val->performance_category }}</span>
                                        </div>
                                    @empty
                                        @for ($i = date('Y') - 3; $i < date('Y'); $i++)
                                            <div class="symbol symbol-60 mr-3 text-center">
                                                <h4>{{ $i }}</h4>
                                                <span>No data</span>
                                            </div>
                                        @endfor
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Keterangan Perorangan</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" class="btn btn-icon btn-sm btn-light-primary mr-1"
                            id="kt_card_personal_information">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_personal_information_body" style="display:none">
                    <div class="container">
                        @if ($sub_data['employee'])
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">Nama Lengkap</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->personnel_number }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">NIK</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->no_ktp }}
                                        </div>
                                    </div>
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">Jenis Kelamin</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->gender_text }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">Agama</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->religious_denomination }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">Email</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->email }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">Alamat Rumah</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->home_address }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">Gelar Akademik</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->major_name }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">NPWP</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->no_npwp }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">Tempat Lahir</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->birthplace }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">Tanggal Lahir</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->date_of_birth }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">Nomor Handphone</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->no_hp }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row my-2">
                                        <label class="col-3 col-form-label">Alamat Social Media</label>
                                        <div class="col-9">
                                            <span class="form-control-plaintext font-weight-bolder">:
                                                {{ $sub_data['employee']->sosmed_address }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            Tidak ada data
                        @endif
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Interest</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_hukdis"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_hukdis_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            @if ($sub_data['employee']->interest)
                                {{ $sub_data['employee']->interest }}
                            @else
                                Tidak ada data
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Keahlian</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" class="btn btn-icon btn-sm btn-light-primary mr-1"
                            id="kt_card_skill_profile">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_skill_profile_body" style="display:none">
                    <div class="journel-panel" style="overflow-x:auto">
                        <div class="container">
                            @if ($sub_data['keahlian']);
                                <table class="table">
                                    <caption>List Keahlian</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Keahlian</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['keahlian'] as $value)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $value->nama_keahlian }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data keahlian
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Riwayat Jabatan</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_employee_career"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_employee_career_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            <h5>A. Jabatan/Pekerjaan yang Pernah/Sedang Diemban</h5>
                            @if ($sub_data['history_jabatan'])
                                <table class="table">
                                    <caption>List Riwayat Jabatan</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Jabatan</th>
                                            <th>Uraian Singkat Tugas Dan Kewenangan</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Achievement</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['history_jabatan'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->position_name }}</td>
                                                <td style="word-break: break-all">{{ $row->uraian_singkat }}</td>
                                                <td>{{ $row->start_date }}</td>
                                                <td>{{ $row->end_date }}</td>
                                                <td style="word-break: break-all">{{ $row->achievement }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Riwayat Jabatan
                            @endif
                            <hr>
                            <h5>B. Penugasan yang berkaitan dengan Jabatan Direksi/Dewan Komisaris/Dewan Pengawas (bila
                                ada)</h5>
                            @if ($sub_data['penugasan'])
                                <table class="table">
                                    <caption>List Penugasan</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Penugasan</th>
                                            <th>Tupoksi</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Instansi / Perusahaan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['penugasan'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->penugasan }}</td>
                                                <td style="word-break: break-all">{{ $row->tupoksi }}</td>
                                                <td>{{ $row->start_date }}</td>
                                                <td>{{ $row->end_date }}</td>
                                                <td style="word-break: break-all">{{ $row->instansi_perusahaan }}
                                                </td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Penugasan
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">KEANGGOTAAN ORGANISASI PROFESI / KOMUNITAS YANG DIIKUTI</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_org_memb"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_org_memb_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            <h5>A. Kegiatan / Organisasi yang Pernah/Sedang Diikuti</h5>
                            @if ($sub_data['organisasi_formal'])
                                <table class="table">
                                    <caption>List Keanggotaan Organisasi</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Kegiatan / Organisasi</th>
                                            <th>Jabatan</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Uraian Singkat Kegiatan / Organisasi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['organisasi_formal'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->nama_keg_org }}</td>
                                                <td style="word-break: break-all">{{ $row->jabatan }}</td>
                                                <td>{{ $row->start_date }}</td>
                                                <td>{{ $row->end_date }}</td>
                                                <td style="word-break: break-all">{{ $row->uraian_singkat }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Keanggotaan Organisasi
                            @endif
                            <hr>
                            <h5>B. Kegiatan/Organisasi yang Pernah/Sedang Diikuti</h5>
                            @if ($sub_data['organisasi_nonformal'])
                                <table class="table">
                                    <caption>List Kegiatan / Organisasi yang Pernah / Sedang Diikuti</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Kegiatan / Organisasi</th>
                                            <th>Jabatan</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Uraian Singkat Kegiatan / Organisasi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['organisasi_nonformal'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->nama_keg_org }}</td>
                                                <td style="word-break: break-all">{{ $row->jabatan }}</td>
                                                <td>{{ $row->start_date }}</td>
                                                <td>{{ $row->end_date }}</td>
                                                <td style="word-break: break-all">{{ $row->uraian_singkat }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Kegiatan/Organisasi yang Pernah/Sedang Diikuti
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">PENGHARGAAN</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_rwd"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_rwd_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            @if ($sub_data['penghargaan'])
                                <table class="table">
                                    <caption>List Penghargaan</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Jenis Penghargaan</th>
                                            <th>Tingkat</th>
                                            <th>Diberikan Oleh</th>
                                            <th>Tahun</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['penghargaan'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->jenis_penghargaan }}</td>
                                                <td style="word-break: break-all">{{ $row->tingkat }}</td>
                                                <td style="word-break: break-all">{{ $row->diberikan_oleh }}</td>
                                                <td>{{ $row->tahun }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Penghargaan
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">RIWAYAT PENDIDIKAN DAN PELATIHAN</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_pnd_pel"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_pnd_pel_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            <h5>1. Pendidikan Formal</h5>
                            @if ($sub_data['pendidikan'])
                                <table class="table">
                                    <caption>List Pendidikan Formal</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Jenjang</th>
                                            <th>Penjurusan</th>
                                            <th>Perguruan Tinggi</th>
                                            <th>Tahun Lulus</th>
                                            <th>Kota / Negara</th>
                                            <th>Penghargaan yang Didapat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['pendidikan'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $row->level_education }}</td>
                                                <td style="word-break: break-all">{{ $row->major_name }}</td>
                                                <td style="word-break: break-all">{{ $row->university_name }}</td>
                                                <td>{{ $row->tahun_lulus }}</td>
                                                <td style="word-break: break-all">{{ $row->kota_negara }}</td>
                                                <td style="word-break: break-all">{{ $row->penghargaan }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Pendidikan Formal
                            @endif
                            <hr>
                            <h5>2. Pendidikan dan Latihan / Pengembangan Kompetensi yang Pernah Diikuti (minimal 16 Jam)
                            </h5>
                            <h6>A. Diklat Jabatan</h6>
                            @if ($sub_data['diklat_jabatan'])
                                <table class="table">
                                    <caption>List Pendidikan dan Latihan</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Pendidikan dan Latihan / Pengembangan Kompetensi</th>
                                            <th>Penyelenggara / Kota</th>
                                            <th>Lama Diklat / Pengembangan Kompetensi</th>
                                            <th>Nomor Sertifikasi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['diklat_jabatan'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->nama_kegiatan }}</td>
                                                <td style="word-break: break-all">{{ $row->penyelenggara }}</td>
                                                <td style="word-break: break-all">{{ $row->lama }}</td>
                                                <td style="word-break: break-all">{{ $row->nomor_sertifikasi }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Pendidikan dan Latihan
                            @endif
                            <hr>
                            <h6>B. Diklat Fungsional</h6>
                            @if ($sub_data['diklat_fungsional'])
                                <table class="table">
                                    <caption>List Pendidikan dan Latihan</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Pendidikan dan Latihan / Pengembangan Kompetensi</th>
                                            <th>Penyelenggara / Kota</th>
                                            <th>Lama Diklat / Pengembangan Kompetensi</th>
                                            <th>Nomor Sertifikasi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['diklat_fungsional'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->nama_kegiatan }}</td>
                                                <td style="word-break: break-all">{{ $row->penyelenggara }}</td>
                                                <td style="word-break: break-all">{{ $row->lama }}</td>
                                                <td style="word-break: break-all">{{ $row->nomor_sertifikasi }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Diklat Fungsional
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">KARYA TULIS ILMIAH</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_kry_tls"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_kry_tls_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            @if ($sub_data['karya'])
                                <table class="table">
                                    <caption>List Karya Tulis Ilmiah</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Judul dan Media Publikasi</th>
                                            <th>Tahun</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['karya'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->judul }}</td>
                                                <td>{{ $row->tahun }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Karya Tulis Ilmiah
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">PENGALAMAN SEBAGAI PEMBICARA / NARASUMBER / JURI</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_exp_spk_judg"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_exp_spk_judg_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            @if ($sub_data['exp_acara'])
                                <table class="table">
                                    <caption>List Pengalaman Sebagai Pembicara / Narasumber / Juri</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Acara / Tema</th>
                                            <th>Penyelenggara</th>
                                            <th>Periode</th>
                                            <th>Lokasi dan Peserta</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['exp_acara'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->acara_tema }}</td>
                                                <td style="word-break: break-all">{{ $row->penyelenggara }}</td>
                                                <td>{{ $row->periode }}</td>
                                                <td style="word-break: break-all">{{ $row->lokasi_peserta }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Pengalaman Sebagai Pembicara / Narasumber / Juri
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">REFERENSI</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_refs"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_refs_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            @if ($sub_data['referensi'])
                                <table class="table">
                                    <caption>List Referensi</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Perusahaan</th>
                                            <th>Jabatan</th>
                                            <th>Nomor Handphone</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['referensi'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->nama }}</td>
                                                <td style="word-break: break-all">{{ $row->perusahaan }}</td>
                                                <td style="word-break: break-all">{{ $row->jabatan }}</td>
                                                <td>{{ $row->no_hp }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Referensi
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">KETERANGAN KELUARGA</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_fams"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_fams_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            <h5>1. Istri / Suami</h5>
                            @if ($sub_data['pasangan'])
                                <table class="table">
                                    <caption>List Istri / Suami</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Tempat Lahir</th>
                                            <th>Tgl Lahir</th>
                                            <th>Tgl Menikah</th>
                                            <th>Pekerjaan</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['pasangan'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->nama }}</td>
                                                <td style="word-break: break-all">{{ $row->tmp_lahir }}</td>
                                                <td>{{ $row->tgl_lahir }}</td>
                                                <td>{{ $row->tgl_menikah }}</td>
                                                <td style="word-break: break-all">{{ $row->pekerjaan }}</td>
                                                <td style="word-break: break-all">{{ $row->keterangan }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Istri / Suami
                            @endif
                            <hr>
                            <h5>2. Anak</h5>
                            @if ($sub_data['children'])
                                <table class="table">
                                    <caption>List Anak</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Tempat Lahir</th>
                                            <th>Tgl Lahir</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Pekerjaan</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['children'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->nama }}</td>
                                                <td style="word-break: break-all">{{ $row->tmp_lahir }}</td>
                                                <td>{{ $row->tgl_lahir }}</td>
                                                <td>{{ $row->jns_kelamin }}</td>
                                                <td style="word-break: break-all">{{ $row->pekerjaan }}</td>
                                                <td style="word-break: break-all">{{ $row->keterangan }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Anak
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">PENGALAMAN DAN KEAHLIAN</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_exp_and_master"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_exp_and_master_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            <h5>1. Pengalaman</h5>
                            @if ($sub_data['pengalaman'])
                                <table class="table">
                                    <caption>List Pengalaman</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Pengalaman</th>
                                            <th>Deskripsi Pengalaman</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['pengalaman'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->name }}</td>
                                                <td style="word-break: break-all">{{ $row->deskripsi }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Pengalaman
                            @endif
                            <hr>
                            <h5>2. Keahlian</h5>
                            @if ($sub_data['emp_keahlian'])
                                <table class="table">
                                    <caption>List Anak</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Keahlian</th>
                                            <th>Deskripsi Keahlian</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['emp_keahlian'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->name }}</td>
                                                <td style="word-break: break-all">{{ $row->deskripsi }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data Keahlian
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">PENGALAMAN KARIR KELAS, KLASTER, dan JABATAN di BUMN</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_exp_klstr_jbtn"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_exp_klstr_jbtn_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            <h5>1. KELAS BUMN yang PERNAH menjadi Perusahaan tempat bekerja</h5>
                            @if ($sub_data['master_kelas_bumn'])
                                <table class="table">
                                    <caption>List Kelas BUMN</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['master_kelas_bumn'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->name }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data KELAS BUMN yang PERNAH menjadi Perusahaan tempat bekerja
                            @endif
                            <hr>
                            <h5>2. KLASTER BUMN dimana Anda memiliki Exposure</h5>
                            @if ($sub_data['master_klaster_bumn'])
                                <table class="table">
                                    <caption>List Anak</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['master_klaster_bumn'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->name }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data KLASTER BUMN dimana Anda memiliki Exposure
                            @endif
                            <hr>
                            <h5>3. Uraikan secara singkat mengenai pengalaman Peserta pada KELAS, KLASTER, dan EXPOSURE
                                yang dipilih.</h5>
                            @if ($sub_data['uraian_pengalaman'])
                                <textarea class="form-control w-100 bg-white" cols="30" rows="10"
                                    disabled>{{ $sub_data['uraian_pengalaman'] }}</textarea>
                            @else
                                Tidak ada data KLASTER BUMN yang menjadi Pengalaman Anda
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">ASPIRASI PADA KELAS, KLASTER, dan JABATAN di BUMN</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:void(0)" id="kt_card_asp_klstr_jbtn"
                            class="btn btn-icon btn-sm btn-light-primary mr-1">
                            <i class="ki ki-arrow-down icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body kt_card_asp_klstr_jbtn_body" style="display:none">
                    <div class="journel-panel">
                        <div class="container">
                            <h5>1. KELAS BUMN yang menjadi Aspirasi</h5>
                            @if ($sub_data['master_asp_kelas_bumn'])
                                <table class="table">
                                    <caption>List Kelas BUMN</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['master_asp_kelas_bumn'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->name }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data KELAS BUMN yang menjadi Aspirasi
                            @endif
                            <hr>
                            <h5>2. KLASTER BUMN yang menjadi Aspirasi Anda</h5>
                            @if ($sub_data['master_asp_klaster_bumn'])
                                <table class="table">
                                    <caption>List Anak</caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($sub_data['master_asp_klaster_bumn'] as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-all">{{ $row->name }}</td>
                                            </tr>
                                        @empty
                                            Tidak ada data
                                        @endforelse
                                    </tbody>
                                </table>
                            @else
                                Tidak ada data KLASTER BUMN yang menjadi Aspirasi Anda
                            @endif
                            <hr>
                            <h5>3. Uraikan secara singkat mengenai pengalaman Peserta pada KELAS, KLASTER, dan EXPOSURE
                                yang dipilih.</h5>
                            @if ($sub_data['uraian_aspirasi'])
                                <textarea class="form-control w-100 bg-white" cols="30" rows="10"
                                    disabled>{{ $sub_data['uraian_aspirasi'] }}</textarea>
                            @else
                                Tidak ada data KLASTER BUMN yang menjadi Aspirasi Peserta
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            {{-- <button type="button" class="btn btn-primary font-weight-bold">Create</button> --}}
        </div>

    </div>
</div>
