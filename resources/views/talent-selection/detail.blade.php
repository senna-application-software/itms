{{-- Extends layout --}}
@extends('layout.default')
<style>
    .checklabel {
        display: block;
        width: 100%;
        border: 2px solid #0000000d;
        background: #1BC5BD;
        border-radius: 22px;
        text-align: center;
        padding: 10px 3em;
        color: #f9f9f9;

    }

    .checklabel:hover {
        background: rgb(207, 207, 207);
    }

</style>
{{-- Content --}}
@section('content')
    <div class="col-12 mb-5">
        <div class="card px-5 py-3">
            <div class="d-flex justify-content-between">
                <h4 class="align-self-center m-0 p-0">
                    {{ $sub_event->name_position }}
                    <span class="label font-weight-bold label-lg {{ $sub_event->sourcing_type == 1 ? 'label-light-success' : 'label-light-primary' }} label-inline">
                        {{ $sub_event->sourcing_type == 1 ? 'External' : 'Internal' }}
                    </span>
                    <small class="text-muted">{{ $sub_event->name_subholding }} - [{{ $sub_event->name_area }}]</small>
                </h4>
                <div>
                     <button class="btn btn-success" data-select-all-click="boards">Pilih Semua</button>
                    <button class="btn btn-warning" id="compare">Compare</button>
                    <button class="btn btn-primary" id="done_selection">Done Selection</button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <!-- <div class="row"> -->
        <form action="{{ route('talent-selection.done_selection') }}" method="POST" id="form_selection">
            @csrf
            <div class="row">
                <input type="hidden" name="sub_event" value="{{ $sub_event->id }}">
                <input type="hidden" name="tag" id="tagging">

                @foreach ($data as $item)
                    <input type="hidden" value="{{ $item->name_position_level }}" id="namePositionLevel{{ $item->id }}">
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                        <div class="card card-custom gutter-b card-stretch">
                            <div class="card-body pt-4 d-flex flex-column">
                                <div class="d-flex justify-content-between my-5" data-select-all="boards">
                                    <label class="form-check form-check-custom form-check-solid align-self-start pl-0">
                                        <input id="check{{ $item->id }}"
                                            class="form-check-input h-25px w-25px checkbox ck_flagging" name="talent_id[]"
                                            {{ $item->is_disposisi == 1 ? 'disabled' : '' }} type="checkbox"
                                            data-name="{{ $item->employee->personnel_number }}"
                                            value="{{ $item->id }}" />
                                    </label>
                                    <div class="symbol symbol-circle symbol-100">
                                        <img src="{{ get_employee_pict($item->employee->prev_persno) }}" alt="image">
                                    </div>

                                </div>

                                <div class="mt-3 mb-5">
                                    <div class="row mb-3">
                                        <div class="col-md-10">
                                            <h3 class="text-primary font-weight-bold text-hover-warning font-size-h4 mb-0 justify-content-between open-career-card"
                                                type="button" data-nik="{{ $item->employee->prev_persno }}"
                                                data-id="{{ $item->id }}" style="-webkit-appearance: none !important;">
                                                {{ $item->employee->personnel_number }}
                                            </h3>
                                        </div>
                                        <div class="col-md-2 btn_flagging_{{ $item->id }}"
                                            id="btn_flagging_{{ $item->id }}" style="display: none">
                                            <div class="dropdown table-action">
                                                <button class="btn btn-light-primary btn-icon btn-sm" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    <i class="ki ki-bold-more-ver"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a href="javascript:void(0)" class="dropdown-item"
                                                        onclick="tandaiKandidat(this)" id="{{ $item->id }}">
                                                        Tandai Kandidat Untuk Posisi Lain
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <p class="text-muted font-weight-bold">{{ $item->employee->master_posisi->name_position }}</p>
                                    <p class="font-weight-bold p-0 m-0">{{ $item->employee->positions->sub_holding->name_subholding ?? "" }}</p>
                                    <p class="font-weight-bold p-0 m-0">{{ $item->employee->positions->master_area->name_area ?? "" }}</p>
                                    @if($item->comitte_proposal)
                                    <div class="form-check mt-3 mb-3">
                                        <input class="form-check-input" type="checkbox" disabled checked>
                                        <label class="form-check-label">
                                            Committee Proposal
                                        </label>
                                    </div>
                                    @endif
                                    <h3 class="mb-4" data-nik-eqs="{{ $item->employee->prev_persno }}">EQS:
                                        {{ nilai_eqs($item->employee->prev_persno, false, $sub_event->id) }}</h3>
                                    <span class="badge badge-primary w-100 mb-2">
                                        Nilai Akhir: {{ final_mark_assessment($item->id) }}
                                    </span>
                                    {!! assessment_label(final_mark_assessment($item->id), true) !!}
                                </div>


                                <div class="row mt-auto">
                                    @foreach ($item->employee->performance as $performance)
                                    <div class="col-md-4 mx-1 rounded p-1 text-center" style="background-color:#82E99F;min-height: 70px;max-width: 30%">
                                        <span>{{ $performance->year }}</span>
                                        <br>
                                        <span>{{ $performance->perfomance_category_align }}</span>
                                    </div>
                                    @endforeach
                                </div>
                                {{-- <div class="row mt-10" id="selectedd">
                                    <label class="checklabel" for="check{{ $item->id }}">Select Employee</label>
                            </div> --}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </form>
    </div>

    <!-- Employee Score Card -->
    <div class="modal fade" id="xl-modal" tabindex="-1" role="dialog" aria-labelledby="xl-modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Employee Career Card</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body data-modal" data-scroll="true" data-height="500">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

    @section('modal')
        {{-- modal done profiling --}}
        <div class="modal fade" id="modal_selection" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirm Talent selection Results</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <form action="" method=" ">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr id="selection_head">

                                    </tr>
                                </thead>
                                <tbody id="selection_body">

                                </tbody>
                            </table>
                            <input type="checkbox" checked required> <span>Kandidat yang terpilih sudah sesuai</span>
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="done">Done</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalAddEvent" data-backdrop="static" tabindex="-1" role="dialog"
            aria-labelledby="staticBackdrop" aria-hidden="true">

        </div>
        <div class="modal fade" id="modalEventPosition" data-backdrop="static" tabindex="-1" role="dialog"
            aria-labelledby="staticBackdrop" aria-hidden="true">

        </div>
    @endsection

    {{-- Styles Section --}}
    @section('styles')
        <link href="{{ asset('css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css" />
    @endsection

    {{-- Scripts Section --}}
    @section('scripts')
        <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/pages/custom/wizard/wizard-1.js') }}" type="text/javascript"></script>
        @include('global.element.script_eqs_peremployee')
        <script>
            // fungsi all checked
            $('[data-select-all-click]').click(function() {
                var selfAllSelector = $(this).data('select-all-click');
                boardInputs = $('[data-select-all='+selfAllSelector+'] input');
                boardInputs.prop('checked', !boardInputs.prop('checked') );
            }); 
            $(document).on("click", "#submit_assessment", function(e) {
                e.preventDefault();

                var id = $('#talent_id').val();
                var performance = $('#performance').val();
                var assessment = $('#cci').val();

                console.log(id);
                console.log(performance);
                console.log(assessment);

                if (id != "" && performance != "" && assessment != "") {
                    $.ajax({
                        url: "{{ url('talent-selection/submit-assessment') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: id,
                            performance: performance,
                            assessment: assessment
                        },
                        success: function(message) {
                            console.log(message);
                            var message = JSON.parse(message);
                            if (message.status == 200) {
                                alert('success !');
                            } else if (message.status == 201) {
                                alert("Error occured !");
                            }
                        }
                    });
                } else {
                    alert('Please fill all the field !');
                }
            })

            //submit
            $('#done_selection').on('click', function() {
                if ($("input[name='talent_id[]']:checked").length == 1) {
                    event.preventDefault();
                    var listProfiling = $("input:checkbox:checked").map(function() {
                        return $(this).attr("data-name");;
                    }).toArray();
                    var html = "";
                    var html_head = `<th scope="col">Choosen Candidates (` + listProfiling.length + `)</th>`;
                    for (let p = 0; p < listProfiling.length; p++) {
                        html += `<tr>
                            <td>` + listProfiling[p] + `</td>
                            </tr>`;
                    }
                    $("#selection_head").html(html_head);
                    $("#selection_body").html(html);


                    $("#modal_selection").modal();

                } else {
                    Swal.fire("Please Select 1 Candidates to Continue!", "", "warning");
                }
            })

            $('#done').on('click', function() {
                $('#tagging').val('selected');
                $('#form_selection').submit();
            })
            $('#compare').on('click', function() {
                if ($("input[name='talent_id[]']:checked").length) {
                    event.preventDefault();
                    $('#form_selection').submit();

                } else {
                    Swal.fire("Please Select 1 Candidates to Continue!", "", "warning");
                }
            })

            $('.ck_flagging').on('click', function() {
                let talent_id = $(this).val();
                let jml_checked = $("input[name='talent_id[]']:checked").length;
                let jml_candidate = $("input[name='talent_id[]']").length;
               
                $("input[name='talent_id[]']").each(function(k, i) {
                    let namePositionLevel = $('#namePositionLevel'+i.value).val();

                    if (jml_checked == 0) {
                        $('.btn_flagging_' + i.value).hide();
                    } else {
                        if ($('#check' + i.value).is(":checked")) {
                            $('.btn_flagging_' + i.value).hide();
                        } else {
                            if ($('#check' + i.value).prop('disabled') || namePositionLevel == 'Expert') {
                                // $('.btn_flagging_' + i.value).hide();
                                $('.btn_flagging_' + i.value).show();
                            } else {
                                $('.btn_flagging_' + i.value).show();
                            }

                        }
                    }

                })
            })

            function tandaiKandidat(obj) {
                let talent_id = $(obj).attr('id');
                $.ajax({
                    url: '{{ route('talent_selection.add.flagging') }}',
                    method: 'get',
                    data: {
                        id: talent_id,
                    },
                    success: function(res) {
                        $('#modalAddEvent').html(res);
                        $('#modalAddEvent').modal('show');
                        $('#listPosition').select2();

                    }
                })
            }

            function createEventPosition(obj) {
                // var id = $(obj).data('id')
                var url = "{{ url('talent-selection/add-event-position') }}" + '/' + $(obj).attr("data-nik")
                var code_position = $(obj).attr('data-code_position');
                var talent_id = $(obj).attr('data-talent_id');


                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {
                        sub_event: "{{ $sub_event->id ?? $sub_event }}",
                        is_editable: "{{ $is_editable ?? 0 }}",
                        is_talent_map: "{{ $is_talent_map ?? 0 }}",
                        code_position: code_position,
                        talent_id: talent_id,
                    },
                    success: function(response) {
                        $('#modalEventPosition').html(response);
                        $('#modalEventPosition').modal('show');
                        
                    }
                })
            }

            function createEventPositionJF(){
                var url = "{{ url('talent-selection/add-event-position') }}" + '/' + $('#listPosition').find(':selected').attr("data-nik")
                var code_position = $('#listPosition').find(':selected').val()
                var talent_id = $('#listPosition').find(':selected').attr('data-talent_id');

                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {
                        sub_event: "{{ $sub_event->id ?? $sub_event }}",
                        is_editable: "{{ $is_editable ?? 0 }}",
                        is_talent_map: "{{ $is_talent_map ?? 0 }}",
                        code_position: code_position,
                        talent_id: talent_id,
                    },
                    success: function(response) {
                        $('#modalEventPosition').html(response);
                        $('#modalEventPosition').modal('show');
                        
                    }
                })

            }
        </script>

        @if (session('status') == 'success')
            <script>
                $(document).ready(function() {
                    Swal.fire({
                        title: "{{ session('title') }}",
                        text: "{{ session('message') }}",
                        icon: "{{ session('status') }}",
                        buttonsStyling: false,
                        confirmButtonText: "close",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    })
                });
            </script>
        @endif

        @if (session('status') == 'error')
            <script>
                $(document).ready(function() {
                    Swal.fire({
                        title: "{{ session('title') }}",
                        text: "{{ session('message') }}",
                        icon: "{{ session('status') }}",
                        buttonsStyling: false,
                        confirmButtonText: "close",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    })
                });
            </script>
        @endif
    @endsection
