<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Position Event for
                {{ $talent->employee->personnel_number }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>

        <div class="modal-body">
            <div class="form-group">
                <label for="">Berdasarkan Aspirasi</label>
                <div class="table-responsive">
                    <table class="table table-bordered pt-3">
                        <thead>
                            <tr>
                                <th>Aspirasi</th>
                                <th>Tipe</th>
                                <th>Aksi</th>
                            </tr>

                        </thead>
                        <tbody>
                            @if (count($talent_aspiration) > 0)
                                @foreach ($talent_aspiration as $row)
                                    <tr>
                                        <td>[{{ $row['name_subholding'] }}] {{ $row['name_position'] }}</td>
                                        <td>{{ $row['aspiration_level'] }}</td>
                                        @if ($talent->sub_events->position == $row['code_position'])
                                            <td><button type="button" class="btn btn-sm btn-danger disabled"
                                                    disabled>Event Sedang Berlangsung</button>
                                            </td>
                                        @else
                                            <td class="text-center"><button type="button"
                                                    class="btn btn-sm btn-primary" onclick="createEventPosition(this)"
                                                    data-code_position="{{ $row['code_position'] }}"
                                                    data-talent_id="{{ $talent->id }}"
                                                    data-nik="{{ $talent->employee->prev_persno }}">Pilih</button>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3" class="text-center">Tidak Ada Data</td>
                                </tr>
                            @endif

                        </tbody>
                    </table>

                </div>
            </div>
            @if (isset($position_by_job_family))
                <div class="form-group">
                    <label>Position By Job Family</label><br>

                    <select name="position" id="listPosition" required class="form-control form-lg mr-1"
                        style="width: 100%" autocomplete="off">
                        <option value="-" selected disabled>- Available Position -</option>
                        @if (!empty($position_by_job_family->vertical))
                            <optgroup label="Vertical">
                                @foreach ($position_by_job_family->vertical as $position)
                                    <option value="{{ $position->code_position }}"
                                        {{ $talent->sub_events->position == $position->code_position ? 'disabled' : '' }}
                                        data-vertical={{ $position->is_vertical ?? 0 }}
                                        data-talent_id="{{ $talent->id }}"
                                        data-nik="{{ $talent->employee->prev_persno }}"
                                        data-horizontal={{ $position->is_horizontal ?? 0 }}>
                                        [{{ $position->name_subholding }}] [{{ $position->name_area }}]
                                        {{ $position->name_position }} ({{ $position->grade_align_notes }})
                                    </option>
                                @endforeach
                            </optgroup>
                        @endif

                        @if (!empty($position_by_job_family->horizontal))
                            <optgroup label="Horizontal">
                                @foreach ($position_by_job_family->horizontal as $position)
                                    <option value="{{ $position->code_position }}"
                                        {{ $talent->sub_events->position == $position->code_position ? 'disabled' : '' }}
                                        data-vertical={{ $position->is_vertical ?? 0 }}
                                        data-talent_id="{{ $talent->id }}"
                                        data-nik="{{ $talent->employee->prev_persno }}"
                                        data-horizontal={{ $position->is_horizontal ?? 0 }}>
                                        [{{ $position->name_subholding }}] [{{ $position->name_area }}]
                                        {{ $position->name_position }} ({{ $position->grade_align_notes }})
                                    </option>
                                @endforeach
                            </optgroup>
                        @endif
                    </select>

                </div>
                <button type="button" class="btn btn-primary font-weight-bold" onclick="createEventPositionJF()" style="float: right">Next</button>
            @endif
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-primary font-weight-bold" 
                data-dismiss="modal">Close</button>
            {{-- <button type="button" class="btn btn-primary font-weight-bold">Create</button> --}}
        </div>

    </div>
</div>
