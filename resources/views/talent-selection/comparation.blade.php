{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-12 mb-5">
            <div class="card py-4 px-8">
                <div class="d-flex justify-content-between">
                    <h4 class="align-self-center m-0 p-0">{{ $sub_event->name_position }}</h4>
                    <div>
                        <a href="{{ url()->previous() }}" class="btn btn-secondary">Back</a>
                        {{-- <button id="done_compare" class="btn btn-primary">Done Selection</button> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <form action="{{ route('talent-selection.done_selection') }}" method="POST" id="form_selection">
                @csrf
                <div class="flex-row-lg-fluid" id="kt_profile_aside">
                    <div class="row">
                        <input type="hidden" name="sub_event" value="{{ $sub_event->id }}">
                        <input type="hidden" name="tag" id="tagging">
                        @php
                            $rank = 1;
                        @endphp
                        @foreach ($data as $index => $item)
                            <div class="col-xl-4">
                                <!--begin::Card-->
                                <div class="card card-custom gutter-b">
                                    <!--begin::Body-->
                                    <div class="card-body">
                                        <!--begin::User-->
                                        <div class="d-flex align-items-center" style="height: 110px;">
                                            <div
                                                class="symbol symbol-80 symbol-xxl-100 mr-5 align-self-center align-self-xxl-center">
                                                {{-- <input hidden class="form-check-input h-20px w-20px" name="talent_id[]" type="checkbox" data-name="{{ $item->employee->personnel_number }}" value="{{ $item->id }}"/>
                                                <br>
                                                <br> --}}
                                                <img class="ml-8symbol-label"
                                                    src="{{ get_employee_pict($item->employee->prev_persno) }}"
                                                    alt="image">
                                            </div>
                                            <div>
                                                <br>
                                                <br>
                                                <a href="#"
                                                    class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary"
                                                    data-id="{{ $item->id }}">{{ $item->employee->personnel_number }}</a>
                                                <div class="mt-1">
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                        <div class="d-flex flex-column text-dark-75"
                                                            style="border-right: 3px solid rgb(222, 222, 222); padding-right:8px;">
                                                            <span class="font-weight-bolder font-size-sm">Rank</span>
                                                            <span
                                                                class="font-weight-bolder font-size-h5">{{ $rank++ }}</span>
                                                        </div>
                                                        <div class="d-flex flex-column text-dark-75 ml-4">
                                                            <span class="font-weight-bolder font-size-sm">EQS</span>
                                                            <span
                                                                class="font-weight-bolder font-size-h5">{{ nilai_eqs($item->employee->prev_persno, false, $sub_event->id) }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::User-->
                                        <!--begin::Contact-->
                                        <div class="pt-8 pb-6">
                                            <div class="separator separator-dashed my-3"></div>
                                            <div class="d-flex align-items-center justify-content-between" style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Nilai Talent Day</span>
                                                <span class="font-weight-bold text-right">{{ final_mark_assessment($item->id) ?? '-' }}</span>
                                            </div>
                                            <div class="separator separator-dashed my-3"></div>
                                            <div class="d-flex align-items-center justify-content-between" style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Hasil Talent Day</span>
                                                <span class="font-weight-bold text-right">{{ assessment_label(final_mark_assessment($item->id)) ?? '-' }}</span>
                                            </div>
                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Perusahaan </span>
                                                <span
                                                    class="font-weight-bold text-right">{{ getSubHolding($item->employee->code_subholding) ? getSubHolding($item->employee->code_subholding) : '-' }}</span>
                                            </div>


                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Work Location</span>
                                                <span
                                                    class="font-weigth-boldest text-right">{{ getAreaLocationWork($item->employee->position) }}</span>
                                            </div>

                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Position Level</span>
                                                <span
                                                    class="font-weigth-boldest text-right">{{ $item->employee->bod_type ? $item->employee->bod_type : '-' }}</span>
                                            </div>
                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Job Grade </span>
                                                <span
                                                    class="font-weigth-boldest text-right">{{ isset($item->employee->person_grade) ? $item->employee->person_grade : '-' }}</span>
                                            </div>

                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Job Family Align</span>
                                                <span
                                                    class="font-weigth-boldest text-right">{{ isset($item->employee->job_family_align) ? $item->employee->job_family_align : '-' }}</span>

                                            </div>

                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Performance Index</span>
                                                <span
                                                    class="font-weigth-boldest text-right">{{ Performanceindex($item->employee->prev_persno, $item->sub_event)? Performanceindex($item->employee->prev_persno, $item->sub_event): '-' }}</span>

                                            </div>

                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Age</span>
                                                <span
                                                    class="font-weigth-boldest text-right">{{ ulangtahun(date('Y-m-d', strtotime($item->employee->date_of_birth)))? ulangtahun(date('Y-m-d', strtotime($item->employee->date_of_birth))): '-' }}</span>
                                            </div>

                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Masa Kerja Jabatan</span>
                                                <span
                                                    class="font-weigth-boldest text-right">{{ masakerja_employee($item->nik, $item->employee->master_posisi->name_position) }}</span>
                                            </div>

                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Masa Kerja Job Family
                                                    Tujuan</span>
                                                <span
                                                    class="font-weigth-boldest text-right">{{ masakerja_employee_job_family([$item->nik], $sub_event->code_job_family)? masakerja_employee_job_family([$item->nik], $sub_event->code_job_family): '-' }}</span>
                                            </div>

                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Assessment Result</span>
                                                <span
                                                    class="font-weigth-boldest text-right">{{ getResultAssessment($item->nik) }}</span>
                                            </div>

                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex align-items-center justify-content-between"
                                                style="height:25px">
                                                <span class="font-weight-normal text-muted mr-2">Talent Cluster</span>
                                                <span
                                                    class="font-weigth-boldest text-right">{{ $item->box->box_cat->cat_name ? $item->box->box_cat->cat_name : '-' }}</span>
                                            </div>
                                            <div class="separator separator-dashed my-3"></div>

                                            <div class="d-flex  justify-content-between" style="height:120px;">
                                                <span class="font-weight-normal text-muted mr-2">Endorsement</span>
                                                <span class="font-weight-bold text-right">
                                                    @if (count(getEndorsement($item->nik, $sub_event->code_position, $sub_event->id_event)) > 0)
                                                        @foreach (getEndorsement($item->nik, $sub_event->code_position, $sub_event->id_event) as $index => $item)
                                                            @if ($item->aspiration_type == 'individual')
                                                                INDIVIDUAL (IA) <br><br>
                                                            @elseif ($item->aspiration_type == 'supervisor')
                                                                SUPERVISOR (SA)<br><br>
                                                            @elseif ($item->aspiration_type == 'unit')
                                                                UNIT (UA)<br><br>
                                                            @elseif ($item->aspiration_type == 'job_holder')
                                                                JOB HOLDER (JA)<br><br>
                                                            @elseif ($item->aspiration_type == '-')
                                                                - <br><br>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <span class="font-weight-normal text-muted mr-2"></span>
                                                        <span class="font-weight-bold text-right">-</span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Body-->
                                </div>
                                <!--end::Card-->
                            </div>
                        @endforeach
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- modal done profiling --}}
    <div class="modal fade" id="modal_selection" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirm Talent selection Results</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="" method=" ">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr id="selection_head">

                                </tr>
                            </thead>
                            <tbody id="selection_body">

                            </tbody>
                        </table>
                        <input type="checkbox" checked required> <span>Kandidat yang terpilih sudah sesuai</span>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="done">Done</button>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/pages/crud/forms/widgets/select2.js') }}"></script>
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
    <script>
        $('input[type=checkbox]').on('change', function(evt) {
            if ($("input[name='talent_id[]']:checked").length >= 2) {
                this.checked = false;
                Swal.fire("Can only select one", "", "warning");
            }
        });
        $('#done_compare').on('click', function() {
            if ($("input[name='talent_id[]']:checked").length) {
                event.preventDefault();
                var listProfiling = $("input:checkbox:checked").map(function() {
                    return $(this).attr("data-name");;
                }).toArray();
                var html = "";
                var html_head = `<th scope="col">Choosen Candidates (` + listProfiling.length + `)</th>`;
                for (let p = 0; p < listProfiling.length; p++) {
                    html += `<tr>
                            <td>` + listProfiling[p] + `</td>
                            </tr>`;
                }
                $("#selection_head").html(html_head);
                $("#selection_body").html(html);


                $("#modal_selection").modal();

            } else {
                Swal.fire("Please Select 1 Candidates to Continue!", "", "warning");
            }
        })

        $('#done').on('click', function() {
            $('#tagging').val('selected');
            $('#form_selection').submit();
        })
    </script>
@endsection
