<!DOCTYPE html>
<html>

<head>
    <title>DOKUMEN SK - {{ $data_raw->employee->personnel_number }}</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
            text-align: justify;
            text-justify: inter-word;
        }

        body {
            padding-right: 7%;
            padding-left: 7%;
        }

        .text-center {
            text-align: center !important;
        }

        .text-right {
            text-align: right !important;
        }

        li {
            margin-bottom: 1.5%;
        }

        .ttd {
            position: absolute;
            right: 0px;
            width: 35%;

            height: auto;
        }

        .page-break {
            page-break-after: always;
        }

    </style>
</head>

<body>
    <p class="text-center">KEPUTUSAN DIREKSI PT AVIASI PARIWISATA INDONESIA (PERSERO)</p>
    <p class="text-center">NOMOR: PLCHLDR-#1-REV-6.9</p>
    <p class="text-center">TENTANG</p>
    <p class="text-center">PENEMPATAN KARYAWAN DALAM JABATAN <br>
        {{ strtoupper($data_raw->sub_events->get_position->name_position) }} DI
        {{ strtoupper($data_raw->sub_events->get_position->sub_holding->name_subholding) }}
    </p>
    <p class="text-center">DIREKSI PT AVIASI PARIWISATA INDONESIA (PERSERO),</p>
    <table>
        <tr>
            <td style="vertical-align: top; text-align: left;">Menimbang</td>
            <td style="vertical-align: top; text-align: left;">:</td>
            <td>
                <ol style="list-style-type: lower-alpha; margin-top: 0;">
                    <li>bahwa telah ditetapkan Struktur Organisasi PT Aviasi Pariwisata Indonesia (Persero);</li>
                    <li>bahwa telah ditetapkan Struktur Organisasi
                        {{ $data_raw->sub_events->get_position->sub_holding->name_subholding }};</li>
                    <li>
                        bahwa dalam rangka meningkatkan daya guna dan hasil guna kinerja karyawan untuk mendukung
                        pencapaian tujuan perusahaan perlu ditunjuk Pejabat untuk Jabatan Level BOD-1 di
                        {{ $data_raw->sub_events->get_position->sub_holding->name_subholding }};
                    </li>
                    <li>
                        bahwa sehubungan dengan hal tersebut di atas, perlu ditetapkan Pejabat untuk Jabatan Level BOD-1
                        di {{ $data_raw->sub_events->get_position->sub_holding->name_subholding }} dalam suatu
                        Keputusan Direksi;
                    </li>
                </ol>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: left;">Mengingat</td>
            <td style="vertical-align: top; text-align: left;">:</td>
            <td>
                <ol style="margin-top: 0;">
                    <li>
                        Peraturan Pemerintah Nomor 45 Tahun 2005 tentang Pendirian, Pengurusan, Pengawasan, dan
                        Pembubaran Badan Usaha Milik Negara (Lembaran Negara RI Tahun 2005 Nomor 117, Tambahan Lembaran
                        Negara RI Nomor 4556);
                    </li>
                    <li>
                        Anggaran Dasar PT Aviasi Pariwisata Indonesia (Persero) sebagaimana dituangkan dalam Akta
                        Notaris Desman, S.H., M.HUM, Nomor 23 tanggal 09 Agustus 2021 yang telah disetujui dan
                        ditetapkan dengan Keputusan Menteri Hukum dan Hak Asasi Manusia Nomor AHU-0044775.AH.01.02 Tahun
                        2021 tanggal 19 Agustus 2021;
                    </li>
                    <li>
                        Surat Keputusan Menteri BUMN selaku Rapat Umum Pemegang Saham Perusahaan Perseroan (Persero) PT
                        Aviasi Pariwisata Indonesia Nomor SK-336/MBU/10/2021 tanggal 04 Oktober 2021.
                    </li>
                    <li>
                        Peraturan Direksi PT Aviasi Pariwisata Indonesia (Persero) Nomor KEP.002/INJOURNEY/11/2021
                        tanggal 16 November 2021 tentang Organisasi Perusahaan PT Aviasi Pariwisata Indonesia (Persero).
                    </li>
                </ol>
            </td>
        </tr>
    </table>
    <div class="page-break"></div>
    <p class="text-center">MEMUTUSKAN:</p>
    <table>
        <tr>
            <td style="vertical-align: top; text-align: left;">Menetapkan</td>
            <td style="vertical-align: top; text-align: left;">:</td>
            <td>
                KEPUTUSAN DIREKSI PT AVIASI PARIWISATA INDONESIA (PERSERO) <br>
                TENTANG PENEMPATAN KARYAWAN DALAM JABATAN <br>
                {{ strtoupper($data_raw->sub_events->get_position->name_position) }} DI
                {{ strtoupper($data_raw->sub_events->get_position->sub_holding->name_subholding) }}.
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: left;">KESATU</td>
            <td style="vertical-align: top; text-align: left;">:</td>
            <td>
                <table>
                    <tr>
                        <td colspan="3">Menunjuk:</td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{ $data_raw->employee->personnel_number }}</td>
                    </tr>
                    <tr>
                        <td>Jabatan Saat Ini</td>
                        <td>:</td>
                        <td>{{ $data_raw->employee->positions->name_position }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: left;">KEDUA</td>
            <td style="vertical-align: top; text-align: left;">:</td>
            <td>
                Mengangkat Karyawan sebagaimana tersebut pada diktum KESATU sebagai
                {{ $data_raw->sub_events->get_position->name_position }}
                {{ $data_raw->sub_events->get_position->sub_holding->name_subholding }}.
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: left;">KETIGA</td>
            <td style="vertical-align: top; text-align: left;">:</td>
            <td>
                {{ $data_raw->sub_events->get_position->name_position }} memiliki tugas, fungsi, tanggung jawab dan
                kewenangan rutin dan strategis sebagaimana Job Profile yang tertuang dalam Peraturan Direksi tentang
                Organisasi Perusahaan.
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: left;">KEEMPAT</td>
            <td style="vertical-align: top; text-align: left;">:</td>
            <td>
                Keputusan ini disampaikan kepada yang bersangkutan untuk diketahui dan dilaksanakan.
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: left;">KELIMA</td>
            <td style="vertical-align: top; text-align: left;">:</td>
            <td>
                Apabila di kemudian hari ternyata terdapat kekeliruan dalam Keputusan ini, akan dilakukan perbaikan
                sebagaimana mestinya.
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: left;">KEENAM</td>
            <td style="vertical-align: top; text-align: left;">:</td>
            <td>
                Keputusan ini berlaku sejak tanggal ditetapkan.
            </td>
        </tr>
    </table>
    <div style="width: 250px; float:right; margin-right: 15%; margin-top: 5%;">
        <table>
            <tr>
                <td>Ditetapkan di</td>
                <td>:</td>
                <td>Jakarta</td>
            </tr>
            <tr>
                <td>Pada Tanggal</td>
                <td>:</td>
                <td>{{ date('d/m/Y') }}</td>
            </tr>
        </table>
        <hr>
        <p class="text-center">
            a.n. DIREKSI <br>
            DIREKTUR UTAMA <br>
            <br>
            <br>
            <br>
            <br>
            DONY OSKARIA
        </p>
    </div>
    <div style="clear:both">
        <p>
            Salinan Keputusan ini <br>
            <u>Disampaikan kepada Yth.:</u> <br>
            <br>
            1.Wakil Direktur Utama; <br>
            2.Direktur SDM & Digital; <br>
            @if ($data_raw->employee->positions->sub_holding->name_subholding == $data_raw->sub_events->get_position->sub_holding->name_subholding)
                3.Direktur Utama {{ $data_raw->employee->positions->sub_holding->name_subholding }}, <br>
                4.Karyawan yang berkepentingan. <br>
            @else
                3.Direktur Utama {{ $data_raw->employee->positions->sub_holding->name_subholding }}, <br>
                4.Direktur Utama {{ $data_raw->sub_events->get_position->sub_holding->name_subholding }}, <br>
                5.Karyawan yang berkepentingan. <br>
            @endif
            <hr>
        </p>
    </div>
</body>

</html>
