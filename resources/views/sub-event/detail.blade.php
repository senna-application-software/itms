{{-- Extends layout --}}
@extends('layout.default')

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .select2 {
            width: 100% !important;
        }

    </style>
@endsection

{{-- Content Section --}}
@section('page_toolbar')
    {{-- kalo di subheader mau ditambahkan button action taruh di sini --}}
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mb-5">
            <div class="card py-4 px-8">
                <div class="d-flex justify-content-between">
                    <h4 class="align-self-center m-0 p-0">{{ $nm_posisi->name_position }}
                        <span class="text-muted font-size-sm"><br>{{ $nm_posisi->name_subholding }}
                            {{ $nm_posisi->area_position }}</span>
                    </h4>
                    <div>
                        <a href="{{ route('sub-event.index') }}" class="btn btn-secondary">Back</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 mb-5">
            <div class="card card-custom">
                <div class="card-body">
                    <h7>Jumlah Talent Committee: {{ $data_tcom->count() }}</h7>
                    {{-- <div class="overflow-auto"> --}}
                    <table class="table table-bordered table-hover" id="table-sub">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Position</th>
                                <th>Organisasi</th>
                                <th>Area</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_tcom as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->nik }}</td>
                                    <td>{{ $row->employees->personnel_number }}</td>
                                    <td>{{ $row->employees->positions->name_position }}</td>
                                    <td>{{ $row->employees->positions->get_sub_holding->name_subholding }}</td>
                                    <td>{{ $row->employees->positions->master_area->name_area }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
        @if (!$show_engage)
            <div class="col-12">
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Proses Saat Ini: {{ $status }}</h6>
                        <h7>Jumlah Talent: {{ count($data) }}</h7>
                        {{-- <div class="overflow-auto"> --}}
                        <table class="table table-bordered table-hover" id="table-sub">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row['nama'] }}</td>
                                        <td>{{ $row['proses'] }}
                                        </td>
                                        <td style="width: 20%">
                                            @if ($row['proses'] == 'Selected')
                                                @if (!$row['is_email'])
                                                    <button class="btn btn-primary"
                                                        onclick="sendMail('{{ $row['id'] }}', $(this))">Kirim
                                                        Notifikasi</button>
                                                @else
                                                    Pakta Integritas dan Keputusan Direksi telah dikirim
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- </div> --}}
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-12 mb-5">
                <div class="card card-custom card-stretch gutter-b">
                    <div class="card-body d-flex p-0">
                        <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat"
                            style="background-position: calc(100% + 0.5rem) bottom; background-size: 50% auto; background-image: url( {{ asset('media/svg/humans/custom-1.svg') }} )">
                            <h2 class="text-dark pb-5 font-weight-bolder">Info.</h2>
                            <p class="text-dark-50 pb-5 font-size-h5">Event ini Tidak memiliki aspirasi dari karyawan.
                                <br>Anda dapat menghapus event ini
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/event/event.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    @if (session('status') == 'success')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
                $(".select-search").select2();
            });
        </script>
    @endif

    @if (session('status') == 'error')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
            });
        </script>
    @endif
    <script>
        function swalfire(title, text, icon) {
            Swal.fire({
                title: title,
                text: text,
                icon: icon,
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        }

        function sendMail(id, context) {
            Swal.fire({
                title: "Apakah Anda Yakin ?",
                text: "Anda akan mengirimkan email hasil Talent Selection dan Pakta Integritas",
                icon: "question",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "Yakin",
                cancelButtonText: "Batal",
                reverseButtons: true,
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-secondary"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        processing: true,
                        serverSide: true,
                        url: "{{ route('sub-event.send_mail_pakta') }}",
                        processing: true,
                        serverSide: true,
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: id
                        },
                        success: function(response) {
                            if (response) {
                                context.parent().html('Pakta Integritas dan Keputusan Direksi telah dikirim');
                                swalfire('Sukses', 'Berhasil Mengirim Email Dan Pakta Integritas',
                                    'success');
                            } else {
                                swalfire('Gagal', 'Gagal Mengirim Email Dan Pakta Integritas',
                                    'failed');
                            }
                        }
                    });
                }
            });
        }
    </script>
@endsection
