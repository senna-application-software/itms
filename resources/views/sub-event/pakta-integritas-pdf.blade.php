<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
            text-align: justify;
            text-justify: inter-word;
        }

        @page {
            margin: 2cm;
        }

        header {
            position: fixed;
            top: -10px;
            left: 0px;
            right: 0px;
            height: 40px;
        }

        footer {
            position: fixed;
            bottom: -18px;
            left: 0px;
            right: 0px;
            height: 20px;
        }

        .text-center {
            text-align: center;
        }

        .w-100 {
            width: 100%;
        }

    </style>
</head>

<body>
    <div class="text-center">
        <img src="{{ public_path('media/logos/logo-injourney.png') }}" style="height: 65px; width: 250px;">
    </div>
    <h3 class="text-center" style="margin-top: 5%">PAKTA INTEGRITAS</h3>
    <p>Saya yang bertanda tangan dibawah ini :</p>
    <table class="w-100">
        <tr>
            <td>Nama</td>
            <td style="font-weight: bold">: {{ $data_raw->employee->personnel_number }}</td>
        </tr>
        <tr>
            <td>Jabatan</td>
            <td style="font-weight: bold">: {{ $data_raw->sub_events->get_position->name_position }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="font-weight: bold">: {{ $data_raw->sub_events->get_position->master_area->name_area }}</td>
        </tr>
    </table>
    <h4 class="text-center">MENYATAKAN :</h4>
    <p style="font-size: 1rem;">
        Bahwa saya akan bekerja dengan sungguh-sungguh dan penuh tanggung jawab dengan menjunjung tinggi prinsip-prinsip
        profesionalisme dan kejujuran demi meningkatkan dan menjaga nama baik Perusahaan. <br>
        <br>
        Bahwa saya tidak akan melakukan transaksi dalam bentuk apapun, baik langsung maupun tidak langsung, dimana saya
        atau keluarga saya mempunyai kepentingan untuk memperoleh manfaat karenanya. <br>
        <br>
        Bahwa saya akan memegang teguh rahasia Perusahaan dalam bentuk apapun dengan sebenar-benarnya. <br>
        <br>
        Bahwa saya akan melaksanakan sepenuhnya prinsip-prinsip Good Corporate Governance yang menekankan pada asas
        transparansi, akuntabilitas, responsibilitas, independensi, dan fairness. <br>
        <br>
        Saya bersedia untuk diberhentikan sewaktu-waktu, apabila menurut pertimbangan Direksi PT Aviasi Pariwisata
        Indonesia (Persero), saya tidak mentaati Pernyataan Jabatan ini. <br>
        <br>
        Saya bertanggung jawab sepenuhnya atas kebenaran dari hal-hal yang saya nyatakan disini, demikian pula akan
        bersedia bertanggung jawab baik secara perdata maupun pidana, apabila laporan dan pernyataan ini tidak sesuai
        dengan keadaan sebenarnya. <br>
        <br>
        Demikian Pernyataan Jabatan ini saya buat untuk dipergunakan semestinya.
        <br>
        <br>
        Jakarta, {{ tanggal_indo(date('Y-m-d')) }}
        <br>
        {{ $data_raw->sub_events->get_position->name_position }},
        <br>
        <br>
        <br>
        <br>
        <br>
        <span style="font-weight: bold">{{ $data_raw->employee->personnel_number }}</span>
    </p>
</body>

</html>
