{{-- Extends layout --}}
@extends('layout.default')

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .select2 {
            width: 100% !important;
        }

    </style>
@endsection

{{-- Content Section --}}
@section('page_toolbar')
    {{-- kalo di subheader mau ditambahkan button action taruh di sini --}}
@endsection

@section('modal')
    <!-- modal create -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Event Position</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>

                <form class="form" method="post" action="{{ route('sub-event.create') }}" id="addEvtPost">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-bold nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#event_add_panel">Event
                                            Detail</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#event_add_tcom_panel"
                                            id="tcomBtn">
                                            Talent Committee
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-12 pt-5">
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="event_add_panel" role="tabpanel"
                                        aria-labelledby="event_add_panel">
                                        <div class="row">
                                            <div class="form-group col-12">
                                                <label>Sourcing Type</label>
                                                <select class="form-control select-search" name="sourcing_type">
                                                    <option value="0">Internal</option>
                                                    <option value="1">External</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-12">
                                                <label>Tipe Posisi</label>
                                                <select name="vacant_type" class="form-control select-search"
                                                    id="select-type" autocomplete="off" required>
                                                    <option value="incumbent" selected readonly>Choose Type of Event
                                                        Position
                                                    </option>
                                                    <option value="incumbent">All Position</option>
                                                    <option value="vacant">Vacant Position</option>
                                                    <option value="vacant_soon">Vacant Soon</option>
                                                    <option value="higher_3year">Positions >3 Year</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-12">
                                                <label>Nama Sub Holding</label>
                                                <select name="subholding" class="form-control select-search"
                                                    id="select-subholding" autocomplete="off">
                                                    <option value="-" disabled selected>- List Data -</option>
                                                    @foreach ($sub_holding as $item)
                                                        <option value="{{ $item->code_subholding }}">
                                                            {{ $item->name_subholding }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group col-12">
                                                <label>Nama Direktorat</label>
                                                <select name="direktorat" class="form-control select-search"
                                                    id="select-direktorat" autocomplete="off">
                                                    <option value="-" selected>- List Data -</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-12">
                                                <label>Nama Group</label>
                                                <select name="group" class="form-control select-search" id="select-group"
                                                    autocomplete="off">
                                                    <option value="-" selected>- List Data -</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-12">
                                                <label>Nama Divisi</label>
                                                <select name="divisi" class="form-control select-search" id="select-divisi"
                                                    autocomplete="off">
                                                    <option value="-" selected>- List Data -</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-12">
                                                <label>Nama Posisi</label>
                                                <select name="position" class="form-control select-search"
                                                    id="select-position" autocomplete="off">
                                                    <option value="-" selected>- List Data -</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-12">
                                                <label>Total Eligible Employee</label>
                                                <input type="text" class="form-control" name="employee_eligible"
                                                    id="employee_eligible" autocomplete="off" readonly>
                                            </div>

                                            <div class="form-group col-12">
                                                <label>Nama Event</label>
                                                <input type="text" class="form-control" name="name"
                                                    placeholder="Masukan nama event" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="event_add_tcom_panel" role="tabpanel"
                                        aria-labelledby="event_add_tcom_panel">
                                        <div class="row mb-5">
                                            <div class="col-md-10">
                                                <select class="form-control talent_com" name="talent_com[]"
                                                    onchange="checker($(this))" required>
                                                </select>
                                                <span class="text-muted font-size-sm">*Required</span>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-danger" type="button"
                                                    onclick="$(this).parent().parent().remove()">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="placeholdertcom" hidden></div>
                                        <button type="button" class="btn btn-primary mt-5 btn-block" onclick="addTcom()">Add
                                            Talent Committee
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal"
                            id="clsAddBtn">Close</button>
                        <button type="button" class="btn btn-primary" id="sbmtAddBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card card-custom mb-5">
        <div class="card-body p-0">
            <div class="wizard wizard-1" id="kt_wizard" data-wizard-state="first" data-wizard-clickable="false">
                <!--begin::Wizard Nav-->
                <div class="wizard-nav border-bottom">
                    <div class="wizard-steps p-8 p-lg-10">
                        <!--begin::Wizard Step 1 Nav-->
                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                            <div class="wizard-label">
                                {{ Metronic::getSVG('media/svg/icons/Navigation/Check.svg', 'svg-icon-4x svg-icon-muted') }}
                                <h3 class="wizard-title">Create Event</h3>
                            </div>
                            <span class="svg-icon svg-icon-xl wizard-arrow">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <rect fill="#000000" opacity="0.3"
                                            transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                            x="11" y="5" width="2" height="14" rx="1"></rect>
                                        <path
                                            d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                            fill="#000000" fill-rule="nonzero"
                                            transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)">
                                        </path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </div>
                        <!--end::Wizard Step 1 Nav-->
                        <!--begin::Wizard Step 2 Nav-->
                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                            <div class="wizard-label">
                                {{ Metronic::getSVG('media/svg/icons/Navigation/Check.svg', 'svg-icon-4x svg-icon-primary') }}
                                <h3 class="wizard-title">Update Vacant Position</h3>
                            </div>
                            <span class="svg-icon svg-icon-xl wizard-arrow">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <rect fill="#000000" opacity="0.3"
                                            transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                            x="11" y="5" width="2" height="14" rx="1"></rect>
                                        <path
                                            d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                            fill="#000000" fill-rule="nonzero"
                                            transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)">
                                        </path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </div>
                        <!--end::Wizard Step 2 Nav-->
                        <!--begin::Wizard Step 3 Nav-->
                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                            <div class="wizard-label">
                                {{ Metronic::getSVG('media/svg/icons/Navigation/Waiting.svg', 'svg-icon-4x svg-icon-muted') }}
                                <h3 class="wizard-title">Update Employee Eligible</h3>
                            </div>
                            <span class="svg-icon svg-icon-xl wizard-arrow">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <rect fill="#000000" opacity="0.3"
                                            transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                            x="11" y="5" width="2" height="14" rx="1"></rect>
                                        <path
                                            d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                            fill="#000000" fill-rule="nonzero"
                                            transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)">
                                        </path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </div>
                        <!--end::Wizard Step 3 Nav-->
                        <!--begin::Wizard Step 4 Nav-->
                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                            <div class="wizard-label">
                                {{ Metronic::getSVG('media/svg/icons/Navigation/Waiting.svg', 'svg-icon-4x svg-icon-muted') }}
                                <h3 class="wizard-title">Talent Planning</h3>
                            </div>
                            <span class="svg-icon svg-icon-xl wizard-arrow">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <rect fill="#000000" opacity="0.3"
                                            transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                            x="11" y="5" width="2" height="14" rx="1"></rect>
                                        <path
                                            d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                            fill="#000000" fill-rule="nonzero"
                                            transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)">
                                        </path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </div>
                        <!--end::Wizard Step 4 Nav-->
                        <!--begin::Wizard Step 5 Nav-->
                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                            <div class="wizard-label">
                                {{ Metronic::getSVG('media/svg/icons/Navigation/Waiting.svg', 'svg-icon-4x svg-icon-muted') }}
                                <h3 class="wizard-title">Talent Acquisition</h3>
                            </div>
                            <span class="svg-icon svg-icon-xl wizard-arrow">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <rect fill="#000000" opacity="0.3"
                                            transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                            x="11" y="5" width="2" height="14" rx="1"></rect>
                                        <path
                                            d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                            fill="#000000" fill-rule="nonzero"
                                            transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)">
                                        </path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </div>
                        <!--end::Wizard Step 5 Nav-->
                        <!--begin::Wizard Step 5 Nav-->
                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                            <div class="wizard-label">
                                {{ Metronic::getSVG('media/svg/icons/Navigation/Waiting.svg', 'svg-icon-4x svg-icon-muted') }}
                                <h3 class="wizard-title">Completed</h3>
                            </div>
                            <span class="svg-icon svg-icon-xl wizard-arrow last">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <rect fill="#000000" opacity="0.3"
                                            transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                            x="11" y="5" width="2" height="14" rx="1"></rect>
                                        <path
                                            d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                            fill="#000000" fill-rule="nonzero"
                                            transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)">
                                        </path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </div>
                        <!--end::Wizard Step 5 Nav-->
                    </div>
                </div>
                <!--end::Wizard Nav-->
            </div>
        </div>
        <!--end::Wizard-->
    </div>

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">
                    <div class="text-muted pt-2 font-size-sm"></div>
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#exampleModal">
                    <span class="svg-icon svg-icon-md">
                        <i class="fa fa-plus-circle"></i>
                    </span>
                    Tambah Event Position
                </a>
            </div>
        </div>

        <div class="card-body">
            {{-- <div class="overflow-auto"> --}}
            <table class="table table-bordered table-hover" id="table-sub">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Event</th>
                        <th>Nama Posisi</th>
                        <th>Tipe Vacant</th>
                        <th>Status Event</th>
                        <th>Sourcing Type</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
            {{-- </div> --}}
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/event/event.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        let tmpAddPos, tmpTcomDt;
        $(document).ready(function() {
            $('.select-search').select2();
            selectData("subholding", $("#select-subholding"));
        });

        $('#table-sub').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: "{{ route('sub-event.datatable') }}"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'name_position',
                    name: 'name_position'
                },
                {
                    data: 'vacant_type',
                    name: 'vacant_type'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'sourcingType',
                    name: 'sourcingType'
                },
                {
                    data: 'button',
                    name: 'button'
                },
            ]
        });



        $("#select-position").change(function() {
            var id_position = $(this).val();

            $.ajax({
                method: 'GET',
                processing: true,
                serverSide: true,
                url: "{{ url('/sub-event/get-attribute-position') }}" + "/" + id_position,
                success: function(response) {
                    $('#employee_eligible').val(response.eligible);
                },
                error: function() {
                    Swal.fire('Sorry', 'Something Went Wrong !', 'error');
                }
            });
        });


        $("#select-type").change(function() {
            selectData("subholding", $("#select-subholding"));
        });

        $("#select-subholding").change(function() {
            selectData("directorat", $("#select-direktorat"), $(this).val());
        });

        $("#select-direktorat").change(function() {
            selectData("group", $("#select-group"), $("#select-subholding").val());
        });

        $("#select-group").change(function() {
            selectData("divisi", $("#select-divisi"), $("#select-subholding").val());
        });

        $("#select-divisi").change(function() {
            selectData("position", $("#select-position"), [$("#select-subholding").val(), $("#select-direktorat")
                .val(), $("#select-group").val(), $(this).val()
            ]);
        });


        function selectData(load, selector, selected = false) {
            KTApp.block('#addEvtPost', {
                overlayColor: '#000000',
                state: 'primary',
                message: 'Processing...'
            });
            if (selected != "-" || load == "subholding") {
                tipe = $("#select-type").val()
                $.ajax({
                    method: "POST",
                    processing: true,
                    serverSide: true,
                    url: "{{ route('sub-event.filter_positions') }}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        tipe: tipe,
                        selected: selected,
                        load: load,
                    },
                    success: function(res) {
                        html = `<option value="-" disabled selected>- List Data -</option>`;
                        if (load != "position") {
                            html += `<option value="" selected>All Data</option>`;
                        }
                        $.each(res, function(key, value) {
                            html += `<option value="${(value.code)}">${(value.name)}</option>`;
                        });
                        selector.html("")
                        selector.html(html);
                        // if (res.length > 0) {
                        if (res.length > 0 && load == "position") {
                            selector.val(res[0].code).trigger("change");
                        } else {
                            selector.val("").trigger("change");
                        }
                        KTApp.unblock("#addEvtPost");
                    }
                })
            } else {
                selector.html(`<option value="-">- List Data -</option>`)
                selector.val("-").trigger("change");
                KTApp.unblock("#addEvtPost");
            }
        }

        $("#tcomBtn").click(function(e) {
            e.preventDefault();
            KTApp.block('#addEvtPost', {
                overlayColor: '#000000',
                state: 'primary',
                message: 'Processing...'
            });
            let currPos = $("select[name=position]").val();
            if (tmpAddPos != currPos) {
                $.ajax({
                    type: "POST",
                    processing: true,
                    serverSide: true,
                    url: "{{ route('sub-event.get_talent_com') }}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        position: currPos
                    },
                    success: function(response) {
                        $("select[name='talent_com[]']").parent().parent().remove();
                        tmpTcomDt = response.data;
                        tmpAddPos = currPos;
                        $.each(response.default, function(indexInArray, valueOfElement) {
                            addTcom();
                            $("select[name='talent_com[]']").eq(indexInArray).val(valueOfElement
                                .nik).trigger('change');
                        });
                        KTApp.unblock("#addEvtPost");
                    }
                });
            } else {
                KTApp.unblock("#addEvtPost");
            }
        });

        function addTcom() {
            let hatml = "";
            if ($("select[name='talent_com[]']").length) {
                hatml = '<div class="row mb-5">';
            } else {
                hatml = '<div class="row">';
            }
            hatml += '<div class="col-md-10">' +
                '<select class="form-control talent_com" name="talent_com[]" onchange="checker($(this))" required>' +
                '<option value="" disabled selected>-- LIST TALENT COMMITTEE --</option>';
            $.each(tmpTcomDt, function(indexInArray, valueOfElement) {
                hatml += "<option value='" + valueOfElement.prev_persno +
                    "'>" + "[" + valueOfElement.name_position + "][" + valueOfElement.name_subholding + "] " +
                    valueOfElement.personnel_number +
                    "</option>";
            });
            hatml += '</select>' +
                '<span class="text-muted font-size-sm">*Required</span>' +
                '</div>' +
                '<div class="col-md-2">' +
                '<button class="btn btn-danger" type="button" onclick="$(this).parent().parent().remove()"><i class="fa fa-trash"></i></button>' +
                '</div>';
            hatml += '</div>';
            $(hatml).insertBefore(".placeholdertcom");
            $("select[name='talent_com[]']").select2();
        };

        function swalfire(title, text, icon) {
            Swal.fire({
                title: title,
                text: text,
                icon: icon,
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        }

        $("#sbmtAddBtn").click(function(e) {
            e.preventDefault();
            if ($("select[name='talent_com[]']").length && $("select[name='talent_com[]']").val()) {
                $("#addEvtPost").submit();
            } else {
                swalfire('Failed', 'Minimal Talent Committee Is 1', 'error');
            }
        });

        function checker(ctx) {
            if (ctx.val() && $(".talent_com").length > 1) {
                $.each($(".talent_com").not(ctx), function() {
                    if (ctx.val() == $(this).val()) {
                        swalfire('Failed', 'Talent Committee Tidak Boleh Sama !', 'error');
                        ctx.val("").trigger("change");
                    }
                });
            }
        }
    </script>


    @if (session('status') == 'success')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
                $(".select-search").select2();
            });
        </script>
    @endif

    @if (session('status') == 'error')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
            });
        </script>
    @endif
@endsection
