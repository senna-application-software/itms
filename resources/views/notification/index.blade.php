{{-- Extends layout --}}
@extends('layout.default')

{{-- Content Section --}}
@section('page_toolbar')
    {{-- kalo di subheader mau ditambahkan button action taruh di sini --}}
@endsection

@section('modal')
@endsection

@section('content')
    <div class="card card-custom mb-3">
        <div class="card-body" style="margin: -10px">
            <form action="" class="mt-5">
                <div class="form-group row">
                    <div class="col-md-2">
                        <input type="text" class="form-control" placeholder="Nama Pegawai...">
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" placeholder="Job Family...">
                    </div>
                    <div class="col-md-2">
                        <select name="" id="member" class="form-control">
                            <option value="">Pilih Member</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="" id="grade" class="form-control">
                            <option value="">Pilih Grade</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <a href="#" class="btn btn-primary font-weight-bolder">
                            <span class="svg-icon svg-icon-md">
                                <i class="fa fa-search"></i>
                            </span>
                            Cari
                        </a>
                    </div>

                </div>
            </form>
        </div>
    </div>
    <div class="card card-custom">


        <div class="card-body">
            {{-- <div class="overflow-auto"> --}}
            <table class="table table-bordered table-hover" id="table-notif">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Pegawai</th>
                        <th>Aspirasi</th>
                        <th>Candidates</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class="text-center"><button class="btn btn-sm btn-primary">Pilih Posisi</button></th>
                    </tr>
                </tbody>
            </table>
            {{-- </div> --}}
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/event/event.js') }}" type="text/javascript"></script>

    <script>
        $('#table-notif').DataTable({
            // processing: true,
            // serverSide: true,
            // responsive: true,
            // ajax: "{{ route('event.datatable') }}",
            // columns: [
            //     { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            //     { data: 'year', name: 'year'},
            //     { data: 'batch', name: 'batch'},
            //     { data: 'status', name: 'status'},
            //     { data: 'button', name: 'button'},
            // ]
        });

        $('#member').select2();
        $('#grade').select2();
    </script>


    @if (session('status') == 'success')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
            });
        </script>
    @endif

    @if (session('status') == 'error')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
            });
        </script>
    @endif
@endsection
