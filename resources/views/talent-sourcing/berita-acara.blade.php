<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
            * {
                font-family: Arial, Helvetica, sans-serif;
            }
            
            @page { margin: 2cm; }
            header { position: fixed; top: -10px; left: 0px; right: 0px; height: 40px; }
            footer { position: fixed; bottom: -18px; left: 0px; right: 0px; height: 20px; }

            table, th, td {
                font-size: 14px;
                text-align:left;
                padding: 2px;
            }

            table.comite { 
                border-spacing: 0;
                border-collapse: collapse;
                font-size: 10px;
            }

            table.kandidat { 
                border-spacing: 0;
                border-collapse: collapse;
                font-size: 14px;
                border: 1px solid #000;
                width: 100%;

            }
            
            table.kandidat td { 
                border-spacing: 0;
                border-collapse: collapse;
                font-size: 14px;
                border: 1px solid #000;
            }
            
            table.ttd { 
                border-spacing: 0;
                border-collapse: collapse;
                font-size: 14px;
                border: 1px solid transparent;
                width: 100%;
            }
        </style>
    </head>
    <body>
        <footer>Print date : {{ date('Y-m-d') }}</footer>
        <h3 style="text-align:center">
            BERITA ACARA KESEPAKATAN<br/>
            PEMILIHAN PESERTA TALENT SOURCING
        </h3>
        <p style="text-align:center">Nomor : {{ $nomor_ba }}</p>
        <br/>

        @php
            $tanggal = hari_ini();
        @endphp


        <p>Pada hari ini {{ $tanggal["nama_hari"] }} tanggal {{ $tanggal["tgl"] }}  
            Bulan {{ $tanggal["nama_bulan"] }} Tahun {{ $tanggal["thn"] }} ({{ $tanggal["tanggal"] }} ) 
            bertempat di PT Aviasi Pariwisata Indonesia (Persero), 
            Talent Committee yang bertanda tangan di bawah ini:
        </p>
        <br/>

        <table class="comite">
            <tr>
                <td style="width:100%" rowspan="3">1</td>
            </tr>
            <tr>
                <td style="width:20%">Nama</td>
                <td style="width:5%">:</td>
                <td style="width:80%">{{ $sub_event[0]->talent_com_1 }}</td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>{{ $sub_event[0]->talent_com_1 }}</td>
            </tr>

            <tr>
            <td style="width:10%" rowspan="3">2</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td>{{ $sub_event[0]->talent_com_2 }}</td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>{{ $sub_event[0]->talent_com_2 }}</td>
            </tr>

            <tr>
            <td style="width:10%" rowspan="3">3</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td>{{ $sub_event[0]->talent_com_3 }}</td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>{{ $sub_event[0]->talent_com_3 }}</td>
            </tr>
        
        </table>
        <br/>

        </p>
            Sesuai dengan hasil pembahasan dalam Talent Committee Meeting, 
            telah disepakati Peserta Talent Sourcing untuk pengisian jabatan 
            {{ $sub_event[0]->name_position }} {{ $sub_event[0]->name_subholding }} adalah sebagai berikut:
        </p>

        <table class="kandidat">
            <tr>
                <td>No.</td>
                <td>NAMA</td>
                <td>NIP</td>
                <td>JABATAN SAAT INI</td>
                <td>NILAI EQS</td>
                <td>REKOMENDASI TALENT SOURCING</td>
            </tr>
            
            @foreach ($sub_event as $index => $se)
                <tr>
                    <td>{{$index+1}}</td>
                    <td>{{ $se->personnel_number }}</td>
                    <td>{{ $se->prev_persno }}</td>
                    <td>{{ $se->position_name }}</td>
                    <td>{{ nilai_eqs($se->prev_persno) }}</td>
                    <td>{{ $se->talent_cluster }}</td>
                </tr>
                
            @endforeach
        </table>

        </p>
            Demikian berita acara kesepakatan ini dibuat dengan sebenarnya dan ditandatangani 
            pada hari dan tanggal tersebut untuk dapat dipergunakan sebagaimana mestinya.
        </p>

        <table class="ttd">
            <tr>
                <td>Jabatan</td>
                <td>Jabatan</td>
                <td>Jabatan</td>
            </tr>
            <tr>
                <td><br/><br/><br/><br/></td>
                <td><br/><br/><br/><br/></td>
                <td><br/><br/><br/><br/></td>
            </tr>
            <tr>
                <td>{{ $sub_event[0]->talent_com_1 }}</td>
                <td>{{ $sub_event[0]->talent_com_2 }}</td>
                <td>{{ $sub_event[0]->talent_com_3 }}</td>
            </tr>
        </table>

    </body>
</html>