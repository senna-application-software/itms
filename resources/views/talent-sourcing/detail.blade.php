{{-- Extends layout --}}
@extends('layout.default')

@section('modal')
{{-- Modal Add Employee External --}}
<div class="modal fade" id="addPegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add External Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>

            <form method="post" action="{{ route('talent-sourcing.addEmployeeSourcing') }}">
                @csrf
                <input type="hidden" name="eventId" value="{{ $sub_event->id_event }}">
                <input type="hidden" name="subEventId" value="{{ $sub_event->id }}">
                <input type="hidden" name="codePosition" value="{{ $sub_event->code_position }}">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12">
                            <label>Employee</label>
                            <select name="employee" id="employee" class="form-control select2-global" required>
                                <option></option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary mr-2" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-secondary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- End Modal add Employee external --}}

{{-- Modal Add Employee Internal --}}
<div class="modal fade" id="addPegawaiInternal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Internal Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>

            <form method="post" action="{{ route('talent-sourcing.addEmployeeSourcing') }}">
                @csrf
                <input type="hidden" name="eventId" value="{{ $sub_event->id_event }}">
                <input type="hidden" name="subEventId" value="{{ $sub_event->id }}">
                <input type="hidden" name="codePosition" value="{{ $sub_event->code_position }}">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12">
                            <label>Employee</label>
                            <select name="employee" id="employeeInternal" class="form-control select2-global" required>
                                <option></option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary mr-2" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-secondary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- End Modal add Employee Internal --}}
@endsection

{{-- Content --}}
@section('content')
<input type="hidden" id="eventId" value="{{ $sub_event->id_event }}">
<input type="hidden" id="subEventId" value="{{ $sub_event->id }}">
<input type="hidden" id="codePosition" value="{{ $sub_event->code_position }}">

<div class="col-12 mb-5">
    <div class="card px-5 py-3">
        <div class="d-flex">
            <h4 class="align-self-center mr-auto p-2">
                {{ $sub_event->name_position }} 
                <span class="label font-weight-bold label-lg {{ $sub_event->sourcing_type == 1 ? 'label-light-success' : 'label-light-primary' }} label-inline">
                    {{ $sub_event->sourcing_type == 1 ? 'External' : 'Internal' }}
                </span>
                <small class="text-muted">{{ $sub_event->name_subholding }} - [{{ $sub_event->name_area }}]</small>
            </h4>
            <div>
             <button class="btn btn-success" data-select-all-click="boards">Pilih Semua</button>
            <button class="btn btn-primary" id="done_sourcing">Done Sourcing</button>

            @if($sub_event->sourcing_type == 1) {{-- button akan muncul jika type sourcingnya itu external --}}
            <a href="#" class="btn btn-warning font-weight-bolder ml-2" data-toggle="modal" data-target="#addPegawai">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>
                Add Employee
            </a>
            @elseif(count($data) == 0 && $sub_event->sourcing_type == 0) {{-- button akan muncul jika type sourcingnya itu internal dan datanya kosong --}}
            <a href="#" class="btn btn-warning font-weight-bolder ml-2" data-toggle="modal" data-target="#addPegawaiInternal">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>
                Add Employee
            </a>
            @endif
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <form action="{{ route('talent-sourcing.done_sourcing') }}" method="POST" id="form_sourcing">
        @csrf
        <div class="row">
            <input type="hidden" name="sub_event" value="{{ $sub_event->id }}">
            <input type="hidden" name="dataStringify" id="dataStringify">
            @foreach($data as $index => $item)
                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12" >
                    <div class="card card-custom gutter-b card-stretch">
                        <div class="card-body pt-4 d-flex flex-column">
                            <div class="d-flex justify-content-between my-5" data-select-all="boards">
                                {{-- <label class="form-check form-check-custom form-check-solid align-self-start">
                                    <input class="form-check-input h-25px w-25px" name="employee_selection[]" data-name="{{ $item->employee->personnel_number }}" type="checkbox" value="{{ $item->employee->prev_persno }}"/>
                                </label> --}}
                                @php
                                    $ceknil = DB::table('employees_status_update')
                                        ->where('nik', $item->nik)
                                        ->where('event_window_time', $sub_event->id_event)
                                        ->first();
                                @endphp
                                <label class="form-check form-check-custom form-check-solid align-self-start">
                                    <input class="form-check-input h-25px w-25px" id="employeeSelection{{ $item->nik }}" name="employee_selection[]" type="checkbox"
                                        data-index="{{ $item->nik }}"
                                        data-talent-aspiration-id="{{ $item->talent_aspiration_id }}"
                                        data-name="{{ $item->employee->personnel_number }}"
                                        value="{{ $item->employee->prev_persno }}" @if($ceknil == null) onclick="swalfiredenied('Warning', 'Employee Belum Mengisi Profile !', 'warning', $(this));" @endif />
                                    @if($ceknil == null)
                                        <i class="fas fa-exclamation-triangle text-danger ml-5 mt-3"></i>
                                    @endif
                                </label>
                                
                                <div class="symbol symbol-circle symbol-lg-100">
                                    <img src="{{ get_employee_pict($item->employee->prev_persno) }}" alt="image">
                                </div>
                            </div>
                            <div class="mt-3 mb-5">
                                <h3 class="text-primary font-weight-bold text-hover-warning font-size-h4 mb-0 open-career-card" type="button" data-id="{{$item->id}}" data-nik="{{$item->employee->prev_persno}}" style="-webkit-appearance: none;">{{ $item->employee->personnel_number }}</h3>
                                <p class="text-muted font-weight-bold">{{ $item->employee->master_posisi->name_position ?? "" }}</p>
                                <p class="font-weight-bold p-0 m-0">{{ $item->employee->positions->sub_holding->name_subholding ?? "" }}</p>
                                <p class="font-weight-bold p-0 m-0">{{ $item->employee->positions->master_area->name_area ?? "" }}</p>
                                <div class="form-check mt-3 mb-3">
                                    <input class="form-check-input" type="checkbox" name="proposal[]" {{ $item->comitte_proposal == 1 ? 'checked' : '' }}  id="proposal{{ $item->nik }}" data-index="{{ $item->nik }}">
                                    <label class="form-check-label" for="proposal{{ $item->nik }}">
                                        Committee Proposal
                                    </label>
                                </div>
                                <h3 class="mb-4" data-nik-eqs="{{ $item->employee->prev_persno }}">EQS: {{ nilai_eqs($item->employee->prev_persno, false, $sub_event->id) }}</h3>
                                
                                @if($item->is_add_talent_sourcing == 1)
                                    @if($ceknil == null)
                                        <a href="{{ route('talent-sourcing.profile', ['nik' => $item->employee->prev_persno, 'urlPrevious' => url()->current()]) }}" class="btn btn-sm btn-warning">Profile</a>
                                    @endif

                                <button type="button" class="btn btn-sm btn-danger" id="deleteTalentAspiration" data-id="{{ $item->id }}">Delete</button>
                                @endif
                            </div>

                            <div class="row mt-auto">
                                @foreach ($item->employee->performance->slice(0, 3) as $performance)
                                <div class="col-md-4 mx-1 rounded p-1 text-center" style="background-color:#82E99F;min-height: 70px;max-width: 30%">
                                    <span>{{ $performance->year }}</span>
                                    <br>
                                    <span>{{ $performance->perfomance_category_align }}</span>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </form>
</div>

<!-- Employee Score Card -->
<div class="modal fade" id="xl-modal" tabindex="-1" role="dialog" aria-labelledby="xl-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Employee Career Card</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body data-modal" data-scroll="true" data-height="500">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary font-weight-bold" data-dismiss="modal" id="submit_assessment">Close</button>
            </div>
        </div>
    </div>
</div>

{{-- modal done profiling --}}
<div class="modal fade" id="modal_sourcing" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirm Talent Sourcing Results</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <form action="" method=" ">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr id="sourcing_head">
                            
                            </tr>
                        </thead>
                        <tbody id="sourcing_body">
                            
                        </tbody>
                    </table>
                    <input type="checkbox" checked required> <label>Kandidat yang terpilih sudah sesuai</label>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="done">Done</button>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link href="{{ asset('css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css"/>
<style>
    .select2 {
        width: 100% !important;
    }
</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/wizard/wizard-1.js') }}" type="text/javascript"></script>

@include('global.element.script_eqs_peremployee')
<script>
    // fungsi all checked
    $('[data-select-all-click]').click(function() {
        var selfAllSelector = $(this).data('select-all-click');
        boardInputs = $('[data-select-all='+selfAllSelector+'] input');
        boardInputs.prop('checked', !boardInputs.prop('checked') );
        }); 
    //fungsi click score card
    //submit
    $('#done_sourcing').on('click', function(){
        if($("input[name='employee_selection[]']:checked").length) {
            event.preventDefault();
            var listProfiling = $("input:checkbox:checked").map(function(){
                return $(this).attr("data-name");
            }).toArray();
            var html = "";
            var html_head = `<th scope="col">Choosen Candidates (`+listProfiling.length+`)</th>`;
            for (let p = 0; p < listProfiling.length; p++) {
                html += `<tr>
                            <td>`+ listProfiling[p] +`</td>
                            </tr>`;
            }
            $("#sourcing_head").html(html_head);
            $("#sourcing_body").html(html);


            $("#modal_sourcing").modal();

        } else {
            Swal.fire("Please Select 1 Candidates to Continue!", "", "warning");
        }
    })
    
    $('#done').on('click', function(){
        var manipulateDataSourcing = [];
        $("input[name='employee_selection[]']:checked").each(function(index){
            let employeeSelection = $('#employeeSelection'+$(this).val()),
                proposal = $('#proposal'+$(this).val());

                if (proposal.is(":checked") && (employeeSelection.attr('data-index') == proposal.attr('data-index'))) {
                    manipulateDataSourcing.push({
                        nik: employeeSelection.val(),
                        proposal: true,
                        talentAspirationId: employeeSelection.attr('data-talent-aspiration-id')
                    });
                } else {
                    manipulateDataSourcing.push({
                        nik: employeeSelection.val(),
                        proposal: false,
                        talentAspirationId: employeeSelection.attr('data-talent-aspiration-id')
                    });
                }
        });
        $('#dataStringify').val(JSON.stringify(manipulateDataSourcing));
        $('#form_sourcing').submit();
    });

    function swalfire(title, text, icon) {
            Swal.fire({
                title: title,
                text: text,
                icon: icon,
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        }

    function swalfiredenied(title, text, icon, context) {
        if(context.is(":checked")){
            Swal.fire({
                title: title,
                text: text,
                icon: icon,
                buttonsStyling: false,
                showDenyButton: true,
                confirmButtonText: "Continue",
                denyButtonText: "Cancel",
                customClass: {
                    confirmButton: "btn btn-primary",
                    denyButton: "btn btn-secondary"
                }
            }).then((result) => {
                if(result.isDenied) {
                    context.prop('checked', false);
                }
            });
        }
    }
</script>

<script type="text/javascript">
    const routeSelect2Employee = "{{ route('talent-sourcing.select2Employee') }}",
        csrfToken = "{{ csrf_token() }}",
        routeConstDeleteTalentAspiration = "{{ route('talent-sourcing.deleteTalentAspiration', ':id') }}";

    var routeDeleteTalentAspiration = "{{ route('talent-sourcing.deleteTalentAspiration', ':id') }}";
</script>

@if (session('status'))
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif

<script type="text/javascript" src="{{ asset('js/talent-sourcing/detail.js?nocache='.time()) }}"></script>
@endsection
