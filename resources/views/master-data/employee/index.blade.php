@extends('layout.default')

@section('styles')
<style type="text/css">
    .select2 {
        width: 100% !important;
    }
</style>
@endsection

@section('page_toolbar')
    <div class="card-toolbar">
        <a href="#" class="btn btn-success font-weight-bolder" data-toggle="modal" data-target="#modalImportEmployee">
            <span class="svg-icon svg-icon-md">
                <i class="fa fa-file-import"></i>
            </span>
            Import
        </a>

        <a href="#" class="btn btn-primary font-weight-bolder ml-2" id="btnModalAddEmployee" data-toggle="modal" data-target="#modalAddEmployee">
            <span class="svg-icon svg-icon-md">
                <i class="fa fa-plus-circle"></i>
            </span>
            Add Employee
        </a>
    </div>
@endsection

@section('modal')
{{-- Modal view data --}}
<div class="modal fade" id="modalViewEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View Data Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>

            <div class="modal-body">
                <div id="dataEmployee"></div>
            </div>
        </div>
    </div>
</div>
{{-- End Modal view data --}}

{{-- Modal add employee --}}
<div class="modal fade" id="modalAddEmployee" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <form method="post" action="{{ route('masterDataEmployee.store') }}">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>

                <div class="modal-body"></div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
{{-- End Modal add employee --}}

{{-- Modal Import Employee --}}
<div class="modal fade" id="modalImportEmployee" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <form method="post" action="{{ route('masterDataEmployee.importEmployee') }}" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Import Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>File</label>
                        <input type="file" name="file" class="form-control" required>
                    </div>
                </div>

                <div class="modal-footer">
                    <a href="{{ route('masterDataEmployee.templateImportEmployee') }}" class="btn btn-sm btn-success">Template</a>
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
{{-- End Modal Import Employee --}}
@endsection

@section('content')
<div class="card card-custom">
    <div class="card-body">
        {{-- Filter --}}
        <div class="form-row mb-5">
            <div class="col-lg-3">
                <select class="form-control filter" id="jobFamily">
                    <option></option>
                    @foreach($jobFamily as $value)
                    <option value="{{ $value->name_job_family }}">{{ $value->name_job_family }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-lg-3">
                <select class="form-control filter" id="subholding" autocomplete="off">
                    <option></option>
                    @foreach($subholding as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="col-lg-3">
                <select class="form-control filter" id="grade">
                    <option></option>
                    @foreach($grade as $value)
                    <option value="{{ $value->person_grade_align }}">{{ $value->person_grade_align }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-lg-3">
                <select class="form-control filter" id="talentCluster">
                    <option></option>
                    @foreach($talentCluster as $value)
                    <option value="{{ $value['key'] }}">{{ $value['val'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        {{-- End Filter --}}

        <table class="table table-bordered table-hover" id="tableEmployee">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Employee ID Number</th>
                    <th>Employee Name</th>
                    <th>Organization</th>
                    <th>Position</th>
                    <th>Area</th>
                    <th>Grade</th>
                    <th>Employee Type</th>
                    <th>Talent Cluster</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@section('scripts')
@if($errors->any())
    @php 
        $errorData = implode(" ", $errors->all());
    @endphp
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "Error",
                text: "{!! $errorData !!}",
                icon: "error",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif

@if (session('status'))
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif

<script type="text/javascript">
    const routeDataTable = "{{ route('masterDataEmployee.dataTableEmployee') }}",
        routeConstViewEmployee = "{{ route('masterDataEmployee.show', ':id') }}",
        routeConstDeleteEmployee = "{{ route('masterDataEmployee.destroy', ':id') }}",
        csrfToken = "{{ csrf_token() }}",
        routeSupplyDataFormEmployee = "{{ route('masterDataEmployee.supplyDataFormEmployee') }}";

    var routeViewEmployee = "{{ route('masterDataEmployee.show', ':id') }}",
        routeDeleteEmployee = "{{ route('masterDataEmployee.destroy', ':id') }}";
</script>
<script type="text/javascript" src="{{ asset('js/master-data/employee/index.js?nocache='.time()) }}"></script>
@endsection