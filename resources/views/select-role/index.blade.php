{{-- Extends layout --}}
@extends('layout.role')

{{-- Content --}}
@section('content')
    @include('select-role.content', ['class' => 'card-stretch gutter-b'])
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $("body").removeClass("aside-fixed").removeClass("aside-enabled").removeClass("subheader-enabled");
        $("#kt_subheader").remove();
        $(".btn-role").click(function(){
            $(".page-loader").css("display", "inherit")
            $(".page-loader").addClass("d-flex justify-content-center")
        })
        $(".topbar .dropdown").hide();
    });
</script>
@endsection