<div class="container justify-content-center">
    <div class="d-flex flex-row-fluid flex-column p-10 p-sm-30">
    <h1 class="text-center mb-16 p-8">Salam InJourney Bestie</h1>
    <div class="row">
        @php
            $color = ['#EC6A56', '#A5C144'];
            $text_col = ['text-white', 'text-white'];
            $btn_type = ['btn-white', 'btn-white'];
        @endphp
        @foreach (Sentinel::check()->roles as $y => $row)
            <div class="col-md-6">
                <div class="card card-custom card-stretch gutter-b">
                    <div class="card-body d-flex p-0">
                        <div class="flex-grow-1 p-12 pb-40 card-rounded flex-grow-1 bgi-no-repeat"
                            style="background-position: calc(100% + 0.5rem) bottom; background-size: 35% auto; background-image: url({{ asset('media/svg/humans/custom-11.svg') }}); background-color: {{ $color[$y] }}">
                            <p class="pt-10 pb-5 font-size-h3 font-weight-bolder line-height-lg {{ $text_col[$y] }}">
                                Login Sebagai {{ strtolower($row->name) == "executive" ? "Talent Committee" : $row->name }}
                            </p>
                            <form action="{{ route('role_switch_process') }}" method="post">
                                @csrf
                                <input type="hidden" name="role" value="{{ $row->slug }}" hidden>
                                <button type="submit" class="btn {{ $btn_type[$y]}} btn-md font-weight-bold py-2 px-6 btn-role">Klik di sini</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
</div>
