@if (config('layout_role.self.layout') == 'blank')
    <div class="d-flex flex-column flex-root">
        @yield('content')
    </div>
@else
    @include('layout.base-role._header-mobile')

    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-row flex-column-fluid page">

            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">

                @include('layout.base-role._header')

                <div class="content {{ Metronic::printClasses('content', false) }} d-flex flex-column flex-column-fluid" 
                    style="
                        background-repeat:no-repeat;
                        background-attachment:fixed;
                        background-position: right top; background-size: 40%; 
                        background-image: url('{{ asset('media/svg/shapes/abstract-2.svg') }}')" 
                    id="kt_content">
                    
                    @if (config('layout_role.subheader.display'))
                        @if (array_key_exists(config('layout_role.subheader.layout'), config('layout_role.subheader.layouts')))
                            @include(
                                'layout.partials.subheader._' . config('layout_role.subheader.layout')
                            )
                        @else
                            @include(
                                'layout.partials.subheader._' .
                                    array_key_first(config('layout_role.subheader.layouts'))
                            )
                        @endif
                    @endif

                    @include('layout.base-role._content')
                </div>

                @include('layout.base-role._footer')
            </div>
        </div>
    </div>

@endif

@if (config('layout_role.self.layout') != 'blank')

    @if (config('layout_role.extras.search.layout') == 'offcanvas')
        @include('layout.partials.extras.offcanvas._quick-search')
    @endif

    @if (config('layout_role.extras.notifications.layout') == 'offcanvas')
        @include(
            'layout.partials.extras.offcanvas._quick-notifications'
        )
    @endif

    @if (config('layout_role.extras.quick-actions.layout') == 'offcanvas')
        @include('layout.partials.extras.offcanvas._quick-actions')
    @endif

    @if (config('layout_role.extras.user.layout') == 'offcanvas')
        @include('layout.partials.extras.offcanvas._quick-user')
    @endif

    @if (config('layout_role.extras.quick-panel.display'))
        @include('layout.partials.extras.offcanvas._quick-panel')
    @endif

    @if (config('layout_role.extras.toolbar.display'))
        @include('layout.partials.extras._toolbar')
    @endif

    @if (config('layout_role.extras.chat.display'))
        @include('layout.partials.extras._chat')
    @endif

    @include('layout.partials.extras._scrolltop')

@endif

@hasSection('modal')
    @yield('modal')
@endif
