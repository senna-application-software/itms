<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ Metronic::printAttrs('html') }}
    {{ Metronic::printClasses('html') }}>

<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Title Section --}}
    <title>{{ config('app.name') }} | @yield('title', $page_title ?? '')</title>

    {{-- Meta Data --}}
    <meta name="description" content="@yield('page_description', $page_description ?? '')" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    {{-- Favicon --}}
    <link rel="shortcut icon" href="{{ asset(config('layout.resources_login.favicon')) }}" />

    {{-- Fonts --}}
    {{ Metronic::getGoogleFontsInclude() }}

    {{-- Global Theme Styles (used by all pages) --}}
    @foreach (config('layout.resources.css') as $style)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($style)) : asset($style) }}"
            rel="stylesheet" type="text/css" />
    @endforeach

    {{-- Layout Themes (used by all pages) --}}
    @foreach (Metronic::initThemes() as $theme)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($theme)) : asset($theme) }}"
            rel="stylesheet" type="text/css" />
    @endforeach

    {{-- Includable CSS --}}
    @yield('styles')
    @laravelPWA
</head>

<body {{ Metronic::printAttrs('body') }} {{ Metronic::printClasses('body') }}>
    @if (config('layout.page-loader.type') != '')
        @include('layout.partials._page-loader')
    @endif

    @php
        if (!Sentinel::check()) {
            $menu = 'menu_aside_metronic';
            $nik = 'guest_demo';
            $name = 'guest_demo';
            $additional_info = 'halaman guest_demo';
            $avatar = get_employee_pict($nik);
        } else {
            if (Session::get('role') == "admin") {
                $menu = 'menu_aside';
                $nik = Sentinel::check()->nik;
                $name = Session::get('role');
                $additional_info = 'Login as ' . $name;
                $avatar = get_employee_pict($nik);
            } elseif (Session::get('role') == "employee") {
                $menu = 'menu_aside_employee';
        
                $nik = Sentinel::check()->nik;
                $dt_name = Sentinel::check()->employee;
                $avatar = get_employee_pict($nik);
                if (!empty($dt_name)) {
                    $name = $dt_name->personnel_number;
                    $additional_info = $dt_name->position_name;
                } else {
                    $name = Session::get('role');
                    $additional_info = 'Login as ' . $name;
                }
            } else if (Session::get('role') == "executive" ) {
                $menu = 'menu_aside_talent_committee_as_executive';
        
                $nik = Sentinel::check()->nik;
                $dt_name = Sentinel::check()->employee;
                $avatar = get_employee_pict($nik);
        
                if (!empty($dt_name)) {
                    $name = $dt_name->personnel_number;
                    $additional_info = $dt_name->position_name;
                } else {
                    $name = Session::get('role');
                    $additional_info = 'Login as ' . $name;
                }
            } else if (Session::get('role') == "talent_committee") {
                $menu = 'menu_aside_talent_committee';
        
                $nik = Sentinel::check()->nik;
                $dt_name = Sentinel::check()->employee;
                $avatar = get_employee_pict($nik);
        
                if (!empty($dt_name)) {
                    $name = $dt_name->personnel_number;
                    $additional_info = $dt_name->position_name;
                } else {
                    $name = Session::get('role');
                    $additional_info = 'Login as ' . $name;
                }
            }
        }
        
    @endphp

    @include('layout.base._layout')

    <script>
        var HOST_URL = "{{ route('quick-search') }}";
    </script>

    {{-- Global Config (global config for global JS scripts) --}}
    <script>
        var KTAppSettings = {!! json_encode(config('layout.js'), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) !!};
    </script>

    {{-- Global Theme JS Bundle (used by all pages) --}}
    @foreach (config('layout.resources.js') as $script)
        <script src="{{ asset($script) }}" type="text/javascript"></script>
    @endforeach

    {{-- Includable JS --}}
    @yield('scripts')
    @stack('scripts')

    {{-- <script src="https://js.pusher.com/7.0/pusher.min.js"></script> --}}
    <script>
        // Buat fungsi dulu disini, nanti dipindah ke global script
        getPendingTask();
        function getPendingTask() {
            $.ajax({
                url: '{{ route('pending_task.badge') }}',
                method: 'get',
                success: function(res) {
                    $('.jumlah_pending_task').text(res);
                }
            })
        }

        // // Enable pusher logging - don't include this in production
        // Pusher.logToConsole = true;

        // var pusher = new Pusher('{!! env('PUSHER_APP_KEY') !!}', {
        //     cluster: '{!! env('PUSHER_APP_CLUSTER') !!}'
        // });

        // var channel = pusher.subscribe('my-pusher-channel');
        //     channel.bind('my-event', function(data) {
        //     alert(JSON.stringify(data));
        // });
    </script>

</body>

</html>
