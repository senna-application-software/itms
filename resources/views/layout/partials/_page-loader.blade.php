{{-- Page Loader Types --}}

{{-- Default --}}
@if (config('layout.page-loader.type') == 'default')
    <div class="page-loader">
        <div class="spinner spinner-primary"></div>
    </div>
@endif

{{-- Spinner Message --}}
@if (config('layout.page-loader.type') == 'spinner-message')
    <div class="page-loader page-loader-base">
        <div class="blockui">
            <span>Silahkan Tunggu...</span>
            <span><div class="spinner spinner-primary"></div></span>
        </div>
    </div>
@endif

{{-- Spinner Logo --}}
@if (config('layout.page-loader.type') == 'spinner-logo')
    <div class="page-loader page-loader-logo bgi-size-cover bgi-position-center bgi-no-repeat p-10 p-sm-30" style="background-image: url( {{ asset('media/bg/bg-3.jpg') }}">
        <img alt="{{ config('app.name') }}" src="{{ asset('media/logos/'. env('COMPANY_LOGO')) }}" style="max-width:300px"/>
        <br/>
        <span class="mt-4">Silahkan Tunggu...</span>
        <div class="spinner spinner-primary"></div>
    </div>
@endif
