{{-- Aside --}}

<div class="aside aside-left {{ Metronic::printClasses('aside', false) }} d-flex flex-column flex-row-auto" id="kt_aside">

    {{-- Brand --}}
    <div class="brand flex-column-auto {{ Metronic::printClasses('brand', false) }}" id="kt_brand">
        <div class="brand-logo">
            <a href="{{ url('/') }}">
                @if (config('layout_executive.aside.self.custome.is_image') )
                    <img alt="{{ config('app.name') }}" class="company-logo" src="{{ asset('media/logos/'. config('layout_executive.aside.self.custome.image')) }}" />                
                @else
                    <span>{{ config('layout_executive.aside.self.custome.text') }}</span>
                @endif
            </a>
        </div>

        @if (config('layout_executive.aside.self.minimize.toggle'))
            <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
                {{ Metronic::getSVG("media/svg/icons/Navigation/Angle-double-left.svg", "svg-icon-xl") }}
            </button>
        @endif

    </div>

    {{-- Aside menu --}}
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">

        @if (config('layout_executive.aside.self.display') === false)
            <div class="header-logo">
                <a href="{{ url('/') }}">
                    @if (config('layout_executive.aside.self.custome.is_image') )
                        <img alt="{{ config('app.name') }}" class="company-logo" src="{{ asset('media/logos/'. config('layout_executive.aside.self.custome.image')) }}" />                      
                    @else
                        <span>{{ config('layout_executive.aside.self.custome.text') }}</span>
                    @endif
                </a>
            </div>
        @endif

        <div
            id="kt_aside_menu"
            class="aside-menu my-4 {{ Metronic::printClasses('aside_menu', false) }}"
            data-menu-vertical="1"
            {{ Metronic::printAttrs('aside_menu') }}>

            <ul class="menu-nav {{ Metronic::printClasses('aside_menu_nav', false) }}">
                @php
                    if ($menu == "menu_aside_employee") {
                        $menu_employee = rule_aspiration(Sentinel::getUser()->nik, "menu");
                    }
                @endphp
                {{ Menu::renderVerMenu($menu_employee ?? config($menu.'.items')) }}
            </ul>
        </div>
    </div>

</div>
