{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    @include('aspirations.individual.content')
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/wizard/wizard-1.js') }}" type="text/javascript"></script>
    <script>
        let countTargetPosition = 0; 

        $(document).ready(function() {
            $('#listPosition').select2();
            console.log("{{ isset($person_grade) ? $person_grade : null }}")
            $("#listPosition").change(function() {
                let viewedTargetPositionName = $("#listPosition option:selected").html();
                $.ajax({
                    method: "POST",
                    url: "{{ route('gap_competency') }}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        nik: "{{ $nik }}",
                        target_position: $(this).val()
                    },
                    success: function(res) {
                        html = "";
                        no = 0;
                        $.each(res, function (indexInArray, valueOfElement) {
                            $("#valueEqs").html(valueOfElement.eqs);
                            textGap = (valueOfElement.gap < 0) ? 'danger' : ((valueOfElement.gap == 0) ? 'primary' : 'success');
                            html += `
                            <tr>
                                <td>${no+=1}</td>
                                <td>${valueOfElement.name_competency}</td>
                                <td>${valueOfElement.current}</td>
                                <td>${valueOfElement.required}</td>
                                <td><strong class="text-${textGap}">${valueOfElement.gap}</strong></td>
                            </tr>
                            `
                        });

                        $("#bodyTargetPosition").html("");
                        $("#viewedTargetPositionName").html(viewedTargetPositionName);
                        $("#bodyTargetPosition").html(html);
                    }
                })
            })
        });

        $("#btnAddTargetPosition").click(function(e) {
            e.preventDefault();
            let selectedTargetPositions = $("[name='selected_positions[]']").map(function(){return $(this).val();}).get();
            let selectedVerticalPositions = $(".data-vertical").map(function(){return $(this).val();}).get();
            let selectedHorizontalPositions = $(".data-horizontal").map(function(){return $(this).val();}).get();
            let targetPositionCode = $("#listPosition").val();
            let selectedInput = $("#listPosition option:selected");
            let validationTargetPosition = true;
            let label = "";
            let nilaiEqs = $("#valueEqs").html();

            if ((selectedVerticalPositions.length+selectedHorizontalPositions.length) < 6 ) {
                if (selectedInput.attr('data-vertical') == "1" && selectedVerticalPositions.length > 2) {
                    validationTargetPosition = false;
                    label = "Vertical";
                }else if (selectedInput.attr('data-horizontal') == "1" && selectedHorizontalPositions.length > 2) {
                    validationTargetPosition = false;
                    label = "Horizontal";
                }

                if (validationTargetPosition) {
                    if (selectedTargetPositions.indexOf(targetPositionCode) == -1) {
                        countTargetPosition += 1;
                        targetPositionName = selectedInput.html();
                        if (targetPositionCode != "-" && targetPositionCode != null) {
                            let bgColor = (selectedInput.attr('data-vertical') == "1") ? 'alert-success' : 'alert-primary'
                            let typePositionInput = (selectedInput.attr('data-vertical') == "1") ? 'data-vertical' : 'data-horizontal' 
                            let typePosition = (selectedInput.attr('data-vertical') == "1") ? 'Vertical' : 'Horizontal'
                            targetPositionHtml = `
                            <div class="alert ${bgColor} alert-dismissible fade show mb-2" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                ${typePosition} Position<br>
                                <strong>${targetPositionName}</strong>
                                <input type="hidden" name="selected_positions[]" class="${typePositionInput}" value="${targetPositionCode}"><br>
                                <h5>EQS: ${nilaiEqs}</h5>
                            </div>
                            `
                            $("#listTargetPosition").append(targetPositionHtml);
                            $("#viewedTargetPositionName").html(targetPositionName);
                        }
                    }else {
                        Swal.fire("Selected Target Positions in List", "", "error");
                    }
                }else {
                    Swal.fire(`${label} Target Positions Reach Limit`, "", "error");
                }
            }else {
                Swal.fire("Target Positions Reach Limit", "", "error");
            }
        })

        function saveIndividualPlan() {
            let selectedTargetPositions = $("[name='selected_positions[]']").map(function(){return $(this).val();}).get();
            if (selectedTargetPositions.length > 0) {
                KTApp.block('#kt_blockui_card', {
                    overlayColor: '#000000',
                    state: 'primary',
                    message: 'Please wait...'
                });
                $.ajax({
                    method: "POST",
                    url: "{{ route('insert-planning-individual') }}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        target_positions: selectedTargetPositions
                    },
                    success: function(res) {
                        KTApp.unblock('#kt_blockui_card');
                        @include('aspirations.element.ajax_condition')
                    },
                    error: function(res) {
                        KTApp.unblock('#kt_blockui_card');
                    }
                });
            }else {
                Swal.fire("Please Add Target Positions", "", "error");
            }
        }
    </script>
@endsection