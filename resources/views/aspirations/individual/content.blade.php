<div class="col-md-12">
    @include('aspirations.element.wizard', ['step1' => 'current', 'step1Svg' => 'primary'])
    
    <div class="card mt-4" id="kt_blockui_card">
        <div class="card-body">
            <h4>Individual Aspiration</h4>
            <h5 class="mt-5">Choose Position</h5>
            <div class="row">
                <div class="col-md-12 d-flex flex-row">
                    <select name="" id="listPosition" class="form-control form-lg mr-1" autocomplete="off"> 
                        <option value="-" selected disabled>- Available Position -</option>
                        @if (!empty($positions->vertical))
                        <optgroup label="Vertical">
                            @foreach ($positions->vertical as $position)
                                <option value="{{ $position->code_position }}" data-vertical={{ $position->is_vertical ?? 0 }} data-horizontal={{ $position->is_horizontal ?? 0 }}>[{{ $position->name_subholding }}]  [{{ $position->name_area }}] {{ $position->name_position }} ({{ $position->grade_align_notes }})</option>
                            @endforeach
                        </optgroup>
                        @endif

                        @if (!empty($positions->horizontal))
                        <optgroup label="Horizontal">
                            @foreach ($positions->horizontal as $position)
                                <option value="{{ $position->code_position }}" data-vertical={{ $position->is_vertical ?? 0 }} data-horizontal={{ $position->is_horizontal ?? 0 }}>[{{ $position->name_subholding }}]  [{{ $position->name_area }}] {{ $position->name_position }} ({{ $position->grade_align_notes }})</option>
                            @endforeach
                        </optgroup>
                        @endif
                    </select>
                    <a href="#" class="btn btn-primary" id="btnAddTargetPosition">+</a>
                </div>
                <form id="formSelectedPositions" class="col-md-4 mt-2">
                    <div id="listTargetPosition">
                        @isset($current_target_positions)
                        @foreach ($current_target_positions as $target_position)
                        <div class="alert {{($target_position->positions->is_vertical) ? 'alert-success' : 'alert-primary'}} alert-dismissible fade show mb-2" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            {{($target_position->positions->is_vertical) ? "Vertical" : "Horizontal"}} Position<br>
                            <strong>[{{ $target_position->positions->name_subholding }}]  [{{ $target_position->positions->name_area }}] {{ $target_position->positions->name_position }} ({{ $target_position->positions->grade_align_notes }})</strong>
                            <input type="hidden" name="selected_positions[]" class="{{($target_position->positions->is_vertical) ? "data-vertical" : "data-horizontal"}}" value="{{ $target_position->positions->code_position }}"><br>
                            <h5>EQS: {{$target_position->eqs_final}}</h5>
                        </div> 
                        @endforeach   
                        @endisset
                    </div>
                </form>
                <div class="col-md-8 mt-2">
                    <h5><strong>Competencies Gap</strong></h5>
                    <h5 id="viewedTargetPositionName"></h5>
                    <span style="display: none;" id="valueEqs"></span>
                    <table class="table">
                        <thead class="bg-light-primary">
                            <tr>
                                <th>NO</th>
                                <th>COMPETENCY</th>
                                <th>CURRENT</th>
                                <th>REQUIRED</th>
                                <th>GAP</th>
                            </tr>
                        </thead>
                        <tbody id="bodyTargetPosition">
                            {{-- @for ($i = 0; $i < 4; $i++)
                            <tr>
                                <td>{{$i+1}}</td>
                                <td>Learning & Growth Mindset</td>
                                <td>{{ $c=random_int(50, 100) }}</td>
                                <td>{{ $r=random_int(50, 100) }}</td>
                                <td><strong class="text-{{ ($c-$r < 0) ? 'danger' : (($c-$r == 0) ? 'primary' : 'success')}}">{{ $c-$r }}</strong></td>
                            </tr>
                            @endfor --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <button onclick="saveIndividualPlan()" class="btn btn-light-success float-right my-5">{{ isset($list_wizard['next']) ? "Save & Next" : "Save" }}</button>
</div>