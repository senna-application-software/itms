{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    @if (isset($data_lengkap) && !$data_lengkap && !$engage)
    <div class="col-md-12 mb-5">
        <div class="card card-custom card-stretch gutter-b">
            <div class="card-body d-flex p-0">
                <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) top; background-size: 33% auto; background-image: url({{ asset('media/svg/humans/custom-8.svg') }})">
                    <h2 class="text-dark pb-5 font-weight-bolder">{{$page_title}}</h2>
                    <p class="text-dark-50 pb-5 font-size-h5">Data anda tidak lengkap silahkan kontak admin agar segera ditindak lanjut!</p>
                </div>
            </div>
        </div>
    </div>
    @elseif (isset($data_lengkap) &&  !$access && !$engage)
    <div class="col-md-12 mb-5">
        <div class="card card-custom card-stretch gutter-b">
            <div class="card-body d-flex p-0">
                <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) top; background-size: 33% auto; background-image: url({{ asset('media/svg/humans/custom-8.svg') }})">
                    <h2 class="text-dark pb-5 font-weight-bolder">{{$page_title}}</h2>
                    <p class="text-dark-50 pb-5 font-size-h5">Anda tidak memiliki akses kedalam page ini.</p>
                </div>
            </div>
        </div>
    </div> 
    @else
    <div class="col-md-12 mb-5">
        <div class="card card-custom card-stretch gutter-b">
            <div class="card-body d-flex p-0">
                <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) top; background-size: 33% auto; background-image: url({{ asset('media/svg/humans/custom-8.svg') }})">
                    <h2 class="text-dark pb-5 font-weight-bolder">{{$page_title}}</h2>
                    <p class="text-dark-50 pb-5 font-size-h5">Tidak ada event <i>Aspirations</i> yang sedang berlangsung.</p>
                </div>
            </div>
        </div>
    </div>  
    @endif
      
@endsection