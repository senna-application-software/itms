{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    @include('aspirations.unit.content')
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/wizard/wizard-1.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $("#filterPositionName").select2();
            $('#listPosition').select2({
                dropdownParent: $('#addTargetPosition')
            });

            $("#filterPositionName").select2();

            $("#fillterButton").click(function() {
                let posisi = $("#filterPositionName").val()
                let employee = $("#filterEmployeeName").val().toLowerCase()
                let selectorPosition = ""
                if (posisi != "" || employee != "") {
                    $(".container-positions").hide();
                    if (posisi != "") {
                        selectorPosition = `[data-posisi=${posisi}]`;
                        $(`.container-positions${selectorPosition }`).show();
                        $(".employee-card").show();
                    }
                    if (employee != "") {
                        $(".employee-card").hide();
                        $(`.container-positions${selectorPosition} [data-name*='${employee}']`).closest(".container-positions").show();
                        $(`.container-positions${selectorPosition} [data-name*='${employee}']`).show();
                    }

                    if ($(`.container-positions${selectorPosition} .employee-card:visible`).length == 0) {
                        $(`.container-positions${selectorPosition}`).hide();
                    }

                    if ($(".container-positions:visible").length == 0) {
                        $(".empty-data").show();
                    }else {
                        $(".empty-data").hide();
                    }
                }else {
                    $(".container-positions").show();
                    $(".employee-card").show();
                }
            });
        });

        $(document).on('click', ".addTargetPositionEmployee", function() {
            nik = $(this).attr("data-nik");
            employee = $(this).attr("data-employee");

            $("#submitTargetPositionEmployee").attr("data-nik", nik)
            $("#employeeName").html(employee);
            generateTargetPositionEmployee(nik);
        });

        let countTargetPosition = [];
        $("#submitTargetPositionEmployee").click(function() {
            let inputHorizontalPositionEmployee = $(`[name='target_positions_employee[${nik}][]'][data-horizontal=1]`).map(function(){return $(this).val();}).get()
            let inputVerticalPositionEmployee = $(`[name='target_positions_employee[${nik}][]'][data-vertical=1]`).map(function(){return $(this).val();}).get()
            let selectedPosition = $("#listPosition").val();
            let selectedInput = $("#listPosition option:selected");
            let validationTargetPosition = true;
            let label = "";

            if (selectedPosition != null && selectedPosition != "") {
                if ((inputVerticalPositionEmployee.length+inputHorizontalPositionEmployee.length) < 6) {
                    if (selectedInput.attr('data-vertical') == 1 && inputVerticalPositionEmployee.length > 2) {
                        validationTargetPosition = false;
                        label = "Vertical";
                    }else if (selectedInput.attr('data-horizontal') == 1 && inputHorizontalPositionEmployee.length > 2) {
                        validationTargetPosition = false;
                        label = "Horizontal";
                    }
                    
                    if (validationTargetPosition) {
                        if (inputVerticalPositionEmployee.indexOf(selectedPosition) == -1 && inputHorizontalPositionEmployee.indexOf(selectedPosition) == -1) {
                            nik = $(this).attr("data-nik");
                            if (typeof countTargetPosition[nik] === 'undefined') {
                                countTargetPosition[nik] = 0;
                            }
                            let bgColor = (selectedInput.attr('data-vertical') == "1") ? 'alert-success' : 'alert-primary'
                            let typePositionInput = (selectedInput.attr('data-vertical') == "1") ? 'data-vertical="1"' : 'data-horizontal="1"' 
                            let typePosition = (selectedInput.attr('data-vertical') == "1") ? 'Vertical' : 'Horizontal'
                            countTargetPosition[nik] += 1;
                            
                            targetPositionName = $("#listPosition option:selected").html();
                            // targetPositionName = targetPositionName.length > 19 ? targetPositionName.substring(0, 19 - 3) + "..." : targetPositionName;
                            targetPositionCode = selectedPosition;
                            targetPositionHtml = `
                            <div class="alert ${bgColor} alert-dismissible fade show mt-2" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                ${typePosition} Position<br>
                                <strong>${ targetPositionName }</strong>
                                <input type="hidden" class="input-target" name="target_positions_employee[${nik}][]" ${typePositionInput} value="${targetPositionCode}">
                            </div>
                            `

                            $("#listPosition").val("");
                            $(`#${nik}`).append(targetPositionHtml);
                            $("#addTargetPosition").modal('hide');
                        }else {
                            Swal.fire("Selected Target in List", "", "error");
                        }
                    }else {
                        Swal.fire(`${label} Target Positions Reach Limit`, "", "error");
                    }
                }else {
                    Swal.fire("Target Positions Reach Limit", "", "error");
                }
            }
        })

        function saveUnitPlan() {
            KTApp.block('#kt_blockui_card', {
                overlayColor: '#000000',
                state: 'primary',
                message: 'Please wait...'
            });
            $.ajax({
                method: "POST",
                url: "{{ route('insert-planning-unit') }}",
                data: $("#listEmployeePerPosition").serialize(),
                success: function(res) {
                    KTApp.unblock('#kt_blockui_card');
                    @include('aspirations.element.ajax_condition')

                },
                error: function(res) {
                    KTApp.unblock('#kt_blockui_card');
                }
            })
        }

        function generateTargetPositionEmployee(nik) {
            $.ajax({
                method: "POST",
                url: "{{ route('generate_target_positions') }}",
                data: {
                    _token: "{{csrf_token()}}",
                    nik: nik,
                },
                success: function(res) {
                    res = JSON.parse(JSON.stringify(res));
                    $("#listPosition").html("");
                    let html = `<option value="" selected disabled>- Available Position -</option>`;
                    let vertical = "";
                    let horizontal = "";
                    $.each(res.vertical, function(key, value) {
                        vertical += `<option value="${(value.code_position)}" data-vertical=${value.is_vertical} data-horizontal=${value.is_horizontal}>[${value.name_subholding}] [${value.name_area}] ${(value.name_position)} (${(value.grade_align_notes)})</option>`;
                    });
                    $.each(res.horizontal, function(key, value) {
                        horizontal += `<option value="${(value.code_position)}" data-vertical=${value.is_vertical} data-horizontal=${value.is_horizontal}>[${value.name_subholding}] [${value.name_area}] ${(value.name_position)} (${(value.grade_align_notes)})</option>`;
                    });
                    html += `
                    <optgroup label="Vertical">
                        ${vertical}
                    </optgroup>
                    <optgroup label="Horizontal">
                        ${horizontal}
                    </optgroup>
                    `
                    $("#listPosition").html(html);
                    $("#addTargetPosition").modal('show');
                },
                error: function() {
                    KTApp.unblock(".kt_blockui_card");
                } 
            })
        }
    </script>
@endsection