@include('aspirations.element.wizard', ['step4' => 'current', 'step4Svg' => 'primary'])

<div class="card mt-4 rounded">
    <div class="card-body">
        <h4>Unit Aspiration</h4>
    </div>
</div>

<div class="row mt-4">
    <div class="col-md-4">
        <select class="form-control " name="" id="filterPositionName" autocomplete="off">
            <option value="" selected>All Postions</option>
            @foreach ($data as $position)
                <option value="{{ $position->code_position }}">{{ $position->name_position }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" placeholder="Search Employee" id="filterEmployeeName" autocomplete="off">
    </div>
    <button class="btn btn-primary" id="fillterButton">Search</button>
</div>

<div id="kt_blockui_card">
    <form id="listEmployeePerPosition">
        @csrf
        @foreach ($data as $position)
        <div class="container-positions" data-posisi="{{ $position->code_position }}">
            <div class="card px-5 py-3 mt-4">
                <div class="d-flex flex-column ">
                    <h4>{{ $position->name_position }}</h4>
                </div>
            </div>

            <div class="scroll scroll-pull mt-4" data-scroll="true" data-suppress-scroll-x="false" data-swipe-easing="false" data-wheel-propagation="true">
                <div style="width: 1200px;">
                    <div class="d-flex flex-row px-0">
                        @foreach ($position->employees as $employee)
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 employee-card" data-name="{{ strtolower($employee->personnel_number) }}">
                            <div class="card card-custom">
                                <div class="card-body pt-4 d-flex flex-column">
                                    <div class="my-5 align-self-end">                            
                                        <div class="symbol symbol-circle symbol-lg-100">
                                            <img src="{{ get_employee_pict($employee->prev_persno) }}" alt="image">
                                        </div>
                                    </div>
        
                                    <div class="mt-3" style="min-height: 120px;">
                                        <h3 class="text-primary font-weight-bold text-hover-warning font-size-h4 mb-0">{{ $employee->personnel_number }}</h3>
                                        <p class="text-muted font-weight-bold">{{ $employee->position_name }}</p>
                                    </div>
        
                                    <div class="row">
                                        @foreach ($employee->performance->slice(0, 3) as $performance)
                                        <div class="col-md-4">
                                            <div class="rounded p-1 text-center text-white" style="background-color: {{ $loop->iteration == 2 ? '#7774FF' : '#82E99F'}}">
                                                <span>{{ $performance->year }}</span>
                                                <br>
                                                <span>{{ $performance->performance_category }}</span>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <a class="btn btn-light-success text-center mt-2 addTargetPositionEmployee" data-nik="{{ $employee->prev_persno }}" data-employee="{{ $employee->personnel_number }}">+ Add Position</a>
                                    <div class="targetPositionEmployee" id="{{ $employee->prev_persno }}">
                                        @isset($employee->talent_aspiration)
                                        @foreach ($employee->talent_aspiration as $target_position)
                                        <div class="alert {{($target_position->positions->is_vertical) ? 'alert-success' : 'alert-primary'}} alert-dismissible fade show mt-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            {{($target_position->positions->is_vertical) ? "Vertical" : "Horizontal"}} Position<br>
                                            <strong>[{{ $target_position->positions->name_subholding }}]  [{{ $target_position->positions->name_area }}] {{ $target_position->positions->name_position }} ({{ $target_position->positions->grade_align_notes }})</strong>
                                            <input type="hidden" class="input-target" name="target_positions_employee[{{ $employee->prev_persno }}][]" {{($target_position->positions->is_vertical) ? 'data-vertical=1' : 'data-horizontal=1'}} value="{{ $target_position->positions->code_position }}">
                                        </div> 
                                        @endforeach   
                                        @endisset
                                    </div>
                                    <input type="hidden" class="input-target" name="employees[]" value="{{ $employee->prev_persno }}">
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </form>
    <div class="empty-data p-12" style="display: none;">
        <h4 class="text-center">Data Not Found</h4>
    </div>
</div>

@if (isset($list_wizard['prev']))
<a href="{{ route('talent-planning', ['page' => $list_wizard['prev']]) }}" class="btn btn-secondary btn-lg float-left">Back</a>
@endif
<a onclick="saveUnitPlan()" class="btn btn-light-success btn-lg float-right my-5">Save</a>

@section('modal')
<div class="modal fade" id="addTargetPosition" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span id="employeeName"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <label for="listPosition">Add Target Positions</label>
                <select name="" id="listPosition" class="form-control form-lg mr-1" autocomplete="off" style="width: 100%"></select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitTargetPositionEmployee">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection