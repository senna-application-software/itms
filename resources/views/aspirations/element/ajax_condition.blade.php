@if (isset($list_wizard['next']))
window.location = "{{ route('talent-planning', ['page' => $list_wizard['next']]) }}";
@else
Swal.fire({
    title: 'Aspirations Saved',
    text: "",
    icon: 'success',
    showCancelButton: true,
    confirmButtonText: 'Go To Dashboard'
    }).then((result) => {
    if (result.isConfirmed) {
        window.location = "{{ url('dashboard') }}";
    }
})
@endif