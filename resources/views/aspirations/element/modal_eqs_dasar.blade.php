<div class="card card-custom gutter-b">
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <input type="hidden" id="nikSelectedEmployee"
                        value="{{ $employee ? $employee[0]->prev_persno : 0 }}">
                    <input type="hidden" id="subEventModal" value="{{ $sub_event }}">
                    <input type="hidden" name="talent_id" value="{{ $talent ? $talent[0]->id : 0 }}" id="talent_id">
                    <div class="col-md-4 justify-content-center">
                        <div class="symbol symbol-50 symbol-lg-120 ml-4 symbol-light-danger">
                            <div class="symbol-label"
                                style="background-image: url( {{ get_employee_pict($employee ? $employee[0]->prev_persno : 0) }} )">
                            </div>
                        </div>
                        <h6 class="text-center">{{ strtoupper($employee ? $employee[0]->personnel_number : '-') }}
                        </h6>
                        <h3 class="text-center" id="EQSModal">EQS: {{ $eqs_final }}</h3>
                    </div>

                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Kinerja</label>
                                    <input type="text" name="kinerja" class="form-control bg-secondary"
                                        value="{{ $eqs ? $eqs->kinerja : 0 }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="">Kesesuaian Kompetensi</label>
                                    <input type="text" name="kompetensi" class="form-control bg-secondary"
                                        value="{{ $eqs ? $eqs->job_fit : 0 }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="">Training dan Sertifikasi</label>
                                    <input type="text" name="training_sertificate" class="form-control bg-secondary"
                                        value="{{ $eqs ? $eqs->training_sertifikasi : 0 }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Track Record</label>
                                    <input type="text" name="track_record" class="form-control bg-secondary"
                                        value="{{ $eqs ? $eqs->track_record : 0 }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="">Experience</label>
                                    <input type="text" name="experience" class="form-control bg-secondary"
                                        value="{{ $eqs ? $eqs->experience : 0 }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="">Aspirasi</label>
                                    <input type="text" name="aspirasi" class="form-control bg-secondary"
                                        value="{{ $eqs ? $eqs->aspiration : 0 }}" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 text-center">
                <h4>Performance</h4>
                <div class="row justify-content-center">
                    <div class="d-flex align-items-center">
                        @forelse ($performance as $val)
                            <div class="symbol symbol-60 mr-3 text-center">
                                <h4>{{ $val->year }}</h4>
                                <span>{{ $val->performance_category }}</span>
                            </div>
                        @empty
                            @for ($i = date('Y') - 3; $i < date('Y'); $i++)
                                <div class="symbol symbol-60 mr-3 text-center">
                                    <h4>{{ $i }}</h4>
                                    <span>No data</span>
                                </div>
                            @endfor
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
