<div class="col-md-12">
    @include('aspirations.element.wizard', ['step2' => 'current', 'step2Svg' => 'primary'])

    <div class="card mt-4 rounded">
        <div class="card-body">
            <h4>Supervisor Aspiration</h4>
        </div>
    </div>

    <div id="kt_blockui_card">
        <form id="listEmployee">
            @csrf
            <div class="card px-5 py-3 mt-4">
                <div class="d-flex flex-column ">
                    <h4>{{ $position }}</h4>
                </div>
            </div>
        
            <div class="row scroll-custom mt-5">
                @forelse($data as $employee)
                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 employee-card mt-5" data-name="{{ strtolower($employee->personnel_number) }}">
                    <div class="card card-custom" style="min-height: 485px">
                        <div class="card-body pt-4 d-flex flex-column">
                            <div class="my-5 align-self-end">                            
                                <div class="symbol symbol-circle symbol-lg-100">
                                    <img src="{{ get_employee_pict($employee->prev_persno) }}" alt="image">
                                </div>
                            </div>
    
                            <div class="mt-3" style="min-height: 120px;">
                                <h3 class="text-primary font-weight-bold text-hover-warning font-size-h4 mb-0 open-career-card-plain" type="button" data-nik="{{$employee->prev_persno}}" data-id="{{$employee->id}}" style="-webkit-appearance: none;">{{ $employee->personnel_number }} </h3>
                                <p class="text-muted font-weight-bold">{{ $employee->position_name }} </p>
                                <p class="font-weight-bold p-0">{{ $employee->positions->sub_holding->name_subholding ?? "" }}</p>
                                <p class="font-weight-bold p-0">{{ $employee->positions->master_area->name_area ?? "" }}</p>
                            </div>

                            <div class="row mt-auto">
                                @foreach ($employee->performance as $performance)
                                <div class="col-md-4 mx-1 rounded p-1 text-center" style="background-color:#82E99F;min-height: 70px;max-width: 30%">
                                    <span>{{ $performance->year }}</span>
                                    <br>
                                    <span>{{ ucfirst($performance->perfomance_category_align) }}</span>
                                </div>
                                @endforeach
                            </div>

                            <a class="btn btn-light-success text-center mt-2 addTargetPositionEmployee" data-nik="{{ $employee->prev_persno }}" data-employee="{{ $employee->personnel_number }}">+ Add Position</a>
                            <div class="targetPositionEmployee" id="{{ $employee->prev_persno }}">
                                @isset($employee->talent_aspiration)
                                @foreach ($employee->talent_aspiration as $target_position)
                                <div class="alert {{($target_position->positions->is_vertical) ? 'alert-success' : 'alert-primary'}} alert-dismissible fade show mt-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        <span class="sr-only">Close</span>
                                    </button>
                                    {{($target_position->positions->is_vertical) ? "Vertical" : "Horizontal"}} Position<br>
                                    <strong>[{{ $target_position->positions->name_subholding }}]  [{{ $target_position->positions->name_area }}] {{ $target_position->positions->name_position }} ({{ $target_position->positions->grade_align_notes }})</strong>
                                    <input type="hidden" class="input-target" name="target_positions_employee[{{ $employee->prev_persno }}][]" {{($target_position->positions->is_vertical) ? 'data-vertical=1' : 'data-horizontal=1'}} value="{{ $target_position->positions->code_position }}">
                                </div> 
                                @endforeach   
                                @endisset
                            </div>
                            <input type="hidden" class="input-target" name="employees[]" value="{{ $employee->prev_persno }}">
                        </div>
                    </div>
                </div>
                @empty
                <div class="col-md-12 p-0 m-0">
                    <div class="alert alert-primary" role="alert">
                        <h4 class="text-center">Karyawan yang anda pimpin bukan karyawan tetap</h4>
                    </div>
                </div>
                @endforelse
            </div>
            
            {{-- <div class="scroll scroll-pull mt-4" data-scroll="true" data-suppress-scroll-x="false" data-swipe-easing="false" data-wheel-propagation="true">
                <div style="width: 1200px;">
                    <div class="d-flex flex-row px-0">
                        @foreach ($data as $employee)
                        @endforeach
                    </div>
                </div>
            </div> --}}
        </form>
    </div>

    <div class="d-flex justify-content-between mt-4">
        @if (isset($list_wizard['prev']))
        <a href="{{ route('talent-planning', ['page' => $list_wizard['prev']]) }}" class="btn btn-secondary btn-lg">Back</a>
        @endif
        <a onclick="saveSupervisorPlan()" class="btn btn-light-success btn-lg">{{ isset($list_wizard['next']) ? "Save & Next" : "Save" }}</a>
    </div>
</div>

@section('modal')
<div class="modal fade" id="addTargetPosition" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span id="employeeName"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <label for="listPosition">Add Target Positions</label>
                <select name="" id="listPosition" class="form-control form-lg mr-1" autocomplete="off" style="width: 100%"></select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitTargetPositionEmployee">Select</button>
            </div>
        </div>
    </div>
</div>
    
<!-- Employee Score Card -->
<div class="modal fade" id="xl-modal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Informasi Karyawan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body data-modal" data-scroll="true" data-height="500">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection