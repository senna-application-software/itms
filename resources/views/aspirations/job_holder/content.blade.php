<div class="col-md-12">
    @include('aspirations.element.wizard', ['step3' => 'current', 'step3Svg' => 'primary'])

    <div class="card mt-4 rounded">
        <div class="card-body">
            <h4>Job Holder Aspiration</h4>
        </div>
    </div>

    <div id="kt_blockui_card">
        <form id="listEmployee">
            @csrf
            <div class="card px-5 py-3 mt-4">
                <div class="d-flex flex-column ">
                    <h4>{{ $position }}</h4>
                </div>
            </div>
        
            <div class="row scroll-custom mt-5">
                @foreach ($data as $employee) 
                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 employee-card" data-name="{{ strtolower($employee->personnel_number) }}">
                    <div class="card card-custom gutter-b card-stretch">
                        <div class="card-body pt-4">
                            <div class="d-flex justify-content-between my-5">
                                <label class="form-check form-check-custom form-check-solid align-self-start">
                                    <input class="form-check-input h-25px w-25px" name="selected_employee[]" value="{{ $employee->prev_persno }}" type="checkbox" autocomplete="off" {{ ($employee->talent_aspiration->count() > 0) ? "checked" : "" }}>
                                    <input type="hidden" name="employees[]" value="{{ $employee->prev_persno }}">
                                </label>
                                
                                <div class="symbol symbol-circle symbol-lg-100">
                                    <img src="{{ get_employee_pict($employee->prev_persno) }}" alt="image">
                                </div>
                            </div>
    
                            <div class="mt-3" style="min-height: 120px;">
                                <h3 class="text-primary font-weight-bold text-hover-warning font-size-h4 mb-0 open-career-card-plain" type="button" data-nik="{{$employee->prev_persno}}" data-id="{{$employee->id}}" style="-webkit-appearance: none;">{{ $employee->personnel_number }} </h3>
                                <p class="text-muted font-weight-bold">{{ $employee->position_name }}</p>
                                <p class="font-weight-bold p-0 m-0">{{ $employee->positions->sub_holding->name_subholding ?? "" }}</p>
                                <p class="font-weight-bold p-0 m-0">{{ $employee->positions->master_area->name_area ?? "" }}</p>
                            </div>

                            <div class="row mt-auto">
                                @foreach ($employee->performance->slice(0, 3) as $performance)
                                <div class="col-md-4 mx-1 rounded p-1 text-center" style="background-color:#82E99F;min-height: 70px;max-width: 30%">
                                    <span>{{ $performance->year }}</span>
                                    <br>
                                    <span>{{ ucfirst($performance->perfomance_category_align) }}</span>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </form>
    </div>
        
<!-- Employee Score Card -->
<div class="modal fade" id="xl-modal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Informasi Karyawan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body data-modal" data-scroll="true" data-height="500">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

    <div class="d-flex justify-content-between mt-4">
        @if (isset($list_wizard['prev']))
        <a href="{{ route('talent-planning', ['page' => $list_wizard['prev']]) }}" class="btn btn-secondary btn-lg">Back</a>
        @endif
        <a onclick="saveJobHolderPlan()" class="btn btn-light-success btn-lg">{{ isset($list_wizard['next']) ? "Save & Next" : "Save" }}</a>
    </div>
</div>