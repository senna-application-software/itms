{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    @include('aspirations.job_holder.content')
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css"/>
    <style type="text/css">
        div.scroll-custom {
            margin:0px auto;
            width: 100%;
            height: 500px;
            overflow-x: hidden;
            overflow-y: auto;
            text-align:justify;
        }
    </style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/wizard/wizard-1.js') }}" type="text/javascript"></script>
    @include('global.element.script_eqs_peremployee')
    <script>
        $(document).ready(function() {
            $("[name='selected_employee[]']").change(function() {
                let totalEmployee = $("[name='selected_employee[]']:checked").length
                if(totalEmployee > 3) {
                    Swal.fire("Selected Employee Reach Limit", "", "error");
                    $(this).prop('checked', false); 
                }
            });
        });

        function saveJobHolderPlan() {
            let totalEmployee = $("[name='selected_employee[]']:checked").length;
            if (totalEmployee > 0) {
                KTApp.block('#kt_blockui_card', {
                    overlayColor: '#000000',
                    state: 'primary',
                    message: 'Please wait...'
                });
                $.ajax({
                    method: "POST",
                    url: "{{ route('insert-planning-job_holder') }}",
                    data: $("#listEmployee").serialize(),
                    success: function(res) {
                        KTApp.unblock('#kt_blockui_card');
                        @include('aspirations.element.ajax_condition')
                    },
                    error: function(res) {
                        KTApp.unblock('#kt_blockui_card');
                    }
                })
            }else {
                Swal.fire("Please Add Employees", "", "error");
            }
        }
    </script>
@endsection