{{-- Extends layout --}}
@extends('layout.default')

{{-- Content Section --}}
@section('page_toolbar')
    {{-- kalo di subheader mau ditambahkan button action taruh di sini --}}
@endsection

@section('modal')
<!-- modal create -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Membuat Event Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>

            <form class="form" method="post" action="{{ route('event.create') }}">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12">
                            <label>Tahun</label>
                            <input type="text" name="year" class="form-control" value="{{ date('Y') }}" readonly="readonly">
                        </div>

                        <div class="form-group col-12">
                            <label>Mulai</label>
                            <div class="input-group date">
                                <input type="text" name="start_date" class="form-control" value="{{ date('d/m/Y') }}" id="datepicker_start_add" data-date-format="dd/mm/yyyy" readonly>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <span class="form-text text-muted">Tanggal dimulainya Event</span>
                        </div>
    
                        <div class="form-group col-12">
                            <label>Akhir</label>
                            <div class="input-group date">
                                <input type="text" name="end_date" class="form-control" value="{{ date('d/m/Y') }}" id="datepicker_end_add" data-date-format="dd/mm/yyyy" readonly>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <span class="form-text text-muted">Tanggal berakhirnya Event</span>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary mr-2" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-secondary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal edit -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Merubah Event</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>

            <form class="form" method="post" action="{{ route('event.edit') }}">
                @csrf
                <input type="hidden" name="id_event" id="edit_id">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12">
                            <label>Tahun</label>
                            <input type="text" name="year" class="form-control" id="edit_tahun" readonly="readonly">
                        </div>
    
                        <div class="form-group col-12">
                            <label>Mulai</label>
                            <div class="input-group date">
                                <input type="text" name="start_date" class="form-control" id="datepicker_start_edit" value="{{ date('d/m/Y') }}" data-date-format="dd/mm/yyyy">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <span class="form-text text-muted">Tanggal dimulainya Event</span>
                        </div>
    
                        <div class="form-group col-12">
                            <label>Akhir</label>
                            <div class="input-group date">
                                <input type="text" name="end_date" class="form-control" id="datepicker_end_edit" value="{{ date('d/m/Y') }}" data-date-format="dd/mm/yyyy" autocomplete="off">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <span class="form-text text-muted">Tanggal berakhirnya Event</span>
                        </div>

                        <div class="form-group col-12">
                            <label>Status Event</label>
                            <select name="status" class="form-control" id="edit_status">
                                <option value="open">Open</option>
                                <option value="closed">Close</option>
                            </select>
                        </div>

                        <div class="form-group col-12">
                            <label>Status Submit Aspiration</label>
                            <select name="status_input" class="form-control" id="edit_status_input">
                                <option value="1" selected>Open</option>
                                <option value="0">Close</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary mr-2" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-secondary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">
                    <div class="text-muted pt-2 font-size-sm"></div>
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#exampleModal">
                    <span class="svg-icon svg-icon-md">
                        <i class="fa fa-plus-circle"></i>
                    </span>
                    Buat Baru
                </a>
            </div>
        </div>

        <div class="card-body">
            {{-- <div class="overflow-auto"> --}}
                <table class="table table-bordered table-hover" id="table-event">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tahun</th>
                            <th>Batch</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            {{-- </div> --}}
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>

<script>
    var arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }

    $(document).ready(function () {
        $('#datepicker_start_add').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            dateFormat: 'dd/mm/yyyy',
            templates: arrows,
            startDate: new Date(),
            minDate: new Date(),
        }).on("changeDate", function(selected){
            var startDate = new Date(selected.date.valueOf());
            $('#datepicker_end_add').datepicker('setStartDate', startDate);
            $('#datepicker_end_add').datepicker('setMinDate', startDate);
            $('#datepicker_end_add').datepicker('setDate', startDate);
        });

        $('#datepicker_end_add').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            dateFormat: 'dd/mm/yyyy',
            templates: arrows,
            startDate: new Date(),
            minDate: new Date()
        });

        $('#datepicker_start_edit').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            dateFormat: 'dd/mm/yyyy',
            templates: arrows,
            startDate: new Date(),
            minDate: new Date(),
        }).on("changeDate", function(selected){
            startDate = new Date(selected.date.valueOf());
            $('#datepicker_end_edit').datepicker('setStartDate', startDate);
            $('#datepicker_end_edit').datepicker('setMinDate', startDate);
            $('#datepicker_end_edit').datepicker('setDate', startDate);
        });

        $('#datepicker_end_edit').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            dateFormat: 'dd/mm/yyyy',
            templates: arrows,
            startDate: new Date(),
            minDate: new Date()
        });
    });

    $('#table-event').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "{{ route('event.datatable') }}",
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'year', name: 'year'},
            { data: 'batch', name: 'batch'},
            { data: 'status', name: 'status'},
            { data: 'button', name: 'button'},
        ]
    });

    function edit_data(id){
        let modal = $('#edit');

        $.ajax({
            method: 'GET',
            url: "{!! url('/event/get-attribute/') !!}"+ "/" + id,
            success: function(response) {
                $('#edit_tahun').val(response.year);
                $('#edit_status').val(response.status);
                let hatml = "";
                if(response.status == 'open') {
                    hatml += "<option value='1'>Open</option>";
                }
                hatml += "<option value='0'>Close</option>"
                $("#edit_status_input").html(hatml);
                $('#edit_status_input').val(response.status_input);
                $('#datepicker_start_edit').val(response.start_date);
                var split = response.start_date.split("/");
                var dt = new Date(Number(split[2]), Number(split[1]) - 1, Number(split[0]));
                $('#datepicker_end_edit').datepicker('setStartDate', dt);
                $('#datepicker_end_edit').datepicker('setMinDate', dt);
                $('#datepicker_end_edit').val(response.end_date);
                split = response.end_date.split("/");
                dt = new Date(Number(split[2]), Number(split[1]) - 1, Number(split[0]));
                $('#datepicker_end_edit').datepicker('setDate', dt);
                $('#edit_id').val(response.id);

                modal.modal('show');
            },
            error: function() {
                Swal.fire('Sorry', 'Something Went Wrong !', 'error');
                modal.modal('hide');
            }
        });
    }

    $("#edit_status").change(function (e) { 
        e.preventDefault();
        let hatml = "";
        if($(this).val() == 'open') {
            hatml += "<option value='1'>Open</option>";
        }
        hatml += "<option value='0'>Close</option>"
        $("#edit_status_input").html(hatml);
    });
</script>


@if (session('status') == 'success')
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif

@if (session('status') == 'error')
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif
@endsection
