{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="col-12 mb-5">
    <div class="card px-5 py-3">
        <div class="d-flex justify-content-between">
            <h4 class="align-self-center m-0 p-0">
                {{ $sub_event->name_position }}
                <span class="label font-weight-bold label-lg {{ $sub_event->sourcing_type == 1 ? 'label-light-success' : 'label-light-primary' }} label-inline">
                    {{ $sub_event->sourcing_type == 1 ? 'External' : 'Internal' }}
                </span>
                <small class="text-muted">{{ $sub_event->name_subholding }} - [{{ $sub_event->name_area }}]</small>
            </h4>
            <div>
                 <button class="btn btn-success" data-select-all-click="boards">Pilih Semua</button>
                <button class="btn btn-primary" id="done_profiling">Done Profiling</button>

            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <form action="{{ route('talent-profiling.done_profiling') }}" method="POST" id="form_profiling">
        @csrf
        <div class="row">
            <input type="hidden" name="sub_event" value="{{ $sub_event->id }}">

            @foreach($data as $item)
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-body pt-4 d-flex flex-column">
                        <div class="d-flex justify-content-between my-5" data-select-all="boards">
                            <label class="form-check form-check-custom form-check-solid align-self-start">
                                <input class="form-check-input h-25px w-25px" name="talent_id[]" type="checkbox" data-name="{{ $item->employee->personnel_number }}" value="{{ $item->id }}"/>
                            </label>
                            
                            <div class="symbol symbol-circle symbol-100">
                                <img src="{{ get_employee_pict($item->employee->prev_persno) }}" alt="image">
                            </div>
                        </div>

                        <div class="mt-3 mb-5">
                            <h3 class="text-primary font-weight-bold text-hover-warning font-size-h4 mb-0 open-career-card" type="button" data-nik="{{$item->employee->prev_persno}}" data-id="{{$item->id}}" style="-webkit-appearance: none">{{ $item->employee->personnel_number }}</h3>
                            <p class="text-muted font-weight-bold">{{ $item->employee->master_posisi->name_position ?? "" }}</p>
                            <p class="font-weight-bold p-0 m-0">{{ $item->employee->positions->sub_holding->name_subholding ?? "" }}</p>
                            <p class="font-weight-bold p-0 m-0">{{ $item->employee->positions->master_area->name_area ?? "" }}</p>
                            @if($item->comitte_proposal)
                            <div class="form-check mt-3 mb-3">
                                <input class="form-check-input" type="checkbox" disabled checked>
                                <label class="form-check-label">
                                    Committee Proposal
                                </label>
                            </div>
                            @endif
                            <h3 class="mb-4" data-nik-eqs="{{ $item->employee->prev_persno }}">EQS: {{ nilai_eqs($item->employee->prev_persno, false, $sub_event->id) }}</h3>
                        </div>
                        <div class="row mt-auto">
                            @foreach ($item->employee->performance->slice(0, 3) as $performance)
                            <div class="col-md-4 mx-1 rounded p-1 text-center" style="background-color:#82E99F;min-height: 70px;max-width: 30%">
                                <span>{{ $performance->year }}</span>
                                <br>
                                <span>{{ $performance->perfomance_category_align }}</span>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </form>
</div>

@php
    

@endphp

<!-- Employee Score Card -->
<div class="modal fade" id="xl-modal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Informasi Karyawan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body data-modal" data-scroll="true" data-height="500">
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-primary font-weight-bold" data-dismiss="modal" id="submit_assessment">Simpan</button> --}}
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{{-- modal done profiling --}}
<div class="modal fade" id="modal_profiling" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirm Talent Profiling Results</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <form action="" method=" ">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr id="profiling_head">
                            
                            </tr>
                        </thead>
                        <tbody id="profiling_body">
                            
                        </tbody>
                    </table>
                    <input type="checkbox" checked required> <label>Kandidat yang terpilih sudah sesuai</label>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="done">Done</button>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link href="{{ asset('css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/custom/wizard/wizard-1.js') }}" type="text/javascript"></script>
@include('global.element.script_eqs_peremployee')
<script>

    // fungsi all checked
    $('[data-select-all-click]').click(function() {
        var selfAllSelector = $(this).data('select-all-click');
        boardInputs = $('[data-select-all='+selfAllSelector+'] input');
        boardInputs.prop('checked', !boardInputs.prop('checked') );
        }); 
    $(document).on("click", "#submit_assessment", function(e){
        e.preventDefault();

        var id          = $('#talent_id').val();
        var performance = $('#performance').val();
        var assessment = $('#cci').val();

        console.log(id);
        console.log(performance);
        console.log(assessment);

        if(id!="" && performance!="" && assessment!=""){
            $.ajax({
                url: "{{ url('talent-profiling/submit-assessment') }}",
                type: "POST",
                data: {
                    _token        : "{{ csrf_token() }}",
                    id            : id,
                    performance   : performance,
                    assessment    : assessment
                },
                success: function(message){
                    console.log(message);
                    var message = JSON.parse(message);
                    if(message.status==200){
                        Swal.fire("Berhasil", "", "success");	
                    }
                    else if(message.status==201){
                        Swal.fire("Error", "", "error");
                    }
                }
            });
        }
        else{
            Swal.fire("Harap lengkapi data yang diperlukan!", "", "error");
        }
    })

    //submit
    $('#done_profiling').on('click', function(){
        if($("input[name='talent_id[]']:checked").length) {
            event.preventDefault();
            var listProfiling = $("input:checkbox:checked").map(function(){
                return $(this).attr("data-name");;
            }).toArray();
            var html = "";
            var html_head = `<th scope="col">Choosen Candidates (`+listProfiling.length+`)</th>`;
            for (let p = 0; p < listProfiling.length; p++) {
                html += `<tr>
                            <td>`+ listProfiling[p] +`</td>
                            </tr>`;
            }
            $("#profiling_head").html(html_head);
            $("#profiling_body").html(html);


            $("#modal_profiling").modal();

        } else {
            Swal.fire("Please Select 1 Candidates to Continue!", "", "warning");
        }
    })

    $('#done').on('click', function(){
        $('#form_profiling').submit();
    })
</script>
@endsection
