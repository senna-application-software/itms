{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="row">
    @forelse($position as $item)
    <div class="col-md-4 mb-5">
        <div class="card shadow-sm" style="min-height: 25vh">
            <div class="card-body d-flex flex-column">
                <a href="{{ route('talent-review.detail', $item->id) }}">
                    <h5 class="card-title text-primary mb-2">{{ $item->name_position }}</h5>
                    <span class="text-muted">{{ $item->name_subholding }} - [{{ $item->name_area }}]</span>
                </a>
                <div>
                    <span class="label label-light-warning label-lg label-inline">{{ $item->status }}</span>
                </div>

                <div class="mt-3 mb-3">
                    <table class="table">
                        <tr>
                            <th>Sourcing Type</th>
                            <td>
                                @if($item->sourcing_type == 1)
                                <span class="label font-weight-bold label-lg label-light-success label-inline">External</span>
                                @else
                                <span class="label font-weight-bold label-lg label-light-primary label-inline">Internal</span>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="row mt-5 mt-auto">
                    <div class="col-md-5 align-self-center">
                        Total Candidates
                    </div>
                    <div class="col-md-auto text-center">
                        <h1 class="text-primary">{{ $item->participant }} of {{ $item->total }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @empty
        @if($show_engage)
            <div class="col-md-12 mb-5">
                <div class="card card-custom card-stretch gutter-b">
                    <div class="card-body d-flex p-0">
                        <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) top; background-size: 33% auto; background-image: url(media/svg/humans/custom-8.svg)">
                            <h2 class="text-dark pb-5 font-weight-bolder">Silahkan buat Event.</h2>
                            <p class="text-dark-50 pb-5 font-size-h5">Proses Talent Review belum dapat dilakukan.
                            <br>Silahkan buat <i>event position</i> terlebih dahulu</p>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-12 mb-5">
                <div class="card card-custom card-stretch gutter-b">
                    <div class="card-body d-flex p-0">
                        <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) top; background-size: 33% auto; background-image: url(media/svg/humans/custom-8.svg)">
                            <h2 class="text-dark pb-5 font-weight-bolder">Talent Review.</h2>
                            <p class="text-dark-50 pb-5 font-size-h5">Tidak ada event <i>Talent Review</i> yang sedang berlangsung.</p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforelse
</div>
@if(Session::has('id_event'))
    <a href="{{ url('talent-review/berita-acara/'. Session::get('id_event')) }}" id="download-ba" hidden>Download BA</a>
@endif
@endsection

{{-- Styles Section --}}
@section('styles')
<link href="{{asset('plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
<script src="{{asset('js/pages/crud/forms/widgets/select2.js')}}"></script>
@if (session('status') == 'success')
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "{{ session('title') }}",
            text: "{{ session('message') }}",
            icon: "{{ session('status') }}",
            buttonsStyling: false,
            confirmButtonText: "close",
            customClass: {
                confirmButton: "btn btn-primary"
            }
        })

        if(typeof $("#download-ba") !== "undefined") {
       document.getElementById('download-ba').click();
       $(".page-loader").css("display","none");
   }
    });
</script>
@endif

@endsection
