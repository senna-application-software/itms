<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
            * {
                font-family: Arial, Helvetica, sans-serif;
            }
            
            @page { margin: 2cm; }
            header { position: fixed; top: -10px; left: 0px; right: 0px; height: 40px; }
            footer { position: fixed; bottom: -18px; left: 0px; right: 0px; height: 20px; }

            table, th, td {
                font-size: 14px;
                text-align:left;
                padding: 2px;
            }

            table.comite { 
                border-spacing: 0;
                border-collapse: collapse;
                font-size: 10px;
            }

            table.kandidat { 
                border-spacing: 0;
                border-collapse: collapse;
                font-size: 14px;
                border: 1px solid #000;
                width: 100%;

            }
            
            table.kandidat td { 
                border-spacing: 0;
                border-collapse: collapse;
                font-size: 14px;
                border: 1px solid #000;
            }
            
            table.ttd { 
                border-spacing: 0;
                border-collapse: collapse;
                font-size: 14px;
                border: 1px solid transparent;
                width: 100%;
            }
        </style>
    </head>
    <body>
        @if ($sub_event[0]->code_subholding == 'ap2' )
            <div style="float: left height: 50px; width: 180px; margin-top:15px;">
            @elseif($sub_event[0]->code_subholding == 'twc' || $sub_event[0]->code_subholding == 'itdc')
            <div style="float: left height: 50px; width: 180px; margin-top:10px;">
            @elseif($sub_event[0]->code_subholding == 'hin' || $sub_event[0]->code_subholding == 'snh' || $sub_event[0]->code_subholding == 'ap1')
            <div style="float: left height: 50px; width: 180px;">
        @endif
            <img src="{{ public_path('images/logo-member/inj.png') }}" style="width: 100%;">
        </div>
        @if ($sub_event[0]->code_subholding != 'inj')
        <div style="float: right; height: 50px; width: 180px;">
            @if ($sub_event[0]->code_subholding == 'twc')
            <img src="{{ public_path('images/logo-member') }}/twc.png" style="width:75%; float:right;" >

            @elseif($sub_event[0]->code_subholding == 'itdc')
            <img src="{{ public_path('images/logo-member') }}/itdc.png" style="width:75%; float:right;" >
            @else 
            <img src="{{ public_path('images/logo-member') }}/{{$sub_event[0]->code_subholding}}.png" style="width:100%;" >
            @endif
           
        </div>            
        @endif

        <br>
        <br>
        <br>
        <br>

        <footer>Print date : {{ date('Y-m-d') }}</footer>
        <h3 style="text-align:center">
            BERITA ACARA KESEPAKATAN<br/>
            HASIL PENILAIAN TALENT REVIEW
        </h3>
        <p style="text-align:center">Nomor : {{ $nomor_ba }}</p>
  

        @php
            $tanggal = hari_ini();
        @endphp

        <p style="word-spacing: 4px; line-height: 1.8; text-align:justify;"">Pada hari {{ $tanggal["nama_hari"] }} tanggal {{ ucwords(strtolower(bilangantext($tanggal["tgl"]))) }}  
            bulan {{ $tanggal["nama_bulan"] }} tahun {{ ucwords(strtolower(bilangantext($tanggal["thn"]))) }} ({{ $tanggal["tanggal"] }}) 
            bertempat di PT Aviasi Pariwisata Indonesia (Persero), 
            Talent Committee yang bertanda tangan di bawah ini:
        </p>
        <table  style="margin-left:40px;">
            @foreach ($tc as $t)
            <tr></tr>
                <tr>
                    <td style="width:3% font-size:15px;" rowspan="3" valign="top">{{ $loop->iteration }} .</td>
                    <td valign="top" style="width:16%; font-size:15px;">Nama</td>
                    <td valign="top" style="width:1%; font-size:15px;">:</td>
                    <td valign="top" style="width:80%; font-size:15px;">{{ ucwords(strtolower($t['name'])) }}</td>
                </tr>
                <tr>
                    <td valign="top" style="font-size:15px;">Jabatan</td>
                    <td valign="top" style="font-size:15px;">:</td>
                    <td valign="top" style="font-size:15px;">{{ $t['position'] }} {{ $t['organisasi'] }}</td>
                </tr>
            @endforeach
        </table>

        <p style="word-spacing: 5px; line-height: 1.8; text-align:justify; ">
            Sesuai dengan hasil pembahasan Talent Review dalam Talent Committee Meeting, 
            telah disepakati nominated candidates untuk pengisian jabatan 
            {{ ucwords(strtolower($sub_event[0]->name_position)) }} {{ $sub_event[0]->name_subholding }} adalah sebagai berikut:
        </p>
        <br/>

        <table class="kandidat">
            <tr>
                <td style="text-align: center" valign="top">NO</td>
                <td style="text-align: center" valign="top">NAMA</td>
                <td style="text-align: center; width:80px" valign="top">NIP</td>
                <td style="text-align: center" valign="top">JABATAN SAAT INI</td>
                <td style="text-align: center" valign="top">NILAI EQS</td>
                <td style="text-align: center" valign="top">REKOMENDASI TALENT DAY</td>
            </tr>
            @foreach ($sub_event as $index => $se)
            @if ($se->kode_posisi != null)
                <tr>
                    <td  style="text-align: center; font-size:15px;">{{$index+1}}</td>
                    <td style="font-size:15px;">{{ ucwords(strtolower($se->personnel_number)) ? ucwords(strtolower($se->personnel_number)) : "-" }}</td>
                    <td style="font-size:15px;">{{ $se->prev_persno ? $se->prev_persno : "-"  }}</td>
                    <td style="font-size:15px;">{{ getPosisi($se->kode_posisi) ? getPosisi($se->kode_posisi) : "-"  }}</td>
                    <td style="font-size:15px;">{{ nilai_eqs($se->prev_persno, false , $se->id_sub_event) }}</td>
                    <td  style="text-align: center; font-size:15px;">{{ assessment_label(final_mark_assessment($se->id_talent)) ?? "-" }}</td>
                </tr>
            @endif
            @endforeach
        </table>
        <br/>

        <p style="word-spacing: 5px; line-height: 1.8; text-align:justify; ">
            Demikian berita acara kesepakatan ini dibuat dengan sebenarnya dan ditandatangani 
            pada hari dan tanggal tersebut untuk dapat dipergunakan sebagaimana mestinya.
        </p>
        <center> 
            <table style="margin-top: 30px; width:100%;" >
             <tr>
              @php
                 $no = 0;                 
              @endphp
            @foreach ($tc as $row)
            @if ($no == 3)
            @php
                $no = 0;
            @endphp
                 </tr>
                 </table>
                 {{-- tabel ini di kasih class auto page break --}}
                 <table style="margin-top:70px; width:100%;">
                 <tr >
            @endif
            
             <td style=" width:30%; font-size:15px; text-align:center; word-wrap:no-wrap">
             {{$row['position']}} <br> {{ $row['organisasi'] }}
             <br> <br> <br> <br> <br> <br> <br> <br>
             {{ucwords(strtolower($row['name']))}}
             </td>
             @php
              $no++;
                 
             @endphp
  
            @endforeach
             
             </tr>
            </table> 
         </center>
    </body>
</html>