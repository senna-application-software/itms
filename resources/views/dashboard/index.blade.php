{{-- Extends layout --}}
@php
    if(Sentinel::check()->inRole('executive')) {
        $extend = 'layout.default';
    } else {
        $extend = 'layout.default';
    }
@endphp

{{-- @extends('layout.default') --}}
@extends($extend)

{{-- Content --}}
@section('content')

    @if (!$show_engage)
        @switch($level_akses)
            @case('employee')
                @include('dashboard.dashboard_employee', ['class' => 'card-stretch gutter-b'])
            @break

            @case('talent_committee')
                @include('dashboard.dashboard_executive', ['class' => 'card-stretch gutter-b'])
            @break
            
            @case('admin')
                @include('dashboard.dashboard_admin', ['class' => 'card-stretch gutter-b'])
            @break

            @case('executive')
                @include('dashboard.dashboard_talent_committee', ['class' => 'card-stretch gutter-b'])
            @break

            @default
                @include('banner_no_event', ['class' => 'card-stretch gutter-b'])
            @break

        @endswitch
    @else
        @include('banner_no_event', ['class' => 'card-stretch gutter-b'])
    @endif

@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css"/>
    <style type="text/css" id="diagramStylesExport">
        #inJourneyDiagram {
            width: 100%;
            height: 100%;
        }

        .node rect {
            text: blue;
            stroke: transparent;
            fill: #57BFC4;
        }

        table td {
            padding: 10px;
        }

        .node text {
            fill: #fff
        }

        .path {
            stroke: black !important;
            stroke-width: 3px;
        }

        #bg-ppdf-btns {
            min-width: 270px;
            font-family: ooredoo, Segoe UI, Roboto, Helvetica, Arial, sans-serif;
        }

        foreignObject {
            font-size: 18px;
            color: white;
        }

        .node.directorate rect {
            fill: #584f4f;
        }

        .node.chief rect {
            fill: #528dd5;
        }
        
        .node.group rect {
            fill: #92d050;
        }
        
        .node.division rect {
            fill: #ffc000;
        }

        .node.department rect {
            fill: #ff98ff;
        }

        .node.expert rect {
            fill: #6363f0;
        }

        .node.staff rect {
            fill: #8800ff;
        }
    </style>
@endsection

@section('modal')
    <div class="modal fade" id="modalPosition" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">

    </div>

    <div class="modal fade" id="modalEmployee" data-backdrop="static" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">

    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/wizard/wizard-1.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        
    @include('global.element.script_dashboard')

    $("#directorateOrganization").select2({});
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    function tableInit() {
        KTApp.block('#vacantTable', {
            overlayColor: '#fff',
            state: 'primary',
            message: 'Processing...'
        })
        let position_type = $("#filterVacantTable").val();
        $.ajax({
            type: "POST",
            url: "{{ url('/vacant-table') }}",
            data: {
                _token: CSRF_TOKEN,
                position_type: position_type
            },
            success: function (response) {
                KTApp.unblock('#vacantTable');
                $("#vacantTable tbody").html("");
                $("#vacantTable tbody").html(response);
            }
        });
    }

    $('#table-history').DataTable();

    function createEventPosition(obj) {
            let code_position = $(obj).attr('data-position_code');
            let id = $(obj).attr('id');
            $.ajax({
                url: '{{ route('pending_task.add.position') }}',
                method: 'get',
                data: {
                    id: id,
                },
                success: function(res) {
                    $('#modalPosition').html(res);
                    $('#modalPosition').modal('show');
                    getEligible(code_position);
                }
            })


        }

        function getEligible(code_position) {
            $.ajax({
                method: 'GET',
                url: "{{ url('/sub-event/get-attribute-position') }}" + "/" + code_position,
                success: function(response) {
                    $('#employee_eligible').val(response.eligible);
                },
                error: function() {
                    Swal.fire('Sorry', 'Something Went Wrong !', 'error');
                }
            });
        }

        function createEventEmployee(obj) {
            let id = $(obj).attr('id');
            $.ajax({
                url: '{{ route('pending_task.add.employee') }}',
                method: 'get',
                data: {
                    id: id,
                },
                success: function(res) {
                    $('#modalEmployee').html(res);
                    $('#modalEmployee').modal('show');
                    $('#position').select2();
                    $('#listPosition').select2();

                }
            })
        }

    </script>

    @if (session('status') == 'success')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
            });
        </script>
        @endif

        @if (session('status') == 'error')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}",
                    icon: "{{ session('status') }}",
                    buttonsStyling: false,
                    confirmButtonText: "close",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
            });
        </script>
    @endif
    
    @if (!$show_engage)
            {{-- @case('employee')
                @include('dashboard.dashboard_employee', ['class' => 'card-stretch gutter-b'])
            @break --}}

            {{-- @case('talent_committee')
                @include('dashboard.dashboard_talent_committee', ['class' => 'card-stretch gutter-b'])
            @break --}}

        @if($level_akses =='admin')
            <script src="https://code.highcharts.com/highcharts.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    tableInit();
                    $("#filterVacantTable").change(function() {
                        tableInit();
                    })
                    $("#history_event").change(function() {
                        var ss = $("#history_event").val();
                        window.location.href = "{{URL::to('sub-event/detail-subevent/')}}"+"/"+ss;
                    })
                });
            </script>
        @endif
        
        @if($level_akses == 'talent_committee')
            {{-- Init External Script --}}
            <script src="https://code.highcharts.com/highcharts.js"></script>
            <script src="{{ asset('js/organization/orgchart.js') }}"></script>
            {{-- Global Init --}}
            <script>
                $(document).ready(function() {
                    // Organization
                    directorateOrganization();
                    $("#subholdingOrganization").change(function() {
                        directorateOrganization();
                    })

                    // Organization
                    chartOrganization();
                    $("#directorateOrganization").change(function() {
                        chartOrganization();
                    });
                    
                    chartEmployeeEducation();
                    $("#subholdingEmployeeEducation").change(function() {
                        chartEmployeeEducation();
                    })

                    chartEmployeeDemograph();
                    $("#subholdingEmployeeGraph").change(function() {
                        chartEmployeeDemograph();
                    });

                    // chartSDMKPI();
                    // chartFemaleKPI();
                    // chartMilenialKPI();
                    // chartTalentDevelopment();
                    // $("#subholdingMonitoringKPI").change(function() {
                    // //     // chartSDMKPI();
                    // //     // chartFemaleKPI();
                    // //     // chartMilenialKPI();
                    //     chartTalentDevelopment();
                    // })
                })
            </script>
            {{-- Init Diagram --}}
            <script>            
                function chartEmployeeDemograph() {
                    subholding = $("#subholdingEmployeeGraph").val()
                    KTApp.block('#containerEmployeeDemograph', {
                        overlayColor: '#fff',
                        state: 'primary',
                        message: 'Processing...'
                    });
                    $.ajax({
                        method: "POST",
                        url: "{{ route('employee_demograph') }}",
                        data: {
                            _token: "{{ csrf_token() }}",
                            subholding: subholding
                        },
                        success: function(res) {
                            let now = new Date().getFullYear();
                            let label_gen_z = `Gen Z (< ${now - 1996} Tahun)`
                            let label_gen_y = `Gen Y (${now - 1996} Tahun s/d ${now - 1980} Tahun)`
                            let label_gen_x = `Gen X (> ${now - 1980} Tahun)`
                            Highcharts.chart("genChart", {
                                chart: {
                                    type: 'bar'
                                },
                                title: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: [label_gen_z, label_gen_y, label_gen_x]
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: 'Total Employee'
                                    }
                                },
                                legend: {
                                    reversed: true
                                },
                                plotOptions: {
                                    series: {
                                        stacking: 'normal',
                                    },
                                    bar: {
                                        dataLabels: {
                                            enabled: true,
                                            inside: true,
                                            color: "#000000",
                                            borderColor: "#000000",
                                            style: {
                                                fontWeight: '',
                                                textOutline: 'none',
                                            },
                                            align: 'right'
                                        }
                                    }
                                },
                                colors: ['#87C6FE', '#FFA1EB'],
                                series: [{
                                    name: 'Male',
                                    data: [res.male_gen_z, res.male_gen_y, res.male_gen_x]
                                }, {
                                    name: 'Female',
                                    data: [res.female_gen_z, res.female_gen_y, res.female_gen_x]
                                }],
                                credits: false
                            });
                            $("#persentaseMale").html(`${res.persentase_total_male}%`);
                            $("#persentaseFemale").html(`${res.persentase_total_female}%`);
                            $("#totalMale").html(`(${res.total_male} of ${res.total})`);
                            $("#totalFemale").html(`(${res.total_female} of ${res.total})`);
                            $("#containerJenjangPendidikan").html("");
                            $("#containerJenjangPendidikan").html(res.card);
                            KTApp.unblock("#containerEmployeeDemograph");
                        },
                        error: function(res) {
                            KTApp.unblock("#containerEmployeeDemograph");
                            console.log("Oops Something Wrong");
                        }
                    })
                }

                function directorateOrganization() {
                    subholding = $("#subholdingOrganization").val();
                    $.ajax({
                        method: "POST",
                        url: "{{ route('directorat_subholding') }}",
                        data: {
                            _token: "{{ csrf_token() }}",
                            subholding: subholding
                        },
                        success: function(res) {
                            html = '<option value="">All Data</option>';
                            $.each(res, function (indexInArray, valueOfElement) {
                                html += `<option value="${valueOfElement.code_directorate}">${valueOfElement.directorate.name_directorate}</option>`
                            });
                            
                            $("#directorateOrganization").html(html)
                            $("#directorateOrganization").val("").trigger("change");
                            // if (res.length > 0) {
                                // $("#directorateOrganization").val(res[0].code_directorate).trigger("change");
                            // }
                        }
                    })
                }

                function chartOrganization() {
                    KTApp.block('#containerChartOrg', {
                        overlayColor: '#fff',
                        state: 'primary',
                        message: 'Processing...'
                    });
                    $.ajax({
                        method: "GET",
                        url: "{{ route('organization_diagram') }}",
                        data: {
                            _token: CSRF_TOKEN,
                            subholding: $("#subholdingOrganization").val(),
                            code_directorate: $("#directorateOrganization").val()
                        },
                        success: function(res) {
                            var chart;
                            chart = new OrgChart(document.getElementById("inJourneyDiagram"), {
                                mouseScrool: OrgChart.action.scroll,
                                lazyLoading: true,
                                template:   'olivia',
                                orientation: OrgChart.orientation.top,
                                enableDragDrop:false,
                                siblingSeparation: 50,
                                scaleInitial:.5,
                                toolbar:!0,
                                    collapse: {
                                    level: 2,
                                    allChildren: true
                                },
                                // menu: {
                                //     pdfPreview: { 
                                //         text: "PDF Preview", 
                                //         icon: OrgChart.icon.pdf(24,24, '#7A7A7A'),
                                //         onClick: preview
                                //     },
                                //     pdf: { text: "Export PDF" },
                                //     png: { text: "Export PNG" },
                                //     svg: { text: "Export SVG" },
                                //     csv: { text: "Export CSV" }
                                // },
                                // nodeMenu: {
                                //     pdf: { text: "Upload Job Desc" },
                                //     png: { text: "Export PNG" },
                                //     svg: { text: "Export SVG" }
                                // },
                                nodeBinding: {
                                    field_0: "Job Holder",
                                    field_1: "Position",
                                    field_2: "Number of Successor",

                                    img_0: "img"
                                },
                                nodes: res
                            });
                            KTApp.unblock("#containerChartOrg");
                        }
                    })
                }

                function chartEmployeeEducation() {
                    KTApp.block('#containerTierEducationEmployee', {
                        overlayColor: '#fff',
                        state: 'primary',
                        message: 'Processing...'
                    });
                    subholding = $("#subholdingEmployeeEducation").val()
                    $.ajax({
                        method: "POST",
                        url: "{{ route('employee_education') }}",
                        data: {
                            _token: "{{ csrf_token() }}",
                            subholding: subholding
                        },
                        success: function(res) {
                            $("#chartTierEducationEmployee").html("");
                            let element = document.getElementById("chartTierEducationEmployee");
                            let height = parseInt(KTUtil.css(element, 'height'));

                            if (!element) {
                                return;
                            }

                            let options = {
                                series: [{
                                    name: '-',
                                    data: [
                                        res.SLTA-5,
                                        res.DI-5,
                                        res.DII-5,
                                        res.DIII-5,
                                        res.DIV-5,
                                        res.S1-5,
                                        res.S2-5,
                                        res.S3-5
                                    ]
                                }, {
                                    name: 'Data',
                                    data: [
                                        res.SLTA,
                                        res.DI,
                                        res.DII,
                                        res.DIII,
                                        res.DIV,
                                        res.S1,
                                        res.S2,
                                        res.S3
                                    ]
                                }],
                                chart: {
                                    type: 'bar',
                                    height: height,
                                    toolbar: {
                                        show: false
                                    },
                                    sparkline: {
                                        enabled: true
                                    },
                                },
                                plotOptions: {
                                    bar: {
                                        horizontal: false,
                                        columnWidth: ['30%'],
                                        endingShape: 'rounded'
                                    },
                                },
                                legend: {
                                    show: false
                                },
                                dataLabels: {
                                    enabled: false
                                },
                                stroke: {
                                    show: true,
                                    width: 1,
                                    colors: ['transparent']
                                },
                                xaxis: {
                                    categories: ['SLTA', 'D.1', 'D.2', 'D.3', 'D.4', 'S.1', 'S.2', 'S.3'],
                                    axisBorder: {
                                        show: false,
                                    },
                                    axisTicks: {
                                        show: false
                                    },
                                    labels: {
                                        style: {
                                            colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                                            fontSize: '12px',
                                            fontFamily: KTApp.getSettings()['font-family']
                                        }
                                    }
                                },
                                yaxis: {
                                    min: 0,
                                    max: 100,
                                    labels: {
                                        style: {
                                            colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                                            fontSize: '12px',
                                            fontFamily: KTApp.getSettings()['font-family']
                                        }
                                    }
                                },
                                fill: {
                                    type: ['solid', 'solid'],
                                    opacity: [0.25, 1]
                                },
                                states: {
                                    normal: {
                                        filter: {
                                            type: 'none',
                                            value: 0
                                        }
                                    },
                                    hover: {
                                        filter: {
                                            type: 'none',
                                            value: 0
                                        }
                                    },
                                    active: {
                                        allowMultipleDataPointsSelection: false,
                                        filter: {
                                            type: 'none',
                                            value: 0
                                        }
                                    }
                                },
                                tooltip: {
                                    style: {
                                        fontSize: '12px',
                                        fontFamily: KTApp.getSettings()['font-family']
                                    },
                                    y: {
                                        formatter: function (val) {
                                            return val;
                                        }
                                    },
                                    marker: {
                                        show: false
                                    }
                                },
                                colors: ['#ffffff', '#ffffff'],
                                grid: {
                                    borderColor: KTApp.getSettings()['colors']['gray']['gray-200'],
                                    strokeDashArray: 4,
                                    yaxis: {
                                        lines: {
                                            show: true
                                        }
                                    },
                                    padding: {
                                        left: 20,
                                        right: 20
                                    }
                                }
                            };

                            let chart = new ApexCharts(element, options);
                            chart.render();

                            $("#valueSLTA").html(res.SLTA ?? 0);
                            $("#valueDI").html(res.DI ?? 0);
                            $("#valueDII").html(res.DII ?? 0);
                            $("#valueDIII").html(res.DIII ?? 0);
                            $("#valueDIV").html(res.DIV ?? 0);
                            $("#valueS1").html(res.S1 ?? 0);
                            $("#valueS2").html(res.S2 ?? 0);
                            $("#valueS3").html(res.S3 ?? 0);
                            KTApp.unblock("#containerTierEducationEmployee");
                        },
                        error: function(res) {
                            KTApp.unblock("#containerTierEducationEmployee");
                            console.log("Oops Something Wrong");

                        }
                    })
                }

                function chartSDMKPI() {
                    Highcharts.chart('chartSDMKPI', {
                        chart: {
                            type: 'item'
                        },

                        title: {
                            text: '% pemenuhan SDM pada Struktur Organisasi'
                        },

                        subtitle: {
                            text: ''
                        },

                        legend: {
                            labelFormat: '{name}'
                        },
                        series: [{
                            name: 'Representatives',
                            keys: ['name', 'y', 'color', 'label'],
                            data: [                           
                                ['Total', Math.floor(Math.random() * 101), '#fab54c', ''],
                                ['Terisi', Math.floor(Math.random() * 101), '#53c3c7', ''],
                                ['Vacant', Math.floor(Math.random() * 101), '#a0bd3e', ''],
                            ],
                            dataLabels: {
                                enabled: true,
                                format: '{point.label}'
                            },

                            // Circular options
                            center: ['50%', '88%'],
                            size: '120%',
                            startAngle: -100,
                            endAngle: 100
                        }],

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 600
                                },
                                chartOptions: {
                                    series: [{
                                        dataLabels: {
                                            distance: -30
                                        }
                                    }]
                                }
                            }]
                        },
                        credits: false
                    });

                }

                function chartFemaleKPI() {
                    Highcharts.chart('chartFemaleKPI', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: '% Perempuan Dalam Nominated Talent'
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: false,
                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                                }
                            }
                        },
                        colors: ['#fab54c', '#53c3c7'],
                        series: [{
                            name: 'data',
                            colorByPoint: true,
                            data: [{
                                name: 'Top Talent Perempuan',
                                y: Math.floor(Math.random() * 101),
                            }, {
                                name: 'Nominated Talent',
                                y: Math.floor(Math.random() * 101)
                            }],
                            innerSize: '60%'
                        }],
                        credits: false
                    });
                }
                
                function chartMilenialKPI() {
                    Highcharts.chart('chartMilenialKPI', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: '% Milennial Dalam Nominated Talent'
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: false,
                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                                }
                            }
                        },
                        colors: ['#a0bd3e', '#db715e'],
                        series: [{
                            name: 'data',
                            colorByPoint: true,
                            data: [{
                                name: 'Top Talent Milenial',
                                y: Math.floor(Math.random() * 101),
                            }, {
                                name: 'Nominated Talent',
                                y: Math.floor(Math.random() * 101)
                            }],
                            innerSize: '60%'
                        }],
                        credits: false
                    });
                }

                function chartTalentDevelopment() {
                    Highcharts.chart('chartTalentDevelopment', {
                        chart: {
                            type: 'column',
                            options3d: {
                                enabled: true,
                                alpha: 20,
                                viewDistance: 25,
                                depth: 40
                            }
                        },
                        title: {
                            text: 'Implementasi / penyusunan kebijakan terkait Talent Development & Career Path System'
                        },
                        subtitle: {
                            text: ''
                        },
                        plotOptions: {
                            column: {
                                depth: 35
                            }
                        },
                        xAxis: {
                            categories: ['Data'],
                            labels: {
                                skew3d: true,
                                style: {
                                    fontSize: '16px'
                                }
                            }
                        },
                        yAxis: {
                            title: {
                                text: null
                            }
                        },
                        colors: ['#db715e', '#a438fc', '#53c3c7', '#fab54c', '#a0bd3e', '#acb5b5'],
                        series: [{
                            name: 'Angkasa Putra I',
                            data: [Math.floor(Math.random() * 101)]
                        }, {
                            name: 'Angkasa Putra II',
                            data: [Math.floor(Math.random() * 101)]
                        }, {
                            name: 'Hotel Indonesia Natour',
                            data: [Math.floor(Math.random() * 101)]
                        }, {
                            name: 'Sarinah',
                            data: [Math.floor(Math.random() * 101)]
                        }, {
                            name: 'Taman Wisata Candi',
                            data: [Math.floor(Math.random() * 101)]
                        }, {
                            name: 'Indonesia Tourism Development Corporation',
                            data: [Math.floor(Math.random() * 101)]
                        }],
                        credits: false
                    });
                }

                function preview(){
                    OrgChart.pdfPrevUI.show(chart, {
                        format: 'A4'
                    });
                }
            </script>
            @endif
            @if($level_akses == "talent_committee" | $level_akses == "admin")
            {{-- Init Chart --}}
            <script src="https://code.highcharts.com/modules/item-series.js"></script>
            <script src="https://code.highcharts.com/highcharts-3d.js"></script>
            <script src="https://code.highcharts.com/highcharts-more.js"></script>
            <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
            <script>
                $(document).ready(function () {
                    init_talent_mapping();
                    chartTalentDevelopment_p3();
                    chartCompetenciesMember();
                    chartCompetenciesMemberWeak();
                    $(".subholding-select, #directorateOrganization, #subholdingOrganization").select2({
                        width: '100%'
                    });
                    $("#history_event").select2({
                        width: '100%'
                    });
                    // $(".subholding-select").val("avt");

                    $("g .highcharts-legend-item").click(function() {
                        if (!$(this).hasClass("highcharts-legend-item-hidden")){
                            hardcode_color = $(this).find("rect").attr("default");
                            $(this).find("rect").attr("fill", hardcode_color);
                        }
                    }); 

                    // Top 10 Positions
                    let topPositionsTable = $("#topPositionsTable").DataTable( {
                        dom: 'ft',
                        processing: true,
                        serverSide: true,
                        lengthMenu: [10],
                        pageLength: 10,
                        ajax: {
                            method: "GET",
                            url: "{{ route('top_desired_position.datatable') }}",
                            data: function(d) {
                                d._token = CSRF_TOKEN;
                                d.subholding = $("#subholdingDesiredPositions").val()
                            }
                        },
                        columns: [
                            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
                            {data: 'position'},
                            {data: 'job_grade_align'},
                            {data: 'job_family_align'},
                            {data: 'directorate'},
                        ]
                    } );
                    
                    chartTopPositions();
                    $("#subholdingDesiredPositions").change(function() {
                        topPositionsTable.ajax.reload();
                        chartTopPositions();
                    });
                });

                function chartTopPositions() {
                    $.ajax({
                        method: "GET",
                        url: "{{ route('top_desired_position.chart') }}",
                        data: {
                            _token: CSRF_TOKEN,
                            subholding: $("#subholdingDesiredPositions").val()
                        },
                        success: function(dataTopPositions) {
                            Highcharts.chart('topPositionsChart', {
                                chart: {
                                    type: 'pie',
                                    options3d: {
                                        enabled: true,
                                        alpha: 45,
                                    }
                                },
                                title: {
                                    text: ''
                                },
                                tooltip: {
                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                },
                                plotOptions: {
                                    pie: {
                                        innerSize: 80,
                                        depth: 50,
                                        allowPointSelect: true,
                                        cursor: 'pointer',
                                        dataLabels: {
                                            enabled: false,
                                            format: '<b>{point.name}</b>: {point.percentage:.1f}'
                                        },
                                        showInLegend: true,
                                    }
                                },
                                series: [{
                                    colorByPoint: true,
                                    data: dataTopPositions
                                }],
                                credits: false
                            });
                        }
                    })
                }

                function init_talent_mapping() {
                    KTApp.block('#mapping-talent', {
                        overlayColor: '#fff',
                        state: 'primary',
                        message: 'Processing...'
                    });
                
                    $.ajax({
                        type: "POST",
                        url: "{{ url('/chart-talent-mapping-boxed') }}",
                        data: {
                            _token: CSRF_TOKEN,
                            sub: $('#talentMappingFilter').val()
                        },
                        serverSide: true,
                        processing: true,
                        success: function (response) {
                            KTApp.unblock('#mapping-talent');
                            let box_a = 0, box_a_persen = 0, box_b = 0, box_b_persen = 0, box_c = 0, box_c_persen = 0, box_d = 0, box_d_persen = 0, box_e = 0, box_e_persen = 0;
                            $.each(response, function (indexInArray, valueOfElement) {
                                $("#panel-talent-"+indexInArray).children('h5').html(valueOfElement.percentage+"%");
                                $("#panel-talent-"+indexInArray).children('h6').html(valueOfElement.total);
                                if(indexInArray == 1) {
                                    box_e += valueOfElement.total;
                                    box_e_persen += valueOfElement.percentage;
                                } else if (indexInArray == 2 | indexInArray == 4) {
                                    box_d += valueOfElement.total;
                                    box_d_persen += valueOfElement.percentage;
                                } else if (indexInArray == 3 | indexInArray == 6) {
                                    box_c += valueOfElement.total;
                                    box_c_persen += valueOfElement.percentage;
                                } else if (indexInArray == 5 | indexInArray == 7 | indexInArray == 8) {
                                    box_b += valueOfElement.total;
                                    box_b_persen += valueOfElement.percentage;
                                } else if (indexInArray == 9) {
                                    box_a += valueOfElement.total;
                                    box_a_persen += valueOfElement.percentage;
                                }
                            });
                            let data = [{
                                'y': box_a,
                                'persen': box_a_persen
                            },
                            {
                                'y': box_b,
                                'persen': box_b_persen
                            },
                            {
                                'y': box_c,
                                'persen': box_c_persen
                            },
                            {
                                'y': box_d,
                                'persen': box_d_persen
                            },
                            {
                                'y': box_e,
                                'persen': box_e_persen
                            }];
                            // init_mapping_chart(data);
                        }
                    });
                }

                function init_mapping_chart(data) {
                    KTApp.block('#talent-mapping-chart2', {
                        overlayColor: '#fff',
                        state: 'primary',
                        message: 'Processing...'
                    });

                    Highcharts.chart("talent-mapping-chart2", {
                        chart: {
                            type: "bar",
                            height: "400",
                        },
                        // colors: ['#FFA800', '#3699FF', '#9c9c9c', '#02ad2a', '#006317', '#181C32'],
                        credits: {
                            enabled: false,
                        },
                        xAxis: {
                            categories: ['High Potential', 'Promotable', 'Solid Contributor', 'Sleeping Tiger', 'Unfit'],
                            crosshair: true,
                            type: "category",
                            visible: true
                        },
                        title: {
                            text: ""
                        },   
                        legend: {
                            enabled: false,
                        },
                        tooltip: {
                            formatter: function() {
                                return this.point.category +": " +`<span>`+this.point.persen+`%</span>)`
                            },              
                            share: true,
                            useHTML: true
                        },
                        plotOptions: {
                            series: {
                                colorByPoint: true,
                                colors: ['#A5C144', '#4DC2C6', '#F9B04F', '#EC6A56', '#c2c2c2']
                            }
                        },
                        series: [
                            {
                                data: data
                            }
                        ]
                    });

                    KTApp.unblock("#talent-mapping-chart2");
                }

                function chartTalentDevelopment_p3() {
                    Highcharts.chart('chartTalentDevelopment_p3', {
                        chart: {
                            type: 'solidgauge',
                            height: '110%',
                        },

                        title: {
                            text: '',
                            style: {
                                fontSize: '24px'
                            }
                        },

                        tooltip: {
                            enabled: true,
                            borderWidth: 0,
                            backgroundColor: 'none',
                            shadow: false,
                            style: {
                                fontSize: '16px'
                            },
                            valueSuffix: '%',
                            pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}</span>',
                            positioner: function (labelWidth) {
                                return {
                                    x: (this.chart.chartWidth - labelWidth) / 2,
                                    y: (this.chart.plotHeight / 2) - 30
                                };
                            }
                        },

                        pane: {
                            startAngle: 0,
                            endAngle: 360,
                            background: [
                            @foreach($option_pemenuhan_sdm->color as $item)
                            {
                                outerRadius: '{{ $option_pemenuhan_sdm->radius += 10 }}%',
                                innerRadius: '{{ $option_pemenuhan_sdm->inner_radius += 10 }}%',
                                backgroundColor: Highcharts.color("{{ $item }}")
                                    .setOpacity(0.3)
                                    .get(),
                                borderWidth: 0
                            }, 
                            @endforeach
                            ]
                        },

                        yAxis: {
                            min: 0,
                            max: 100,
                            lineWidth: 0,
                            tickPositions: []
                        },

                        plotOptions: {
                            solidgauge: {
                                dataLabels: {
                                    enabled: false
                                },
                                linecap: 'round',
                                stickyTracking: false,
                                rounded: true,
                                showInLegend: true,
                            },
                        },
                        series: [
                        @foreach($option_pemenuhan_sdm->color as $key => $item)
                        {
                            name: '{{ $key }}',
                            marker: {
                                fillColor: Highcharts.color("{{ $item }}")
                            },
                            data: [{
                                color: "{{ $item }}",
                                radius: '{{ ($option_pemenuhan_sdm->radius == 100) ? $option_pemenuhan_sdm->radius = 40 : $option_pemenuhan_sdm->radius += 10 }}%',
                                innerRadius: '{{ ($option_pemenuhan_sdm->inner_radius == 91) ? $option_pemenuhan_sdm->inner_radius = 30 : $option_pemenuhan_sdm->inner_radius += 10 }}%',
                                {{-- y: {{ isset($pemenuhan_sdm[$key]) ? array_sum(array_column($pemenuhan_sdm[$key], 'is_change')) : 0 }} --}}
                                y: {{ isset($pemenuhan_sdm[$key]) ? $pemenuhan_sdm[$key] : 0 }}
                            }]
                        }, 
                        @endforeach
                        ],
                        credits: false
                    });

                    // re-color Pemenuhan SDM
                    @foreach($option_pemenuhan_sdm->color as $key => $item)
                    $("g text:contains('{{$key}}')").parent().find("rect").attr("fill", "{{$item}}")
                    $("g text:contains('{{$key}}')").parent().find("rect").attr("default", "{{$item}}")
                    @endforeach
                }
                function chartCompetenciesMember() {
                    data = 1;
                    Highcharts.chart("chartCompetenciesMember", {
                        chart: {
                            type: 'bar'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: [
                                @foreach (current($top_strong_competency) as $competency => $value)
                                "{{$competency}}",
                                @endforeach
                            ],
                            title: {
                                text: null
                            },
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: '',
                                align: 'high'
                            },
                            labels: {
                                overflow: 'justify'
                            },
                            max: 100
                        },
                        tooltip: {
                            // pointFormat: 'Job Fit {series.name}: <b>{point.tt}</b> ({point.perholding}%)<br/>'
                            pointFormat: 'Average Job Fit {series.name}: ({point.perholding}%)<br/>'
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal',
                            }
                        },
                        colors: ['#db715e', '#a438fc', '#53c3c7', '#fab54c', '#a0bd3e', '#acb5b5', '#3699ff'],
                        credits: {
                            enabled: false
                        },
                        series: [
                        @foreach($top_strong_competency as $key => $item) 
                        {
                            name: '{{$key}}',
                            data: [
                                @foreach ($item as $value)
                                {
                                    y: {{ $value['result'] }},
                                    tt: {{ $value['raw_data'] }},
                                    perholding: {{ $value['result_perholding'] }},
                                },
                                @endforeach
                            ],
                        },
                        @endforeach
                        ],
                        credits: false
                    });
                }

                function chartCompetenciesMemberWeak() {
                    data = 1;
                    Highcharts.chart("chartCompetenciesMemberWeak", {
                        chart: {
                            type: 'bar'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: [
                                @foreach (current($top_weak_competency) as $competency => $value)
                                "{{$competency}}",
                                @endforeach
                            ],
                            title: {
                                text: null
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: '',
                                align: 'high'
                            },
                            labels: {
                                overflow: 'justify'
                            },
                            max: 100
                        },
                        tooltip: {
                            // pointFormat: 'Job Fit {series.name}: <b>{point.tt}</b> ({point.perholding}%)<br/>'
                            pointFormat: 'Average Job Fit {series.name}: ({point.perholding}%)<br/>'
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal',
                            }
                        },
                        colors: ['#db715e', '#a438fc', '#53c3c7', '#fab54c', '#a0bd3e', '#acb5b5', '#3699ff'],
                        credits: {
                            enabled: false
                        },
                        series: [
                        @foreach($top_weak_competency as $key => $item) 
                        {
                            name: '{{$key}}',
                            data: [
                                @foreach ($item as $value)
                                {
                                    y: {{ $value['result'] }},
                                    tt: {{ $value['raw_data'] }},
                                    perholding: {{ $value['result_perholding'] }},
                                },
                                @endforeach
                            ],
                        },
                        @endforeach
                        ],
                        credits: false
                    });
                }

                $("#talentMappingFilter").change(function (e) { 
                    e.preventDefault();
                    init_talent_mapping();
                });
            </script>
        @endif

    @else {{-- show engage --}}
        <script type="text/javascript">
            $(document).ready(function () {
                $(document).ready(function () { 
                    tableInit();
                });
            });
        </script>
    @endif

@endsection
