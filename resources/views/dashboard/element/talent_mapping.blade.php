<div class="col-xl-6 col-md-12 col-sm-12">
    {{-- TALENT MAPPING --}}
    <div class="card card-custom card-stretch gutter-b">
        <div class="card-body">
            <div class="row justify-content-center talent-mapping-load">
                <div class="col-md-6">
                    <h4 class="align-middle">Talent Mapping</h4>
                </div>
                <div class="col-md-6">
                    <select id="talentMappingFilter" class="form-control subholding-select">
                        <option value="">All Data</option>
                        @foreach ($subholding as $data)
                            <option value="{{ $data->code_subholding }}">{{ $data->name_subholding }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="d-none d-md-block" style="margin-top:5%; width: 100%"></div>
                <div class="col-auto text-center">
                    <div class="px-4">
                        <table class="mt-5" border="2" bordercolor="white" height="100%" id="mapping-talent">
                            <tbody>
                                @foreach ($boxes->chunk(3) as $chunk)
                                    <tr>
                                        @foreach ($chunk as $box)
                                            <td width="125" height="185"
                                                style="border-radius: 7px 7px 7px 7px; background-color:{{ $box->color }} !important"
                                                cellpadding="7">
                                                <div class="text-dark">
                                                    <div align="center">
                                                        <strong style="line-height:1px!important">
                                                            {{ $box->box }}. {{ ucwords($box->cat_name) }}
                                                        </strong>
                                                    </div>
                                                    <div>&nbsp;&nbsp;</div>
                                                    <div class="panel" id="panel-talent-{{ $box->box }}"
                                                        data-panel-talent="{{ $box->box }}" align="center">
                                                        <h5 class="text-dark">0%</h5>
                                                        <h6 class="text-dark">0</h6>
                                                    </div>
                                                    <div>&nbsp;&nbsp;</div>
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="d-none d-sm-block d-md-none" style="margin-top:5%; width: 100%"></div>
            </div>
        </div>
    </div>
</div>

<div class="col-xl-6 col-md-12 col-sm-12">
    <div class="card card-custom card-stretch gutter-b">
        <div class="card-body">
            <h4 class="card-title">Pemenuhan SDM</h4>
            <div id="chartTalentDevelopment_p3"></div>
        </div>
    </div>
</div>

<div class="col-xl-7 col-md-12 col-sm-12" hidden>
    <div class="card card-custom card-stretch gutter-b">
        <div class="card-header row" style="border-bottom: none !important;">
            <div class="col-md-5 card-title">
                <h3 class="card-label">
                    Talent Mapping Chart
                </h3>
            </div>
        </div>
        <div class="card-body">
            <div class="talent-mapping-chart-load">
                <div id="talent-mapping-chart2"></div>
            </div>
        </div>
    </div>
</div>
