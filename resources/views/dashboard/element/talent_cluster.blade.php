<div class="col-xl-4 col-md-12 col-sm-12">
    {{-- TALENT CLUSTER --}}
    <div class="card card-custom card-stretch gutter-b">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <h4 class="align-middle">Talent Cluster</h4>
                </div>
                <table class="mt-5" border="2" bordercolor="white" height="100%" id="mapping-talent">
                    <tbody>
                        @foreach ($boxes->chunk(3) as $chunk)
                            <tr>
                                @foreach ($chunk as $box)
                                    <td width="125" height="120"
                                        style="border-radius: 7px 7px 7px 7px; background-color:{{ $box->color }} !important"
                                        cellpadding="7">
                                        <div class="text-dark">
                                            <div align="center">
                                                <small style="line-height:1px!important">
                                                    {{ $box->box }}. {{ ucwords($box->cat_name) }}
                                                </small>
                                            </div>
                                            <div>&nbsp;&nbsp;</div>
                                            <div class="panel" id="panel-talent-{{ $box->box }}"
                                                data-panel-talent="{{ $box->box }}" align="center">
                                                <h5 class="text-dark">@if($box->box == $cluster) <i class="fa fa-check text-dark"></i> @endif</h5>
                                            </div>
                                            <div>&nbsp;&nbsp;</div>
                                        </div>
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>