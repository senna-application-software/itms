
<h1>Element EQS</h1>
<table class="table">            
    <tr>
        <th>Aspiration (20%)</th>
        <td>:</td>
        <td>{{ $data[0] && isset($data[0]['aspiration']) ? $data[0]['aspiration'] : "" }}</td>
    </tr>             
    <tr>
        <th>Experience (15%)</th>
        <td>:</td>
        <td>{{ $data[0] && isset($data[0]['experience']) ? $data[0]['experience'] : "" }}</td>
    </tr>             
    <tr>
        <th>Job fit (20%)</th>
        <td>:</td>
        <td>{{ $data[0] && isset($data[0]['job_fit']) ? $data[0]['job_fit'] : "" }}</td>
    </tr>             
    <tr>
        <th>Kinerja (20%)</th>
        <td>:</td>
        <td>{{ $data[0] && isset($data[0]['kinerja']) ? $data[0]['kinerja'] : "" }}</td>
    </tr>             
    <tr>
        <th>Penghargaan</th>
        <td>:</td>
        <td>{{ $data[0] && isset($data[0]['penghargaan']) ? $data[0]['penghargaan'] : "" }}</td>
    </tr>             
    <tr>
        <th>Sosial</th>
        <td>:</td>
        <td>{{ $data[0] && isset($data[0]['sosial']) ? $data[0]['sosial'] : "" }}</td>
    </tr>             
    <tr>
        <th>Talent cluster</th>
        <td>:</td>
        <td>{{ $data[0] && isset($data[0]['talent_cluster']) ? $data[0]['talent_cluster'] : "" }}</td>
    </tr>             
    <tr>
        <th>Track Record(5%)</th>
        <td>:</td>
        <td>{{ $data[0] && isset($data[0]['track_record']) ? $data[0]['track_record'] : "" }}</td>
    </tr>             
    <tr>
        <th>Training (10%) sertifikasi (10%)</th>
        <td>:</td>
        <td>{{ $data[0] && isset($data[0]['training_sertifikasi']) ? $data[0]['training_sertifikasi'] : "" }}</td>
    </tr>             
</table>