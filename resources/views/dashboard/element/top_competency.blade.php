<div class="col-xl-12 col-md-12 col-sm-12">
    {{-- TALENT CLUSTER --}}
    <div class="card card-custom card-stretch gutter-b">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-6">
                    <h4 class="align-middle py-8">Top 3 Strength Competencies</h4>
                    <div id="chartCompetenciesMember"></div>
                </div>

                <div class="col-6">                    
                    <h4 class="align-middle py-8">Top 3 Competencies Need To Be Improved</h4>
                    <div id="chartCompetenciesMemberWeak"></div>
                </div>
            </div>
        </div>
    </div>
</div>