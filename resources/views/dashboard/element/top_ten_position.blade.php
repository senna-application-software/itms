<div class="col-xl-12 col-md-12 col-sm-12">
    <div class="card card-custom card-stretch gutter-b">
        <div class="card-body">
            <div class="row">
                <div class="col-md-8">
                    <h4 class="align-middle">Top 10 Desired Positions</h4>
                </div>
                <div class="col-md-4 mb-4">
                    <select name="" id="subholdingDesiredPositions" class="form-control subholding-select"
                        autocomplete="off">
                        <option value="">All Data</option>
                        @foreach ($subholding as $value)
                            <option value="{{ $value->code_subholding }}">{{ $value->name_subholding }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <div id="topPositionsChart"></div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table" id="topPositionsTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Position</th>
                                    <th>Job Grade Alignment</th>
                                    <th>Job Family Alignment</th>
                                    <th>Organization</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
