{{-- Extends layout --}}
@extends('layout.default')

{{-- Content Section --}}
@section('page_toolbar')
    {{-- kalo di subheader mau ditambahkan button action taruh di sini --}}
@endsection

@section('modal')
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('insert_external') }}" id="updPrfFrm" method="POST">
                @csrf
                <input type="hidden" id="directorate" name="directorate">
                <input type="hidden" id="division" name="division">
                <input type="hidden" id="group" name="group">
                <input type="hidden" id="position" name="position">

                <div class="row pt-6">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama Lengkap </label>
                            <input type="text" name="full_name" class="form-control" required>
                            <span class="form-text text-danger">* Sesuai KTP, Menggunakan Huruf
                                Kapital</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">NIK</label>
                            <input type="text" name="nik" class="form-control"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                maxlength="16" required>
                            <span class="form-text text-danger">* Diisi Dengan Nomor KTP, 16 Digit
                                Angka</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">NPWP</label>
                            <input type="text" name="npwp" class="form-control" maxlength="15"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                required>
                            <span class="form-text text-danger">
                                * Diisi Dengan 15 Digit Angka Tanpa Tanda Baca
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama Sub Holding</label>
                            <select name="codeSubholding" class="form-control subholding-select" id="select-subholding"
                                autocomplete="off" required>
                                <option value="-" disabled>- List Data -</option>
                                <option value="" selected>All Data</option>
                                @foreach ($subholding as $value)
                                    <option value="{{ $value->code_subholding }}">{{ $value->name_subholding }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama Direktorat</label>
                            <select name="codeDirectorate" id="select-direktorat" class="form-control subholding-select"
                                autocomplete="off">
                                <option value="-" selected>- List Data -</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama Group</label>
                            <select name="codeGroup" id="select-group" class="form-control subholding-select"
                                autocomplete="off">
                                <option value="-" selected>- List Data -</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama Divisi</label>
                            <select name="codeDivision" id="select-divisi" class="form-control subholding-select"
                                autocomplete="off">
                                <option value="-" selected>- List Data -</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama Posisi</label>
                            <select name="codePosition" id="select-position" class="form-control subholding-select"
                                autocomplete="off" required>
                                <option value="-" selected>- List Data -</option>
                            </select>
                            <span class="form-text text-danger">
                                * Wajib
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap-editable/css/bootstrap-editable.css"
        integrity="sha512-e0rbO6UJET0zDdXOHjwc6D44UpeKumn7cU7XR/fa4S0/Jso0bZqcCqlIF6mtvcimMbf846mkv8aSWFnTwABr/g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

{{-- Scripts Section --}}
@section('scripts')
    @if (session('status') == 'success')
        <script>
            $(document).ready(function() {
                Swal.fire({
                    title: "{{ session('title') }}",
                    text: "{{ session('message') }}, {{ session('confirm') }}",
                    icon: "{{ session('status') }}",
                    showCancelButton: false,
                    showDenyButton: true,
                    confirmButtonText: 'Ya !',
                    denyButtonText: 'Tidak !',
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = "{{ route('talent-sourcing.profile', session('nik')) }}";
                    } else if (result.isDenied) {
                        window.location.href = "{{ route('masterDataEmployee.index') }}";
                    }
                });
            });
        </script>
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js">
    </script>
    <script src="{{ asset('js/pages/features/miscellaneous/sticky-panels.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.subholding-select').select2();
            selectData("directorat", $("#select-direktorat"));
        });

        $("#select-subholding").change(function() {
            selectData("directorat", $("#select-direktorat"), $(this).val());
        })

        $("#select-direktorat").change(function() {
            selectData("group", $("#select-group"), $("#select-subholding").val());
        })

        $("#select-group").change(function() {
            selectData("divisi", $("#select-divisi"), $("#select-subholding").val());
        })

        $("#select-divisi").change(function() {
            selectData("position", $("#select-position"), [$("#select-subholding").val(), $("#select-direktorat")
                .val(), $("#select-group").val(), $(this).val()
            ]);
        })

        function selectData(load, selector, selected = false) {
            KTApp.block('#updPrfFrm', {
                overlayColor: '#000000',
                state: 'primary',
                message: 'Processing...'
            });
            if (selected != "-" || load == "subholding") {
                $.ajax({
                    method: "POST",
                    url: "{{ route('filter_positions') }}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        selected: selected,
                        load: load,
                    },
                    success: function(res) {
                        html = `<option value="-" disabled selected>- List Data -</option>`;
                        if (load != "position") {
                            html += `<option value="" selected>All Data</option>`;
                        }
                        $.each(res, function(key, value) {
                            html += `<option value="${(value.code)}">${(value.name)}</option>`;
                        });
                        selector.html("")
                        selector.html(html);
                        // if (res.length > 0) {
                        if (res.length > 0 && load == "position") {
                            selector.val(res[0].code).trigger("change");
                        } else {
                            selector.val("").trigger("change");
                        }
                    }
                })
            } else {
                selector.html(`<option value="-">- List Data -</option>`)
                selector.val("-").trigger("change");
            }
            KTApp.unblock("#updPrfFrm");
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/employee-external/employeeExternal.js?nocache=' . time()) }}">
    </script>
@endsection
