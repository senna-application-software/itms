{{-- Notif pending task di hide karena permintaan users :D --}}
{{-- <div class="row">
    @if (count($pending_task) > 0)
        <div class="col-md-12 mt-5">
            @foreach ($pending_task as $item)
                @if ($item->jenis_notifikasi == 'employee')
                    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                        <div class="alert-text"> <strong>{{ $item->judul }}</strong> <br>
                            <p>{!! $item->deskripsi !!}
                            </p>
                        </div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="ki ki-close"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    @endif
</div> --}}
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom gutter-b">
            <div class="card-header border-0 py-5 d-block text-center">
                <h2 class="card-title flex-column">
                    <h2 class="flex-column">
                        <span class="font-weight-bolder text-dark">Hai
                            {{ Sentinel::getUser()->employee->personnel_number }}, Selamat Datang
                            Di <b class="text-danger">ITMS</b></span>
                    </h2>
                </h2>
            </div>
        </div>
    </div>
    <!-- Personal Information -->
    <div class="col-md-12">
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-md-2">
                            <img class="img-thumbnail" src="{{ get_employee_pict(Sentinel::getUser()->nik) }}">
                        </div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-12 mb-5">
                                    <h3>{{ Sentinel::getUser()->employee->personnel_number }}</h3>
                                </div>
                                <div class="col-md-6">
                                    <table class="table">
                                        <tr>
                                            <td>Organization</td>
                                            <td>:</td>
                                            <td>{{ Sentinel::getUser()->employee->subholding->name_subholding ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Directorate</td>
                                            <td>:</td>
                                            <td>{{ Sentinel::getUser()->employee->positions->directorate->name_directorate ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Group</td>
                                            <td>:</td>
                                            <td>{{ Sentinel::getUser()->employee->positions->group->name_group ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Division</td>
                                            <td>:</td>
                                            <td>{{ Sentinel::getUser()->employee->positions->division->name_division ?? '-' }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table">
                                        <tr>
                                            <td>Area Nomenklatur</td>
                                            <td>:</td>
                                            <td>{{ Sentinel::getUser()->employee->positions->master_area->name_area ?? "-" }}</td>
                                        </tr>
                                        <tr>
                                            <td>Position</td>
                                            <td>:</td>
                                            <td>{{ Sentinel::getUser()->employee->positions->name_position ?? "-"}}</td>
                                        </tr>
                                        <tr>
                                            <td>Grade Alignment</td>
                                            <td>:</td>
                                            <td>{{ Sentinel::getUser()->employee->person_grade_align ?? "-" }}</td>
                                        </tr>
                                        <tr>
                                            <td>Job Family</td>
                                            <td>:</td>
                                            <td>
                                                {!! Sentinel::getUser()->employee->positions ? Sentinel::getUser()->employee->positions->job_family_align() : "-" !!}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                {{-- <div class="col-md-12">
                                    <h4 class="text-muted">EQS : {{ $eqs_employee }}</h4>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.element.talent_cluster')
    @if ($aspiration)
        <div class="col-xl-8 col-md-12 col-sm-12">
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 py-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Aspirasi Posisi Anda</span>
                        <span class="text-muted mt-3 font-weight-bold font-size-sm">Aspirasi Posisi Horizontal dan Vertical
                            yang Anda pilih</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-0 pb-3">
                    <div class="tab-content">
                        <!--begin::Table-->
                        <div class="table-responsive">
                            <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                                <thead>
                                    <tr class="text-left text-uppercase">
                                        <th style="min-width: 200px" class="pl-7">
                                            <span class="text-dark-75">Nama Posisi</span>
                                        </th>
                                        <th style="min-width: 30px">Tipe Aspirasi</th>
                                        <th style="min-width: 30px">Sumber Aspirasi</th>
                                        <th style="min-width: 10px">Peringkat</th>
                                        <th style="min-width: 20px">Total Employee Aspiration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($aspiration as $row)
                                        <tr>
                                            <td class="pl-0 py-2">
                                                <div class="d-flex align-items-center">
                                                    <div class="symbol symbol-50 symbol-light mr-4">
                                                        <h1 class="symbol-label font-weight-bolder">
                                                            {{ substr($row['name_position'], 0, 1) }}
                                                        </h1>
                                                    </div>
                                                    <div>
                                                        <a href="#"
                                                            class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg open-career-card-by-position" 
                                                            data-nik="{{ $row['nik'] }}" 
                                                            data-code_position="{{ $row['code_position'] }}" 
                                                            data-name_position="{{ $row['name_position'] }}">{{ $row['name_position'] }}</a>
                                                        <span
                                                            class="text-muted font-weight-bold d-block">{{ $row['name_subholding'] }}
                                                            - {{ $row['name_area'] }}
                                                        </span>
                                                        <span class="text-muted font-weight-bold d-block">EQS:
                                                            {{ $row['nilai_eqs'] }}
                                                        </span>
                                                        <span class="text-muted font-weight-bold d-block">Grade Align:
                                                            {{ $row['grade_align'] }}
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <span
                                                    class="text-muted d-block font-size-lg">{{ $row['aspiration_level'] }}
                                                </span>
                                            </td>
                                            <td>
                                                <span
                                                    class="text-muted d-block font-size-lg">{{ $row['aspiration_type'] }}
                                                </span>
                                            </td>
                                            <td class="text-center">
                                                <span
                                                    class="text-dark-75 font-weight-bolder d-block font-size-lg">{{ $row['my_ranking'] }}
                                                </span>
                                            </td>
                                            <td class="text-center">
                                                <span
                                                    class="text-dark-75 font-weight-bolder d-block font-size-lg">{{ $row['total'] }}
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                </div>
                <!--end::Body-->
            </div>
        </div>
    @endif
<div class="{{ ($aspiration) ? 'col-md-12' : 'col-md-8' }}">
    <div class="card">
        <div class="card-body">
            <h4>Historical Aspiration</h4>
            <table class="table table-stipped" id="table-history">
                <thead>
                    <tr>
                        <th>Nama Event</th>
                        <th>Posisi</th>
                        <th>Perusahaan</th>
                        <th>Tipe Vacant</th>
                        <th>Periode Aspirasi</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($talent as $item)
                        <tr>
                            <td>{{ $item->name_event ? $item->name_event : '-' }}</td>
                            <td>{{ $item->name_position }}</td>
                            <td>{{ $item->name_subholding }}</td>
                            <td>{{ $item->vacant_type ? $item->vacant_type : '-' }}</td>
                            <td>{{ date('d/m/Y', strtotime($item->start_date)) }} -
                                {{ date('d/m/Y', strtotime($item->end_date)) }}</td>
                            <td>
                                <span class="badge badge-primary">{{ $item->proses }}</span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

<!-- Employee Score Card -->
<div class="modal fade" id="xl-modal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Informasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body data-modal" data-scroll="true" data-height="500">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>