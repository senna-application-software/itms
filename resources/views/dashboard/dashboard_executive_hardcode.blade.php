<div class="container-fluid">
    <div class="row">
        {{-- Employees Demography --}}
        <div class="col-xl-12 col-md-12 col-sm-12" id="containerEmployeeDemograph">
            <div class="card card-custom card-stretch gutter-b">
                <div class="card-body d-flex flex-column">
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="align-middle">Employees Demography</h4>
                        </div>
                        <div class="col-md-4 mb-4">
                            <select name="" id="subholdingEmployeeGraph" class="form-control subholding-select"
                                autocomplete="off">
                                <option value="">All Data</option>
                                @foreach ($subholding as $value)
                                    <option value="{{ $value->code_subholding }}">{{ $value->name_subholding }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <center><img src="{{ asset('images/images.png') }}" alt=""></center>
                            <center>
                                <table style="width: 80%;text-align: center">
                                    <tr>
                                        <td class="text-primary">
                                            <h3 id="persentaseMale"></h3>
                                        </td>
                                        <td class="text-danger">
                                            <h3 id="persentaseFemale"></h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-primary" id="totalMale"></td>
                                        <td class="text-danger" id="totalFemale"></td>
                                    </tr>
                                </table>
                            </center>
                        </div>
                        <div class="col-md-8">
                            <div id="genChart"></div>
                        </div>
                    </div>

                    {{-- Pendidikan Level --}}
                    <div class="row" id="containerJenjangPendidikan">
                    </div>
                </div>
            </div>
        </div>

        {{-- Talent Mapping dan Pemenuhan SDM --}}
        <div class="col-xl-12 col-md-12 col-sm-12">
            <div class="row">
                @include('dashboard.element.talent_mapping')
            </div>
        </div>

        {{-- Top 10 Position --}}
        @include('dashboard.element.top_ten_position')

        @include('dashboard.element.top_competency')

        <div class="col-xl-12 col-md-12 col-sm-12" id="containerChartOrg">
            <div class="card card-custom card-stretch gutter-b" style="height:max-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-8">
                            <h3 class="col-4">Detail Organisasi</h3>
                        </div>
                        <div class="col-4">
                            <div class="form-group float-right">
                                <select name="" id="subholdingOrganization" class="form-control subholding-selectmb-4"
                                    autocomplete="off" style="width: 100% !important">
                                    <option value="" disabled>- List Subholding -</option>
                                    @foreach ($subholding as $value)
                                        <option value="{{ $value->code_subholding }}">{{ $value->name_subholding }}
                                        </option>
                                    @endforeach
                                </select>
                                <select name="" id="directorateOrganization" class="form-control subholding-select mt-4"
                                    autocomplete="off" style="width: 100% !important">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="inJourneyDiagram" class="diagram-entry"></div>
                </div>
            </div>
        </div>

        {{-- Monitoring Human Capital KPI --}}
        <div class="col-xl-12 col-md-12 col-sm-12">
            <h4>Monitoring Human Capital KPI</h4>
            <div class="row">
                @foreach ($subholding as $item)
                    @if ($item->code_subholding == 'inj')
                        <div class="col-xl-4 col-md-6 col-sm-12 container-member offset-xl-4">
                            <div class="card card-custom card-stretch gutter-b">
                                <div class="d-inline-block text-left p-4" style="height:50px">
                                    <div class="content-img" style="height:50px">
                                        <img class="p-8"
                                            src="{{ asset('images/logo-member/' . $item->logo) }}" style="width:40%">
                                    </div>
                                </div>
                                <div class="card-body d-flex flex-column">
                                    <div class="d-flex flex-column w-100 me-2">
                                        <div class="d-flex justify-content-between mt-2 mb-2">
                                            <span class=" me-2 fs-7 fw-bold">Perempuan Nominated</span>
                                            <div class="text-right">
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #D0F7F2;width: auto !important; color: #7DBAB3">
                                                    {{ count_curr_nom_percent($item->nominated_female, $item->total_nominated) }}%
                                                    of
                                                    {{ $item->persen_perempuan }}%
                                                </span>
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #E4F0FC;width: auto !important; color: #86A3C1">
                                                    {{ $item->nominated_female }}
                                                    of 
                                                    {{ count_lads_total($item->total_nominated, $item->persen_perempuan) }}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="progress h-6px w-100">
                                            <div class="progress-bar bg-warning" role="progressbar"
                                                style="width: {{ count_bar_percent(count_curr_nom_percent($item->nominated_female, $item->total_nominated),$item->persen_perempuan) }}%"
                                                aria-valuenow="{{ $item->nominated_female }}" aria-valuemin="0"
                                                aria-valuemax="{{ count_lads_total($item->total_nominated, $item->persen_perempuan) }}">
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2 mb-2">
                                            <span class=" me-2 fs-7 fw-bold">Milenial Nominated</span>
                                            <div class="text-right">
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #D0F7F2;width: auto !important; color: #7DBAB3">
                                                    {{ count_curr_nom_percent($item->nominated_milenial, $item->total_nominated) }}%
                                                    of
                                                    {{ $item->persen_milenial }}%
                                                </span>
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #E4F0FC;width: auto !important; color: #86A3C1">
                                                    {{ $item->nominated_milenial }}
                                                    of
                                                    {{ count_lads_total($item->total_nominated, $item->persen_milenial) }}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="progress h-6px w-100">
                                            <div class="progress-bar bg-danger" role="progressbar"
                                                style="width: {{ count_bar_percent(count_curr_nom_percent($item->nominated_milenial, $item->total_nominated),$item->persen_milenial) }}%"
                                                aria-valuenow="{{ $item->nominated_milenial }}" aria-valuemin="0"
                                                aria-valuemax="{{ count_lads_total($item->total_nominated, $item->persen_milenial) }}">
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2 mb-2">
                                            <span class=" me-2 fs-7 fw-bold">ITMS</span>
                                            <div class="text-right">
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #D0F7F2;width: auto !important; color: #7DBAB3">
                                                    {{ count_curr_nom_percent($item->terisi, $item->vacant) }}%
                                                    of
                                                    {{ $item->persen_vacant }}%
                                                </span>
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #E4F0FC;width: auto !important; color: #86A3C1">
                                                    {{ $item->terisi }}
                                                    of
                                                    {{ count_lads_total($item->vacant, $item->persen_vacant) }}
                                                </span>
                                            </div>
                                            {{-- <span
                                                class="text-muted">{{ floor(($item->vacant / $item->terisi) * 100) }}%({{ $item->vacant }}
                                                of {{ $item->terisi }})</span> --}}
                                        </div>
                                        <div class="progress h-6px w-100">
                                            <div class="progress-bar bg-primary" role="progressbar"
                                                style="width: {{ count_bar_percent(count_curr_nom_percent($item->terisi, $item->vacant), $item->persen_vacant) }}%"
                                                aria-valuenow="{{ $item->terisi }}" aria-valuemin="0"
                                                aria-valuemax="{{ count_lads_total($item->vacant, $item->persen_vacant) }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="row">
                @foreach ($subholding as $item)
                    @if ($item->code_subholding != 'inj')
                        <div class="col-xl-4 col-md-6 col-sm-12 container-member">
                            <div class="card card-custom card-stretch gutter-b">
                                <div class="content-img" style="height:50px">
                                    <img class="p-8"
                                        src="{{ asset('images/logo-member/' . $item->logo) }}" style="width:40%">
                                </div>
                                <div class="card-body d-flex flex-column">
                                    <div class="d-flex flex-column w-100 me-2">
                                        <div class="d-flex justify-content-between mt-2 mb-2">
                                            <span class=" me-2 fs-7 fw-bold">Perempuan Nominated</span>
                                            <div class="text-right">
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #D0F7F2;width: auto !important; color: #7DBAB3">
                                                    {{ count_curr_nom_percent($item->nominated_female, $item->total_nominated) }}%
                                                    of
                                                    {{ $item->persen_perempuan }}%
                                                </span>
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #E4F0FC;width: auto !important; color: #86A3C1">
                                                    {{ $item->nominated_female }}
                                                    of
                                                    {{ count_lads_total($item->total_nominated, $item->persen_perempuan) }}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="progress h-6px w-100">
                                            <div class="progress-bar bg-warning" role="progressbar"
                                                style="width: {{ count_bar_percent(count_curr_nom_percent($item->nominated_female, $item->total_nominated),$item->persen_perempuan) }}%"
                                                aria-valuenow="{{ $item->nominated_female }}" aria-valuemin="0"
                                                aria-valuemax="{{ count_lads_total($item->total_nominated, $item->persen_perempuan) }}">
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2 mb-2">
                                            <span class=" me-2 fs-7 fw-bold">Milenial Nominated</span>
                                            <div class="text-right">
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #D0F7F2;width: auto !important; color: #7DBAB3">
                                                    {{ count_curr_nom_percent($item->nominated_milenial, $item->total_nominated) }}%
                                                    of
                                                    {{ $item->persen_milenial }}%
                                                </span>
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #E4F0FC;width: auto !important; color: #86A3C1">
                                                    {{ $item->nominated_milenial }}
                                                    of
                                                    {{ count_lads_total($item->total_nominated, $item->persen_milenial) }}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="progress h-6px w-100">
                                            <div class="progress-bar bg-danger" role="progressbar"
                                                style="width: {{ count_bar_percent(count_curr_nom_percent($item->nominated_milenial, $item->total_nominated),$item->persen_milenial) }}%"
                                                aria-valuenow="{{ $item->nominated_milenial }}" aria-valuemin="0"
                                                aria-valuemax="{{ count_lads_total($item->total_nominated, $item->persen_milenial) }}">
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2 mb-2">
                                            <span class=" me-2 fs-7 fw-bold">ITMS</span>
                                            <div class="text-right">
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #D0F7F2;width: auto !important; color: #7DBAB3">
                                                    {{ count_curr_nom_percent($item->terisi, $item->vacant) }}%
                                                    of
                                                    {{ $item->persen_vacant }}%
                                                </span>
                                                <span class="label label-md label-rounded px-2"
                                                    style="background-color: #E4F0FC;width: auto !important; color: #86A3C1">
                                                    {{ $item->terisi }}
                                                    of
                                                    {{ count_lads_total($item->vacant, $item->persen_vacant) }}
                                                </span>
                                            </div>
                                            {{-- <span
                                                class="text-muted">{{ floor(($item->vacant / $item->terisi) * 100) }}%({{ $item->vacant }}
                                                of {{ $item->terisi }})</span> --}}
                                        </div>
                                        <div class="progress h-6px w-100">
                                            <div class="progress-bar bg-primary" role="progressbar"
                                                style="width: {{ count_bar_percent(count_curr_nom_percent($item->terisi, $item->vacant), $item->persen_vacant) }}%"
                                                aria-valuenow="{{ $item->terisi }}" aria-valuemin="0"
                                                aria-valuemax="{{ count_lads_total($item->vacant, $item->persen_vacant) }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

        </div>

    </div>
</div>
