{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="row">
    @forelse($sub_event as $data)
    <div class="col-md-4 mb-5">
        <div class="card shadow-sm" style="min-height: 25vh">
            <div class="card-body d-flex flex-column">
                <a href="{{ route('talent-day.detail', $data->id) }}">
                    <h5 class="card-title text-primary mb-2">{{ $data->name_position }}</h5>
                    <span class="text-muted">{{ $data->name_subholding }} - [{{ $data->name_area }}]</span>
                </a>
                <div>
                    <span class="label label-light-warning label-lg label-inline">Talent Day</span>
                </div>

                <div class="mt-3 mb-3">
                    <table class="table">
                        <tr>
                            <th>Sourcing Type</th>
                            <td>
                                @if($data->sourcing_type == 1)
                                <span class="label font-weight-bold label-lg label-light-success label-inline">External</span>
                                @else
                                <span class="label font-weight-bold label-lg label-light-primary label-inline">Internal</span>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="row mt-5 mt-auto">
                    <div class="col-md-5 align-self-center">
                        Total Candidates
                    </div>
                    <div class="col-md-auto text-center">
                        <h1 class="text-primary">{{ $data->participant }} of {{ $data->total }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @empty
        @if($show_engage)
            <div class="col-md-12 mb-5">
                <div class="card card-custom card-stretch gutter-b">
                    <div class="card-body d-flex p-0">
                        <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) top; background-size: 33% auto; background-image: url(media/svg/humans/custom-8.svg)">
                            <h2 class="text-dark pb-5 font-weight-bolder">Silahkan buat Event.</h2>
                            <p class="text-dark-50 pb-5 font-size-h5">Proses Talent Profiling belum dapat dilakukan.
                            <br>Silahkan buat <i>event position</i> terlebih dahulu</p>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-12 mb-5">
                <div class="card card-custom card-stretch gutter-b">
                    <div class="card-body d-flex p-0">
                        <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) top; background-size: 33% auto; background-image: url(media/svg/humans/custom-8.svg)">
                            <h2 class="text-dark pb-5 font-weight-bolder">Talent Day.</h2>
                            <p class="text-dark-50 pb-5 font-size-h5">Tidak ada event <i>Talent Day</i> yang sedang berlangsung.</p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforelse
</div>

@if(Session::has('id_event'))
    <a href="{{ url('talent-day/berita-acara/'. Session::get('id_event')) }}" id="download-ba" hidden>Download BA</a>
@endif

@endsection

{{-- Styles Section --}}
@section('styles')
<link href="{{asset('plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<style>
    @media only screen and (max-width: 600px) {
        .d-flex {
            flex-direction: column;
            flex-flow: wrap;
            justify-content: space-between;
        }
        .d-flex > .btn {
            margin: 4px;
        }
    }
    
    .container-customeHeight {
        height: 200px;
    }

    .label-custome {
        padding: 6px 8px;
        width: 40px;
        height: 40px;
        justify-content: center;
        margin: 0;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        border-radius: 50%;
        background-color: #EBEDF3;
        font-weight: 900!important;
        color: black!important;
        font-size: 3rem!important;
    }
    .content {
        overflow-x: auto;
    }
    .overlay {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
    }
    .blockOverlay {
        background-color: transparent!important
    }
    
    .placeHold{
        border: 1px dashed blue;
        background-color: #c7c7c7;
        height:40px;
        width:228px;
        list-style-type:none;
    }

    .symbol-label-custome {
        width:35px!important;
        height:25px!important;
    }
    
    .facet {
        width:228px;
        height:40px;
        margin-right: 2px;
        display:flex;
        overflow-x:hidden;
        flex:auto;
    }  

    .blockOverlay {
        background-color: transparent!important
    }

    .master-facet {
        list-style-type:none;
        min-width:100%;
        flex-wrap: wrap;
        flex-direction: row;
        overflow-y: auto;
        /* height: 180px; */
        place-content: flex-start;
        /* flex-flow: column; */
    }

    .master-facet-bigger {
        list-style-type:none;
        min-width:100%;
        flex-wrap: wrap;
        flex-direction: row;
        overflow-x: auto;
        overflow-y: auto;
        height: 214px;
        /* flex-flow: column; */
    }

    .xxxx {        
        list-style-type:none;
        overflow: auto;
        flex-flow: wrap;
        flex-direction: row;
        flex-wrap: wrap;
        flex-basis: auto;
        display: flex;
        align-items: flex-start;
        height: 228px;        
    }
</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    if(typeof $("#download-ba") !== "undefined") {
        document.getElementById('download-ba').click();
        $(".page-loader").css("display","none");
    }
</script>
@endsection
