{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="col-md-12 mb-5">
        <div class="card px-5 py-3">
            <div class="d-flex justify-content-between">
                <h4 class="align-self-center m-0 p-0">{{ $position_name }}</h4>
                @if (Session::get('role') == 'talent_committee')
                    <a href="{{ route('talent-day.detail', $sub_event) }}" class="btn btn-primary">Back</a>
                @elseif (Session::get('role') == 'admin')
                    <a href="{{ route('talent-day.detail-assessment') . '?id=' . request('id') }}"
                        class="btn btn-primary">Back</a>
                @endif
            </div>
        </div>
    </div>

    <div class="card-panel col-md-12">
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="card card-custom gutter-b">
                    <div class="card-body pt-4 d-flex flex-column">
                        <div class="my-5 text-right">
                            <div class="symbol symbol-circle symbol-100">
                                <img src="{{ get_employee_pict($data->employee->prev_persno) }}" alt="image">
                            </div>
                        </div>

                        <div class="mt-3 mb-5">
                            <a type="button"
                                class="text-primary font-weight-bold text-hover-warning font-size-h4 mb-0 open-career-card"
                                data-nik="{{ $data->employee->prev_persno }}" style="-webkit-appearance: none;">
                                {{ $data->employee->personnel_number }}
                            </a>
                            <p class="text-muted font-weight-bold">{{ $data->employee->position_name }}</p>
                            <p class="font-weight-bold p-0 m-0">
                                {{ $data->employee->positions->sub_holding->name_subholding ?? '' }}</p>
                            <p class="font-weight-bold p-0 m-0">
                                {{ $data->employee->positions->master_area->name_area ?? '' }}</p>
                            <h3 class="mb-4">EQS:
                                {{ nilai_eqs($data->employee->prev_persno, false, $sub_event) }}</h3>
                        </div>

                        <div class="row mt-auto">
                            @foreach ($data->employee->performance as $performance)
                                <div class="col-md-4 mx-1 rounded p-1 text-center"
                                    style="background-color:#82E99F;min-height: 70px;max-width: 30%">
                                    <span>{{ $performance->year }}</span>
                                    <br>
                                    <span>{{ $performance->perfomance_category_align }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-6 col-md-6 col-sm-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header">
                        <h4 class="card-title">Assessment</h4>
                    </div>
                    <div class="card-body pt-4">
                        <table class="table">
                            <thead style="background-color: #E3F8FF">
                                <tr>
                                    <th class="text-center">No</th>
                                    <th>Pertanyaan</th>
                                    <th>Nilai (0-5)</th>
                                    <th>Evidence</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($assessment as $row)
                                    @php
                                        if (Session::get('role') == 'admin') {
                                            $nik_tcom = request('tcom');
                                        } elseif (Session::get('role') == 'talent_committee') {
                                            $nik_tcom = Sentinel::getUser()->nik;
                                        }
                                        $que = DB::table('master_nilai_assessment')
                                            ->where('id_pertanyaan', $row->id)
                                            ->where('nik', $data->employee->prev_persno)
                                            ->where('sub_event', $sub_event)
                                            ->where('nik_talentcom', $nik_tcom)
                                            ->select('nilai', 'keterangan')
                                            ->first();
                                    @endphp
                                    <tr>
                                        <td>
                                            {{ $loop->iteration }}
                                        </td>
                                        <td>
                                            {{ $row->pertanyaan }}
                                        </td>
                                        <td>
                                            <a
                                                @if (Session::get('role') == 'talent_committee') href="javascript:void(0)" class="assessment-editable"
                                                data-pk="{{ $row->id }}"
                                                data-nik="{{ $data->employee->prev_persno }}"
                                                data-sub="{{ $sub_event }}" data-value="{{ isset($que) ? $que->nilai : null }}" @endif>
                                                {{ isset($que) ? $que->nilai : null }}
                                            </a>
                                        </td>
                                        <td>
                                            @if (Session::get('role') == 'talent_committee')
                                                <a href="javascript:void(0)" class="assessment-ket-editable"
                                                    data-pk="{{ $row->id }}"
                                                    data-nik="{{ $data->employee->prev_persno }}"
                                                    data-sub="{{ $sub_event }}">
                                                    {{ isset($que) ? $que->keterangan : '' }}
                                                </a>
                                            @else
                                                <a>
                                                    {{ $que->keterangan ?? '-' }}
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th></th>
                                    <th>Keterangan</th>
                                    <th id="total_nilai">{{ $total_nilai }}</th>
                                    <th id="ket_nilai">{{ $ket_nilai }}</th>
                                </tr>
                                @if (Session::get('role') == 'talent_committee')
                                    <tr class="text-right">
                                        <td colspan="3">
                                            <button type="button" class="btn btn-secondary" id="drftBtn">Save As
                                                Draft</button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-primary" id="saveBtn">Save</button>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3 employee-panel" style="display:none;">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <h4 class="card-title">Employee List</h4>
                <div class="card-toolbar">
                    <a href="javascript:void(0)" class="filter-modal-tampilkan" data-toggle="modal"
                        data-target="#filterModal">
                        <i class="fa fa-filter"></i>
                    </a>
                </div>
            </div>
            <div class="card-body pt-1 ">
                <input id="search-employee" type="text" name="" class="form-control form-control-sm"
                    placeholder="Search Employee...">
                <br>
                <section class="list-employee" style="height:55vh; overflow:auto;">
                    <ul id="myUl" data-panel="0" class="pl-1 master-facet 0"></ul>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade" id="xl-modal" tabindex="-1" role="dialog" aria-labelledby="xl-modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Employee Career Card</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body data-modal" data-scroll="true" data-height="500">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
        rel="stylesheet" />
    <style>
        @media only screen and (max-width: 600px) {
            .d-flex {
                flex-direction: column;
                flex-flow: wrap;
                justify-content: space-between;
            }

            .d-flex>.btn {
                margin: 4px;
            }
        }

    </style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/pages/crud/forms/widgets/select2.js') }}"></script>
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js">
    </script>
    @include('global.element.script_eqs_peremployee')
    <script>
        $.fn.editable.defaults.mode = 'inline';
        $.fn.editable.defaults.onblur = "submit";
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content"),
                },
            });

            initEditable();
        });

        function initEditable() {
            $('.assessment-editable').editable({
                type: 'select',
                pk: $(this).data('pk'),
                url: "{{ route('talent-day.detail-assessment.update') }}",
                showbuttons: false,
                source: [{
                        value: 1,
                        text: '1'
                    },
                    {
                        value: 2,
                        text: '2'
                    },
                    {
                        value: 3,
                        text: '3'
                    },
                    {
                        value: 4,
                        text: '4'
                    },
                    {
                        value: 5,
                        text: '5'
                    }
                ],
                params: function(params) {
                    params.sct = "nil";
                    params.nik = $(this).data('nik');
                    params.sub = $(this).data('sub');
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }
                    totalKet();
                }
            });

            $('.assessment-ket-editable').editable({
                type: 'text',
                pk: $(this).data('pk'),
                url: "{{ route('talent-day.detail-assessment.update') }}",
                showbuttons: false,
                params: function(params) {
                    params.sct = "ket";
                    params.nik = $(this).data('nik');
                    params.sub = $(this).data('sub');
                    return params;
                },
                display: function(value, res) {
                    if (value == '') {
                        $(this).addClass('btn btn-danger')
                    } else {
                        $(this).text(value);
                    }
                },
                success: function(response) {
                    if (response == '') {
                        $(this).text('Empty')
                        $(this).css("background-color", "#F64E60")
                        $(this).addClass('btn btn-danger')
                        $(this).addClass('editable-empty')
                    } else {
                        $(this).text(response)
                        $(this).css("background-color", "rgba(0, 0,0,0)")
                        $(this).removeClass('btn btn-danger')
                    }

                }
            });
            $('.assessment-editable.editable.editable-click.editable-empty').addClass('btn btn-danger');
        }

        function totalKet() {
            let total = 0,
                question = $("a.assessment-editable").length,
                ket = 'Sangat Kurang';
            $.each($("a.assessment-editable"), function() {
                if ($(this).text() != '' && $(this).text() != "Empty") {
                    total += Number($(this).text());
                }
            });
            if (total) {
                total = total / question;
                if (total >= 5) {
                    ket = "Sangat Baik";
                } else if (total >= 4) {
                    ket = "Baik";
                } else if (total >= 3) {
                    ket = "Cukup";
                } else if (total >= 2) {
                    ket = "Kurang";
                } else if (total <= 1) {
                    ket = "Sangat Kurang";
                }
            }

            $("#total_nilai").html(total);
            $("#ket_nilai").html(ket);
        }

        $("#drftBtn").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('talent-day.detail_assessment.save') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    sct: "drft",
                    nk: "{{ $data->employee->prev_persno }}",
                    sub: "{{ $sub_event }}"
                },
                success: function(response) {
                    let title = "Failed",
                        message = "Cannot Save Data",
                        status = "failed";
                    if (response) {
                        title = "Success";
                        message = "Save Data Success";
                        status = "success";
                    }
                    Swal.fire({
                        title: title,
                        text: message,
                        icon: status,
                        buttonsStyling: false,
                        confirmButtonText: "close",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    }, function() {
                        window.location.reload();
                    });
                    setTimeout(function() {
                        window.location.reload();
                    }, 3000);
                }
            });
        });

        $("#saveBtn").click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('talent-day.detail_assessment.save') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    sct: "save",
                    nk: "{{ $data->employee->prev_persno }}",
                    sub: "{{ $sub_event }}"
                },
                success: function(response) {
                    let title = "Failed",
                        message = "Cannot Save Data",
                        status = "failed";
                    if (response) {
                        title = "Success";
                        message = "Save Data Success";
                        status = "success";
                    }
                    Swal.fire({
                        title: title,
                        text: message,
                        icon: status,
                        buttonsStyling: false,
                        confirmButtonText: "close",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    }, function() {
                        window.location.reload();
                    });
                    setTimeout(function() {
                        window.location.reload();
                    }, 3000);
                }
            });
        });
    </script>
@endsection
