{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="col-12 mb-5">
        <div class="card px-5 py-3">
            <div class="d-flex justify-content-between">
                <h4 class="align-self-center m-0 p-0">
                    {{ $sub_event->name_position }}
                    <span
                        class="label font-weight-bold label-lg {{ $sub_event->sourcing_type == 1 ? 'label-light-success' : 'label-light-primary' }} label-inline">
                        {{ $sub_event->sourcing_type == 1 ? 'External' : 'Internal' }}
                    </span>
                    <small class="text-muted">{{ $sub_event->name_subholding }} -
                        [{{ $sub_event->name_area }}]</small>
                </h4>
                @if (Session::get('role') == 'admin')
                    <div>
                     <button class="btn btn-success" data-select-all-click="boards">Pilih Semua</button>
                    <button class="btn btn-primary" id="done_assessment" type="button">Done Assessment</button>
                    </div>
                @else
                    <a href="{{ route('talent-day.index') }}" class="btn btn-primary">Back</a>
                @endif
            </div>
        </div>
    </div>


    <div class="col-sm-12">
        <form action="{{ route('talent-day.done-assessment') }}" method="POST" id="form_assessment">
            @csrf
            <div class="row">
                <input type="hidden" name="sub_event" value="{{ $sub_event->id }}">

                @foreach ($data as $item)
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                        <div class="card card-custom gutter-b card-stretch">
                            <div class="card-body pt-4 d-flex flex-column">
                                <div class="d-flex justify-content-between my-5" data-select-all="boards">
                                    @if (is_completed_assessment($item->id))
                                        <label class="form-check form-check-custom form-check-solid align-self-start">
                                            @if (Session::get('role') == 'admin')
                                                <input class="form-check-input h-25px w-25px" name="talent_id[]"
                                                    type="checkbox" data-name="{{ $item->employee->personnel_number }}"
                                                    value="{{ $item->id }}" />
                                            @endif
                                        </label>
                                    @else
                                        <label
                                            class="form-check form-check-custom form-check-solid align-self-start tooltip-wrapper">
                                            @if (Session::get('role') == 'admin')
                                                <input class="form-check-input h-25px w-25px" type="checkbox"
                                                    onclick="swalfire('Warning', 'Assessment Belum Selesai !', 'warning');$(this).prop('checked', false)" />
                                                <span
                                                    style=" position: absolute;
                                                                                                position: absolute;
                                                                                                top: 4px;
                                                                                                left: 0;
                                                                                                height: 25px;
                                                                                                width: 25px;
                                                                                                background-color: #f23f44a3;
                                                                                                border-radius: 4px;"></span>
                                                <i class="fas fa-exclamation-triangle text-danger ml-5 mt-3"></i>
                                            @endif
                                        </label>
                                    @endif

                                    <div class="symbol symbol-circle symbol-100">
                                        <img src="{{ get_employee_pict($item->employee->prev_persno) }}" alt="image">
                                    </div>
                                </div>

                                <div class="mt-3 mb-5">
                                    <a href="{{ route('talent-day.detail-assessment') . '?id=' . $item->id }}"
                                        class="text-primary font-weight-bold text-hover-warning font-size-h4 mb-0 open-career-card">
                                        {{ $item->employee->personnel_number }}</a>
                                    <p class="text-muted font-weight-bold">
                                        {{ $item->employee->master_posisi->name_position }}</p>
                                    <p class="font-weight-bold p-0 m-0">
                                        {{ $item->employee->positions->sub_holding->name_subholding ?? '' }}</p>
                                    <p class="font-weight-bold p-0 m-0">
                                        {{ $item->employee->positions->master_area->name_area ?? '' }}</p>
                                    @if ($item->comitte_proposal)
                                        <div class="form-check mt-3 mb-3">
                                            <input class="form-check-input" type="checkbox" disabled checked>
                                            <label class="form-check-label">
                                                Committee Proposal
                                            </label>
                                        </div>
                                    @endif
                                    <h3 class="mb-4">EQS:
                                        {{ nilai_eqs($item->employee->prev_persno, false, $sub_event->id) }}</h3>
                                    <span class="badge badge-primary w-100 mb-2">
                                        Nilai Akhir: {{ final_mark_assessment($item->id) }}
                                    </span>
                                    {!! assessment_label(final_mark_assessment($item->id), true) !!}
                                </div>

                                <div class="row mt-auto">
                                    @foreach ($item->employee->performance as $performance)
                                        <div class="col-md-4 mx-1 rounded p-1 text-center"
                                            style="background-color:#82E99F;min-height: 70px;max-width: 30%">
                                            <span>{{ $performance->year }}</span>
                                            <br>
                                            <span>{{ $performance->perfomance_category_align }}</span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </form>
    </div>

    <div class="modal fade" id="modal_assessment" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirm Talent Day Results</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="" method=" ">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr id="assessment_head">
                                </tr>
                            </thead>
                            <tbody id="assessment_body">

                            </tbody>
                        </table>
                        <input type="checkbox" checked required> <label>Kandidat yang terpilih sudah sesuai</label>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" form="form_assessment">Done</button>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <style>
        @media only screen and (max-width: 600px) {
            .d-flex {
                flex-direction: column;
                flex-flow: wrap;
                justify-content: space-between;
            }

            .d-flex>.btn {
                margin: 4px;
            }
        }

    </style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/pages/crud/forms/widgets/select2.js') }}"></script>
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
    // fungsi all checked
    $('[data-select-all-click]').click(function() {
                var selfAllSelector = $(this).data('select-all-click');
                boardInputs = $('[data-select-all='+selfAllSelector+'] input');
                boardInputs.prop('checked', !boardInputs.prop('checked') );
            }); 
        $('#done_assessment').on('click', function() {
            if ($("input[name='talent_id[]']:checked").length) {
                event.preventDefault();
                var listAssessment = $("input:checkbox:checked").map(function() {
                    return $(this).attr("data-name");;
                }).toArray();
                var html = "";
                var html_head = `<th scope="col">Choosen Candidates (` + listAssessment.length + `)</th>`;
                for (let p = 0; p < listAssessment.length; p++) {
                    html += `<tr>
                            <td>` + listAssessment[p] + `</td>
                            </tr>`;
                }
                $("#assessment_head").html(html_head);
                $("#assessment_body").html(html);


                $("#modal_assessment").modal();

            } else {
                Swal.fire("Please Select Talent Atleast 1 !", "", "error");
            }
        });

        function swalfire(title, text, icon) {
            Swal.fire({
                title: title,
                text: text,
                icon: icon,
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        }
    </script>
@endsection
