{{-- Extends layout --}}
@extends('layout.default')

{{-- @php
    dd($assessor[0])
@endphp --}}

{{-- Content --}}
@section('content')
    <div class="col-md-12 mb-5">
        <div class="card px-5 py-3">
            <div class="d-flex justify-content-between">
                <h4 class="align-self-center m-0 p-0">{{ $assessor[0]['position'] }}</h4>
                <a href="{{ route('talent-day.detail', $sub_event) }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>

    <div class="card-panel col-md-12">
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="card card-custom gutter-b">
                    <div class="card-body pt-4 d-flex flex-column">
                        <div class="my-5 text-right">
                            <div class="symbol symbol-circle symbol-100">
                                <img src="{{ get_employee_pict($data->employee->prev_persno) }}" alt="image">
                            </div>
                        </div>

                        <div class="mt-3 mb-5">
                            <a type="button"
                                class="text-primary font-weight-bold text-hover-warning font-size-h4 mb-0 open-career-card"
                                data-nik="{{ $data->employee->prev_persno }}">
                                {{ $data->employee->personnel_number }}</a>
                            <p class="text-muted font-weight-bold">{{ $data->employee->position_name }}</p>
                            <p class="font-weight-bold p-0 m-0">
                                {{ $data->employee->positions->sub_holding->name_subholding ?? '' }}</p>
                            <p class="font-weight-bold p-0 m-0">
                                {{ $data->employee->positions->master_area->name_area ?? '' }}</p>
                            <h3 class="mb-4">EQS:
                                {{ nilai_eqs($data->employee->prev_persno, false, $sub_event) }}</h3>
                        </div>

                        <div class="row mt-auto">
                            @foreach ($data->employee->performance as $performance)
                                <div class="col-md-4 mx-1 rounded p-1 text-center"
                                    style="background-color:#82E99F;min-height: 70px;max-width: 30%">
                                    <span>{{ $performance->year }}</span>
                                    <br>
                                    <span>{{ $performance->perfomance_category_align }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-6 col-md-6 col-sm-12">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header">
                        <h4 class="card-title">Assessor</h4>
                    </div>
                    <div class="card-body pt-4">
                        <table class="table">
                            <thead style="background-color: #E3F8FF">
                                <tr>
                                    <th class="text-center">No</th>
                                    <th>Nama Assessor</th>
                                    <th>Nilai Rata - Rata</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total_avg = 0;
                                    $total_committee = 0;
                                @endphp
                                @foreach ($assessor[0]['talent_committee'] as $row)
                                    <tr>
                                        <td>
                                            {{ $loop->iteration }}
                                        </td>
                                        <td>
                                            {{ $row['nama'] }}
                                        </td>
                                        <td class="text-center">
                                            {{ $row['avg'] }}
                                        </td>
                                        <td>
                                            {{ $row['completed'] }}
                                        </td>
                                        <td>
                                            <a href="{{ route('talent-day.detail-assessment') . '?id=' . request('id') . '&tcom=' . $row['nik'] }}"
                                                class="btn btn-primary">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                    @php
                                        $total_avg += $row['avg'];
                                        $total_committee++;
                                    @endphp
                                @endforeach
                                @php
                                    $avg = 0;
                                    if($total_avg && $total_committee) {
                                        $avg = number_format($total_avg / $total_committee, 2);
                                    }
                                    $label = assessment_label($avg);
                                @endphp
                                <tr>
                                    <th>#</th>
                                    <th>Total Nilai</th>
                                    <th class="text-center">{{ $total_avg }}</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>#</th>
                                    <th>Nilai Akhir (Rata - rata)</th>
                                    <th class="text-center">{{ $avg }}</th>
                                    <th>{{ $label }}</th>
                                    <th></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3 employee-panel" style="display:none;">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <h4 class="card-title">Employee List</h4>
                <div class="card-toolbar">
                    <a href="javascript:void(0)" class="filter-modal-tampilkan" data-toggle="modal"
                        data-target="#filterModal">
                        <i class="fa fa-filter"></i>
                    </a>
                </div>
            </div>
            <div class="card-body pt-1 ">
                <input id="search-employee" type="text" name="" class="form-control form-control-sm"
                    placeholder="Search Employee...">
                <br>
                <section class="list-employee" style="height:55vh; overflow:auto;">
                    <ul id="myUl" data-panel="0" class="pl-1 master-facet 0"></ul>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade" id="xl-modal" tabindex="-1" role="dialog" aria-labelledby="xl-modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Employee Career Card</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body data-modal" data-scroll="true" data-height="500">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
        rel="stylesheet" />
    <style>
        @media only screen and (max-width: 600px) {
            .d-flex {
                flex-direction: column;
                flex-flow: wrap;
                justify-content: space-between;
            }

            .d-flex>.btn {
                margin: 4px;
            }
        }

    </style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/pages/crud/forms/widgets/select2.js') }}"></script>
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js">
    </script>
    @include('global.element.script_eqs_peremployee')
@endsection
