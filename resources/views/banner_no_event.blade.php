@if ($level_akses == 'admin')
    <div class="card card-custom gutter-b">
        <div class="card-body d-flex p-0">
            <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) top; background-size: 20% auto; background-image: url({{ asset('media/svg/humans/custom-5.svg') }})">
                <h2 class="text-dark pb-5 font-weight-bolder">Start Now</h2>
                <p class="text-dark-50 pb-5 font-size-h5">Klik tombol di bawah ini
                <br>untuk membuat event baru</p>
                <a href="{{ url('/event') }}" class="btn btn-danger font-weight-bold py-2 px-6">
                    {{ Metronic::getSVG("media/svg/icons/Devices/LTE2.svg", "svg-icon-3x svg-icon-white") }}
                    Buat Event
                </a>
            </div>
        </div>
    </div>
@else
    <div class="card card-custom card-stretch gutter-b">
        <div class="card-body d-flex p-0">
            <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) top; background-size: 20% auto; background-image: url(media/svg/humans/custom-3.svg)">
                <h2 class="text-dark pb-5 font-weight-bolder">Belum ada Event</h2>
                <p class="text-dark-50 pb-5 font-size-h5">Saat ini Admin HC
                <br>tidak sedang mengadakan event. Anda bisa kembali lagi nanti.</p>
            </div>
        </div>
    </div>
@endif
