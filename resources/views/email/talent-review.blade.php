@component('mail::message')
Dear {{ $prefix }} {{ $nama }},

Salam InJourney Bestie,

Dengan ini diinformasikan bahwa {{ $prefix }} {{ $nama }} telah lolos Talent Review dan akan mengikuti Talent Selection yang diadakan oleh Talent Committee InJourney Group. <br>

Demikian disampaikan atas perhatian dan kerjasamanya diucapkan terima kasih.

#Salamsehatselalu

Regards,<br>
**Talent Committee InJourney Group**
@endcomponent
