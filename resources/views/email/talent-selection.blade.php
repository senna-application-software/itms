@component('mail::message')
Dear {{ $prefix }} {{ $nama }},

Salam InJourney Bestie,

<p style="text-align: justify; text-justify: inter-word;">
	Dengan ini diinformasikan bahwa {{ $prefix }} {{ $nama }} telah lolos Talent Selection yang diadakan oleh Talent Committee InJourney Group. <br>
</p>

<p style="text-align: justify; text-justify: inter-word;">
	Demikian disampaikan atas perhatian dan kerjasamanya diucapkan terima kasih.
</p>

#Salamsehatselalu

Regards,<br>
**Talent Committee InJourney Group**
@endcomponent