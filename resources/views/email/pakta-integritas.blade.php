@component('mail::message')
Dear {{ $prefix }} {{ $nama }},

Salam InJourney Bestie,

<p style="text-align: justify; text-justify: inter-word;">
	Selamat kepada {{ $prefix }} {{ $nama }} telah terpilih untuk pengisian jabatan {{ $posisi }} {{ $area }} {{ $name_subholding }} setelah melalui proses talent selection oleh Talent Committee InJourney Group.
</p>

<p style="text-align: justify; text-justify: inter-word;">
	Sehubungan dengan hal tersebut di atas, bersama ini terlampir disampaikan Keputusan Direksi dan Pakta Integritas untuk ditanda tangani dan dikirimkan kembali ke email ini selambat-lambatnya 2 (dua) hari setelah email ini diterima.
</p>

<p style="text-align: justify; text-justify: inter-word;">
	Demikian disampaikan atas perhatian dan kerjasamanya diucapkan terima kasih.
</p>

#Salamsehatselalu

Regards,<br>
**Talent Committee InJourney Group**
@endcomponent