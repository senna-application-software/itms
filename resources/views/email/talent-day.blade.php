@component('mail::message')
Dear {{ $prefix }} {{ $nama }},

Salam InJourney Bestie,

Dengan ini diinformasikan bahwa {{ $prefix }} adalah salah satu kandidat {{ $jabatan }} {{ $sub_holding }}. Berkaitan dengan hal tersebut, kami mengundang {{ $prefix }} untuk mengikuti Talent Day yang diadakan oleh Talent Committee InJourney Group pada:

@component('mail::table')
| <!-- -->          | <!-- -->                                  |
| ----------------- | ------------------------------------------|
| Hari, Tanggal     |: {{$hari_tanggal}}                        |
| Waktu             |: {{$waktu}} (WIB)                         |
| Lokasi            |: {{$lokasi}}                              |
| Agenda            |: Presentasi/Interview Pengisian Jabatan   |
| <!-- -->          | {{ $jabatan }} {{ $sub_holding }}         |
@endcomponent

Mohon untuk konfirmasi kehadiran dengan membalas email ini pada kesempatan pertama, <br>
<br>
Demikian disampaikan atas perhatian dan kerjasamanya diucapkan terima kasih.

#Salamsehatselalu

Regards,<br>
**Talent Committee InJourney Group**
@endcomponent