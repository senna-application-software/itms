@component('mail::message')
Dear {{ $prefix }} {{ $nama }},

Salam InJourney Bestie,

<p style="text-align: justify; text-justify: inter-word;">
	Dengan ini dimohon kepada {{ $prefix }} untuk mengupdate Data diri pada:
</p>
@component('mail::table')
| <!-- -->          | <!-- -->                      |
| ----------------- | ------------------------------|
| Link Pengisian    |: [Klik di sini]({{ $url }})   |
| **Username**      |: {{ $username }}              |
| **Password**      |: {{ $password }}              |
| Periode Update    |: {{ $periode_update }} (WIB)  |
@endcomponent

<p style="text-align: justify; text-justify: inter-word;">
	Apabila mengalami kesulitan, mohon dapat menghubungi email: ITMShelpdesk@injourney.id atau ITMS helpdesk di masing-masing Perusahaan.
</p>

<p style="text-align: justify; text-justify: inter-word;">
	Demikian disampaikan atas perhatian dan kerjasamanya diucapkan terima kasih.
</p>

#Salamsehatselalu

Regards,<br>
**Talent Committee InJourney Group**
@endcomponent
