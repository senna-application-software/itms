@extends('layout.default')

@section('styles')
@endsection

@section('page_toolbar')

@endsection

@section('modal')
{{-- Modal view data --}}
<div class="modal fade" id="modalViewEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View Data Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>

            <div class="modal-body">
                <div id="dataEmployee"></div>
            </div>
        </div>
    </div>
</div>
{{-- End Modal view data --}}
@endsection

@section('content')
<div class="card card-custom">
    <div class="card-body">
        {{-- Filter --}}
        <div class="form-row mb-5">
            <div class="col-lg-4 mb-4">
                <select class="form-control filter" id="subholding" autocomplete="off">
                    <option></option>
                    @foreach($subholding as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-lg-4 mb-4">
                <select class="form-control filter" id="areaNomenklatur" autocomplete="off">
                    <option></option>
                </select>
            </div>

            <div class="col-lg-4 mb-4">
                <select class="form-control filter" id="targetPosition" autocomplete="off">
                    <option></option>
                </select>
            </div>

            <div class="col-lg-4 mb-4">
                <select class="form-control filter" id="jobFamily" autocomplete="off">
                    <option></option>
                    @foreach($jobFamily as $value)
                    <option value="{{ $value->name_job_family }}">{{ $value->name_job_family }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-lg-4 mb-4">
                <select class="form-control filter" id="grade" autocomplete="off">
                    <option></option>
                    @foreach($grade as $value)
                    <option value="{{ $value->person_grade_align }}">{{ $value->person_grade_align }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-lg-4 mb-4">
                <select class="form-control filter" id="talentCluster" autocomplete="off">
                    <option></option>
                    @foreach($talentCluster as $value)
                    <option value="{{ $value['key'] }}">{{ $value['val'] }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-lg-4 mb-4">
                <select class="form-control filter" id="isCalibrate" autocomplete="off">
                    <option></option>
                    <option value="1">Calibrated</option>
                    <option value="0">Non-Calibrated</option>
                </select>
            </div>
        </div>
        {{-- End Filter --}}

        <table class="table table-bordered table-hover" id="tableEmployee">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Employee ID Number</th>
                    <th>Employee Name</th>
                    <th>Organization</th>
                    <th>Position</th>
                    <th>Area</th>
                    <th>Grade</th>
                    <th>Employee Type</th>
                    <th>Talent Cluster</th>
                    <th>Info</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@section('scripts')
@if (session('status'))
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif

<script type="text/javascript">
    const routeDataTable = "{{ route('settings.table_talent_poll') }}",
        routeFilterOption = "{{ route('settings.filterOption') }}",
        csrfToken = "{{ csrf_token() }}";
</script>
<script type="text/javascript" src="{{ asset('js/tools-settings/talent-poll.js?nocache='.time()) }}"></script>
@endsection