@extends('layout.default')

@section('styles')
@endsection

@section('page_toolbar')
    <a href="{{ route('settings.talent_pool') }}" class="btn btn-secondary btn-md">Back</a>
@endsection

@section('content')
<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="flaticon2-chat-1 text-primary"></i>
            </span>
            <h3 class="my-4">
                Apirasi {{ $data->personnel_number }}
                <br/>
                <small>Nomor Induk Pegawai: {{ $data->prev_persno }}</small>
            </h3>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered table-hover" id="tableListAspirasiEmployee">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tipe Aspirasi</th>                    
                    <th>Aspirasi Posisi</th>
                    <th>Holding Member</th>
                    <th>Area</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    const routeDataTableListAspirasiEmployee = "{{ route('settings.data_list_aspirasi_employee') }}";
    const nik = "{{ $data->prev_persno }}";
</script>
<script type="text/javascript" src="{{ asset('js/tools-settings/list-aspirasi-employee.js?nocache='.time()) }}"></script>
@endsection