@extends('layout.default')

@section('styles')
@endsection

@section('page_toolbar')
@endsection

@section('modal')
{{-- Modal view data --}}
<div class="modal fade" id="modalViewEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View Data Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>

            <div class="modal-body">
                <div id="dataEmployee"></div>
            </div>
        </div>
    </div>
</div>
{{-- End Modal view data --}}
@endsection

@section('content')
<div class="card card-custom">
    {{-- <div class="card-header"> --}}
        {{-- <div class="card-title">
            <h3 class="card-label">Search Employee</h3>
        </div> --}}

        {{-- <div class="card-toolbar"> --}}
            {{-- <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>
                Add Employee
            </a> --}}
        {{-- </div> --}}
    {{-- </div> --}}

    <div class="card-body">
        {{-- Filter --}}
        <div class="form-row mb-5">
            <div class="col-lg-3">
                <select class="form-control filter" id="jobFamily">
                    <option></option>
                    @foreach($jobFamily as $value)
                    <option value="{{ $value->name_job_family }}">{{ $value->name_job_family }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-lg-3">
                <select class="form-control filter" id="subholding" autocomplete="off">
                    <option></option>
                    @foreach($subholding as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="col-lg-3">
                <select class="form-control filter" id="grade">
                    <option></option>
                    @foreach($grade as $value)
                    <option value="{{ $value->person_grade_align }}">{{ $value->person_grade_align }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-lg-3">
                <select class="form-control filter" id="talentCluster">
                    <option></option>
                    @foreach($talentCluster as $value)
                    <option value="{{ $value['key'] }}">{{ $value['val'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        {{-- End Filter --}}

        <table class="table table-bordered table-hover" id="tableEmployee">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Employee ID Number</th>
                    <th>Employee Name</th>
                    <th>Organization</th>
                    <th>Position</th>
                    <th>Area</th>
                    <th>Grade</th>
                    <th>Employee Type</th>
                    <th>Talent Cluster</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@section('scripts')
@if (session('status'))
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif

<script type="text/javascript">
    const routeDataTable = "{{ route('SearchDataEmployee.dataTableEmployee') }}",
        routeConstViewEmployee = "{{ route('SearchDataEmployee.show', ':id') }}",
        routeConstDeleteEmployee = "{{ route('SearchDataEmployee.destroy', ':id') }}",
        csrfToken = "{{ csrf_token() }}";

    var routeViewEmployee = "{{ route('SearchDataEmployee.show', ':id') }}",
        routeDeleteEmployee = "{{ route('SearchDataEmployee.destroy', ':id') }}";
</script>
<script type="text/javascript" src="{{ asset('js/search-employee/employee/index.js?nocache='.time()) }}"></script>
@endsection