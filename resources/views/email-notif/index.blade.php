@component('mail::message')
Dear {{ $prefix }} {{ $nama }},

Salam InJourney Bestie,

<p style="text-align: justify; text-justify: inter-word;">
	Dengan ini dimohon kepada {{ $prefix }} untuk melakukan pengisian aspirasi karir pada:
</p>
@component('mail::table')
| <!-- -->          | <!-- -->                   |
| ----------------- | ---------------------------|
| Link Pengisian    |: [Klik di sini]({{ $url }})|
| **Username**      |: {{ $username }}           |
| **Password**      |: {{ $password }}           |
| Periode Pengisian |: {{ $periode_pengisian }}  |
@endcomponent

<p style="text-align: justify; text-justify: inter-word;">
	Demikian disampaikan atas perhatian dan kerjasamanya diucapkan terima kasih.
</p>

#Salamsehatselalu

Regards,<br>
**Talent Committee InJourney Group**
@endcomponent
