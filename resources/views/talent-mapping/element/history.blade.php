
<div class="modal fade" id="xl-modal-historyLog" tabindex="-1" role="dialog" aria-labelledby="xl-modal-historyLog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">History Log</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body" id="modalHistory">
                <button id="reload_ajax" class="btn btn-sm btn-secondary">Reload</button>
                <table class="table tbl-historyLog table-sm table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th>NIK</th>
                            <th>NAMA KARYAWAN</th>
                            <th>NAMA POSISI</th>
                            <th>PREVIOUS BOX</th>
                            <th>FINAL BOX</th>
                            <th>JUSTIFICATION BOX</th>
                            <th>LAST UPDATE</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>