@if(!empty($data))
    @foreach($data as $key => $value)
        <li class="facet mb-1" data-id="{{ Crypt::encryptString($value->id) }}" style="list-style-type:none" data-panel="{{ $value->panel }}">
            <div class="badge badge-light-secondary font-weight-bold" style="text-align:left !important;" 
                data-toggle="tooltip" data-theme="dark" title="{{ strtoupper($value->personnel_number) }}">
                <div class="d-flex align-items-center ttip">
                    <div class="symbol symbol-50 symbol-light mr-4">
                        <span class="symbol-label symbol-label-custome compare-mark compare-{{ $value->prev_persno }}" data-id="{{ $value->id }}" data-nik="{{ $value->prev_persno }}" data-name="{{ $value->personnel_number }}" style="cursor:pointer;">
                            <i class="fa fa-check text-success float-right overlay {{ in_array($panel, [1,2,3,4,6]) ? '' : 'check-compare' }} " style="display:none;"></i>
                            <img src="{{ get_employee_pict($value->prev_persno) }}" class="h-75 align-self-end" alt="">
                        </span>
                    </div>
                    <h6>
                        <a href="javascript:void(0);" class="open-career-card {{ $panel != 0 ? 'text-white' : 'text-dark' }}" data-nik="{{ $value->prev_persno }}" data-niks="{{ $value->prev_persno }}"  data-id="{{ Crypt::encryptString($value->id) }}">
                            <b>{{ strtoupper($value->personnel_number) }}</b>
                        </a>
                    </h6>
                </div>
            </div>
        </li>
    @endforeach
@endif
