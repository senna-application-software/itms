@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
<script src="{{asset('asset-mt/js/pages/crud/forms/widgets/select2.js')}}"></script>
<script src="{{asset('asset-mt/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>

<script>
    var urlAjaxChief = '{{ url("employee/ajax_chief") }}';
    var urlAjaxPosition = '{{ url("employee/ajax_position") }}';
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var param = {
        _token : CSRF_TOKEN,
        chief: $("body #chief").val(),
        group: $("body #group").val(),
        position: $("body #position").val(),
        grade : $('body #grade').val(),
    }

    // get data chief
    if ($("body #filter-chief").prop("disabled")){
            $("body #filter-chief").val("").trigger("change");
    } else {
        // get data chief
        $("body #filter-chief").select2({
            placeholder: "All Chief",
            allowClear: true,
            dropdownAutoWidth: true,
            width: '100%',
            enable: $(this).prop("disabled"),
            ajax: {
                delay: 100,
                url: urlAjaxChief,
                dataType: "json",
                type: "POST",
                data: function (params) {
                    var query = {
                        search: params.term,
                        param: param,
                        _token: CSRF_TOKEN,
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data.results,
                        pagination: {
                            more: data.more,
                        },
                    };
                },
            },
        });
    }

    if ($("body #filter-position").prop("disabled")){
        $("body #filter-position").val("").trigger("change");
    } else {
        // get data filter-position
        $("body #filter-position").select2({
            placeholder: "- All Position Name -",
            allowClear: true,
            dropdownAutoWidth: true,
            width: '100%',
            ajax: {
                delay: 100,
                url: urlAjaxPosition,
                dataType: "json",
                type: "POST",
                data: function (params) {
                    var query = {
                        search: params.term,
                        param: param,
                        _token: CSRF_TOKEN,
                    };
                    return query;
                },
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data.results,
                        pagination: {
                            more: data.more,
                        },
                    };
                },
            },
        });
    }

    $('body #grade').select2({
        width: '100%',
        placeholder: "- All Grade -",
        allowClear: true
    });

    function newexportaction(e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;
        dt.one('preXhr', function (e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = 2147483647;
            dt.one('preDraw', function (e, settings) {
                // Call the original action function
                if (button[0].className.indexOf('buttons-copy') >= 0) {
                    $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                    $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                    $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                    $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-print') >= 0) {
                    $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                }
                dt.one('preXhr', function (e, s, data) {
                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                    // Set the property to what it was before exporting.
                    settings._iDisplayStart = oldStart;
                    data.start = oldStart;
                });
                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                setTimeout(dt.ajax.reload, 0);
                // Prevent rendering of the full data to the DOM
                return false;
            });
        });
        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    };

    //Datatables
    function init_DataTables(){
        if (typeof ($.fn.DataTable) === 'undefined') { return; }
        var tableHistory =  $(".tbl-historyLog").DataTable({
            // dom: "ftp",
            dom: 'Bflrtip',
            lengthMenu: [[10, -1], [10, "All"]],
            processing: true,
            serverSide: true,
            retrieve: true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            buttons: [
                // { extend: 'excel', text: '<i class="fa fa-download"></i>'}
                { extend: 'excelHtml5', text: '<i class="fa fa-download"></i>',
                    exportOptions: {
                        modifer: {
                            page: 'all',
                            search: 'none'
                        }
                    },
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7],
                    action: newexportaction
                }
            ],
            order : [7, "desc"],
            ajax: {
                url: "{{ url('calibration/ajaxDataTablesHistoryLog') }}",
                data: function(d){
                    d.year = $('#yearHistoryLog').val();
                    d.filter_chief = $('body #filter-chief').val();
                    d.filter_position = $('body #filter-position').val();
                    d.filter_grade = $('body #grade').val();
                }
            },
            // columnDefs: [{ "visible": false,  "targets": [ 7 ] }],
            columns: [
                {
                    data: "user_id",
                    defaultContent: ""
                },
                {
                    data: "personal_name",
                    defaultContent: ""
                },
                {
                    data: "position_name",
                    defaultContent: ""
                },
                {
                    data: "ps_group",
                    defaultContent: ""
                },
                {
                    data: "proposed_box",
                    defaultContent: ""
                },
                {
                    data: "panel",
                    defaultContent: ""
                },
                {
                    data: "change_by_name",
                    defaultContent: ""
                },
                {
                    data: "updated_at",
                    defaultContent: ""
                }
            ]
        });
    }

    $('#exports_career_card').click(() => {
        let action = "{{ url('calibration/download-excel') }}"
        $('#form_exports').attr('action', action);
        $('#form_exports').submit();
    });

    $(document).on("click", ".btn-historyLog", function (e) {
        init_DataTables();
    });

	$(document).on("change", "#yearHistoryLog", function(e){
		e.preventDefault();
		$('.tbl-historyLog').DataTable().ajax.reload();
	});

    $(document).on("change", ".filter-employee", function(e) {
        let filter_chief = $('#filter-chief').val(),
            filter_position = $('#filter-position').val(),
            filter_grade = $('#filter-grade').val();
            // tableHistory.ajax.reload();
    })
</script>

<script>
    function updateChoosenName(searchText) {

        var tag = document.getElementsByTagName("b");
        var element;
        var result = true;

        for (var i = 0; i < tag.length; i++) {
            if (tag[i].textContent == searchText) {
                li = tag[i].parentElement.parentElement.parentElement.parentElement.parentElement;
                li.style.display = "block";
                panel = $(".master-facet").data("panel");
                ul = li.closest("ul");
                li.remove();
                ul.prepend(li)

                // mungkin tidak support di safari
                // li.scrollIntoView()
                console.log($(li).offset().top);
                result = true;
                $('html, body').animate({
                    scrollTop: $(li).offset().top - 120
                    },1000,
                    "swing",
                    setTimeout( function() { $( li ).effect( 'bounce') },
                    1000))
                $(ul).animate({ scrollTop: $(li).offset().top - 1200 }, 1000, "swing", setTimeout( function() { $( li ).effect( 'bounce') }, 1000))
                $("#filter_selectName").val(null).trigger("change");
                break;
            }
            result = false;
        }
        if(result === false){
            showToaster(0, "Thats name may in box 0")
        }
    }

    $(".resetFilter").on("click", function(){
        window.location.reload();
    });

    function template(data, container) {
        return $('<strong class="text-muted"></strong>').text(data.text);
    }

    function select_filter_name(){
        $(".filter_selectName_button").on("click", function(){
            updateChoosenName( $("#filter_selectName").select2('data')[0]['text'] );

            setTimeout( function(){ $(".filter_selectName").val(null).trigger("change"); },200);
        });

        $('#filter_selectName').select2({
            templateSelection: template,
            placeholder: "Search by Name...",
            ajax: {
                delay       : 250,
                url         : '{{ url("calibration/ajax-select-filter-name") }}',
                dataType    : 'json',
                data        : function (params) {
                    return {
                        search  : params.term,
                        tipe    : 'personnel_number',
                        tahun    : $('#tahun_panel').val(),
                    };
                },
                processResults: function (data) {
                    return {
                        results     : data.results,
                        pagination  : {
                            more: data.more
                        }
                    };
                }
            }
        });

        $('#tahun_panel').select2({});

        $(".select2-selection--single").css("border-top-right-radius", "0");
        $(".select2-selection--single").css("border-bottom-right-radius", "0");
    }
</script>

<script>
    var dataFilter  = "";
    var hrOnly      = false;
    var pos         = [];
    var grade       = [];
    var talent      = false;
    var is_session  = false;

    $(document).ready(function (e) {
        if ($('#talent-switch').checked) {
            $('body .card-hide').hide();
        } else {
            $('body .card-hide').show();
        }

        if ($('#box0-switch').checked) {
            $('body .card-hide').hide();
        } else {
            $('body .card-hide').show();
        }

        $(document).on('click', "#talent-switch", function() {
            if (this.checked) {
                // getTalentMappingChart();
                talent = true;
                $('body .card-hide').hide();
                $('body #card-nine').addClass('order-12');
                $('.xxxx').css('height', '70vh').css('overflow', 'auto')
                $('.height-custome').css('height', 'unset')
                $('.card-body').css('height', '80vh').css('overflow','hidden')
                $('body .card-8').removeClass('card-customize-bigger');
                $('body .card-7, body .card-8, body .card-9').removeClass('card-customize');

                $('body .card-7').parent().removeClass('col-md-6').addClass('col-md-4');
                $('body .card-8').parent().removeClass('col-md-3').addClass('col-md-4');
                $('body .card-9').parent().removeClass('col-md-3').addClass('col-md-4');
                $('body #form-label-switch').text('Showing Talent Only');
                loadAllEmpPercent();
            } else {
                talent = false;
                $('body .card-hide').show();
                $('body #card-nine').removeClass('order-12');
                
                $('.xxxx').css('height', 'unset').css('overflow', 'auto')
                $('.card-body').css('height', 'unset').css('overflow','hidden')
                $('body .card-8').addClass('card-customize-bigger');
                $('.height-custome').css('height', '12vh')
                $('body .card-7, body .card-8, body .card-9').addClass('card-customize');
                $('body .card-7').parent().removeClass('col-md-4').removeClass('card-customize').addClass('col-md-6');
                $('body .card-8').parent().removeClass('col-md-4').removeClass('card-customize-bigger').addClass('col-md-3');
                $('body .card-9').parent().removeClass('col-md-4').removeClass('card-customize-bigger').addClass('col-md-3');
                
                $('body #form-label-switch').text('Showing All Employee');
                loadAllEmpPercent();
            }
        });

        $(document).on('change', "#box0-switch, .btn-check-box-nol", function() {
            if (this.checked) {
                $('.card-panel').removeClass("col-md-12")
                $('.card-panel').addClass("col-md-9")
                $(".employee-panel").show();
                if (!($('body').hasClass("aside-minimize"))) {
                    $('body').addClass("aside-minimize")
                }
                // $('body #form-box0-switch').text('Hide Box 0');
            } else {
                $('.card-panel').addClass("col-md-12")
                $('.card-panel').removeClass("col-md-9")
                $(".employee-panel").hide();
                $('body').removeClass("aside-minimize")    
                // $('body #form-box0-switch').text('Show Box 0')
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });

        loadAllEmp() 
        select_filter_name();
    })

    $('#search-employee').keyup(function (e) {
        var input, filter, ul, li, a, i, txtValue;
        input   = document.getElementById("search-employee");
        filter  = input.value.toUpperCase();
        ul      = document.getElementById("myUl");
        li      = ul.getElementsByTagName("li");

        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("b")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            }
            else {
                li[i].style.display = "none";
            }
        }
    })

    $('#filter_select').select2({
        placeholder: "Search in context selected above",
        dropdownParent: $('#filterModal'),
        ajax: {
            delay       : 250,
            url         : '{{ url("calibration/ajax-select-filter") }}',
            dataType    : 'json',
            data        : function (params) {
                return {
                    search  : params.term,
                    tipe    : $('input[name="tipe"]:checked').val()
                };
            },
            processResults: function (data) {
                return {
                    results     : data.results,
                    pagination  : {
                        more    : data.more
                    }
                };
            }
        }
    });

    $(document).on('click','.clear-filter', function (e) {
        loadEmployee(0);
        loadEmployeePercent(0);
        $("#filter_select").val(null).trigger("change");
            showToaster(1, "Employee List Filter Cleared")
    })

    $('#form-filter').submit(function (e) {
        e.preventDefault();
        $('.close').click()

        dataFilter = $(this).serialize()
        KTApp.block('.0', {
            overlayColor    : '#000000',
            state           : 'primary',
            message         : 'Processing...'
        })
        $.ajax({
            url     : $(this).attr('action'),
            type    : $(this).attr('method'),
            data    : $(this).serialize(),
            success : function( data ) {
                KTApp.unblock('.0')
                showToaster(1, "Employee List Filtered")
                $('.0').html(data)
            },
            error   : function( xhr, err ) {
                KTApp.unblock('.0')
                showToaster(0, "Error On Filter Employee")
            }
        });
        return false;
    });

    $(document).on('click','.btn-employee', function (e) {
        let open = $(this).data('open')
        console.log(open)
        if (open == 1) {
            $('.card-panel').removeClass("col-md-12")
            $('.card-panel').addClass("col-md-9")
            $(".employee-panel").show();
            if (!($('body').hasClass("aside-minimize"))) {
                $('body').addClass("aside-minimize")
            }
        }
        else {
            $('.card-panel').addClass("col-md-12")
            $('.card-panel').removeClass("col-md-9")
            $(".employee-panel").hide();
            $('body').removeClass("aside-minimize")
        }
        $('.btn-employee').toggle();
    });

    $(document).on("click",".open-career-card", function (e) {
        e.preventDefault();
        var nik = $(this).data('nik')
        
        var url = "{{ url('employee/view-career-card') }}" + '/' + nik + '/true'
        $.ajax({
            url     : url,
            type    : 'GET',
            success : function (response) {
                $('body #xl-modal').modal('show');
                $('.data-modal').html(response)
            }
        })
    })

    var compareData = [];
    var compareDataEnc = [];

    $(document).on("click",".compare-mark", function (e) {
        
        $(this).find(".check-compare").removeClass("fa-square");
        $(this).find(".check-compare").addClass("fa-check");
        $(this).find(".check-compare").addClass("text-success");

        let usernik = $(this).data('nik');
        let usernikEnc = $(this).data('nik-enc');

        if (compareData.includes(usernik)) {
            compareData = removeNikFromArray(compareData, usernik)
            compareDataEnc = removeNikFromArray(compareDataEnc, usernikEnc)
            // $(this).find(".check-compare").hide();
            $(this).find(".check-compare").removeClass("fa-check");        
            $(this).find(".check-compare").removeClass("text-success");
            $(this).find(".check-compare").addClass("fa-square");

        } else {
            compareData.push(usernik);
            compareDataEnc.push(usernikEnc);
            // $(this).find(".check-compare").show();
            $(this).find(".check-compare").removeClass("fa-square");
            $(this).find(".check-compare").addClass("fa-check");
            $(this).find(".check-compare").addClass("text-success");
        }

        let jmlData = compareData.length

        if ( jmlData > 3 ) {
            let delNik = compareData[0]
            // $(".compare-"+delNik).find(".check-compare").hide();
            $(".compare-"+delNik).find(".check-compare").removeClass("fa-check");        
            $(".compare-"+delNik).find(".check-compare").removeClass("text-success");
            $(".compare-"+delNik).find(".check-compare").addClass("fa-square");
            compareData.shift();
            compareDataEnc.shift();
        }

        if ( jmlData < 2 ) {
            $(".btn-compare").prop('href','javascript:void(0)')            
            $(".btn-compare").text("Compare")    
        } else {
            $(".btn-compare").text("OK")

            let url = "{{ url('calibration/compare') . '/' }}"  + compareDataEnc.join("-")
            $(".btn-compare").prop('href',url)
        }
    })

    var compareDataModal = [];
    var compareDataModalEnc = [];

    $(document).on("click",".compare-mark-modal", function (e) {
        e.preventDefault();
        let usernik = $(this).data('nik');
        let usernikEnc = $(this).data('nik-enc');

        if (compareDataModal.includes(usernik)) {
            compareDataModal = removeNikFromArray(compareDataModal, usernik)
            compareDataModalEnc = removeNikFromArray(compareDataModalEnc, usernikEnc)
            $(this).find(".check-compare").hide();
        }
        else {
            compareDataModal.push(usernik);
            compareDataModalEnc.push(usernikEnc);
            $(this).find(".check-compare").show();
        }

        let jmlData = compareDataModal.length
        console.log(jmlData)
        if ( jmlData > 3 ) {
            let delNik = compareDataModal[0]
            $(document).find(".compare-modal-"+delNik).find(".check-compare").hide();
            compareDataModal.shift();
            compareDataModalEnc.shift();
        }

        if ( jmlData < 2 ) {
            $(document).find(".btn-compare-modal").removeClass("btn-warning")
            $(document).find(".btn-compare-modal").addClass("btn-default")
            $(document).find(".btn-compare-modal").prop('href','javascript:void(0)')
        }
        else {
            $(document).find(".btn-compare-modal").removeClass("btn-default")
            $(document).find(".btn-compare-modal").addClass("btn-warning")

        let url = "{{ url('calibration/compare') . '/' }}"  + compareDataModalEnc.join("-")
            $(document).find(".btn-compare-modal").prop('href',url)
        }
    })

    $(".btn-hronly").click(function (e) {
        $('.btn-hronly').toggle()
        compareDataModal = []
        $(".btn-compare").removeClass("btn-warning")
        $(".btn-compare").addClass("btn-default")
        $(".btn-compare").prop('href','javascript:void(0)')

        if (hrOnly) {
            hrOnly = false
        }
        else {
            hrOnly = true
        }
        loadAllEmp()
    })
</script>

<script>
    function loadAllEmp() {
        loadEmployee()
    }

    function loadAllEmpPercent() {
        loadEmployeePercent()
    }

    function removeNikFromArray(array, usernik) {
        return $.grep(array, function(value) {
            return value != usernik;
        });
    }

    $(function() {
        let role = ( '{{  Auth::user()->role }}' ).toLowerCase();
        if( role == "pm"){
            return;
        }
        $(".master-facet").sortable({
            connectWith : "ul",
            placeholder : "placeHold",
            delay       : 150,
            cursor      : 'move',
            helper      : 'clone',
            appendTo    : 'body',
            zIndex      : 10000,
            easing      : "cubic-bezier(1, 0, 0, 1)",
            stop        : function (e, item) {
                var helper      = item.item.children().children().children().children()[1];
                var cekDisplay  = $(".employee-panel").css("display");
                var destinasi   = $(item.item.parent().parent().parent().parent().parent()).hasClass("employee-panel");
                if(destinasi){
                    $(helper).removeClass("text-white");
                    $(helper).addClass("text-dark");
                }
                else{
                    $(helper).removeClass("text-dark");
                    $(helper).addClass("text-white");
                }
            },
            receive     : function (event, ui) {
                if ($('#periodeInput').val() == 'Expired') {
                    $(this).sortable('cancel');
                    $(ui.sender).sortable('cancel');
                    Swal.fire('Calibration update periode is over. <br> Please contact admin for further information');
                }
                else{
                    let target  = $(event.target)
                    let item    = ui.item
                    let panel   = target.data('panel')
                    let nik     = item.data('nik')
                    let url     = "{{ url('calibration/ajax-update-panel') }}"
                    var res = $.ajax({
                        url     : url,
                        type    : 'POST',
                        data    : {
                            "_token"    : "{{ csrf_token() }}",
                            "panel"     : panel,
                            "nik"       : nik
                        },
                        success : function (response) {
                            // tableHistory.ajax.reload();
                            loadAllEmpPercent();
                            console.log(response);
                        }
                    });
                }
            }
        })
        .disableSelection()
    });

    function loadEmployee(panel) {
        $('.0,.1,.2,.3,.4,.5,.6,.7,.8,.9').html("")

        KTApp.block('.0,.1,.2,.3,.4,.5,.6,.7,.8,.9', {
            overlayColor    : '#000000',
            state           : 'primary',
            message         : 'Processing...'
        });

        let url = "{{ url('calibration/ajax-employee') }}"
        var xhr = $.ajax({
            url     : url,
            type    : 'POST',
            data    : {
                "_token"    : "{{ csrf_token() }}",
                // "panel"     : panel,
                "hrOnly"    : hrOnly,
                "pos"       : pos,
                "grade"     : grade,
                "talent"    : talent,
                "is_session" : is_session
            },
            timeout: 100000,
            success : function (response) {

                let res = JSON.parse(response);
                $.each(res.view, function( key, value ) {
                    $('.'+key).html(value)
                    $('.'+key).find(".check-compare").each(function() {
                        $('.'+key).find(".check-compare").removeClass("fa-check");
                        $('.'+key).find(".check-compare").removeClass("text-success");
                        $('.'+key).find(".check-compare").addClass("fa-square");
                        $('.'+key).find(".check-compare").css("display","inline");
                    });
                });
                $.each(res.total, function( key, value ) {
                    $('body #label-panel-'+key).html(value)
                });
                KTApp.unblock('.0,.1,.2,.3,.4,.5,.6,.7,.8,.9')
            },
            error: function(jqXHR, textStatus, errorThrown){
                KTApp.unblock('.'+panel)
                // KTApp.unblock('.xxxx')
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                let appendText = "<span>Unable to load. Please reload this box</span><br>"
                let appendButton = "<button class='btn btn-sm btn-inline btn-dark' onclick='loadEmployee("+panel+");'>Reload</button>"
                let $containerDivFail = $("<div class='btn-reload-fail-box-"+panel+"'>"+appendText+appendButton+"</div>")
                $('.'+panel).html($containerDivFail)
            }
        })
    }

    function loadEmployeePercent(panel) {
        $('body .label-panel').html("");

        KTApp.block('body .label-panel', {
            state           : 'light',
        });

        let url = "{{ url('calibration/ajax-employee-percent') }}"
        $.ajax({
            url     : url,
            type    : 'POST',
            data    : {
                "_token"    : "{{ csrf_token() }}",
                // "panel"     : panel,
                "hrOnly"    : hrOnly,
                "pos"       : pos,
                "grade"     : grade,
                "talent"    : talent,
                "is_session" : is_session
            },
            // timeout: 100000,
            success : function (response) {
                let res = JSON.parse(response);
                let bar1 = 0;
                let bar2 = 0;
                let bar3 = 0;

                $.each(res.view, function( key, value ) {
                    $('body #label-panel-'+key).html(value)
                });

                let dt = $(".persentase");
                
                $.each(dt, function(key, value){
                    // console.log($(this).data("length"));
                    if (key == 0 || key == 1 || key == 2) {
                        bar1 += parseInt($(this).data("length"));
                    } else if (key == 3 || key == 4 || key == 5) {
                        bar2 += parseInt($(this).data("length"));
                    } else if (key == 6 || key == 7 || key == 8) {
                        bar3 += parseInt($(this).data("length"));
                    } else {

                    }
                });

                let col = 4;
                $.each(dt, function(key, value){                    
                    if (key == 0 || key == 1 || key == 2) {
                        if($(this).data("length") == 0) {                  
                            $(this).parent().parent().parent().parent().addClass("col-auto");
                        } else {
                            col = Math.ceil($(this).data("length") / bar1 * 12);
                            console.log($(this).data("length"));
                            if(col <= 3) {
                                col = 4;
                            } else if( col == 4) {
                                col = 4;
                            } else if(col > 4){
                                col = 4;
                            } else {
                                col = col;
                            }  
                            $(this).parent().parent().parent().parent().removeClass("col-md-4")
                            $(this).parent().parent().parent().parent().addClass("col-"+col)                        
                            $(this).parent().parent().parent().parent().addClass("col-auto")
                            // $(this).parent().parent().parent().css("height",col * 10 + "%")
                        }
                    } else if (key == 3 || key == 4 || key == 5) {
                        if($(this).data("length") == 0) {                  
                            $(this).parent().parent().parent().parent().addClass("col-auto");
                        } else {
                            col = Math.ceil($(this).data("length") / bar2 * 12);
                            console.log($(this).data("length"));
                            if(col <= 3) {
                                col = 4;
                            } else if( col == 4) {
                                col = 4;
                            } else if(col > 4){
                                col = 4;
                            } else {
                                col = col;
                            }  
                            $(this).parent().parent().parent().parent().removeClass("col-md-4")
                            $(this).parent().parent().parent().parent().addClass("col-"+col)                        
                            $(this).parent().parent().parent().parent().addClass("col-auto")
                            // $(this).parent().parent().parent().css("height",col * 10 + "%")
                        }
                    } else if (key == 6 || key == 7 || key == 8) {
                        if($(this).data("length") == 0) {                  
                            $(this).parent().parent().parent().parent().addClass("col-auto");
                        } else {
                            col = Math.ceil($(this).data("length") / bar3 * 12);
                            console.log($(this).data("length"));
                            if(col <= 3) {
                                col = 4;
                            } else if( col == 4) {
                                col = 4;
                            } else if(col > 4){
                                col = 4;
                            } else {
                                col = col;
                            }  
                            $(this).parent().parent().parent().parent().removeClass("col-md-4")
                            $(this).parent().parent().parent().parent().addClass("col-"+col)                        
                            $(this).parent().parent().parent().parent().addClass("col-auto")
                            // $(this).parent().parent().parent().css("height",col * 10 + "%")
                        }
                    } else {

                    }

                })
                KTApp.unblock('body .label-panel')
            },
            error: function(jqXHR, textStatus, errorThrown){

            }
        })

    }

    $('.dropdown-toggle').on('click', function(event) {
        $(this).parent().find(".dropdown-menu").slideToggle();
        event.stopPropagation();

        let buttonClick = $(this);
        let buttonQuery = $('body').find('.dropdown-toggle').eq(0);
        let buttonQuery2 = $('body').find('.dropdown-toggle').eq(1);

        if(buttonClick.attr('id') == buttonQuery.attr('id')){
            if(buttonQuery2.parent().find(".dropdown-menu").is(':visible')){
                $(buttonQuery2).parent().find(".dropdown-menu").slideToggle();
            }
        }

        if(buttonClick.attr('id') == buttonQuery2.attr('id')){
            if(buttonQuery.parent().find(".dropdown-menu").is(':visible')){
                $(buttonQuery).parent().find(".dropdown-menu").slideToggle();
            }
        }
    });

    $('.dropdown-menu').on('click', function(event) {
        event.stopPropagation();
    });

    $(window).on('click', function() {
        $('.dropdown-menu').slideUp();
    });

    $('.btn-check').click(function (e) {
        let tipe = $(this).data('tipe');

        if (tipe == 'checkAll') {
            $('input:checkbox.btn-check-pos').prop('checked', 'checked');
            updateCheckbox()
        }
        else {
            $('input:checkbox.btn-check-pos').prop('checked', '');
        }
    })

    $('.btn-check-grade').click(function (e) {
        let tipe = $(this).data('tipe');

        if (tipe == 'checkAll') {
            $('input:checkbox.btn-check-grades').prop('checked', 'checked');
            updateCheckbox()
        }
        else {
            $('input:checkbox.btn-check-grades').prop('checked', '');
        }
    })

    $('input[name="pos[]"]').click(function (e) {
        updateCheckbox()
    })
    $('input[name="grade[]"]').click(function (e) {
        updateCheckbox()
    })

    function updateCheckbox() {
        pos = [];
        grade = [];
        is_session = true;
        $('input[name="pos[]"]:checked').each(function() {
            pos.push(this.value);
        });
        $('input[name="pos[]"]:checked').each(function() {
            pos.push(this.value);
        });

        $('input[name="grade[]"]:checked').each(function() {
            grade.push(this.value);
        });
        $('input[name="grade[]"]:checked').each(function() {
            grade.push(this.value);
        });

        loadAllEmp()
    }
</script>

<script>
    function loadEmployeeOnModal(panel) {
        let url = "{{ url('calibration/ajax-employee') }}"
        $("#modal-body-full-screen").html("")
        KTApp.block("#modal-body-full-screen", {
            overlayColor    : '#000000',
            state           : 'primary',
            message         : 'Processing...'
        })
        $.ajax({
            url     : url,
            type    : 'POST',
            data    : {
                "_token"    : "{{ csrf_token() }}",
                "panel"     : panel,
                "hrOnly"    : hrOnly,
                "pos"       : pos,
                "grade"     : grade,
                "showInModal"  : true
            },
            success : function (response) {
                KTApp.unblock("#modal-body-full-screen")
                $("#modal-body-full-screen").html(response)
            }
        })
    }

    $(document).on("click", ".btn-show-full-screen", function () {
        let panel = $(this).parent().find("ul").data("panel");
        let color = $(this).data("color");

        $("#modal-title-full-screen").html("Talent Box - " + panel);
        $("#modal-title-full-screen").css("color", color);
        // $("#modal-header-full-screen").css("background-color", color);

        loadEmployeeOnModal(panel);
    });

    $(document).on("click", ".btn-compare-modal, .btn-compare", function (e) {
        e.preventDefault();
        let link = $(this).attr("href");
        if(link == "javascript:void(0)"){
            
            Swal.fire('Please choose employee max 3 person'); 
            return false;
            
        }
        else{
            window.open(link,"_blank");
        }
    });

    $('#show-full-screen-modal').on('hidden.bs.modal', function () {
        compareDataModal = [];
        compareDataModalEnc = [];
        $(".btn-compare-modal").removeClass("btn-warning")
        $(".btn-compare-modal").addClass("btn-default")
    });
</script>
@endsection