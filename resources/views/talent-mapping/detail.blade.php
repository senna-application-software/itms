{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-12 mb-5">
        <div class="card px-5 py-3">
            <div class="d-flex justify-content-between">
                <h4 class="align-self-center m-0 p-4">
                    {{ $sub_event->name_position }} 
                    <span class="label font-weight-bold label-lg {{ $sub_event->sourcing_type == 1 ? 'label-light-success' : 'label-light-primary' }} label-inline">
                        {{ $sub_event->sourcing_type == 1 ? 'External' : 'Internal' }}
                    </span>
                    <small class="text-muted">{{ $sub_event->name_subholding }} - [{{ $sub_event->name_area }}]</small>
                </h4>
                <button id="done_mapping" class="btn btn-primary">Done Mapping</button>
            </div>
        </div>
    </div>

    <div class="card-panel col-md-12">
        <div class="row"> 
            @foreach($boxes as $key => $box)
                <div class="col-md-4 customeHeight card-hide">
                    <div class="card card-custom card-customize gutter-b bg-primary" style="background-color:{{ $box->cat_color }}!important">
                        <div class="card-body px-4 pt-3 py-0 mb-0">
                            <div class="text-center text-white font-size-h1 font-weight-bold">{{ ucwords($box->cat_name) }}</div> 
                            <h1 class="text-center font-weight-bolder label-custome bg-warning float-right">{{ $box->box }}</h1>
                            <span class="label-panel mt-0 pt-0" id="label-panel-{{ $box->box }}" style="margin-left: 50% !important"></span>                                    
                            <br>
                            <div class="xxxx">
                                <ul data-panel="{{ $box->box }}" class="pl-1 master-facet master-facet-bigger {{ $box->box }} scroll d-flex justify-content-start height-custome" id="panel-{{ $box->box }}">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach 
        </div>
    </div>

    <div class="col-md-3 employee-panel" style="display:none;">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <h4 class="card-title">Employee List</h4>
                <div class="card-toolbar">
                    <a href="javascript:void(0)" class="filter-modal-tampilkan" data-toggle="modal" data-target="#filterModal">
                        <i class="fa fa-filter"></i>
                    </a>
                </div>
            </div>
            <div class="card-body pt-1 ">
                <input id="search-employee" type="text" name="" class="form-control form-control-sm" placeholder="Search Employee...">
                <br>
                <section class="list-employee" style="height:55vh; overflow:auto;">
                    <ul id="myUl" data-panel="0" class="pl-1 master-facet 0"></ul>
                </section>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="xl-modal" tabindex="-1" role="dialog" aria-labelledby="xl-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Employee Career Card</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body data-modal" data-scroll="true" data-height="500">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{{-- modal done mapping --}}
<div class="modal fade" id="modal_mapping" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('talent-mapping.done_mapping') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $id }}">
                <input type="hidden" name="emp_selected" id="emp_selected">
                <div class="modal-header">
                    <h5 class="modal-title">Confirm Talent Mapping Results</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Panel Schedule</h4>
                            <div class="form-group">
                                <label for="">Tanggal</label>
                                <input type="date" name="tanggal" class="form-control" data-date-format="dd/mm/yy"  required>
                                
                            </div>
    
                            <div class="form-group">
                                <label for="">Waktu</label>
                                <input type="time" name="waktu" class="form-control" required>
                            </div>
        
                            <div class="form-group">
                                <label for="">Lokasi</label>
                                <input type="text" name="lokasi" class="form-control" required>
                            </div>
                            
                            <div class="form-group">
                                <label for="">Agenda</label>
                                <input type="text" name="agenda" class="form-control" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr id="mapping_head">
                                    
                                    </tr>
                                </thead>
                                <tbody id="mapping_body">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    

                    <input type="checkbox" required> <label>Kandidat yang terpilih sudah sesuai</label>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="done">Done</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('modal')
    @include("talent-mapping.element.history")
@endsection

{{-- Content Section --}}
@section('page_toolbar')
    <button id="btn_history" class="btn btn-primary">Riwayat</button>
@endsection

{{-- Styles Section --}}
@section('styles')
<link href="{{asset('plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<style>
    @media only screen and (max-width: 600px) {
        .d-flex {
            flex-direction: column;
            flex-flow: wrap;
            justify-content: space-between;
        }
        .d-flex > .btn {
            margin: 4px;
        }
    }
    
    .container-customeHeight {
        height: 200px;
    }

    .label-custome {
        padding: 6px 8px;
        width: 40px;
        height: 40px;
        justify-content: center;
        margin: 0;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        border-radius: 50%;
        background-color: #EBEDF3;
        font-weight: 900!important;
        color: black!important;
        font-size: 2rem!important;
    }
    .content {
        overflow-x: auto;
    }
    .overlay {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
    }
    .blockOverlay {
        background-color: transparent!important
    }
    
    .placeHold{
        border: 1px dashed blue;
        background-color: #c7c7c7;
        height:40px;
        width:228px;
        list-style-type:none;
    }

    .symbol-label-custome {
        width:35px!important;
        height:25px!important;
    }
    
    .facet {
        width:228px;
        height:40px;
        margin-right: 2px;
        display:flex;
        overflow-x:hidden;
        flex:auto;
    }  

    .blockOverlay {
        background-color: transparent!important
    }

    .master-facet {
        list-style-type:none;
        min-width:100%;
        flex-wrap: wrap;
        flex-direction: row;
        overflow-y: auto;
        /* height: 180px; */
        place-content: flex-start;
        /* flex-flow: column; */
    }

    .master-facet-bigger {
        list-style-type:none;
        min-width:100%;
        flex-wrap: wrap;
        flex-direction: row;
        overflow-x: auto;
        overflow-y: auto;
        height: 214px;
        /* flex-flow: column; */
    }

    .xxxx {        
        list-style-type:none;
        overflow: auto;
        flex-flow: wrap;
        flex-direction: row;
        flex-wrap: wrap;
        flex-basis: auto;
        display: flex;
        align-items: flex-start;
        height: 228px;        
    }
</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous"></script>
<script src="{{asset('js/pages/crud/forms/widgets/select2.js')}}"></script>
<script src="{{asset('plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
@include('global.element.script_eqs_peremployee')
<script>
    var dataFilter  = "";
    var hrOnly      = false;
    var pos         = [];
    var grade       = [];
    var talent      = false;
    var is_session  = false;

    var sub_event   = {{ $sub_event->id }}

    //fungsi load employee
    function loadEmployee(panel) {
        $('.0,.1,.2,.3,.4,.5,.6,.7,.8,.9').html("")

        KTApp.block('.0,.1,.2,.3,.4,.5,.6,.7,.8,.9', {
            overlayColor    : '#000000',
            state           : 'primary',
            message         : 'Processing...'
        });

        let url = "{{ url('talent-mapping/get-employee') }}"
        var xhr = $.ajax({
            url     : url,
            type    : 'POST',
            data    : {
                "_token"    : "{{ csrf_token() }}",
                // "panel"     : panel,
                "hrOnly"    : hrOnly,
                "pos"       : pos,
                "grade"     : grade,
                "talent"    : talent,
                "is_session" : is_session,
                "subid": "{{ $sub_event->id }}"
            },
            timeout: 100000,
            success : function (response) {

                let res = JSON.parse(response);
                $.each(res.view, function( key, value ) {
                    $('.'+key).html(value)
                    $('.'+key).find(".check-compare").each(function() {
                        $('.'+key).find(".check-compare").removeClass("fa-check");
                        $('.'+key).find(".check-compare").removeClass("text-success");
                        $('.'+key).find(".check-compare").addClass("fa-square");
                        $('.'+key).find(".check-compare").css("display","inline");
                    });
                });
                $.each(res.total, function( key, value ) {
                    $('body #label-panel-'+key).html(value)
                });
                KTApp.unblock('.0,.1,.2,.3,.4,.5,.6,.7,.8,.9')
            },
            error: function(jqXHR, textStatus, errorThrown){
                KTApp.unblock('.'+panel)
                
                let appendText = "<span>Unable to load. Please reload this box</span><br>"
                let appendButton = "<button class='btn btn-sm btn-inline btn-dark' onclick='loadEmployee("+panel+");'>Reload</button>"
                let $containerDivFail = $("<div class='btn-reload-fail-box-"+panel+"'>"+appendText+appendButton+"</div>")
                $('.'+panel).html($containerDivFail)
            }
        })
    }

    var compareData = [];
    var compareDataEnc = [];

    //fungsi cek checkbox
    $(document).on("click",".compare-mark", function (e) {
        if($(this).parent().parent().parent().parent().attr('data-panel') == 5 | $(this).parent().parent().parent().parent().attr('data-panel') > 6) {
            $(this).find(".check-compare").removeClass("fa-square");
            $(this).find(".check-compare").addClass("fa-check");
            $(this).find(".check-compare").addClass("text-success");
    
            let usernik = $(this).data('nik');
            let usernikEnc = $(this).data('name');
    
            if (compareData.includes(usernik)) {
                compareData = removeNikFromArray(compareData, usernik)
                compareDataEnc = removeNikFromArray(compareDataEnc, usernikEnc)
                // $(this).find(".check-compare").hide();
                $(this).find(".check-compare").removeClass("fa-check");        
                $(this).find(".check-compare").removeClass("text-success");
                $(this).find(".check-compare").addClass("fa-square");
    
            } else {
                compareData.push(usernik);
                compareDataEnc.push(usernikEnc);
                // $(this).find(".check-compare").show();
                $(this).find(".check-compare").removeClass("fa-square");
                $(this).find(".check-compare").addClass("fa-check");
                $(this).find(".check-compare").addClass("text-success");
            }
        }
        
    });

    function removeNikFromArray(array, usernik) {
        return $.grep(array, function(value) {
            return value != usernik;
        });
    }

    $("#done_mapping").click(function (e) { 
        e.preventDefault();

        if(compareData.length == 0){
            Swal.fire(
                'Error!',
                'Silahkan ceklis salah satu data',
                'error'
            )
        } else {
            let hatml_head = '',hatml_body = '', total = 0;

            for (let p = 0; p < compareDataEnc.length; p++) {
                hatml_body += `<tr>
                            <td>`+ compareDataEnc[p] +`</td>
                        </tr>`;
                
                total += 1;
            }

            $('#emp_selected').val(compareData)

            $("#mapping_head").html('<th scope="col">Choosen Candidates (' + total + ')</th>');
            $("#mapping_body").html(hatml_body);
            $("#modal_mapping").modal();
        }
    });
    
    function load_percent(panel) {
        $('body .label-panel').html("");

        KTApp.block('body .label-panel', {
            state           : 'light',
        });

        let url = "{{ url('talent-mapping/update-percent') }}"
        $.ajax({
            url     : url,
            type    : 'POST',
            data    : {
                "_token"    : "{{ csrf_token() }}",
                "hrOnly"    : hrOnly,
                "pos"       : pos,
                "grade"     : grade,
                "talent"    : talent,
                "is_session" : is_session
            },
            success : function (response) {
                let res = JSON.parse(response);
                let bar1 = 0;
                let bar2 = 0;
                let bar3 = 0;

                $.each(res.view, function( key, value ) {
                    $('body #label-panel-'+key).html(value)
                });

                let dt = $(".persentase");
                
                $.each(dt, function(key, value){
                    if (key == 0 || key == 1 || key == 2) {
                        bar1 += parseInt($(this).data("length"));
                    } else if (key == 3 || key == 4 || key == 5) {
                        bar2 += parseInt($(this).data("length"));
                    } else if (key == 6 || key == 7 || key == 8) {
                        bar3 += parseInt($(this).data("length"));
                    } else {

                    }
                });

                let col = 4;
                $.each(dt, function(key, value){                    
                    if (key == 0 || key == 1 || key == 2) {
                        if($(this).data("length") == 0) {                  
                            $(this).parent().parent().parent().parent().addClass("col-auto");
                        } else {
                            col = Math.ceil($(this).data("length") / bar1 * 12);
                            if(col <= 3) {
                                col = 4;
                            } else if( col == 4) {
                                col = 4;
                            } else if(col > 4){
                                col = 4;
                            } else {
                                col = col;
                            }  
                            $(this).parent().parent().parent().parent().removeClass("col-md-4")
                            $(this).parent().parent().parent().parent().addClass("col-"+col)                        
                            $(this).parent().parent().parent().parent().addClass("col-auto")
                            // $(this).parent().parent().parent().css("height",col * 10 + "%")
                        }
                    } else if (key == 3 || key == 4 || key == 5) {
                        if($(this).data("length") == 0) {                  
                            $(this).parent().parent().parent().parent().addClass("col-auto");
                        } else {
                            col = Math.ceil($(this).data("length") / bar2 * 12);
                            console.log($(this).data("length"));
                            if(col <= 3) {
                                col = 4;
                            } else if( col == 4) {
                                col = 4;
                            } else if(col > 4){
                                col = 4;
                            } else {
                                col = col;
                            }  
                            $(this).parent().parent().parent().parent().removeClass("col-md-4")
                            $(this).parent().parent().parent().parent().addClass("col-"+col)                        
                            $(this).parent().parent().parent().parent().addClass("col-auto")
                        }
                    } else if (key == 6 || key == 7 || key == 8) {
                        if($(this).data("length") == 0) {                  
                            $(this).parent().parent().parent().parent().addClass("col-auto");
                        } else {
                            col = Math.ceil($(this).data("length") / bar3 * 12);
                            console.log($(this).data("length"));
                            if(col <= 3) {
                                col = 4;
                            } else if( col == 4) {
                                col = 4;
                            } else if(col > 4){
                                col = 4;
                            } else {
                                col = col;
                            }  
                            $(this).parent().parent().parent().parent().removeClass("col-md-4")
                            $(this).parent().parent().parent().parent().addClass("col-"+col)                        
                            $(this).parent().parent().parent().parent().addClass("col-auto")
                        }
                    } else {

                    }

                })
                KTApp.unblock('body .label-panel')
            },
            error: function(jqXHR, textStatus, errorThrown){
            }
        })

    }

    $(".master-facet").sortable({
        connectWith : "ul",
        placeholder : "placeHold",
        delay       : 150,
        cursor      : 'move',
        helper      : 'clone',
        appendTo    : 'body',
        zIndex      : 10000,
        easing      : "cubic-bezier(1, 0, 0, 1)",
        stop        : function (e, item) {
            var helper      = item.item.children().children().children().children()[1];
            var cekDisplay  = $(".employee-panel").css("display");
            var destinasi   = $(item.item.parent().parent().parent().parent().parent()).hasClass("employee-panel");
            if(destinasi){
                $(helper).removeClass("text-white");
                $(helper).addClass("text-dark");
            }
            else{
                $(helper).removeClass("text-dark");
                $(helper).addClass("text-white");
            }
        },
        receive     : function (event, ui) {
            if ($('#periodeInput').val() == 'Expired') {
                $(this).sortable('cancel');
                $(ui.sender).sortable('cancel');
                Swal.fire('Calibration update periode is over. <br> Please contact admin for further information');
            } else {
                let target      = $(event.target)
                let item        = ui.item
                let current_box = item.closest('li').data('panel');
                let panel       = target.data('panel')
                let id          = item.data('id')
                let myArray     = new Array(7,8,9,5);
                let nik         = item.find("[data-nik]").data('nik');
                var kurang  = parseInt(panel) - item.data('panel');

                // * Static Rule move box
                let rule = ruleMovedBox(item.data('panel'), parseInt(panel));
                
                if (!rule.status) {
                    $(this).sortable('cancel');
                    $(ui.sender).sortable('cancel');
                    Swal.fire("Warning!", rule.message, "warning");
                } else {
                    Swal.fire({
                        title: 'Justification',
                        input: 'text',
                        inputAttributes: {
                            autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        showLoaderOnConfirm: true,
                        preConfirm: (justification) => {
                            if(!justification){
                                Swal.showValidationMessage(`Justifikasi tidak boleh kosong`)
                            }

                            return justification
                        },
                    }).then((result) => {
                        if (result.isConfirmed) {
                            let url     = "{{ url('talent-mapping/update-panel') }}"
                            var res     = $.ajax({
                                url     : url,
                                type    : 'POST',
                                data    : {
                                    "_token"        : "{{ csrf_token() }}",
                                    "panel"         : panel,
                                    "id"            : id,
                                    "justification" : result.value
                                },
                                success : function (response) {
                                    console.log(JSON.parse(response))

                                    loadEmployee(0);
                                    if (JSON.parse(response).is_moved == "1") {
                                        let is_down;
                                        let urlModal = "{{ url('talent-profiling/view-career-card') }}" + '/' + nik

                                        if (current_box - 1 == panel) {
                                            is_down = true;
                                        }else {
                                            is_down = false;
                                        }
                                        careerCard(urlModal, JSON.parse(response), is_down)
                                    }
                                }
                            });
                        }
                        $(this).sortable('cancel');
                        $(ui.sender).sortable('cancel');
                    })
                }

                // * INI RULE SEBELUMNYA saya hide ya takutnya butuh lagi (Marking by irp4ndi)
                // if(Math.abs(kurang) == 1) {
                //     Swal.fire({
                //         title: 'Justification',
                //         input: 'text',
                //         inputAttributes: {
                //             autocapitalize: 'off'
                //         },
                //         showCancelButton: true,
                //         confirmButtonText: 'Submit',
                //         showLoaderOnConfirm: true,
                //         preConfirm: (justification) => {
                //             if(!justification){
                //                 Swal.showValidationMessage(`Justifikasi tidak boleh kosong`)
                //             }

                //             return justification
                //         },
                //     }).then((result) => {
                //         if (result.isConfirmed) {
                //             let url     = "{{ url('talent-mapping/update-panel') }}"
                //             var res     = $.ajax({
                //                 url     : url,
                //                 type    : 'POST',
                //                 data    : {
                //                     "_token"        : "{{ csrf_token() }}",
                //                     "panel"         : panel,
                //                     "id"            : id,
                //                     "justification" : result.value
                //                 },
                //                 success : function (response) {
                //                     console.log(JSON.parse(response))
                //                     if(JSON.parse(response).message == 'is_box'){
                //                         Swal.fire('Karyawan ini telah dikalibrasi. Anda tidak bisa memindahkannya lagi selama event ini berlangsung');
                //                         return false;
                //                     } else if(JSON.parse(response).message == 'down'){
                //                         Swal.fire('Perpindahan terlalu jauh. Maximal 1 box');
                //                         return false;
                //                     }

                //                     loadEmployee(0);
                //                     if (JSON.parse(response).is_moved == "1") {
                //                         let is_down;
                //                         let urlModal = "{{ url('talent-profiling/view-career-card') }}" + '/' + nik

                //                         if (current_box - 1 == panel) {
                //                             is_down = true;
                //                         }else {
                //                             is_down = false;
                //                         }
                //                         careerCard(urlModal, JSON.parse(response), is_down)
                //                     }
                //                 }
                //             });
                //         }
                //         $(this).sortable('cancel');
                //         $(ui.sender).sortable('cancel');
                //     })
                // } else {
                //     $(this).sortable('cancel');
                //     $(ui.sender).sortable('cancel');

                //     Swal.fire('Perpindahan terlalu jauh. Maximal 1 box');
                // }
                // * End INI RULE SEBELUMNYA saya hide ya takutnya butuh lagi (Marking by irp4ndi)
            }
        }
    })
    .disableSelection()

    loadEmployee(0);

    // * Function untuk new rule perpindahan box static
    function ruleMovedBox(panelBefore, panelAfter) {
        let moveMsgDiagonally = "Can't move employee diagonnally",
            moveMsg2Box = "Can't move employees more than 1 box",
            falseStatus = false,
            allow = {
                'message': '',
                'status': true
            };

        if (panelBefore == 6) { // * Rule jika panelnya berasal dari box 6
            if(panelAfter == 9 || panelAfter == 1) { // * untuk cek move lebih dari 1 kotak
                allow.status = falseStatus;
                allow.message = moveMsg2Box;
            }else if(panelAfter == 2 || panelAfter == 4 || panelAfter == 7 || panelAfter == 5) {  // * Untuk cek move diagonally
                allow.status = falseStatus;
                allow.message = moveMsgDiagonally;
            }
        }

        if (panelBefore == 3) { // * Rule jika panelnya berasal dari box 3
            if(panelAfter == 7) { // * untuk cek move lebih dari 1 kotak
                allow.status = falseStatus;
                allow.message = moveMsg2Box;
            }else if(panelAfter == 2 || panelAfter == 8 || panelAfter == 9 || panelAfter == 4) { // * Untuk cek move diagonally
                allow.status = falseStatus;
                allow.message = moveMsgDiagonally;
            }
        }

        if (panelBefore == 1) { // * Rule jika panelnya berasal dari box 1
            if (panelAfter == 4 || panelAfter == 6) { // * untuk cek move lebih dari 1 kotak
                allow.status = falseStatus;
                allow.message = moveMsg2Box;
            }else if(panelAfter == 7 || panelAfter == 9 || panelAfter == 5 || panelAfter == 8) { // * Untuk cek move diagonally
                allow.status = falseStatus;
                allow.message = moveMsgDiagonally;
            }
        }
        
        if (panelBefore == 2) { // * Rule jika panelnya berasal dari box 2
            if (panelAfter == 8){ // * untuk cek move lebih dari 1 kotak
                allow.status = falseStatus;
                allow.message = moveMsg2Box;
            }else if(panelAfter == 7 || panelAfter == 9 || panelAfter == 3 || panelAfter == 6) { // * Untuk cek move diagonally
                allow.status = falseStatus;
                allow.message = moveMsgDiagonally;
            }
        }

        if (panelBefore == 5) { // * Rule jika panelnya berasal dari box 5
            if(panelAfter == 1 || panelAfter == 6 || panelAfter == 9 || panelAfter == 4) { // * Untuk cek move diagonally
                allow.status = falseStatus;
                allow.message = moveMsgDiagonally;
            }
        }

        if (panelBefore == 8) { // * Rule jika panelnya berasal dari box 8
            if (panelAfter == 2) { // * untuk cek move lebih dari 1 kotak
                allow.status = falseStatus;
                allow.message = moveMsg2Box;
            }else if(panelAfter == 1 || panelAfter == 3 || panelAfter == 4 || panelAfter == 7) { // * Untuk cek move diagonally
                allow.status = falseStatus;
                allow.message = moveMsgDiagonally;
            }
        }

        if (panelBefore == 9) { // * Rule jika panelnya berasal dari box 9
            if (panelAfter == 4 || panelAfter == 6) { // * untuk cek move lebih dari 1 kotak
                allow.status = falseStatus;
                allow.message = moveMsg2Box;
            }else if(panelAfter == 2 || panelAfter == 5 || panelAfter == 1 || panelAfter == 3) { // * Untuk cek move diagonally
                allow.status = falseStatus;
                allow.message = moveMsgDiagonally;
            }
        }

        if (panelBefore == 7) { // * Rule jika panelnya berasal dari box 7
            if (panelAfter == 3) { // * untuk cek move lebih dari 1 kotak
                allow.status = falseStatus;
                allow.message = moveMsg2Box;
            }else if(panelAfter == 2 || panelAfter == 8 || panelAfter == 1 || panelAfter == 6) { // * Untuk cek move diagonally
                allow.status = falseStatus;
                allow.message = moveMsgDiagonally;
            }
        }
        
        if (panelBefore == 4) { // * Rule jika panelnya berasal dari box 4
            if (panelAfter == 9 || panelAfter == 1) { // * untuk cek move lebih dari 1 kotak
                allow.status = falseStatus;
                allow.message = moveMsg2Box;
            }if(panelAfter == 5 || panelAfter == 8 || panelAfter == 3 || panelAfter == 6) { // * Untuk cek move diagonally
                allow.status = falseStatus;
                allow.message = moveMsgDiagonally;
            }
        }

        return allow;
    }
    
    //history

    function newexportaction(e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;
        dt.one('preXhr', function (e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = 2147483647;
            dt.one('preDraw', function (e, settings) {
                // Call the original action function
                if (button[0].className.indexOf('buttons-copy') >= 0) {
                    $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                    $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                    $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                    $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-print') >= 0) {
                    $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                }
                dt.one('preXhr', function (e, s, data) {
                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                    // Set the property to what it was before exporting.
                    settings._iDisplayStart = oldStart;
                    data.start = oldStart;
                });
                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                setTimeout(dt.ajax.reload, 0);
                // Prevent rendering of the full data to the DOM
                return false;
            });
        });
        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    };
    
    function init_DataTables(){
        if (typeof ($.fn.DataTable) === 'undefined') { return; }
        var tableHistory =  $(".tbl-historyLog").DataTable({
            dom: 'Bflrtip',
            lengthMenu: [[10, -1], [10, "All"]],
            processing: true,
            serverSide: true,
            retrieve: true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            buttons: [
                { 
                    extend: 'excelHtml5', text: '<i class="fa fa-download"></i>',
                    exportOptions: {
                        modifer: {
                            page: 'all',
                            search: 'none'
                        }
                    },
                    columns: [ 0, 1, 2, 3, 4, 5, 6],
                    action: newexportaction
                }
            ],
            order : [5, "desc"],
            ajax: {
                url: "{{ route('talent-calibration.history') }}",
                data: function(d){
                    d.year = $('#yearHistoryLog').val();
                    d.filter_chief = $('body #filter-chief').val();
                    d.filter_position = $('body #filter-position').val();
                    d.filter_grade = $('body #grade').val();
                    d.sub_event     = sub_event;
                }
            },
            columns: [
                {
                    data: "user_id",
                    defaultContent: ""
                },
                {
                    data: "personnel_number",
                    defaultContent: ""
                },
                {
                    data: "position_name",
                    defaultContent: ""
                },
                {
                    data: "panel_before",
                    defaultContent: ""
                },
                {
                    data: "panel",
                    defaultContent: ""
                },
                {
                    data: "justification_box",
                    defaultContent: ""
                },
                {
                    data: "updated_at",
                    defaultContent: ""
                }
            ]
        });
    }

    //history
    $("#btn_history").click(function() {
        $("#xl-modal-historyLog").modal("show");
        init_DataTables();
    });

    $("#reload_ajax").click(function() {
        $(".tbl-historyLog").DataTable().ajax.reload();
    });

    var arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }
    
    $('#kt_datepicker_3_edit').datepicker({
        rtl: KTUtil.isRTL(),
        todayBtn: "linked",
        clearBtn: true,
        todayHighlight: true,
        dateFormat: 'dd/mm/yy',
        templates: arrows
    });
</script>
@endsection
