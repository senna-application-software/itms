<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Successor</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }

        .list,p{
            font-size:11px;
        }
        td{
            padding-bottom: 15.5px;
            vertical-align:top;
        }
    </style>
</head>
<body>
    <div style="width:100%;border-radius:10px;">
        <div class="" style="padding:30px">
            <h3>{{ $employee->personnel_number }}</h3>
            <hr style="border: 1px solid gray;width:100%;">
            <div style="width:100%;margin-top:15px">
            <table style="width:100%">
                    <tr>
                        <td style="width:23%;padding:0px;text-align:center;border:1px solid gray;border-radius:5px;vertical-align:center;padding-top:20px;">
                            <img width="130" height="130" src="{{ public_path("/userprofile/".$img) }}">
                        </td>
                        <td style="width:77%;padding:15px;border: 1px solid gray;border-radius:5px">
                        <table class="list" style="width:100%;">
                            <tr>
                                <td>Member Holding</td>
                                <td>:
                                    {{ $employee->subholding->name_subholding ?? '-' }}
                                </td>
                                <td>Area Nomenklatur</td>
                                <td>: {{ $employee->area_nomenklatur ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>Directorate</td>
                                <td>:
                                {{ $employee->positions->directorate->name_directorate ?? '-' }}
                                </td>
                                <td>Grade Alignment</td>
                                <td>: {{ $employee->person_grade_align ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>Group</td>
                                <td>:
                                {{ $employee->positions->group->name_group ?? '-' }}
                                </td>
                                <td>Job Family</td>
                                <td>: {{ $employee->job_family_align ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>Division</td>
                                <td>:
                                {{ $employee->positions->division->name_division ?? '-' }}
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width:100%;margin-top:15px;">
                <table style="width:100%;">
                    <tr>
                        <td style="width:50%;padding-right:4px;">
                            <div style="width:100%;border:1px solid gray;border-radius:5px;min-height:180px;">
                                <div style="padding-left:15px;padding-right:15px;">
                                    <h4 style="padding:0px">
                                        Posisi Awal
                                    </h4>
                                    <hr style="border: 1px solid gray;width:100%;">
                                    <h5>
                                        {{ $employee->positions->name_position ?? '-' }}
                                    </h5>
                                    <p style="color: gray">
                                        {{ $employee->subholding->name_subholding ?? '-' }}
                                    </p>
                                </div>
                            </div>
                        </td>
                        <td style="width:50%;padding-left:4px;">
                            <div style="width:100%;border:1px solid gray;border-radius:5px;min-height:180px;">
                                <div style="padding-left:15px;padding-right:15px;">
                                    <h4>
                                        Posisi Terpilih
                                    </h4>
                                    <hr style="border: 1px solid gray;width:100%;">
                                    <h5>
                                        {{ $talent->get_sub_event->get_position->name_position }}
                                    </h5>
                                    <p style="color: gray">
                                        {{ $talent->get_sub_event->get_position->get_sub_holding->name_subholding }}
                                    </p>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width:100%;display: inline-block;vertical-align: top;margin-top:5px;">
                <div style="width:95%;border:1px solid gray;padding-left:15px;padding-right:15px;border-radius:5px">
                    <h4 style="padding:0px">
                        Endorsmen Position [{{ $employee->subholding->name_subholding ?? '-' }}] {{ $talent->get_sub_event->get_position->name_position }}
                    </h4>
                    <hr style="border: 1px solid gray;width:100%;">
                    <table style="width:100%;">
                        <tbody>
                            @foreach ($endorsment as $index =>  $item)                             
                                <tr>
                                    <td style="text-align:center">
                                        <h5>
                                    @php
                                        if ( $item->aspiration_type == "individual")
                                        {
                                            echo "INDIVIDUAL (IA)";
                                        }elseif ($item->aspiration_type == "supervisor")
                                        {
                                            echo "SUPERVISOR (SA)";
                                        }
                                        elseif ($item->aspiration_type == "unit")
                                        {
                                            echo "UNIT (UA)";
                                        }
                                        elseif ($item->aspiration_type == "job_holder")
                                        {
                                            echo "JOB HOLDER (JA)";
                                        }
                                        elseif ($item->aspiration_type == "-")
                                        {
                                            echo "-";
                                        }
                                    @endphp
                                        </h5>
                                    </td>
                                    <td style="text-align:center">
                                        <h5>
                                            {{  $item->employee->personnel_number}}
                                        </h5>
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <div style="width:100%;top;margin-top:20px;">
                <table style="width:100%;">
                    <tr>
                        <td style="width:20%;padding-right:4px">
                            <div style="border:1px solid gray;padding-left:15px;padding-right:15px;border-radius:5px;min-height:140px;">
                                <h4 style="padding:0px">
                                    Hasil Assessment
                                </h4>
                                <hr style="border: 1px solid gray;width:100%;">
                                <h5>
                                    {{ empty($talent->talent_day_result) ? "-" : $talent->talent_day_result }}
                                </h5>
                            </div>
                        </td>
                        <td style="width:20%;padding-right:4px;padding-left:4px">
                            <div style="border:1px solid gray;padding-left:15px;padding-right:15px;border-radius:5px;min-height:140px;">
                                <h4 style="padding:0px">
                                    Talent Cluster
                                </h4>
                                <hr style="border: 1px solid gray;width:100%;">
                                <h5>
                                    {{ empty($talent_clushter) ? "-" : $talent_clushter }}
                                </h5
                            </div>
                        </td>
                        <td style="width:20%;padding-left:4px">
                            <div style="border:1px solid gray;padding-left:15px;padding-right:15px;border-radius:5px;min-height:140px;">
                                <h4 style="padding:0px">
                                    EQS
                                </h4>
                                <hr style="border: 1px solid gray;width:100%;">
                                <h5>
                                    {{ empty($eqs["eqs_final"]) ? "-" : $eqs["eqs_final"] }}
                                </h5
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>