{{-- Extends layout --}}
@extends('layout.default')

{{-- Content Section --}}
@section('page_toolbar')
    <a href="{{ $back }}" class="btn btn-secondary">Back</a>
@endsection
@section('modal')

@endsection

@section('content')
<div class="col-md-12">
    <div class="card card-custom gutter-b">
        <div class="card-body">
            <div class="tab-content">
                <div class="row">
                    <div class="col-md-12">
                        <h3>{{ $employee->personnel_number }}</h3>
                        <hr>
                    </div>
                    <div class="col-md-2">
                        <img class="img-thumbnail" src="{{ get_employee_pict($employee->nik) }}">
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table">
                                    <tbody><tr>
                                        <td>Member Holding</td>
                                        <td>:
                                            {{ $employee->subholding->name_subholding ?? '-' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Directorate</td>
                                        <td>:
                                        {{ $employee->positions->directorate->name_directorate ?? '-' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Group</td>
                                        <td>:
                                        {{ $employee->positions->group->name_group ?? '-' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Division</td>
                                        <td>:
                                        {{ $employee->positions->division->name_division ?? '-' }}
                                        </td>
                                    </tr>
                                </tbody></table>
                            </div>
                            <div class="col-md-6">
                                <table class="table">
                                    <tbody><tr>
                                        <td>Area Nomenklatur</td>
                                        <td>: {{ $employee->area_nomenklatur ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Grade Alignment</td>
                                        <td>: {{ $employee->person_grade_align ?? '-' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Job Family</td>
                                        <td>: {{ $employee->job_family_align ?? '-' }}</td>
                                    </tr>
                                </tbody></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row px-4">
    <div class="col-md-5 mt-2 d-flex align-items-stretch">
        <div class="card card-custom gutter-b w-100">
            <div class="card-body">
                <div class="tab-content">
                    <h3>Posisi Awal</h3>
                    <hr>
                    <h4>
                        {{ $employee->positions->name_position ?? '-' }}
                    </h4>
                    <p class="text-muted">
                        {{ $employee->subholding->name_subholding ?? '-' }}
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-2 mt-2 d-flex align-items-stretch">
        <div class="gutter-b text-center w-100 d-flex align-items-center">
            <div class="card-body">
                <div class="tab-content">
                    <i class="fa fa-arrow-right text-primary" style="font-size:60px;"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-5 mt-2 d-flex align-items-stretch">
        <div class="card card-custom gutter-b w-100">
            <div class="card-body">
                <div class="tab-content">
                    <h3>Posisi Terpilih</h3>
                    <hr>
                    <h4>
                        {{ $talent->get_sub_event->get_position->name_position }}
                    </h4>
                    <p class="text-muted">
                        {{ $talent->get_sub_event->get_position->get_sub_holding->name_subholding }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row px-4">
    <div class="col-md-12 mt-2">
        <div class="card card-custom gutter-b w-100">
            <div class="card-body">
                <div class="tab-content">
                    <h3>Endorsmen Position [{{ $employee->subholding->name_subholding ?? '-' }}] {{ $talent->get_sub_event->get_position->name_position }}</h3>
                    <table class="table mt-5">
                        <tbody>
                            @foreach ($endorsment as $index =>  $item)                             
                                <tr>
                                    <td>
                                    @php
                                        if ( $item->aspiration_type == "individual")
                                        {
                                            echo "INDIVIDUAL (IA)";
                                        }elseif ($item->aspiration_type == "supervisor")
                                        {
                                            echo "SUPERVISOR (SA)";
                                        }
                                        elseif ($item->aspiration_type == "unit")
                                        {
                                            echo "UNIT (UA)";
                                        }
                                        elseif ($item->aspiration_type == "job_holder")
                                        {
                                            echo "JOB HOLDER (JA)";
                                        }
                                        elseif ($item->aspiration_type == "-")
                                        {
                                            echo "-";
                                        }
                                    @endphp
                                    </td>
                                    <td>
                                        {{  $item->employee->personnel_number}}
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row px-4">
    <div class="col-md-4 mt-2 d-flex align-items-stretch">
        <div class="card card-custom gutter-b w-100">
            <div class="card-body">
                <div class="tab-content">
                    <h3>Hasil Assessment</h3>
                    <hr>
                    <h4>
                        {{ empty($talent->talent_day_result) ? "-" : $talent->talent_day_result }}
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 mt-2 d-flex align-items-stretch">
        <div class="card card-custom gutter-b w-100">
            <div class="card-body">
                <div class="tab-content">
                    <h3>Talent Cluster</h3>
                    <hr>
                    <h4>
                        {{ empty($talent_clushter) ? "-" : $talent_clushter }}
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 mt-2 d-flex align-items-stretch">
        <div class="card card-custom gutter-b w-100">
            <div class="card-body">
                <div class="tab-content">
                    <h3>EQS</h3>
                    <hr>
                    <h4>
                        {{ empty($eqs["eqs_final"]) ? "-" : $eqs["eqs_final"] }}
                    </h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/event/event.js') }}" type="text/javascript"></script>

@if (session('status') == 'success')
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif

@if (session('status') == 'error')
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif
@endsection
