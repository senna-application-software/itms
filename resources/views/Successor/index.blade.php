{{-- Extends layout --}}
@extends('layout.default')

{{-- Content Section --}}
@section('page_toolbar')
    {{-- kalo di subheader mau ditambahkan button action taruh di sini --}}
@endsection

@section('modal')

@endsection

@section('content')
    <div class="card card-custom mb-5">
        <form id="filter">
            <div class="card-body">
                <h3 class="mb-6">
                    Filter Data
                </h3>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                        <label for="">Posisi Terpilih</label>
                        <select class="form-control" name="" id="posisi_terpilih">
                            <option value="">-- PILIH POSISI TERPILIH --</option>
                            @foreach($posisi_terpilih as $val)
                                <option value="{{ $val->code_position }}">{{ "[".$val->get_sub_holding->name_subholding."] ".$val->name_position }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Sumber Aspirasi</label>
                            <select class="form-control" name="" id="sumber_aspirasi">
                                <option value="">-- PILIH SUMBER ASPIRASI --</option>
                                <option value="individual">INDIVIDUAL (IA)</option>
                                <option value="supervisor">SUPERVISOR (SA)</option>
                                <option value="unit">UNIT (UA)</option>
                                <option value="job_holder">JOB HOLDER (JA)</option>
                                <option value="commitee">Commitee</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Grade</label>
                            <select class="form-control" name="" id="grade">
                                <option value="">-- PILIH GRADE --</option>
                                @foreach($grade as $val)
                                    @if(empty($val->person_grade_align))
                                    <option value="{{ $val->person_grade_align }}">-</option>
                                    @else
                                    <option value="{{ $val->person_grade_align }}">{{ $val->person_grade_align }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 pt-2">
                        <div class="form-group mt-6">
                            <button type="submit" class="btn btn-primary">SUBMIT</button>
                            <button type="reset" id="reset" class="btn btn-danger">RESET</button>
                        </div>    
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="card card-custom">
        <div class="card-body">
            {{-- <div class="overflow-auto"> --}}
                <table class="table table-bordered table-hover" id="table-event">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Pegawai</th>
                            <th>Posisi Sebelum</th>
                            <th>Posisi Terpilih</th>
                            <th>Sumber Aspirasi</th>
                            <th>Tanggal Terpilih</th>
                            <th>Jenis Event</th>
                            <th>Grade</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            {{-- </div> --}}
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/event/event.js') }}" type="text/javascript"></script>

<script>
    let posisi_terpilih = "";
    let sumber_aspirasi = "";
    let grade = "";

    $("#posisi_terpilih").select2();
    $("#sumber_aspirasi").select2();
    $("#grade").select2();

    $("#reset").click(function (e) { 
        e.preventDefault();
        $("#posisi_terpilih").val("");
        $("#sumber_aspirasi").val("");
        $("#grade").val("");
        $("#select2-posisi_terpilih-container").html("-- PILIH POSISI TERPILIH --");
        $("#select2-sumber_aspirasi-container").html("-- PILIH SUMBER ASPIRASI --");
        $("#select2-grade-container").html("-- PILIH GRADE --");
        
        posisi_terpilih = "";
        sumber_aspirasi = "";
        grade = "";
        getData();
        
    });

    getData();

    function getData() {
        $('#table-event').dataTable().fnDestroy();
        $('#table-event').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: "{{ route('successor.datatable') }}",
                method: 'GET',
                data: {
                    'posisi_terpilih' : posisi_terpilih,
                    'sumber_aspirasi' : sumber_aspirasi,
                    'grade' : grade,
                },
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex'},
                { data: 'nama_pegawai', name: 'nama_pegawai'},
                { data: 'posisi_sebelum', name: 'posisi_sebelum'},
                { data: 'posisi_terpilih', name: 'posisi_terpilih'},
                { data: 'sumber_aspirasi', name: 'sumber_aspirasi'},
                { data: 'tanggal_terpilih', name: 'tanggal_terpilih'},
                { data: 'vacant_type', name: 'vacant_type'},
                { data: 'grade', name: 'grade'},
                { data: 'status', name: 'status'},
                { data: 'button', name: 'button'},

            ]
        });
    }

    $("#filter").submit(function (e) { 
        e.preventDefault();
        posisi_terpilih = $("#posisi_terpilih").val();
        sumber_aspirasi = $("#sumber_aspirasi").val();
        grade = $("#grade").val();
        getData();
    });

</script>


@if (session('status') == 'success')
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif

@if (session('status') == 'error')
    <script>
        $(document).ready(function() {
            Swal.fire({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                icon: "{{ session('status') }}",
                buttonsStyling: false,
                confirmButtonText: "close",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>
@endif
@endsection
