<!--begin::Main-->
<div class="d-flex flex-column flex-root">
	<!--begin::Login-->
	<div class="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
		<!--begin::Aside-->
		<div class="login-aside order-2 order-lg-1 d-flex flex-row-auto position-relative overflow-hidden">
			<!--begin: Aside Container-->
			<div class="d-flex flex-column-fluid flex-column justify-content-between py-9 px-7 py-lg-13 px-lg-35">
				<!--begin::Logo-->
				<a href="#" class="text-center pt-2">
					<img src="{{ asset('media/logos/'.env('COMPANY_LOGO')) }}" class="max-h-75px" alt="" />
				</a>
				<!--end::Logo-->
				<!--begin::Aside body-->
				<div class="d-flex flex-column-fluid flex-column flex-center">
					<!--begin::Signin-->
					<div class="login-form login-signin py-11">
						<!--begin::Form-->
						<form class="form" novalidate="novalidate" id="kt_login_signin_form" method="post" action="{{ route('auth.validate') }}">
							@csrf						
							@include('auth.flash')
							<!--begin::Title-->
							<div class="text-center pb-8">
								<h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Login</h2>
							</div>
							<!--end::Title-->
							<!--begin::Form group-->
							<div class="form-group">
								<label class="font-size-h6 font-weight-bolder text-dark">NIK atau Email</label>
								<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" type="text" name="nik" autocomplete="off" />
							</div>
							<!--end::Form group-->
							<!--begin::Form group-->
							<div class="form-group">
								<div class="d-flex justify-content-between mt-n5">
									<label class="font-size-h6 font-weight-bolder text-dark pt-5">Password</label>
								</div>
								<div class="input-icon input-icon-right">
									<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" type="password" name="password" id="password" autocomplete="off" value="itms2022"/>
									<span>
										<i class="fa fa-eye text-secondary icon-md mr-5" id="icon-view-pwd"></i>
									</span>
								</div>
							</div>
							<!--end::Form group-->
							<!--begin::Action-->
							<div class="text-center pt-2">
								<button id="kt_login_signin_submit" class="btn btn-dark font-weight-bolder font-size-h6 px-8 py-4 my-3">Login</button>
							</div>
							<!--end::Action-->
						</form>
						{{-- @include('auth.credential_example')	 --}}
						<!--end::Form-->
					</div>
					<!--end::Signin-->
				</div>
				<!--end::Aside body-->
			</div>
			<!--end: Aside Container-->
		</div>
		<!--begin::Aside-->
		<!--begin::Content-->
		<div class="content order-1 order-lg-2 d-flex flex-column w-100 pb-0" style="background-color: #B1DCED;">
			<!--begin::Title-->
			<div class="d-flex flex-column justify-content-center text-center pt-lg-40 pt-md-5 pt-sm-5 px-lg-0 pt-5 px-7">
				<h3 class="display4 font-weight-bolder my-7 text-dark" style="color: #986923;">Selamat Datang</h3>
				<p class="font-weight-bolder font-size-h2-md font-size-lg text-dark opacity-70">{{ env('APP_NAME') }}
				<br />{{ env('COMPANY_NAME') }}</p>
			</div>
			<!--end::Title-->
			<!--begin::Image-->
			<div class="content-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-top bgi-position-x-center" style="background-image: url({{asset('media/svg/illustrations/login-visual-2.svg') }});"></div>
			<!--end::Image-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Login-->
</div>
<!--end::Main-->