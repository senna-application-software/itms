<div class="d-flex flex-column flex-root">
	<!--begin::Login-->
	<div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
		<!--begin::Aside-->
		<div class="login-aside d-flex flex-column flex-row-auto" style="background-color: #F2C98A;">
			<!--begin::Aside Top-->
			<div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15">
				<!--begin::Aside header-->
				<a href="#" class="text-center mb-10">
					<!-- <img src="assets/media/logos/logo-letter-1.png" class="max-h-70px" alt="" /> -->
					<img src="{{ asset('media/logos/'.env('COMPANY_LOGO')) }}" class="max-h-75px" alt="" />
				</a>
				<!--end::Aside header-->
				<!--begin::Aside title-->
				<h3 class="font-weight-bolder text-center font-size-h4 font-size-h1-lg" style="color: #986923;">{{ env('APP_NAME') }}
				<br />{{ env('COMPANY_NAME') }}</h3>
				<!--end::Aside title-->
			</div>
			<!--end::Aside Top-->
			<!--begin::Aside Bottom-->
			<div class="aside-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center" style="background-image: url(media/svg/illustrations/login-visual-1.svg)"></div>
			<!--end::Aside Bottom-->
		</div>
		<!--begin::Aside-->
		<!--begin::Content-->
		<div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
			<!--begin::Content body-->
			<div class="d-flex flex-column-fluid flex-center">
				<!--begin::Signin-->
				<div class="login-form login-signin">
					<!--begin::Form-->
					<form class="form" novalidate="novalidate" id="kt_login_signin_form">
						<!--begin::Title-->
						<div class="pb-13 pt-lg-0 pt-5">
							<h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Selamat Datang</h3>
						</div>
						<!--begin::Title-->
						<!--begin::Form group-->
						<div class="form-group">
							<label class="font-size-h6 font-weight-bolder text-dark">NIK</label>
							<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="text" name="username" autocomplete="off" />
						</div>
						<!--end::Form group-->
						<!--begin::Form group-->
						<div class="form-group">
							<div class="d-flex justify-content-between mt-n5">
								<label class="font-size-h6 font-weight-bolder text-dark pt-5">Password</label>
							</div>
							<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="password" name="password" autocomplete="off" />
						</div>
						<!--end::Form group-->
						<!--begin::Action-->
						<div class="pb-lg-0 pb-5">
							<button type="button" id="kt_login_signin_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Log In</button>
						</div>
						<!--end::Action-->
					</form>
					<!--end::Form-->
				</div>
				<!--end::Signin-->
			</div>
			<!--end::Content body-->
			<!--begin::Content footer-->
			<div class="d-flex justify-content-lg-start justify-content-center align-items-end py-7 py-lg-0">
				<div class="text-dark-50 font-size-lg font-weight-bolder mr-10">
					<span class="mr-1">&copy; {{ date('Y') }}</span>
					<a href="#" class="text-dark-75 text-hover-primary">{{ env('COMPANY_NAME') }}</a>
				</div>
			</div>
			<!--end::Content footer-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Login-->
</div>