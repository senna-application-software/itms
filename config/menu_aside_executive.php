<?php
// talent committee
return [
    'items' => [
        // Dashboard
        [
            'title' => 'Dashboard',
            'root' => false,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => 'dashboard',
            'new-tab' => false,
        ]
    ]

];