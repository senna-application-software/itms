<?php
// Menu Admin Dipindah kehelper dengan nama function menu_admin()
// Admin menu
return [
    'items' => [
        // Dashboard
        [
            'title' => 'Dashboard',
            'root' => false,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => 'dashboard',
            'new-tab' => false,
        ],

        [
            'title' => 'Pending Task',
            'root' => false,
            'icon' => 'media/svg/icons/Navigation/Waiting.svg',
            'page' => 'pending-task',
            'new-tab' => false,
            'label' => [  // Coba set display = true dan lihat apa yang terjadi
                'type'  => 'label-rounded-circle label label-danger jumlah_pending_task',
                'value' => 4,
                'display' => true
            ],
        ],

        [
            'section' => 'Main Menu',
        ],

        [
            'title' => 'Event',
            'icon' => 'media/svg/icons/Devices/LTE2.svg',
            'bullet' => 'line',
            'root' => false,
            'submenu' => [
                [
                    'title' => 'Event Aspiration',
                    'page' => 'event'
                ],
                [
                    'title' => 'Event Positions',
                    'page' => 'sub-event'
                ]
            ]
        ],
        [
            'title' => 'On Going Event',
            'icon' => 'media/svg/icons/General/Star.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Talent Sourcing',
                    'page' => 'talent-sourcing'
                ],
                [
                    'title' => 'Talent Profiling',
                    'page' => 'talent-profiling'
                ],
                [
                    'title' => 'Talent Mapping',
                    'page' => 'talent-mapping'
                ],
                [
                    'title' => 'Talent Day',
                    'page' => 'talent-day'
                ],
                [
                    'title' => 'Talent Review',
                    'page' => 'talent-review'
                ],
                [
                    'title' => 'Talent Selection',
                    'page' => 'talent-selection'
                ]
            ]
        ],
        [
            'title' => 'Struktur Organisasi',
            'icon' => 'media/svg/icons/Code/Git4.svg',
            'bullet' => 'line',
            'root' => false,
            'page' => 'struktur-organisasi',
            'new-tab' => false,
            'label' => [  // Coba set display = true dan lihat apa yang terjadi
                'type'  => 'label-rounded label label-primary',
                'value' => 1,
                'display' => false
            ],
        ],
        [
            'title' => 'Learning & Development',
            'icon' => 'media/svg/icons/Home/Book-open.svg',
            'bullet' => 'line',
            'root' => false,
            'page' => '#',
            'new-tab' => false,
            'label' => [  // Coba set display = true dan lihat apa yang terjadi
                'type'  => 'label-rounded label label-primary',
                'value' => 1,
                'display' => false
            ],
        ],
        [
            'title' => 'Successor List',
            'icon' => 'media/svg/icons/Communication/Group.svg',
            'bullet' => 'line',
            'root' => false,
            'page' => 'successor-list',
            'new-tab' => false,
            'label' => [  // Coba set display = true dan lihat apa yang terjadi
                'type'  => 'label-rounded label label-primary',
                'value' => 1,
                'display' => false
            ],
        ],
        [
            'title' => 'Search Employee',
            'root' => false,
            'icon' => 'media/svg/icons/General/Search.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => 'SearchEmployee/SearchDataEmployee',
            'new-tab' => false,
        ],
        
        
        // [
        //     'title' => 'List Suksesor',
        //     'icon' => 'media/svg/icons/General/Star.svg',
        //     'bullet' => 'line',
        //     'root' => false,
        //     'page' => '#',
        //     'new-tab' => false,
        //     'label' => [  // Coba set display = true dan lihat apa yang terjadi
        //         'type'  => 'label-rounded label label-primary',
        //         'value' => 1,
        //         'display' => false
        //     ],
        // ],

        // [
        //     'title' => 'Kotak Kalibrasi',
        //     'icon' => 'media/svg/icons/Communication/Dial-numbers.svg',
        //     'bullet' => 'line',
        //     'root' => false,
        //     'page' => 'calibration',
        //     'new-tab' => false,
        //     'label' => [  // Coba set display = true dan lihat apa yang terjadi
        //         'type'  => 'label-rounded label label-primary',
        //         'value' => 1,
        //         'display' => false
        //     ],
        // ],

        // [
        //     'title' => 'Talent Profile',
        //     'icon' => 'media/svg/icons/Communication/Group.svg',
        //     'bullet' => 'line',
        //     'root' => false,
        //     'page' => 'talent-profilling',
        //     'new-tab' => false,
        //     'label' => [  // Coba set display = true dan lihat apa yang terjadi
        //         'type'  => 'label-rounded label label-primary',
        //         'value' => 1,
        //         'display' => false
        //     ],
        // ],

        // Custom
        [
            'section' => 'Master Data',
        ],
        [
            'title' => 'Master Data',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Master Employee',
                    'page' => 'masterData/masterDataEmployee'
                ],
                [
                    'title' => 'Master Posisi',
                    'page' => '#'
                ],
                [
                    'title' => 'Master Grade',
                    'page' => '#'
                ],
                [
                    'title' => 'Master Unit',
                    'page' => '#'
                ],
                [
                    'title' => 'Master Subholding',
                    'page' => '#'
                ],
                [
                    'title' => 'Master Competency',
                    'page' => '#'
                ],
                [
                    'title' => 'Master Learning',
                    'page' => '#'
                ],
                [
                    'title' => 'Master Training',
                    'page' => '#'
                ],
                // TODO: saya hide dulu 
                // [
                //     'title' => 'Histori Berita Acara',
                //     'page' => 'Histori/BeritaAcara'
                // ]
            ]
        ],
        [
            'title' => 'Tools & Setting',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Vacant Position',
                    'page' => '#'
                ],
                [
                    'title' => 'Employee Eligible',
                    'page' => '#'
                ],
                [
                    'title' => 'Employee External',
                    'page' => 'employee_external'
                ]
            ]
        ],

        // Custom
        [
            'section' => 'My Profile',
        ],
        [
            'title' => 'Personal Information',
            'icon' => 'media/svg/icons/General/User.svg',
            'bullet' => 'line',
            'root' => false
        ],
    ]

];
