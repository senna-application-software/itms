<?php
// Header menu
return [

    'items' => [
        [],
        [
            'title' => env('APP_NAME'),
            'root' => false,
            'page' => '#',
            'new-tab' => false,
        ]
    ]

];
