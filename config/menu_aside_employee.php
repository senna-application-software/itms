<?php
//  Menu Employee Dipindah kehelper dengan nama function menu_aspiration()
// employee    
return [
    'items' => [
        // Dashboard
        [
            'title' => 'Dashboard',
            'root' => false,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/dashboard',
            'new-tab' => false,
        ],

        [
            'section' => 'Aspirations',
        ],

        [
            'title' => 'Aspirations',
            'icon' => 'media/svg/icons/Communication/Chat4.svg',
            'bullet' => 'line',
            'root' => false,
            'page' => 'aspirations/individual',
            'new-tab' => false,
            'submenu' => [
                [
                    'title' => 'Individual Aspiration',
                    'page' => 'aspirations/individual'
                ],
                [
                    'title' => 'Supervisor Aspiration',
                    'page' => 'aspirations/supervisor'
                ],
                [
                    'title' => 'Job Holder Aspiration',
                    'page' => 'aspirations/job_holder'
                ],
                [
                    'title' => 'Unit Aspiration',
                    'page' => 'aspirations/unit'
                ]
            ]
        ],

        // Custom
        [
            'section' => 'My Profile',
        ],
        [
            'title' => 'Personal Information',
            'icon' => 'media/svg/icons/General/User.svg',
            'bullet' => 'line',
            'root' => false,
            'page' => 'my-profile',
        ],
    ]

];
