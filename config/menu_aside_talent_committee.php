<?php
// talent committee
return [
    'items' => [
        // Dashboard
        [
            'title' => 'Dashboard',
            'root' => false,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => 'dashboard',
            'new-tab' => false,
        ],
        [
            'title' => 'Search Employee',
            'root' => false,
            'icon' => 'media/svg/icons/General/Search.svg',
            'page' => 'SearchEmployee/SearchDataEmployee',
            'new-tab' => false,
        ],
        [
            'title' => 'Talent Pool',
            'root' => false,
            'icon' => 'media/svg/icons/Communication/Shield-user.svg',
            'page' => 'settings/talent_poll',
            'new-tab' => false,
        ],

        [
            'section' => 'Main Menu',
        ],

        [
            'title' => 'Talent Mapping',
            'icon' => 'media/svg/icons/Files/Selected-file.svg',
            'bullet' => 'line',
            'root' => false,
            'page' => 'talent-mapping',
            'new-tab' => false,
            'label' => [
                'type'  => 'label-rounded label label-primary',
                'value' => 1,
                'display' => false
            ],
        ],

        [
            'title' => 'Talent Day',
            'icon' => 'media/svg/icons/Files/Selected-file.svg',
            'bullet' => 'line',
            'root' => false,
            'page' => 'talent-day',
            'new-tab' => false,
            'label' => [
                'type'  => 'label-rounded label label-primary',
                'value' => 1,
                'display' => false
            ],
        ],

        [
            'title' => 'Talent Review',
            'icon' => 'media/svg/icons/Files/Selected-file.svg',
            'bullet' => 'line',
            'root' => false,
            'page' => 'talent-review',
            'new-tab' => false,
            'label' => [
                'type'  => 'label-rounded label label-primary',
                'value' => 1,
                'display' => false
            ],
        ],

        [
            'title' => 'Talent Selection',
            'icon' => 'media/svg/icons/Files/Selected-file.svg',
            'bullet' => 'line',
            'root' => false,
            'page' => 'talent-selection',
            'new-tab' => false,
            'label' => [
                'type'  => 'label-rounded label label-primary',
                'value' => 1,
                'display' => false
            ],
        ]
    ]

];